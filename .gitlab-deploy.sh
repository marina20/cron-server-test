#!/bin/bash

#Get servers list
set -f
string=$DEPLOY_SERVER
key=$CRON_PEM
array=(${string//,/ })

#Iterate servers for deploy and pull last commit
for i in "${!array[@]}"; do
    echo "Deploying information to EC2  cron server and Gitlab"
    echo "Deploy project on server ${array[i]}"
    echo "Step: ssh -i ${key} ec2-user@${array[i]}"
    chmod 400 ${key}
    ifconfig
    ssh -i ${key} ec2-user@${array[i]} "cd /home/ec2-user/cron-server-test && git pull origin master"
done
