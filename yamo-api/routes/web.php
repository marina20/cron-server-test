<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// two endpoints just for test
$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/hello', function () use ($router) {
    return json_encode(['response'=>'Hello world! The server works.']);
});

// protected routes
$router->group(['prefix'=>'api/v1/protected', 'middleware' => 'auth'], function () use ($router) {

    $router->patch('profile/{id}', 'ProfileController@update');

    $router->get('user/{id}', 'UserController@show');
    $router->post('user/addTag', 'UserController@addTagToUser');

    $router->get('user/{id}/profile', 'UserProfileController@show');
    $router->patch('user/{id}/profile', 'UserProfileController@update');
    $router->post('user/delete', 'UserProfileController@delete');
    $router->get('user/profile/next_delivery_date', 'UserProfileController@getNextDeliveryDate');

    $router->post('order', 'OrderController@store');
    $router->patch('order/{id}', 'OrderController@update');
    $router->delete('order/{id}', 'OrderController@delete');
    $router->get('order/{id}/cancel', 'OrderController@cancel');

    $router->get('order', 'OrderController@index');
    $router->get('my-account/orders', 'OrderController@list');
    $router->get('order/{id}', 'OrderController@show');

    $router->get('subscription/{id}/pause', 'SubscriptionController@pause');
    $router->post('subscription/{id}/frequency', 'SubscriptionController@frequency');
    $router->get('subscription/{id}/next_delivery_date', 'SubscriptionController@getNextDeliveryDate');
    $router->get('my-account/subscriptions', 'SubscriptionController@myAccountSubscriptions');
    $router->get('my-account/subscription/{id}/box/list', 'SubscriptionController@myAccountBoxList');
    $router->post('my-account/subscription/{id}/box/save', 'SubscriptionController@myAccountBoxSave');
    $router->post('my-account/subscription/{id}/pause', 'SubscriptionController@setNextDeliveryDate');
    $router->post('my-account/subscription/{id}/cancel', 'SubscriptionController@cancel');
    $router->get('my-account/invite-friend', 'UserProfileController@getInviteFriendData');


    $router->post('checkout/result', 'CheckoutController@result');
    $router->get('checkout/result/{id}', 'CheckoutController@resultProducts');

    $router->post('checkout/wizard/order', 'CheckoutController@funnelCheckoutSecondStep');
    $router->post('products/cart/funnel', 'ProductController@cartFunnelProducts'); // using

    $router->post('newsletter/add', 'NewsletterController@add');
    $router->post('newsletter/remove', 'NewsletterController@remove');

    $router->get('loyalty/box', 'LoyaltyController@box');
});

// public routes
$router->group(['prefix'=>'api/v1/public'], function () use ($router) {

    $router->post('/auth/login', 'AuthController@loginPost');
    $router->get('delivery_dates', 'ProductController@deliveryDatesEndpoint');
    $router->post('/password/email', 'PasswordController@postEmail');
    $router->post('/password/reset', 'PasswordController@postReset');

    $router->get('/auth/verify/{confirmationCode}', 'UserController@confirm');

    $router->post('/auth/register', 'UserController@register');

    $router->post('/user/register', 'UserController@registerWithEmail');

    $router->post('checkout/wizard', 'CheckoutController@funnelCheckoutFirstStep');

    $router->post('checkout/shop', 'CheckoutController@shopCheckoutFirstStep');
    $router->post('/tracking', 'TrackingController@save'); // only a alias for FE

    $router->get('products', 'ProductController@products');
    $router->post('products/filteredproducts', 'ProductController@filteredProducts');
    $router->get('products/categories', 'ProductController@categories');
    $router->get('products/main-categories', 'ProductController@mainCategories');
    $router->get('products/filters', 'ProductController@filters');
    $router->get('products/ingredients', 'ProductController@ingredients');
    $router->post('products/tracking/filters', 'ProductController@trackingFilters');

    $router->get('bundles/{nameKey}', 'ProductController@getBundle');

    $router->get('products/default-packages', 'ProductController@defaultPackages');
    $router->get('products/package-selection', 'ProductController@getFirstPackageContent');
    $router->post('products/customize/save', 'ProductController@listCustomizeSave');
    $router->post('product/customize/save', 'ProductController@listCustomizeSave'); // temp alias
    $router->get('products/frequency', 'ProductController@frequency');
    $router->get('products/customize', 'ProductController@listCustomizeAllProducts');

    $router->post('products/cart', 'ProductController@cart');

    $router->get('category', 'CategoryController@index');
    $router->get('category/{id}', 'CategoryController@show');

    $router->get('testimonial', 'TestimonialController@index');

    $router->get('press', 'PressController@index');

    $router->get('loyalty', 'LoyaltyController@index');
    /**
     * For testing the translatio
     *
     */
    $router->get('translation/test', 'TranslationController@testMethod');
    $router->get('translation/set/{locale}', 'TranslationController@setLocale');

    $router->post('newsletter', 'NewsletterController@store');
    $router->get('newsletter/confirm/{confirmationCode}', 'NewsletterController@confirm');

    $router->get('/referral/check','ReferralController@checkReferral');
    $router->get('/voucher/check','VoucherController@checkVoucher');


    $router->post('email/test/order/confirmation', 'EmailTestController@OrderConfirmation');
    $router->post('email/test/single-box/confirmation', 'EmailTestController@SingleBoxConfirmation');
    $router->post('email/test/reset/password', 'EmailTestController@ResetPassword');
    $router->post('email/test/user/registration', 'EmailTestController@UserConfirmation');
    $router->post('email/test/subscription/confirmation', 'EmailTestController@SubscriptionConfirmation');
    $router->post('email/test/delivery/reminder', 'EmailTestController@DeliveryReminder');
    $router->post('email/test/subscription/cancel', 'EmailTestController@SubscriptionCancel');
    $router->post('email/test/newsletter/activation', 'EmailTestController@NewsletterActivation');
    $router->post('email/test/mass/delivery/reminder', 'EmailTestController@MassDeliveryReminder');
    $router->post('email/test/adyen/link', 'EmailTestController@GetAdyenLink');
    $router->post('email/test/adyen/sdk', 'EmailTestController@TestAdyenSdk');
    $router->post('email/test/excel', 'EmailTestController@TestExcelAttachment');
    $router->post('email/test/sofort/link', 'EmailTestController@GetSofortLink');
    $router->post('email/test/subscription/change', 'EmailTestController@SubscriptionChange');

    $router->post('email/test/recurring/failure/cancel', 'EmailTestController@RecurringFailureCancel');
    $router->post('email/test/recurring/failure', 'EmailTestController@RecurringFailure');
    
	$router->post('mfg/refund', 'CheckoutController@refundMFG');
	$router->post('adyen/notification', 'CheckoutController@adyenNotification');
	
	$router->get('zip-city/{search}', 'UserProfileController@getZipCity');
});