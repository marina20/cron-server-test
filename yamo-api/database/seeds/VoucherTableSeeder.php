<?php

use App\Models\ProductRegion;
use App\Models\ReferralUrl;
use App\Models\Region;
use App\Models\Product;
use App\Models\VoucherProduct;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class VoucherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new \App\Models\User();
        $user->email = "tester@test.com";
        $user->name = "A tester";
        $user->save();

        $profile = new \App\Models\Profile();
        $profile->user_id = $user->id;
        $profile->first_name = "Tester";
        $profile->last_name = "Tester";
        $profile->shipping_country = "DE";
        $profile->billing_country = "DE";
        $profile->customer_birthday = "2000-01-01 00:00:00";
        $profile->save();

        $user = new \App\Models\User();
        $user->email = "ref@test.com";
        $user->name = "A user";
        $user->save();

        $profile = new \App\Models\Profile();
        $profile->user_id = $user->id;
        $profile->first_name = "REF";
        $profile->last_name = "UsEr";
        $profile->shipping_country = "DE";
        $profile->billing_country = "DE";
        $profile->customer_birthday = "2015-01-01 00:00:00";
        $profile->save();

        $germanRegion = Region::where(['country_id' => 1])->first();

        $user = new \App\Models\User();
        $user->email = "referalfriend@test.com";
        $user->name = "A referalfriend";
        $user->save();

        $rurl = ReferralUrl::create(['user_id' => $user->id,'url' =>'helloWorld11','region_id' => $germanRegion->id]);


        $profile = new \App\Models\Profile();
        $profile->user_id = $user->id;
        $profile->first_name = "parti";
        $profile->last_name = "boi";
        $profile->shipping_country = "DE";
        $profile->billing_country = "DE";
        $profile->customer_birthday = "1969-01-01 00:00:00";
        $profile->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="percent";
        $voucher->code="TEST_FIRST_SUBSCRIPTION_CH";

        $voucher->value=30.00;
        $voucher->active=1;
        $voucher->save();

        $regionRestrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'region'])->first();

        /*
        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'first_subscription'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();
        */

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="percent";
        $voucher->code="TEST_SUBSCRIPTION_CH";
        $voucher->value=10.00;
        $voucher->active=1;
        $voucher->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="product";
        $voucher->code="PRODUCT_TESTER";
        $voucher->value=0;
        $voucher->active=1;
        $voucher->save();
        $product = Product::withoutGlobalScope('active')->where(['product_identifier_letter' => 'B'])->first();
        VoucherProduct::create(['product_id' => $product->id, 'voucher_id' => $voucher->id]);

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="productPercent";
        $voucher->code="PRODUCT_PERCENT_TESTER";
        $voucher->value=10;
        $voucher->active=1;
        $voucher->save();
        $product = Product::withoutGlobalScope('active')->where(['product_identifier_letter' => 'B'])->first();
        VoucherProduct::create(['product_id' => $product->id, 'voucher_id' => $voucher->id]);

        /*
        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'subscription'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();
        */

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "1";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'region'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="TWENTY_AND_AHALF";
        $voucher->value=20.50;
        $voucher->active=1;
        $voucher->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="AGERESTRICTION";
        $voucher->value=20.50;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "19";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'age'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();


        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="FREQUENCYRESTRICTION";
        $voucher->value=20.50;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "1";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'frequency'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="UPU";
        $voucher->value=20.50;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "2";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'uses_per_user'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="MAXUSES";
        $voucher->value=20.50;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "5";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'max_total_uses'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();


        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="RICH";
        $voucher->value=200.00;
        $voucher->active=1;
        $voucher->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="USESCOMBO";
        $voucher->value=20.50;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "4";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'max_total_uses'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "2";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'uses_per_user'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="COUNTRY";
        $voucher->value=20.50;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "DE";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'country'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="CUSTOMERTYPE";
        $voucher->value=20.50;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "B2C";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'customer_type'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="ORDERTYPESINGLE";
        $voucher->value=20.50;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "SINGLE";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'order_type'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="ONORDERNUMBER";
        $voucher->value=20.50;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "1";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'on_order_number'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "3";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'on_order_number'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="ONORDERNUMBERNONFIRST";
        $voucher->value=10.00;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "3";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'on_order_number'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="FIRST3SUBSCRIPTIONS";
        $voucher->value=10.00;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "3";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'first_x_subscription_boxes'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="FIRST5ORDERS";
        $voucher->value=12.00;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "5";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'first_x_boxes'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="ONORDER1AND3AND5";
        $voucher->value=5.00;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "1";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'on_order_number'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "3";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'on_order_number'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "5";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'on_order_number'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="ONORDER2AND5";
        $voucher->value=5.00;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "2";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'on_order_number'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "5";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'on_order_number'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="ONSUBSCRIPTIONORDER2AND5";
        $voucher->value=5.00;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "2";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'on_subscription_order_number'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "5";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'on_subscription_order_number'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="ONSUBSCRIPTIONORDER1AND3";
        $voucher->value=5.00;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "1";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'on_subscription_order_number'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "3";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'on_subscription_order_number'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="NEED4PRODUCTS";
        $voucher->value=5.00;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "4";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'min_products_needed'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();


        $voucher = new \App\Models\Voucher();
        $voucher->value_type="amount";
        $voucher->code="NEED4PRODUCTS2PIRATES";
        $voucher->value=5.00;
        $voucher->active=1;
        $voucher->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "4";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'min_products_needed'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();

        $voucherRestriction = new \App\Models\VoucherRestriction();
        $voucherRestriction->voucher_id = $voucher->id;
        $voucherRestriction->restriction_payload = "P:2";
        $restrictionType = \App\Models\VoucherRestrictionType::where(['type' => 'min_products_needed'])->first();
        $voucherRestriction->restriction_type_id = $restrictionType->id;
        $voucherRestriction->save();
        //

    }
}