<?php

use Illuminate\Database\Seeder;

class ShippingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shippings')->insert([
            'country_id' => '1',
            'value' => '4.90',
            'tax' => '0.78',
            'tax_percentage' => '19',
        ]);

        DB::table('shippings')->insert([
            'country_id' => '2',
            'value' => '9.90',
            'tax' => '0.71',
            'tax_percentage' => '7.7',
        ]);

        DB::table('shippings')->insert([
            'country_id' => '3',
            'value' => '6.90',
            'tax' => '1.15',
            'tax_percentage' => '20',
        ]);
    }
}
