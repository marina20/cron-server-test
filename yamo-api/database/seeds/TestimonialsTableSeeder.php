<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/7/17
 * Time: 4:09 PM
 */

use Illuminate\Database\Seeder;

class TestimonialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('testimonials')->insert([
            'image' => 'oskar-testimonial-150x150.png',
            'description' => 'yamo ist die beste Alternative zu selbst gemachtem Babybrei',
            'customer_details' => '– Prof. Dr. med. Oskar Baenziger, Facharzt für Kinder- und Jugendmedizin',
            'created_at'=>DB::raw('now()'),
            'updated_at'=>DB::raw('now()')
        ]);

        DB::table('testimonials')->insert([
            'image' => 'sabrina-testimonial-150x150.png',
            'description' => 'Mit Kindern ist immer etwas los. Um die Zeit mit den Kleinen nutzen zu können, ist yamo perfekt: Einfach aus dem Kühlschrank nehmen und geniessen. yamo schmeckt wie Selbstgemachtes in Rekordzeit!',
            'customer_details' => '– Sabrina Corvini-Mohn, Mama von 2 Kindern, VR+GL-Assistentin und Politikerin',
            'created_at'=>DB::raw('now()'),
            'updated_at'=>DB::raw('now()')
        ]);

        DB::table('testimonials')->insert([
            'image' => 'cecile-testimonial-150x150.png',
            'description' => 'Endlich ein fixfertiger Babybrei, der so frisch und lecker ist, dass auch meine Kleine ihn liebt.',
            'customer_details' => '– Cécile Imfeld, Mama von 2 Kindern',
            'created_at'=>DB::raw('now()'),
            'updated_at'=>DB::raw('now()')
        ]);

        DB::table('testimonials')->insert([
            'image' => 'stephanie-testimonial-150x150.png',
            'description' => 'Die hochwertigen yamo-Breirezepte sind auf das Alter der Kleinkinder abgestimmt, so dass sie genau die Nährstoffe bekommen, die sie im jeweiligen Wachstumsstadium benötigen.',
            'customer_details' => '– Stefanie Bürge, Ernährungsberaterin BSc BFH',
            'created_at'=>DB::raw('now()'),
            'updated_at'=>DB::raw('now()')
        ]);

    }
}