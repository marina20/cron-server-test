<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/7/17
 * Time: 4:09 PM
 */

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('press')->insert([
            'link' => 'https://www.zentralplus.ch/de/news/wirtschaft/5507878/Beste-Gesch%C3%A4ftsidee-Babyfood-r%C3%A4umt-ab.htm',
            'image' => 'zentralplus-150x150.jpg',
            'meta_alt' => 'zentralplus',
            'meta_title' => 'zentralplus',
            'created_at'=>DB::raw('now()'),
            'updated_at'=>DB::raw('now()')
        ]);
        DB::table('press')->insert([
            'link' => 'http://www.luzernerzeitung.ch/start-ups+in+der+zentralschweiz./Mit-Hochdruck-zum-gesunden-Babybrei;art9642,917596',
            'image' => 'LuzernerZeitung-150x150.jpg',
            'meta_alt' => 'luzernerzeitung',
            'meta_title' => 'luzernerzeitung',
            'created_at'=>DB::raw('now()'),
            'updated_at'=>DB::raw('now()')
        ]);
        DB::table('press')->insert([
            'link' => 'http://www.watson.ch/Wirtschaft/Schweiz/987622821-10-Schweizer-Start-ups--die-dein-Leben-etwas-besser-machen-wollen',
            'image' => 'watson-150x150.jpg',
            'meta_alt' => 'watson',
            'meta_title' => 'watson',
            'created_at'=>DB::raw('now()'),
            'updated_at'=>DB::raw('now()')
        ]);
        DB::table('press')->insert([
            'link' => 'http://www.telezueri.ch/63-show-zueriinfo/15710-episode-drei-maenner-wollen-den-babybrei-markt-revolutionieren',
            'image' => 'telezuri-150x150.jpg',
            'meta_alt' => 'telezuri',
            'meta_title' => 'telezuri',
            'created_at'=>DB::raw('now()'),
            'updated_at'=>DB::raw('now()')
        ]);
        DB::table('press')->insert([
            'link' => 'http://www.20min.ch/',
            'image' => '20minuten-150x150.jpg',
            'meta_alt' => '20minuten',
            'meta_title' => '20minuten',
            'created_at'=>DB::raw('now()'),
            'updated_at'=>DB::raw('now()')
        ]);
    }
}