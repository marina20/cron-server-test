    <?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/7/17
 * Time: 4:09 PM
 */

    use App\Models\Product;
    use App\Models\ProductRegion;
    use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

//        $product = Product::create([
//            'name_key' => 'Yamo Latz',
//            'product_identifier_letter' => 'V',
//            'position' => '10000'
//        ]);
//        ProductRegion::create(['product_id' => $product->id,'price' => '15.00','region_id' => 1]);
//        $product = Product::create([
//            'name_key' => 'Kühltasche',
//            'position' => '10000'
//        ]);
//        ProductRegion::create(['product_id' => $product->id,'price' => '15.00','region_id' => 1]);
//        $product = Product::create([
//            'name_key' => 'Brei-Abo Monate 4+',
//            'position' => '8000'
//        ]);
//        ProductRegion::create(['product_id' => $product->id,'price' => '62.00','region_id' => 1]);
//        $product = Product::create([
//            'name_key' => 'Brei-Abo Monate 6+',
//            'position' => '8000'
//        ]);
//        ProductRegion::create(['product_id' => $product->id,'price' => '62.00','region_id' => 1]);
//        $product = Product::create([
//            'name_key' => 'Brei-Abo Monate 8+',
//            'position' => '8000'
//        ]);
//        ProductRegion::create(['product_id' => $product->id,'price' => '62.00','region_id' => 1]);
//        $product = Product::create([
//            'name_key' => 'Deine individuelle Box',
//            'position' => '8000'
//        ]);
//        ProductRegion::create(['product_id' => $product->id,'price' => '62.00','region_id' => 1]);
        /*DB::table('products')->insert([
            //'category' => 'tasche',
            'name_key' => 'Kühltasche',
            //'slug'=>'kuhltasche',
            //'image' => 'cooling-box.png',
            //'short_description' => '',
            //'characteristics' => '',
            //'price' => '15.00',
            //'tax_percentage' => '8.0',
            //'is_coupon_applicable' => 1,
            'position' => '10000',
            'created_at'=>DB::raw('now()'),
            'updated_at'=>DB::raw('now()')
        ]);

        DB::table('products')->insert([
            //'category' => 'subscription',
            'name_key' => 'Brei-Abo Monate 4+',
            //'slug'=>'brei-abo-monate-4',
            //'image' => 'yamobox.png',
            //'short_description' => '5 x Pirates of the Carrotean, 5 x Fresh Prince of Bel Pear, 6 x AppleCalypse Now',
            //'characteristics' => '{"characteristics":[{"category":"for_months","value":"4"},{"category":"ingredients","value":"5 x Pirates of the Carrotean"},{"category":"ingredients","value":"5 x Fresh Prince of Bel Pear"},{"category":"ingredients","value":"6 x AppleCalypse Now"}]}',
            //'price' => '62',
            //'tax_percentage' => '2.5',
            //'is_coupon_applicable' => 1,
            'position' => '8000',
            'created_at'=>DB::raw('now()'),
            'updated_at'=>DB::raw('now()')
        ]);
        DB::table('products')->insert([
            'name_key' => 'Brei-Abo Monate 6+',
            'category' => 'subscription',
            'name' => 'Brei-Abo Monate 6+',
            'slug'=>'brei-abo-monate-6',
            'image' => 'yamobox.png',
            'short_description' => '2 x Fresh Prince of Bel Pear, 2 x Fresh Prince of Bel Pear, 2 x AppleCalypse Now, 5 x David Zuchetta, 5 x Mango No 5',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"6"},{"category":"ingredients","value":"2 x Fresh Prince of Bel Pear"},{"category":"ingredients","value":"2 x Fresh Prince of Bel Pear"},{"category":"ingredients","value":"2 x AppleCalypse Now"},{"category":"ingredients","value":"5 x David Zuchetta"},{"category":"ingredients","value":"5 x Mango No 5"}]}',
            'price' => '62',
            'tax_percentage' => '2.5',
            'is_coupon_applicable' => 1,
            'position' => '8000',
            'created_at'=>DB::raw('now()'),
            'updated_at'=>DB::raw('now()')
        ]);
        DB::table('products')->insert([
            'name_key' => 'Brei-Abo Monate 8+',
            'category' => 'subscription',
            'name' => 'Brei-Abo Monate 8+',
            'slug'=>'brei-abo-monate-8',
            'image' => 'yamobox.png',
            'short_description' => '2 x Fresh Prince of Bel Pear, 3 x AppleCalypse Now, 2 x David Zuchetta, 3 x Mango No 5, 3 x Broccoly Balboa, 3 x Beetney Spears',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"8"},{"category":"ingredients","value":"2 x Fresh Prince of Bel Pear"},{"category":"ingredients","value":"3 x AppleCalypse Now"},{"category":"ingredients","value":"2 x David Zuchetta"},{"category":"ingredients","value":"3 x Mango No 5"},{"category":"ingredients","value":"3 x Broccoly Balboa"},{"category":"ingredients","value":"3 x Beetney Spears"}]}',
            'price' => '62',
            'tax_percentage' => '2.5',
            'is_coupon_applicable' => 1,
            'position' => '8000',
            'created_at'=>DB::raw('now()'),
            'updated_at'=>DB::raw('now()')
        ]);
        DB::table('products')->insert([
            'name_key' => 'Deine individuelle Box',
            'category' => 'subscription-custom-box',
            'name' => 'Deine individuelle Box',
            'slug'=>'subscription-custom-box',
            'image' => 'yamobox.png',
            'short_description' => 'Du erhälst einen Gutschein für 4 Boxen à jeweils 16 Breie.',
            'characteristics' => '',
            'price' => '62',
            'tax_percentage' => '2.5',
            'is_coupon_applicable' => 1,
            'position' => '8000',
            'created_at'=>DB::raw('now()'),
            'updated_at'=>DB::raw('now()')
        ]);*/
    }
}