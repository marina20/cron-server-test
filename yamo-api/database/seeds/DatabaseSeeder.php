<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(ProfilesTableSeeder::class);
        $this->call(PressTableSeeder::class);
        $this->call(TestimonialsTableSeeder::class);
//        $this->call(ProductsTableSeeder::class);
        $this->call(ShippingsTableSeeder::class);
        $this->call(VoucherTableSeeder::class);

        Model::reguard();
    }
}
