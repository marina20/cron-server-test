<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMfgRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mfg_request', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('mfg_request_type_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('order_id');
            $table->text('raw_request')->nullable();
            $table->text('raw_response')->nullable();
            $table->timestamps();
            $table->foreign('mfg_request_type_id')->references('id')->on('mfg_request_type');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('order_id')->references('id')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mfg_request');
    }
}
