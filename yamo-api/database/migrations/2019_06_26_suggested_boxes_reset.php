<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\BoxContent;
use App\Models\SuggestedBox;
use App\Models\Product;
use App\Models\Box;
use App\Models\Price;

class BoxesReset extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $art_pirates_of_the_carrotean = Product::withoutGlobalScope('active')->where(['slug' => 'pirates-of-the-carrotean'])->first();
        $art_fresh_prince_of_bel_pear = Product::withoutGlobalScope('active')->where(['slug' => 'fresh-prince-of-bel-pear'])->first();
        $art_applecalypse_now = Product::withoutGlobalScope('active')->where(['slug' => 'applecalypse-now'])->first();
        $art_david_zucchetta = Product::withoutGlobalScope('active')->where(['slug' =>  'david-zucchetta'])->first();
        $art_mango_nr_five = Product::withoutGlobalScope('active')->where(['slug' => 'mango-no-5'])->first();
        $art_broccoly_balboa = Product::withoutGlobalScope('active')->where(['slug' => 'broccoly-balboa'])->first();
        $art_peach_boys = Product::withoutGlobalScope('active')->where(['slug' => 'peach-boys'])->first();
        $art_anthony_pumpkins = Product::withoutGlobalScope('active')->where(['slug' => 'anthony-pumpkins'])->first();
        $art_inbanana_jones = Product::withoutGlobalScope('active')->where(['slug' => 'inbanana-jones'])->first();
        $art_avocado_di_caprio = Product::withoutGlobalScope('active')->where(['slug' => 'avocado-di-caprio'])->first();
        $art_katy_berry = Product::withoutGlobalScope('active')->where(['slug' => 'katy-berry'])->first();
        $art_sweet_home_albanana = Product::withoutGlobalScope('active')->where(['slug' => 'sweet-home-albanana'])->first();
        $art_beetney_spears = Product::withoutGlobalScope('active')->where(['slug' => 'beetney-spears'])->first();
        $art_cocohontas = Product::withoutGlobalScope('active')->where(['slug' => 'cocohontas'])->first();

        $country_ch = \App\Models\Country::where(['content_code' => 'de-ch'])->first();
        $country_de = \App\Models\Country::where(['content_code' => 'de-de'])->first();
        $country_at = \App\Models\Country::where(['content_code' => 'de-at'])->first();

        // Workaround for gitlab-test. Reason: Products are not created via migrations. But they exist on our locals, staging and production...
        if(!empty($art_pirates_of_the_carrotean)) {

            //$box1 = Box::create(['type' => 'Suggested','status' => 'active']);

            $sbox1 = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'breil', 'product_content' => 'fruchtig'])->first();
            $items = $sbox1->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $sbox1->box_id, 'quantity' => 6]);
            BoxContent::create(['product_id' => $art_applecalypse_now->id, 'box_id' => $sbox1->box_id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $sbox1->box_id, 'quantity' => 5]);

            //$box2 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox2 = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'breil', 'product_content' => 'gemusig'])->first();
            $items = $sbox2->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_pirates_of_the_carrotean->id, 'box_id' => $sbox2->box_id, 'quantity' => 8]);
            BoxContent::create(['product_id' => $art_anthony_pumpkins->id, 'box_id' => $sbox2->box_id, 'quantity' => 8]);

            //$box3 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'breil', 'product_content' => 'all'])->first();
            $items = $sbox->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_pirates_of_the_carrotean->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $sbox->box_id, 'quantity' => 4]);
            BoxContent::create(['product_id' => $art_applecalypse_now->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_anthony_pumpkins->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);

            //$box4 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'pouch', 'product_content' => 'fruchtig','default_box' => 1])->first();
            $items = $sbox->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $sbox->box_id, 'quantity' => 6]);
            BoxContent::create(['product_id' => $art_avocado_di_caprio->id, 'box_id' => $sbox->box_id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $sbox->box_id, 'quantity' => 5]);

            //$box5 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'pouch', 'product_content' => 'gemusig'])->first();
            $items = $sbox->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $sbox->box_id, 'quantity' => 6]);
            BoxContent::create(['product_id' => $art_avocado_di_caprio->id, 'box_id' => $sbox->box_id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $sbox->box_id, 'quantity' => 5]);

            //$box6 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'pouch', 'product_content' => 'all'])->first();
            $items = $sbox->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $sbox->box_id, 'quantity' => 6]);
            BoxContent::create(['product_id' => $art_avocado_di_caprio->id, 'box_id' => $sbox->box_id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $sbox->box_id, 'quantity' => 5]);

            //$box7 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'all', 'product_content' => 'fruchtig'])->first();
            $items = $sbox->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $sbox->box_id, 'quantity' => 6]);
            BoxContent::create(['product_id' => $art_applecalypse_now->id, 'box_id' => $sbox->box_id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $sbox->box_id, 'quantity' => 5]);

            //$box8 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'all', 'product_content' => 'gemusig'])->first();
            $items = $sbox->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_pirates_of_the_carrotean->id, 'box_id' => $sbox->box_id, 'quantity' => 8]);
            BoxContent::create(['product_id' => $art_anthony_pumpkins->id, 'box_id' => $sbox->box_id, 'quantity' => 8]);

            // $box9 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'all', 'product_content' => 'all'])->first();
            $items = $sbox->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_pirates_of_the_carrotean->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $sbox->box_id, 'quantity' => 4]);
            BoxContent::create(['product_id' => $art_applecalypse_now->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_anthony_pumpkins->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);

            // $box10 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'breil', 'product_content' => 'fruchtig'])->first();
            $items = $sbox->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $sbox->box_id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_applecalypse_now->id, 'box_id' => $sbox->box_id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $sbox->box_id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_mango_nr_five->id, 'box_id' => $sbox->box_id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_sweet_home_albanana->id, 'box_id' => $sbox->box_id, 'quantity' => 5]);

            // $box11 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'breil', 'product_content' => 'gemusig'])->first();
            $items = $sbox->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_pirates_of_the_carrotean->id, 'box_id' => $sbox->box_id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_anthony_pumpkins->id, 'box_id' => $sbox->box_id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_david_zucchetta->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_broccoly_balboa->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_beetney_spears->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_cocohontas->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);

            // $box12 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'breil', 'product_content' => 'all'])->first();
            $items = $sbox->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_mango_nr_five->id, 'box_id' => $sbox->box_id, 'quantity' => 4]);
            BoxContent::create(['product_id' => $art_sweet_home_albanana->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_broccoly_balboa->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_beetney_spears->id, 'box_id' =>$sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_cocohontas->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);

            //$box13 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'pouch', 'product_content' => 'fruchtig'])->first();
            $items = $sbox->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $sbox->box_id, 'quantity' => 6]);
            BoxContent::create(['product_id' => $art_avocado_di_caprio->id, 'box_id' => $sbox->box_id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $sbox->box_id, 'quantity' => 5]);

            //$box14 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'pouch', 'product_content' => 'gemusig'])->first();
            $items = $sbox->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $sbox->box_id, 'quantity' => 6]);
            BoxContent::create(['product_id' => $art_avocado_di_caprio->id, 'box_id' => $sbox->box_id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $sbox->box_id, 'quantity' => 5]);

            // $box15 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'pouch', 'product_content' => 'all'])->first();
            $items = $sbox->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $sbox->box_id, 'quantity' => 6]);
            BoxContent::create(['product_id' => $art_avocado_di_caprio->id, 'box_id' => $sbox->box_id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $sbox->box_id, 'quantity' => 5]);

            //$box16 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'all', 'product_content' => 'fruchtig'])->first();
            $items = $sbox->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $sbox->box_id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $sbox->box_id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_mango_nr_five->id, 'box_id' => $sbox->box_id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_sweet_home_albanana->id, 'box_id' => $sbox->box_id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_avocado_di_caprio->id, 'box_id' => $sbox->box_id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);


            // $box17 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'all', 'product_content' => 'gemusig'])->first();
            $items = $sbox->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_pirates_of_the_carrotean->id, 'box_id' => $sbox->box_id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_anthony_pumpkins->id, 'box_id' => $sbox->box_id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_david_zucchetta->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_broccoly_balboa->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_beetney_spears->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_cocohontas->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);


            // $box18 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'all', 'product_content' => 'all'])->first();
            $items = $sbox->box->items;
            foreach($items as $item){
                $item->delete();
            }

            BoxContent::create(['product_id' => $art_mango_nr_five->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_sweet_home_albanana->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);

            BoxContent::create(['product_id' => $art_beetney_spears->id, 'box_id' => $sbox->box_id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $sbox->box_id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_avocado_di_caprio->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $sbox->box_id, 'quantity' => 3]);

        }

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
