<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\AdyenNotification;

class FixAdyen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        set_time_limit(0);
        $notifications = AdyenNotification::get();
        foreach ($notifications as $notification) {
            $data = json_decode($notification->notification_body);
            if(empty($data))
                continue;
            $notification_item = reset($data->notificationItems);
            $notification_item = $notification_item->NotificationRequestItem;
            if ($notification_item->success) {
                $notification->success = $notification_item->success === 'true' ? true : false;
            }

            $notification->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
