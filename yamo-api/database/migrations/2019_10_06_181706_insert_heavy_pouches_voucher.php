<?php

use App\Models\Translation;
use App\Models\VatGroup;
use App\Services\CreateOrder\CreateOrder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Models\Order;
use \App\Models\Product;
use \App\Models\ProductIngredient;
use Illuminate\Database\Migrations\Migration;
use App\Traits\FormatMoney;
use \App\Models\Ingredient;
use \App\Models\Voucher;
use \App\Models\ProductMedia;
use \App\Models\ProductRegion;


class InsertHeavyPouchesVoucher extends Migration
{
    use FormatMoney;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Translation::create(['key-key' => 'ERROR.CUSTOMIZE_BOX_CANCELLATION_ALREADY_PAYED', 'de-de' => 'Deine letzte Box wurde bereits bezahlt. Du kannst den Inhalt nicht ändern', 'de-ch' => 'Deine letzte Box wurde bereits bezahlt. Du kannst den Inhalt nicht ändern', 'de-at' => 'Deine letzte Box wurde bereits bezahlt. Du kannst den Inhalt nicht ändern']);

        Voucher::create(['code' => 'SUBSCRIPTION_200_CH','value_type' => 'percent','voucher_type' => 'system','value' => 12.39,'active' => 1]);
        Voucher::create(['code' => 'SUBSCRIPTION_200_DE','value_type' => 'percent','voucher_type' => 'system','value' => 11.39,'active' => 1]);
        Voucher::create(['code' => 'SUBSCRIPTION_200_AT','value_type' => 'percent','voucher_type' => 'system','value' => 13.41,'active' => 1]);

        Voucher::create(['code' => 'FIRST_SUBSCRIPTION_200_CH','value_type' => 'percent','voucher_type' => 'system','value' => 30.09,'active' => 1]);
        Voucher::create(['code' => 'FIRST_SUBSCRIPTION_200_DE','value_type' => 'percent','voucher_type' => 'system','value' => 30.38,'active' => 1]);
        Voucher::create(['code' => 'FIRST_SUBSCRIPTION_200_AT','value_type' => 'percent','voucher_type' => 'system','value' => 30.49,'active' => 1]);
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
