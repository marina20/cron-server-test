<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAltTagsInTableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            DB::statement("update products set image_alt_tag_key = 'PIRATES_CARROTEAN',image_alt_tag_text = 'Yamo Brei Pirates of the Carrotean', updated_at = now() where slug = 'pirates-of-the-carrotean';");
            DB::statement("update products set image_alt_tag_key = 'FRESH_PRINCE',image_alt_tag_text = 'Yamo Brei Fresh Prince of Bel Pear', updated_at = now() where slug = 'fresh-prince-of-bel-pear';");
            DB::statement("update products set image_alt_tag_key = 'APPLECALYPSE',image_alt_tag_text = 'Yamo Brei Applecalypse Now', updated_at = now() where slug = 'applecalypse-now';");
            DB::statement("update products set image_alt_tag_key = 'DAVID_ZUCCHETTA',image_alt_tag_text = 'Yamo Brei David Zucchetta', updated_at = now() where slug = 'david-zucchetta';");
            DB::statement("update products set image_alt_tag_key = 'MANGO_NO_5',image_alt_tag_text = 'Yamo Brei Mango No. 5', updated_at = now() where slug = 'mango-no-5';");
            DB::statement("update products set image_alt_tag_key = 'BEETNEY_SPEARS',image_alt_tag_text = 'Yamo Brei Beetney Spears', updated_at = now() where slug = 'beetney-spears';");
            DB::statement("update products set image_alt_tag_key = 'BROCCOLY_BALBOA',image_alt_tag_text = 'Yamo Brei Broccoly Balboa', updated_at = now() where slug = 'broccoly-balboa';");
            DB::statement("update products set image_alt_tag_key = 'ANTHONY_PUMPKINS',image_alt_tag_text = 'Yamo Brei Anthony Pumpkins', updated_at = now() where slug = 'anthony-pumpkins';");
            DB::statement("update products set image_alt_tag_key = 'SWEET_HOME',image_alt_tag_text = 'Yamo Brei Sweet Home Albanana', updated_at = now() where slug = 'sweet-home-albanana';");
            DB::statement("update products set image_alt_tag_key = 'COCOHONTAS',image_alt_tag_text = 'Yamo Brei Cocohontas', updated_at = now() where slug = 'cocohontas';");
            DB::statement("update products set image_alt_tag_key = 'GUTSCHEIN_2BOX',image_alt_tag_text = 'Yamo Geschenkgutschein', updated_at = now() where slug = 'christmas-brei-box-2';");
            DB::statement("update products set image_alt_tag_key = 'GUTSCHEIN_3BOX',image_alt_tag_text = 'Yamo Geschenkgutschein', updated_at = now() where slug = 'christmas-brei-box-3';");
            DB::statement("update products set image_alt_tag_key = 'GUTSCHEIN_4BOX',image_alt_tag_text = 'Yamo Geschenkgutschein', updated_at = now() where slug = 'christmas-brei-box-4';");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            DB::statement("update products set image_alt_tag_key = null,image_alt_tag_text = null, updated_at = now() where slug = 'pirates-of-the-carrotean';");
            DB::statement("update products set image_alt_tag_key = null,image_alt_tag_text = null, updated_at = now() where slug = 'fresh-prince-of-bel-pear';");
            DB::statement("update products set image_alt_tag_key = null,image_alt_tag_text = null, updated_at = now() where slug = 'applecalypse-now';");
            DB::statement("update products set image_alt_tag_key = null,image_alt_tag_text = null, updated_at = now() where slug = 'david-zucchetta';");
            DB::statement("update products set image_alt_tag_key = null,image_alt_tag_text = null, updated_at = now() where slug = 'mango-no-5';");
            DB::statement("update products set image_alt_tag_key = null,image_alt_tag_text = null, updated_at = now() where slug = 'beetney-spears';");
            DB::statement("update products set image_alt_tag_key = null,image_alt_tag_text = null, updated_at = now() where slug = 'broccoly-balboa';");
            DB::statement("update products set image_alt_tag_key = null,image_alt_tag_text = null, updated_at = now() where slug = 'anthony-pumpkins';");
            DB::statement("update products set image_alt_tag_key = null,image_alt_tag_text = null, updated_at = now() where slug = 'sweet-home-albanana';");
            DB::statement("update products set image_alt_tag_key = null,image_alt_tag_text = null, updated_at = now() where slug = 'cocohontas';");
            DB::statement("update products set image_alt_tag_key = null,image_alt_tag_text = null, updated_at = now() where slug = 'christmas-brei-box-2';");
            DB::statement("update products set image_alt_tag_key = null,image_alt_tag_text = null, updated_at = now() where slug = 'christmas-brei-box-3';");
            DB::statement("update products set image_alt_tag_key = null,image_alt_tag_text = null, updated_at = now() where slug = 'christmas-brei-box-4';");
        });
    }
}
