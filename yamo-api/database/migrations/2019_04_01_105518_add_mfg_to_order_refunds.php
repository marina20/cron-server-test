<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMfgToOrderRefunds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        DB::statement('ALTER TABLE `order_refunds` MODIFY `adyen_notification_id` INTEGER UNSIGNED NULL;');
        DB::statement('ALTER TABLE `order_refunds` MODIFY `value` decimal(15,2);');
        Schema::table('order_refunds', function (Blueprint $table) {
            $table->unsignedInteger('mfg_request_id')->nullable()->after('adyen_notification_id');
            $table->foreign('mfg_request_id')->references('id')->on('mfg_request');
            $table->foreign('adyen_notification_id')->references('id')->on('adyen_notifications');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('order_refunds', function (Blueprint $table) {
            $table->dropForeign(['mfg_request_id']);
            $table->dropForeign(['adyen_notification_id']);
            $table->dropColumn('mfg_request_id');
        });
        DB::statement('ALTER TABLE `order_refunds` MODIFY `adyen_notification_id` INTEGER UNSIGNED NOT NULL;');
        DB::statement('ALTER TABLE `order_refunds` MODIFY `value` VARCHAR(255)');
        Schema::enableForeignKeyConstraints();
    }
}
