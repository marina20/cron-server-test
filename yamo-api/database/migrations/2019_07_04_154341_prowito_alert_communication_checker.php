<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProwitoAlertCommunicationChecker extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prowito_alert_communication_checker', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('api_called_at');
            $table->text('endpoint');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prowito_alert_communication_checker');
    }
}
