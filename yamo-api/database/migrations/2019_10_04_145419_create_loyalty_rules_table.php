<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoyaltyRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loyalty_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('box_number');
            $table->unsignedInteger('voucher_id');
            $table->string('description');
            $table->timestamps();
            $table->foreign('voucher_id')->references('id')->on('vouchers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('loyalty_rules');
        Schema::enableForeignKeyConstraints();
    }
}
