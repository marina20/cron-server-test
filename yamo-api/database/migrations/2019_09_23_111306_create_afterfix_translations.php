<?php

use App\Models\Translation;
use App\Models\VatGroup;
use App\Services\CreateOrder\CreateOrder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Models\Order;
use Illuminate\Database\Migrations\Migration;
use App\Traits\FormatMoney;


class CreateAfterfixTranslations extends Migration
{
    use FormatMoney;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Translation::create(['key-key' => 'MAIL.ORDER-DETAILS.REFERRALDISCOUNT','de-de'=>'Freunde einladen Rabatt','de-ch'=>'Freunde einladen Rabatt','de-at'=>'Freunde einladen Rabatt','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.ORDER-DETAILS.WALLETDISCOUNT','de-de'=>'Guthabenrabatt','de-ch'=>'Guthabenrabatt','de-at'=>'Guthabenrabatt','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'REFERRAL.CHECK.NOTNEW','de-de'=>'Um das Freundschaftssystem nutzen zu können, musst du ein neuer Kunde sein','de-ch'=>'Um das Freundschaftssystem nutzen zu können, musst du ein neuer Kunde sein','de-at'=>'Um das Freundschaftssystem nutzen zu können, musst du ein neuer Kunde sein','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        $orders = Order::where('created_at','<','2019-09-25 00:00:00')->where('created_at','>','2019-09-17 00:00:00')->where('is_subscription','!=','0')->whereNotNull('parent_id')->get();
        foreach($orders as $order){
            $vat = VatGroup::where(['country_id' => $order->country_id, 'category' => 'food'])->first();
            if($order->order_subtotal=='40.00'){
                $order->order_total = '36.00';
                $order->system_discount = '4.00';
                $order->system_discount_tax = CreateOrder::calculateVAT(4.00, $vat->value);
                $order->discount = $order->system_discount_tax;
                if($order->items->isNotEmpty()) {
                    foreach ($order->items as $item) {
                        $item->item_price = "2.25";
                        $discount = 0;
                        if(!empty($order->system_discount)) {
                            $singlePriceSystemDiscount = (float)$order->system_discount / 16;
                            $singlePriceSystemDiscountTax = (float)$order->system_discount_tax / 16;
                            $item->system_discount_tax = $item->qty*$singlePriceSystemDiscountTax;
                            $item->system_discount = $item->qty*$singlePriceSystemDiscount;
                            $discount = $item->system_discount;
                        }
                        $item->total = ((float)$item->item_price * $item->qty);
                        $item->total_tax = CreateOrder::calculateVAT($item->total, $vat->value);
                        $item->save();
                    }
                }
            } elseif ($order->order_subtotal=='68.00'){
                $order->order_total = '60.00';
                $order->system_discount = '8.00';
                $order->system_discount_tax = CreateOrder::calculateVAT(8.00, $vat->value);
                $order->discount = $order->system_discount_tax;
                if($order->items->isNotEmpty()) {
                    foreach ($order->items as $item) {
                        $item->item_price = "3.75";
                        $discount = 0;
                        if(!empty($order->system_discount)) {
                            $singlePriceSystemDiscount = (float)$order->system_discount / 16;
                            $singlePriceSystemDiscountTax = (float)$order->system_discount_tax / 16;
                            $item->system_discount_tax = $item->qty*$singlePriceSystemDiscountTax;
                            $item->system_discount = $item->qty*$singlePriceSystemDiscount;
                            $discount = $item->system_discount;

                        }
                        $item->total = ((float)$item->item_price * $item->qty);
                        $item->total_tax = CreateOrder::calculateVAT($item->total, $vat->value);
                        $item->save();
                    }
                }
            } elseif ($order->order_subtotal=='42.40'){
                $order->order_total = '39.20';
                $order->system_discount = '3.20';
                $order->system_discount_tax = CreateOrder::calculateVAT(3.20, $vat->value);
                $order->discount = $order->system_discount_tax;
                if($order->items->isNotEmpty()) {
                    foreach ($order->items as $item) {
                        $item->item_price = "2.45";
                        $discount = 0;
                        if(!empty($order->system_discount)) {
                            $singlePriceSystemDiscount = (float)$order->system_discount / 16;
                            $singlePriceSystemDiscountTax = (float)$order->system_discount_tax / 16;
                            $item->system_discount_tax = $item->qty*$singlePriceSystemDiscountTax;
                            $item->system_discount = $item->qty*$singlePriceSystemDiscount;
                            $discount = $item->system_discount;
                        }
                        $item->total = ((float)$item->item_price * $item->qty);
                        $item->total_tax = CreateOrder::calculateVAT($item->total, $vat->value);
                        $item->save();
                    }
                }
            }
            $order->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vat_groups');
    }
}
