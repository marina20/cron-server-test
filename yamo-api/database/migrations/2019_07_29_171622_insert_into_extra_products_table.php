<?php

use App\Models\Product;
use App\Models\ExtraProduct;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoExtraProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $productPiratesOfTheCarrotean = Product::withoutGlobalScope('active')->where(['slug' => 'pirates-of-the-carrotean'])->first();
        $productFreshPrinceOfBelPear = Product::withoutGlobalScope('active')->where(['slug' => 'fresh-prince-of-bel-pear'])->first();
        $productApplecalypseNow = Product::withoutGlobalScope('active')->where(['slug' => 'applecalypse-now'])->first();
        $productDavidZucchetta = Product::withoutGlobalScope('active')->where(['slug' =>  'david-zucchetta'])->first();
        $productMangoNrFive = Product::withoutGlobalScope('active')->where(['slug' => 'mango-no-5'])->first();
        $productBeetneySpears = Product::withoutGlobalScope('active')->where(['slug' => 'beetney-spears'])->first();
        $productBroccolyBalboa = Product::withoutGlobalScope('active')->where(['slug' => 'broccoly-balboa'])->first();
        $productAnthonyPumpkins = Product::withoutGlobalScope('active')->where(['slug' => 'anthony-pumpkins'])->first();
        $productSweetHomeAlbanana = Product::withoutGlobalScope('active')->where(['slug' => 'sweet-home-albanana'])->first();
        $productCocohontas = Product::withoutGlobalScope('active')->where(['slug' => 'cocohontas'])->first();
        $productPeachBoys = Product::withoutGlobalScope('active')->where(['slug' => 'peach-boys'])->first();
        $productInbananaJones = Product::withoutGlobalScope('active')->where(['slug' => 'inbanana-jones'])->first();
        $productAvocadoDiCaprio = Product::withoutGlobalScope('active')->where(['slug' => 'avocado-di-caprio'])->first();
        $productKatyBerry = Product::withoutGlobalScope('active')->where(['slug' => 'katy-berry'])->first();

        $productFlyer = Product::withoutGlobalScope('active')->where(['slug' => 'flyer'])->first();
        if(empty($productFlyer))
        {
            $prod = Product::create(['category'=>'flyer','product_identifier'=>'X', 'name'=>'Flyer', 'slug'=>'flyer', 'position'=>12000]);
            $productFlyer = Product::withoutGlobalScope('active')->where(['slug' => 'flyer'])->first();
        }

        if(!empty($productPiratesOfTheCarrotean))
            ExtraProduct::create(['product_id'=>$productPiratesOfTheCarrotean->id,'product_code'=>$productPiratesOfTheCarrotean->product_identifier]);
        
        if(!empty($productFreshPrinceOfBelPear))
            ExtraProduct::create(['product_id'=>$productFreshPrinceOfBelPear->id,'product_code'=>$productFreshPrinceOfBelPear->product_identifier]);
        
        if(!empty($productApplecalypseNow))
            ExtraProduct::create(['product_id'=>$productApplecalypseNow->id,'product_code'=>$productApplecalypseNow->product_identifier]);

        if(!empty($productDavidZucchetta))
            ExtraProduct::create(['product_id'=>$productDavidZucchetta->id,'product_code'=>$productDavidZucchetta->product_identifier]);

        if(!empty($productMangoNrFive))
            ExtraProduct::create(['product_id'=>$productMangoNrFive->id,'product_code'=>$productMangoNrFive->product_identifier]);

        if(!empty($productBeetneySpears))
            ExtraProduct::create(['product_id'=>$productBeetneySpears->id,'product_code'=>$productBeetneySpears->product_identifier]);

        if(!empty($productBroccolyBalboa))
            ExtraProduct::create(['product_id'=>$productBroccolyBalboa->id,'product_code'=>$productBroccolyBalboa->product_identifier]);

        if(!empty($productAnthonyPumpkins))
            ExtraProduct::create(['product_id'=>$productAnthonyPumpkins->id,'product_code'=>$productAnthonyPumpkins->product_identifier]);

        if(!empty($productSweetHomeAlbanana))
            ExtraProduct::create(['product_id'=>$productSweetHomeAlbanana->id,'product_code'=>$productSweetHomeAlbanana->product_identifier]);

        if(!empty($productCocohontas))
            ExtraProduct::create(['product_id'=>$productCocohontas->id,'product_code'=>$productCocohontas->product_identifier]);

        if(!empty($productPeachBoys))
            ExtraProduct::create(['product_id'=>$productPeachBoys->id,'product_code'=>$productPeachBoys->product_identifier]);

        if(!empty($productInbananaJones))
            ExtraProduct::create(['product_id'=>$productInbananaJones->id,'product_code'=>$productInbananaJones->product_identifier]);

        if(!empty($productAvocadoDiCaprio))
            ExtraProduct::create(['product_id'=>$productAvocadoDiCaprio->id,'product_code'=>$productAvocadoDiCaprio->product_identifier]);

        if(!empty($productKatyBerry))
            ExtraProduct::create(['product_id'=>$productKatyBerry->id,'product_code'=>$productKatyBerry->product_identifier]);

        if(!empty($productFlyer))
            ExtraProduct::create(['product_id'=>$productFlyer->id,'product_code'=>$productFlyer->product_identifier]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('extra_products')->truncate();
    }
}
