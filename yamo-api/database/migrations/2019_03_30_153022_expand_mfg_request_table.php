<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ExpandMfgRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mfg_request', function (Blueprint $table) {
            $table->text('external_reference')->nullable()->after('raw_response');
            $table->text('ip_address')->nullable()->after('external_reference');
            $table->text('gender')->nullable()->after('ip_address');
            $table->text('first_name')->nullable()->after('gender');
            $table->text('last_name')->nullable()->after('first_name');
            $table->text('street')->nullable()->after('last_name');
            $table->text('city')->nullable()->after('street');
            $table->text('zip')->nullable()->after('city');
            $table->text('country')->nullable()->after('zip');
            $table->text('language')->nullable()->after('country');
            $table->text('email')->nullable()->after('language');
            $table->text('birthdate')->nullable()->after('email');
            $table->text('merchant_id')->nullable()->after('birthdate');
            $table->text('filial_id')->nullable()->after('merchant_id');
            $table->text('terminal_id')->nullable()->after('filial_id');
            $table->decimal('amount', 15, 2)->nullable()->after('terminal_id');
            $table->text('currency_code')->nullable()->after('amount');
            $table->decimal('available_credit', 15, 2)->nullable()->after('currency_code');
            $table->decimal('maximal_credit', 15, 2)->nullable()->after('available_credit');
            $table->text('credit_refusal_reason')->nullable()->after('maximal_credit');
            $table->text('response_code')->nullable()->after('credit_refusal_reason');
            $table->text('request_date')->nullable()->after('response_code');
            $table->text('transaction_type')->nullable()->after('request_date');
            $table->text('currency')->nullable()->after('transaction_type');
            $table->text('response_date')->nullable()->after('currency');
            $table->text('authorization_code')->nullable()->after('response_date');
            $table->decimal('balance', 15, 2)->nullable()->after('authorization_code');
            $table->text('expiration_date')->nullable()->after('balance');
            $table->decimal('total', 15, 2)->nullable()->after('expiration_date');
            $table->decimal('purchase', 15, 2)->nullable()->after('total');
            $table->decimal('credit', 15, 2)->nullable()->after('purchase');
            $table->decimal('reversal', 15, 2)->nullable()->after('credit');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mfg_request', function (Blueprint $table) {
            $table->dropColumn('external_reference');
            $table->dropColumn('ip_address');
            $table->dropColumn('gender');
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('street');
            $table->dropColumn('city');
            $table->dropColumn('zip');
            $table->dropColumn('country');
            $table->dropColumn('language');
            $table->dropColumn('email');
            $table->dropColumn('birthdate');
            $table->dropColumn('merchant_id');
            $table->dropColumn('filial_id');
            $table->dropColumn('terminal_id');
            $table->dropColumn('amount');
            $table->dropColumn('currency_code');
            $table->dropColumn('available_credit');
            $table->dropColumn('maximal_credit');
            $table->dropColumn('credit_refusal_reason');
            $table->dropColumn('response_code');
            $table->dropColumn('request_date');
            $table->dropColumn('transaction_type');
            $table->dropColumn('currency');
            $table->dropColumn('response_date');
            $table->dropColumn('authorization_code');
            $table->dropColumn('balance');
            $table->dropColumn('expiration_date');
            $table->dropColumn('total');
            $table->dropColumn('purchase');
            $table->dropColumn('credit');
            $table->dropColumn('reversal');
        });
    }
}
