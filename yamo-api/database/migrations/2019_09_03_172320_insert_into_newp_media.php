<?php

use App\Models\Product;
use App\Models\Media;
use App\Models\Translation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoNewpMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $pirates = Product::withoutGlobalScope('active')->where('name_key',strtoupper('pirates-of-the-carrotean'))->first();
        $freshPrince = Product::withoutGlobalScope('active')->where('name_key',strtoupper('fresh-prince-of-bel-pear'))->first();
        $applecalypse = Product::withoutGlobalScope('active')->where('name_key',strtoupper('applecalypse-now'))->first();
        $david = Product::withoutGlobalScope('active')->where('name_key',strtoupper('david-zucchetta'))->first();
        $mango = Product::withoutGlobalScope('active')->where('name_key',strtoupper('mango-no-5'))->first();
        $beetney = Product::withoutGlobalScope('active')->where('name_key',strtoupper('beetney-spears'))->first();
        $broccoly = Product::withoutGlobalScope('active')->where('name_key',strtoupper('broccoly-balboa'))->first();
        $anthony = Product::withoutGlobalScope('active')->where('name_key',strtoupper('anthony-pumpkins'))->first();
        $sweetHome = Product::withoutGlobalScope('active')->where('name_key',strtoupper('sweet-home-albanana'))->first();
        $cocohontas = Product::withoutGlobalScope('active')->where('name_key',strtoupper('cocohontas'))->first();
        $inbanana = Product::withoutGlobalScope('active')->where('name_key',strtoupper('inbanana-jones'))->first();
        $avocado = Product::withoutGlobalScope('active')->where('name_key',strtoupper('avocado-di-caprio'))->first();
        $katy = Product::withoutGlobalScope('active')->where('name_key',strtoupper('katy-berry'))->first();
        $peach = Product::withoutGlobalScope('active')->where('name_key',strtoupper('peach-boys'))->first();
        $prince = Product::withoutGlobalScope('active')->where('name_key',strtoupper('prince-vanilliam'))->first();
        $nicki = Product::withoutGlobalScope('active')->where('name_key',strtoupper('nicki-spinaj'))->first();
        $quentin = Product::withoutGlobalScope('active')->where('name_key',strtoupper('quentin-carrotino'))->first();
        $cocofield = Product::withoutGlobalScope('active')->where('name_key',strtoupper('david-cocofield'))->first();
        $berry = Product::withoutGlobalScope('active')->where('name_key',strtoupper('berry-potter'))->first();
        $bib = Product::withoutGlobalScope('active')->where('name_key',strtoupper('bib'))->first();
        $coolingBag = Product::withoutGlobalScope('active')->where('name_key',strtoupper('kuhltasche'))->first();
        Media::insert([
            ['name_key'=>'IMG-' . $pirates->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $freshPrince->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $applecalypse->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $david->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $mango->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $beetney->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $broccoly->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $anthony->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $sweetHome->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $cocohontas->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $inbanana->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $avocado->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $katy->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $peach->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $prince->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $nicki->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $quentin->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $cocofield->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $berry->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $bib->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'IMG-' . $coolingBag->name_key, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')]
        ]);

        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$pirates->name_key.'.IMG', 'de-de'=>'pirates-of-the-carrotean.png', 'de-ch'=>'pirates-of-the-carrotean.png','de-at'=>'pirates-of-the-carrotean.png'],
            ['key-key'=>'MEDIA.IMG-'.$pirates->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Brei Pirates of the Carrotean', 'de-ch'=>'Yamo Brei Pirates of the Carrotean','de-at'=>'Yamo Brei Pirates of the Carrotean'],
            ['key-key'=>'MEDIA.IMG-'.$pirates->name_key.'.IMG-THUMB', 'de-de'=>'timg-pirates-of-the-carrotean.png', 'de-ch'=>'timg-pirates-of-the-carrotean.png','de-at'=>'timg-pirates-of-the-carrotean.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$freshPrince->name_key.'.IMG', 'de-de'=>'fresh-prince-of-bel-pear.png', 'de-ch'=>'fresh-prince-of-bel-pear.png','de-at'=>'fresh-prince-of-bel-pear.png'],
            ['key-key'=>'MEDIA.IMG-'.$freshPrince->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Brei Fresh Prince of Bel Pear', 'de-ch'=>'Yamo Brei Fresh Prince of Bel Pear','de-at'=>'Yamo Brei Fresh Prince of Bel Pear'],
            ['key-key'=>'MEDIA.IMG-'.$freshPrince->name_key.'.IMG-THUMB', 'de-de'=>'timg-fresh-prince-of-bel-pear.png', 'de-ch'=>'timg-fresh-prince-of-bel-pear.png','de-at'=>'timg-fresh-prince-of-bel-pear.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$applecalypse->name_key.'.IMG', 'de-de'=>'applecalypse-now.png', 'de-ch'=>'applecalypse-now.png','de-at'=>'applecalypse-now.png'],
            ['key-key'=>'MEDIA.IMG-'.$applecalypse->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Brei Applecalypse Now', 'de-ch'=>'Yamo Brei Applecalypse Now','de-at'=>'Yamo Brei Applecalypse Now'],
            ['key-key'=>'MEDIA.IMG-'.$applecalypse->name_key.'.IMG-THUMB', 'de-de'=>'timg-applecalypse-now.png', 'de-ch'=>'timg-applecalypse-now.png','de-at'=>'timg-applecalypse-now.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$david->name_key.'.IMG', 'de-de'=>'david-zucchetta.png', 'de-ch'=>'david-zucchetta.png','de-at'=>'david-zucchetta.png'],
            ['key-key'=>'MEDIA.IMG-'.$david->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Brei David Zucchetta', 'de-ch'=>'Yamo Brei David Zucchetta','de-at'=>'Yamo Brei David Zucchetta'],
            ['key-key'=>'MEDIA.IMG-'.$david->name_key.'.IMG-THUMB', 'de-de'=>'timg-david-zucchetta.png', 'de-ch'=>'timg-david-zucchetta.png','de-at'=>'timg-david-zucchetta.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$mango->name_key.'.IMG', 'de-de'=>'mango-no-5.png', 'de-ch'=>'mango-no-5.png','de-at'=>'mango-no-5.png'],
            ['key-key'=>'MEDIA.IMG-'.$mango->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Brei Mango No. 5', 'de-ch'=>'Yamo Brei Mango No. 5','de-at'=>'Yamo Brei Mango No. 5'],
            ['key-key'=>'MEDIA.IMG-'.$mango->name_key.'.IMG-THUMB', 'de-de'=>'timg-mango-no-5.png', 'de-ch'=>'timg-mango-no-5.png','de-at'=>'timg-mango-no-5.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$beetney->name_key.'.IMG', 'de-de'=>'beetney-spears.png', 'de-ch'=>'beetney-spears.png','de-at'=>'beetney-spears.png'],
            ['key-key'=>'MEDIA.IMG-'.$beetney->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Brei Beetney Spears', 'de-ch'=>'Yamo Brei Beetney Spears','de-at'=>'Yamo Brei Beetney Spears'],
            ['key-key'=>'MEDIA.IMG-'.$beetney->name_key.'.IMG-THUMB', 'de-de'=>'timg-beetney-spears.png', 'de-ch'=>'timg-beetney-spears.png','de-at'=>'timg-beetney-spears.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$broccoly->name_key.'.IMG', 'de-de'=>'broccoly-balboa.png', 'de-ch'=>'broccoly-balboa.png','de-at'=>'broccoly-balboa.png'],
            ['key-key'=>'MEDIA.IMG-'.$broccoly->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Brei Broccoly Balboa', 'de-ch'=>'Yamo Brei Broccoly Balboa','de-at'=>'Yamo Brei Broccoly Balboa'],
            ['key-key'=>'MEDIA.IMG-'.$broccoly->name_key.'.IMG-THUMB', 'de-de'=>'timg-broccoly-balboa.png', 'de-ch'=>'timg-broccoly-balboa.png','de-at'=>'timg-broccoly-balboa.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$anthony->name_key.'.IMG', 'de-de'=>'anthony-pumpkins.png', 'de-ch'=>'anthony-pumpkins.png','de-at'=>'anthony-pumpkins.png'],
            ['key-key'=>'MEDIA.IMG-'.$anthony->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Brei Anthony Pumpkins', 'de-ch'=>'Yamo Brei Anthony Pumpkins','de-at'=>'Yamo Brei Anthony Pumpkins'],
            ['key-key'=>'MEDIA.IMG-'.$anthony->name_key.'.IMG-THUMB', 'de-de'=>'timg-anthony-pumpkins.png', 'de-ch'=>'timg-anthony-pumpkins.png','de-at'=>'timg-anthony-pumpkins.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$sweetHome->name_key.'.IMG', 'de-de'=>'sweet-home-albanana.png', 'de-ch'=>'sweet-home-albanana.png','de-at'=>'sweet-home-albanana.png'],
            ['key-key'=>'MEDIA.IMG-'.$sweetHome->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Brei Sweet Home Albanana', 'de-ch'=>'Yamo Brei Sweet Home Albanana','de-at'=>'Yamo Brei Sweet Home Albanana'],
            ['key-key'=>'MEDIA.IMG-'.$sweetHome->name_key.'.IMG-THUMB', 'de-de'=>'timg-sweet-home-albanana.png', 'de-ch'=>'timg-sweet-home-albanana.png','de-at'=>'timg-sweet-home-albanana.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$cocohontas->name_key.'.IMG', 'de-de'=>'cocohontas.png', 'de-ch'=>'cocohontas.png','de-at'=>'cocohontas.png'],
            ['key-key'=>'MEDIA.IMG-'.$cocohontas->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Brei Cocohontas', 'de-ch'=>'Yamo Brei Cocohontas','de-at'=>'Yamo Brei Cocohontas'],
            ['key-key'=>'MEDIA.IMG-'.$cocohontas->name_key.'.IMG-THUMB', 'de-de'=>'timg-cocohontas.png', 'de-ch'=>'timg-cocohontas.png','de-at'=>'timg-cocohontas.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$inbanana->name_key.'.IMG', 'de-de'=>'inbanana-jones.png', 'de-ch'=>'inbanana-jones.png','de-at'=>'inbanana-jones.png'],
            ['key-key'=>'MEDIA.IMG-'.$inbanana->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Quetschie Inbanana Jones', 'de-ch'=>'Yamo Quetschie Inbanana Jones','de-at'=>'Yamo Quetschie Inbanana Jones'],
            ['key-key'=>'MEDIA.IMG-'.$inbanana->name_key.'.IMG-THUMB', 'de-de'=>'timg-inbanana-jones.png', 'de-ch'=>'timg-inbanana-jones.png','de-at'=>'timg-inbanana-jones.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$avocado->name_key.'.IMG', 'de-de'=>'avocado-di-caprio.png', 'de-ch'=>'avocado-di-caprio.png','de-at'=>'avocado-di-caprio.png'],
            ['key-key'=>'MEDIA.IMG-'.$avocado->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Quetschie Avocado di Caprio', 'de-ch'=>'Yamo Quetschie Avocado di Caprio','de-at'=>'Yamo Quetschie Avocado di Caprio'],
            ['key-key'=>'MEDIA.IMG-'.$avocado->name_key.'.IMG-THUMB', 'de-de'=>'timg-avocado-di-caprio.png', 'de-ch'=>'timg-avocado-di-caprio.png','de-at'=>'timg-avocado-di-caprio.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$katy->name_key.'.IMG', 'de-de'=>'katy-berry.png', 'de-ch'=>'katy-berry.png','de-at'=>'katy-berry.png'],
            ['key-key'=>'MEDIA.IMG-'.$katy->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Quetschie Katy Berry', 'de-ch'=>'Yamo Quetschie Katy Berry','de-at'=>'Yamo Quetschie Katy Berry'],
            ['key-key'=>'MEDIA.IMG-'.$katy->name_key.'.IMG-THUMB', 'de-de'=>'timg-katy-berry.png', 'de-ch'=>'timg-katy-berry.png','de-at'=>'timg-katy-berry.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$peach->name_key.'.IMG', 'de-de'=>'peach-boys.png', 'de-ch'=>'peach-boys.png','de-at'=>'peach-boys.png'],
            ['key-key'=>'MEDIA.IMG-'.$peach->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Brei Peach Boys', 'de-ch'=>'Yamo Brei Peach Boys','de-at'=>'Yamo Brei Peach Boys'],
            ['key-key'=>'MEDIA.IMG-'.$peach->name_key.'.IMG-THUMB', 'de-de'=>'timg-peach-boys.png', 'de-ch'=>'timg-peach-boys.png','de-at'=>'timg-peach-boys.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$prince->name_key.'.IMG', 'de-de'=>'prince-vanilliam.png', 'de-ch'=>'prince-vanilliam.png','de-at'=>'prince-vanilliam.png'],
            ['key-key'=>'MEDIA.IMG-'.$prince->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Quetschie Prince Vanilliam', 'de-ch'=>'Yamo Quetschie Prince Vanilliam','de-at'=>'Yamo Quetschie Prince Vanilliam'],
            ['key-key'=>'MEDIA.IMG-'.$prince->name_key.'.IMG-THUMB', 'de-de'=>'timg-prince-vanilliam.png', 'de-ch'=>'timg-prince-vanilliam.png','de-at'=>'timg-prince-vanilliam.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$nicki->name_key.'.IMG', 'de-de'=>'nicki-spinaj.png', 'de-ch'=>'nicki-spinaj.png','de-at'=>'nicki-spinaj.png'],
            ['key-key'=>'MEDIA.IMG-'.$nicki->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Quetschie Nicki Spinaj', 'de-ch'=>'Yamo Quetschie Nicki Spinaj','de-at'=>'Yamo Quetschie Nicki Spinaj'],
            ['key-key'=>'MEDIA.IMG-'.$nicki->name_key.'.IMG-THUMB', 'de-de'=>'timg-nicki-spinaj.png', 'de-ch'=>'timg-nicki-spinaj.png','de-at'=>'timg-nicki-spinaj.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$quentin->name_key.'.IMG', 'de-de'=>'quentin-carrotino.png', 'de-ch'=>'quentin-carrotino.png','de-at'=>'quentin-carrotino.png'],
            ['key-key'=>'MEDIA.IMG-'.$quentin->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Quetschie Quentin Carrotino', 'de-ch'=>'Yamo Quetschie Quentin Carrotino','de-at'=>'Yamo Quetschie Quentin Carrotino'],
            ['key-key'=>'MEDIA.IMG-'.$quentin->name_key.'.IMG-THUMB', 'de-de'=>'timg-quentin-carrotino.png', 'de-ch'=>'timg-quentin-carrotino.png','de-at'=>'timg-quentin-carrotino.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$cocofield->name_key.'.IMG', 'de-de'=>'david-cocofield.png', 'de-ch'=>'david-cocofield.png','de-at'=>'david-cocofield.png'],
            ['key-key'=>'MEDIA.IMG-'.$cocofield->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Quetschie David Cocofield', 'de-ch'=>'Yamo Quetschie David Cocofield','de-at'=>'Yamo Quetschie David Cocofield'],
            ['key-key'=>'MEDIA.IMG-'.$cocofield->name_key.'.IMG-THUMB', 'de-de'=>'timg-david-cocofield.png', 'de-ch'=>'timg-david-cocofield.png','de-at'=>'timg-david-cocofield.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$berry->name_key.'.IMG', 'de-de'=>'berry-potter.png', 'de-ch'=>'berry-potter.png','de-at'=>'berry-potter.png'],
            ['key-key'=>'MEDIA.IMG-'.$berry->name_key.'.ALT-TAGS', 'de-de'=>'Yamo Quetschie Berry Potter', 'de-ch'=>'Yamo Quetschie Berry Potter','de-at'=>'Yamo Quetschie Berry Potter'],
            ['key-key'=>'MEDIA.IMG-'.$berry->name_key.'.IMG-THUMB', 'de-de'=>'timg-berry-potter.png', 'de-ch'=>'timg-berry-potter.png','de-at'=>'timg-berry-potter.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$bib->name_key.'.IMG', 'de-de'=>'bib.png', 'de-ch'=>'bib.png','de-at'=>'bib.png'],
            ['key-key'=>'MEDIA.IMG-'.$bib->name_key.'.ALT-TAGS', 'de-de'=>'yamo latz', 'de-ch'=>'yamo latz','de-at'=>'yamo latz'],
            ['key-key'=>'MEDIA.IMG-'.$bib->name_key.'.IMG-THUMB', 'de-de'=>'bib.png', 'de-ch'=>'bib.png','de-at'=>'bib.png']
        ]);
        Translation::insert([
            ['key-key'=>'MEDIA.IMG-'.$coolingBag->name_key.'.IMG', 'de-de'=>'cooling-box.png', 'de-ch'=>'cooling-box.png','de-at'=>'cooling-box.png'],
            ['key-key'=>'MEDIA.IMG-'.$coolingBag->name_key.'.ALT-TAGS', 'de-de'=>'Kühltasche', 'de-ch'=>'Kühltasche','de-at'=>'Kühltasche'],
            ['key-key'=>'MEDIA.IMG-'.$coolingBag->name_key.'.IMG-THUMB', 'de-de'=>'cooling-box.png', 'de-ch'=>'cooling-box.png','de-at'=>'cooling-box.png']
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('media')->truncate();
        Schema::enableForeignKeyConstraints();
        Translation::where('key-key','LIKE','MEDIA.IMG-%')->delete();
    }
}
