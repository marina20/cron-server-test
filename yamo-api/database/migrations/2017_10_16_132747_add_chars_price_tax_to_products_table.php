<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCharsPriceTaxToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function($table) {
            $table->string('characteristics')->nullable()->after('full_description');
            $table->string('price')->nullable()->after('characteristics');
            $table->string('tax_percentage')->nullable()->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function($table) {
            $table->dropColumn('characteristics');
            $table->dropColumn('price');
            $table->dropColumn('tax_percentage');
        });
    }
}
