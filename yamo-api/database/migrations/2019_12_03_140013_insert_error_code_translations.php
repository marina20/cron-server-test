<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Translation;

class InsertErrorCodeTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Translation::create(['key-key' => 'ERROR.MY_ACCOUNT.ORDER_NOT_FOUND','de-de'=>'Die Bestellung wurde im System nicht gefunden.','de-ch'=>'Die Bestellung wurde im System nicht gefunden.','de-at'=>'Die Bestellung wurde im System nicht gefunden','en-uk'=>'The order does not exist.','nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'ERROR.MY_ACCOUNT.SUBSCRIPTION_ALREADY_CANCELLED','de-de'=>'Das Abo wurde bereits gekündigt.','de-ch'=>'Das Abo wurde bereits gekündigt.','de-at'=>'Das Abo wurde bereits gekündigt.','en-uk'=>'The subscription is already cancelled.','nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'ERROR.MY_ACCOUNT.BOX_CUSTOMIZATION_FAILED','de-de'=>'Die Änderung konnte nicht durchgeführt werden.','de-ch'=>'Die Änderung konnte nicht durchgeführt werden.','de-at'=>'Die Änderung konnte nicht durchgeführt werden.','en-uk'=>'Box customization failed.','nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Translation::where('key-key', 'ERROR.MY_ACCOUNT.ORDER_NOT_FOUND')->delete();
        Translation::where('key-key', 'ERROR.MY_ACCOUNT.SUBSCRIPTION_ALREADY_CANCELLED')->delete();
        Translation::where('key-key', 'ERROR.MY_ACCOUNT.BOX_CUSTOMIZATION_FAILED')->delete();
    }
}
