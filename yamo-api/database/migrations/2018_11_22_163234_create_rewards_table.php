<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rewards', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('advocate_id');
            $table->unsignedInteger('referral_id');
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('referral_timestamp');
            $table->boolean('used');
            $table->timestamps();
            $table->foreign('advocate_id')->references('id')->on('users');
            $table->foreign('referral_id')->references('id')->on('users');
            $table->foreign('order_id')->references('id')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rewards');
    }
}
