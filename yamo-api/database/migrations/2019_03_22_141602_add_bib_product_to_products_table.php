<?php

use App\Models\Product;
use App\Models\Price;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBibProductToProductsTable extends Migration
{
    const BIB_SLUG = 'bib';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $bib_id = DB::table('products')->insertGetId([
            'category'=>Product::PRODUCT_CATEGORY_PROMO_GIFT,
            'name'=>'yamo Latz',
            'short_name'=>'yamo Latz',
            'full_description'=>'BIB_DESCRIPTION',
            'slug'=>self::BIB_SLUG,
            'image'=>'bib.png',
            'price'=>'0.00',
            'tax_percentage'=>'0',
            'is_coupon_applicable'=>'0',
            'position'=>'9500',
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()')
        ]);

        DB::table('prices')->insert([
            [
                'country_id'=>1,
                'product_id'=>$bib_id,
                'value'=>'0.00',
                'tax_percentage'=>'0',
                'tax'=>'0.00',
                'shipping'=>'0.00',
                'shipping_tax'=>'0.00',
                'shipping_tax_percentage'=>'0',
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'country_id'=>2,
                'product_id'=>$bib_id,
                'value'=>'0.00',
                'tax_percentage'=>'0',
                'tax'=>'0.00',
                'shipping'=>'0.00',
                'shipping_tax'=>'0.00',
                'shipping_tax_percentage'=>'0',
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'country_id'=>3,
                'product_id'=>$bib_id,
                'value'=>'0.00',
                'tax_percentage'=>'0',
                'tax'=>'0.00',
                'shipping'=>'0.00',
                'shipping_tax'=>'0.00',
                'shipping_tax_percentage'=>'0',
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ]]);
    }


    /**
     * @throws Exception
     */
    public function down()
    {
        try {
            $product = Product::where('slug', self::BIB_SLUG)->first();
            if (!empty($product) && isset($product->id) && $product instanceof Product) {
                Price::where('product_id', $product->id)->delete();
                $product->delete();
            }
        }
        catch (Exception $exception)
        {
            app('sentry')->captureException($exception);
        }
    }
}
