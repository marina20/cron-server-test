<?php

use App\Models\Country;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Order;

class AddOriginalDeliveryDateField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
            $de = Country::where(['id' => 1])->first();
            if(empty($de)) {
                $de = Country::create(
                    ['id' => 1,
                        'name' => 'Germany',
                        'content_code' => 'de-de',
                        'language' => 'de-de',
                        'currency' => '€',
                        'iso_alpha_2' => 'DE',
                        'iso_alpha_3' => 'DEU',
                        'iso_numeric' => '276'
                    ]);
                $de->save();
            }
        $ch = Country::where(['id' => 2])->first();
        if(empty($ch)) {
            $ch = Country::create([
                'id' => 2,
                'name' => 'Switzerland',
                'content_code' => 'de-ch',
                'language' => 'de-ch',
                'currency' => 'CHF',
                'iso_alpha_2' => 'CH',
                'iso_alpha_3' => 'CHE',
                'iso_numeric' => '756'
            ]);
            $ch->save();
        }
        $at = Country::where(['id' => 3])->first();
        if(empty($at)) {
            $at = Country::firstOrCreate([
                'id' => 3,
                'name' => 'Austria',
                'content_code' => 'de-at',
                'language' => 'de-at',
                'currency' => '€',
                'iso_alpha_2' => 'AT',
                'iso_alpha_3' => 'AUT',
                'iso_numeric' => '040'
            ]);
            $at->save();
        }
        Schema::table('orders', function($table) {
            $table->datetime('original_delivery_date')->after('deposit_authorisation')->nullable();
        });
        Schema::table('delivery_dates', function($table) {
            $table->datetime('cut_off_date')->before('created_at');
        });
        $deliveryDates = \App\Models\DeliveryDate::all();
        foreach($deliveryDates as $deliveryDate){

                $ex = \Carbon\Carbon::parse($deliveryDate->export_date);
            if($deliveryDate->country_id!=$ch->id) {
                $ex->setTime(11, 30, 0, 0);
            }
                $deliveryDate->cut_off_date = $ex;
                $deliveryDate->save();

        }
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['delivery_date' => "2019-10-03 00:00:00",'country_id' => $de->id]);
        #$deliveryDate->delivery_date = "2019-10-03 00:00:00";
        $deliveryDate->alternate_date = "2019-10-08";
        #$deliveryDate->country_id = $de->id;
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['delivery_date' => "2019-10-04 00:00:00",'country_id' => $de->id]);
        #$deliveryDate->delivery_date = "2019-10-04";
        $deliveryDate->alternate_date = "2019-10-08";
        #$deliveryDate->country_id = $de->id;
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['delivery_date' => "2019-10-31 00:00:00",'country_id' => $de->id]);
        #$deliveryDate->delivery_date = "2019-10-31";
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2019-11-05";
        #$deliveryDate->country_id = $de->id;
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['delivery_date' => "2019-11-01 00:00:00",'country_id' => $de->id]);
        # $deliveryDate->delivery_date = "2019-11-01";
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2019-11-05";
        # $deliveryDate->country_id = $de->id;
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['delivery_date' => "2019-12-25 00:00:00",'country_id' => $de->id]);
        # $deliveryDate->delivery_date = "2019-12-25";
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2019-12-31";
        # $deliveryDate->country_id = $de->id;
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['delivery_date' => "2019-12-26 00:00:00",'country_id' => $de->id]);
        #$deliveryDate->delivery_date = "2019-12-26";
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2019-12-31";
        #$deliveryDate->country_id = $de->id;
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['delivery_date' => "2019-12-27 00:00:00",'country_id' => $de->id]);
        #$deliveryDate->delivery_date = "2019-12-27";
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2019-12-31";
        #$deliveryDate->country_id = $de->id;
        $deliveryDate->disabled = "1";
        $deliveryDate->save();


        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['delivery_date' => "2020-01-01 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2020-01-03";
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        /* $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['delivery_date' => "2020-01-02 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2020-01-03";
        $deliveryDate->disabled = "1";
        $deliveryDate->save(); */


        // CH

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['delivery_date' => "2019-11-01 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2019-11-05";
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['delivery_date' => "2019-12-27 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2019-12-31";
        $deliveryDate->disabled = "1";
        $deliveryDate->save();


        // AT

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2019-12-23 00:00:00",'delivery_date' => "2019-12-24 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2019-12-24";
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['delivery_date' => "2019-12-26 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2019-12-24";
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['delivery_date' => "2020-01-02 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2020-01-03";
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        # 2020

        # CH

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-01-01 00:00:00",'delivery_date' => "2020-01-03 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2020-01-07";
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-01-03 00:00:00",'delivery_date' => "2020-01-07 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-01-08 00:00:00",'delivery_date' => "2020-01-10 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-01-10 00:00:00",'delivery_date' => "2020-01-14 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-01-15 00:00:00",'delivery_date' => "2020-01-17 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-01-17 00:00:00",'delivery_date' => "2020-01-21 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-01-22 00:00:00",'delivery_date' => "2020-01-24 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-01-24 00:00:00",'delivery_date' => "2020-01-28 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-01-29 00:00:00",'delivery_date' => "2020-01-31 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-01-31 00:00:00",'delivery_date' => "2020-02-04 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-02-05 00:00:00",'delivery_date' => "2020-02-07 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-02-07 00:00:00",'delivery_date' => "2020-02-11 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-02-12 00:00:00",'delivery_date' => "2020-02-14 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-02-14 00:00:00",'delivery_date' => "2020-02-18 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-02-19 00:00:00",'delivery_date' => "2020-02-21 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-02-21 00:00:00",'delivery_date' => "2020-02-25 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-02-26 00:00:00",'delivery_date' => "2020-02-28 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-02-28 00:00:00",'delivery_date' => "2020-03-03 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-03-04 00:00:00",'delivery_date' => "2020-03-06 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-03-06 00:00:00",'delivery_date' => "2020-03-10 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-03-11 00:00:00",'delivery_date' => "2020-03-13 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-03-13 00:00:00",'delivery_date' => "2020-03-17 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-03-18 00:00:00",'delivery_date' => "2020-03-20 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-03-20 00:00:00",'delivery_date' => "2020-03-24 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-03-25 00:00:00",'delivery_date' => "2020-03-27 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::create(['export_date' => "2020-03-27 00:00:00",'delivery_date' => "2020-03-31 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-01 00:00:00",'delivery_date' => "2020-04-03 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-03 00:00:00",'delivery_date' => "2020-04-07 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-08 00:00:00",'delivery_date' => "2020-04-10 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2020-04-17";
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-10 00:00:00",'delivery_date' => "2020-04-14 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2020-04-17";
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-15 00:00:00",'delivery_date' => "2020-04-17 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-17 00:00:00",'delivery_date' => "2020-04-21 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-22 00:00:00",'delivery_date' => "2020-04-24 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-24 00:00:00",'delivery_date' => "2020-04-28 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-29 00:00:00",'delivery_date' => "2020-05-01 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2020-05-05";
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-01 00:00:00",'delivery_date' => "2020-05-05 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-06 00:00:00",'delivery_date' => "2020-05-08 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-08 00:00:00",'delivery_date' => "2020-05-12 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-13 00:00:00",'delivery_date' => "2020-05-15 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-15 00:00:00",'delivery_date' => "2020-05-19 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-20 00:00:00",'delivery_date' => "2020-05-22 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2020-05-26";
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-22 00:00:00",'delivery_date' => "2020-05-26 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-27 00:00:00",'delivery_date' => "2020-05-29 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-29 00:00:00",'delivery_date' => "2020-06-02 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2020-06-05";
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-03 00:00:00",'delivery_date' => "2020-06-05 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-05 00:00:00",'delivery_date' => "2020-06-09 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-10 00:00:00",'delivery_date' => "2020-06-12 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2020-06-16";
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-12 00:00:00",'delivery_date' => "2020-06-16 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-17 00:00:00",'delivery_date' => "2020-06-19 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-19 00:00:00",'delivery_date' => "2020-06-23 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-24 00:00:00",'delivery_date' => "2020-06-26 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-26 00:00:00",'delivery_date' => "2020-06-30 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-01 00:00:00",'delivery_date' => "2020-07-03 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-03 00:00:00",'delivery_date' => "2020-07-07 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-08 00:00:00",'delivery_date' => "2020-07-10 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-10 00:00:00",'delivery_date' => "2020-07-14 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-15 00:00:00",'delivery_date' => "2020-07-17 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-17 00:00:00",'delivery_date' => "2020-07-21 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-22 00:00:00",'delivery_date' => "2020-07-24 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-24 00:00:00",'delivery_date' => "2020-07-28 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-29 00:00:00",'delivery_date' => "2020-07-31 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-31 00:00:00",'delivery_date' => "2020-08-04 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-05 00:00:00",'delivery_date' => "2020-08-07 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-07 00:00:00",'delivery_date' => "2020-08-11 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-12 00:00:00",'delivery_date' => "2020-08-14 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-14 00:00:00",'delivery_date' => "2020-08-18 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-19 00:00:00",'delivery_date' => "2020-08-21 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-21 00:00:00",'delivery_date' => "2020-08-25 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-26 00:00:00",'delivery_date' => "2020-08-28 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-28 00:00:00",'delivery_date' => "2020-09-01 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-02 00:00:00",'delivery_date' => "2020-09-04 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-04 00:00:00",'delivery_date' => "2020-09-08 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-09 00:00:00",'delivery_date' => "2020-09-11 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-11 00:00:00",'delivery_date' => "2020-09-15 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-16 00:00:00",'delivery_date' => "2020-09-18 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-18 00:00:00",'delivery_date' => "2020-09-22 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-23 00:00:00",'delivery_date' => "2020-09-25 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-25 00:00:00",'delivery_date' => "2020-09-29 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-30 00:00:00",'delivery_date' => "2020-10-02 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-02 00:00:00",'delivery_date' => "2020-10-06 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-07 00:00:00",'delivery_date' => "2020-10-09 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-09 00:00:00",'delivery_date' => "2020-10-13 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();


        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-14 00:00:00",'delivery_date' => "2020-10-16 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-16 00:00:00",'delivery_date' => "2020-10-20 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-21 00:00:00",'delivery_date' => "2020-10-23 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-23 00:00:00",'delivery_date' => "2020-10-27 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-28 00:00:00",'delivery_date' => "2020-10-30 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-30 00:00:00",'delivery_date' => "2020-11-03 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-04 00:00:00",'delivery_date' => "2020-11-06 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-06 00:00:00",'delivery_date' => "2020-11-10 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-11 00:00:00",'delivery_date' => "2020-11-13 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-13 00:00:00",'delivery_date' => "2020-11-17 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-18 00:00:00",'delivery_date' => "2020-11-20 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-20 00:00:00",'delivery_date' => "2020-11-24 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-25 00:00:00",'delivery_date' => "2020-11-27 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-27 00:00:00",'delivery_date' => "2020-12-01 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-02 00:00:00",'delivery_date' => "2020-12-04 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-04 00:00:00",'delivery_date' => "2020-12-08 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->alternate_date = "2020-12-11";
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-09 00:00:00",'delivery_date' => "2020-12-11 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-11 00:00:00",'delivery_date' => "2020-12-15 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-16 00:00:00",'delivery_date' => "2020-12-18 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-18 00:00:00",'delivery_date' => "2020-12-22 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-23 00:00:00",'delivery_date' => "2020-12-25 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-30 00:00:00",'delivery_date' => "2021-01-01 00:00:00",'country_id' => $ch->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2021-01-05";
        $deliveryDate->save();

        # AT

        /*
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-01 00:00:00",'delivery_date' => "2020-01-02 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2020-01-03";
        $deliveryDate->save();
        */
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-02 00:00:00",'delivery_date' => "2020-01-03 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-08 00:00:00",'delivery_date' => "2020-01-09 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-15 00:00:00",'delivery_date' => "2020-01-16 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-22 00:00:00",'delivery_date' => "2020-01-23 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-29 00:00:00",'delivery_date' => "2020-01-30 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-05 00:00:00",'delivery_date' => "2020-02-06 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-12 00:00:00",'delivery_date' => "2020-02-13 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-19 00:00:00",'delivery_date' => "2020-02-20 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-26 00:00:00",'delivery_date' => "2020-02-27 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-04 00:00:00",'delivery_date' => "2020-03-05 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-11 00:00:00",'delivery_date' => "2020-03-12 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-18 00:00:00",'delivery_date' => "2020-03-19 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-25 00:00:00",'delivery_date' => "2020-03-26 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-01 00:00:00",'delivery_date' => "2020-04-02 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-08 00:00:00",'delivery_date' => "2020-04-09 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-15 00:00:00",'delivery_date' => "2020-04-16 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-22 00:00:00",'delivery_date' => "2020-04-23 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-29 00:00:00",'delivery_date' => "2020-04-30 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-06 00:00:00",'delivery_date' => "2020-05-07 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-13 00:00:00",'delivery_date' => "2020-05-14 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-19 00:00:00",'delivery_date' => "2020-05-20 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-20 00:00:00",'delivery_date' => "2020-05-21 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2020-05-20";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-27 00:00:00",'delivery_date' => "2020-05-28 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-03 00:00:00",'delivery_date' => "2020-06-04 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-09 00:00:00",'delivery_date' => "2020-06-10 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-10 00:00:00",'delivery_date' => "2020-06-11 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2020-06-10";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-17 00:00:00",'delivery_date' => "2020-06-18 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-24 00:00:00",'delivery_date' => "2020-06-25 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-01 00:00:00",'delivery_date' => "2020-07-02 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-08 00:00:00",'delivery_date' => "2020-07-09 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-15 00:00:00",'delivery_date' => "2020-07-16 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-22 00:00:00",'delivery_date' => "2020-07-23 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-29 00:00:00",'delivery_date' => "2020-07-30 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-05 00:00:00",'delivery_date' => "2020-08-06 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-12 00:00:00",'delivery_date' => "2020-08-13 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-19 00:00:00",'delivery_date' => "2020-08-20 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-26 00:00:00",'delivery_date' => "2020-08-27 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-02 00:00:00",'delivery_date' => "2020-09-03 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-09 00:00:00",'delivery_date' => "2020-09-10 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-16 00:00:00",'delivery_date' => "2020-09-17 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-23 00:00:00",'delivery_date' => "2020-09-24 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-30 00:00:00",'delivery_date' => "2020-10-01 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-07 00:00:00",'delivery_date' => "2020-10-08 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-14 00:00:00",'delivery_date' => "2020-10-15 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-21 00:00:00",'delivery_date' => "2020-10-22 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-28 00:00:00",'delivery_date' => "2020-10-29 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-04 00:00:00",'delivery_date' => "2020-11-05 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-11 00:00:00",'delivery_date' => "2020-11-12 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-18 00:00:00",'delivery_date' => "2020-11-19 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-25 00:00:00",'delivery_date' => "2020-11-26 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();


        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-02 00:00:00",'delivery_date' => "2020-12-03 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-09 00:00:00",'delivery_date' => "2020-12-10 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-16 00:00:00",'delivery_date' => "2020-12-17 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-22 00:00:00",'delivery_date' => "2020-12-23 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-23 00:00:00",'delivery_date' => "2020-12-24 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2020-12-23";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-29 00:00:00",'delivery_date' => "2020-12-30 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-30 00:00:00",'delivery_date' => "2020-12-31 00:00:00",'country_id' => $at->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2020-12-30";
        $deliveryDate->save();

         # DE!!
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-01 00:00:00",'delivery_date' => "2020-01-02 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2020-01-03";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-02 00:00:00",'delivery_date' => "2020-01-03 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-06 00:00:00",'delivery_date' => "2020-01-07 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2020-01-08";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-07 00:00:00",'delivery_date' => "2020-01-08 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-08 00:00:00",'delivery_date' => "2020-01-09 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-09 00:00:00",'delivery_date' => "2020-01-10 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-13 00:00:00",'delivery_date' => "2020-01-14 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-14 00:00:00",'delivery_date' => "2020-01-15 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-15 00:00:00",'delivery_date' => "2020-01-16 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-16 00:00:00",'delivery_date' => "2020-01-17 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-20 00:00:00",'delivery_date' => "2020-01-21 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-21 00:00:00",'delivery_date' => "2020-01-22 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-22 00:00:00",'delivery_date' => "2020-01-23 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-23 00:00:00",'delivery_date' => "2020-01-24 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-27 00:00:00",'delivery_date' => "2020-01-28 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-28 00:00:00",'delivery_date' => "2020-01-29 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-29 00:00:00",'delivery_date' => "2020-01-30 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-01-30 00:00:00",'delivery_date' => "2020-01-31 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-03 00:00:00",'delivery_date' => "2020-02-04 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-04 00:00:00",'delivery_date' => "2020-02-05 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-05 00:00:00",'delivery_date' => "2020-02-06 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-06 00:00:00",'delivery_date' => "2020-02-07 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-10 00:00:00",'delivery_date' => "2020-02-11 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-11 00:00:00",'delivery_date' => "2020-02-12 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-12 00:00:00",'delivery_date' => "2020-02-13 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-13 00:00:00",'delivery_date' => "2020-02-14 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-17 00:00:00",'delivery_date' => "2020-02-18 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-18 00:00:00",'delivery_date' => "2020-02-19 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-19 00:00:00",'delivery_date' => "2020-02-20 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-20 00:00:00",'delivery_date' => "2020-02-21 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-24 00:00:00",'delivery_date' => "2020-02-25 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-25 00:00:00",'delivery_date' => "2020-02-26 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-26 00:00:00",'delivery_date' => "2020-02-27 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-02-27 00:00:00",'delivery_date' => "2020-02-28 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-02 00:00:00",'delivery_date' => "2020-03-03 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-03 00:00:00",'delivery_date' => "2020-03-04 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-04 00:00:00",'delivery_date' => "2020-03-05 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-05 00:00:00",'delivery_date' => "2020-03-06 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-09 00:00:00",'delivery_date' => "2020-03-10 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-10 00:00:00",'delivery_date' => "2020-03-11 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-11 00:00:00",'delivery_date' => "2020-03-12 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-12 00:00:00",'delivery_date' => "2020-03-13 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();


        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-16 00:00:00",'delivery_date' => "2020-03-17 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-17 00:00:00",'delivery_date' => "2020-03-18 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-18 00:00:00",'delivery_date' => "2020-03-19 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-19 00:00:00",'delivery_date' => "2020-03-20 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-23 00:00:00",'delivery_date' => "2020-03-24 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-24 00:00:00",'delivery_date' => "2020-03-25 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-25 00:00:00",'delivery_date' => "2020-03-26 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-26 00:00:00",'delivery_date' => "2020-03-27 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-30 00:00:00",'delivery_date' => "2020-03-31 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-03-31 00:00:00",'delivery_date' => "2020-04-01 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-01 00:00:00",'delivery_date' => "2020-04-02 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-02 00:00:00",'delivery_date' => "2020-04-03 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-06 00:00:00",'delivery_date' => "2020-04-07 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-07 00:00:00",'delivery_date' => "2020-04-08 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-08 00:00:00",'delivery_date' => "2020-04-09 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-09 00:00:00",'delivery_date' => "2020-04-10 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2020-04-15";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-13 00:00:00",'delivery_date' => "2020-04-14 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2020-04-15";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-14 00:00:00",'delivery_date' => "2020-04-15 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-15 00:00:00",'delivery_date' => "2020-04-16 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-16 00:00:00",'delivery_date' => "2020-04-17 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-20 00:00:00",'delivery_date' => "2020-04-21 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-21 00:00:00",'delivery_date' => "2020-04-22 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-22 00:00:00",'delivery_date' => "2020-04-23 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-23 00:00:00",'delivery_date' => "2020-04-24 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-27 00:00:00",'delivery_date' => "2020-04-28 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-28 00:00:00",'delivery_date' => "2020-04-29 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-29 00:00:00",'delivery_date' => "2020-04-30 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-04-30 00:00:00",'delivery_date' => "2020-05-01 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2020-05-05";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-04 00:00:00",'delivery_date' => "2020-05-05 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-05 00:00:00",'delivery_date' => "2020-05-06 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-06 00:00:00",'delivery_date' => "2020-05-07 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-07 00:00:00",'delivery_date' => "2020-05-08 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-11 00:00:00",'delivery_date' => "2020-05-12 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-12 00:00:00",'delivery_date' => "2020-05-13 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-13 00:00:00",'delivery_date' => "2020-05-14 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-14 00:00:00",'delivery_date' => "2020-05-15 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();


        # Afterwork, double-check?


        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-18 00:00:00",'delivery_date' => "2020-05-19 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-19 00:00:00",'delivery_date' => "2020-05-20 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-20 00:00:00",'delivery_date' => "2020-05-21 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2020-05-26";
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-21 00:00:00",'delivery_date' => "2020-05-22 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2020-05-26";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-25 00:00:00",'delivery_date' => "2020-05-26 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-26 00:00:00",'delivery_date' => "2020-05-27 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-27 00:00:00",'delivery_date' => "2020-05-28 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-05-28 00:00:00",'delivery_date' => "2020-05-29 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-01 00:00:00",'delivery_date' => "2020-06-02 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2020-06-03";
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-02 00:00:00",'delivery_date' => "2020-06-03 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-03 00:00:00",'delivery_date' => "2020-06-04 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-04 00:00:00",'delivery_date' => "2020-06-05 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-08 00:00:00",'delivery_date' => "2020-06-09 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-09 00:00:00",'delivery_date' => "2020-06-10 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-10 00:00:00",'delivery_date' => "2020-06-11 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2020-06-16";
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-11 00:00:00",'delivery_date' => "2020-06-12 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2020-06-16";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-15 00:00:00",'delivery_date' => "2020-06-16 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-16 00:00:00",'delivery_date' => "2020-06-17 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-17 00:00:00",'delivery_date' => "2020-06-18 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-18 00:00:00",'delivery_date' => "2020-06-19 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-22 00:00:00",'delivery_date' => "2020-06-23 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-23 00:00:00",'delivery_date' => "2020-06-24 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-24 00:00:00",'delivery_date' => "2020-06-25 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-25 00:00:00",'delivery_date' => "2020-06-26 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-29 00:00:00",'delivery_date' => "2020-06-30 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-06-30 00:00:00",'delivery_date' => "2020-07-01 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-01 00:00:00",'delivery_date' => "2020-07-02 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-02 00:00:00",'delivery_date' => "2020-07-03 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-06 00:00:00",'delivery_date' => "2020-07-07 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-07 00:00:00",'delivery_date' => "2020-07-08 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-08 00:00:00",'delivery_date' => "2020-07-09 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-09 00:00:00",'delivery_date' => "2020-07-10 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-13 00:00:00",'delivery_date' => "2020-07-14 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-14 00:00:00",'delivery_date' => "2020-07-15 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-15 00:00:00",'delivery_date' => "2020-07-16 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-16 00:00:00",'delivery_date' => "2020-07-17 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-20 00:00:00",'delivery_date' => "2020-07-21 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-21 00:00:00",'delivery_date' => "2020-07-22 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-22 00:00:00",'delivery_date' => "2020-07-23 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-23 00:00:00",'delivery_date' => "2020-07-24 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-27 00:00:00",'delivery_date' => "2020-07-28 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-28 00:00:00",'delivery_date' => "2020-07-29 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-29 00:00:00",'delivery_date' => "2020-07-30 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-07-30 00:00:00",'delivery_date' => "2020-07-31 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-03 00:00:00",'delivery_date' => "2020-08-04 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-04 00:00:00",'delivery_date' => "2020-08-05 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-05 00:00:00",'delivery_date' => "2020-08-06 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-06 00:00:00",'delivery_date' => "2020-08-07 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-10 00:00:00",'delivery_date' => "2020-08-11 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-11 00:00:00",'delivery_date' => "2020-08-12 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-12 00:00:00",'delivery_date' => "2020-08-13 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-13 00:00:00",'delivery_date' => "2020-08-14 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();


        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-17 00:00:00",'delivery_date' => "2020-08-18 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-18 00:00:00",'delivery_date' => "2020-08-19 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-19 00:00:00",'delivery_date' => "2020-08-20 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-20 00:00:00",'delivery_date' => "2020-08-21 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();


        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-24 00:00:00",'delivery_date' => "2020-08-25 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-25 00:00:00",'delivery_date' => "2020-08-26 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-26 00:00:00",'delivery_date' => "2020-08-27 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-27 00:00:00",'delivery_date' => "2020-08-28 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-08-31 00:00:00",'delivery_date' => "2020-09-01 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-01 00:00:00",'delivery_date' => "2020-09-02 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-02 00:00:00",'delivery_date' => "2020-09-03 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-03 00:00:00",'delivery_date' => "2020-09-04 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-07 00:00:00",'delivery_date' => "2020-09-08 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-08 00:00:00",'delivery_date' => "2020-09-09 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-09 00:00:00",'delivery_date' => "2020-09-10 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-10 00:00:00",'delivery_date' => "2020-09-11 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-14 00:00:00",'delivery_date' => "2020-09-15 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-15 00:00:00",'delivery_date' => "2020-09-16 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-16 00:00:00",'delivery_date' => "2020-09-17 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-17 00:00:00",'delivery_date' => "2020-09-18 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-21 00:00:00",'delivery_date' => "2020-09-22 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-22 00:00:00",'delivery_date' => "2020-09-23 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-23 00:00:00",'delivery_date' => "2020-09-24 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-24 00:00:00",'delivery_date' => "2020-09-25 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-28 00:00:00",'delivery_date' => "2020-09-29 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-29 00:00:00",'delivery_date' => "2020-09-30 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-09-30 00:00:00",'delivery_date' => "2020-10-01 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-01 00:00:00",'delivery_date' => "2020-10-02 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-05 00:00:00",'delivery_date' => "2020-10-06 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-06 00:00:00",'delivery_date' => "2020-10-07 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-07 00:00:00",'delivery_date' => "2020-10-08 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-08 00:00:00",'delivery_date' => "2020-10-09 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-12 00:00:00",'delivery_date' => "2020-10-13 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-13 00:00:00",'delivery_date' => "2020-10-14 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-14 00:00:00",'delivery_date' => "2020-10-15 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-15 00:00:00",'delivery_date' => "2020-10-16 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-19 00:00:00",'delivery_date' => "2020-10-20 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-20 00:00:00",'delivery_date' => "2020-10-21 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-21 00:00:00",'delivery_date' => "2020-10-22 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-22 00:00:00",'delivery_date' => "2020-10-23 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-26 00:00:00",'delivery_date' => "2020-10-27 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-27 00:00:00",'delivery_date' => "2020-10-28 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-28 00:00:00",'delivery_date' => "2020-10-29 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-10-29 00:00:00",'delivery_date' => "2020-10-30 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-02 00:00:00",'delivery_date' => "2020-11-03 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-03 00:00:00",'delivery_date' => "2020-11-04 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-04 00:00:00",'delivery_date' => "2020-11-05 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-05 00:00:00",'delivery_date' => "2020-11-06 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-09 00:00:00",'delivery_date' => "2020-11-10 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-10 00:00:00",'delivery_date' => "2020-11-11 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-11 00:00:00",'delivery_date' => "2020-11-12 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-12 00:00:00",'delivery_date' => "2020-11-13 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-16 00:00:00",'delivery_date' => "2020-11-17 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-17 00:00:00",'delivery_date' => "2020-11-18 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-18 00:00:00",'delivery_date' => "2020-11-19 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-19 00:00:00",'delivery_date' => "2020-11-20 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-23 00:00:00",'delivery_date' => "2020-11-24 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-24 00:00:00",'delivery_date' => "2020-11-25 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-25 00:00:00",'delivery_date' => "2020-11-26 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-26 00:00:00",'delivery_date' => "2020-11-27 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-11-30 00:00:00",'delivery_date' => "2020-12-01 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-01 00:00:00",'delivery_date' => "2020-12-02 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-02 00:00:00",'delivery_date' => "2020-12-03 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-03 00:00:00",'delivery_date' => "2020-12-04 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-07 00:00:00",'delivery_date' => "2020-12-08 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-08 00:00:00",'delivery_date' => "2020-12-09 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-09 00:00:00",'delivery_date' => "2020-12-10 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-10 00:00:00",'delivery_date' => "2020-12-11 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-14 00:00:00",'delivery_date' => "2020-12-15 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-15 00:00:00",'delivery_date' => "2020-12-16 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-16 00:00:00",'delivery_date' => "2020-12-17 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-17 00:00:00",'delivery_date' => "2020-12-18 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-21 00:00:00",'delivery_date' => "2020-12-22 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-22 00:00:00",'delivery_date' => "2020-12-23 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-23 00:00:00",'delivery_date' => "2020-12-24 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-24 00:00:00",'delivery_date' => "2020-12-25 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2020-06-29";
        $deliveryDate->save();

        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-28 00:00:00",'delivery_date' => "2020-12-29 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-29 00:00:00",'delivery_date' => "2020-12-30 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-30 00:00:00",'delivery_date' => "2020-12-31 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->save();
        $deliveryDate = \App\Models\DeliveryDate::firstOrCreate(['export_date' => "2020-12-31 00:00:00",'delivery_date' => "2021-01-01 00:00:00",'country_id' => $de->id]);
        $deliveryDate->cut_off_date = $deliveryDate->export_date;
        $deliveryDate->disabled = "1";
        $deliveryDate->alternate_date = "2021-01-08";
        $deliveryDate->save();

        $deliveryDates = \App\Models\DeliveryDate::all();
        foreach($deliveryDates as $deliveryDate){

                $ex = \Carbon\Carbon::parse($deliveryDate->export_date);
            if($deliveryDate->country_id!=$ch->id) {
                $ex->setTime(11, 30, 0, 0);
            }
                $deliveryDate->cut_off_date = $ex;
                $deliveryDate->save();

        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function($table) {
            $table->dropColumn('original_delivery_date');
        });
        Schema::table('delivery_dates', function($table) {
            $table->dropColumn('cut_off_date');
        });
    }
}
