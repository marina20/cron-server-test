<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Models\Product;
use Illuminate\Database\Migrations\Migration;

class Add5PouchesToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $princeVanilliam = DB::table('products')->insertGetId([
            'product_identifier' => 'J',
            'name' => 'Prince Vanilliam',
            'slug' => 'prince-vanilliam',
            'image' => 'prince-vanilliam.png',
            'image_thumbnail' => 'timg-prince-vanilliam.png',
            'image_alt_tag_key' => 'PRINCE_VANILLIAM',
            'image_alt_tag_text' => 'Yamo Quetschie Prince Vanilliam',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"6"},{"category":"produce_type","value":"fruchtig"},{"category":"weight","value":"120g"},{"category":"ingredients","value":"Banane"},{"category":"ingredients","value":"Erdbeere"},{"category":"ingredients","value":"Vanille"},{"category":"product_name_color","value":"#DB9E95"}, {"category":"information","value":"Unser Prinz bringt royale Stilsicherheit in die yamo Quetschies. Dank königlich leckeren Zutaten und einem Hauch Vanille stehen nicht nur die Jüngsten unter uns sondern auch kreischende Mamas auf den charmanten Gentleman. Man munkelt, selbst die Queen lagere einen kleinen Vorrat in ihrem Kühlschrank."},{"category":"time_of_day","value":"Für zwischendurch"}, {"category":"prep_type","value":"Zum Kühl geniessen"},{"category":"ingredients_description","value":"Banane 48%, Erdbeere 42%, Joghurt 10%, Borboun-Vanille Extrakt 0,2%. Enthält Milch."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"280 kJ/ 66 kcal","indentation":""},{"name":"Fett","value":"0,6 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,3 g","indentation":"true"},{"name":"Kohlenhydrate","value":"12 g","indentation":""},{"name":"davon Zucker","value":"11 g","indentation":"true"},{"name":"Eiweiss","value":"1,3 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]}]}',
            'position' => '8300'
        ]);
        $nickiSpinaj = DB::table('products')->insertGetId([
            'product_identifier' => 'G',
            'name' => 'Nicki Spinaj',
            'slug' => 'nicki-spinaj',
            'image' => 'nicki-spinaj.png',
            'image_thumbnail' => 'timg-nicki-spinaj.png',
            'image_alt_tag_key' => 'NICKI_SPINAJ',
            'image_alt_tag_text' => 'Yamo Quetschie Nicki Spinaj',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"6"},{"category":"produce_type","value":"fruchtig"},{"category":"weight","value":"120g"},{"category":"ingredients","value":"Apfel"},{"category":"ingredients","value":"Gurke"},{"category":"ingredients","value":"Spinat"},{"category":"ingredients","value":"Banane"},{"category":"product_name_color","value":"#89AE1B"}, {"category":"information","value":"Wenn Nicki Spinaj ihre Hüllen fallen lässt, entblösst sie knackiges Gemüse und süsse Früchte, die nicht nur Gangster Rapper betören. Im Gegensatz zu ihrer Musik, ist unsere Nicki ein Star aller Altersgruppen."},{"category":"time_of_day","value":"Für zwischendurch"}, {"category":"prep_type","value":"Zum Kühl geniessen"},{"category":"ingredients_description","value":"Apfel 62%, Banane 18%, Gurke 12%, Spinat 8%. Keine Allergene."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"233 kJ/ 55 kcal","indentation":""},{"name":"Fett","value":"<0,5 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,2 g","indentation":"true"},{"name":"Kohlenhydrate","value":"11 g","indentation":""},{"name":"davon Zucker","value":"9,7 g","indentation":"true"},{"name":"Eiweiss","value":"0,7 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]}]}',
            'position' => '8400'
        ]);
        $quentinCarrotino = DB::table('products')->insertGetId([
            'product_identifier' => 'I',
            'name' => 'Quentin Carrotino',
            'slug' => 'quentin-carrotino',
            'image' => 'quentin-carrotino.png',
            'image_thumbnail' => 'timg-quentin-carrotino.png',
            'image_alt_tag_key' => 'QUENTIN_CARROTINO',
            'image_alt_tag_text' => 'Yamo Quetschie Quentin Carrotino',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"6"},{"category":"produce_type","value":"fruchtig"},{"category":"weight","value":"120g"},{"category":"ingredients","value":"Aprikose"},{"category":"ingredients","value":"Mango"},{"category":"ingredients","value":"Süsskartoffel"},{"category":"ingredients","value":"Karotte"},{"category":"product_name_color","value":"#EF7E07"}, {"category":"information","value":"Anstatt der langen Dialoge aus seinen Filmen, kommt unser Quentin Carrotino schnell zum Punkt. Eine Geschmacksexplosion, ganz ohne schwarzen Humor dafür mit genau so viel Coolness wie in Pulp Fiction. Definitiv der heisseste Anwärter für die Oscars!"},{"category":"time_of_day","value":"Für zwischendurch"}, {"category":"prep_type","value":"Zum Kühl geniessen"},{"category":"ingredients_description","value":"Aprikose 34%, Mango 28%, Süsskartoffel 27%, Karotte 11%, Zitronensaft. Keine Allergene."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"302 kJ/ 71 kcal","indentation":""},{"name":"Fett","value":"<0,5 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"<0,01 g","indentation":"true"},{"name":"Kohlenhydrate","value":"14 g","indentation":""},{"name":"davon Zucker","value":"8,7 g","indentation":"true"},{"name":"Eiweiss","value":"1,1 g","indentation":""},{"name":"Salz","value":"0,03 g","indentation":""}]}]}',
            'position' => '8500'
        ]);
        $davidCocofield = DB::table('products')->insertGetId([
            'product_identifier' => 'W',
            'name' => 'David Cocofield',
            'slug' => 'david-cocofield',
            'image' => 'david-cocofield.png',
            'image_thumbnail' => 'timg-david-cocofield.png',
            'image_alt_tag_key' => 'DAVID_COCOFIELD',
            'image_alt_tag_text' => 'Yamo Quetschie David Cocofield',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"6"},{"category":"produce_type","value":"fruchtig"},{"category":"weight","value":"120g"},{"category":"ingredients","value":"Apfel"},{"category":"ingredients","value":"Birne"},{"category":"ingredients","value":"Kokosmilch"},{"category":"ingredients","value":"Hirse"},{"category":"product_name_color","value":"#BEA068"}, {"category":"information","value":""},{"category":"time_of_day","value":"Für zwischendurch"}, {"category":"prep_type","value":"Zum Kühl geniessen"},{"category":"ingredients_description","value":"Apfel 44%, Birne 34%, Kokosmilch 20%, Hirse 2%. Keine Allergene."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"363 kJ/ 87 kcal","indentation":""},{"name":"Fett","value":"3,9 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"1,7 g","indentation":"true"},{"name":"Kohlenhydrate","value":"11 g","indentation":""},{"name":"davon Zucker","value":"8,1 g","indentation":"true"},{"name":"Eiweiss","value":"0,6 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]}]}',
            'position' => '8600'
        ]);
        $berryPotter = DB::table('products')->insertGetId([
            'product_identifier' => 'K',
            'name' => 'Berry Potter',
            'slug' => 'berry-potter',
            'image' => 'berry-potter.png',
            'image_thumbnail' => 'timg-berry-potter.png',
            'image_alt_tag_key' => 'BERRY_POTTER',
            'image_alt_tag_text' => 'Yamo Quetschie Berry Potter',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"6"},{"category":"produce_type","value":"fruchtig"},{"category":"weight","value":"120g"},{"category":"ingredients","value":"Joghurt"},{"category":"ingredients","value":"Birne"},{"category":"ingredients","value":"Heidelbeeren"},{"category":"ingredients","value":"Haferflocken"},{"category":"product_name_color","value":"#8173AB"}, {"category":"information","value":""},{"category":"time_of_day","value":"Für zwischendurch"}, {"category":"prep_type","value":"Zum Kühl geniessen"},{"category":"ingredients_description","value":"Joghurt 40%, Birne 31%, Banane 15%, Heidelbeere 12%, Haferflocken 2%. Enthält Milch und Gluten."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"308 kJ/ 73 kcal","indentation":""},{"name":"Fett","value":"1,8 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"1,0 g","indentation":"true"},{"name":"Kohlenhydrate","value":"11 g","indentation":""},{"name":"davon Zucker","value":"8,4 g","indentation":"true"},{"name":"Eiweiss","value":"2,3 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]}]}',
            'position' => '8700'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Product::where('slug','prince-vanilliam')->first()->delete();
        Product::where('slug','nicki-spinaj')->first()->delete();
        Product::where('slug','quentin-carrotino')->first()->delete();
        Product::where('slug','david-cocofield')->first()->delete();
        Product::where('slug','berry-potter')->first()->delete();
    }
}
