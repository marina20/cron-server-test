<?php

use App\Models\Press;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateImageNamesInPressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $presses = Press::all();
        foreach($presses as $press){
            $press->delete();
        }
        Press::create(['link'=>'','meta_title'=>"20 Minuten",'meta_alt'=>'20 Minuten','image'=>'20_minuten.png']);
        Press::create(['link'=>'','meta_title'=>"Wir Eltern",'meta_alt'=>'Wir Eltern','image'=>'wir_eltern.png']);
        Press::create(['link'=>'','meta_title'=>"Forbes",'meta_alt'=>'Forbes','image'=>'forbes.png']);
        Press::create(['link'=>'','meta_title'=>"Tagesanzeiger",'meta_alt'=>'Tagesanzeiger','image'=>'tagesanzeiger.png']);
        Press::create(['link'=>'','meta_title'=>"Coop",'meta_alt'=>'Coop','image'=>'coop.png']);
        Press::create(['link'=>'','meta_title'=>"Swissmom",'meta_alt'=>'Swissmom','image'=>'swissmom.png']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
