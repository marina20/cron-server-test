<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Order;

class AddDepositFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function($table) {
            $table->boolean('deposit_authorisation')->after('shipping_phone')->default(true);
            $table->text('deposit_location')->after('deposit_authorisation');
        });
        DB::table('orders')->update(['deposit_location'=> Order::LOCATION_AT_THE_DOOR]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function($table) {
            $table->dropColumn('deposit_authorisation');
            $table->dropColumn('deposit_location');
        });
    }
}
