<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrdersTableAddTitlesColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function(Blueprint $table) {
            DB::statement('alter table orders change column billing_gender billing_title VARCHAR(255) NULL;');
            $table->string('shipping_title')->nullable()->after('billing_title');
            DB::statement('alter table orders modify payment_method_id VARCHAR(255) NULL;');
            DB::statement('alter table orders modify payment_method_title VARCHAR(255) NULL;');
            DB::statement('alter table orders modify transaction_id VARCHAR(255) NULL;');
            DB::statement('alter table orders modify customer_ip_address VARCHAR(255) NULL;');
            DB::statement('alter table orders modify customer_user_agent VARCHAR(255) NULL;');
            DB::statement('alter table orders modify created_via VARCHAR(255) NULL;');
            DB::statement('alter table orders modify completed_date VARCHAR(255) NULL;');
            DB::statement('alter table orders modify paid_date VARCHAR(255) NULL;');
            DB::statement('alter table orders modify billing_company VARCHAR(255) NULL;');
            DB::statement('alter table orders modify billing_address_2 VARCHAR(255) NULL;');
            DB::statement('alter table orders modify billing_state VARCHAR(255) NULL;');
            DB::statement('alter table orders modify shipping_company VARCHAR(255) NULL;');
            DB::statement('alter table orders modify shipping_address_2 VARCHAR(255) NULL;');
            DB::statement('alter table orders modify shipping_state VARCHAR(255) NULL;');
            DB::statement('alter table orders modify discount VARCHAR(255) NULL;');
            DB::statement('alter table orders modify discount_tax VARCHAR(255) NULL;');
            DB::statement('alter table orders modify shipping VARCHAR(255) NULL;');
            DB::statement('alter table orders modify shipping_tax VARCHAR(255) NULL;');
            DB::statement('alter table orders modify order_tax VARCHAR(255) NULL;');
            DB::statement('alter table orders modify order_total VARCHAR(255) NULL;');
            DB::statement('alter table orders modify shipping_method VARCHAR(255) NULL;');
            DB::statement('alter table orders modify pending_state VARCHAR(255) NULL;');
            DB::statement('alter table orders modify child_birthday VARCHAR(255) NULL;');
            DB::statement('alter table orders modify billing_delivery_date VARCHAR(255) NULL;');
            DB::statement('alter table orders modify billing_title VARCHAR(255) NULL;');
            DB::statement('alter table orders modify cancelled_email_sent VARCHAR(255) NULL;');
            DB::statement('alter table orders modify order_discount VARCHAR(255) NULL;');
            DB::statement('alter table orders modify billing_child_birthday VARCHAR(255) NULL;');
            DB::statement('alter table orders modify parent_id VARCHAR(255) NULL;');
            DB::statement('alter table orders modify status VARCHAR(255) NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function(Blueprint $table) {
            DB::statement('alter table orders change column billing_title billing_gender VARCHAR(255) NULL;');
            $table->dropColumn('shipping_title');
            DB::statement('alter table orders modify payment_method_id VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify payment_method_title VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify transaction_id VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify customer_ip_address VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify customer_user_agent VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify created_via VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify completed_date VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify paid_date VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify billing_company VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify billing_address_2 VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify billing_state VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify shipping_company VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify shipping_address_2 VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify shipping_state VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify discount VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify discount_tax VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify shipping VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify shipping_tax VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify order_tax VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify order_total VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify shipping_method VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify pending_state VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify child_birthday VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify billing_delivery_date VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify billing_gender VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify cancelled_email_sent VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify order_discount VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify billing_child_birthday VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify parent_id VARCHAR(255) NOT NULL;');
            DB::statement('alter table orders modify status VARCHAR(255) NOT NULL;');
        });
    }
}
