<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdjustAllTablesForNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('alter table categories modify url VARCHAR(255) NULL;');
        DB::statement('alter table categories modify category_id VARCHAR(255) NULL;');
        DB::statement('alter table order_items modify item_name VARCHAR(255) NULL;');
        DB::statement('alter table order_items modify item_type VARCHAR(255) NULL;');
        DB::statement('alter table order_items modify product_id VARCHAR(255) NULL;');
        DB::statement('alter table order_items modify qty VARCHAR(255) NULL;');
        DB::statement('alter table order_items modify tax VARCHAR(255) NULL;');
        DB::statement('alter table order_items modify total VARCHAR(255) NULL;');
        DB::statement('alter table order_items modify subtotal VARCHAR(255) NULL;');
        DB::statement('alter table order_items modify subtotal_tax VARCHAR(255) NULL;');
        DB::statement('alter table order_items modify description VARCHAR(255) NULL;');
        DB::statement('alter table order_items modify tax_rate VARCHAR(255) NULL;');
        DB::statement('alter table press modify meta_alt VARCHAR(255) NULL;');
        DB::statement('alter table press modify meta_title VARCHAR(255) NULL;');
        DB::statement('alter table products modify url VARCHAR(255) NULL;');
        DB::statement('alter table products modify image VARCHAR(255) NULL;');
        DB::statement('alter table testimonials modify image VARCHAR(255) NULL;');
        DB::statement('alter table testimonials modify description VARCHAR(255) NOT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('alter table categories modify url VARCHAR(255) NOT NULL;');
        DB::statement('alter table categories modify category_id VARCHAR(255) NOT NULL;');
        DB::statement('alter table order_items modify item_name VARCHAR(255) NOT NULL;');
        DB::statement('alter table order_items modify item_type VARCHAR(255) NOT NULL;');
        DB::statement('alter table order_items modify product_id VARCHAR(255) NOT NULL;');
        DB::statement('alter table order_items modify qty VARCHAR(255) NOT NULL;');
        DB::statement('alter table order_items modify tax VARCHAR(255) NOT NULL;');
        DB::statement('alter table order_items modify total VARCHAR(255) NOT NULL;');
        DB::statement('alter table order_items modify subtotal VARCHAR(255) NOT NULL;');
        DB::statement('alter table order_items modify subtotal_tax VARCHAR(255) NOT NULL;');
        DB::statement('alter table order_items modify description VARCHAR(255) NOT NULL;');
        DB::statement('alter table order_items modify tax_rate VARCHAR(255) NOT NULL;');
        DB::statement('alter table press modify meta_alt VARCHAR(255) NOT NULL;');
        DB::statement('alter table press modify meta_title VARCHAR(255) NOT NULL;');
        DB::statement('alter table products modify url VARCHAR(255) NOT NULL;');
        DB::statement('alter table products modify image VARCHAR(255) NOT NULL;');
        DB::statement('alter table testimonials modify image VARCHAR(255) NOT NULL;');
        DB::statement('alter table testimonials modify description VARCHAR(255) NOT NULL;');
    }
}
