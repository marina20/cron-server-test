<?php

use App\Models\BoxContent;
use App\Models\Product;
use App\Models\SuggestedBox;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSuggestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $art_pirates_of_the_carrotean = Product::withoutGlobalScope('active')->where(['slug' => 'pirates-of-the-carrotean'])->first();
        $art_fresh_prince_of_bel_pear = Product::withoutGlobalScope('active')->where(['slug' => 'fresh-prince-of-bel-pear'])->first();
        $art_applecalypse_now = Product::withoutGlobalScope('active')->where(['slug' => 'applecalypse-now'])->first();
        $art_david_zucchetta = Product::withoutGlobalScope('active')->where(['slug' =>  'david-zucchetta'])->first();
        $art_mango_nr_five = Product::withoutGlobalScope('active')->where(['slug' => 'mango-no-5'])->first();
        $art_broccoly_balboa = Product::withoutGlobalScope('active')->where(['slug' => 'broccoly-balboa'])->first();
        $art_peach_boys = Product::withoutGlobalScope('active')->where(['slug' => 'peach-boys'])->first();
        $art_anthony_pumpkins = Product::withoutGlobalScope('active')->where(['slug' => 'anthony-pumpkins'])->first();
        $art_inbanana_jones = Product::withoutGlobalScope('active')->where(['slug' => 'inbanana-jones'])->first();
        $art_avocado_di_caprio = Product::withoutGlobalScope('active')->where(['slug' => 'avocado-di-caprio'])->first();
        $art_katy_berry = Product::withoutGlobalScope('active')->where(['slug' => 'katy-berry'])->first();
        $art_sweet_home_albanana = Product::withoutGlobalScope('active')->where(['slug' => 'sweet-home-albanana'])->first();
        $art_beetney_spears = Product::withoutGlobalScope('active')->where(['slug' => 'beetney-spears'])->first();
        $art_cocohontas = Product::withoutGlobalScope('active')->where(['slug' => 'cocohontas'])->first();

        $country_ch = \App\Models\Country::where(['content_code' => 'de-ch'])->first();
        $country_de = \App\Models\Country::where(['content_code' => 'de-de'])->first();
        $country_at = \App\Models\Country::where(['content_code' => 'de-at'])->first();

        // Workaround for gitlab-test. Reason: Products are not created via migrations. But they exist on our locals, staging and production...
        if(!empty($art_pirates_of_the_carrotean)) {

            $sbox1 = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'breil', 'product_content' => 'fruchtig'])->first();

            $sbox2 = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'breil', 'product_content' => 'gemusig'])->first();
            $sbox3 = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'breil', 'product_content' => 'all'])->first();


            $items = $sbox3->box->items;
            foreach($items as $item){
                 $item->delete();
            }
            BoxContent::create(['product_id' => $art_pirates_of_the_carrotean->id, 'box_id' => $sbox3->box->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $sbox3->box->id, 'quantity' => 4]);
            BoxContent::create(['product_id' => $art_applecalypse_now->id, 'box_id' => $sbox3->box->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $sbox3->box->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_anthony_pumpkins->id, 'box_id' => $sbox3->box->id, 'quantity' => 3]);


            $sbox4 = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'pouch', 'product_content' => 'fruchtig'])->first();

            $items = $sbox4->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $sbox4->box->id, 'quantity' => 8]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $sbox4->box->id, 'quantity' => 8]);



            $sbox5 = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'pouch', 'product_content' => 'gemusig'])->first();
            $items = $sbox5->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $sbox5->box->id, 'quantity' => 8]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $sbox5->box->id, 'quantity' => 8]);

            $sbox6 = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'pouch', 'product_content' => 'all'])->first();
            $items = $sbox6->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $sbox6->box->id, 'quantity' => 8]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $sbox6->box->id, 'quantity' => 8]);

            $sbox7 = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'all', 'product_content' => 'fruchtig'])->first();
            $sbox8 = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'all', 'product_content' => 'gemusig'])->first();
            $sbox9 = SuggestedBox::where(['children_age' => '4+', 'product_type' => 'all', 'product_content' => 'all'])->first();
            $items = $sbox9->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_pirates_of_the_carrotean->id, 'box_id' => $sbox9->box->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $sbox9->box->id, 'quantity' => 4]);
            BoxContent::create(['product_id' => $art_applecalypse_now->id, 'box_id' => $sbox9->box->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $sbox9->box->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_anthony_pumpkins->id, 'box_id' => $sbox9->box->id, 'quantity' => 3]);


            $sbox10 = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'breil', 'product_content' => 'fruchtig'])->first();
            $items = $sbox10->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $sbox10->box->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_applecalypse_now->id, 'box_id' => $sbox10->box->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $sbox10->box->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_mango_nr_five->id, 'box_id' => $sbox10->box->id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_sweet_home_albanana->id, 'box_id' => $sbox10->box->id, 'quantity' => 5]);




            $sbox11 = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'breil', 'product_content' => 'gemusig'])->first();
            $sbox12 = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'breil', 'product_content' => 'all'])->first();
            $items = $sbox12->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_mango_nr_five->id, 'box_id' => $sbox12->box->id, 'quantity' => 4]);
            BoxContent::create(['product_id' => $art_sweet_home_albanana->id, 'box_id' => $sbox12->box->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_broccoly_balboa->id, 'box_id' => $sbox12->box->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_beetney_spears->id, 'box_id' => $sbox12->box->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_cocohontas->id, 'box_id' => $sbox12->box->id, 'quantity' => 3]);

            $sbox13 = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'pouch', 'product_content' => 'fruchtig'])->first();
            $items = $sbox13->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $sbox13->box->id, 'quantity' => 8]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $sbox13->box->id, 'quantity' => 8]);

            $sbox14 = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'pouch', 'product_content' => 'gemusig'])->first();
            $items = $sbox14->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $sbox14->box->id, 'quantity' => 8]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $sbox14->box->id, 'quantity' => 8]);

            $sbox15 = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'pouch', 'product_content' => 'all'])->first();
            $items = $sbox15->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $sbox15->box->id, 'quantity' => 8]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $sbox15->box->id, 'quantity' => 8]);

            $sbox16 = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'all', 'product_content' => 'fruchtig'])->first();
            $items = $sbox16->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $sbox16->box->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $sbox16->box->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_mango_nr_five->id, 'box_id' => $sbox16->box->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_sweet_home_albanana->id, 'box_id' => $sbox16->box->id, 'quantity' => 2]);

            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $sbox16->box->id, 'quantity' => 4]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $sbox16->box->id, 'quantity' => 4]);

            $sbox17 = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'all', 'product_content' => 'gemusig'])->first();
            $sbox18 = SuggestedBox::where(['children_age' => '6+', 'product_type' => 'all', 'product_content' => 'all'])->first();
            $items = $sbox18->box->items;
            foreach($items as $item){
                $item->delete();
            }
            BoxContent::create(['product_id' => $art_mango_nr_five->id, 'box_id' => $sbox18->box->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_sweet_home_albanana->id, 'box_id' => $sbox18->box->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_beetney_spears->id, 'box_id' => $sbox18->box->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $sbox18->box->id, 'quantity' => 4]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $sbox18->box->id, 'quantity' => 4]);



        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
