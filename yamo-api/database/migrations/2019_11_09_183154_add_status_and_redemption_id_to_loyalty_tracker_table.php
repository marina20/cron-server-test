<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusAndRedemptionIdToLoyaltyTrackerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loyalty_trackers', function($table) {
            $table->unsignedInteger('redemption_id')->after('voucher_id')->nullable();
            $table->string('status')->after('redemption_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loyalty_trackers', function (Blueprint $table) {
            $table->dropColumn('redemption_id');
            $table->dropColumn('status');
        });
    }
}
