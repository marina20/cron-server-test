<?php

use App\Models\AdyenNotification;
use App\Models\Order;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrderAdyenAuthResult extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adyen_notifications', function (Blueprint $table) {
            $table->unsignedInteger('order_id')->nullable()->after('notification_body');
            $table->foreign('order_id')->references('id')->on('orders');
            $table->string('event_code')->nullable()->after('order_id');
            $table->string('reason')->nullable()->after('event_code');
            $table->string('psp_reference')->nullable()->after('reason');
            $table->boolean('success')->nullable()->after('psp_reference');
        });

        set_time_limit(0);
        $notifications = AdyenNotification::get();
        foreach ($notifications as $notification) {
            $data = json_decode($notification->notification_body);
            if(empty($data))
                continue;
            $notification_item = reset($data->notificationItems);
            $notification_item = $notification_item->NotificationRequestItem;
            if ($notification_item->pspReference) {
                $notification->psp_reference = $notification_item->pspReference;
            }
            if ($notification_item->eventCode) {
                $notification->event_code = $notification_item->eventCode;
            }
            if ($notification_item->reason) {
                $notification->reason = $notification_item->reason;
            }
            if ($notification_item->success) {
                $notification->success = $notification_item->success;
            }
            $order = Order::find($notification_item->merchantReference);
            if ($order) {
                $notification->order_id = $order->id;
                if($order->adyen_auth_result === NULL && $notification_item->eventCode) {
                    $order->adyen_auth_result = $notification_item->eventCode;
                    if ($order->transaction_id != $notification_item->pspReference) {
                        $order->transaction_id = $notification_item->pspReference;
                    }
                } elseif ($order->transaction_id === NULL  && $notification_item->pspReference) {
                    $order->transaction_id = $notification_item->pspReference;
                }
                $order->save();
            }
            $notification->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('adyen_notifications', function ($table){
            $table->dropForeign(['order_id']);
            $table->dropColumn('order_id');
            $table->dropColumn('event_code');
            $table->dropColumn('psp_reference');
            $table->dropColumn('reason');
            $table->dropColumn('success');
        });

        Schema::enableForeignKeyConstraints();
    }
}
