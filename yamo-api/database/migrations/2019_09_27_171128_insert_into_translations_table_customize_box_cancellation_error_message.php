<?php

use App\Models\Translation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoTranslationsTableCustomizeBoxCancellationErrorMessage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Translation::create(['key-key' => 'ERROR.CUSTOMIZE_BOX_CANCELLATION_DEADLINE_PASSED','de-de'=>'Die Änderung der Produkte in deiner Box ist nur bis 5 Tage vor Versand möglich.','de-ch'=>'Die Änderung der Produkte in deiner Box ist nur bis 5 Tage vor Versand möglich.','de-at'=>'Die Änderung der Produkte in deiner Box ist nur bis 5 Tage vor Versand möglich.','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
