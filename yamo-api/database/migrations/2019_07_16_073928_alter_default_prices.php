<?php

use App\Models\Product;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDefaultPrices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $countries = \App\Models\Country::all();
        $product = Product::withoutGlobalScope('active')->where('slug','subscription-custom-box')->first();
        if(!empty($product)) {
            foreach ($countries as $country) {
                $thePrice = \App\Models\Price::where(['product_id' => $product->id])->where(['country_id' => $country->id])->first();
                if($country->content_code == 'de-de'){
                    $thePrice->value = '36.00';
                    $thePrice->tax = '2.355';
                }
                else if($country->content_code == 'de-at'){
                    $thePrice->value = '39.20';
                    $thePrice->tax = '3.564';
                }
                else if($country->content_code == 'de-ch'){
                    $thePrice->value = '60.00';
                    $thePrice->tax = '1.463';
                }
                $thePrice->save();
            }
        }

        $product = Product::withoutGlobalScope('active')->where('slug','brei-abo-monate-4-v3')->first();
        if(!empty($product)) {
            foreach ($countries as $country) {
                $thePrice = \App\Models\Price::where(['product_id' => $product->id])->where(['country_id' => $country->id])->first();
                if($country->content_code == 'de-de'){
                    $thePrice->value = '36.00';
                    $thePrice->tax = '2.355';
                }
                else if($country->content_code == 'de-at'){
                    $thePrice->value = '39.20';
                    $thePrice->tax = '3.564';
                }
                else if($country->content_code == 'de-ch'){
                    $thePrice->value = '60.00';
                    $thePrice->tax = '1.463';
                }
                $thePrice->save();
            }
        }

        $product = Product::withoutGlobalScope('active')->where('slug','brei-abo-monate-6-v3')->first();
        if(!empty($product)) {
            foreach ($countries as $country) {
                $thePrice = \App\Models\Price::where(['product_id' => $product->id])->where(['country_id' => $country->id])->first();
                if($country->content_code == 'de-de'){
                    $thePrice->value = '36.00';
                    $thePrice->tax = '2.355';
                }
                else if($country->content_code == 'de-at'){
                    $thePrice->value = '39.20';
                    $thePrice->tax = '3.564';
                }
                else if($country->content_code == 'de-ch'){
                    $thePrice->value = '60.00';
                    $thePrice->tax = '1.463';
                }
                $thePrice->save();
            }
        }

        // first abo-box

        $product = new Product();
        $product->image = 'yamobox-abonew.png';
        $product->image_thumbnail = 'yamobox-abonew.png';
        $product->name = "Deine individuelle Box";
        $product->short_name = "Abo konfigurieren";
        $product->category = "subscription-first-custom-box";
        $product->slug = "subscription-first-custom-box";
        $product->is_coupon_applicable = 1;
        $product->save();
        $price = new \App\Models\Price();
        $price->value = "47.20";
        $price->product_id = $product->id;
        $price->country_id = 2;
        $price->tax_percentage = 2.5;
        $price->shipping_tax_percentage = 7.7;
        $price->tax = "1.151";
        $price->save();

        $price = new \App\Models\Price();
        $price->value = "29.60";
        $price->product_id = $product->id;
        $price->country_id = 3;
        $price->tax_percentage = 10;
        $price->shipping_tax_percentage = 20;
        $price->tax = "2.691";
        $price->save();

        $price = new \App\Models\Price();
        $price->value = "28.00";
        $price->product_id = $product->id;
        $price->country_id = 1;
        $price->tax_percentage = 7;
        $price->shipping_tax_percentage = 19;
        $price->tax = "1.832";
        $price->save();

        // single-box-custom-box

        $product = Product::withoutGlobalScope('active')->where('slug','single-box-custom-box')->first();
        if(!empty($product)) {
            foreach ($countries as $country) {
                $thePrice = \App\Models\Price::where(['product_id' => $product->id])->where(['country_id' => $country->id])->first();
                if($country->content_code == 'de-de'){
                    $thePrice->value = '40.00';
                    $thePrice->tax = '2.617';
                }
                else if($country->content_code == 'de-at'){
                    $thePrice->value = '42.40';
                    $thePrice->tax = '3.855';
                }
                else if($country->content_code == 'de-ch'){
                    $thePrice->value = '68.00';
                    $thePrice->tax = '1.659';
                }
                $thePrice->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function($table) {
            $table->dropColumn('resent');
        });
    }
}
