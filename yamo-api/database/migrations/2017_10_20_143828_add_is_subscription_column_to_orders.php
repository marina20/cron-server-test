<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsSubscriptionColumnToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function($table) {
            $table->boolean('is_subscription')->after('billing_child_birthday');
            $table->string('parent_id')->after('is_subscription');
            $table->string('status')->after('parent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function($table) {
            $table->dropColumn('is_subscription');
            $table->dropColumn('parent_id');
            $table->dropColumn('status');
        });
    }
}
