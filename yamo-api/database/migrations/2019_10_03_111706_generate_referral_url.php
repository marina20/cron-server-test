<?php
ini_set('memory_limit','1000M');
use App\Models\Translation;
use App\Models\ReferralUrl;
use App\Services\CreateOrder\CreateOrder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use App\Traits\FormatMoney;


class GenerateReferralUrl extends Migration
{
    use FormatMoney;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $users = User::all();
        foreach($users as $user){
            if(!empty($user->profile) && !empty($user->profile->shipping_country)) {
                ReferralUrl::generateReferral($user);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
