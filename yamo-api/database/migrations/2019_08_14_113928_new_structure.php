<?php

use App\Models\Product;
use App\Models\Translation;
use App\Models\Voucher;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewStructure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.GIVEBACK','de-de'=>'Jetzt gibt es &#128184; zurück!','de-ch'=>'Jetzt gibt es &#128184; zurück!','de-at'=>'Jetzt gibt es &#128184; zurück!','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.YOURNEXTBOX','de-de'=>'deine nächste yamo-Box bekommst du','de-ch'=>'deine nächste yamo-Box bekommst du','de-at'=>'deine nächste yamo-Box bekommst du','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.CHEAPER','de-de'=>'günstiger','de-ch'=>'günstiger','de-at'=>'günstiger','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.HELLO','de-de'=>'Hallo','de-ch'=>'Hallo','de-at'=>'Hallo','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.CONCRATS','de-de'=>'Gratuliere! Einer deiner&nbsp;Freunde &nbsp;hat dank dir eine yamo-Box bestellt.','de-ch'=>'Gratuliere! Einer deiner&nbsp;Freunde &nbsp;hat dank dir eine yamo-Box bestellt.','de-at'=>'Gratuliere! Einer deiner&nbsp;Freunde &nbsp;hat dank dir eine yamo-Box bestellt.','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.YOURECEIVE','de-de'=>'Somit erh&auml;ltst du','de-ch'=>'Somit erh&auml;ltst du','de-at'=>'Somit erh&auml;ltst du','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.DISCOUNTFORNEXTORDER','de-de'=>'auf deine n&auml;chste Bestellung geschenkt!','de-ch'=>'auf deine n&auml;chste Bestellung geschenkt!','de-at'=>'auf deine n&auml;chste Bestellung geschenkt!','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.WANTMORE','de-de'=>'Lust noch mehr Rabatte zu sammeln? Dann lade gleich weitere Freunde ein:','de-ch'=>'Lust noch mehr Rabatte zu sammeln? Dann lade gleich weitere Freunde ein:','de-at'=>'Lust noch mehr Rabatte zu sammeln? Dann lade gleich weitere Freunde ein:','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.MYPERSONALLINK','de-de'=>'Mein persönlicher Link','de-ch'=>'Mein persönlicher Link','de-at'=>'Mein persönlicher Link','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.BESTREGARDS','de-de'=>'Herzliche Gr&uuml;sse,','de-ch'=>'Herzliche Gr&uuml;sse,','de-at'=>'Herzliche Gr&uuml;sse,','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.YOURTEAM','de-de'=>'Zoe und das yamo-Team','de-ch'=>'Zoe und das yamo-Team','de-at'=>'Zoe und das yamo-Team','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.FURTHERQUESTIONS','de-de'=>'Hast du weitere Fragen? Dann schau in unseren','de-ch'=>'Hast du weitere Fragen? Dann schau in unseren','de-at'=>'Hast du weitere Fragen? Dann schau in unseren','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.FAQS','de-de'=>'FAQs','de-ch'=>'FAQs','de-at'=>'FAQs','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.ORDIRECTCONTACT','de-de'=>'vorbei oder melde dich direkt per E-Mail oder Telefon&nbsp;bei uns.','de-ch'=>'vorbei oder melde dich direkt per E-Mail oder Telefon&nbsp;bei uns.','de-at'=>'vorbei oder melde dich direkt per E-Mail oder Telefon&nbsp;bei uns.','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.SOCIALMEDIAFUN','de-de'=>'Falls dich interessiert, was wir auf Social Media Lustiges treiben, folge uns auf:','de-ch'=>'Falls dich interessiert, was wir auf Social Media Lustiges treiben, folge uns auf:','de-at'=>'Falls dich interessiert, was wir auf Social Media Lustiges treiben, folge uns auf:','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.COPYRIGHT','de-de'=>'Copyright &copy; 2019&nbsp;| yamo food GmbH. All rights reserved.','de-ch'=>'Copyright &copy; 2019&nbsp;| yamo food GmbH. All rights reserved.','de-at'=>'Copyright &copy; 2019&nbsp;| yamo food GmbH. All rights reserved.','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.IMPRESSUM','de-de'=>'Impressum','de-ch'=>'Impressum','de-at'=>'Impressum','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.YAMONAME','de-de'=>'yamo food GmbH','de-ch'=>'yamo AG','de-at'=>'yamo food GmbH','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.YAMOADDRESS','de-de'=>'Nymphenburger Str. 1&nbsp;| 80335 M&uuml;nchen','de-ch'=>'Zählerweg 3 | 6300 Zug','de-at'=>'Nymphenburger Str. 1&nbsp;| 80335 M&uuml;nchen','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.YAMOLAW','de-de'=>'Amtsgericht M&uuml;nchen: HRB 243321 | Umsatzsteuer-ID: DE314599497','de-ch'=>'Handelsregisteramt: Kanton Zug | MwSt-Nr.: CHE-406.373.849','de-at'=>'Amtsgericht M&uuml;nchen: HRB 243321 | Umsatzsteuer-ID: DE314599497','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.YAMOLEADERS','de-de'=>'Gesch&auml;ftsf&uuml;hrer: Tobias Gunzenhauser, Jos&eacute; Amado-Blanco','de-ch'=>'Geschäftsführer: Tobias Gunzenhauser, José Amado-Blanco, Luca Michas','de-at'=>'Gesch&auml;ftsf&uuml;hrer: Tobias Gunzenhauser, Jos&eacute; Amado-Blanco','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.UNSUBSCRIBE','de-de'=>'Unsubscribe','de-ch'=>'Unsubscribe','de-at'=>'Unsubscribe','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.UNSUBSCRIBEPREFERENCES','de-de'=>'Unsubscribe Preferences','de-ch'=>'Unsubscribe Preferences','de-at'=>'Unsubscribe Preferences','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MAIL.REFEREE-COMPLETED.SUBJECT','de-de'=>'Dein Freundes-Discount wurde verwendet','de-ch'=>'Dein Freundes-Discount wurde verwendet','de-at'=>'Dein Freundes-Discount wurde verwendet','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);

        Translation::create(['key-key' => 'REFERRAL.CHECK.SUCCESS','de-de'=>'Bei deiner Bestellung wird der Freundes-link genutzt und du bekommst einen Rabatt','de-ch'=>'Bei deiner Bestellung wird der Freundes-link genutzt und du bekommst einen Rabatt','de-at'=>'Bei deiner Bestellung wird der Freundes-link genutzt und du bekommst einen Rabatt','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'REFERRAL.CHECK.COUNTRYFAIL','de-de'=>'Der Freundes-link funktioniert leider nicht, da du nicht aus dem gleichen Land wie dein Freund bist','de-ch'=>'Der Freundes-link funktioniert leider nicht, da du nicht aus dem gleichen Land wie dein Freund bist','de-at'=>'Der Freundes-link funktioniert leider nicht, da du nicht aus dem gleichen Land wie dein Freund bist','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'REFERRAL.CHECK.EXISTFAIL','de-de'=>'Dieser Freundes-link existiert nicht','de-ch'=>'Dieser Freundes-link existiert nicht','de-at'=>'Dieser Freundes-link existiert nicht','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'REFERRAL.CHECK.PARAMFAIL','de-de'=>'Der Parameter war nicht gesetzt (dies sollte nie passieren)','de-ch'=>'Der Parameter war nicht gesetzt (dies sollte nie passieren)','de-at'=>'Der Parameter war nicht gesetzt (dies sollte nie passieren)','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);

        Translation::create(['key-key' => 'VOUCHER.CHECK.START_VALUE_SUCCESS','de-de'=>'Bei deiner Bestellung werden ','de-ch'=>'Bei deiner Bestellung werden ','de-at'=>'Bei deiner Bestellung werden ','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'VOUCHER.CHECK.END_VALUE_SUCCESS','de-de'=>' Rabatt abgezogen.','de-ch'=>' Rabatt abgezogen.','de-at'=>' Rabatt abgezogen.','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'VOUCHER.CHECK.START_PRODUCT_SUCCESS','de-de'=>'Bei deiner Bestellung wird ein ','de-ch'=>'Bei deiner Bestellung wird ein ','de-at'=>'Bei deiner Bestellung wird ein ','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'VOUCHER.CHECK.END_PRODUCT_SUCCESS','de-de'=>' hinzugefügt.','de-ch'=>' hinzugefügt.','de-at'=>' hinzugefügt.','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'VOUCHER.CHECK.EXISTFAIL','de-de'=>'Dieser Gutschein-code existiert nicht','de-ch'=>'Dieser Gutschein-code existiert nicht','de-at'=>'Dieser Gutschein-code existiert nicht','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'VOUCHER.CHECK.PARAMFAIL','de-de'=>'Der Parameter war nicht gesetzt (dies sollte nie passieren)','de-ch'=>'Der Parameter war nicht gesetzt (dies sollte nie passieren)','de-at'=>'Der Parameter war nicht gesetzt (dies sollte nie passieren)','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'VOUCHER.CHECK.RESTRICTIONFAIL','de-de'=>'Der Gutschein kann nicht eingelöst werden, da nicht alle Bedinungen erfüllt sind','de-ch'=>'Der Gutschein kann nicht eingelöst werden, da nicht alle Bedinungen erfüllt sind','de-at'=>'Der Gutschein kann nicht eingelöst werden, da nicht alle Bedinungen erfüllt sind','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'VOUCHER.CHECK.COUNTRYFAIL','de-de'=>'Der Gutschein kann nicht eingelöst werden, da er nicht für Deutschland gültig ist','de-ch'=>'Der Gutschein kann nicht eingelöst werden, da er nicht für die Schweiz gültig ist','de-at'=>'Der Gutschein kann nicht eingelöst werden, da er nicht für Österreich gültig ist','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);

        // model
        Schema::create('voucher_restriction_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->timestamps();
        });

        // model
        Schema::create('referral_urls', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('url')->unique();
            $table->boolean('enabled')->nullable()->default(true); // unconfirmed add, makes most sense here
            $table->unsignedInteger('region_id');
            $table->foreign('region_id')->references('id')->on('regions');
            $table->unsignedInteger('share_facebook');
            $table->unsignedInteger('share_twitter');
            $table->unsignedInteger('share_instagram');
            $table->unsignedInteger('share_whatsapp');
            $table->unsignedInteger('share_none');
            $table->unsignedInteger('share_other');
            $table->timestamps();
        });

        // model
        Schema::create('referrals_used', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('referrer_url_id');
            $table->float('amount_referrer');
            $table->float('amount_referree');
            $table->string('currency');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('order_id');
            $table->foreign('order_id')->references('id')->on('orders');
            $table->timestamps();
        });

        Schema::table('profiles', function($table) {
            $table->float('wallet_amount_cents')->after('last_update')->default(0);
            $table->string('wallet_amount_currency')->after('wallet_amount_cents')->default("");
        });

        Schema::table('regions', function($table) {
            $table->float('referrer_reward_cents')->after('region')->default(0);
            $table->float('referree_reward_cents')->after('referrer_reward_cents')->default(0);
        });

        // model
        Schema::create('vouchers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('value_type');
            $table->string('voucher_type')->default('public');
            $table->string('code')->unique();
            $table->float('value');
            $table->string('campaign')->nullable();
            $table->boolean('active')->default(false);
            $table->dateTime('start_at')->nullable();
            $table->dateTime('end_at')->nullable();
            $table->integer('import_counter')->default(0);
            $table->timestamps();
        });

        // model
        Schema::create('voucher_attempts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('status');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('order_id')->nullable();
            $table->foreign('order_id')->references('id')->on('orders');
            $table->unsignedInteger('voucher_id')->nullable();
            $table->timestamps();
        });

        // model
        Schema::create('referral_attempts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('status');
            $table->unsignedInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedInteger('order_id')->nullable();
            $table->foreign('order_id')->references('id')->on('orders');
            $table->unsignedInteger('referral_url_id')->nullable();
            $table->timestamps();
        });

        Schema::create('vouchercategories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('category_voucher', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('voucher_id');
            $table->foreign('voucher_id')->references('id')->on('vouchers');
            $table->unsignedInteger('category_id');
            $table->foreign('category_id')->references('id')->on('vouchercategories');
            $table->timestamps();
        });
        // model
        Schema::create('voucher_restrictions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('voucher_id')->nullable();
            $table->foreign('voucher_id')->references('id')->on('vouchers');
            $table->unsignedInteger('restriction_type_id'); // -> nrestriction_type_list
            $table->foreign('restriction_type_id')->references('id')->on('voucher_restriction_types');
            $table->string('restriction_payload')->nullable(); // old name: product_id_category
            $table->timestamps();
        });
        // model
        Schema::create('voucher_redemptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('voucher_id')->nullable();
            $table->unsignedInteger('order_id')->nullable();
            $table->unsignedInteger('user_id')->nullable(); // redundant; we don't have to search order-table this way (which is big)
            $table->foreign('voucher_id')->references('id')->on('vouchers');
            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('user_id')->references('id')->on('users');
            $table->float('amount_cents')->nullable();
            $table->boolean('invalid')->default(false);
            $table->timestamps();
        });

        // model
        Schema::create('voucher_products', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('voucher_id');
            $table->foreign('voucher_id')->references('id')->on('vouchers');
            $table->unsignedInteger('product_id')->nullable();
            $table->foreign('product_id')->references('id')->on('products');
            $table->timestamps();
        });

        $rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "category";
        $rt->save();

        $rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "product";
        $rt->save();

        // count overall
        $rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "on_order_number";
        $rt->save();

        // count subcription-orders
        $rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "on_subscription_order_number";
        $rt->save();

        $rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "uses_per_user";
        $rt->save();

        $rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "max_total_uses";
        $rt->save();

        $rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "region";
        $rt->save();

        $rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "country";
        $rt->save();

        // SUBSCRIPTION or SINGLE
        $rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "order_type";
        $rt->save();

        // the existence is enough
        $rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "new_user";
        $rt->save();

        // B2B, B2C, VIP -> order_type_id
        $rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "customer_type";
        $rt->save();

        $rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "age";
        $rt->save();

        $rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "first_x_boxes";
        $rt->save();
        $rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "first_x_subscription_boxes";
        $rt->save();
        /*$rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "first_subscription";
        $rt->save();

        $rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "subscription";
        $rt->save();*/

        $rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "min_products_needed";
        $rt->save();

        /* $rt = new \App\Models\VoucherRestrictionType();
        $rt->type = "voucher_code";
        $rt->save(); */

        Voucher::create(['code' => 'SUBSCRIPTION_CH','value_type' => 'percent','voucher_type' => 'system','value' => 11.76,'active' => 1]);
        Voucher::create(['code' => 'SUBSCRIPTION_DE','value_type' => 'percent','voucher_type' => 'system','value' => 10.00,'active' => 1]);
        Voucher::create(['code' => 'SUBSCRIPTION_AT','value_type' => 'percent','voucher_type' => 'system','value' => 7.55,'active' => 1]);

        Voucher::create(['code' => 'FIRST_SUBSCRIPTION_CH','value_type' => 'percent','voucher_type' => 'system','value' => 30.59,'active' => 1]);
        Voucher::create(['code' => 'FIRST_SUBSCRIPTION_DE','value_type' => 'percent','voucher_type' => 'system','value' => 30.00,'active' => 1]);
        Voucher::create(['code' => 'FIRST_SUBSCRIPTION_AT','value_type' => 'percent','voucher_type' => 'system','value' => 30.19,'active' => 1]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function($table) {
            $table->dropColumn('wallet_amount_cents');
            $table->dropColumn('wallet_amount_currency');
        });
        Schema::table('regions', function($table) {
            $table->dropColumn('referrer_reward_cents');
            $table->dropColumn('referree_reward_cents');
        });
        Schema::table('regions', function($table) {
            $table->dropColumn('referrer_reward_cents');
            $table->dropColumn('referree_reward_cents');
        });
        Schema::dropIfExists('voucher_restriction_types');
        Schema::dropIfExists('vouchers');
        Schema::dropIfExists('voucher_attempts');
        Schema::dropIfExists('vouchercategories');
        Schema::dropIfExists('voucher_redemptions');
        Schema::dropIfExists('voucher_products');
        Schema::dropIfExists('voucher_restrictions');
        Schema::dropIfExists('referral_attempts');
        Schema::dropIfExists('referrals_used');
        Schema::dropIfExists('referral_urls');
        Translation::where(['key-key' => 'MAIL.REFEREE-COMPLETED.GIVEBACK'])->first()->delete();
        Translation::where(['key-key' => 'MAIL.REFEREE-COMPLETED.YOURNEXTBOX'])->first()->delete();
        Translation::where(['key-key' => 'MAIL.REFEREE-COMPLETED.CHEAPER'])->first()->delete();
        Translation::where(['key-key' => 'APPMAIL.EMAILREFEREECOMPLETED.SUBJECT'])->first()->delete();
        Translation::where(['key-key' => 'REFERRAL.CHECK.SUCCESS'])->first()->delete();
        Translation::where(['key-key' => 'REFERRAL.CHECK.COUNTRYFAIL'])->first()->delete();
        Translation::where(['key-key' => 'REFERRAL.CHECK.EXISTFAIL'])->first()->delete();
        Translation::where(['key-key' => 'REFERRAL.CHECK.PARAMFAIL'])->first()->delete();
    }
}
