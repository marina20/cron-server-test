<?php
use App\Models\Category;
use App\Models\Translation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertCategoriesDetailsIntoTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->integer('position')->default(0)->after('new');
        });

        $food = Category::where(['name_key' => 'food'])->first();

        $meow = Category::create(['name_key' => 'bundles','category_id' => $food->id, 'new' => 1]);
        $meow->position = 1000;
        $meow->save();
        $meow = Category::where(['name_key' => 'cups'])->first();
        $meow->position = 2000;
        $meow->save();
        $meow = Category::where(['name_key' => 'pouches'])->first();
        $meow->position = 3000;
        $meow->save();
        $meow = Category::where(['name_key' => 'all'])->first();
        $meow->position = 4000;
        $meow->save();

        Translation::insert([
            [
                'key-key'=>'CATEGORIES.'.strtoupper('bundles'),
                'de-de'=>'Produkt-Sets',
                'de-ch'=>'Produkt-Sets',
                'de-at'=>'Produkt-Sets'
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('bundles').'.IMG',
                'de-de'=>'beides.png',
                'de-ch'=>'beides.png',
                'de-at'=>'beides.png'
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('bundles').'.IMG-THUMB',
                'de-de'=>'categories-bundles.png',
                'de-ch'=>'categories-bundles.png',
                'de-at'=>'categories-bundles.png'
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('bundles').'.ALT-TAGS',
                'de-de'=>'Produkt-Sets',
                'de-ch'=>'Produkt-Sets',
                'de-at'=>'Produkt-Sets'
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('bundles').'.DESCRIPTION',
                'de-de'=>'Unsere beliebtesten Produkte als fertige Pakete zusammengestellt und angepasst auf deine Bedürfnisse.',
                'de-ch'=>'Unsere beliebtesten Produkte als fertige Pakete zusammengestellt und angepasst auf deine Bedürfnisse.',
                'de-at'=>'Unsere beliebtesten Produkte als fertige Pakete zusammengestellt und angepasst auf deine Bedürfnisse.'
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('cups'),
                'de-de'=>'Becher',
                'de-ch'=>'Becher',
                'de-at'=>'Becher'
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('cups').'.IMG',
                'de-de'=>'cups.png',
                'de-ch'=>'cups.png',
                'de-at'=>'cups.png'
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('cups').'.IMG-THUMB',
                'de-de'=>'categories-cups.png',
                'de-ch'=>'categories-cups.png',
                'de-at'=>'categories-cups.png'
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('cups').'.ALT-TAGS',
                'de-de'=>'Becher',
                'de-ch'=>'Becher',
                'de-at'=>'Becher'
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('cups').'.DESCRIPTION',
                'de-de'=>'Lecker, frisch und praktisch für unterwegs, die Quetschies für Babies und ältere Kids.',
                'de-ch'=>'Lecker, frisch und praktisch für unterwegs, die Quetschies für Babies und ältere Kids.',
                'de-at'=>'Lecker, frisch und praktisch für unterwegs, die Quetschies für Babies und ältere Kids.'
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('pouches'),
                'de-de'=>'Quetschies',
                'de-ch'=>'Quetschies',
                'de-at'=>'Quetschies'
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('pouches').'.DESCRIPTION',
                'de-de'=>'Frische Breisorten mit vitaminreichen Zutaten für Babies ab 4+ oder 6+ Monaten.',
                'de-ch'=>'Frische Breisorten mit vitaminreichen Zutaten für Babies ab 4+ oder 6+ Monaten.',
                'de-at'=>'Frische Breisorten mit vitaminreichen Zutaten für Babies ab 4+ oder 6+ Monaten.'
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('pouches').'.IMG',
                'de-de'=>'quetschies.png',
                'de-ch'=>'quetschies.png',
                'de-at'=>'quetschies.png'
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('pouches').'.IMG-THUMB',
                'de-de'=>'categories-quetchies.png',
                'de-ch'=>'categories-quetchies.png',
                'de-at'=>'categories-quetchies.png'
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('pouches').'.ALT-TAGS',
                'de-de'=>'Quetschies',
                'de-ch'=>'Quetschies',
                'de-at'=>'Quetschies'
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('all'),
                'de-de'=>'Alle Produkte',
                'de-ch'=>'Alle Produkte',
                'de-at'=>'Alle Produkte'
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('all').'.IMG',
                'de-de'=>'',
                'de-ch'=>'',
                'de-at'=>''
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('all').'.IMG-THUMB',
                'de-de'=>'',
                'de-ch'=>'',
                'de-at'=>''
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('all').'.ALT-TAGS',
                'de-de'=>'Alles',
                'de-ch'=>'Alles',
                'de-at'=>'Alles'
            ],[
                'key-key'=>'CATEGORIES.'.strtoupper('all').'.DESCRIPTION',
                'de-de'=>'',
                'de-ch'=>'',
                'de-at'=>''
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('position');
        });
        Category::where(['name_key' => 'bundles'])->first()->delete();

        Translation::where('key-key','LIKE','CATEGORIES.BUNDLES%')->delete();
        Translation::where('key-key','LIKE','CATEGORIES.POUCHES%')->delete();
        Translation::where('key-key','LIKE','CATEGORIES.CUPS%')->delete();
        Translation::where('key-key','LIKE','CATEGORIES.ALL%')->delete();
    }
}
