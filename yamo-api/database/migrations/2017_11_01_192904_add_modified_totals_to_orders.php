<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModifiedTotalsToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function($table) {
            $table->string('order_subtotal_tax')->nullable()->after('order_total');
            $table->string('order_subtotal')->nullable()->after('order_subtotal_tax');
            $table->string('order_total_without_tax')->nullable()->after('shipping_tax');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function($table) {
            $table->dropColumn('order_subtotal_tax');
            $table->dropColumn('order_subtotal');
            $table->dropColumn('order_total_without_tax');
        });
    }
}
