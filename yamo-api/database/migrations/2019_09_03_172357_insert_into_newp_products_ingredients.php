<?php

use App\Models\Product;
use App\Models\Ingredient;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoNewpProductsIngredients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $pirates = Product::withoutGlobalScope('active')->where('name_key',strtoupper('pirates-of-the-carrotean'))->first();
        $freshPrince = Product::withoutGlobalScope('active')->where('name_key',strtoupper('fresh-prince-of-bel-pear'))->first();
        $applecalypse = Product::withoutGlobalScope('active')->where('name_key',strtoupper('applecalypse-now'))->first();
        $david = Product::withoutGlobalScope('active')->where('name_key',strtoupper('david-zucchetta'))->first();
        $mangoNo5 = Product::withoutGlobalScope('active')->where('name_key',strtoupper('mango-no-5'))->first();
        $beetney = Product::withoutGlobalScope('active')->where('name_key',strtoupper('beetney-spears'))->first();
        $broccoly = Product::withoutGlobalScope('active')->where('name_key',strtoupper('broccoly-balboa'))->first();
        $anthony = Product::withoutGlobalScope('active')->where('name_key',strtoupper('anthony-pumpkins'))->first();
        $sweetHome = Product::withoutGlobalScope('active')->where('name_key',strtoupper('sweet-home-albanana'))->first();
        $cocohontas = Product::withoutGlobalScope('active')->where('name_key',strtoupper('cocohontas'))->first();
        $inbanana = Product::withoutGlobalScope('active')->where('name_key',strtoupper('inbanana-jones'))->first();
        $avocadoDiCaprio = Product::withoutGlobalScope('active')->where('name_key',strtoupper('avocado-di-caprio'))->first();
        $katy = Product::withoutGlobalScope('active')->where('name_key',strtoupper('katy-berry'))->first();
        $peach = Product::withoutGlobalScope('active')->where('name_key',strtoupper('peach-boys'))->first();
        $prince = Product::withoutGlobalScope('active')->where('name_key',strtoupper('prince-vanilliam'))->first();
        $nicki = Product::withoutGlobalScope('active')->where('name_key',strtoupper('nicki-spinaj'))->first();
        $quentin = Product::withoutGlobalScope('active')->where('name_key',strtoupper('quentin-carrotino'))->first();
        $cocofield = Product::withoutGlobalScope('active')->where('name_key',strtoupper('david-cocofield'))->first();
        $berry = Product::withoutGlobalScope('active')->where('name_key',strtoupper('berry-potter'))->first();

        $karotte = Ingredient::where('name_key','INGREDIENTS.KAROTTE')->first();
        $birne = Ingredient::where('name_key','INGREDIENTS.BIRNE')->first();
        $apfel = Ingredient::where('name_key','INGREDIENTS.APFEL')->first();
        $dinkel = Ingredient::where('name_key','INGREDIENTS.DINKEL')->first();
        $zucchini = Ingredient::where('name_key','INGREDIENTS.ZUCCHINI')->first();
        $haferflocken = Ingredient::where('name_key','INGREDIENTS.HAFERFLOCKEN')->first();
        $banane = Ingredient::where('name_key','INGREDIENTS.BANANE')->first();
        $mango = Ingredient::where('name_key','INGREDIENTS.MANGO')->first();
        $linsen = Ingredient::where('name_key','INGREDIENTS.LINSEN')->first();
        $roteBeete = Ingredient::where('name_key','INGREDIENTS.ROTE-BEETE')->first();
        $broccoli = Ingredient::where('name_key','INGREDIENTS.BROCCOLI')->first();
        $grunkohl = Ingredient::where('name_key','INGREDIENTS.GRÜNKOHL')->first();
        $butternusskurbis = Ingredient::where('name_key','INGREDIENTS.BUTTERNUSSKÜRBIS')->first();
        $kartoffel = Ingredient::where('name_key','INGREDIENTS.KARTOFFEL')->first();
        $joghurt = Ingredient::where('name_key','INGREDIENTS.JOGHURT')->first();
        $kichererbsen = Ingredient::where('name_key','INGREDIENTS.KICHERERBSEN')->first();
        $kokosmilch = Ingredient::where('name_key','INGREDIENTS.KOKOSMILCH')->first();
        $spinat = Ingredient::where('name_key','INGREDIENTS.SPINAT')->first();
        $avocado = Ingredient::where('name_key','INGREDIENTS.AVOCADO')->first();
        $erdbeere = Ingredient::where('name_key','INGREDIENTS.ERDBEERE')->first();
        $pfirsich = Ingredient::where('name_key','INGREDIENTS.PFIRSICH')->first();
        $vanille = Ingredient::where('name_key','INGREDIENTS.VANILLE')->first();
        $gurke = Ingredient::where('name_key','INGREDIENTS.GURKE')->first();
        $aprikose = Ingredient::where('name_key','INGREDIENTS.APRIKOSE')->first();
        $susskartoffel = Ingredient::where('name_key','INGREDIENTS.SÜSSKARTOFFEL')->first();
        $hirse = Ingredient::where('name_key','INGREDIENTS.HIRSE')->first();
        $heidelbeeren = Ingredient::where('name_key','INGREDIENTS.HEIDELBEEREN')->first();


        DB::table('products_ingredients')->insert([
            ['product_id'=>$pirates->id,'ingredient_id'=>$karotte->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$freshPrince->id,'ingredient_id'=>$birne->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$applecalypse->id,'ingredient_id'=>$apfel->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$applecalypse->id,'ingredient_id'=>$dinkel->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$david->id,'ingredient_id'=>$karotte->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$david->id,'ingredient_id'=>$zucchini->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$david->id,'ingredient_id'=>$haferflocken->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$mangoNo5->id,'ingredient_id'=>$apfel->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$mangoNo5->id,'ingredient_id'=>$banane->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$mangoNo5->id,'ingredient_id'=>$mango->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$beetney->id,'ingredient_id'=>$linsen->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$beetney->id,'ingredient_id'=>$birne->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$beetney->id,'ingredient_id'=>$roteBeete->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$broccoly->id,'ingredient_id'=>$broccoli->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$broccoly->id,'ingredient_id'=>$banane->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$broccoly->id,'ingredient_id'=>$grunkohl->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$anthony->id,'ingredient_id'=>$butternusskurbis->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$anthony->id,'ingredient_id'=>$kartoffel->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$sweetHome->id,'ingredient_id'=>$joghurt->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$sweetHome->id,'ingredient_id'=>$banane->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$sweetHome->id,'ingredient_id'=>$haferflocken->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$cocohontas->id,'ingredient_id'=>$kichererbsen->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$cocohontas->id,'ingredient_id'=>$kokosmilch->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$cocohontas->id,'ingredient_id'=>$spinat->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$inbanana->id,'ingredient_id'=>$banane->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$inbanana->id,'ingredient_id'=>$mango->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$inbanana->id,'ingredient_id'=>$kokosmilch->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$avocadoDiCaprio->id,'ingredient_id'=>$birne->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$avocadoDiCaprio->id,'ingredient_id'=>$avocado->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$katy->id,'ingredient_id'=>$apfel->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$katy->id,'ingredient_id'=>$erdbeere->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$katy->id,'ingredient_id'=>$roteBeete->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$peach->id,'ingredient_id'=>$pfirsich->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$peach->id,'ingredient_id'=>$apfel->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$peach->id,'ingredient_id'=>$erdbeere->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$prince->id,'ingredient_id'=>$banane->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$prince->id,'ingredient_id'=>$erdbeere->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$prince->id,'ingredient_id'=>$vanille->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$nicki->id,'ingredient_id'=>$apfel->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$nicki->id,'ingredient_id'=>$gurke->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$nicki->id,'ingredient_id'=>$spinat->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$nicki->id,'ingredient_id'=>$banane->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$quentin->id,'ingredient_id'=>$aprikose->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$quentin->id,'ingredient_id'=>$mango->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$quentin->id,'ingredient_id'=>$susskartoffel->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$quentin->id,'ingredient_id'=>$karotte->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$cocofield->id,'ingredient_id'=>$apfel->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$cocofield->id,'ingredient_id'=>$birne->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$cocofield->id,'ingredient_id'=>$kokosmilch->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$cocofield->id,'ingredient_id'=>$hirse->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$berry->id,'ingredient_id'=>$joghurt->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$berry->id,'ingredient_id'=>$birne->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$berry->id,'ingredient_id'=>$heidelbeeren->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$berry->id,'ingredient_id'=>$haferflocken->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('products_ingredients')->truncate();
        Schema::enableForeignKeyConstraints();
    }
}
