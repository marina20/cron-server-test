<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveVoucherifyRedeemedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('voucherify_redeemed');
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('voucherify_redeemed', function (Blueprint $table) {
            $table->increments('id');
            $table->string('redeem_id');
            $table->string('customer_id');
            $table->unsignedInteger('tracking_id');
            $table->foreign('tracking_id')->references('id')->on('users');
            $table->unsignedInteger('order_id');
            $table->foreign('order_id')->references('id')->on('orders');
            $table->string('voucher_id');
            $table->foreign('voucher_id')->references('code')->on('voucherify_vouchers');
            $table->boolean('result');
            $table->float('discount_amount')->nullable();
            $table->string('failure_code')->nullable();
            $table->date('external_created_at');
            $table->date('external_updated_at');
            $table->timestamps();
        });
    }
}
