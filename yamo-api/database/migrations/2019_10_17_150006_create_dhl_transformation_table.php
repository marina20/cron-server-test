<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Postcode;
use Carbon\Carbon;
use App\Models\PostcodesTransformation;


class CreateDhlTransformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postcodes_transformations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('postcode_id');
            $table->foreign('postcode_id')->references('id')->on('postcodes');
            $table->string('alias');
            $table->timestamps();
        });
        $geneve = Postcode::where(['city' => 'Genève'])->get();
        foreach ($geneve as $singleGeneve){
            PostcodesTransformation::create(['postcode_id' => $singleGeneve->id, 'alias' => 'GENEVA']);
        }

        $geneve = Postcode::where(['city' => 'Aarau Rohr'])->where(['postcode' => '5032'])->get();
        foreach ($geneve as $singleGeneve){
            PostcodesTransformation::create(['postcode_id' => $singleGeneve->id, 'alias' => 'Rohr AG']);
        }

        $geneve = Postcode::where(['city' => 'Luzern'])->where(['postcode' => '6014'])->get();
        foreach ($geneve as $singleGeneve){
            PostcodesTransformation::create(['postcode_id' => $singleGeneve->id, 'alias' => 'Littau']);
        }

        $geneve = Postcode::where(['city' => 'Zürich'])->get();
        foreach ($geneve as $singleGeneve){
            PostcodesTransformation::create(['postcode_id' => $singleGeneve->id, 'alias' => 'ZUERICH']);
        }

        $geneve = Postcode::where(['city' => 'Wetzikon ZH'])->where(['postcode' => '8620'])->get();
        foreach ($geneve as $singleGeneve){
            PostcodesTransformation::create(['postcode_id' => $singleGeneve->id, 'alias' => 'Wetzikon ZH 1']);
        }

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        PostcodesTransformation::truncate();
    }
}
