<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductsTableTestbox6Plus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("update products set short_description = '1x Birne, 1x Apfel, Banane, Mango, 1x Joghurt, Banane, Haferflocken, 1x Apfel', characteristics = '{\"characteristics\":[{\"category\":\"for_months\",\"value\":\"6\"},{\"category\":\"ingredients\",\"value\":\"1 x Birne\"},{\"category\":\"ingredients\",\"value\":\"1 x Apfel, Banane, Mango\"},{\"category\":\"ingredients\",\"value\":\"1 x Joghurt, Banane, Haferflocken\"},{\"category\":\"ingredients\",\"value\":\"1 x Apfel\"}]}' where slug = 'testpaket-monate-6-v3';");
        DB::statement("update box_content set product_id = (select id from products where slug = 'applecalypse-now') where box_id = (select id from boxes where product_id = (select id from products where slug = 'testpaket-monate-6-v3' limit 1) limit 1) and product_id = (select id from products where slug = 'cocohontas' limit 1);");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("update products set short_description = '1x Birne, 1x Apfel, Banane, Mango, 1x Joghurt, Banane, Haferflocken, 1x Kichererbsen, Kokosmilch, Spinat', characteristics = '{\"characteristics\":[{\"category\":\"for_months\",\"value\":\"6\"},{\"category\":\"ingredients\",\"value\":\"1 x Birne\"},{\"category\":\"ingredients\",\"value\":\"1 x Apfel, Banane, Mango\"},{\"category\":\"ingredients\",\"value\":\"1 x Joghurt, Banane, Haferflocken\"},{\"category\":\"ingredients\",\"value\":\"1 x Kichererbsen, Kokosmilch, Spinat\"}]}' where slug = 'testpaket-monate-6-v3';");
        DB::statement("update box_content set product_id = (select id from products where slug = 'cocohontas') where box_id = (select id from boxes where product_id = (select id from products where slug = 'testpaket-monate-6-v3' limit 1) limit 1) and product_id = (select id from products where slug = 'applecalypse-now' limit 1);");
    }
}
