<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostcodesIfNotExist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('postcodes')) {
            Schema::create('postcodes', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('country_id');
                $table->foreign('country_id')->references('id')->on('countries');
                $table->string('bundesland');
                $table->string('country');
                $table->string('city');
                $table->string('postcode');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
