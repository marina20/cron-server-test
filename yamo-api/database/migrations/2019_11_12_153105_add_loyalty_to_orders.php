<?php

use App\Models\Product;
use App\Models\Translation;
use App\Models\Voucher;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Order;
use App\Services\LoyaltyProgramme\LoyaltyFacade;
use App\Services\LoyaltyProgramme\OrderBoxNumber;
use App\Services\LoyaltyProgramme\ManageLoyaltyVoucher;
use App\Services\LoyaltyProgramme\ManageLoyaltyTracker;
class AddLoyaltyToOrders extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $orderBoxNumber = new OrderBoxNumber();
        $theDate = \Carbon\Carbon::now();
        $theDate->setDate(2019,11,13);
        $orders = Order::where('delivery_date','>',$theDate)->whereIn('status',[Order::STATUS_PAYED,Order::STATUS_PENDING])->get();
        foreach($orders as $order){
            Helpers::set_locale($order->getCountryCode($order->shipping_country));
            $voucherLoyalty = new ManageLoyaltyVoucher();
            $trackerLoyalty = new ManageLoyaltyTracker();
            if($order->status == Order::STATUS_PAYED && $orderBoxNumber->current($order->user) !== 6){
                $currentBoxNr = $orderBoxNumber->current($order->user);
                $voucher = $voucherLoyalty->get($currentBoxNr);
                if(!empty($voucher)){
                    $res = Voucher::applyVoucher($voucher->code, $order);
                    if($res['success']){
                        $trackerLoyalty->save($order,$voucher,$currentBoxNr);
                    }
                }
            }
            if($order->status == Order::STATUS_PENDING){
                $nextBoxNr = $orderBoxNumber->next($order->user);
                $voucher = $voucherLoyalty->get($nextBoxNr);
                if(!empty($voucher)){
                    $res = Voucher::applyVoucher($voucher->code, $order);
                    if($res['success']){
                        $trackerLoyalty->save($order,$voucher,$nextBoxNr);
                    }
                }

            }
            $order->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
