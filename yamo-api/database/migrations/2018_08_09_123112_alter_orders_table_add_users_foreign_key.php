<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrdersTableAddUsersForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // we don' thave default test account here
        // maybe better solution would be to delete these orders, in that case also order_items should be deleted
        Schema::table('orders', function(Blueprint $table) {
            DB::statement('delete from orders where user_id not in (select id from users)');
            DB::statement('alter table orders modify user_id INT(10) unsigned');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('orders', function(Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        DB::statement('alter table orders modify user_id varchar(255)');
        Schema::enableForeignKeyConstraints();
    }
}
