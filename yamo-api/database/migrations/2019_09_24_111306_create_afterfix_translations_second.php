<?php

use App\Models\Translation;
use App\Models\VatGroup;
use App\Services\CreateOrder\CreateOrder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Models\Order;
use Illuminate\Database\Migrations\Migration;
use App\Traits\FormatMoney;


class CreateAfterfixTranslationsSecond extends Migration
{
    use FormatMoney;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $orders = Order::where('created_at','<','2019-09-25 00:00:00')->where('created_at','>','2019-09-17 00:00:00')->whereNotNull('parent_id')->get();
        foreach($orders as $order){
            $vat = VatGroup::where(['country_id' => $order->country_id, 'category' => 'food'])->first();
            if($order->order_subtotal=='40.00'){
                $order->discount_tax = CreateOrder::calculateVAT(4.00, $vat->value);
                $order->discount = '4.00';
                $order->order_tax = CreateOrder::calculateVAT($order->order_total, $vat->value);
                $order->order_total_without_tax = (float)$order->order_total - (float)$order->total_tax;
                $order->save();
                //if($order->items->isNotEmpty()) {
                    foreach ($order->items as $item) {
                        $item->item_price = "2.25";
                        //echo "DE\n";
                        $discount = 0;
                        if(!empty($order->system_discount)) {
                            $singlePriceSystemDiscount = (float)$order->system_discount / 16;
                            $singlePriceSystemDiscountTax = (float)$order->system_discount_tax / 16;
                            $item->system_discount_tax = $item->qty*$singlePriceSystemDiscountTax;
                            $item->system_discount = $item->qty*$singlePriceSystemDiscount;
                        }
                        $item->total = ((float)$item->item_price * $item->qty);
                        $item->total_tax = CreateOrder::calculateVAT($item->total, $vat->value);
                        $item->save();
                    }
                //}
            } elseif ($order->order_subtotal=='68.00'){

                $order->discount_tax = CreateOrder::calculateVAT(8.00, $vat->value);
                $order->discount = '8.00';
                $order->order_tax = CreateOrder::calculateVAT($order->order_total, $vat->value);
                $order->order_total_without_tax = (float)$order->order_total - (float)$order->total_tax;
                $order->save();
                //if($order->items->isNotEmpty()) {
                    foreach ($order->items as $item) {
                        $item->item_price = "3.75";
                        $discount = 0;
                        //echo "CH\n";
                        if(!empty($order->system_discount)) {
                            $singlePriceSystemDiscount = (float)$order->system_discount / 16;
                            $singlePriceSystemDiscountTax = (float)$order->system_discount_tax / 16;
                            $item->system_discount_tax = $item->qty*$singlePriceSystemDiscountTax;
                            $item->system_discount = $item->qty*$singlePriceSystemDiscount;
                            $discount = $item->system_discount;

                        }
                        $item->total = ((float)$item->item_price * $item->qty);
                        $item->total_tax = CreateOrder::calculateVAT($item->total, $vat->value);
                        $item->save();
                    }
                //}
            } elseif ($order->order_subtotal=='42.40'){

                $order->discount_tax = CreateOrder::calculateVAT(3.20, $vat->value);
                $order->discount = '3.20';
                $order->order_tax = CreateOrder::calculateVAT($order->order_total, $vat->value);
                $order->order_total_without_tax = (float)$order->order_total - (float)$order->total_tax;
                $order->save();
                //if($order->items->isNotEmpty()) {
                    foreach ($order->items as $item) {
                        $item->item_price = "2.45";
                        $discount = 0;
                        //echo "AT\n";
                        if(!empty($order->system_discount)) {
                            $singlePriceSystemDiscount = (float)$order->system_discount / 16;
                            $singlePriceSystemDiscountTax = (float)$order->system_discount_tax / 16;
                            $item->system_discount_tax = $item->qty*$singlePriceSystemDiscountTax;
                            $item->system_discount = $item->qty*$singlePriceSystemDiscount;
                            $discount = $item->system_discount;
                        }
                        $item->total = ((float)$item->item_price * $item->qty);
                        $item->total_tax = CreateOrder::calculateVAT($item->total, $vat->value);
                        $item->save();
                    }
                }
            }
        //}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vat_groups');
    }
}
