<?php

use App\Models\AcEventDefinition;
use App\Models\AcEventLookup;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActiveCampaignTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ac_event_definitions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('event_name')->unique();
            $table->string('event_type');
            $table->text('event_query')->nullable();
            $table->timestamps();
        });

        Schema::create('ac_event_occurred', function (Blueprint $table) {
            $table->increments('id');
            $table->string('event_name');
            $table->string('email');
            //$table->unsignedInteger('user_id');
            //$table->foreign('user_id')->references('id')->on('users');
            $table->dateTime('timestamp');
            $table->boolean('synced')->default(false);
            $table->timestamps();
        });

        Schema::create('ac_event_lookups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('event_name');
            $table->string('tags_add');
            $table->string('tags_remove');
            $table->timestamps();
        });

        AcEventDefinition::create(['event_name' => 'abo_canceller', 'event_type' => 'SQL', 'event_query' => "SELECT 
            email, updated_at as timestamp
            FROM
                (SELECT 
                     s.user_id,
                     s.updated_at,
                     u.created_at user_created_at,
                     cancelled_at subscription_cancelled_at,
                     ns.email,
                     o.billing_first_name,
                     o.billing_last_name,
                     o.billing_country,
                    ROUND((DATEDIFF(DATE(CURDATE()), DATE(o.child_birthday))) / 30, 1) baby_months,
                    COUNT(DISTINCT o.id) tot_orders,
                    COUNT(DISTINCT (IF(s.status = 'pending', s.id, NULL))) pending_subs
                    FROM
                    subscriptions s
                    INNER JOIN users u ON s.user_id = u.id
                    INNER JOIN newsletter_signup ns ON ns.email = u.email
                    INNER JOIN profiles p ON s.user_id = p.user_id
                    INNER JOIN orders o ON s.user_id = o.user_id
                    WHERE
                        1 = 1
                        AND (accepts_newsletter != 0
                        OR accepts_newsletter IS NULL)
                        AND paid_date IS NOT NULL
                        AND DATE(cancelled_at) >= '2019-08-01'             
                    GROUP BY s.user_id
                    HAVING pending_subs=0) a"]);


        AcEventDefinition::create(['event_name' => 'inactive', 'event_type' => 'SQL', 'event_query' => "SELECT 
    email, timestamp
FROM
    (SELECT 
        email,
            SUM(IF(status = 'pending', 1, 0)) active,
            MAX(s.updated_at) timestamp
    FROM
        subscriptions s
    INNER JOIN users u ON s.user_id = u.id
    GROUP BY 1
    HAVING active = 0) a"]);

        AcEventDefinition::create(['event_name' => 'paid_order', 'event_type' => 'SQL', 'event_query' => "SELECT 
    email, timestamp
FROM
    (SELECT 
        billing_email AS email,
            SUM(IF(paid_date IS NOT NULL, 1, 0)) paid_order,
            MAX(created_at) timestamp
    FROM
        orders
    GROUP BY 1
    HAVING paid_order > 0) a"]);

        AcEventLookup::create(['event_name' => 'abo_canceller','tags_add' => 'canceller','tags_remove' => 'other,active']);
        AcEventLookup::create(['event_name' => 'sovendus','tags_add' => 'sovendus','tags_remove' => '']);
        AcEventLookup::create(['event_name' => 'inactive','tags_add' => 'inactive','tags_remove' => 'active']);
        AcEventLookup::create(['event_name' => 'paid_order','tags_add' => 'active','tags_remove' => 'inactive,cart_abandon,cart_abandond_wizard,cart_abandon_shop_single_order,payment_problems,account_only,abo_canceller,other']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ac_event_definitions');
        Schema::drop('ac_event_occurred');
        Schema::drop('ac_event_lookups');
    }
}
