<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostcodeIdColumnsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function($table) {
            $table->integer('billing_postcode_id')->unsigned()->nullable()->after('billing_postcode');
            $table->integer('shipping_postcode_id')->unsigned()->nullable()->after('shipping_postcode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function($table) {
            $table->dropColumn('billing_postcode_id');
            $table->dropColumn('shipping_postcode_id');
        });
    }
}
