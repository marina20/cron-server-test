<?php

use App\Models\Bundle;
use App\Models\BundleRegion;
use App\Models\Product;
use App\Models\BundleMedia;
use App\Models\Box;
use App\Models\BoxContent;
use App\Models\Region;
use App\Models\Media;
use App\Models\Translation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoBundlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $prodFreshPrince = Product::withoutGlobalScope('active')->where(['name_key' => 'fresh-prince-of-bel-pear'])->first();
        $prodApplecalypse = Product::withoutGlobalScope('active')->where(['name_key' => 'applecalypse-now'])->first();
        $prodDavidZucchetta = Product::withoutGlobalScope('active')->where(['name_key' =>  'david-zucchetta'])->first();
        $prodMangoNo5 = Product::withoutGlobalScope('active')->where(['name_key' => 'mango-no-5'])->first();
        $prodBeetneySpears = Product::withoutGlobalScope('active')->where(['name_key' => 'beetney-spears'])->first();
        $prodBroccolyBalboa = Product::withoutGlobalScope('active')->where(['name_key' => 'broccoly-balboa'])->first();
        $prodAnthonyPumpkins = Product::withoutGlobalScope('active')->where(['name_key' => 'anthony-pumpkins'])->first();
        $prodSweetHomeAlbanana = Product::withoutGlobalScope('active')->where(['name_key' => 'sweet-home-albanana'])->first();
        $prodCocohontas = Product::withoutGlobalScope('active')->where(['name_key' => 'cocohontas'])->first();
        $prodInbananaJones = Product::withoutGlobalScope('active')->where(['name_key' => 'inbanana-jones'])->first();
        $prodAvocadoDiCaprio = Product::withoutGlobalScope('active')->where(['name_key' => 'avocado-di-caprio'])->first();
        $prodKatyBerry = Product::withoutGlobalScope('active')->where(['name_key' => 'katy-berry'])->first();
        $prodPeachBoys = Product::withoutGlobalScope('active')->where(['name_key' => 'peach-boys'])->first();
        $prodPrinceVanilliam = Product::withoutGlobalScope('active')->where(['name_key' => 'prince-vanilliam'])->first();
        $prodNicki = Product::withoutGlobalScope('active')->where(['name_key' => 'nicki-spinaj'])->first();
        $prodQuentinCarrotino = Product::withoutGlobalScope('active')->where(['name_key' => 'quentin-carrotino'])->first();
        $prodDavidCocofield = Product::withoutGlobalScope('active')->where(['name_key' => 'david-cocofield'])->first();
        $prodBerryPotter = Product::withoutGlobalScope('active')->where(['name_key' => 'berry-potter'])->first();
        $prodBroccolyBalboa200 = Product::withoutGlobalScope('active')->where(['name_key' => 'broccoly-balboa-200'])->first();
        $prodDavidZucchetta200 = Product::withoutGlobalScope('active')->where(['name_key' => 'david-zucchetta-200'])->first();
        $prodSweetHomeAlbanana200 = Product::withoutGlobalScope('active')->where(['name_key' => 'sweet-home-albanana-200'])->first();

        $box1 = Box::create(['type'=>'custom','status'=>'active']);
        $box2 = Box::create(['type'=>'custom','status'=>'active']);
        $box3 = Box::create(['type'=>'custom','status'=>'active']);
        $box4 = Box::create(['type'=>'custom','status'=>'active']);
        $box5 = Box::create(['type'=>'custom','status'=>'active']);
        $box6 = Box::create(['type'=>'custom','status'=>'active']);

        BoxContent::create(['box_id'=>$box1->id, 'product_id'=>$prodFreshPrince->id, 'quantity'=>4]);
        BoxContent::create(['box_id'=>$box1->id, 'product_id'=>$prodApplecalypse->id, 'quantity'=>4]);
        BoxContent::create(['box_id'=>$box1->id, 'product_id'=>$prodPeachBoys->id, 'quantity'=>4]);
        BoxContent::create(['box_id'=>$box1->id, 'product_id'=>$prodAnthonyPumpkins->id, 'quantity'=>4]);

        BoxContent::create(['box_id'=>$box2->id, 'product_id'=>$prodPeachBoys->id, 'quantity'=>1]);
        BoxContent::create(['box_id'=>$box2->id, 'product_id'=>$prodAnthonyPumpkins->id, 'quantity'=>1]);
        BoxContent::create(['box_id'=>$box2->id, 'product_id'=>$prodDavidZucchetta->id, 'quantity'=>1]);
        BoxContent::create(['box_id'=>$box2->id, 'product_id'=>$prodMangoNo5->id, 'quantity'=>1]);
        BoxContent::create(['box_id'=>$box2->id, 'product_id'=>$prodSweetHomeAlbanana->id, 'quantity'=>1]);
        BoxContent::create(['box_id'=>$box2->id, 'product_id'=>$prodBroccolyBalboa->id, 'quantity'=>1]);
        BoxContent::create(['box_id'=>$box2->id, 'product_id'=>$prodBeetneySpears->id, 'quantity'=>1]);
        BoxContent::create(['box_id'=>$box2->id, 'product_id'=>$prodCocohontas->id, 'quantity'=>1]);
        BoxContent::create(['box_id'=>$box2->id, 'product_id'=>$prodInbananaJones->id, 'quantity'=>1]);
        BoxContent::create(['box_id'=>$box2->id, 'product_id'=>$prodAvocadoDiCaprio->id, 'quantity'=>1]);
        BoxContent::create(['box_id'=>$box2->id, 'product_id'=>$prodKatyBerry->id, 'quantity'=>1]);
        BoxContent::create(['box_id'=>$box2->id, 'product_id'=>$prodPrinceVanilliam->id, 'quantity'=>1]);
        BoxContent::create(['box_id'=>$box2->id, 'product_id'=>$prodNicki->id, 'quantity'=>1]);
        BoxContent::create(['box_id'=>$box2->id, 'product_id'=>$prodQuentinCarrotino->id, 'quantity'=>1]);
        BoxContent::create(['box_id'=>$box2->id, 'product_id'=>$prodDavidCocofield->id, 'quantity'=>1]);
        BoxContent::create(['box_id'=>$box2->id, 'product_id'=>$prodBerryPotter->id, 'quantity'=>1]);

        BoxContent::create(['box_id'=>$box3->id, 'product_id'=>$prodInbananaJones->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box3->id, 'product_id'=>$prodAvocadoDiCaprio->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box3->id, 'product_id'=>$prodKatyBerry->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box3->id, 'product_id'=>$prodPrinceVanilliam->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box3->id, 'product_id'=>$prodNicki->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box3->id, 'product_id'=>$prodQuentinCarrotino->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box3->id, 'product_id'=>$prodDavidCocofield->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box3->id, 'product_id'=>$prodBerryPotter->id, 'quantity'=>2]);

        BoxContent::create(['box_id'=>$box4->id, 'product_id'=>$prodMangoNo5->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box4->id, 'product_id'=>$prodSweetHomeAlbanana->id, 'quantity'=>3]);
        BoxContent::create(['box_id'=>$box4->id, 'product_id'=>$prodBeetneySpears->id, 'quantity'=>3]);
        BoxContent::create(['box_id'=>$box4->id, 'product_id'=>$prodInbananaJones->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box4->id, 'product_id'=>$prodPrinceVanilliam->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box4->id, 'product_id'=>$prodDavidCocofield->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box4->id, 'product_id'=>$prodBerryPotter->id, 'quantity'=>2]);

        BoxContent::create(['box_id'=>$box5->id, 'product_id'=>$prodBeetneySpears->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box5->id, 'product_id'=>$prodCocohontas->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box5->id, 'product_id'=>$prodDavidZucchetta->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box5->id, 'product_id'=>$prodSweetHomeAlbanana->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box5->id, 'product_id'=>$prodBroccolyBalboa200->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box5->id, 'product_id'=>$prodDavidCocofield->id, 'quantity'=>3]);
        BoxContent::create(['box_id'=>$box5->id, 'product_id'=>$prodBerryPotter->id, 'quantity'=>3]);

        BoxContent::create(['box_id'=>$box6->id, 'product_id'=>$prodAnthonyPumpkins->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box6->id, 'product_id'=>$prodDavidZucchetta->id, 'quantity'=>1]);
        BoxContent::create(['box_id'=>$box6->id, 'product_id'=>$prodMangoNo5->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box6->id, 'product_id'=>$prodBroccolyBalboa->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box6->id, 'product_id'=>$prodBeetneySpears->id, 'quantity'=>3]);
        BoxContent::create(['box_id'=>$box6->id, 'product_id'=>$prodCocohontas->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box6->id, 'product_id'=>$prodNicki->id, 'quantity'=>2]);
        BoxContent::create(['box_id'=>$box6->id, 'product_id'=>$prodQuentinCarrotino->id, 'quantity'=>2]);

        $bundle1 = Bundle::create(['box_id'=>$box1->id,'name_key'=>'BEIKOSTSTART-SET','position'=>1000]);
        $bundle2 = Bundle::create(['box_id'=>$box2->id,'name_key'=>'ENTDECKER-SET','position'=>2000]);
        $bundle3 = Bundle::create(['box_id'=>$box3->id,'name_key'=>'QUETSCHIE-SET','position'=>3000]);
        $bundle4 = Bundle::create(['box_id'=>$box4->id,'name_key'=>'BESTSELLER-SET','position'=>4000]);
        $bundle5 = Bundle::create(['box_id'=>$box5->id,'name_key'=>'MAHLZEITEN-SET','position'=>5000]);
        $bundle6 = Bundle::create(['box_id'=>$box6->id,'name_key'=>'VITAMIN-SET','position'=>6000]);

        $regions = Region::get();
        foreach ($regions as $region) {
            BundleRegion::create(['bundle_id'=>$bundle1->id,'region_id'=>$region->id,'active_start_date'=>'2019-11-01 12:12:12','active_end_date'=>'2021-11-01 12:12:12']);
            BundleRegion::create(['bundle_id'=>$bundle2->id,'region_id'=>$region->id,'active_start_date'=>'2019-11-01 12:12:12','active_end_date'=>'2021-11-01 12:12:12']);
            BundleRegion::create(['bundle_id'=>$bundle3->id,'region_id'=>$region->id,'active_start_date'=>'2019-11-01 12:12:12','active_end_date'=>'2021-11-01 12:12:12']);
            BundleRegion::create(['bundle_id'=>$bundle4->id,'region_id'=>$region->id,'active_start_date'=>'2019-11-01 12:12:12','active_end_date'=>'2021-11-01 12:12:12']);
            BundleRegion::create(['bundle_id'=>$bundle5->id,'region_id'=>$region->id,'active_start_date'=>'2019-11-01 12:12:12','active_end_date'=>'2021-11-01 12:12:12']);
            BundleRegion::create(['bundle_id'=>$bundle6->id,'region_id'=>$region->id,'active_start_date'=>'2019-11-01 12:12:12','active_end_date'=>'2021-11-01 12:12:12']);
        }

        $media1 = Media::create(['name_key'=>'IMG-BEIKOSTSTART-SET']);
        $media2 = Media::create(['name_key'=>'IMG-ENTDECKER-SET']);
        $media3 = Media::create(['name_key'=>'IMG-QUETSCHIE-SET']);
        $media4 = Media::create(['name_key'=>'IMG-BESTSELLER-SET']);
        $media5 = Media::create(['name_key'=>'IMG-MAHLZEITEN-SET']);
        $media6 = Media::create(['name_key'=>'IMG-VITAMIN-SET']);

        BundleMedia::create(['media_id'=>$media1->id,'bundle_id'=>$bundle1->id]);
        BundleMedia::create(['media_id'=>$media2->id,'bundle_id'=>$bundle2->id]);
        BundleMedia::create(['media_id'=>$media3->id,'bundle_id'=>$bundle3->id]);
        BundleMedia::create(['media_id'=>$media4->id,'bundle_id'=>$bundle4->id]);
        BundleMedia::create(['media_id'=>$media5->id,'bundle_id'=>$bundle5->id]);
        BundleMedia::create(['media_id'=>$media6->id,'bundle_id'=>$bundle6->id]);


        Translation::create(['key-key' => 'BUNDLES.POUCHES','de-de'=>'Quetschies','de-ch'=>'Quetschies','de-at'=>'Quetschies','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.CUPS','de-de'=>'Becher','de-ch'=>'Becher','de-at'=>'Becher','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);


        // Translations for bundle name and description
        Translation::create(['key-key' => 'BUNDLES.BEIKOSTSTART-SET','de-de'=>'Beikoststart-Set','de-ch'=>'Beikoststart-Set','de-at'=>'Beikoststart-Set','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.BEIKOSTSTART-SET.DESCRIPTION','de-de'=>'Für alle Eltern, die mit Beikost starten und ihrem Kind verschiedene Geschmäcker beibringen möchten','de-ch'=>'Für alle Eltern, die mit Beikost starten und ihrem Kind verschiedene Geschmäcker beibringen möchten','de-at'=>'Für alle Eltern, die mit Beikost starten und ihrem Kind verschiedene Geschmäcker beibringen möchten','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.BEIKOSTSTART-SET.PRODUCT-NAME-COLOR','de-de'=>'#ffffff','de-ch'=>'#ffffff','de-at'=>'#ffffff','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.BEIKOSTSTART-SET.PRODUCT-GRADIENT-COLOR-ONE','de-de'=>'#FED6C2','de-ch'=>'#FED6C2','de-at'=>'#FED6C2','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.BEIKOSTSTART-SET.PRODUCT-GRADIENT-COLOR-TWO','de-de'=>'#E18560','de-ch'=>'#E18560','de-at'=>'#E18560','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-BEIKOSTSTART-SET.IMG','de-de'=>'beikoststart-set.png','de-ch'=>'beikoststart-set.png','de-at'=>'beikoststart-set.png','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-BEIKOSTSTART-SET.IMG-THUMB','de-de'=>'beikoststart-set.png','de-ch'=>'beikoststart-set.png','de-at'=>'beikoststart-set.png','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-BEIKOSTSTART-SET.ALT-TAGS','de-de'=>'Beikoststart-Set','de-ch'=>'Beikoststart-Set','de-at'=>'Beikoststart-Set','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);


        Translation::create(['key-key' => 'BUNDLES.ENTDECKER-SET','de-de'=>'Entdecker-Set','de-ch'=>'Entdecker-Set','de-at'=>'Entdecker-Set','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.ENTDECKER-SET.DESCRIPTION','de-de'=>'Für alle neugierigen Entdecker, die aus dem ganzen yamo-Sortiment probieren möchten','de-ch'=>'Für alle neugierigen Entdecker, die aus dem ganzen yamo-Sortiment probieren möchten','de-at'=>'Für alle neugierigen Entdecker, die aus dem ganzen yamo-Sortiment probieren möchten','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.ENTDECKER-SET.PRODUCT-NAME-COLOR','de-de'=>'#ffffff','de-ch'=>'#ffffff','de-at'=>'#ffffff','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.ENTDECKER-SET.PRODUCT-GRADIENT-COLOR-ONE','de-de'=>'#FECACB','de-ch'=>'#FECACB','de-at'=>'#FECACB','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.ENTDECKER-SET.PRODUCT-GRADIENT-COLOR-TWO','de-de'=>'#F48083','de-ch'=>'#F48083','de-at'=>'#F48083','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-ENTDECKER-SET.IMG','de-de'=>'entdecker-set.png','de-ch'=>'entdecker-set.png','de-at'=>'entdecker-set.png','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-ENTDECKER-SET.IMG-THUMB','de-de'=>'entdecker-set.png','de-ch'=>'entdecker-set.png','de-at'=>'entdecker-set.png','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-ENTDECKER-SET.ALT-TAGS','de-de'=>'Entdecker-Set','de-ch'=>'Entdecker-Set','de-at'=>'Entdecker-Set','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);


        Translation::create(['key-key' => 'BUNDLES.QUETSCHIE-SET','de-de'=>'Quetschie-Set','de-ch'=>'Quetschie-Set','de-at'=>'Quetschie-Set','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.QUETSCHIE-SET.DESCRIPTION','de-de'=>'Für alle, die Quetschies lieben und alle leckeren Sorten testen möchten','de-ch'=>'Für alle, die Quetschies lieben und alle leckeren Sorten testen möchten','de-at'=>'Für alle, die Quetschies lieben und alle leckeren Sorten testen möchten','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.QUETSCHIE-SET.PRODUCT-NAME-COLOR','de-de'=>'#ffffff','de-ch'=>'#ffffff','de-at'=>'#ffffff','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.QUETSCHIE-SET.PRODUCT-GRADIENT-COLOR-ONE','de-de'=>'#FFE96D','de-ch'=>'#FFE96D','de-at'=>'#FFE96D','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.QUETSCHIE-SET.PRODUCT-GRADIENT-COLOR-TWO','de-de'=>'#F2CF4C','de-ch'=>'#F2CF4C','de-at'=>'#F2CF4C','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-QUETSCHIE-SET.IMG','de-de'=>'quetschie-set.png','de-ch'=>'quetschie-set.png','de-at'=>'quetschie-set.png','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-QUETSCHIE-SET.IMG-THUMB','de-de'=>'quetschie-set.png','de-ch'=>'quetschie-set.png','de-at'=>'quetschie-set.png','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-QUETSCHIE-SET.ALT-TAGS','de-de'=>'Quetschie-Set','de-ch'=>'Quetschie-Set','de-at'=>'Quetschie-Set','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);


        Translation::create(['key-key' => 'BUNDLES.BESTSELLER-SET','de-de'=>'Bestseller-Set','de-ch'=>'Bestseller-Set','de-at'=>'Bestseller-Set','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.BESTSELLER-SET.DESCRIPTION','de-de'=>'Ein Paket gefüllt mit den beliebtesten yamo-Sorten. Damit kannst du nicht falsch liegen','de-ch'=>'Ein Paket gefüllt mit den beliebtesten yamo-Sorten. Damit kannst du nicht falsch liegen','de-at'=>'Ein Paket gefüllt mit den beliebtesten yamo-Sorten. Damit kannst du nicht falsch liegen','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.BESTSELLER-SET.PRODUCT-NAME-COLOR','de-de'=>'#ffffff','de-ch'=>'#ffffff','de-at'=>'#ffffff','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.BESTSELLER-SET.PRODUCT-GRADIENT-COLOR-ONE','de-de'=>'#C0B3DF','de-ch'=>'#C0B3DF','de-at'=>'#C0B3DF','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.BESTSELLER-SET.PRODUCT-GRADIENT-COLOR-TWO','de-de'=>'#7E66BC','de-ch'=>'#7E66BC','de-at'=>'#7E66BC','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-BESTSELLER-SET.IMG','de-de'=>'bestseller-set.png','de-ch'=>'bestseller-set.png','de-at'=>'bestseller-set.png','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-BESTSELLER-SET.IMG-THUMB','de-de'=>'bestseller-set.png','de-ch'=>'bestseller-set.png','de-at'=>'bestseller-set.png','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-BESTSELLER-SET.ALT-TAGS','de-de'=>'Bestseller-Set','de-ch'=>'Bestseller-Set','de-at'=>'Bestseller-Set','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);


        Translation::create(['key-key' => 'BUNDLES.MAHLZEITEN-SET','de-de'=>'Mahlzeiten-Set','de-ch'=>'Mahlzeiten-Set','de-at'=>'Mahlzeiten-Set','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.MAHLZEITEN-SET.DESCRIPTION','de-de'=>'Unser Paket für besonders Hungrige Kids. Dank den 200g Bechern wird jeder Frechdachs satt.','de-ch'=>'Unser Paket für besonders Hungrige Kids. Dank den 200g Bechern wird jeder Frechdachs satt.','de-at'=>'Unser Paket für besonders Hungrige Kids. Dank den 200g Bechern wird jeder Frechdachs satt.','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.MAHLZEITEN-SET.PRODUCT-NAME-COLOR','de-de'=>'#ffffff','de-ch'=>'#ffffff','de-at'=>'#ffffff','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.MAHLZEITEN-SET.PRODUCT-GRADIENT-COLOR-ONE','de-de'=>'#FDB9BE','de-ch'=>'#FDB9BE','de-at'=>'#FDB9BE','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.MAHLZEITEN-SET.PRODUCT-GRADIENT-COLOR-TWO','de-de'=>'#B03F46','de-ch'=>'#B03F46','de-at'=>'#B03F46','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-MAHLZEITEN-SET.IMG','de-de'=>'mahlzeiten-set.png','de-ch'=>'mahlzeiten-set.png','de-at'=>'mahlzeiten-set.png','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-MAHLZEITEN-SET.IMG-THUMB','de-de'=>'mahlzeiten-set.png','de-ch'=>'mahlzeiten-set.png','de-at'=>'mahlzeiten-set.png','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-MAHLZEITEN-SET.ALT-TAGS','de-de'=>'Mahlzeiten-Set','de-ch'=>'Mahlzeiten-Set','de-at'=>'Mahlzeiten-Set','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);


        Translation::create(['key-key' => 'BUNDLES.VITAMIN-SET','de-de'=>'Vitamin-Set','de-ch'=>'Vitamin-Set','de-at'=>'Vitamin-Set','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.VITAMIN-SET.DESCRIPTION','de-de'=>'Unser Paket mit der vitaminreichsten Auswahl aller yamo-Breie','de-ch'=>'Unser Paket mit der vitaminreichsten Auswahl aller yamo-Breie.','de-at'=>'Unser Paket mit der vitaminreichsten Auswahl aller yamo-Breie.','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.VITAMIN-SET.PRODUCT-NAME-COLOR','de-de'=>'#ffffff','de-ch'=>'#ffffff','de-at'=>'#ffffff','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.VITAMIN-SET.PRODUCT-GRADIENT-COLOR-ONE','de-de'=>'#D1EAAC','de-ch'=>'#D1EAAC','de-at'=>'#D1EAAC','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'BUNDLES.VITAMIN-SET.PRODUCT-GRADIENT-COLOR-TWO','de-de'=>'#94B659','de-ch'=>'#94B659','de-at'=>'#94B659','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-VITAMIN-SET.IMG','de-de'=>'vitamin-set.png','de-ch'=>'vitamin-set.png','de-at'=>'vitamin-set.png','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-VITAMIN-SET.IMG-THUMB','de-de'=>'vitamin-set.png','de-ch'=>'vitamin-set.png','de-at'=>'vitamin-set.png','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'MEDIA.IMG-VITAMIN-SET.ALT-TAGS','de-de'=>'Vitamin-Set','de-ch'=>'Vitamin-Set','de-at'=>'Vitamin-Set','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('bundles')->truncate();
        DB::table('bundles_regions')->truncate();
        DB::table('bundles_media')->truncate();
        Schema::enableForeignKeyConstraints();

        Translation::where('key-key','LIKE','%BEIKOSTSTART-SET%')->delete();
        Translation::where('key-key','LIKE','%ENTDECKER-SET%')->delete();
        Translation::where('key-key','LIKE','%QUETSCHIE-SET%')->delete();
        Translation::where('key-key','LIKE','%BESTSELLER-SET%')->delete();
        Translation::where('key-key','LIKE','%MAHLZEITEN-SET%')->delete();
        Translation::where('key-key','LIKE','%VITAMIN-SET%')->delete();
        Translation::where('key-key','LIKE','BUNDLES.BEIKOSTSTART-SET%')->delete();
        Translation::where('key-key','LIKE','BUNDLES.ENTDECKER-SET%')->delete();
        Translation::where('key-key','LIKE','BUNDLES.QUETSCHIE-SET%')->delete();
        Translation::where('key-key','LIKE','BUNDLES.BESTSELLER-SET%')->delete();
        Translation::where('key-key','LIKE','BUNDLES.MAHLZEITEN-SET%')->delete();
        Translation::where('key-key','LIKE','BUNDLES.VITAMIN-SET%')->delete();
        Translation::where('key-key','LIKE','BUNDLES.POUCHES')->delete();
        Translation::where('key-key','LIKE','BUNDLES.CUPS')->delete();
        Translation::where('key-key','LIKE','MEDIA.IMG-BEIKOSTSTART-SET%')->delete();
        Translation::where('key-key','LIKE','MEDIA.IMG-ENTDECKER-SET%')->delete();
        Translation::where('key-key','LIKE','MEDIA.IMG-QUETSCHIE-SET%')->delete();
        Translation::where('key-key','LIKE','MEDIA.IMG-BESTSELLER-SET%')->delete();
        Translation::where('key-key','LIKE','MEDIA.IMG-MAHLZEITEN-SET%')->delete();
        Translation::where('key-key','LIKE','MEDIA.IMG-VITAMIN-SET%')->delete();
    }
}
