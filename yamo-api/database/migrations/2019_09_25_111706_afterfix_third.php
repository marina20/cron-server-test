<?php

use App\Models\Translation;
use App\Models\VatGroup;
use App\Services\CreateOrder\CreateOrder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Models\Order;
use Illuminate\Database\Migrations\Migration;
use App\Traits\FormatMoney;


class AfterfixThird extends Migration
{
    use FormatMoney;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $orders = Order::where('created_at','<','2019-09-21 00:00:00')->where('created_at','>','2019-09-17 00:00:00')->whereNotNull('parent_id')->get();
        foreach($orders as $order){
            $vat = VatGroup::where(['country_id' => $order->country_id, 'category' => 'food'])->first();
            if($order->order_subtotal=='40.00'){
                //if($order->items->isNotEmpty()) {
                    foreach ($order->items as $item) {
                        $item->total_discount = $this->formatMoney((float)$item->system_discount+(float)$item->wallet_discount+(float)$item->public_discount);
                        $item->total_discount_tax = $this->formatMoney(CreateOrder::calculateVAT($item->total_discount, $vat->value));
                        $item->save();
                    }
                //}
            } elseif ($order->order_subtotal=='68.00'){
                foreach ($order->items as $item) {
                    $item->total_discount = $this->formatMoney((float)$item->system_discount+(float)$item->wallet_discount+(float)$item->public_discount);
                    $item->total_discount_tax = $this->formatMoney(CreateOrder::calculateVAT($item->total_discount, $vat->value));
                    $item->save();
                }
                //}
            } elseif ($order->order_subtotal=='42.40'){
                foreach ($order->items as $item) {
                    $item->total_discount = $this->formatMoney((float)$item->system_discount+(float)$item->wallet_discount+(float)$item->public_discount);
                    $item->total_discount_tax = $this->formatMoney(CreateOrder::calculateVAT($item->total_discount, $vat->value));
                    $item->save();
                }
                }
            }
        //}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vat_groups');
    }
}
