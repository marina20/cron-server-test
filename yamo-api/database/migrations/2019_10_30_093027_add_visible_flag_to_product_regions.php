<?php


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddvisibleFlagToProductRegions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products_regions', function($table) {
            // it's product_visible cause visible-only is a predefined property for models
            $table->boolean('product_visible')->after('out_of_stock')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products_regions', function (Blueprint $table) {
            // it's product_visible cause visible-only is a predefined property for models
            $table->dropColumn('product_visible');
        });
    }
}
