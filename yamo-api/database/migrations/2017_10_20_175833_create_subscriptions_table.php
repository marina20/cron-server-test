<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id')->nullable();
            $table->string('billing_period')->nullable();
            $table->string('billing_interval')->nullable();
            $table->string('suspension_count')->nullable();
            $table->string('cancelled_email_sent')->nullable();
            $table->string('requires_manual_renewal')->nullable();
            $table->string('schedule_next_payment')->nullable();
            $table->string('schedule_cancelled')->nullable();
            $table->string('schedule_end')->nullable();
            $table->string('initial_transaction_recurring')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
