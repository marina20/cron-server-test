<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAltTagsInTableTestimonials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('testimonials', function (Blueprint $table) {
            DB::statement("update testimonials set image_alt_tag_key = 'ISABELL_BEN',image_alt_tag_text = 'Testimonial & Erfahrungsbericht Yamo von Isabell Ben Nescher', updated_at = now() where id = 1;");
            DB::statement("update testimonials set image_alt_tag_key = 'CECILE_IMFELD',image_alt_tag_text = 'Testimonial & Erfahrungsbericht  Yamo von Cécile Imfeld', updated_at = now() where id = 2;");
            DB::statement("update testimonials set image_alt_tag_key = 'STEFANIE_BURGE',image_alt_tag_text = 'Testimonial & Erfahrungsbericht  Yamo von Stefanie Bürge', updated_at = now() where id = 3;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('testimonials', function (Blueprint $table) {
            DB::statement("update testimonials set image_alt_tag_key = null,image_alt_tag_text = null, updated_at = now() where id = 1;");
            DB::statement("update testimonials set image_alt_tag_key = null,image_alt_tag_text = null, updated_at = now() where id = 2;");
            DB::statement("update testimonials set image_alt_tag_key = null,image_alt_tag_text = null, updated_at = now() where id = 3;");
        });
    }
}
