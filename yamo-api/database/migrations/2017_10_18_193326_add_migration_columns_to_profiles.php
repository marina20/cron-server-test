<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMigrationColumnsToProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function($table) {
            $table->string('bc_capabilities')->after('billing_phone');
            $table->string('bc_user_level')->after('bc_capabilities');
            $table->string('last_update')->after('bc_user_level');
            $table->string('shipping_method')->after('last_update');
                $table->string('_order_count')->after('shipping_method');
            $table->string('_money_spent')->after('_order_count');
                $table->string('billing_delivery')->after('_money_spent');
            $table->string('billing_state')->after('billing_delivery');
            $table->string('shipping_state')->after('billing_state');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function($table) {
            $table->dropColumn('bc_capabilities');
            $table->dropColumn('bc_user_level');
            $table->dropColumn('last_update');
            $table->dropColumn('shipping_method');
            $table->dropColumn('_order_count');
            $table->dropColumn('_money_spent');
            $table->dropColumn('billing_delivery');
            $table->dropColumn('billing_state');
            $table->dropColumn('shipping_state');
        });
    }
}
