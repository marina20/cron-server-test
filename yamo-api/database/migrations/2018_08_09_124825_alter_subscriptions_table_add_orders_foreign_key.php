<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSubscriptionsTableAddOrdersForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function(Blueprint $table) {
            DB::statement("delete from subscriptions where order_id not in (select id from orders)");
            DB::statement('alter table subscriptions modify order_id INT(10) unsigned');
            $table->foreign('order_id')->references('id')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('subscriptions', function(Blueprint $table) {
            $table->dropForeign(['order_id']);
        });
        DB::statement('alter table subscriptions modify order_id varchar(255)');
        Schema::enableForeignKeyConstraints();
    }
}
