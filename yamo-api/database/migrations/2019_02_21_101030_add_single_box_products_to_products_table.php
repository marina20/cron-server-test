<?php

use App\Models\Product;
use App\Models\Price;
use App\Models\Box;
use App\Models\BoxContent;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSingleBoxProductsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $piratesOfCarroteanId = DB::table('products')->insertGetId([
            'category' => 'breil',
            'product_identifier' => 'R',
            'name' => 'Pirates of the Carrotean',
            'slug' => 'pirates-of-the-carrotean',
            'image' => 'pirates-of-the-carrotean.png',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"4"},{"category":"weight","value":"118g"},{"category":"characteristic","value":"Stärkende Süsse"},{"category":"characteristic","value":"Nachmittagsbrei"},{"category":"characteristic","value":"Zum kühl geniessen"},{"category":"ingredients","value":"Karotte"},{"category":"product_name_color","value":"#D0865D"}, {"category":"information","value":"Arrr, dieser kleine Halunke lässt Skorbut keine Chance. Trotz seiner kaltblütigen Namensgeber verträgt sich unser Karottenbrei sehr gut mit allerlei Leichtmatrosen ab dem 4. Monat. Für Mütter, die Johnny Depp gern etwas näher kommen würden"},{"category":"time_of_day","value":"Mittagsbrei"}, {"category":"prep_type","value":"Kann im Wasserbad erwärmt werden"},{"category":"ingredients_description","value":"Karotte 70%, Weisser Traubensaft, Wasser, Rapsöl 2%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"229 kJ/ 55 kcal","indentation":""},{"name":"Fett","value":"2,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,20 g","indentation":"true"},{"name":"Kohlenhydrate","value":"6,2 g","indentation":""},{"name":"davon Zucker","value":"6,0 g","indentation":"true"},{"name":"Eiweiss","value":"0,6 g","indentation":""},{"name":"Salz","value":"< 0,1 g","indentation":""}]}]}',
            'position' => '10300'
            ]);

        $freshPrinceOfBelPearId = DB::table('products')->insertGetId([
            'category' => 'breil',
            'product_identifier' => 'B',
            'name' => 'Fresh Prince of Bel Pear',
            'slug' => 'fresh-prince-of-bel-pear',
            'image' => 'fresh-prince-of-bel-pear.png',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"4"},{"category":"produce_type","value":"fruchtig"},{"category":"weight","value":"118g"},{"category":"characteristic","value":"Stärkende Süsse"},{"category":"characteristic","value":"Nachmittagsbrei"},{"category":"characteristic","value":"Zum kühl geniessen"},{"category":"ingredients","value":"Birne"},{"category":"product_name_color","value":"#D0865D"}, {"category":"information","value":"Dieser kleine Rebell hat\'s Faustdick hinter der Birne. Er zeichnet sich nicht nur durch seine stärkende Süsse aus, sondern trumpft auch noch mit seinem hohen Kalium und Eisen auf. Besonders “cool” wirkt er am Nachmittag."},{"category":"time_of_day","value":"Nachmittagsbrei"}, {"category":"prep_type","value":"Zum Kühl geniessen"},{"category":"ingredients_description","value":"Birne 99%, Fruchtpulver der Acerola-Kirsche. Kann Gluten, Milch und Sellerie enthalten."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"234 kJ/ 56 kcal","indentation":""},{"name":"Fett","value":"0,3 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,0 g","indentation":"true"},{"name":"Kohlenhydrate","value":"11,8 g","indentation":""},{"name":"davon Zucker","value":"11,7 g","indentation":"true"},{"name":"Eiweiss","value":"0,4 g","indentation":""},{"name":"Salz","value":"0,00 g","indentation":""}]}]}',
            'position' => '10100'
        ]);

        $applecalypseNowId = DB::table('products')->insertGetId([
            'category' => 'breil',
            'product_identifier' => 'A',
            'name' => 'Applecalypse Now',
            'slug' => 'applecalypse-now',
            'image' => 'applecalypse-now.png',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"4"},{"category":"produce_type","value":"fruchtig"},{"category":"weight","value":"118g"},{"category":"characteristic","value":"Sättigender Fruchtgenuss"},{"category":"characteristic","value":"Abendbrei"},{"category":"characteristic","value":"Zum kühl geniessen"},{"category":"ingredients","value":"Apfel"},{"category":"ingredients","value":"Dinkel"},{"category":"product_name_color","value":"#D4B178"}, {"category":"information","value":"Dieser Verfechter des gesunden Essens ist eine regelrechte Wunderwaffe. Zu seinem Arsenal zählen hohe Werte an Proteinen, Eisen, Magnesium. Am Besten nähert man sich ihm Abends, wenn die Luft abgekühlt ist."},{"category":"time_of_day","value":"Abendbrei"}, {"category":"prep_type","value":"Zum Kühl geniessen"},{"category":"ingredients_description","value":"Apfel 84%, Wasser, Dinkelmehl 3%,Fruchtpulver der Acerola-Kirsche. Enthält Gluten, kann Milch und Sellerie enthalten."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"263 kJ/ 63 kcal","indentation":""},{"name":"Fett","value":"0,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,10 g","indentation":"true"},{"name":"Kohlenhydrate","value":"12,7 g","indentation":""},{"name":"davon Zucker","value":"9,6 g","indentation":"true"},{"name":"Eiweiss","value":"1,0 g","indentation":""},{"name":"Salz","value":"0,01 g","indentation":""}]}]}',
            'position' => '10200'
        ]);

        $davidZucchetaId = DB::table('products')->insertGetId([
            'category' => 'breil',
            'product_identifier' => 'Z',
            'name' => 'David Zucchetta',
            'slug' => 'david-zucchetta',
            'image' => 'david-zucchetta.png',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"6"},{"category":"produce_type","value":"gemüsig"},{"category":"weight","value":"118g"},{"category":"characteristic","value":"Abwechslungsreicher Gemüsebrei"},{"category":"characteristic","value":"Mittagsbrei"},{"category":"characteristic","value":"Kann im Wasserbad erwärmt werden"},{"category":"ingredients","value":"Karotte"},{"category":"ingredients","value":"Zucchini"},{"category":"ingredients","value":"Haferflocken"},{"category":"product_name_color","value":"#D9A57E"}, {"category":"information","value":"Nicht nur mit seinen Beats sondern auch mit seinen phänomenalen Inhalten überzeugt dieser Brei von sich. Der frische Geschmack weckt “Memories” an früher, bringt aber auch Abwechslung zu gewöhnlichen Gemüsebreien. Besonders lecker schmeckt dieses Brei DJ Set leicht erwärmt und “before the sun goes down”."},{"category":"time_of_day","value":"Mittagsbrei"}, {"category":"prep_type","value":"Kann im Wasserbad erwärmt werden"},{"category":"ingredients_description","value":"Karotte 58%, Zucchini 10%, Wasser, weisser Traubensaft, Haferflocken 5%, Limettensaft Rapsöl 3%. Enthält Gluten, kann Milch und Sellerie enthalten."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"317 kJ/ 76 kcal","indentation":""},{"name":"Fett","value":"3,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,28 g","indentation":"true"},{"name":"Kohlenhydrate","value":"8,6 g","indentation":""},{"name":"davon Zucker","value":"5,7 g","indentation":"true"},{"name":"Eiweiss","value":"1,4 g","indentation":""},{"name":"Salz","value":"0,05 g","indentation":""}]}]}',
            'position' => '9400'
        ]);

        $mangoNo5Id = DB::table('products')->insertGetId([
            'category' => 'breil',
            'product_identifier' => 'M',
            'name' => 'Mango No. 5',
            'slug' => 'mango-no-5',
            'image' => 'mango-no-5.png',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"6"},{"category":"weight","value":"118g"},{"category":"characteristic","value":"Exotischer Vitaminmix"},{"category":"characteristic","value":"Nachmittagsbrei"},{"category":"characteristic","value":"Zum kühl geniessen"},{"category":"ingredients","value":"Apfel"},{"category":"ingredients","value":"Banane"},{"category":"ingredients","value":"Mango"},{"category":"product_name_color","value":"#FDB933"}, {"category":"information","value":"Ladies and Gentlemen, this is Mango No. 5! Diesem Casanova kann kaum jemand widerstehen. Mit seiner fruchtigen Süsse wickelt er auch Erwachsene um den Finger. Besonders verführerisch wirkt er gekühlt an einem heissen Sommernachmittag."},{"category":"time_of_day","value":"Nachmittagsbrei"}, {"category":"prep_type","value":"Zum Kühl geniessen"},{"category":"ingredients_description","value":"Apfel 49%, Mango 21%, Banane 20%, Fruchtpulver der Acerola-Kirsche. Kann Gluten, Milch und Sellerie enthalten."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"248 kJ/ 59 kcal","indentation":""},{"name":"Fett","value":"0,3 g","indentation":""},{"name":"davon gesättige Fettsäuren**","value":"0,09 g","indentation":"true"},{"name":"Kohlenhydrate", "value":"13,3 g", "indentation":""},{"name":"davon Zucker", "value":"12,7 g", "indentation":"true"},{"name":"Eiweiss", "value":"0,6 g", "indentation":""},{"name":"Salz", "value":"0,02 g", "indentation":""}]}]}',
            'position' => '9000'
        ]);

        $beetneySpearsId = DB::table('products')->insertGetId([
            'category' => 'breil',
            'product_identifier' => 'L',
            'name' => 'Beetney Spears',
            'slug' => 'beetney-spears',
            'image' => 'beetney-spears.png',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"6"},{"category":"produce_type","value":"gemüsig"},{"category":"weight","value":"118g"},{"category":"characteristic","value":"Natürliche Proteine und Eisen"},{"category":"characteristic","value":"Mittagsbrei"},{"category":"characteristic","value":"Kann im Wasserbad erwärmt werden"},{"category":"ingredients","value":"Linsen"},{"category":"ingredients","value":"Birne"},{"category":"ingredients","value":"Rote Beete"},{"category":"product_name_color","value":"#B25364"}, {"category":"information","value":"Feed me Baby one more time! Aber Achtung, wer ihn unterschätzt macht einen Fehler, denn unsere Beetney ist ein echter Star. Frau Spears tritt am liebsten Mittags und leicht erwärmt auf, so kann sie die Massen von sich überzeugen. Oops I eat it again."},{"category":"time_of_day","value":"Mittagsbrei"}, {"category":"prep_type","value":"Kann im Wasserbad erwärmt werden"},{"category":"ingredients_description","value":"Linsen 31%, Birne 30%, Apfel, Rote Beete 5%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"358 kJ/ 82 kcal","indentation":""},{"name":"Fett","value":"0,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,06 g","indentation":"true"},{"name":"Kohlenhydrate","value":"15,2 g","indentation":""},{"name":"davon Zucker","value":"8,3 g","indentation":"true"},{"name":"Eiweiss","value":"3,7 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]}]}',
            'position' => '9200'
        ]);

        $broccolyBalboaId = DB::table('products')->insertGetId([
            'category' => 'breil',
            'product_identifier' => 'F',
            'name' => 'Beetney Spears',
            'slug' => 'broccoly-balboa',
            'image' => 'broccoly-balboa.png',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"6"},{"category":"produce_type","value":"gemüsig"},{"category":"weight","value":"118g"},{"category":"characteristic","value":"Leckerer Grüngenuss"},{"category":"characteristic","value":"Mittagsbrei"},{"category":"characteristic","value":"Kann im Wasserbad erwärmt werden"},{"category":"ingredients","value":"Broccoli"},{"category":"ingredients","value":"Banane"},{"category":"ingredients","value":"Grünkohl"},{"category":"product_name_color","value":"#8F9834"}, {"category":"information","value":"Jeder Champion fängt mal klein an. Er ist vielleicht ein wenig Grün hinter den Blättern aber voller Energie und nicht K.O zu kriegen. Im Gegensatz zu seinem Namensvetter hilft er bei Knochenaufbau und nicht -abbau. Eine besonders gute Figur macht er Mittags im Ring, wenn er schonend erwärmt wird."},{"category":"time_of_day","value":"Mittagsbrei"}, {"category":"prep_type","value":"Kann im Wasserbad erwärmt werden"},{"category":"ingredients_description","value":"Broccoli 33%, Banane 29%, Birne, Grünkohl 5%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"246 kJ/ 59 kcal","indentation":""},{"name":"Fett","value":"0,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,09 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,9 g","indentation":""},{"name":"davon Zucker","value":"9,9 g","indentation":"true"},{"name":"Eiweiss","value":"1,6 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]}]}',
            'position' => '9300'
        ]);

        $anthonyPumpkinsId = DB::table('products')->insertGetId([
            'category' => 'breil',
            'product_identifier' => 'P',
            'name' => 'Anthony Pumpkins',
            'slug' => 'anthony-pumpkins',
            'image' => 'anthony-pumpkins.png',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"4"},{"category":"produce_type","value":"gemüsig"},{"category":"weight","value":"118g"},{"category":"characteristic","value":"Sättigende Stärkung"},{"category":"characteristic","value":"Mittagsbrei"},{"category":"characteristic","value":"Kann im Wasserbad erwärmt werden"},{"category":"ingredients","value":"Butternusskürbis"},{"category":"ingredients","value":"Kartoffel"},{"category":"product_name_color","value":"#E79D17"}, {"category":"information","value":"Unser Brei mit dem furchteinflössenden Grinsen wird am Besten bei Tageslicht am Mittag verzehrt. So entfaltet er seine gut sättigende Wirkung. Trotz seiner Stärke ist dieser Brei am Boden geblieben und macht auch aus deinem Baby keinen Kannibalen."},{"category":"time_of_day","value":"Mittagsbrei"}, {"category":"prep_type","value":"Kann im Wasserbad erwärmt werden"},{"category":"ingredients_description","value":"Kürbis 56%, Apfel, Kartoffel, Rapsöl 3%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"300 kJ/ 72 kcal","indentation":""},{"name":"Fett","value":"3,2 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,28 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,8 g","indentation":""},{"name":"davon Zucker","value":"4,3 g","indentation":"true"},{"name":"Eiweiss","value":"0,7 g","indentation":""},{"name":"Salz","value":"0,01 g","indentation":""}]}]}',
            'position' => '10000'
        ]);

        $sweetHomeAlbananaId = DB::table('products')->insertGetId([
            'category' => 'breil',
            'product_identifier' => 'H',
            'name' => 'Sweet Home Albanana',
            'slug' => 'sweet-home-albanana',
            'image' => 'sweet-home-albanana.png',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"6"},{"category":"produce_type","value":"fruchtig"},{"category":"weight","value":"118g"},{"category":"characteristic","value":"Porridge für Babys"},{"category":"characteristic","value":"Morgenbrei"},{"category":"characteristic","value":"Zum kühl geniessen"},{"category":"ingredients","value":"Joghurt"},{"category":"ingredients","value":"Banane"},{"category":"ingredients","value":"Haferflocken"},{"category":"product_name_color","value":"#CEBCA5"}, {"category":"information","value":"Zurück aufs Land entführt dich unser Karaoke-Liebling. Ob kühl oder leicht erwärmt, der Start in den Tag gelingt dir mit diesem gut sättigenden Brei alle Mal. Der frische Joghurt sorgt für ein authentisches Gefühl wie auf dem Bauernhof."},{"category":"time_of_day","value":"Morgenbrei"}, {"category":"prep_type","value":"Zum Kühl geniessen"},{"category":"ingredients_description","value":"Joghurt 38%, Banane 38%, Apfel, Haferflocken 8%, Zimt. Enthält Gluten, enthält Milch. Kann Sellerie enthalten."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"420 kJ/ 100 kcal","indentation":""},{"name":"Fett","value":"2,1 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,9 g","indentation":"true"},{"name":"Kohlenhydrate","value":"16,1 g","indentation":""},{"name":"davon Zucker","value":"10,6 g","indentation":"true"},{"name":"Eiweiss","value":"3,0 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]}]}',
            'position' => '9100'
        ]);

        $cocohontasId = DB::table('products')->insertGetId([
            'category' => 'breil',
            'product_identifier' => 'S',
            'name' => 'Cocohontas',
            'slug' => 'cocohontas',
            'image' => 'cocohontas.png',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"6"},{"category":"produce_type","value":"fruchtig"},{"category":"weight","value":"118g"},{"category":"characteristic","value":"Exotischer Proteinmix"},{"category":"characteristic","value":"Mittagsbrei"},{"category":"characteristic","value":"Kann im Wasserbad erwärmt werden"},{"category":"ingredients","value":"Kichererbsen"},{"category":"ingredients","value":"Kokosmilch"},{"category":"ingredients","value":"Spinat"},{"category":"product_name_color","value":"#B3C192"}, {"category":"information","value":"Unsere exotische Schönheit erwacht am besten mittags, leicht erwärmt. Das Beste von Fern und Nah vereint in einem Brei. So wie Kokosnussmilch, welche reich an guten Fetten ist, die eisen- und proteinreichen Kichererbsen und der nahrhafte Spinat."},{"category":"time_of_day","value":"Mittagsbrei"}, {"category":"prep_type","value":"Kann im Wasserbad erwärmt werden"},{"category":"ingredients_description","value":"Birne, Kichererbsen 18%, Kokosmilch 18%, Apfel, Spinat 8%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"382 kJ/ 91 kcal","indentation":""},{"name":"Fett","value":"3,9 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"3,1 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,5 g","indentation":""},{"name":"davon Zucker","value":"7,1 g","indentation":"true"},{"name":"Eiweiss","value":"2,3 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]}]}',
            'position' => '9500'
        ]);

        $peachBoysId = DB::table('products')->insertGetId([
            'category' => 'breil',
            'product_identifier' => 'E',
            'name' => 'Peach Boys',
            'slug' => 'peach-boys',
            'image' => 'peach-boys.png',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"6"},{"category":"produce_type","value":"fruchtig"},{"category":"weight","value":"118g"},{"category":"ingredients","value":"Pfirsich"},{"category":"ingredients","value":"Apfel"},{"category":"ingredients","value":"Erdbeere"},{"category":"product_name_color","value":"#F9A980"}, {"category":"information","value":"Die fruchtige Zusammensetzung von Pfirsich, Apfel und Erdbeere lässt die Strand-Melodie in deinem Kopf erklingen. “All Summer long” versorgt er dich mit Vitamin B3, Magnesium, Zink und Ballaststoffen. Danach kann man sich getrost zurücklehnen und den Wellen lauschen."},{"category":"time_of_day","value":"Nachmittagsbrei"}, {"category":"prep_type","value":"Zum Kühl geniessen"},{"category":"ingredients_description","value":"Pfirsich 53%, Apfel 35%, Erdbeere 12%, Fruchtpulver der Acerola-Kirsche. Kann Gluten, Milch und Sellerie enthalten."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"210 kJ/ 50 kcal","indentation":""},{"name":"Fett","value":"0,3 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,04 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,4 g","indentation":""},{"name":"davon Zucker","value":"10,3 g","indentation":"true"},{"name":"Eiweiss","value":"0,5 g","indentation":""},{"name":"Salz","value":"0,01 g","indentation":""}]}]}',
            'position' => '9600'
        ]);

        $inbananaJonesId = DB::table('products')->insertGetId([
            'category' => 'pouch',
            'product_identifier' => 'C',
            'name' => 'Inbanana Jones',
            'slug' => 'inbanana-jones',
            'image' => 'inbanana-jones.png',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"6"},{"category":"produce_type","value":"fruchtig"},{"category":"weight","value":"120g"},{"category":"ingredients","value":"Banane"},{"category":"ingredients","value":"Mango"},{"category":"ingredients","value":"Kokosmilch"},{"category":"product_name_color","value":"#F1CB15"}, {"category":"information","value":"Unser abenteuerlicher Brei mit Hut und Peitsche hat die leckerste Zutatenkombination entstaubt. Als Jäger des verlorenen Schatzes ist er ständig auf Entdeckungsreisen und daher wunderbar zwischendurch geniessbar."},{"category":"time_of_day","value":"Für zwischendurch"}, {"category":"prep_type","value":"Zum Kühl geniessen"},{"category":"ingredients_description","value":"Banane 31%, Mango 30%, Birne 20%, Kokosmilch 14%, Pfirsich 5%. Keine Allergene vorhanden."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"352 kJ/ 84 kcal","indentation":""},{"name":"Fett","value":"2,5 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"1,1 g","indentation":"true"},{"name":"Kohlenhydrate","value":"14 g","indentation":""},{"name":"davon Zucker","value":"12 g","indentation":"true"},{"name":"Eiweiss","value":"0,8 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]}]}',
            'position' => '8000'
        ]);

        $avocadoDiCaprioId = DB::table('products')->insertGetId([
            'category' => 'pouch',
            'product_identifier' => 'D',
            'name' => 'Avocado di Caprio',
            'slug' => 'avocado-di-caprio',
            'image' => 'avocado-di-caprio.png',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"6"},{"category":"produce_type","value":"fruchtig"},{"category":"weight","value":"120g"},{"category":"ingredients","value":"Birne"},{"category":"ingredients","value":"Avocado"},{"category":"product_name_color","value":"#C0C972"}, {"category":"information","value":"Auf der Breitanic ging er unter, bei uns hat er seine leckere Wiederauferstehung. Wie Romeo und Julia vereinen sich Birne und Avocado zu einer wunderbaren Liebesgeschichte. Wie sein Namensvetter, wird dieser Brei noch einige Bab(i)es bezirzen."},{"category":"time_of_day","value":"Für zwischendurch"}, {"category":"prep_type","value":"Zum Kühl geniessen"},{"category":"ingredients_description","value":"Birne 90 %, Avocado 10%. Keine Allergene vorhanden."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"276 kJ/ 66 kcal","indentation":""},{"name":"Fett","value":"1,9 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,4 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10 g","indentation":""},{"name":"davon Zucker","value":"9,5 g","indentation":"true"},{"name":"Eiweiss","value":"<0,5 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]}]}',
            'position' => '8200'
        ]);

        $katyBerryId = DB::table('products')->insertGetId([
            'category' => 'pouch',
            'product_identifier' => 'Y',
            'name' => 'Katy Berry',
            'slug' => 'katy-berry',
            'image' => 'katy-berry.png',
            'short_description' => '',
            'characteristics' => '{"characteristics":[{"category":"for_months","value":"6"},{"category":"produce_type","value":"fruchtig"},{"category":"weight","value":"120g"},{"category":"ingredients","value":"Apfel"},{"category":"ingredients","value":"Erdbeere"},{"category":"ingredients","value":"Rote Beete"},{"category":"product_name_color","value":"#BC4056"}, {"category":"information","value":"Nach dem Genuss unserer Katy wirst du sagen: I kissed a Brei and I liked it! Sie schmeckt am besten kühl und nicht Hot N Cold. Und wenn dein Baby fertig ist, wird es genüsslich ROARR schnurren."},{"category":"time_of_day","value":"Für zwischendurch"}, {"category":"prep_type","value":"Zum Kühl geniessen"},{"category":"ingredients_description","value":"Apfel 73 %, Erdbeere 23%, Rote Beete 4%. Keine Allergene vorhanden."}, {"category":"table_of_contents", "value":[{"name":"Energie","value":"187 kJ/ 44 kcal","indentation":""},{"name":"Fett","value":"<0,5 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"<0,1 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10 g","indentation":""},{"name":"davon Zucker","value":"9,5 g","indentation":"true"},{"name":"Eiweiss","value":"<0,5 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]}]}',
            'position' => '8100'
        ]);

        $subscription4PlusProduct = Product::withoutGlobalScope('active')->where('slug','like','%'.Product::PRODUCT_ABO_SLUG_4_MONTHS.'%')
            ->where('category',Product::PRODUCT_CATEGORY_SUBSCRIPTION)->first();
        $subscription4PlusProductId = $subscription4PlusProduct ? $subscription4PlusProduct->id : null;

        $subscription6PlusProduct = Product::withoutGlobalScope('active')->where('slug','like','%'.Product::PRODUCT_ABO_SLUG_6_MONTHS.'%')
            ->where('category',Product::PRODUCT_CATEGORY_SUBSCRIPTION)->first();
        $subscription6PlusProductId = $subscription6PlusProduct ? $subscription6PlusProduct->id : null;

        $single_4_plus_id = DB::table('products')->insertGetId([
            'category'=>'single-box',
            'subscription_product_id'=> $subscription4PlusProductId,
            'name'=>'Einzelbestellung 4+ Monate',
            'short_name'=>'4+ Monate',
            'slug'=>'single-box-monate-4',
            'image'=>'yamobox-newv1.png',
            'short_description'=>'3x Karotte, 5x Birne, 5x Apfel, Dinkel, 3x Butternusskürbis, Kartoffel',
            'characteristics'=>'{"characteristics":[{"category":"for_months","value":"4"},{"category":"ingredients","value":"3 x Karotte"},{"category":"ingredients","value":"5 x Birne"},{"category":"ingredients","value":"5 x Apfel, Dinkel"},{"category":"ingredients","value":"3 x Butternusskürbis, Kartoffel"}]}',
            'price'=>'72.90',
            'tax_percentage'=>'2.5',
            'is_coupon_applicable'=>'1',
            'position'=>'8000',
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()')
        ]);

        $single_6_plus_id = DB::table('products')->insertGetId([
            'category'=>'single-box',
            'subscription_product_id'=> $subscription6PlusProductId,
            'name'=>'Einzelbestellung 6+ Monate',
            'short_name'=>'6+ Monate',
            'slug'=>'single-box-monate-6',
            'image'=>'yamobox-newv1.png',
            'short_description'=>'3x Birne, 2x Karotte, Zucchini, Haferflocken, 3x Apfel, Banane, Mango, 2x Joghurt, Banane, Haferflocken, 2x Broccoli, Banane, Grünkohl, 2x Linse, Birne, Rote Beete, 2x Kichererbsen, Kokosmilch, Spinat',
            'characteristics'=>'{"characteristics":[{"category":"for_months","value":"6"},{"category":"ingredients","value":"3 x Birne"},{"category":"ingredients","value":"2 x Karotte, Zucchini, Haferflocken"},{"category":"ingredients","value":"3 x Apfel, Banane, Mango"},{"category":"ingredients","value":"2 x Joghurt, Banane, Haferflocken"},{"category":"ingredients","value":"2 x Broccoli, Banane, Grünkohl"},{"category":"ingredients","value":"2 x Linse, Birne, Rote Beete"},{"category":"ingredients","value":"2 x Kichererbsen, Kokosmilch, Spinat"}]}',
            'price'=>'72.90',
            'tax_percentage'=>'2.5',
            'is_coupon_applicable'=>'1',
            'position'=>'8000',
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()')
        ]);

        $single_custom_id = DB::table('products')->insertGetId([
            'category'=>'single-box-custom-box',
            'name'=>'Deine individuelle Box',
            'short_name'=>'Box konfigurieren',
            'slug'=>'single-box-custom-box',
            'image'=>'yamobox-newv1.png',
            'short_description'=>'please select',
            'characteristics'=>'',
            'price'=>'72.90',
            'tax_percentage'=>'2.5',
            'is_coupon_applicable'=>'1',
            'position'=>'8000',
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()')
        ]);

        DB::table('prices')->insert([
            [
                'country_id'=>1,
                'product_id'=>$single_4_plus_id,
                'value'=>'43.50',
                'tax_percentage'=>'7',
                'tax'=>'3.17',
                'shipping'=>'4.90',
                'shipping_tax'=>'0.00',
                'shipping_tax_percentage'=>19,
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'country_id'=>2,
                'product_id'=>$single_4_plus_id,
                'value'=>'72.90',
                'tax_percentage'=>'2.5',
                'tax'=>'2.02',
                'shipping'=>'9.90',
                'shipping_tax'=>'0.00',
                'shipping_tax_percentage'=>'7.7',
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'country_id'=>3,
                'product_id'=>$single_4_plus_id,
                'value'=>'43.90',
                'tax_percentage'=>'10',
                'tax'=>'4.62',
                'shipping'=>'6.90',
                'shipping_tax'=>'0.00',
                'shipping_tax_percentage'=>'20',
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'country_id'=>1,
                'product_id'=>$single_6_plus_id,
                'value'=>'43.50',
                'tax_percentage'=>'7',
                'tax'=>'3.17',
                'shipping'=>'4.90',
                'shipping_tax'=>'0.00',
                'shipping_tax_percentage'=>19,
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'country_id'=>2,
                'product_id'=>$single_6_plus_id,
                'value'=>'72.90',
                'tax_percentage'=>'2.5',
                'tax'=>'2.02',
                'shipping'=>'9.90',
                'shipping_tax'=>'0.00',
                'shipping_tax_percentage'=>'7.7',
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'country_id'=>3,
                'product_id'=>$single_6_plus_id,
                'value'=>'43.90',
                'tax_percentage'=>'10',
                'tax'=>'4.62',
                'shipping'=>'6.90',
                'shipping_tax'=>'0.00',
                'shipping_tax_percentage'=>'20',
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'country_id'=>1,
                'product_id'=>$single_custom_id,
                'value'=>'43.50',
                'tax_percentage'=>'7',
                'tax'=>'3.17',
                'shipping'=>'4.90',
                'shipping_tax'=>'0.00',
                'shipping_tax_percentage'=>19,
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'country_id'=>2,
                'product_id'=>$single_custom_id,
                'value'=>'72.90',
                'tax_percentage'=>'2.5',
                'tax'=>'2.02',
                'shipping'=>'9.90',
                'shipping_tax'=>'0.00',
                'shipping_tax_percentage'=>'7.7',
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'country_id'=>3,
                'product_id'=>$single_custom_id,
                'value'=>'43.90',
                'tax_percentage'=>'10',
                'tax'=>'4.62',
                'shipping'=>'6.90',
                'shipping_tax'=>'0.00',
                'shipping_tax_percentage'=>'20',
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ]
        ]);

        $single_4_plus_box_id = DB::table('boxes')->insertGetId([
            'product_id'=>$single_4_plus_id,
            'type'=>'single-box',
            'status'=>'active',
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()')
        ]);

        $single_6_plus_box_id = DB::table('boxes')->insertGetId([
            'product_id'=>$single_6_plus_id,
            'type'=>'single-box',
            'status'=>'active',
            'created_at' => DB::raw('now()'),
            'updated_at' => DB::raw('now()')
        ]);

        DB::table('box_content')->insert([
            [
                'box_id'=>$single_4_plus_box_id,
                'product_id'=>$piratesOfCarroteanId,
                'quantity'=>3,
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'box_id'=>$single_4_plus_box_id,
                'product_id'=>$freshPrinceOfBelPearId,
                'quantity'=>5,
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'box_id'=>$single_4_plus_box_id,
                'product_id'=>$applecalypseNowId,
                'quantity'=>5,
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'box_id'=>$single_4_plus_box_id,
                'product_id'=>$anthonyPumpkinsId,
                'quantity'=>3,
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'box_id'=>$single_6_plus_box_id,
                'product_id'=>$freshPrinceOfBelPearId,
                'quantity'=>3,
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'box_id'=>$single_6_plus_box_id,
                'product_id'=>$davidZucchetaId,
                'quantity'=>2,
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'box_id'=>$single_6_plus_box_id,
                'product_id'=>$mangoNo5Id,
                'quantity'=>3,
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'box_id'=>$single_6_plus_box_id,
                'product_id'=>$sweetHomeAlbananaId,
                'quantity'=>2,
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'box_id'=>$single_6_plus_box_id,
                'product_id'=>$broccolyBalboaId,
                'quantity'=>2,
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'box_id'=>$single_6_plus_box_id,
                'product_id'=>$beetneySpearsId,
                'quantity'=>2,
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ],
            [
                'box_id'=>$single_6_plus_box_id,
                'product_id'=>$cocohontasId,
                'quantity'=>2,
                'created_at' => DB::raw('now()'),
                'updated_at' => DB::raw('now()')
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $single_box_product_category = 'single-box';
        $single_box_custom_product_category = 'single-box-custom-box';
        $this->deleteProductCompletelyByCategory($single_box_product_category);
        $this->deleteProductCompletelyByCategory($single_box_custom_product_category);

    }

    private function deleteProductCompletelyByCategory($product_category)
    {
        $products = Product::where('category',$product_category)->get();
        foreach ($products as $product) {
            $box = Box::where('product_id',$product->id)->first();
            if($box instanceof Box)
            {
                $box->items()->delete();
                $box->delete();
            }
            Price::where('product_id',$product->id)->delete();
            $product->delete();
        }
    }
}
