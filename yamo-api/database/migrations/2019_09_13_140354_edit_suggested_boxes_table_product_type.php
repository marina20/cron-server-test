<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditSuggestedBoxesTableProductType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("update suggested_boxes set product_type = 'cups' where product_type = 'breil';");
        DB::statement("update suggested_boxes set product_type = 'pouches' where product_type = 'pouch';");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("update suggested_boxes set product_type = 'breil' where product_type = 'cups';");
        DB::statement("update suggested_boxes set product_type = 'pouch' where product_type = 'pouches';");
    }
}
