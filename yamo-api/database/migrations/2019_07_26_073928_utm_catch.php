<?php

use App\Models\Product;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UtmCatch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utm_tracking', function (Blueprint $table) {
            $table->increments('id');
            $table->string('utm_source')->default(null)->nullable();
            $table->string('utm_medium')->default(null)->nullable();
            $table->string('utm_campaign')->default(null)->nullable();
            $table->string('utm_term')->default(null)->nullable();
            $table->string('utm_content')->default(null)->nullable();
            $table->string('type')->default(null)->nullable();
            $table->string('reference_id')->default(null)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('utm_tracking');
    }
}
