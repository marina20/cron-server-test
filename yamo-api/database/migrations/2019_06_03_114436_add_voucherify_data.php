<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVoucherifyData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucherify_vouchers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('voucher_id');
            $table->string('code')->unique();
            $table->string('type');
            $table->boolean('active');
            $table->integer('gift_amount')->nullable();
            $table->integer('gift_balance')->nullable();
            $table->string('discount_type')->nullable();
            $table->integer('discount_amount_off')->comment('This value can be either a percentage or a number depending on discount type.')->nullable();
            $table->integer('reedemmed_counter')->nullable();
            $table->date('external_created_at');
            $table->date('expiration_date')->nullable();
            $table->timestamps();
        });

        Schema::create('voucherify_redeemed', function (Blueprint $table) {
            $table->increments('id');
            $table->string('redeem_id');
            $table->string('customer_id');
            $table->unsignedInteger('tracking_id');
            $table->foreign('tracking_id')->references('id')->on('users');
            $table->unsignedInteger('order_id');
            $table->foreign('order_id')->references('id')->on('orders');
            $table->string('voucher_id');
            $table->foreign('voucher_id')->references('code')->on('voucherify_vouchers');
            $table->boolean('result');
            $table->float('discount_amount')->nullable();
            $table->string('faliure_code')->nullable();
            $table->date('external_created_at');
            $table->date('external_updated_at');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucherify_redeemed');
        Schema::dropIfExists('voucherify_vouchers');
        //
    }
}
