<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChildBirthdayColumnToProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function($table) {
            DB::statement('alter table profiles change column child_birth_date child_birthday VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify gender VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify shipping_title VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify shipping_first_name VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify shipping_last_name VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify shipping_company VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify shipping_address_1 VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify shipping_address_2 VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify shipping_city VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify shipping_postcode VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify shipping_country VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify shipping_email VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify shipping_phone VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify billing_first_name VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify billing_last_name VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify billing_company VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify billing_address_1 VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify billing_address_2 VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify billing_city VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify billing_postcode VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify billing_country VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify billing_email VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify billing_phone VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify bc_capabilities VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify bc_user_level VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify last_update VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify shipping_method VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify _order_count VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify _money_spent VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify billing_delivery VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify billing_state VARCHAR(255) NULL;');
            DB::statement('alter table profiles modify shipping_state VARCHAR(255) NULL;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function($table) {
            DB::statement('alter table profiles change column child_birthday child_birth_date VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify gender VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify shipping_title VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify shipping_first_name VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify shipping_last_name VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify shipping_company VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify shipping_address_1 VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify shipping_address_2 VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify shipping_city VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify shipping_postcode VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify shipping_country VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify shipping_email VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify shipping_phone VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify billing_first_name VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify billing_last_name VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify billing_company VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify billing_address_1 VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify billing_address_2 VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify billing_city VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify billing_postcode VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify billing_country VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify billing_email VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify billing_phone VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify bc_capabilities VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify bc_user_level VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify last_update VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify shipping_method VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify _order_count VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify _money_spent VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify billing_delivery VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify billing_state VARCHAR(255) NOT NULL;');
            DB::statement('alter table profiles modify shipping_state VARCHAR(255) NOT NULL;');
        });
    }
}
