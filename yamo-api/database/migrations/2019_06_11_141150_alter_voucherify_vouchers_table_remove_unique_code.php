<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVoucherifyVouchersTableRemoveUniqueCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voucherify_redeemed', function (Blueprint $table) {
            $table->dropForeign('voucherify_redeemed_voucher_id_foreign');
        });

        Schema::table('voucherify_vouchers', function (Blueprint $table) {
            $table->dropUnique('voucherify_vouchers_code_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voucherify_vouchers', function (Blueprint $table) {
            $table->unique('code');
        });
        Schema::table('voucherify_redeemed', function (Blueprint $table) {
            $table->foreign('voucher_id')->references('code')->on('voucherify_vouchers');
        });
    }
}
