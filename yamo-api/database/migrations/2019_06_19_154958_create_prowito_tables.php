<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProwitoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prowito_sent', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->timestamps();
        });

        Schema::create('prowito_retrieved', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->timestamps();
        });

        Schema::create('prowito_tracking_number', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->text('tracking_number');
            $table->text('shipping_company');
            $table->timestamps();
        });

        Schema::create('prowito_error', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id');
            $table->text('error');
            $table->boolean('error_resolved');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prowito_sent');
        Schema::dropIfExists('prowito_retrieved');
        Schema::dropIfExists('prowito_tracking_number');
        Schema::dropIfExists('prowito_error');
    }
}
