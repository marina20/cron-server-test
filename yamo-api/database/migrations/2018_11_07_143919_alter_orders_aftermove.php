<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrdersAftermove extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
	        DB::statement("UPDATE orders SET delivery_date = DATE_SUB(delivery_date, INTERVAL 1 DAY ) WHERE `billing_country`= 'Deutschland' AND delivery_date > CURDATE() AND DAYOFWEEK(delivery_date)=6;");
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
	        //
        });
    }
}
