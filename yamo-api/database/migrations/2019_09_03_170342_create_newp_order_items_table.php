<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewpOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function (Blueprint $table) {
            $table->dropForeign('order_items_order_id_foreign');
        });

        Schema::rename('order_items','oldp_order_items');
        Schema::create('order_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_name')->nullable();
            $table->string('item_type')->nullable();
            $table->unsignedInteger('order_id');
            $table->foreign('order_id')->references('id')->on('orders');
            $table->unsignedInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products');
            $table->string('qty')->nullable();
            $table->string('item_price')->nullable();
            $table->string('total')->nullable();
            $table->string('total_tax')->nullable();
            $table->string('subtotal')->nullable();
            $table->string('subtotal_tax')->nullable();
            $table->string('total_discount')->nullable();
            $table->string('total_discount_tax')->nullable();
            $table->string('tax_rate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_items');
        Schema::rename('oldp_order_items','order_items');
        Schema::table('order_items', function($table) {
            $table->foreign('order_id')->references('id')->on('orders');
        });
    }
}
