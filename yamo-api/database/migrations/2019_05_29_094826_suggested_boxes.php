<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\BoxContent;
use App\Models\SuggestedBox;
use App\Models\Product;
use App\Models\Box;
use App\Models\Price;

class SuggestedBoxes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suggested_boxes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('children_age');
            $table->string('product_type');
            $table->unsignedInteger('box_id');
            $table->foreign('box_id')->references('id')->on('boxes');

            $table->string('product_content');
            $table->boolean('default_box');
            $table->timestamps();
        });

        $art_pirates_of_the_carrotean = Product::withoutGlobalScope('active')->where(['slug' => 'pirates-of-the-carrotean'])->first();
        $art_fresh_prince_of_bel_pear = Product::withoutGlobalScope('active')->where(['slug' => 'fresh-prince-of-bel-pear'])->first();
        $art_applecalypse_now = Product::withoutGlobalScope('active')->where(['slug' => 'applecalypse-now'])->first();
        $art_david_zucchetta = Product::withoutGlobalScope('active')->where(['slug' =>  'david-zucchetta'])->first();
        $art_mango_nr_five = Product::withoutGlobalScope('active')->where(['slug' => 'mango-no-5'])->first();
        $art_broccoly_balboa = Product::withoutGlobalScope('active')->where(['slug' => 'broccoly-balboa'])->first();
        $art_peach_boys = Product::withoutGlobalScope('active')->where(['slug' => 'peach-boys'])->first();
        $art_anthony_pumpkins = Product::withoutGlobalScope('active')->where(['slug' => 'anthony-pumpkins'])->first();
        $art_inbanana_jones = Product::withoutGlobalScope('active')->where(['slug' => 'inbanana-jones'])->first();
        $art_avocado_di_caprio = Product::withoutGlobalScope('active')->where(['slug' => 'avocado-di-caprio'])->first();
        $art_katy_berry = Product::withoutGlobalScope('active')->where(['slug' => 'katy-berry'])->first();
        $art_sweet_home_albanana = Product::withoutGlobalScope('active')->where(['slug' => 'sweet-home-albanana'])->first();
        $art_beetney_spears = Product::withoutGlobalScope('active')->where(['slug' => 'beetney-spears'])->first();
        $art_cocohontas = Product::withoutGlobalScope('active')->where(['slug' => 'cocohontas'])->first();

        $country_ch = \App\Models\Country::where(['content_code' => 'de-ch'])->first();
        $country_de = \App\Models\Country::where(['content_code' => 'de-de'])->first();
        $country_at = \App\Models\Country::where(['content_code' => 'de-at'])->first();

        // Workaround for gitlab-test. Reason: Products are not created via migrations. But they exist on our locals, staging and production...
        if(!empty($art_pirates_of_the_carrotean)) {


            // Make prices complete!
            $art_peach_boys->price = 3.87;
            $art_peach_boys->tax_percentage = 2.5;
            $art_peach_boys->product_identifier = 'E';
            $art_peach_boys->save();
            $art_cocohontas->price = 3.87;
            $art_cocohontas->tax_percentage = 2.5;
            $art_cocohontas->save();
            $art_beetney_spears->price = 3.87;
            $art_beetney_spears->tax_percentage = 2.5;
            $art_beetney_spears->save();
            $art_sweet_home_albanana->price  = 3.87;
            $art_sweet_home_albanana->tax_percentage = 2.5;
            $art_sweet_home_albanana->save();
            $art_katy_berry->price = 3.87;
            $art_katy_berry->tax_percentage = 2.5;
            $art_katy_berry->save();
            $art_avocado_di_caprio->price = 3.87;
            $art_avocado_di_caprio->tax_percentage = 2.5;
            $art_avocado_di_caprio->save();
            $art_inbanana_jones->price = 3.87;
            $art_inbanana_jones->tax_percentage = 2.5;
            $art_inbanana_jones->save();
            $art_anthony_pumpkins->price = 3.87;
            $art_anthony_pumpkins->tax_percentage = 2.5;
            $art_anthony_pumpkins->save();
            $art_broccoly_balboa->price = 3.87;
            $art_broccoly_balboa->tax_percentage = 2.5;
            $art_broccoly_balboa->save();
            $art_mango_nr_five->price = 3.87;
            $art_mango_nr_five->tax_percentage = 2.5;
            $art_mango_nr_five->save();
            $art_david_zucchetta->price = 3.87;
            $art_david_zucchetta->tax_percentage = 2.5;
            $art_david_zucchetta->save();
            $art_applecalypse_now->price = 3.87;
            $art_applecalypse_now->tax_percentage = 2.5;
            $art_applecalypse_now->save();
            $art_fresh_prince_of_bel_pear->price = 3.87;
            $art_fresh_prince_of_bel_pear->tax_percentage = 2.5;
            $art_fresh_prince_of_bel_pear->save();
            $art_pirates_of_the_carrotean->price = 3.87;
            $art_pirates_of_the_carrotean->tax_percentage = 2.5;
            $art_pirates_of_the_carrotean->save();






          //  Price::create(['product_id'=>$art_peach_boys->id,'country_id'=>$country_ch->id,'tax'=>1.89,'tax_percentage'=>2.5,'shipping_tax_percentage'=>7.7,'value'=>3.5]);
          //  Price::create(['product_id'=>$art_peach_boys->id,'country_id'=>$country_ch->id,'tax'=>1.89,'tax_percentage'=>2.5,'shipping_tax_percentage'=>7.7,'value'=>3.5]);




            $box1 = Box::create(['type' => 'Suggested','status' => 'active']);
            $sbox1 = SuggestedBox::create(['box_id' => $box1->id,'children_age' => '4+', 'product_type' => 'breil', 'product_content' => 'fruchtig']);
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $box1->id, 'quantity' => 6]);
            BoxContent::create(['product_id' => $art_applecalypse_now->id, 'box_id' => $box1->id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $box1->id, 'quantity' => 5]);

            $box2 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox2 = SuggestedBox::create(['box_id' => $box2->id,'children_age' => '4+', 'product_type' => 'breil', 'product_content' => 'gemusig']);
            BoxContent::create(['product_id' => $art_pirates_of_the_carrotean->id, 'box_id' => $box2->id, 'quantity' => 8]);
            BoxContent::create(['product_id' => $art_anthony_pumpkins->id, 'box_id' => $box2->id, 'quantity' => 8]);

            $box3 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox3 = SuggestedBox::create(['box_id' => $box3->id,'children_age' => '4+', 'product_type' => 'breil', 'product_content' => 'all']);
            BoxContent::create(['product_id' => $art_pirates_of_the_carrotean->id, 'box_id' => $box3->id, 'quantity' => 4]);
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $box3->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_applecalypse_now->id, 'box_id' => $box3->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $box3->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_anthony_pumpkins->id, 'box_id' => $box3->id, 'quantity' => 3]);

            $box4 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox4 = SuggestedBox::create(['box_id' => $box4->id,'children_age' => '4+', 'product_type' => 'pouch', 'product_content' => 'fruchtig','default_box' => 1]);
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $box4->id, 'quantity' => 6]);
            BoxContent::create(['product_id' => $art_avocado_di_caprio->id, 'box_id' => $box4->id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $box4->id, 'quantity' => 5]);

            $box5 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox5 = SuggestedBox::create(['box_id'=>$box5->id,'children_age' => '4+', 'product_type' => 'pouch', 'product_content' => 'gemusig']);
            BoxContent::create(['produfeature/YAMO-1959/suggestedpackagesct_id' => $art_inbanana_jones->id, 'box_id' => $box5->id, 'quantity' => 6]);
            BoxContent::create(['product_id' => $art_avocado_di_caprio->id, 'box_id' => $box5->id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $box5->id, 'quantity' => 5]);

            $box6 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox6 = SuggestedBox::create(['box_id'=>$box6->id,'children_age' => '4+', 'product_type' => 'pouch', 'product_content' => 'all']);
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $box6->id, 'quantity' => 6]);
            BoxContent::create(['product_id' => $art_avocado_di_caprio->id, 'box_id' => $box6->id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $box6->id, 'quantity' => 5]);

            $box7 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox7 = SuggestedBox::create(['box_id'=>$box7->id,'children_age' => '4+', 'product_type' => 'all', 'product_content' => 'fruchtig']);
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $box7->id, 'quantity' => 6]);
            BoxContent::create(['product_id' => $art_applecalypse_now->id, 'box_id' => $box7->id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $box7->id, 'quantity' => 5]);

            $box8 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox8 = SuggestedBox::create(['box_id'=>$box8->id,'children_age' => '4+', 'product_type' => 'all', 'product_content' => 'gemusig']);
            BoxContent::create(['product_id' => $art_pirates_of_the_carrotean->id, 'box_id' => $box8->id, 'quantity' => 8]);
            BoxContent::create(['product_id' => $art_anthony_pumpkins->id, 'box_id' => $box8->id, 'quantity' => 8]);

            $box9 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox9 = SuggestedBox::create(['box_id'=>$box9->id,'children_age' => '4+', 'product_type' => 'all', 'product_content' => 'all']);
            BoxContent::create(['product_id' => $art_pirates_of_the_carrotean->id, 'box_id' => $box9->id, 'quantity' => 4]);
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $box9->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_applecalypse_now->id, 'box_id' => $box9->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $box9->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_anthony_pumpkins->id, 'box_id' => $box9->id, 'quantity' => 3]);

            $box10 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox10 = SuggestedBox::create(['box_id'=>$box10->id,'children_age' => '6+', 'product_type' => 'breil', 'product_content' => 'fruchtig']);
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $box10->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_applecalypse_now->id, 'box_id' => $box10->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $box10->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_mango_nr_five->id, 'box_id' => $box10->id, 'quantity' => 4]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $box10->id, 'quantity' => 3]);

            $box11 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox11 = SuggestedBox::create(['box_id'=>$box11->id,'children_age' => '6+', 'product_type' => 'breil', 'product_content' => 'gemusig']);
            BoxContent::create(['product_id' => $art_pirates_of_the_carrotean->id, 'box_id' => $box11->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_anthony_pumpkins->id, 'box_id' => $box11->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_david_zucchetta->id, 'box_id' => $box11->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_broccoly_balboa->id, 'box_id' => $box11->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_beetney_spears->id, 'box_id' => $box11->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_cocohontas->id, 'box_id' => $box11->id, 'quantity' => 3]);

            $box12 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox12 = SuggestedBox::create(['box_id'=>$box12->id,'children_age' => '6+', 'product_type' => 'breil', 'product_content' => 'all']);
            BoxContent::create(['product_id' => $art_pirates_of_the_carrotean->id, 'box_id' => $box12->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $box12->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_applecalypse_now->id, 'box_id' => $box12->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $box12->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_anthony_pumpkins->id, 'box_id' => $box12->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_david_zucchetta->id, 'box_id' => $box12->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_mango_nr_five->id, 'box_id' => $box12->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_sweet_home_albanana->id, 'box_id' => $box12->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_broccoly_balboa->id, 'box_id' => $box12->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_beetney_spears->id, 'box_id' => $box12->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_cocohontas->id, 'box_id' => $box12->id, 'quantity' => 2]);

            $box13 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox13 = SuggestedBox::create(['box_id'=>$box13->id,'children_age' => '6+', 'product_type' => 'pouch', 'product_content' => 'fruchtig']);
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $box13->id, 'quantity' => 6]);
            BoxContent::create(['product_id' => $art_avocado_di_caprio->id, 'box_id' => $box13->id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $box13->id, 'quantity' => 5]);

            $box14 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox14 = SuggestedBox::create(['box_id'=>$box14->id,'children_age' => '6+', 'product_type' => 'pouch', 'product_content' => 'gemusig']);
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $box14->id, 'quantity' => 6]);
            BoxContent::create(['product_id' => $art_avocado_di_caprio->id, 'box_id' => $box14->id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $box14->id, 'quantity' => 5]);

            $box15 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox15 = SuggestedBox::create(['box_id'=>$box15->id,'children_age' => '6+', 'product_type' => 'pouch', 'product_content' => 'all']);
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $box15->id, 'quantity' => 6]);
            BoxContent::create(['product_id' => $art_avocado_di_caprio->id, 'box_id' => $box15->id, 'quantity' => 5]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $box15->id, 'quantity' => 5]);

            $box16 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox16 = SuggestedBox::create(['box_id'=>$box16->id,'children_age' => '6+', 'product_type' => 'all', 'product_content' => 'fruchtig']);
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $box16->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_applecalypse_now->id, 'box_id' => $box16->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $box16->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_david_zucchetta->id, 'box_id' => $box16->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_mango_nr_five->id, 'box_id' => $box16->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_sweet_home_albanana->id, 'box_id' => $box16->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $box16->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_avocado_di_caprio->id, 'box_id' => $box16->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $box16->id, 'quantity' => 2]);


            $box17 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox17 = SuggestedBox::create(['box_id'=>$box17->id,'children_age' => '6+', 'product_type' => 'all', 'product_content' => 'gemusig']);
            BoxContent::create(['product_id' => $art_pirates_of_the_carrotean->id, 'box_id' => $box17->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_anthony_pumpkins->id, 'box_id' => $box17->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_david_zucchetta->id, 'box_id' => $box17->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_broccoly_balboa->id, 'box_id' => $box17->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_beetney_spears->id, 'box_id' => $box17->id, 'quantity' => 3]);
            BoxContent::create(['product_id' => $art_cocohontas->id, 'box_id' => $box17->id, 'quantity' => 3]);


            $box18 = Box::create(["type" => "Suggested","status" => "active"]);
            $sbox18 = SuggestedBox::create(['box_id'=>$box18->id,'children_age' => '6+', 'product_type' => 'all', 'product_content' => 'all']);


            BoxContent::create(['product_id' => $art_pirates_of_the_carrotean->id, 'box_id' => $box18->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_fresh_prince_of_bel_pear->id, 'box_id' => $box18->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_applecalypse_now->id, 'box_id' => $box18->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_peach_boys->id, 'box_id' => $box18->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_anthony_pumpkins->id, 'box_id' => $box18->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_david_zucchetta->id, 'box_id' => $box18->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_mango_nr_five->id, 'box_id' => $box18->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_sweet_home_albanana->id, 'box_id' => $box18->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_broccoly_balboa->id, 'box_id' => $box18->id, 'quantity' => 2]);

            BoxContent::create(['product_id' => $art_beetney_spears->id, 'box_id' => $box18->id, 'quantity' => 2]);
            BoxContent::create(['product_id' => $art_cocohontas->id, 'box_id' => $box18->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_inbanana_jones->id, 'box_id' => $box18->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_avocado_di_caprio->id, 'box_id' => $box18->id, 'quantity' => 1]);
            BoxContent::create(['product_id' => $art_katy_berry->id, 'box_id' => $box18->id, 'quantity' => 1]);

        }

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suggested_boxes');
        Schema::dropIfExists('suggested_box_contents');
    }
}
