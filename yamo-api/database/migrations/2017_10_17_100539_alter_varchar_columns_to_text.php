<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVarcharColumnsToText extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('testimonials', function(Blueprint $table) {
            DB::statement('alter table testimonials modify description text');
            DB::statement('alter table testimonials modify customer_details text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('testimonials', function(Blueprint $table) {
            DB::statement('alter table testimonials modify description varchar(255)');
            DB::statement('alter table testimonials modify customer_details varchar(255)');
        });
    }
}
