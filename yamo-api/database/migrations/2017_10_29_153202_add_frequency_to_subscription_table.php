<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFrequencyToSubscriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function($table) {
            $table->integer('delivery_frequency')->nullable()->after('status');
            $table->string('delivery_frequency_period')->nullable()->after('delivery_frequency');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subscriptions', function($table) {
            $table->dropColumn('delivery_frequency');
            $table->dropColumn('delivery_frequency_period');
        });
    }
}
