<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeStringColumnsToTextColumnsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function(Blueprint $table) {
            DB::statement('alter table products modify characteristics text');
            DB::statement('alter table products modify short_description text');
            DB::statement('alter table products modify full_description text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function(Blueprint $table) {
            DB::statement('alter table products modify characteristics varchar(255)');
            DB::statement('alter table products modify short_description varchar(255)');
            DB::statement('alter table products modify full_description varchar(255)');
        });
    }
}
