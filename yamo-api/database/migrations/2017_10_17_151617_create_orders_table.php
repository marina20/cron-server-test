<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('payment_method_id');
            $table->string('payment_method_title');
            $table->string('transaction_id');
            $table->string('customer_ip_address');
            $table->string('customer_user_agent');
            $table->string('created_via');
            $table->string('completed_date');
            $table->string('paid_date');
            $table->string('billing_first_name');
            $table->string('billing_last_name');
            $table->string('billing_company');
            $table->string('billing_address_1');
            $table->string('billing_address_2');
            $table->string('billing_city');
            $table->string('billing_state');
            $table->string('billing_postcode');
            $table->string('billing_country');
            $table->string('billing_email');
            $table->string('billing_phone');
            $table->string('shipping_first_name');
            $table->string('shipping_last_name');
            $table->string('shipping_company');
            $table->string('shipping_address_1');
            $table->string('shipping_address_2');
            $table->string('shipping_city');
            $table->string('shipping_state');
            $table->string('shipping_postcode');
            $table->string('shipping_country');
            $table->string('currency');
            $table->string('discount');
            $table->string('discount_tax');
            $table->string('shipping');
            $table->string('shipping_tax');
            $table->string('order_tax');
            $table->string('order_total');
            $table->string('shipping_method');
            $table->string('delivery_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
