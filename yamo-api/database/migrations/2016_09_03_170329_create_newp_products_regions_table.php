<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewpProductsRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('products_regions')){
            Schema::create('products_regions', function(Blueprint $table){
                $table->increments('id');
                $table->unsignedInteger('product_id');
                $table->unsignedInteger('region_id');
                $table->unsignedInteger('vat_group_id');
                $table->timestamp('active_start_date');
                $table->timestamp('active_end_date');
                $table->boolean('active')->nullable();
                $table->string('price');
                $table->integer('out_of_stock')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_regions');
    }
}
