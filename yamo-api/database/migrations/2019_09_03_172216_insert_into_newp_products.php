<?php
use App\Models\Product;
use App\Models\ExtraProduct;
use App\Models\Category;
use App\Models\Translation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoNewpProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $pirates = DB::table('oldp_products')->where('slug','pirates-of-the-carrotean')->first();
        $freshPrince = DB::table('oldp_products')->where('slug','fresh-prince-of-bel-pear')->first();
        $applecalypse = DB::table('oldp_products')->where('slug','applecalypse-now')->first();
        $david = DB::table('oldp_products')->where('slug','david-zucchetta')->first();
        $mango = DB::table('oldp_products')->where('slug','mango-no-5')->first();
        $beetney = DB::table('oldp_products')->where('slug','beetney-spears')->first();
        $broccoly = DB::table('oldp_products')->where('slug','broccoly-balboa')->first();
        $anthony = DB::table('oldp_products')->where('slug','anthony-pumpkins')->first();
        $sweetHome = DB::table('oldp_products')->where('slug','sweet-home-albanana')->first();
        $cocohontas = DB::table('oldp_products')->where('slug','cocohontas')->first();
        $inbanana = DB::table('oldp_products')->where('slug','inbanana-jones')->first();
        $avocado = DB::table('oldp_products')->where('slug','avocado-di-caprio')->first();
        $katy = DB::table('oldp_products')->where('slug','katy-berry')->first();
        $peach = DB::table('oldp_products')->where('slug','peach-boys')->first();
        $prince = DB::table('oldp_products')->where('slug','prince-vanilliam')->first();
        $nicki = DB::table('oldp_products')->where('slug','nicki-spinaj')->first();
        $quentin = DB::table('oldp_products')->where('slug','quentin-carrotino')->first();
        $cocofield = DB::table('oldp_products')->where('slug','david-cocofield')->first();
        $berry = DB::table('oldp_products')->where('slug','berry-potter')->first();
        $bib = DB::table('oldp_products')->where('slug','bib')->first();
        $coolingBag = DB::table('oldp_products')->where('slug','kuhltasche')->first();
        $flyer = DB::table('oldp_products')->where('slug','flyer')->first();

        if(empty($coolingBag)){
            $coolingBag = DB::table('oldp_products')->insert([
                'category'=>'tasche',
                'product_identifier'=>'Q',
                'name'=>'Kühltasche',
                'slug'=>'kuhltasche',
                'position'=>'10000'
            ]);
        }

        $coolingBag = DB::table('oldp_products')->where('slug','kuhltasche')->first();

        if(empty($flyer)){
            $flyer = DB::table('oldp_products')->insert([
                'category'=>'flyer',
                'product_identifier'=>'X',
                'name'=>'Flyer',
                'slug'=>'flyer',
                'position'=>'12000'
            ]);
        }

        $flyer = DB::table('oldp_products')->where('slug','flyer')->first();

        $extraProductFlyer = ExtraProduct::where('product_code',$flyer->product_identifier)->first();
        if(!empty($flyer) && empty($extraProductFlyer))
            ExtraProduct::create(['product_id'=>$flyer->id,'product_code'=>$flyer->product_identifier]);

        $categoryCups = Category::where('name_key', Product::PRODUCT_CATEGORY_BREIL)->first();
        $categoryPouches = Category::where('name_key', Product::PRODUCT_CATEGORY_POUCH)->first();
        $categoryPromo = Category::where('name_key', Product::PRODUCT_CATEGORY_PROMO_GIFT)->first();
        $categoryFlyer = Category::where('name_key', Product::PRODUCT_CATEGORY_FLYER)->first();

        Product::withoutGlobalScope('active')->insert([
            [
                'id'=>$pirates->id,
                'category_id'=>$categoryCups->id,
                'name_key'=>strtoupper($pirates->slug),
                'product_family'=>$pirates->slug,
                'product_identifier_letter'=>$pirates->product_identifier,
                'size'=>118,
                'months'=>4,
                'position'=>$pirates->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$freshPrince->id,
                'category_id'=>$categoryCups->id,
                'name_key'=>strtoupper($freshPrince->slug),
                'product_family'=>$freshPrince->slug,
                'product_identifier_letter'=>$freshPrince->product_identifier,
                'size'=>118,
                'months'=>4,
                'position'=>$freshPrince->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$applecalypse->id,
                'category_id'=>$categoryCups->id,
                'name_key'=>strtoupper($applecalypse->slug),
                'product_family'=>$applecalypse->slug,
                'product_identifier_letter'=>$applecalypse->product_identifier,
                'size'=>118,
                'months'=>4,
                'position'=>$applecalypse->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$david->id,
                'category_id'=>$categoryCups->id,
                'name_key'=>strtoupper($david->slug),
                'product_family'=>$david->slug,
                'product_identifier_letter'=>$david->product_identifier,
                'size'=>118,
                'months'=>6,
                'position'=>$david->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$mango->id,
                'category_id'=>$categoryCups->id,
                'name_key'=>strtoupper($mango->slug),
                'product_family'=>$mango->slug,
                'product_identifier_letter'=>$mango->product_identifier,
                'size'=>118,
                'months'=>6,
                'position'=>$mango->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$beetney->id,
                'category_id'=>$categoryCups->id,
                'name_key'=>strtoupper($beetney->slug),
                'product_family'=>$beetney->slug,
                'product_identifier_letter'=>$beetney->product_identifier,
                'size'=>118,
                'months'=>6,
                'position'=>$beetney->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$broccoly->id,
                'category_id'=>$categoryCups->id,
                'name_key'=>strtoupper($broccoly->slug),
                'product_family'=>$broccoly->slug,
                'product_identifier_letter'=>$broccoly->product_identifier,
                'size'=>118,
                'months'=>6,
                'position'=>$broccoly->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$anthony->id,
                'category_id'=>$categoryCups->id,
                'name_key'=>strtoupper($anthony->slug),
                'product_family'=>$anthony->slug,
                'product_identifier_letter'=>$anthony->product_identifier,
                'size'=>118,
                'months'=>4,
                'position'=>$anthony->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$sweetHome->id,
                'category_id'=>$categoryCups->id,
                'name_key'=>strtoupper($sweetHome->slug),
                'product_family'=>$sweetHome->slug,
                'product_identifier_letter'=>$sweetHome->product_identifier,
                'size'=>118,
                'months'=>6,
                'position'=>$sweetHome->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$cocohontas->id,
                'category_id'=>$categoryCups->id,
                'name_key'=>strtoupper($cocohontas->slug),
                'product_family'=>$cocohontas->slug,
                'product_identifier_letter'=>$cocohontas->product_identifier,
                'size'=>118,
                'months'=>6,
                'position'=>$cocohontas->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$inbanana->id,
                'category_id'=>$categoryPouches->id,
                'name_key'=>strtoupper($inbanana->slug),
                'product_family'=>$inbanana->slug,
                'product_identifier_letter'=>$inbanana->product_identifier,
                'size'=>120,
                'months'=>6,
                'position'=>$inbanana->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$avocado->id,
                'category_id'=>$categoryPouches->id,
                'name_key'=>strtoupper($avocado->slug),
                'product_family'=>$avocado->slug,
                'product_identifier_letter'=>$avocado->product_identifier,
                'size'=>120,
                'months'=>6,
                'position'=>$avocado->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$katy->id,
                'category_id'=>$categoryPouches->id,
                'name_key'=>strtoupper($katy->slug),
                'product_family'=>$katy->slug,
                'product_identifier_letter'=>$katy->product_identifier,
                'size'=>120,
                'months'=>6,
                'position'=>$katy->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$peach->id,
                'category_id'=>$categoryCups->id,
                'name_key'=>strtoupper($peach->slug),
                'product_family'=>$peach->slug,
                'product_identifier_letter'=>$peach->product_identifier,
                'size'=>118,
                'months'=>6,
                'position'=>$peach->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$prince->id,
                'category_id'=>$categoryPouches->id,
                'name_key'=>strtoupper($prince->slug),
                'product_family'=>$prince->slug,
                'product_identifier_letter'=>$prince->product_identifier,
                'size'=>120,
                'months'=>6,
                'position'=>$prince->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$nicki->id,
                'category_id'=>$categoryPouches->id,
                'name_key'=>strtoupper($nicki->slug),
                'product_family'=>$nicki->slug,
                'product_identifier_letter'=>$nicki->product_identifier,
                'size'=>120,
                'months'=>6,
                'position'=>$nicki->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$quentin->id,
                'category_id'=>$categoryPouches->id,
                'name_key'=>strtoupper($quentin->slug),
                'product_family'=>$quentin->slug,
                'product_identifier_letter'=>$quentin->product_identifier,
                'size'=>120,
                'months'=>6,
                'position'=>$quentin->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$cocofield->id,
                'category_id'=>$categoryPouches->id,
                'name_key'=>strtoupper($cocofield->slug),
                'product_family'=>$cocofield->slug,
                'product_identifier_letter'=>$cocofield->product_identifier,
                'size'=>120,
                'months'=>6,
                'position'=>$cocofield->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$berry->id,
                'category_id'=>$categoryPouches->id,
                'name_key'=>strtoupper($berry->slug),
                'product_family'=>$berry->slug,
                'product_identifier_letter'=>$berry->product_identifier,
                'size'=>120,
                'months'=>6,
                'position'=>$berry->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$bib->id,
                'category_id'=>$categoryPromo->id,
                'name_key'=>strtoupper($bib->slug),
                'product_family'=>$bib->slug,
                'product_identifier_letter'=>$bib->product_identifier,
                'size'=>0,
                'months'=>0,
                'position'=>$bib->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$coolingBag->id,
                'category_id'=>$categoryPromo->id,
                'name_key'=>strtoupper($coolingBag->slug),
                'product_family'=>$coolingBag->slug,
                'product_identifier_letter'=>$coolingBag->product_identifier,
                'size'=>0,
                'months'=>0,
                'position'=>$coolingBag->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ],[
                'id'=>$flyer->id,
                'category_id'=>$categoryFlyer->id,
                'name_key'=>strtoupper($flyer->slug),
                'product_family'=>$flyer->slug,
                'product_identifier_letter'=>$flyer->product_identifier,
                'size'=>0,
                'months'=>0,
                'position'=>$flyer->position,
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ]
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($pirates->slug),
                'de-de'=>$pirates->name,
                'de-ch'=>$pirates->name,
                'de-at'=>$pirates->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($freshPrince->slug),
                'de-de'=>$freshPrince->name,
                'de-ch'=>$freshPrince->name,
                'de-at'=>$freshPrince->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($applecalypse->slug),
                'de-de'=>$applecalypse->name,
                'de-ch'=>$applecalypse->name,
                'de-at'=>$applecalypse->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($david->slug),
                'de-de'=>$david->name,
                'de-ch'=>$david->name,
                'de-at'=>$david->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($mango->slug),
                'de-de'=>$mango->name,
                'de-ch'=>$mango->name,
                'de-at'=>$mango->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($beetney->slug),
                'de-de'=>$beetney->name,
                'de-ch'=>$beetney->name,
                'de-at'=>$beetney->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($broccoly->slug),
                'de-de'=>$broccoly->name,
                'de-ch'=>$broccoly->name,
                'de-at'=>$broccoly->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($anthony->slug),
                'de-de'=>$anthony->name,
                'de-ch'=>$anthony->name,
                'de-at'=>$anthony->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($sweetHome->slug),
                'de-de'=>$sweetHome->name,
                'de-ch'=>$sweetHome->name,
                'de-at'=>$sweetHome->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($cocohontas->slug),
                'de-de'=>$cocohontas->name,
                'de-ch'=>$cocohontas->name,
                'de-at'=>$cocohontas->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($inbanana->slug),
                'de-de'=>$inbanana->name,
                'de-ch'=>$inbanana->name,
                'de-at'=>$inbanana->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($avocado->slug),
                'de-de'=>$avocado->name,
                'de-ch'=>$avocado->name,
                'de-at'=>$avocado->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($katy->slug),
                'de-de'=>$katy->name,
                'de-ch'=>$katy->name,
                'de-at'=>$katy->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($peach->slug),
                'de-de'=>$peach->name,
                'de-ch'=>$peach->name,
                'de-at'=>$peach->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($prince->slug),
                'de-de'=>$prince->name,
                'de-ch'=>$prince->name,
                'de-at'=>$prince->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($nicki->slug),
                'de-de'=>$nicki->name,
                'de-ch'=>$nicki->name,
                'de-at'=>$nicki->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($quentin->slug),
                'de-de'=>$quentin->name,
                'de-ch'=>$quentin->name,
                'de-at'=>$quentin->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($cocofield->slug),
                'de-de'=>$cocofield->name,
                'de-ch'=>$cocofield->name,
                'de-at'=>$cocofield->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($berry->slug),
                'de-de'=>$berry->name,
                'de-ch'=>$berry->name,
                'de-at'=>$berry->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($bib->slug),
                'de-de'=>$bib->name,
                'de-ch'=>$bib->name,
                'de-at'=>$bib->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($coolingBag->slug),
                'de-de'=>$coolingBag->name,
                'de-ch'=>$coolingBag->name,
                'de-at'=>$coolingBag->name
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($flyer->slug),
                'de-de'=>$flyer->name,
                'de-ch'=>$flyer->name,
                'de-at'=>$flyer->name
            ]
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($pirates->slug).'.CHARACTERISTIC',
                'de-de'=>'["Stärkende Süsse","Nachmittagsbrei","Zum kühl geniessen"]',
                'de-ch'=>'["Stärkende Süsse","Nachmittagsbrei","Zum kühl geniessen"]',
                'de-at'=>'["Stärkende Süsse","Nachmittagsbrei","Zum kühl geniessen"]'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($pirates->slug).'.PRODUCE-TYPE',
                'de-de'=>'fruchtig',
                'de-ch'=>'fruchtig',
                'de-at'=>'fruchtig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($pirates->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#D0865D',
                'de-ch'=>'#D0865D',
                'de-at'=>'#D0865D'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($pirates->slug).'.INFORMATION',
                'de-de'=>'Arrr, dieser kleine Halunke lässt Skorbut keine Chance. Trotz seiner kaltblütigen Namensgeber verträgt sich unser Karottenbrei sehr gut mit allerlei Leichtmatrosen ab dem 4. Monat. Für Mütter, die Johnny Depp gern etwas näher kommen würden',
                'de-ch'=>'Arrr, dieser kleine Halunke lässt Skorbut keine Chance. Trotz seiner kaltblütigen Namensgeber verträgt sich unser Karottenbrei sehr gut mit allerlei Leichtmatrosen ab dem 4. Monat. Für Mütter, die Johnny Depp gern etwas näher kommen würden',
                'de-at'=>'Arrr, dieser kleine Halunke lässt Skorbut keine Chance. Trotz seiner kaltblütigen Namensgeber verträgt sich unser Karottenbrei sehr gut mit allerlei Leichtmatrosen ab dem 4. Monat. Für Mütter, die Johnny Depp gern etwas näher kommen würden'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($pirates->slug).'.TIME-OF-DAY',
                'de-de'=>'Mittagsbrei',
                'de-ch'=>'Mittagsbrei',
                'de-at'=>'Mittagsbrei'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($pirates->slug).'.PREP-TYPE',
                'de-de'=>'Kann im Wasserbad erwärmt werden',
                'de-ch'=>'Kann im Wasserbad erwärmt werden',
                'de-at'=>'Kann im Wasserbad erwärmt werden'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($pirates->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Karotte 70%, Weisser Traubensaft, Wasser, Rapsöl 2%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten.',
                'de-ch'=>'Karotte 70%, Weisser Traubensaft, Wasser, Rapsöl 2%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten.',
                'de-at'=>'Karotte 70%, Weisser Traubensaft, Wasser, Rapsöl 2%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($pirates->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"229 kJ/ 55 kcal","indentation":""},{"name":"Fett","value":"2,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,20 g","indentation":"true"},{"name":"Kohlenhydrate","value":"6,2 g","indentation":""},{"name":"davon Zucker","value":"6,0 g","indentation":"true"},{"name":"Eiweiss","value":"0,6 g","indentation":""},{"name":"Salz","value":"< 0,1 g","indentation":""}]',
                'de-ch'=>'[{"name":"Energie","value":"229 kJ/ 55 kcal","indentation":""},{"name":"Fett","value":"2,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,20 g","indentation":"true"},{"name":"Kohlenhydrate","value":"6,2 g","indentation":""},{"name":"davon Zucker","value":"6,0 g","indentation":"true"},{"name":"Eiweiss","value":"0,6 g","indentation":""},{"name":"Salz","value":"< 0,1 g","indentation":""}]',
                'de-at'=>'[{"name":"Energie","value":"229 kJ/ 55 kcal","indentation":""},{"name":"Fett","value":"2,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,20 g","indentation":"true"},{"name":"Kohlenhydrate","value":"6,2 g","indentation":""},{"name":"davon Zucker","value":"6,0 g","indentation":"true"},{"name":"Eiweiss","value":"0,6 g","indentation":""},{"name":"Salz","value":"< 0,1 g","indentation":""}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($freshPrince->slug).'.CHARACTERISTIC',
                'de-de'=>'["Stärkende Süsse","Nachmittagsbrei","Zum kühl geniessen"]',
                'de-ch'=>'["Stärkende Süsse","Nachmittagsbrei","Zum kühl geniessen"]',
                'de-at'=>'["Stärkende Süsse","Nachmittagsbrei","Zum kühl geniessen"]'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($freshPrince->slug).'.PRODUCE-TYPE',
                'de-de'=>'fruchtig',
                'de-ch'=>'fruchtig',
                'de-at'=>'fruchtig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($freshPrince->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#D0865D',
                'de-ch'=>'#D0865D',
                'de-at'=>'#D0865D'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($freshPrince->slug).'.INFORMATION',
                'de-de'=>"Dieser kleine Rebell hat''s Faustdick hinter der Birne. Er zeichnet sich nicht nur durch seine stärkende Süsse aus, sondern trumpft auch noch mit seinem hohen Kalium und Eisen auf. Besonders “cool” wirkt er am Nachmittag.",
                'de-ch'=>"Dieser kleine Rebell hat''s Faustdick hinter der Birne. Er zeichnet sich nicht nur durch seine stärkende Süsse aus, sondern trumpft auch noch mit seinem hohen Kalium und Eisen auf. Besonders “cool” wirkt er am Nachmittag.",
                'de-at'=>"Dieser kleine Rebell hat''s Faustdick hinter der Birne. Er zeichnet sich nicht nur durch seine stärkende Süsse aus, sondern trumpft auch noch mit seinem hohen Kalium und Eisen auf. Besonders “cool” wirkt er am Nachmittag."
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($freshPrince->slug).'.TIME-OF-DAY',
                'de-de'=>'Nachmittagsbrei',
                'de-ch'=>'Nachmittagsbrei',
                'de-at'=>'Nachmittagsbrei'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($freshPrince->slug).'.PREP-TYPE',
                'de-de'=>'Zum Kühl geniessen',
                'de-ch'=>'Zum Kühl geniessen',
                'de-at'=>'Zum Kühl geniessen'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($freshPrince->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Birne 99%, Fruchtpulver der Acerola-Kirsche. Kann Gluten, Milch und Sellerie enthalten.',
                'de-ch'=>'Birne 99%, Fruchtpulver der Acerola-Kirsche. Kann Gluten, Milch und Sellerie enthalten.',
                'de-at'=>'Birne 99%, Fruchtpulver der Acerola-Kirsche. Kann Gluten, Milch und Sellerie enthalten.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($freshPrince->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"234 kJ/ 56 kcal","indentation":""},{"name":"Fett","value":"0,3 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,0 g","indentation":"true"},{"name":"Kohlenhydrate","value":"11,8 g","indentation":""},{"name":"davon Zucker","value":"11,7 g","indentation":"true"},{"name":"Eiweiss","value":"0,4 g","indentation":""},{"name":"Salz","value":"0,00 g","indentation":""}]',
                'de-ch'=>'[{"name":"Energie","value":"234 kJ/ 56 kcal","indentation":""},{"name":"Fett","value":"0,3 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,0 g","indentation":"true"},{"name":"Kohlenhydrate","value":"11,8 g","indentation":""},{"name":"davon Zucker","value":"11,7 g","indentation":"true"},{"name":"Eiweiss","value":"0,4 g","indentation":""},{"name":"Salz","value":"0,00 g","indentation":""}]',
                'de-at'=>'[{"name":"Energie","value":"234 kJ/ 56 kcal","indentation":""},{"name":"Fett","value":"0,3 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,0 g","indentation":"true"},{"name":"Kohlenhydrate","value":"11,8 g","indentation":""},{"name":"davon Zucker","value":"11,7 g","indentation":"true"},{"name":"Eiweiss","value":"0,4 g","indentation":""},{"name":"Salz","value":"0,00 g","indentation":""}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($applecalypse->slug).'.CHARACTERISTIC',
                'de-de'=>'["Sättigender Fruchtgenuss","Abendbrei","Zum kühl geniessen"]',
                'de-ch'=>'["Sättigender Fruchtgenuss","Abendbrei","Zum kühl geniessen"]',
                'de-at'=>'["Sättigender Fruchtgenuss","Abendbrei","Zum kühl geniessen"]'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($applecalypse->slug).'.PRODUCE-TYPE',
                'de-de'=>'fruchtig',
                'de-ch'=>'fruchtig',
                'de-at'=>'fruchtig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($applecalypse->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#D4B178',
                'de-ch'=>'#D4B178',
                'de-at'=>'#D4B178'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($applecalypse->slug).'.INFORMATION',
                'de-de'=>'Dieser Verfechter des gesunden Essens ist eine regelrechte Wunderwaffe. Zu seinem Arsenal zählen hohe Werte an Proteinen, Eisen, Magnesium. Am Besten nähert man sich ihm Abends, wenn die Luft abgekühlt ist.',
                'de-ch'=>'Dieser Verfechter des gesunden Essens ist eine regelrechte Wunderwaffe. Zu seinem Arsenal zählen hohe Werte an Proteinen, Eisen, Magnesium. Am Besten nähert man sich ihm Abends, wenn die Luft abgekühlt ist.',
                'de-at'=>'Dieser Verfechter des gesunden Essens ist eine regelrechte Wunderwaffe. Zu seinem Arsenal zählen hohe Werte an Proteinen, Eisen, Magnesium. Am Besten nähert man sich ihm Abends, wenn die Luft abgekühlt ist.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($applecalypse->slug).'.TIME-OF-DAY',
                'de-de'=>'Abendbrei',
                'de-ch'=>'Abendbrei',
                'de-at'=>'Abendbrei'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($applecalypse->slug).'.PREP-TYPE',
                'de-de'=>'Zum Kühl geniessen',
                'de-ch'=>'Zum Kühl geniessen',
                'de-at'=>'Zum Kühl geniessen'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($applecalypse->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Apfel 84%, Wasser, Dinkelmehl 3%,Fruchtpulver der Acerola-Kirsche. Enthält Gluten, kann Milch und Sellerie enthalten.',
                'de-ch'=>'Apfel 84%, Wasser, Dinkelmehl 3%,Fruchtpulver der Acerola-Kirsche. Enthält Gluten, kann Milch und Sellerie enthalten.',
                'de-at'=>'Apfel 84%, Wasser, Dinkelmehl 3%,Fruchtpulver der Acerola-Kirsche. Enthält Gluten, kann Milch und Sellerie enthalten.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($applecalypse->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"263 kJ/ 63 kcal","indentation":""},{"name":"Fett","value":"0,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,10 g","indentation":"true"},{"name":"Kohlenhydrate","value":"12,7 g","indentation":""},{"name":"davon Zucker","value":"9,6 g","indentation":"true"},{"name":"Eiweiss","value":"1,0 g","indentation":""},{"name":"Salz","value":"0,01 g","indentation":""}]',
                'de-ch'=>'[{"name":"Energie","value":"263 kJ/ 63 kcal","indentation":""},{"name":"Fett","value":"0,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,10 g","indentation":"true"},{"name":"Kohlenhydrate","value":"12,7 g","indentation":""},{"name":"davon Zucker","value":"9,6 g","indentation":"true"},{"name":"Eiweiss","value":"1,0 g","indentation":""},{"name":"Salz","value":"0,01 g","indentation":""}]',
                'de-at'=>'[{"name":"Energie","value":"263 kJ/ 63 kcal","indentation":""},{"name":"Fett","value":"0,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,10 g","indentation":"true"},{"name":"Kohlenhydrate","value":"12,7 g","indentation":""},{"name":"davon Zucker","value":"9,6 g","indentation":"true"},{"name":"Eiweiss","value":"1,0 g","indentation":""},{"name":"Salz","value":"0,01 g","indentation":""}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($david->slug).'.CHARACTERISTIC',
                'de-de'=>'["Abwechslungsreicher Gemüsebrei","Mittagsbrei","Kann im Wasserbad erwärmt werden"]',
                'de-ch'=>'["Abwechslungsreicher Gemüsebrei","Mittagsbrei","Kann im Wasserbad erwärmt werden"]',
                'de-at'=>'["Abwechslungsreicher Gemüsebrei","Mittagsbrei","Kann im Wasserbad erwärmt werden"]'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($david->slug).'.PRODUCE-TYPE',
                'de-de'=>'gemüsig',
                'de-ch'=>'gemüsig',
                'de-at'=>'gemüsig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($david->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#D9A57E',
                'de-ch'=>'#D9A57E',
                'de-at'=>'#D9A57E'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($david->slug).'.INFORMATION',
                'de-de'=>'Nicht nur mit seinen Beats sondern auch mit seinen phänomenalen Inhalten überzeugt dieser Brei von sich. Der frische Geschmack weckt “Memories" an früher, bringt aber auch Abwechslung zu gewöhnlichen Gemüsebreien. Besonders lecker schmeckt dieses Brei DJ Set leicht erwärmt und “before the sun goes down".',
                'de-ch'=>'Nicht nur mit seinen Beats sondern auch mit seinen phänomenalen Inhalten überzeugt dieser Brei von sich. Der frische Geschmack weckt “Memories" an früher, bringt aber auch Abwechslung zu gewöhnlichen Gemüsebreien. Besonders lecker schmeckt dieses Brei DJ Set leicht erwärmt und “before the sun goes down".',
                'de-at'=>'Nicht nur mit seinen Beats sondern auch mit seinen phänomenalen Inhalten überzeugt dieser Brei von sich. Der frische Geschmack weckt “Memories" an früher, bringt aber auch Abwechslung zu gewöhnlichen Gemüsebreien. Besonders lecker schmeckt dieses Brei DJ Set leicht erwärmt und “before the sun goes down".'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($david->slug).'.TIME-OF-DAY',
                'de-de'=>'Mittagsbrei',
                'de-ch'=>'Mittagsbrei',
                'de-at'=>'Mittagsbrei'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($david->slug).'.PREP-TYPE',
                'de-de'=>'Kann im Wasserbad erwärmt werden',
                'de-ch'=>'Kann im Wasserbad erwärmt werden',
                'de-at'=>'Kann im Wasserbad erwärmt werden'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($david->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Karotte 58%, Zucchini 10%, Wasser, weisser Traubensaft, Haferflocken 5%, Limettensaft Rapsöl 3%. Enthält Gluten, kann Milch und Sellerie enthalten.',
                'de-ch'=>'Karotte 58%, Zucchini 10%, Wasser, weisser Traubensaft, Haferflocken 5%, Limettensaft Rapsöl 3%. Enthält Gluten, kann Milch und Sellerie enthalten.',
                'de-at'=>'Karotte 58%, Zucchini 10%, Wasser, weisser Traubensaft, Haferflocken 5%, Limettensaft Rapsöl 3%. Enthält Gluten, kann Milch und Sellerie enthalten.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($david->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"317 kJ/ 76 kcal","indentation":""},{"name":"Fett","value":"3,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,28 g","indentation":"true"},{"name":"Kohlenhydrate","value":"8,6 g","indentation":""},{"name":"davon Zucker","value":"5,7 g","indentation":"true"},{"name":"Eiweiss","value":"1,4 g","indentation":""},{"name":"Salz","value":"0,05 g","indentation":""}]',
                'de-ch'=>'[{"name":"Energie","value":"317 kJ/ 76 kcal","indentation":""},{"name":"Fett","value":"3,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,28 g","indentation":"true"},{"name":"Kohlenhydrate","value":"8,6 g","indentation":""},{"name":"davon Zucker","value":"5,7 g","indentation":"true"},{"name":"Eiweiss","value":"1,4 g","indentation":""},{"name":"Salz","value":"0,05 g","indentation":""}]',
                'de-at'=>'[{"name":"Energie","value":"317 kJ/ 76 kcal","indentation":""},{"name":"Fett","value":"3,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,28 g","indentation":"true"},{"name":"Kohlenhydrate","value":"8,6 g","indentation":""},{"name":"davon Zucker","value":"5,7 g","indentation":"true"},{"name":"Eiweiss","value":"1,4 g","indentation":""},{"name":"Salz","value":"0,05 g","indentation":""}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($mango->slug).'.CHARACTERISTIC',
                'de-de'=>'["Exotischer Vitaminmix","Nachmittagsbrei","Zum kühl geniessen"]',
                'de-ch'=>'["Exotischer Vitaminmix","Nachmittagsbrei","Zum kühl geniessen"]',
                'de-at'=>'["Exotischer Vitaminmix","Nachmittagsbrei","Zum kühl geniessen"]'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($mango->slug).'.PRODUCE-TYPE',
                'de-de'=>'fruchtig',
                'de-ch'=>'fruchtig',
                'de-at'=>'fruchtig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($mango->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#FDB933',
                'de-ch'=>'#FDB933',
                'de-at'=>'#FDB933'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($mango->slug).'.INFORMATION',
                'de-de'=>'Ladies and Gentlemen, this is Mango No. 5! Diesem Casanova kann kaum jemand widerstehen. Mit seiner fruchtigen Süsse wickelt er auch Erwachsene um den Finger. Besonders verführerisch wirkt er gekühlt an einem heissen Sommernachmittag.',
                'de-ch'=>'Ladies and Gentlemen, this is Mango No. 5! Diesem Casanova kann kaum jemand widerstehen. Mit seiner fruchtigen Süsse wickelt er auch Erwachsene um den Finger. Besonders verführerisch wirkt er gekühlt an einem heissen Sommernachmittag.',
                'de-at'=>'Ladies and Gentlemen, this is Mango No. 5! Diesem Casanova kann kaum jemand widerstehen. Mit seiner fruchtigen Süsse wickelt er auch Erwachsene um den Finger. Besonders verführerisch wirkt er gekühlt an einem heissen Sommernachmittag.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($mango->slug).'.TIME-OF-DAY',
                'de-de'=>'Nachmittagsbrei',
                'de-ch'=>'Nachmittagsbrei',
                'de-at'=>'Nachmittagsbrei'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($mango->slug).'.PREP-TYPE',
                'de-de'=>'Zum Kühl geniessen',
                'de-ch'=>'Zum Kühl geniessen',
                'de-at'=>'Zum Kühl geniessen'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($mango->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Apfel 49%, Mango 21%, Banane 20%, Fruchtpulver der Acerola-Kirsche. Kann Gluten, Milch und Sellerie enthalten.',
                'de-ch'=>'Apfel 49%, Mango 21%, Banane 20%, Fruchtpulver der Acerola-Kirsche. Kann Gluten, Milch und Sellerie enthalten.',
                'de-at'=>'Apfel 49%, Mango 21%, Banane 20%, Fruchtpulver der Acerola-Kirsche. Kann Gluten, Milch und Sellerie enthalten.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($mango->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"248 kJ/ 59 kcal","indentation":""},{"name":"Fett","value":"0,3 g","indentation":""},{"name":"davon gesättige Fettsäuren**","value":"0,09 g","indentation":"true"},{"name":"Kohlenhydrate", "value":"13,3 g", "indentation":""},{"name":"davon Zucker", "value":"12,7 g", "indentation":"true"},{"name":"Eiweiss", "value":"0,6 g", "indentation":""},{"name":"Salz", "value":"0,02 g", "indentation":""}]',
                'de-ch'=>'[{"name":"Energie","value":"248 kJ/ 59 kcal","indentation":""},{"name":"Fett","value":"0,3 g","indentation":""},{"name":"davon gesättige Fettsäuren**","value":"0,09 g","indentation":"true"},{"name":"Kohlenhydrate", "value":"13,3 g", "indentation":""},{"name":"davon Zucker", "value":"12,7 g", "indentation":"true"},{"name":"Eiweiss", "value":"0,6 g", "indentation":""},{"name":"Salz", "value":"0,02 g", "indentation":""}]',
                'de-at'=>'[{"name":"Energie","value":"248 kJ/ 59 kcal","indentation":""},{"name":"Fett","value":"0,3 g","indentation":""},{"name":"davon gesättige Fettsäuren**","value":"0,09 g","indentation":"true"},{"name":"Kohlenhydrate", "value":"13,3 g", "indentation":""},{"name":"davon Zucker", "value":"12,7 g", "indentation":"true"},{"name":"Eiweiss", "value":"0,6 g", "indentation":""},{"name":"Salz", "value":"0,02 g", "indentation":""}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($beetney->slug).'.CHARACTERISTIC',
                'de-de'=>'["Natürliche Proteine und Eisen","Mittagsbrei","Kann im Wasserbad erwärmt werden"]',
                'de-ch'=>'["Natürliche Proteine und Eisen","Mittagsbrei","Kann im Wasserbad erwärmt werden"]',
                'de-at'=>'["Natürliche Proteine und Eisen","Mittagsbrei","Kann im Wasserbad erwärmt werden"]'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($beetney->slug).'.PRODUCE-TYPE',
                'de-de'=>'gemüsig',
                'de-ch'=>'gemüsig',
                'de-at'=>'gemüsig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($beetney->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#B25364',
                'de-ch'=>'#B25364',
                'de-at'=>'#B25364'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($beetney->slug).'.INFORMATION',
                'de-de'=>'Feed me Baby one more time! Aber Achtung, wer ihn unterschätzt macht einen Fehler, denn unsere Beetney ist ein echter Star. Frau Spears tritt am liebsten Mittags und leicht erwärmt auf, so kann sie die Massen von sich überzeugen. Oops I eat it again.',
                'de-ch'=>'Feed me Baby one more time! Aber Achtung, wer ihn unterschätzt macht einen Fehler, denn unsere Beetney ist ein echter Star. Frau Spears tritt am liebsten Mittags und leicht erwärmt auf, so kann sie die Massen von sich überzeugen. Oops I eat it again.',
                'de-at'=>'Feed me Baby one more time! Aber Achtung, wer ihn unterschätzt macht einen Fehler, denn unsere Beetney ist ein echter Star. Frau Spears tritt am liebsten Mittags und leicht erwärmt auf, so kann sie die Massen von sich überzeugen. Oops I eat it again.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($beetney->slug).'.TIME-OF-DAY',
                'de-de'=>'Mittagsbrei',
                'de-ch'=>'Mittagsbrei',
                'de-at'=>'Mittagsbrei'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($beetney->slug).'.PREP-TYPE',
                'de-de'=>'Kann im Wasserbad erwärmt werden',
                'de-ch'=>'Kann im Wasserbad erwärmt werden',
                'de-at'=>'Kann im Wasserbad erwärmt werden'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($beetney->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Linsen 31%, Birne 30%, Apfel, Rote Beete 5%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten.',
                'de-ch'=>'Linsen 31%, Birne 30%, Apfel, Rote Beete 5%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten.',
                'de-at'=>'Linsen 31%, Birne 30%, Apfel, Rote Beete 5%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($beetney->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"358 kJ/ 82 kcal","indentation":""},{"name":"Fett","value":"0,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,06 g","indentation":"true"},{"name":"Kohlenhydrate","value":"15,2 g","indentation":""},{"name":"davon Zucker","value":"8,3 g","indentation":"true"},{"name":"Eiweiss","value":"3,7 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]',
                'de-ch'=>'[{"name":"Energie","value":"358 kJ/ 82 kcal","indentation":""},{"name":"Fett","value":"0,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,06 g","indentation":"true"},{"name":"Kohlenhydrate","value":"15,2 g","indentation":""},{"name":"davon Zucker","value":"8,3 g","indentation":"true"},{"name":"Eiweiss","value":"3,7 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]',
                'de-at'=>'[{"name":"Energie","value":"358 kJ/ 82 kcal","indentation":""},{"name":"Fett","value":"0,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,06 g","indentation":"true"},{"name":"Kohlenhydrate","value":"15,2 g","indentation":""},{"name":"davon Zucker","value":"8,3 g","indentation":"true"},{"name":"Eiweiss","value":"3,7 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($broccoly->slug).'.CHARACTERISTIC',
                'de-de'=>'["Leckerer Grüngenuss","Mittagsbrei","Kann im Wasserbad erwärmt werden"]',
                'de-ch'=>'["Leckerer Grüngenuss","Mittagsbrei","Kann im Wasserbad erwärmt werden"]',
                'de-at'=>'["Leckerer Grüngenuss","Mittagsbrei","Kann im Wasserbad erwärmt werden"]'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($broccoly->slug).'.PRODUCE-TYPE',
                'de-de'=>'gemüsig',
                'de-ch'=>'gemüsig',
                'de-at'=>'gemüsig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($broccoly->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#8F9834',
                'de-ch'=>'#8F9834',
                'de-at'=>'#8F9834'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($broccoly->slug).'.INFORMATION',
                'de-de'=>'Jeder Champion fängt mal klein an. Er ist vielleicht ein wenig Grün hinter den Blättern aber voller Energie und nicht K.O zu kriegen. Im Gegensatz zu seinem Namensvetter hilft er bei Knochenaufbau und nicht -abbau. Eine besonders gute Figur macht er Mittags im Ring, wenn er schonend erwärmt wird.',
                'de-ch'=>'Jeder Champion fängt mal klein an. Er ist vielleicht ein wenig Grün hinter den Blättern aber voller Energie und nicht K.O zu kriegen. Im Gegensatz zu seinem Namensvetter hilft er bei Knochenaufbau und nicht -abbau. Eine besonders gute Figur macht er Mittags im Ring, wenn er schonend erwärmt wird.',
                'de-at'=>'Jeder Champion fängt mal klein an. Er ist vielleicht ein wenig Grün hinter den Blättern aber voller Energie und nicht K.O zu kriegen. Im Gegensatz zu seinem Namensvetter hilft er bei Knochenaufbau und nicht -abbau. Eine besonders gute Figur macht er Mittags im Ring, wenn er schonend erwärmt wird.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($broccoly->slug).'.TIME-OF-DAY',
                'de-de'=>'Mittagsbrei',
                'de-ch'=>'Mittagsbrei',
                'de-at'=>'Mittagsbrei'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($broccoly->slug).'.PREP-TYPE',
                'de-de'=>'Kann im Wasserbad erwärmt werden',
                'de-ch'=>'Kann im Wasserbad erwärmt werden',
                'de-at'=>'Kann im Wasserbad erwärmt werden'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($broccoly->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Broccoli 33%, Banane 29%, Birne, Grünkohl 5%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten.',
                'de-ch'=>'Broccoli 33%, Banane 29%, Birne, Grünkohl 5%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten.',
                'de-at'=>'Broccoli 33%, Banane 29%, Birne, Grünkohl 5%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($broccoly->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"246 kJ/ 59 kcal","indentation":""},{"name":"Fett","value":"0,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,09 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,9 g","indentation":""},{"name":"davon Zucker","value":"9,9 g","indentation":"true"},{"name":"Eiweiss","value":"1,6 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]',
                'de-ch'=>'[{"name":"Energie","value":"246 kJ/ 59 kcal","indentation":""},{"name":"Fett","value":"0,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,09 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,9 g","indentation":""},{"name":"davon Zucker","value":"9,9 g","indentation":"true"},{"name":"Eiweiss","value":"1,6 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]',
                'de-at'=>'[{"name":"Energie","value":"246 kJ/ 59 kcal","indentation":""},{"name":"Fett","value":"0,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,09 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,9 g","indentation":""},{"name":"davon Zucker","value":"9,9 g","indentation":"true"},{"name":"Eiweiss","value":"1,6 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($anthony->slug).'.CHARACTERISTIC',
                'de-de'=>'["Sättigende Stärkung","Mittagsbrei","Kann im Wasserbad erwärmt werden"]',
                'de-ch'=>'["Sättigende Stärkung","Mittagsbrei","Kann im Wasserbad erwärmt werden"]',
                'de-at'=>'["Sättigende Stärkung","Mittagsbrei","Kann im Wasserbad erwärmt werden"]'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($anthony->slug).'.PRODUCE-TYPE',
                'de-de'=>'gemüsig',
                'de-ch'=>'gemüsig',
                'de-at'=>'gemüsig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($anthony->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#E79D17',
                'de-ch'=>'#E79D17',
                'de-at'=>'#E79D17'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($anthony->slug).'.INFORMATION',
                'de-de'=>'Unser Brei mit dem furchteinflössenden Grinsen wird am Besten bei Tageslicht am Mittag verzehrt. So entfaltet er seine gut sättigende Wirkung. Trotz seiner Stärke ist dieser Brei am Boden geblieben und macht auch aus deinem Baby keinen Kannibalen.',
                'de-ch'=>'Unser Brei mit dem furchteinflössenden Grinsen wird am Besten bei Tageslicht am Mittag verzehrt. So entfaltet er seine gut sättigende Wirkung. Trotz seiner Stärke ist dieser Brei am Boden geblieben und macht auch aus deinem Baby keinen Kannibalen.',
                'de-at'=>'Unser Brei mit dem furchteinflössenden Grinsen wird am Besten bei Tageslicht am Mittag verzehrt. So entfaltet er seine gut sättigende Wirkung. Trotz seiner Stärke ist dieser Brei am Boden geblieben und macht auch aus deinem Baby keinen Kannibalen.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($anthony->slug).'.TIME-OF-DAY',
                'de-de'=>'Mittagsbrei',
                'de-ch'=>'Mittagsbrei',
                'de-at'=>'Mittagsbrei'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($anthony->slug).'.PREP-TYPE',
                'de-de'=>'Kann im Wasserbad erwärmt werden',
                'de-ch'=>'Kann im Wasserbad erwärmt werden',
                'de-at'=>'Kann im Wasserbad erwärmt werden'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($anthony->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Kürbis 56%, Apfel, Kartoffel, Rapsöl 3%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten.',
                'de-ch'=>'Kürbis 56%, Apfel, Kartoffel, Rapsöl 3%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten.',
                'de-at'=>'Kürbis 56%, Apfel, Kartoffel, Rapsöl 3%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($anthony->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"300 kJ/ 72 kcal","indentation":""},{"name":"Fett","value":"3,2 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,28 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,8 g","indentation":""},{"name":"davon Zucker","value":"4,3 g","indentation":"true"},{"name":"Eiweiss","value":"0,7 g","indentation":""},{"name":"Salz","value":"0,01 g","indentation":""}]',
                'de-ch'=>'[{"name":"Energie","value":"300 kJ/ 72 kcal","indentation":""},{"name":"Fett","value":"3,2 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,28 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,8 g","indentation":""},{"name":"davon Zucker","value":"4,3 g","indentation":"true"},{"name":"Eiweiss","value":"0,7 g","indentation":""},{"name":"Salz","value":"0,01 g","indentation":""}]',
                'de-at'=>'[{"name":"Energie","value":"300 kJ/ 72 kcal","indentation":""},{"name":"Fett","value":"3,2 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,28 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,8 g","indentation":""},{"name":"davon Zucker","value":"4,3 g","indentation":"true"},{"name":"Eiweiss","value":"0,7 g","indentation":""},{"name":"Salz","value":"0,01 g","indentation":""}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($sweetHome->slug).'.CHARACTERISTIC',
                'de-de'=>'["Porridge für Babys","Morgenbrei","Zum kühl geniessen"]',
                'de-ch'=>'["Porridge für Babys","Morgenbrei","Zum kühl geniessen"]',
                'de-at'=>'["Porridge für Babys","Morgenbrei","Zum kühl geniessen"]'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($sweetHome->slug).'.PRODUCE-TYPE',
                'de-de'=>'fruchtig',
                'de-ch'=>'fruchtig',
                'de-at'=>'fruchtig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($sweetHome->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#CEBCA5',
                'de-ch'=>'#CEBCA5',
                'de-at'=>'#CEBCA5'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($sweetHome->slug).'.INFORMATION',
                'de-de'=>'Zurück aufs Land entführt dich unser Karaoke-Liebling. Ob kühl oder leicht erwärmt, der Start in den Tag gelingt dir mit diesem gut sättigenden Brei alle Mal. Der frische Joghurt sorgt für ein authentisches Gefühl wie auf dem Bauernhof.',
                'de-ch'=>'Zurück aufs Land entführt dich unser Karaoke-Liebling. Ob kühl oder leicht erwärmt, der Start in den Tag gelingt dir mit diesem gut sättigenden Brei alle Mal. Der frische Joghurt sorgt für ein authentisches Gefühl wie auf dem Bauernhof.',
                'de-at'=>'Zurück aufs Land entführt dich unser Karaoke-Liebling. Ob kühl oder leicht erwärmt, der Start in den Tag gelingt dir mit diesem gut sättigenden Brei alle Mal. Der frische Joghurt sorgt für ein authentisches Gefühl wie auf dem Bauernhof.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($sweetHome->slug).'.TIME-OF-DAY',
                'de-de'=>'Morgenbrei',
                'de-ch'=>'Morgenbrei',
                'de-at'=>'Morgenbrei'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($sweetHome->slug).'.PREP-TYPE',
                'de-de'=>'Zum Kühl geniessen',
                'de-ch'=>'Zum Kühl geniessen',
                'de-at'=>'Zum Kühl geniessen'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($sweetHome->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Joghurt 38%, Banane 38%, Apfel, Haferflocken 8%, Zimt. Enthält Gluten, enthält Milch. Kann Sellerie enthalten.',
                'de-ch'=>'Joghurt 38%, Banane 38%, Apfel, Haferflocken 8%, Zimt. Enthält Gluten, enthält Milch. Kann Sellerie enthalten.',
                'de-at'=>'Joghurt 38%, Banane 38%, Apfel, Haferflocken 8%, Zimt. Enthält Gluten, enthält Milch. Kann Sellerie enthalten.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($sweetHome->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"420 kJ/ 100 kcal","indentation":""},{"name":"Fett","value":"2,1 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,9 g","indentation":"true"},{"name":"Kohlenhydrate","value":"16,1 g","indentation":""},{"name":"davon Zucker","value":"10,6 g","indentation":"true"},{"name":"Eiweiss","value":"3,0 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]',
                'de-ch'=>'[{"name":"Energie","value":"420 kJ/ 100 kcal","indentation":""},{"name":"Fett","value":"2,1 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,9 g","indentation":"true"},{"name":"Kohlenhydrate","value":"16,1 g","indentation":""},{"name":"davon Zucker","value":"10,6 g","indentation":"true"},{"name":"Eiweiss","value":"3,0 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]',
                'de-at'=>'[{"name":"Energie","value":"420 kJ/ 100 kcal","indentation":""},{"name":"Fett","value":"2,1 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,9 g","indentation":"true"},{"name":"Kohlenhydrate","value":"16,1 g","indentation":""},{"name":"davon Zucker","value":"10,6 g","indentation":"true"},{"name":"Eiweiss","value":"3,0 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($cocohontas->slug).'.CHARACTERISTIC',
                'de-de'=>'["Exotischer Proteinmix","Mittagsbrei","Kann im Wasserbad erwärmt werden"]',
                'de-ch'=>'["Exotischer Proteinmix","Mittagsbrei","Kann im Wasserbad erwärmt werden"]',
                'de-at'=>'["Exotischer Proteinmix","Mittagsbrei","Kann im Wasserbad erwärmt werden"]'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($cocohontas->slug).'.PRODUCE-TYPE',
                'de-de'=>'fruchtig',
                'de-ch'=>'fruchtig',
                'de-at'=>'fruchtig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($cocohontas->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#B3C192',
                'de-ch'=>'#B3C192',
                'de-at'=>'#B3C192'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($cocohontas->slug).'.INFORMATION',
                'de-de'=>'Unsere exotische Schönheit erwacht am besten mittags, leicht erwärmt. Das Beste von Fern und Nah vereint in einem Brei. So wie Kokosnussmilch, welche reich an guten Fetten ist, die eisen- und proteinreichen Kichererbsen und der nahrhafte Spinat.',
                'de-ch'=>'Unsere exotische Schönheit erwacht am besten mittags, leicht erwärmt. Das Beste von Fern und Nah vereint in einem Brei. So wie Kokosnussmilch, welche reich an guten Fetten ist, die eisen- und proteinreichen Kichererbsen und der nahrhafte Spinat.',
                'de-at'=>'Unsere exotische Schönheit erwacht am besten mittags, leicht erwärmt. Das Beste von Fern und Nah vereint in einem Brei. So wie Kokosnussmilch, welche reich an guten Fetten ist, die eisen- und proteinreichen Kichererbsen und der nahrhafte Spinat.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($cocohontas->slug).'.TIME-OF-DAY',
                'de-de'=>'Mittagsbrei',
                'de-ch'=>'Mittagsbrei',
                'de-at'=>'Mittagsbrei'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($cocohontas->slug).'.PREP-TYPE',
                'de-de'=>'Kann im Wasserbad erwärmt werden',
                'de-ch'=>'Kann im Wasserbad erwärmt werden',
                'de-at'=>'Kann im Wasserbad erwärmt werden'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($cocohontas->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Birne, Kichererbsen 18%, Kokosmilch 18%, Apfel, Spinat 8%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten.',
                'de-ch'=>'Birne, Kichererbsen 18%, Kokosmilch 18%, Apfel, Spinat 8%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten.',
                'de-at'=>'Birne, Kichererbsen 18%, Kokosmilch 18%, Apfel, Spinat 8%, Limettensaft. Kann Gluten, Milch und Sellerie enthalten.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($cocohontas->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"382 kJ/ 91 kcal","indentation":""},{"name":"Fett","value":"3,9 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"3,1 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,5 g","indentation":""},{"name":"davon Zucker","value":"7,1 g","indentation":"true"},{"name":"Eiweiss","value":"2,3 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]',
                'de-ch'=>'[{"name":"Energie","value":"382 kJ/ 91 kcal","indentation":""},{"name":"Fett","value":"3,9 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"3,1 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,5 g","indentation":""},{"name":"davon Zucker","value":"7,1 g","indentation":"true"},{"name":"Eiweiss","value":"2,3 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]',
                'de-at'=>'[{"name":"Energie","value":"382 kJ/ 91 kcal","indentation":""},{"name":"Fett","value":"3,9 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"3,1 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,5 g","indentation":""},{"name":"davon Zucker","value":"7,1 g","indentation":"true"},{"name":"Eiweiss","value":"2,3 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($inbanana->slug).'.PRODUCE-TYPE',
                'de-de'=>'fruchtig',
                'de-ch'=>'fruchtig',
                'de-at'=>'fruchtig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($inbanana->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#F1CB15',
                'de-ch'=>'#F1CB15',
                'de-at'=>'#F1CB15'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($inbanana->slug).'.INFORMATION',
                'de-de'=>'Unser abenteuerlicher Brei mit Hut und Peitsche hat die leckerste Zutatenkombination entstaubt. Als Jäger des verlorenen Schatzes ist er ständig auf Entdeckungsreisen und daher wunderbar zwischendurch geniessbar.',
                'de-ch'=>'Unser abenteuerlicher Brei mit Hut und Peitsche hat die leckerste Zutatenkombination entstaubt. Als Jäger des verlorenen Schatzes ist er ständig auf Entdeckungsreisen und daher wunderbar zwischendurch geniessbar.',
                'de-at'=>'Unser abenteuerlicher Brei mit Hut und Peitsche hat die leckerste Zutatenkombination entstaubt. Als Jäger des verlorenen Schatzes ist er ständig auf Entdeckungsreisen und daher wunderbar zwischendurch geniessbar.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($inbanana->slug).'.TIME-OF-DAY',
                'de-de'=>'Für zwischendurch',
                'de-ch'=>'Für zwischendurch',
                'de-at'=>'Für zwischendurch'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($inbanana->slug).'.PREP-TYPE',
                'de-de'=>'Zum Kühl geniessen',
                'de-ch'=>'Zum Kühl geniessen',
                'de-at'=>'Zum Kühl geniessen'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($inbanana->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Banane 31%, Mango 30%, Birne 20%, Kokosmilch 14%, Pfirsich 5%. Keine Allergene vorhanden.',
                'de-ch'=>'Banane 31%, Mango 30%, Birne 20%, Kokosmilch 14%, Pfirsich 5%. Keine Allergene vorhanden.',
                'de-at'=>'Banane 31%, Mango 30%, Birne 20%, Kokosmilch 14%, Pfirsich 5%. Keine Allergene vorhanden.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($inbanana->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"352 kJ/ 84 kcal","indentation":""},{"name":"Fett","value":"2,5 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"1,1 g","indentation":"true"},{"name":"Kohlenhydrate","value":"14 g","indentation":""},{"name":"davon Zucker","value":"12 g","indentation":"true"},{"name":"Eiweiss","value":"0,8 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]',
                'de-ch'=>'[{"name":"Energie","value":"352 kJ/ 84 kcal","indentation":""},{"name":"Fett","value":"2,5 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"1,1 g","indentation":"true"},{"name":"Kohlenhydrate","value":"14 g","indentation":""},{"name":"davon Zucker","value":"12 g","indentation":"true"},{"name":"Eiweiss","value":"0,8 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]',
                'de-at'=>'[{"name":"Energie","value":"352 kJ/ 84 kcal","indentation":""},{"name":"Fett","value":"2,5 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"1,1 g","indentation":"true"},{"name":"Kohlenhydrate","value":"14 g","indentation":""},{"name":"davon Zucker","value":"12 g","indentation":"true"},{"name":"Eiweiss","value":"0,8 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($avocado->slug).'.PRODUCE-TYPE',
                'de-de'=>'fruchtig',
                'de-ch'=>'fruchtig',
                'de-at'=>'fruchtig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($avocado->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#C0C972',
                'de-ch'=>'#C0C972',
                'de-at'=>'#C0C972'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($avocado->slug).'.INFORMATION',
                'de-de'=>'Auf der Breitanic ging er unter, bei uns hat er seine leckere Wiederauferstehung. Wie Romeo und Julia vereinen sich Birne und Avocado zu einer wunderbaren Liebesgeschichte. Wie sein Namensvetter, wird dieser Brei noch einige Bab(i)es bezirzen.',
                'de-ch'=>'Auf der Breitanic ging er unter, bei uns hat er seine leckere Wiederauferstehung. Wie Romeo und Julia vereinen sich Birne und Avocado zu einer wunderbaren Liebesgeschichte. Wie sein Namensvetter, wird dieser Brei noch einige Bab(i)es bezirzen.',
                'de-at'=>'Auf der Breitanic ging er unter, bei uns hat er seine leckere Wiederauferstehung. Wie Romeo und Julia vereinen sich Birne und Avocado zu einer wunderbaren Liebesgeschichte. Wie sein Namensvetter, wird dieser Brei noch einige Bab(i)es bezirzen.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($avocado->slug).'.TIME-OF-DAY',
                'de-de'=>'Für zwischendurch',
                'de-ch'=>'Für zwischendurch',
                'de-at'=>'Für zwischendurch'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($avocado->slug).'.PREP-TYPE',
                'de-de'=>'Zum Kühl geniessen',
                'de-ch'=>'Zum Kühl geniessen',
                'de-at'=>'Zum Kühl geniessen'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($avocado->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Birne 90 %, Avocado 10%. Keine Allergene vorhanden.',
                'de-ch'=>'Birne 90 %, Avocado 10%. Keine Allergene vorhanden.',
                'de-at'=>'Birne 90 %, Avocado 10%. Keine Allergene vorhanden.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($avocado->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"276 kJ/ 66 kcal","indentation":""},{"name":"Fett","value":"1,9 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,4 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10 g","indentation":""},{"name":"davon Zucker","value":"9,5 g","indentation":"true"},{"name":"Eiweiss","value":"<0,5 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]',
                'de-ch'=>'[{"name":"Energie","value":"276 kJ/ 66 kcal","indentation":""},{"name":"Fett","value":"1,9 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,4 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10 g","indentation":""},{"name":"davon Zucker","value":"9,5 g","indentation":"true"},{"name":"Eiweiss","value":"<0,5 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]',
                'de-at'=>'[{"name":"Energie","value":"276 kJ/ 66 kcal","indentation":""},{"name":"Fett","value":"1,9 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,4 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10 g","indentation":""},{"name":"davon Zucker","value":"9,5 g","indentation":"true"},{"name":"Eiweiss","value":"<0,5 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($katy->slug).'.PRODUCE-TYPE',
                'de-de'=>'fruchtig',
                'de-ch'=>'fruchtig',
                'de-at'=>'fruchtig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($katy->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#BC4056',
                'de-ch'=>'#BC4056',
                'de-at'=>'#BC4056'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($katy->slug).'.INFORMATION',
                'de-de'=>'Nach dem Genuss unserer Katy wirst du sagen: I kissed a Brei and I liked it! Sie schmeckt am besten kühl und nicht Hot N Cold. Und wenn dein Baby fertig ist, wird es genüsslich ROARR schnurren.',
                'de-ch'=>'Nach dem Genuss unserer Katy wirst du sagen: I kissed a Brei and I liked it! Sie schmeckt am besten kühl und nicht Hot N Cold. Und wenn dein Baby fertig ist, wird es genüsslich ROARR schnurren.',
                'de-at'=>'Nach dem Genuss unserer Katy wirst du sagen: I kissed a Brei and I liked it! Sie schmeckt am besten kühl und nicht Hot N Cold. Und wenn dein Baby fertig ist, wird es genüsslich ROARR schnurren.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($katy->slug).'.TIME-OF-DAY',
                'de-de'=>'Für zwischendurch',
                'de-ch'=>'Für zwischendurch',
                'de-at'=>'Für zwischendurch'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($katy->slug).'.PREP-TYPE',
                'de-de'=>'Zum Kühl geniessen',
                'de-ch'=>'Zum Kühl geniessen',
                'de-at'=>'Zum Kühl geniessen'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($katy->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Apfel 73 %, Erdbeere 23%, Rote Beete 4%. Keine Allergene vorhanden.',
                'de-ch'=>'Apfel 73 %, Erdbeere 23%, Rote Beete 4%. Keine Allergene vorhanden.',
                'de-at'=>'Apfel 73 %, Erdbeere 23%, Rote Beete 4%. Keine Allergene vorhanden.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($katy->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"187 kJ/ 44 kcal","indentation":""},{"name":"Fett","value":"<0,5 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"<0,1 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10 g","indentation":""},{"name":"davon Zucker","value":"9,5 g","indentation":"true"},{"name":"Eiweiss","value":"<0,5 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]',
                'de-ch'=>'[{"name":"Energie","value":"187 kJ/ 44 kcal","indentation":""},{"name":"Fett","value":"<0,5 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"<0,1 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10 g","indentation":""},{"name":"davon Zucker","value":"9,5 g","indentation":"true"},{"name":"Eiweiss","value":"<0,5 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]',
                'de-at'=>'[{"name":"Energie","value":"187 kJ/ 44 kcal","indentation":""},{"name":"Fett","value":"<0,5 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"<0,1 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10 g","indentation":""},{"name":"davon Zucker","value":"9,5 g","indentation":"true"},{"name":"Eiweiss","value":"<0,5 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($peach->slug).'.PRODUCE-TYPE',
                'de-de'=>'fruchtig',
                'de-ch'=>'fruchtig',
                'de-at'=>'fruchtig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($peach->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#F9A980',
                'de-ch'=>'#F9A980',
                'de-at'=>'#F9A980'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($peach->slug).'.INFORMATION',
                'de-de'=>'Die fruchtige Zusammensetzung von Pfirsich, Apfel und Erdbeere lässt die Strand-Melodie in deinem Kopf erklingen. “All Summer long" versorgt er dich mit Vitamin B3, Magnesium, Zink und Ballaststoffen. Danach kann man sich getrost zurücklehnen und den Wellen lauschen.',
                'de-ch'=>'Die fruchtige Zusammensetzung von Pfirsich, Apfel und Erdbeere lässt die Strand-Melodie in deinem Kopf erklingen. “All Summer long" versorgt er dich mit Vitamin B3, Magnesium, Zink und Ballaststoffen. Danach kann man sich getrost zurücklehnen und den Wellen lauschen.',
                'de-at'=>'Die fruchtige Zusammensetzung von Pfirsich, Apfel und Erdbeere lässt die Strand-Melodie in deinem Kopf erklingen. “All Summer long" versorgt er dich mit Vitamin B3, Magnesium, Zink und Ballaststoffen. Danach kann man sich getrost zurücklehnen und den Wellen lauschen.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($peach->slug).'.TIME-OF-DAY',
                'de-de'=>'Nachmittagsbrei',
                'de-ch'=>'Nachmittagsbrei',
                'de-at'=>'Nachmittagsbrei'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($peach->slug).'.PREP-TYPE',
                'de-de'=>'Zum Kühl geniessen',
                'de-ch'=>'Zum Kühl geniessen',
                'de-at'=>'Zum Kühl geniessen'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($peach->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Pfirsich 53%, Apfel 35%, Erdbeere 12%, Fruchtpulver der Acerola-Kirsche. Kann Gluten, Milch und Sellerie enthalten.',
                'de-ch'=>'Pfirsich 53%, Apfel 35%, Erdbeere 12%, Fruchtpulver der Acerola-Kirsche. Kann Gluten, Milch und Sellerie enthalten.',
                'de-at'=>'Pfirsich 53%, Apfel 35%, Erdbeere 12%, Fruchtpulver der Acerola-Kirsche. Kann Gluten, Milch und Sellerie enthalten.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($peach->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"210 kJ/ 50 kcal","indentation":""},{"name":"Fett","value":"0,3 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,04 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,4 g","indentation":""},{"name":"davon Zucker","value":"10,3 g","indentation":"true"},{"name":"Eiweiss","value":"0,5 g","indentation":""},{"name":"Salz","value":"0,01 g","indentation":""}]',
                'de-ch'=>'[{"name":"Energie","value":"210 kJ/ 50 kcal","indentation":""},{"name":"Fett","value":"0,3 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,04 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,4 g","indentation":""},{"name":"davon Zucker","value":"10,3 g","indentation":"true"},{"name":"Eiweiss","value":"0,5 g","indentation":""},{"name":"Salz","value":"0,01 g","indentation":""}]',
                'de-at'=>'[{"name":"Energie","value":"210 kJ/ 50 kcal","indentation":""},{"name":"Fett","value":"0,3 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,04 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,4 g","indentation":""},{"name":"davon Zucker","value":"10,3 g","indentation":"true"},{"name":"Eiweiss","value":"0,5 g","indentation":""},{"name":"Salz","value":"0,01 g","indentation":""}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($prince->slug).'.PRODUCE-TYPE',
                'de-de'=>'fruchtig',
                'de-ch'=>'fruchtig',
                'de-at'=>'fruchtig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($prince->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#DB9E95',
                'de-ch'=>'#DB9E95',
                'de-at'=>'#DB9E95'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($prince->slug).'.INFORMATION',
                'de-de'=>'Unser Prinz bringt royale Stilsicherheit in die yamo Quetschies. Dank königlich leckeren Zutaten und einem Hauch Vanille stehen nicht nur die Jüngsten unter uns sondern auch kreischende Mamas auf den charmanten Gentleman. Man munkelt, selbst die Queen lagere einen kleinen Vorrat in ihrem Kühlschrank.',
                'de-ch'=>'Unser Prinz bringt royale Stilsicherheit in die yamo Quetschies. Dank königlich leckeren Zutaten und einem Hauch Vanille stehen nicht nur die Jüngsten unter uns sondern auch kreischende Mamas auf den charmanten Gentleman. Man munkelt, selbst die Queen lagere einen kleinen Vorrat in ihrem Kühlschrank.',
                'de-at'=>'Unser Prinz bringt royale Stilsicherheit in die yamo Quetschies. Dank königlich leckeren Zutaten und einem Hauch Vanille stehen nicht nur die Jüngsten unter uns sondern auch kreischende Mamas auf den charmanten Gentleman. Man munkelt, selbst die Queen lagere einen kleinen Vorrat in ihrem Kühlschrank.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($prince->slug).'.TIME-OF-DAY',
                'de-de'=>'Für zwischendurch',
                'de-ch'=>'Für zwischendurch',
                'de-at'=>'Für zwischendurch'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($prince->slug).'.PREP-TYPE',
                'de-de'=>'Zum Kühl geniessen',
                'de-ch'=>'Zum Kühl geniessen',
                'de-at'=>'Zum Kühl geniessen'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($prince->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Banane 48%, Erdbeere 42%, Joghurt 10%, Borboun-Vanille Extrakt 0,2%. Enthält Milch.',
                'de-ch'=>'Banane 48%, Erdbeere 42%, Joghurt 10%, Borboun-Vanille Extrakt 0,2%. Enthält Milch.',
                'de-at'=>'Banane 48%, Erdbeere 42%, Joghurt 10%, Borboun-Vanille Extrakt 0,2%. Enthält Milch.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($prince->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"280 kJ/ 66 kcal","indentation":""},{"name":"Fett","value":"0,6 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,3 g","indentation":"true"},{"name":"Kohlenhydrate","value":"12 g","indentation":""},{"name":"davon Zucker","value":"11 g","indentation":"true"},{"name":"Eiweiss","value":"1,3 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]}]',
                'de-ch'=>'[{"name":"Energie","value":"280 kJ/ 66 kcal","indentation":""},{"name":"Fett","value":"0,6 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,3 g","indentation":"true"},{"name":"Kohlenhydrate","value":"12 g","indentation":""},{"name":"davon Zucker","value":"11 g","indentation":"true"},{"name":"Eiweiss","value":"1,3 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]}]',
                'de-at'=>'[{"name":"Energie","value":"280 kJ/ 66 kcal","indentation":""},{"name":"Fett","value":"0,6 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,3 g","indentation":"true"},{"name":"Kohlenhydrate","value":"12 g","indentation":""},{"name":"davon Zucker","value":"11 g","indentation":"true"},{"name":"Eiweiss","value":"1,3 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($nicki->slug).'.PRODUCE-TYPE',
                'de-de'=>'fruchtig',
                'de-ch'=>'fruchtig',
                'de-at'=>'fruchtig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($nicki->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#89AE1B',
                'de-ch'=>'#89AE1B',
                'de-at'=>'#89AE1B'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($nicki->slug).'.INFORMATION',
                'de-de'=>'Wenn Nicki Spinaj ihre Hüllen fallen lässt, entblösst sie knackiges Gemüse und süsse Früchte, die nicht nur Gangster Rapper betören. Im Gegensatz zu ihrer Musik, ist unsere Nicki ein Star aller Altersgruppen.',
                'de-ch'=>'Wenn Nicki Spinaj ihre Hüllen fallen lässt, entblösst sie knackiges Gemüse und süsse Früchte, die nicht nur Gangster Rapper betören. Im Gegensatz zu ihrer Musik, ist unsere Nicki ein Star aller Altersgruppen.',
                'de-at'=>'Wenn Nicki Spinaj ihre Hüllen fallen lässt, entblösst sie knackiges Gemüse und süsse Früchte, die nicht nur Gangster Rapper betören. Im Gegensatz zu ihrer Musik, ist unsere Nicki ein Star aller Altersgruppen.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($nicki->slug).'.TIME-OF-DAY',
                'de-de'=>'Für zwischendurch',
                'de-ch'=>'Für zwischendurch',
                'de-at'=>'Für zwischendurch'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($nicki->slug).'.PREP-TYPE',
                'de-de'=>'Zum Kühl geniessen',
                'de-ch'=>'Zum Kühl geniessen',
                'de-at'=>'Zum Kühl geniessen'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($nicki->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Apfel 62%, Banane 18%, Gurke 12%, Spinat 8%. Keine Allergene.',
                'de-ch'=>'Apfel 62%, Banane 18%, Gurke 12%, Spinat 8%. Keine Allergene.',
                'de-at'=>'Apfel 62%, Banane 18%, Gurke 12%, Spinat 8%. Keine Allergene.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($nicki->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"233 kJ/ 55 kcal","indentation":""},{"name":"Fett","value":"<0,5 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,2 g","indentation":"true"},{"name":"Kohlenhydrate","value":"11 g","indentation":""},{"name":"davon Zucker","value":"9,7 g","indentation":"true"},{"name":"Eiweiss","value":"0,7 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]}]',
                'de-ch'=>'[{"name":"Energie","value":"233 kJ/ 55 kcal","indentation":""},{"name":"Fett","value":"<0,5 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,2 g","indentation":"true"},{"name":"Kohlenhydrate","value":"11 g","indentation":""},{"name":"davon Zucker","value":"9,7 g","indentation":"true"},{"name":"Eiweiss","value":"0,7 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]}]',
                'de-at'=>'[{"name":"Energie","value":"233 kJ/ 55 kcal","indentation":""},{"name":"Fett","value":"<0,5 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,2 g","indentation":"true"},{"name":"Kohlenhydrate","value":"11 g","indentation":""},{"name":"davon Zucker","value":"9,7 g","indentation":"true"},{"name":"Eiweiss","value":"0,7 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($quentin->slug).'.PRODUCE-TYPE',
                'de-de'=>'fruchtig',
                'de-ch'=>'fruchtig',
                'de-at'=>'fruchtig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($quentin->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#EF7E07',
                'de-ch'=>'#EF7E07',
                'de-at'=>'#EF7E07'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($quentin->slug).'.INFORMATION',
                'de-de'=>'Anstatt der langen Dialoge aus seinen Filmen, kommt unser Quentin Carrotino schnell zum Punkt. Eine Geschmacksexplosion, ganz ohne schwarzen Humor dafür mit genau so viel Coolness wie in Pulp Fiction. Definitiv der heisseste Anwärter für die Oscars!',
                'de-ch'=>'Anstatt der langen Dialoge aus seinen Filmen, kommt unser Quentin Carrotino schnell zum Punkt. Eine Geschmacksexplosion, ganz ohne schwarzen Humor dafür mit genau so viel Coolness wie in Pulp Fiction. Definitiv der heisseste Anwärter für die Oscars!',
                'de-at'=>'Anstatt der langen Dialoge aus seinen Filmen, kommt unser Quentin Carrotino schnell zum Punkt. Eine Geschmacksexplosion, ganz ohne schwarzen Humor dafür mit genau so viel Coolness wie in Pulp Fiction. Definitiv der heisseste Anwärter für die Oscars!'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($quentin->slug).'.TIME-OF-DAY',
                'de-de'=>'Für zwischendurch',
                'de-ch'=>'Für zwischendurch',
                'de-at'=>'Für zwischendurch'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($quentin->slug).'.PREP-TYPE',
                'de-de'=>'Zum Kühl geniessen',
                'de-ch'=>'Zum Kühl geniessen',
                'de-at'=>'Zum Kühl geniessen'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($quentin->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Aprikose 34%, Mango 28%, Süsskartoffel 27%, Karotte 11%, Zitronensaft. Keine Allergene.',
                'de-ch'=>'Aprikose 34%, Mango 28%, Süsskartoffel 27%, Karotte 11%, Zitronensaft. Keine Allergene.',
                'de-at'=>'Aprikose 34%, Mango 28%, Süsskartoffel 27%, Karotte 11%, Zitronensaft. Keine Allergene.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($quentin->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"302 kJ/ 71 kcal","indentation":""},{"name":"Fett","value":"<0,5 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"<0,01 g","indentation":"true"},{"name":"Kohlenhydrate","value":"14 g","indentation":""},{"name":"davon Zucker","value":"8,7 g","indentation":"true"},{"name":"Eiweiss","value":"1,1 g","indentation":""},{"name":"Salz","value":"0,03 g","indentation":""}]}]',
                'de-ch'=>'[{"name":"Energie","value":"302 kJ/ 71 kcal","indentation":""},{"name":"Fett","value":"<0,5 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"<0,01 g","indentation":"true"},{"name":"Kohlenhydrate","value":"14 g","indentation":""},{"name":"davon Zucker","value":"8,7 g","indentation":"true"},{"name":"Eiweiss","value":"1,1 g","indentation":""},{"name":"Salz","value":"0,03 g","indentation":""}]}]',
                'de-at'=>'[{"name":"Energie","value":"302 kJ/ 71 kcal","indentation":""},{"name":"Fett","value":"<0,5 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"<0,01 g","indentation":"true"},{"name":"Kohlenhydrate","value":"14 g","indentation":""},{"name":"davon Zucker","value":"8,7 g","indentation":"true"},{"name":"Eiweiss","value":"1,1 g","indentation":""},{"name":"Salz","value":"0,03 g","indentation":""}]}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($cocofield->slug).'.PRODUCE-TYPE',
                'de-de'=>'fruchtig',
                'de-ch'=>'fruchtig',
                'de-at'=>'fruchtig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($cocofield->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#BEA068',
                'de-ch'=>'#BEA068',
                'de-at'=>'#BEA068'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($cocofield->slug).'.INFORMATION',
                'de-de'=>'',
                'de-ch'=>'',
                'de-at'=>''
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($cocofield->slug).'.TIME-OF-DAY',
                'de-de'=>'Für zwischendurch',
                'de-ch'=>'Für zwischendurch',
                'de-at'=>'Für zwischendurch'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($cocofield->slug).'.PREP-TYPE',
                'de-de'=>'Zum Kühl geniessen',
                'de-ch'=>'Zum Kühl geniessen',
                'de-at'=>'Zum Kühl geniessen'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($cocofield->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Apfel 44%, Birne 34%, Kokosmilch 20%, Hirse 2%. Keine Allergene.',
                'de-ch'=>'Apfel 44%, Birne 34%, Kokosmilch 20%, Hirse 2%. Keine Allergene.',
                'de-at'=>'Apfel 44%, Birne 34%, Kokosmilch 20%, Hirse 2%. Keine Allergene.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($cocofield->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"363 kJ/ 87 kcal","indentation":""},{"name":"Fett","value":"3,9 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"1,7 g","indentation":"true"},{"name":"Kohlenhydrate","value":"11 g","indentation":""},{"name":"davon Zucker","value":"8,1 g","indentation":"true"},{"name":"Eiweiss","value":"0,6 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]}]',
                'de-ch'=>'[{"name":"Energie","value":"363 kJ/ 87 kcal","indentation":""},{"name":"Fett","value":"3,9 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"1,7 g","indentation":"true"},{"name":"Kohlenhydrate","value":"11 g","indentation":""},{"name":"davon Zucker","value":"8,1 g","indentation":"true"},{"name":"Eiweiss","value":"0,6 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]}]',
                'de-at'=>'[{"name":"Energie","value":"363 kJ/ 87 kcal","indentation":""},{"name":"Fett","value":"3,9 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"1,7 g","indentation":"true"},{"name":"Kohlenhydrate","value":"11 g","indentation":""},{"name":"davon Zucker","value":"8,1 g","indentation":"true"},{"name":"Eiweiss","value":"0,6 g","indentation":""},{"name":"Salz","value":"<0,01 g","indentation":""}]}]'
            ],
        ]);

        Translation::insert([
            [
                'key-key'=>'PRODUCTS.'.strtoupper($berry->slug).'.PRODUCE-TYPE',
                'de-de'=>'fruchtig',
                'de-ch'=>'fruchtig',
                'de-at'=>'fruchtig'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($berry->slug).'.PRODUCT-NAME-COLOR',
                'de-de'=>'#8173AB',
                'de-ch'=>'#8173AB',
                'de-at'=>'#8173AB'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($berry->slug).'.INFORMATION',
                'de-de'=>'',
                'de-ch'=>'',
                'de-at'=>''
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($berry->slug).'.TIME-OF-DAY',
                'de-de'=>'Für zwischendurch',
                'de-ch'=>'Für zwischendurch',
                'de-at'=>'Für zwischendurch'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($berry->slug).'.PREP-TYPE',
                'de-de'=>'Zum Kühl geniessen',
                'de-ch'=>'Zum Kühl geniessen',
                'de-at'=>'Zum Kühl geniessen'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($berry->slug).'.INGREDIENTS-DESCRIPTION',
                'de-de'=>'Joghurt 40%, Birne 31%, Banane 15%, Heidelbeere 12%, Haferflocken 2%. Enthält Milch und Gluten.',
                'de-ch'=>'Joghurt 40%, Birne 31%, Banane 15%, Heidelbeere 12%, Haferflocken 2%. Enthält Milch und Gluten.',
                'de-at'=>'Joghurt 40%, Birne 31%, Banane 15%, Heidelbeere 12%, Haferflocken 2%. Enthält Milch und Gluten.'
            ],[
                'key-key'=>'PRODUCTS.'.strtoupper($berry->slug).'.TABLE-OF-CONTENTS',
                'de-de'=>'[{"name":"Energie","value":"308 kJ/ 73 kcal","indentation":""},{"name":"Fett","value":"1,8 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"1,0 g","indentation":"true"},{"name":"Kohlenhydrate","value":"11 g","indentation":""},{"name":"davon Zucker","value":"8,4 g","indentation":"true"},{"name":"Eiweiss","value":"2,3 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]}]',
                'de-ch'=>'[{"name":"Energie","value":"308 kJ/ 73 kcal","indentation":""},{"name":"Fett","value":"1,8 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"1,0 g","indentation":"true"},{"name":"Kohlenhydrate","value":"11 g","indentation":""},{"name":"davon Zucker","value":"8,4 g","indentation":"true"},{"name":"Eiweiss","value":"2,3 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]}]',
                'de-at'=>'[{"name":"Energie","value":"308 kJ/ 73 kcal","indentation":""},{"name":"Fett","value":"1,8 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"1,0 g","indentation":"true"},{"name":"Kohlenhydrate","value":"11 g","indentation":""},{"name":"davon Zucker","value":"8,4 g","indentation":"true"},{"name":"Eiweiss","value":"2,3 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]}]'
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('products')->truncate();
        Schema::enableForeignKeyConstraints();
        $pirates = DB::table('oldp_products')->where('slug','pirates-of-the-carrotean')->first();
        $freshPrince = DB::table('oldp_products')->where('slug','fresh-prince-of-bel-pear')->first();
        $applecalypse = DB::table('oldp_products')->where('slug','applecalypse-now')->first();
        $david = DB::table('oldp_products')->where('slug','david-zucchetta')->first();
        $mango = DB::table('oldp_products')->where('slug','mango-no-5')->first();
        $beetney = DB::table('oldp_products')->where('slug','beetney-spears')->first();
        $broccoly = DB::table('oldp_products')->where('slug','broccoly-balboa')->first();
        $anthony = DB::table('oldp_products')->where('slug','anthony-pumpkins')->first();
        $sweetHome = DB::table('oldp_products')->where('slug','sweet-home-albanana')->first();
        $cocohontas = DB::table('oldp_products')->where('slug','cocohontas')->first();
        $inbanana = DB::table('oldp_products')->where('slug','inbanana-jones')->first();
        $avocado = DB::table('oldp_products')->where('slug','avocado-di-caprio')->first();
        $katy = DB::table('oldp_products')->where('slug','katy-berry')->first();
        $peach = DB::table('oldp_products')->where('slug','peach-boys')->first();
        $prince = DB::table('oldp_products')->where('slug','prince-vanilliam')->first();
        $nicki = DB::table('oldp_products')->where('slug','nicki-spinaj')->first();
        $quentin = DB::table('oldp_products')->where('slug','quentin-carrotino')->first();
        $cocofield = DB::table('oldp_products')->where('slug','david-cocofield')->first();
        $berry = DB::table('oldp_products')->where('slug','berry-potter')->first();

        $bib = DB::table('oldp_products')->where('slug','bib')->first();
        $coolingBag = DB::table('oldp_products')->where('slug','kuhltasche')->first();
        $flyer = DB::table('oldp_products')->where('slug','flyer')->first();

        if(!empty($pirates)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($pirates->slug) . '%')->delete();
        if(!empty($freshPrince)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($freshPrince->slug) . '%')->delete();
        if(!empty($applecalypse)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($applecalypse->slug) . '%')->delete();
        if(!empty($david)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($david->slug) . '%')->delete();
        if(!empty($mango)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($mango->slug) . '%')->delete();
        if(!empty($beetney)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($beetney->slug) . '%')->delete();
        if(!empty($broccoly)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($broccoly->slug) . '%')->delete();
        if(!empty($anthony)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($anthony->slug) . '%')->delete();
        if(!empty($sweetHome)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($sweetHome->slug) . '%')->delete();
        if(!empty($cocohontas)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($cocohontas->slug) . '%')->delete();
        if(!empty($inbanana)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($inbanana->slug) . '%')->delete();
        if(!empty($avocado)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($avocado->slug) . '%')->delete();
        if(!empty($katy)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($katy->slug) . '%')->delete();
        if(!empty($peach)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($peach->slug) . '%')->delete();
        if(!empty($prince)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($prince->slug) . '%')->delete();
        if(!empty($nicki)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($nicki->slug) . '%')->delete();
        if(!empty($quentin)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($quentin->slug) . '%')->delete();
        if(!empty($cocofield)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($cocofield->slug) . '%')->delete();
        if(!empty($berry)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($berry->slug) . '%')->delete();
        if(!empty($bib)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($bib->slug) . '%')->delete();
        if(!empty($coolingBag)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($coolingBag->slug) . '%')->delete();
        if(!empty($flyer)) Translation::where('key-key','LIKE','PRODUCTS.' . strtoupper($flyer->slug) . '%')->delete();
    }
}
