<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProwitoOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prowito_manual_orders', function (Blueprint $table) {
            $table->increments('id');
            //string on ext_id to stay flexible
            $table->string('ext_id')->nullable();
            $table->string('name1')->nullable();
            $table->string('name2')->nullable();
            $table->string('adresse')->nullable();
            $table->string('plz')->nullable();
            $table->string('ort')->nullable();
            $table->string('land_iso2')->nullable();
            $table->string('artikelname')->nullable();
            $table->string('email')->nullable();
            $table->string('telefon')->nullable();
            $table->string('kundenreferenz')->nullable();
            $table->string('artikelnummer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('prowito_manual_orders');
    }
}
