<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProducsIdentifiersPoches extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            DB::statement("update products set product_identifier = 'V', updated_at = now() where slug = 'bib';");
            DB::statement("update products set product_identifier = 'C', updated_at = now() where slug = 'inbanana-jones';");
            DB::statement("update products set product_identifier = 'D', updated_at = now() where slug = 'avocado-di-caprio';");
            DB::statement("update products set product_identifier = 'Y', updated_at = now() where slug = 'katy-berry';");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("update products set product_identifier = null, updated_at = now() where slug = 'bib';");
        DB::statement("update products set product_identifier = null, updated_at = now() where slug = 'inbanana-jones';");
        DB::statement("update products set product_identifier = null, updated_at = now() where slug = 'avocado-di-caprio';");
        DB::statement("update products set product_identifier = null, updated_at = now() where slug = 'katy-berry';");
    }
}
