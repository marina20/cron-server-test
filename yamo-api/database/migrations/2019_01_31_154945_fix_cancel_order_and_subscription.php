<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Order;

class FixCancelOrderAndSubscription extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function(Blueprint $table) {
            DB::statement("update orders o1
                  join orders o2 on o1.parent_id = o2.id
                  join subscriptions s on s.order_id = o1.id
                  set o1.status = '" . Order::STATUS_CANCELLED . "', s.status = '" . Order::STATUS_CANCELLED . "'
                  where o2.status = '" . Order::STATUS_CANCELLED . "' and o1.status
                        not in ( '" . Order::STATUS_CANCELLED . "', '" . Order::STATUS_PAYED . "')
                        and s.status = '" . Order::STATUS_PENDING .  "'");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {}
}
