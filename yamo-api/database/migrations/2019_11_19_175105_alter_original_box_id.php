<?php

use App\Models\Box;
use App\Models\SuggestedBox;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOriginalBoxId extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('boxes', function($table) {
            $table->unsignedInteger('original_box_id')->after('id')->nullable();
        });
        $boxes = Box::whereNotNull('original_suggested_box_id')->get();
        foreach($boxes as $box){
            $suggestedBox = SuggestedBox::find($box->original_suggested_box_id);
            $box->original_box_id = $suggestedBox->box_id;
            $box->save();
        }
        Schema::table('boxes', function (Blueprint $table) {
            $table->dropColumn('original_suggested_box_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('boxes', function($table) {
            $table->unsignedInteger('original_suggested_box_id')->after('id')->nullable();
        });

        Schema::table('boxes', function (Blueprint $table) {
            $table->dropColumn('original_box_id');
        });
    }
}
