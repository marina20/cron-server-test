<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Translation as Transmodel;

class MonthTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Transmodel::create(['key-key' => 'COMMON.MONTHS.JANUARY','de-de'=>'Januar','de-ch'=>'Januar','de-at'=>'Januar','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>'fe']) ;
        Transmodel::create(['key-key' => 'COMMON.MONTHS.FEBRUARY','de-de'=>'Februar','de-ch'=>'Februar','de-at'=>'Februar','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>'fe']) ;
        Transmodel::create(['key-key' => 'COMMON.MONTHS.MARCH','de-de'=>'März','de-ch'=>'März','de-at'=>'März','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>'fe']) ;
        Transmodel::create(['key-key' => 'COMMON.MONTHS.APRIL','de-de'=>'April','de-ch'=>'April','de-at'=>'April','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>'fe']) ;
        Transmodel::create(['key-key' => 'COMMON.MONTHS.MAY','de-de'=>'Mai','de-ch'=>'Mai','de-at'=>'Mai','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>'fe']) ;
        Transmodel::create(['key-key' => 'COMMON.MONTHS.JUNE','de-de'=>'Juni','de-ch'=>'Juni','de-at'=>'Juni','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>'fe']) ;
        Transmodel::create(['key-key' => 'COMMON.MONTHS.JULY','de-de'=>'Juli','de-ch'=>'Juli','de-at'=>'Juli','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>'fe']) ;
        Transmodel::create(['key-key' => 'COMMON.MONTHS.AUGUST','de-de'=>'August','de-ch'=>'August','de-at'=>'August','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>'fe']) ;
        Transmodel::create(['key-key' => 'COMMON.MONTHS.SEPTEMBER','de-de'=>'September','de-ch'=>'September','de-at'=>'September','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>'fe']) ;
        Transmodel::create(['key-key' => 'COMMON.MONTHS.OCTOBER','de-de'=>'Oktober','de-ch'=>'Oktober','de-at'=>'Oktober','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>'fe']) ;
        Transmodel::create(['key-key' => 'COMMON.MONTHS.NOVEMBER','de-de'=>'November','de-ch'=>'November','de-at'=>'November','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>'fe']) ;
        Transmodel::create(['key-key' => 'COMMON.MONTHS.DECEMBER','de-de'=>'Dezember','de-ch'=>'Dezember','de-at'=>'Dezember','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>'fe']) ;


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Transmodel::where(['key-key' => 'COMMON.MONTHS.JANUARY'])->first()->delete();
        Transmodel::where(['key-key' => 'COMMON.MONTHS.FEBRUARY'])->first()->delete();
        Transmodel::where(['key-key' => 'COMMON.MONTHS.MARCH'])->first()->delete();
        Transmodel::where(['key-key' => 'COMMON.MONTHS.APRIL'])->first()->delete();
        Transmodel::where(['key-key' => 'COMMON.MONTHS.MAY'])->first()->delete();
        Transmodel::where(['key-key' => 'COMMON.MONTHS.JUNE'])->first()->delete();
        Transmodel::where(['key-key' => 'COMMON.MONTHS.JULY'])->first()->delete();
        Transmodel::where(['key-key' => 'COMMON.MONTHS.AUGUST'])->first()->delete();
        Transmodel::where(['key-key' => 'COMMON.MONTHS.SEPTEMBER'])->first()->delete();
        Transmodel::where(['key-key' => 'COMMON.MONTHS.OCTOBER'])->first()->delete();
        Transmodel::where(['key-key' => 'COMMON.MONTHS.NOVEMBER'])->first()->delete();
        Transmodel::where(['key-key' => 'COMMON.MONTHS.DECEMBER'])->first()->delete();
    }
}