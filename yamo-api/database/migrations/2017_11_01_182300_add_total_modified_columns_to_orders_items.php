<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalModifiedColumnsToOrdersItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function($table) {
            $table->string('total_without_tax')->nullable()->after('subtotal_tax');
            $table->string('total_without_discount')->nullable()->after('total_without_tax');
            $table->string('total_without_discount_tax')->nullable()->after('total_without_discount');
            $table->string('total_discount')->nullable()->after('total_without_discount_tax');
            $table->string('total_discount_tax')->nullable()->after('total_discount');
            $table->boolean('is_discounted')->nullable()->after('tax_rate');
            $table->dropColumn('tax');
            $table->string('total_tax')->nullable()->after('total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_items', function($table) {
            $table->dropColumn('total_without_tax');
            $table->dropColumn('total_without_discount');
            $table->dropColumn('total_without_discount_tax');
            $table->dropColumn('total_discount');
            $table->dropColumn('total_discount_tax');
            $table->dropColumn('is_discounted');
            $table->dropColumn('total_tax');
            $table->string('tax')->nullable()->after('qty');
        });
    }
}
