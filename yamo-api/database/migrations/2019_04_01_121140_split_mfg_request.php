<?php

use App\Models\MfgRequest;
use App\Models\OrderRefund;
use Illuminate\Database\Migrations\Migration;

class SplitMfgRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        set_time_limit(0);
        MfgRequest::chunk(100, function($mfg_requests){
            foreach ($mfg_requests as $mfg) {
                $mfg->split();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $refunds = OrderRefund::where('event_code', MfgRequest::FINANCIAL_CREDIT_REQUEST)->get();
        foreach ($refunds as $refund) {
            $refund->delete();
        }
    }
}
