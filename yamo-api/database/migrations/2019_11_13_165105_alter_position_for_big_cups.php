<?php

use App\Models\Product;
use Illuminate\Database\Migrations\Migration;

class AlterPositionForBigCups extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $bigProducts = Product::where('size', 200)->get();
        foreach($bigProducts as $bigProduct){
            $bigProduct->position += 1;
            $bigProduct->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $bigProducts = Product::where('size', 200)->get();
        foreach($bigProducts as $bigProduct){
            $bigProduct->position = $bigProduct->position-1;
            $bigProduct->save();
        }
    }
}
