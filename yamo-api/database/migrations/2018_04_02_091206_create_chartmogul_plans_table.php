<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChartmogulPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chartmogul_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_id');
            $table->string('data_source_uuid');
            $table->string('name');
            $table->integer('interval_count');
            $table->string('interval_unit');
            $table->string('chartmogul_uuid');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chartmogul_plans');
    }
}
