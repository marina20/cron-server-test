<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Country;
use App\Models\VatGroup;

class InsertIntoVatGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $countryDE = Country::query()->where('content_code',Helpers::LOCALE_DE)->first();
        $countryCH = Country::query()->where('content_code',Helpers::LOCALE_CH)->first();
        $countryAT = Country::query()->where('content_code',Helpers::LOCALE_AT)->first();
        if(empty($countryDE)) {
            $countryDE = Country::create(
                ['id' => 1,
                    'name' => 'Germany',
                    'content_code' => 'de-de',
                    'language' => 'de-de',
                    'currency' => '€',
                    'iso_alpha_2' => 'DE',
                    'iso_alpha_3' => 'DEU',
                    'iso_numeric' => '276'
                ]);
            $countryDE->save();
        }
        if(empty($countryCH)) {
            $countryCH = Country::create([
                'id' => 2,
                'name' => 'Switzerland',
                'content_code' => 'de-ch',
                'language' => 'de-ch',
                'currency' => 'CHF',
                'iso_alpha_2' => 'CH',
                'iso_alpha_3' => 'CHE',
                'iso_numeric' => '756'
            ]);
            $countryCH->save();
        }
        if(empty($countryAT)) {
            $countryAT = Country::firstOrCreate([
                'id' => 3,
                'name' => 'Austria',
                'content_code' => 'de-at',
                'language' => 'de-at',
                'currency' => '€',
                'iso_alpha_2' => 'AT',
                'iso_alpha_3' => 'AUT',
                'iso_numeric' => '040'
            ]);
            $countryAT->save();
        }

        $countryDE = Country::query()->where('content_code',Helpers::LOCALE_DE)->first();
        $countryCH = Country::query()->where('content_code',Helpers::LOCALE_CH)->first();
        $countryAT = Country::query()->where('content_code',Helpers::LOCALE_AT)->first();

        VatGroup::create([
            'value'=>7,
            'description'=>'VAT percentage on food items in Germany',
            'country_id'=>$countryDE->id
        ]);
        VatGroup::create([
            'value'=>19,
            'description'=>'VAT percentage on non food items in Germany',
            'country_id'=>$countryDE->id
        ]);
        VatGroup::create([
            'value'=>2.5,
            'description'=>'VAT percentage on food items in Switzerland',
            'country_id'=>$countryCH->id
        ]);
        VatGroup::create([
            'value'=>7.7,
            'description'=>'VAT percentage on non food items in Switzerland',
            'country_id'=>$countryCH->id
        ]);
        VatGroup::create([
            'value'=>10,
            'description'=>'VAT percentage on food items in Austria',
            'country_id'=>$countryAT->id
        ]);
        VatGroup::create([
            'value'=>20,
            'description'=>'VAT percentage on non food items in Austria',
            'country_id'=>$countryAT->id
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('vat_groups')->truncate();
        Schema::enableForeignKeyConstraints();
    }
}
