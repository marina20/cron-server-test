<?php

use App\Models\Translation;
use App\Models\VatGroup;
use App\Services\CreateOrder\CreateOrder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Models\Order;
use Illuminate\Database\Migrations\Migration;
use App\Traits\FormatMoney;


class InsertSpinocchio extends Migration
{
    use FormatMoney;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Translation::create(['key-key' => 'INGREDIENTS.SEIDENTOFU', 'de-de' => 'Seidentofu', 'de-ch' => 'Seidentofu','de-at' => 'Seidentofu']);
        Translation::create(['key-key' => 'INGREDIENTS.BLUMENKOHL', 'de-de' => 'Blumenkohl', 'de-ch' => 'Blumenkohl','de-at' => 'Blumenkohl']);
        Translation::create(['key-key' => 'MEDIA.IMG-SPINOCCHIO.ALT-TAGS', 'de-de' => 'Spinocchio', 'de-ch' => 'Spinocchio','de-at' => 'Spinocchio']);
        Translation::create(['key-key' => 'PRODUCTS.SPINOCCHIO', 'de-de' => 'Spinocchio', 'de-ch' => 'Spinocchio','de-at' => 'Spinocchio']);
        Translation::create(['key-key' => 'PRODUCTS.SPINOCCHIO.TIME-OF-DAY', 'de-de' => 'Mittagsbrei', 'de-ch' => 'Mittagsbrei','de-at' => 'Mittagsbrei']);
        Translation::create(['key-key' => 'PRODUCTS.SPINOCCHIO.PRODUCE-TYPE', 'de-de' => 'gemüsig', 'de-ch' => 'gemüsig','de-at' => 'gemüsig']);
        Translation::create(['key-key' => 'PRODUCTS.SPINOCCHIO.PREP-TYPE', 'de-de' => 'Kann im Wasserbad erwärmt werden', 'de-ch' => 'Kann im Wasserbad erwärmt werden','de-at' => 'Kann im Wasserbad erwärmt werden']);
        Translation::create(['key-key' => 'PRODUCTS.SPINOCCHIO.TABLE-OF-CONTENTS', 'de-de' => '[{"name":"Energie","value":"354 kJ/ 85 kcal","indentation":""},{"name":"Fett","value":"3,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,33 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,8 g","indentation":""},{"name":"davon Zucker","value":"10,3 g","indentation":"true"},{"name":"Eiweiss","value":"1,6 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]', 'de-ch' => '[{"name":"Energie","value":"354 kJ/ 85 kcal","indentation":""},{"name":"Fett","value":"3,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,33 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,8 g","indentation":""},{"name":"davon Zucker","value":"10,3 g","indentation":"true"},{"name":"Eiweiss","value":"1,6 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]','de-at' => '[{"name":"Energie","value":"354 kJ/ 85 kcal","indentation":""},{"name":"Fett","value":"3,4 g","indentation":""},{"name":"davon gesättige Fettsäuren*","value":"0,33 g","indentation":"true"},{"name":"Kohlenhydrate","value":"10,8 g","indentation":""},{"name":"davon Zucker","value":"10,3 g","indentation":"true"},{"name":"Eiweiss","value":"1,6 g","indentation":""},{"name":"Salz","value":"0,02 g","indentation":""}]']);
        Translation::create(['key-key' => 'PRODUCTS.SPINOCCHIO.INGREDIENTS-DESCRIPTION', 'de-de' => 'Apfel 65%, Seidentofu 15%, Spinat 9%, Blumenkohl 5%, Rapsöl 3%, Limettensaft 3%. Kann Spuren von Milch und Sellerie enthalten.', 'de-ch' => 'Apfel 65%, Seidentofu 15%, Spinat 9%, Blumenkohl 5%, Rapsöl 3%, Limettensaft 3%. Kann Spuren von Milch und Sellerie enthalten.','de-at' => 'Apfel 65%, Seidentofu 15%, Spinat 9%, Blumenkohl 5%, Rapsöl 3%, Limettensaft 3%. Kann Spuren von Milch und Sellerie enthalten.']);
        Translation::create(['key-key' => 'PRODUCTS.SPINOCCHIO.INFORMATION', 'de-de' => 'Spinocchio ist in Zusammenarbeit mit Hiltl, dem ersten vegetarischen Restaurant der Welt, entstanden. Diese Brei-Kreation ist gut als Mittagsmahlzeit geeignet.', 'de-ch' => 'Spinocchio ist in Zusammenarbeit mit Hiltl, dem ersten vegetarischen Restaurant der Welt, entstanden. Diese Brei-Kreation ist gut als Mittagsmahlzeit geeignet.','de-at' => 'Spinocchio ist in Zusammenarbeit mit Hiltl, dem ersten vegetarischen Restaurant der Welt, entstanden. Diese Brei-Kreation ist gut als Mittagsmahlzeit geeignet.']);
        Translation::create(['key-key' => 'PRODUCTS.SPINOCCHIO.PRODUCT-NAME-COLOR', 'de-de' => '#77b182', 'de-ch' => '#77b182','de-at' => '#77b182']);
        Translation::create(['key-key' => 'PRODUCTS.SPINOCCHIO.CHARACTERISTIC', 'de-de' => '["frisch nach Apfel und Spinat und cremig wie Seidentofu","Blumenkohl sehr fein bemerkbar"]', 'de-ch' => '["frisch nach Apfel und Spinat und cremig wie Seidentofu","Blumenkohl sehr fein bemerkbar"]','de-at' => '["frisch nach Apfel und Spinat und cremig wie Seidentofu","Blumenkohl sehr fein bemerkbar"]']);
        Translation::create(['key-key' => 'MEDIA.IMG-SPINOCCHIO.IMG', 'de-de' => 'spinocchio.png', 'de-ch' => 'spinocchio.png','de-at' => 'spinocchio.png']);
        Translation::create(['key-key' => 'MEDIA.IMG-SPINOCCHIO.IMG-THUMB', 'de-de' => 'timg-spinocchio.png', 'de-ch' => 'timg-spinocchio.png','de-at' => 'timg-spinocchio.png']);
        $media = \App\Models\Media::create(['name_key' => 'IMG-SPINOCCHIO']);
        $cupsCategory = \App\Models\Category::where(['name_key' => \App\Models\Product::PRODUCT_CATEGORY_BREIL])->first();
        $appel = \App\Models\Ingredient::where(['name_key' => 'INGREDIENTS.APFEL'])->first();
        $spinach = \App\Models\Ingredient::where(['name_key' => 'INGREDIENTS.SPINAT'])->first();
        $tofu = \App\Models\Ingredient::create(['name_key' => 'INGREDIENTS.SEIDENTOFU']);
        $flowercol = \App\Models\Ingredient::create(['name_key' => 'INGREDIENTS.BLUMENKOHL']);
        $product = \App\Models\Product::create(['name_key' => 'SPINOCCHIO', 'category_id' => $cupsCategory->id, 'product_family' => 'spinocchio', 'product_identifier_letter' => 'T', 'product_identifier_number' => '134', 'size' => '118', 'months' => '6', 'position' => '10400']);
        \App\Models\ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $appel->id]);
        \App\Models\ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $spinach->id]);
        \App\Models\ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $tofu->id]);
        \App\Models\ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $flowercol->id]);
        $country = \App\Models\Country::where(['iso_alpha_2' => \App\Models\Country::COUNTRY_SWITZERLAND])->first();
        $region = \App\Models\Region::where(['country_id' => $country->id])->first();
        $vat = VatGroup::where(['country_id' => $country->id, 'category' => 'food'])->first();
        \App\Models\ProductMedia::create(['media_id' => $media->id, 'product_id' => $product->id]);
        \App\Models\ProductRegion::create(['region_id' => $region->id,'product_id' => $product->id,'vat_group_id' => $vat->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '4.25']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
