<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditExtraProductsTableForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('extra_products', function (Blueprint $table) {
            $table->dropForeign('extra_products_product_id_foreign');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('extra_products', function (Blueprint $table) {
            $table->dropForeign('extra_products_product_id_foreign');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }
}
