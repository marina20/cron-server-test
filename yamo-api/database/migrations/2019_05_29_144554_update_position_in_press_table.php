<?php

use App\Models\Press;
use Illuminate\Database\Migrations\Migration;

class UpdatePositionInPressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $presses = Press::all();
        foreach($presses as $press){
            $press->delete();
        }
        Press::create([
            'link'=>'',
            'meta_title'=>'Coop',
            'meta_alt'=>'Coop',
            'image'=>'coop.png',
            'position'=>1000
        ]);
        Press::create([
            'link'=>'',
            'meta_title'=>'Tagesanzeiger',
            'meta_alt'=>'Tagesanzeiger',
            'image'=>'tagesanzeiger.png',
            'position'=>2000
        ]);
        Press::create([
            'link'=>'',
            'meta_title'=>'20 Minuten',
            'meta_alt'=>'20 Minuten',
            'image'=>'20_minutes.png',
            'position'=>3000
        ]);
        Press::create([
            'link'=>'',
            'meta_title'=>'Swissmom',
            'meta_alt'=>'Swissmom',
            'image'=>'swissmom.png',
            'position'=>4000
        ]);
        Press::create([
            'link'=>'',
            'meta_title'=>'Wir Eltern',
            'meta_alt'=>'Wir Eltern',
            'image'=>'wir_eltern.png',
            'position'=>5000
        ]);
        Press::create([
            'link'=>'',
            'meta_title'=>'Forbes',
            'meta_alt'=>'Forbes',
            'image'=>'forbes.png',
            'position'=>6000
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $presses = Press::all();
        foreach($presses as $press){
            $press->delete();
        }
        Press::create([
            'link'=>'',
            'meta_title'=>'20 Minuten',
            'meta_alt'=>'20 Minuten',
            'image'=>'20_minutes.png'
        ]);
        Press::create([
            'link'=>'',
            'meta_title'=>'Wir Eltern',
            'meta_alt'=>'Wir Eltern',
            'image'=>'wir_eltern.png'
        ]);
        Press::create([
            'link'=>'',
            'meta_title'=>'Forbes',
            'meta_alt'=>'Forbes',
            'image'=>'forbes.png'
        ]);
        Press::create([
            'link'=>'',
            'meta_title'=>'Tagesanzeiger',
            'meta_alt'=>'Tagesanzeiger',
            'image'=>'tagesanzeiger.png'
        ]);
        Press::create([
            'link'=>'',
            'meta_title'=>'Coop',
            'meta_alt'=>'Coop',
            'image'=>'coop.png'
        ]);
        Press::create([
            'link'=>'',
            'meta_title'=>'Swissmom',
            'meta_alt'=>'Swissmom',
            'image'=>'swissmom.png']);
    }
}
