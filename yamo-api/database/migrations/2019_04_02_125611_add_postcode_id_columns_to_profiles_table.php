<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPostcodeIdColumnsToProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('profiles', function(Blueprint $table) {
            $table->unsignedInteger('billing_postcode_id');
            // Before foreign-key, we actually should create postcodes-table..
            // $table->foreign('billing_postcode_id')->references('id')->on('postcodes');
            $table->unsignedInteger('shipping_postcode_id');
           // $table->foreign('shipping_postcode_id')->references('id')->on('postcodes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('profiles', function(Blueprint $table) {
            $table->dropColumn('shipping_postcode_id');
            $table->dropColumn('billing_postcode_id');
        });
    }
}
