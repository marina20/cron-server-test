<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSkuCodOrderTypeIdentifierColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('orders', function (Blueprint $table) {
		    $table->string('SKU')->nullable()->after('referral_candy_aic');
		    $table->string('COD')->nullable()->after('SKU');
		    $table->unsignedInteger('order_type_id')->nullable()->after('COD');
	    });

	    Schema::create('order_type', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('type');
		    $table->timestamps();
	    });
	    DB::table('order_type')->insert([
		    [
			    'id' => 1,
			    'type' => 'B2C',
			    'created_at' => DB::raw('now()'),
			    'updated_at' => DB::raw('now()')
		    ],
		    [
			    'id' => 2,
			    'type' => 'B2B',
			    'created_at' => DB::raw('now()'),
			    'updated_at' => DB::raw('now()')
		    ],
		    [
			    'id' => 3,
			    'type' => 'VIP',
			    'created_at' => DB::raw('now()'),
			    'updated_at' => DB::raw('now()')
		    ]
	    ]);
	    Schema::table('orders', function (Blueprint $table) {
		    $table->foreign('order_type_id')->references('id')->on('order_type');
	    });
	    Schema::table('products', function (Blueprint $table) {
		    $table->char('product_identifier')->nullable()->after('subscription_product_id');
	    });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
	    Schema::table('orders', function ($table){
		    $table->dropColumn('SKU');
		    $table->dropColumn('COD');
            $table->dropForeign(['order_type_id']);
		    $table->dropColumn('order_type_id');
	    });
	    Schema::table('products', function ($table){
		    $table->dropColumn('product_identifier');
	    });
	    Schema::dropIfExists('order_type');
        Schema::enableForeignKeyConstraints();
    }
}
