<?php
use App\Models\VoucherRestrictionType;
use Illuminate\Database\Migrations\Migration;

class InsertDeliveryFrequencyVoucherRestriction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        VoucherRestrictionType::create(['type' => 'frequency']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        VoucherRestrictionType::where(['type' => 'frequency'])->delete();
    }
}
