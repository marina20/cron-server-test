<?php
ini_set('memory_limit','1000M');
use App\Models\Translation;
use App\Models\VatGroup;
use App\Services\CreateOrder\CreateOrder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Models\Order;
use Illuminate\Database\Migrations\Migration;
use App\Traits\FormatMoney;


class AddSkuN extends Migration
{
    use FormatMoney;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $orders = Order::where('status','!=','cancelled')->where('delivery_date', '>', \Carbon\Carbon::today()->addDay(1))->get();
        foreach ($orders as $order){
            \App\Services\OrderService::generateSKU($order);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
