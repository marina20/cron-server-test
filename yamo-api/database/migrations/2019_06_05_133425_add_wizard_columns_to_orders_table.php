<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWizardColumnsToOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function(Blueprint $table) {
            $table->boolean('from_wizard')->nullable()->after('order_type_id');
            $table->boolean('mfg_eligible')->nullable()->after('from_wizard');
            $table->string('wdc_code')->nullable()->after('mfg_eligible');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function(Blueprint $table) {
            $table->dropColumn('from_wizard');
            $table->dropColumn('mfg_eligible');
            $table->dropColumn('wdc_code');
        });
    }
}
