<?php

use App\Models\Ingredient;
use App\Models\Translation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoNewpIngredients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Ingredient::insert([
            ['name_key'=>'INGREDIENTS.KAROTTE', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.BIRNE', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.APFEL', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.DINKEL', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.ZUCCHINI', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.HAFERFLOCKEN', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.BANANE', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.MANGO', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.LINSEN', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.ROTE-BEETE', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.BROCCOLI', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.GRÜNKOHL', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.BUTTERNUSSKÜRBIS', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.KARTOFFEL', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.JOGHURT', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.KICHERERBSEN', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.KOKOSMILCH', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.SPINAT', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.AVOCADO', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.ERDBEERE', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.PFIRSICH', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.VANILLE', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.GURKE', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.APRIKOSE', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.SÜSSKARTOFFEL', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.HIRSE', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['name_key'=>'INGREDIENTS.HEIDELBEEREN', 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
        ]);
        Translation::insert([
            ['key-key'=>'INGREDIENTS.KAROTTE','de-de'=>'Karotte','de-ch'=>'Karotte','de-at'=>'Karotte'],
            ['key-key'=>'INGREDIENTS.BIRNE','de-de'=>'Birne','de-ch'=>'Birne','de-at'=>'Birne'],
            ['key-key'=>'INGREDIENTS.APFEL','de-de'=>'Apfel','de-ch'=>'Apfel','de-at'=>'Apfel'],
            ['key-key'=>'INGREDIENTS.DINKEL','de-de'=>'Dinkel','de-ch'=>'Dinkel','de-at'=>'Dinkel'],
            ['key-key'=>'INGREDIENTS.ZUCCHINI','de-de'=>'Zucchini','de-ch'=>'Zucchini','de-at'=>'Zucchini'],
            ['key-key'=>'INGREDIENTS.HAFERFLOCKEN','de-de'=>'Haferflocken','de-ch'=>'Haferflocken','de-at'=>'Haferflocken'],
            ['key-key'=>'INGREDIENTS.BANANE','de-de'=>'Banane','de-ch'=>'Banane','de-at'=>'Banane'],
            ['key-key'=>'INGREDIENTS.MANGO','de-de'=>'Mango','de-ch'=>'Mango','de-at'=>'Mango'],
            ['key-key'=>'INGREDIENTS.LINSEN','de-de'=>'Linsen','de-ch'=>'Linsen','de-at'=>'Linsen'],
            ['key-key'=>'INGREDIENTS.ROTE-BEETE','de-de'=>'Rote Beete','de-ch'=>'Rote Beete','de-at'=>'Rote Beete'],
            ['key-key'=>'INGREDIENTS.BROCCOLI','de-de'=>'Broccoli','de-ch'=>'Broccoli','de-at'=>'Broccoli'],
            ['key-key'=>'INGREDIENTS.GRÜNKOHL','de-de'=>'Grünkohl','de-ch'=>'Grünkohl','de-at'=>'Grünkohl'],
            ['key-key'=>'INGREDIENTS.BUTTERNUSSKÜRBIS','de-de'=>'Butternusskürbis','de-ch'=>'Butternusskürbis','de-at'=>'Butternusskürbis'],
            ['key-key'=>'INGREDIENTS.KARTOFFEL','de-de'=>'Kartoffel','de-ch'=>'Kartoffel','de-at'=>'Kartoffel'],
            ['key-key'=>'INGREDIENTS.JOGHURT','de-de'=>'Joghurt','de-ch'=>'Joghurt','de-at'=>'Joghurt'],
            ['key-key'=>'INGREDIENTS.KICHERERBSEN','de-de'=>'Kichererbsen','de-ch'=>'Kichererbsen','de-at'=>'Kichererbsen'],
            ['key-key'=>'INGREDIENTS.KOKOSMILCH','de-de'=>'Kokosmilch','de-ch'=>'Kokosmilch','de-at'=>'Kokosmilch'],
            ['key-key'=>'INGREDIENTS.SPINAT','de-de'=>'Spinat','de-ch'=>'Spinat','de-at'=>'Spinat'],
            ['key-key'=>'INGREDIENTS.AVOCADO','de-de'=>'Avocado','de-ch'=>'Avocado','de-at'=>'Avocado'],
            ['key-key'=>'INGREDIENTS.ERDBEERE','de-de'=>'Erdbeere','de-ch'=>'Erdbeere','de-at'=>'Erdbeere'],
            ['key-key'=>'INGREDIENTS.PFIRSICH','de-de'=>'Pfirsich','de-ch'=>'Pfirsich','de-at'=>'Pfirsich'],
            ['key-key'=>'INGREDIENTS.VANILLE','de-de'=>'Vanille','de-ch'=>'Vanille','de-at'=>'Vanille'],
            ['key-key'=>'INGREDIENTS.GURKE','de-de'=>'Gurke','de-ch'=>'Gurke','de-at'=>'Gurke'],
            ['key-key'=>'INGREDIENTS.APRIKOSE','de-de'=>'Aprikose','de-ch'=>'Aprikose','de-at'=>'Aprikose'],
            ['key-key'=>'INGREDIENTS.SÜSSKARTOFFEL','de-de'=>'Süsskartoffel','de-ch'=>'Süsskartoffel','de-at'=>'Süsskartoffel'],
            ['key-key'=>'INGREDIENTS.HIRSE','de-de'=>'Hirse','de-ch'=>'Hirse','de-at'=>'Hirse'],
            ['key-key'=>'INGREDIENTS.HEIDELBEEREN','de-de'=>'Heidelbeeren','de-ch'=>'Heidelbeeren','de-at'=>'Heidelbeeren'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('ingredients')->truncate();
        Schema::enableForeignKeyConstraints();
        Translation::where('key-key','LIKE','INGREDIENTS.%')->delete();
    }
}
