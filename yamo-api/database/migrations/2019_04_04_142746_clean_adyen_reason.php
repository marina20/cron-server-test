<?php

use App\Models\AdyenNotification;
use Illuminate\Database\Migrations\Migration;

class CleanAdyenReason extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        AdyenNotification::where('reason', 'null')->update(['reason' => null]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){}
}
