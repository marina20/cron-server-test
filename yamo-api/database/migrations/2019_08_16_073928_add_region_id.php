<?php

// Workaround for out-of-memory-exception
ini_set('memory_limit','1000M');
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Order;
use App\Models\Region;

class AddRegionId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function($table) {
            $table->unsignedInteger('region_id')->after('note')->nullable();
        });
        $allOrders = Order::all();
        foreach ($allOrders as $order)
        {
            if(!empty($order->country_id)) {
                $region = Region::firstOrCreate(['region' => 'all', 'country_id' => $order->country_id]);
                $region->save();
                $order->region_id = $region->id;
                $order->save();
            }
        }
        $region = Region::firstOrCreate(['region' => 'all', 'country_id' => 1]);
        $region->referrer_reward_cents = 10.00;
        $region->referree_reward_cents = 10.00;
        $region->save();

        $region = Region::firstOrCreate(['region' => 'all', 'country_id' => 2]);
        $region->referrer_reward_cents = 15.00;
        $region->referree_reward_cents = 15.00;
        $region->save();

        $region = Region::firstOrCreate(['region' => 'all', 'country_id' => 3]);
        $region->referrer_reward_cents = 10.00;
        $region->referree_reward_cents = 10.00;
        $region->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function($table) {
            $table->dropColumn('region_id');
        });
    }
}
