<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveVoucherifyVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('voucherify_vouchers');
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('voucherify_vouchers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('voucher_id');
            $table->string('code')->unique();
            $table->string('type');
            $table->boolean('active');
            $table->integer('gift_amount')->nullable();
            $table->integer('gift_balance')->nullable();
            $table->string('discount_type')->nullable();
            $table->integer('discount_amount_off')->comment('This value can be either a percentage or a number depending on discount type.')->nullable();
            $table->integer('reedemmed_counter')->nullable();
            $table->date('external_created_at');
            $table->date('expiration_date')->nullable();
            $table->timestamps();
        });
    }
}
