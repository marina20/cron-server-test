<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\DeliveryFrequency;

class DeliveryFrequencyIcons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $deliveryFrequency1 = DeliveryFrequency::where(['icon' => 'clock-twelve'])->first();
        $deliveryFrequency1->icon = 'delivery-1-week';
        $deliveryFrequency1->save();
        $deliveryFrequency2 = DeliveryFrequency::where(['icon' => 'clock-six'])->first();
        $deliveryFrequency2->icon = 'delivery-2-week';
        $deliveryFrequency2->save();
        $deliveryFrequency3 = DeliveryFrequency::where(['icon' => 'clock'])->first();
        $deliveryFrequency3->icon = 'delivery-4-week';
        $deliveryFrequency3->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $deliveryFrequency1 = DeliveryFrequency::where(['icon' => 'delivery-1-week'])->first();
        $deliveryFrequency1->icon = 'clock-twelve';
        $deliveryFrequency1->save();
        $deliveryFrequency2 = DeliveryFrequency::where(['icon' => 'delivery-2-week'])->first();
        $deliveryFrequency2->icon = 'dclock-six';
        $deliveryFrequency2->save();
        $deliveryFrequency3 = DeliveryFrequency::where(['icon' => 'delivery-4-week'])->first();
        $deliveryFrequency3->icon = 'clock';
        $deliveryFrequency3->save();
    }
}
