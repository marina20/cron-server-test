<?php

use App\Models\Product;
use App\Models\Voucher;
use App\Models\VoucherProduct;
use App\Models\VoucherType;
use Illuminate\Database\Migrations\Migration;

class InsertLoyaltyVouchersIntoVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $loyaltyBib = Voucher::create(['code' => 'LOYALTY_BIB','value_type' => 'product','voucher_type' => VoucherType::TYPE_LOYALTY,'value' => 1.00,'active' => 1]);
        $loyaltyProducts = Voucher::create(['code' => 'LOYALTY_PRODUCTS','value_type' => 'product','voucher_type' => VoucherType::TYPE_LOYALTY,'value' => 2.00,'active' => 1]);
        $loyalty50 = Voucher::create(['code' => 'LOYALTY_50','value_type' => 'percent','voucher_type' => VoucherType::TYPE_LOYALTY,'value' => 50.00,'active' => 1]);

        $productBib = Product::where('name_key','BIB')->first();
        $productNicki = Product::where('name_key','NICKI-SPINAJ')->first();
        $productQuentin = Product::where('name_key','QUENTIN-CARROTINO')->first();

        VoucherProduct::create(['voucher_id'=>$loyaltyBib->id,'product_id'=>$productBib->id,'qty'=>1]);
        VoucherProduct::create(['voucher_id'=>$loyaltyProducts->id,'product_id'=>$productNicki->id,'qty'=>1]);
        VoucherProduct::create(['voucher_id'=>$loyaltyProducts->id,'product_id'=>$productQuentin->id,'qty'=>1]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $loyaltyVouchers = Voucher::where('voucher_type',VoucherType::TYPE_LOYALTY)->get();
        foreach ($loyaltyVouchers as $voucher)
        {
            $voucher->voucherProducts()->delete();
            $voucher->delete();
        }
    }
}
