<?php


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Translation;

class AddQtyToVoucherProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Translation::create(['key-key' => 'VOUCHER.CHECK.AND','de-de'=>'und','de-ch'=>'und','de-at'=>'und','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Schema::table('voucher_products', function($table) {
            $table->integer('qty')->after('product_id')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Translation::where(['key-key' => 'VOUCHER.CHECK.AND'])->delete();
        Schema::table('voucher_products', function (Blueprint $table) {
            $table->dropColumn('qty');
        });
    }
}
