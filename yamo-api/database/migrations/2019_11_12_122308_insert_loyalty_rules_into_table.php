<?php

use App\Models\LoyaltyRule;
use App\Models\Voucher;
use Illuminate\Database\Migrations\Migration;

class InsertLoyaltyRulesIntoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $loyaltyBib = Voucher::where('code','LOYALTY_BIB')->first();
        $loyaltyProducts = Voucher::where('code','LOYALTY_PRODUCTS')->first();
        $loyalty50 = Voucher::where('code','LOYALTY_50')->first();

        LoyaltyRule::create(['box_number'=>2,'voucher_id'=>$loyaltyBib->id,'description'=>'Free Bib']);
        LoyaltyRule::create(['box_number'=>4,'voucher_id'=>$loyaltyProducts->id,'description'=>'2 Surprise products']);
        LoyaltyRule::create(['box_number'=>6,'voucher_id'=>$loyalty50->id,'description'=>'50% discount']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('loyalty_rules')->truncate();
    }
}
