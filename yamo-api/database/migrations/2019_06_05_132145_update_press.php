<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Press;

class UpdatePress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $presses = Press::all();
        foreach($presses as $press){
            $press->delete();
        }
        Press::create(['link'=>'','meta_title'=>"20 Minuten",'meta_alt'=>'20 Minuten','image'=>'20_minuten_new.png']);
        Press::create(['link'=>'','meta_title'=>"Wir Eltern",'meta_alt'=>'Wir Eltern','image'=>'wir_eltern_new.png']);
        Press::create(['link'=>'','meta_title'=>"Forbes",'meta_alt'=>'Forbes','image'=>'forbes_new.png']);
        Press::create(['link'=>'','meta_title'=>"Tagesanzeiger",'meta_alt'=>'Tagesanzeiger','image'=>'tagesanzeiger_new.png']);
        Press::create(['link'=>'','meta_title'=>"Coop",'meta_alt'=>'Coop','image'=>'coop_new.png']);
        Press::create(['link'=>'','meta_title'=>"Swissmom",'meta_alt'=>'Swissmom','image'=>'swissmom_new.png']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
