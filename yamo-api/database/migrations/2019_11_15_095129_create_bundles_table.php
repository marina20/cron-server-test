<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBundlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bundles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('box_id');
            $table->string('name_key');
            $table->unsignedInteger('voucher_id')->nullable();
            $table->boolean('new')->default(false);
            $table->integer('position');
            $table->timestamps();
        });
        Schema::table('bundles', function (Blueprint $table) {
            $table->foreign('box_id')->references('id')->on('boxes');
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->boolean('new')->default(false)->after('description');
        });
        Schema::table('products', function (Blueprint $table) {
            $table->boolean('new')->default(false)->after('months');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('bundles');
        Schema::enableForeignKeyConstraints();
    }
}
