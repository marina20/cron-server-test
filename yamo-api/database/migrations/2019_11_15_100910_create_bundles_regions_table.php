<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBundlesRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bundles_regions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('bundle_id');
            $table->unsignedInteger('region_id');
            $table->timestamp('active_start_date');
            $table->timestamp('active_end_date');
            $table->boolean('active')->default(1);
            $table->integer('out_of_stock')->nullable();
            $table->integer('visible')->default(1);
            $table->timestamps();
        });
        Schema::table('bundles_regions', function (Blueprint $table) {
            $table->foreign('bundle_id')->references('id')->on('bundles');
            $table->foreign('region_id')->references('id')->on('regions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('bundles_regions');
        Schema::enableForeignKeyConstraints();
    }
}
