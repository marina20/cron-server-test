<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCampaignMaxRedemptionsToCouponTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coupons', function($table) {
            $table->integer('campaign_id')->after('expired_at');
            $table->integer('max_redemptions')->after('campaign_id');
            $table->unique('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coupons', function($table) {
            $table->dropUnique('coupons_name_unique');
            $table->dropColumn('campaign_id');
            $table->dropColumn('max_redemptions');
        });
    }
}
