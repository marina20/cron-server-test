<?php
use App\Models\Product;
use App\Models\Translation;
use App\Models\BoxContent;
use Illuminate\Database\Migrations\Migration;
use App\Models\Order;
use App\Services\OrderService;
use App\Models\ProductRegion;
use App\Models\OrderItem;

class ReplacePiratesWithPumpkins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $pirates = Product::where(['product_identifier_letter' => 'R'])->first();
        $pumpkin = Product::where(['product_identifier_letter' => 'P'])->first();

        $orders = Order::where('delivery_date','>=','2019-11-16 00:00:00')->whereNotNull('country_id')->whereNotNull('created_at')->get();
        foreach ($orders as $order){
                OrderItem::where(['order_id' => $order->id])->where(['product_id' => $pirates->id])->update(['item_name' => Translation::trans("PRODUCTS.".$pirates->name_key), 'product_id' => $pumpkin->id]);
                if(!empty($order->custom_box_id)){
                    BoxContent::where(['box_id' => $order->custom_box_id])->where(['product_id' => $pirates->id])->update(['product_id' => $pumpkin->id]);
                }
                OrderService::generateSKU($order);
        }
        ProductRegion::where(['product_id' => $pirates->id])->update(['active' => 0]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
