<?php

use App\Models\Product;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFirstPackageShippment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $countries = \App\Models\Country::all();
        $product = Product::withoutGlobalScope('active')->where('slug','subscription-first-custom-box')->first();
        if(!empty($product)) {
            foreach ($countries as $country) {
                $thePrice = \App\Models\Price::where(['product_id' => $product->id])->where(['country_id' => $country->id])->first();
                if($country->content_code == 'de-de'){
                    $thePrice->shipping = '0.00';
                    $thePrice->shipping_tax = '0.00';
                }
                else if($country->content_code == 'de-at'){
                    $thePrice->shipping = '0.00';
                    $thePrice->shipping_tax = '0.00';
                }
                else if($country->content_code == 'de-ch'){
                    $thePrice->shipping = '0.00';
                    $thePrice->shipping_tax = '0.00';
                }
                $thePrice->save();
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
