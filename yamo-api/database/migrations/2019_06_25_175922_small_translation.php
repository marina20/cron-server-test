<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Translation as Transmodel;

class SmallTranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Transmodel::create(['key-key' => 'MAIL.DELIVERY-REMINDER.DESCRIPTION','de-de'=>'Inhalt deiner Box','de-ch'=>'Inhalt deiner Box','de-at'=>'Inhalt deiner Box','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Transmodel::create(['key-key' => 'MAIL.DELIVERY-REMINDER.SUBTOTAL','de-de'=>'Preis inkl. USt.','de-ch'=>'Preis inkl. MwSt.','de-at'=>'Preis inkl. USt.','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Transmodel::create(['key-key' => 'MAIL.DELIVERY-REMINDER.INCL','de-de'=>'inkl.','de-ch'=>'inkl.','de-at'=>'inkl.','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Transmodel::create(['key-key' => 'MAIL.DELIVERY-REMINDER.MWST','de-de'=>'USt.','de-ch'=>'MwSt.','de-at'=>'USt.','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        $tmpModel = Transmodel::where(['key-key' => 'MAIL.DELIVERY-REMINDER.TOTAL'])->first();
        $tmpModel['de-de'] = "Rechnungsbetrag";
        $tmpModel['de-ch'] = "Rechnungsbetrag";
        $tmpModel['de-at'] = "Rechnungsbetrag";
        $tmpModel->save();
        Transmodel::create(['key-key' => 'MAIL.DELIVERY-REMINDER.FREE','de-de'=>'gratis','de-ch'=>'gratis','de-at'=>'gratis','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}