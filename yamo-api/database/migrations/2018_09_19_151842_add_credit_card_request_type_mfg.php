<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCreditCardRequestTypeMfg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mfg_request', function (Blueprint $table) {
	        DB::statement("update `mfg_request` req join mfg_request_type typ on req.`mfg_request_type_id`= typ.`id` set `mfg_request_type_id` = 1 where `type` = 'card_number_request'");
	        DB::statement("update `mfg_request` req join mfg_request_type typ on req.`mfg_request_type_id`= typ.`id` set `mfg_request_type_id` = 2 where `type` = 'financial_request'");
	        DB::statement("update `mfg_request` req join mfg_request_type typ on req.`mfg_request_type_id`= typ.`id` set `mfg_request_type_id` = 3 where `type` = 'confirmation_request'");
	        DB::statement("update `mfg_request_type` set type = 'financial_request_credit' where id=4");
	        DB::statement("delete from `mfg_request_type` where id > 4");
	        DB::statement("alter table mfg_request_type AUTO_INCREMENT = 5");
	        $table->string('card_number')->nullable()->after('order_id');
	        //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mfg_request', function (Blueprint $table) {
	        $table->dropColumn('card_number');
	        //
        });
    }
}
