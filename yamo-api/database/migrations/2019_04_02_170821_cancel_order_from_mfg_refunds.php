<?php

use App\Models\MfgRequest;
use App\Models\OrderRefund;
use App\Services\OrderService;
use Illuminate\Database\Migrations\Migration;

class CancelOrderFromMfgRefunds extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $refunds = OrderRefund::where('event_code', MfgRequest::FINANCIAL_CREDIT_REQUEST)->get();
        foreach ($refunds as $refund) {
            OrderService::findAndCancelOrderDueToRefund($refund->order_id, MfgRequest::FINANCIAL_CREDIT_REQUEST);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){}
}
