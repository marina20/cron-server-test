<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterOrderItemsTableAddOrdersForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_items', function(Blueprint $table) {
            DB::statement("delete from order_items where order_id not in (select id from orders)");
            DB::statement('alter table order_items modify order_id INT(10) unsigned');
            $table->foreign('order_id')->references('id')->on('orders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('order_items', function(Blueprint $table) {
            $table->dropForeign(['order_id']);
        });
        DB::statement('alter table order_items modify order_id varchar(255)');
        Schema::enableForeignKeyConstraints();
    }
}
