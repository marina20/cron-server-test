<?php

use App\Models\Translation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertFreiYamoClubBibProductThumbnailTranslationIntoTranslationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Translation::create(['key-key' => 'LOYALTY.FREE','de-de'=>'Frei','de-ch'=>'Frei','de-at'=>'Frei','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);

        Translation::where('key-key','MEDIA.IMG-BIB.IMG-THUMB')->delete();
        Translation::create(['key-key' => 'MEDIA.IMG-BIB.IMG-THUMB','de-de'=>'bib_thumbnail.png','de-ch'=>'bib_thumbnail.png','de-at'=>'bib_thumbnail.png','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);

        Translation::create(['key-key' => 'YAMO-CLUB','de-de'=>'Yamo Club','de-ch'=>'Yamo Club','de-at'=>'Yamo Club','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Translation::where('key-key', 'LOYALTY.FREE')->delete();

        Translation::where('key-key','MEDIA.IMG-BIB.IMG-THUMB')->delete();
        Translation::create(['key-key' => 'MEDIA.IMG-BIB.IMG-THUMB','de-de'=>'bib.png','de-ch'=>'bib.png','de-at'=>'bib.png','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);

        Translation::where('key-key', 'YAMO-CLUB')->delete();
    }
}
