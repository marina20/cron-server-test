<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoNewpOrderItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        DB::statement("insert into order_items(item_name,item_type, order_id, product_id, qty, item_price, total, total_tax, subtotal, subtotal_tax, total_discount, total_discount_tax, tax_rate, created_at, updated_at)
                        select
                        distinct 
                        p.name as item_name,
                        oi.item_type as item_type,
                        o.id as order_id,
                        p.id as product_id,
                        bc.`quantity` as qty, 
                        round(oi.total/16,2) as item_price,
                        (bc.`quantity` * round(oi.total/16,2)) as total, 
                        (bc.`quantity` * round(oi.total_tax/16,2)) as total_tax, 
                        (bc.`quantity` * round(oi.total/16,2)) as subtotal, 
                        (bc.`quantity` * round(oi.total_tax/16,2)) as subtotal_tax, 
                        (bc.`quantity` * round(oi.total_discount/16,2)) as total_discount, 
                        (bc.`quantity` * round(oi.total_discount_tax/16,2)) as total_discount_tax, 
                        (select tax_percentage from prices where product_id = oi.product_id and country_id = o.country_id) as tax_rate,
                        oi.created_at,
                        oi.updated_at
                        from orders o  
                        inner join oldp_order_items oi on oi.order_id = o.id
                        inner join boxes b on b.id = oi.product_id
                        inner join box_content bc on bc.box_id = b.id
                        inner join oldp_products p on p.id = bc.product_id
                        where o.custom_box_id is null
                        and o.status != 'cancelled'
                        and date(delivery_date) >= current_date
                        and o.created_via != 'testbox'
                        and oi.item_type != 'shipping'
                        order by o.id desc;");

        DB::statement("insert into order_items(item_name,item_type, order_id, product_id, qty, item_price, total, total_tax, subtotal, subtotal_tax, total_discount, total_discount_tax, tax_rate, created_at, updated_at)
                        select
                        distinct 
                        p.name as item_name,
                        oi.item_type as item_type,
                        o.id as order_id,
                        p.id as product_id,
                        bc.`quantity` as qty, 
                        round(oi.total/16,2) as item_price,
                        (bc.`quantity` * round(oi.total/16,2)) as total, 
                        (bc.`quantity` * round(oi.total_tax/16,2)) as total_tax, 
                        (bc.`quantity` * round(oi.total/16,2)) as subtotal, 
                        (bc.`quantity` * round(oi.total_tax/16,2)) as subtotal_tax, 
                        (bc.`quantity` * round(oi.total_discount/16,2)) as total_discount, 
                        (bc.`quantity` * round(oi.total_discount_tax/16,2)) as total_discount_tax, 
                        (select tax_percentage from prices where product_id = oi.product_id and country_id = o.country_id) as tax_rate,
                        oi.created_at,
                        oi.updated_at
                        from orders o  
                        inner join oldp_order_items oi on oi.order_id = o.id
                        inner join boxes b on b.id = o.custom_box_id
                        inner join box_content bc on bc.box_id = b.id
                        inner join oldp_products p on p.id = bc.product_id
                        where o.custom_box_id is not null
                        and o.status != 'cancelled'
                        and date(delivery_date) >= current_date
                        and o.created_via != 'testbox'
                        and oi.item_type != 'shipping'
                        order by o.id desc;");

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('order_items')->truncate();
        Schema::enableForeignKeyConstraints();
    }
}
