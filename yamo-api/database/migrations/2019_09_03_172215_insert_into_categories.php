<?php

use App\Models\Category;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('categories')->truncate();
        $categoryFoodId = Category::insertGetId([
                'name_key'=>'food',
                'category_id'=>null,
                'description'=>'',
                'created_at'=>DB::raw('NOW()'),
                'updated_at'=>DB::raw('NOW()')
            ]);
        Category::insert([
            'name_key'=>'cups',
            'category_id'=>$categoryFoodId,
            'description'=>'',
            'created_at'=>DB::raw('NOW()'),
            'updated_at'=>DB::raw('NOW()')
        ]);
        Category::insert([
            'name_key'=>'pouches',
            'category_id'=>$categoryFoodId,
            'description'=>'',
            'created_at'=>DB::raw('NOW()'),
            'updated_at'=>DB::raw('NOW()')
        ]);
        Category::insert([
            'name_key'=>'all',
            'category_id'=>$categoryFoodId,
            'description'=>'',
            'created_at'=>DB::raw('NOW()'),
            'updated_at'=>DB::raw('NOW()')
        ]);
        Category::insert([
            'name_key'=>'promo-gift',
            'category_id'=>null,
            'description'=>'',
            'created_at'=>DB::raw('NOW()'),
            'updated_at'=>DB::raw('NOW()')
        ]);
        Category::insert([
            'name_key'=>'flyer',
            'category_id'=>null,
            'description'=>'',
            'created_at'=>DB::raw('NOW()'),
            'updated_at'=>DB::raw('NOW()')
        ]);
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('categories')->truncate();
        Schema::enableForeignKeyConstraints();
    }
}
