<?php

use App\Models\Country;
use App\Models\Product;
use App\Models\ProductRegion;
use App\Models\Region;
use App\Models\VatGroup;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function($table) {
            $table->float('public_discount')->after('discount')->default(0);
            $table->float('public_discount_tax')->after('public_discount')->default(0);
            $table->float('system_discount')->after('discount')->default(0);
            $table->float('system_discount_tax')->after('system_discount')->default(0);
            $table->float('wallet_discount')->after('discount')->default(0);
            $table->float('wallet_discount_tax')->after('wallet_discount')->default(0);
        });

        Schema::table('order_items', function($table) {
            $table->float('public_discount')->after('total_discount')->default(0);
            $table->float('public_discount_tax')->after('public_discount')->default(0);
            $table->float('system_discount')->after('total_discount')->default(0);
            $table->float('system_discount_tax')->after('system_discount')->default(0);
            $table->float('wallet_discount')->after('total_discount')->default(0);
            $table->float('wallet_discount_tax')->after('wallet_discount')->default(0);
        });

        Schema::table('vat_groups', function($table) {
            $table->string('category')->after('description')->default('');
        });
        $vatGroupNG = VatGroup::where(['description' => 'VAT percentage on non food items in Germany'])->first();
        $vatGroupNG->category = 'non-food';
        $vatGroupNG->save();
        $vatGroupNS = VatGroup::where(['description' => 'VAT percentage on non food items in Switzerland'])->first();
        $vatGroupNS->category = 'non-food';
        $vatGroupNS->save();
        $vatGroupNA = VatGroup::where(['description' => 'VAT percentage on non food items in Austria'])->first();
        $vatGroupNA->category = 'non-food';
        $vatGroupNA->save();

        $vatGroupFG = VatGroup::where(['description' => 'VAT percentage on food items in Germany'])->first();
        $vatGroupFG->category = 'food';
        $vatGroupFG->save();
        $vatGroupFS = VatGroup::where(['description' => 'VAT percentage on food items in Switzerland'])->first();
        $vatGroupFS->category = 'food';
        $vatGroupFS->save();
        $vatGroupFA = VatGroup::where(['description' => 'VAT percentage on food items in Austria'])->first();
        $vatGroupFA->category = 'food';
        $vatGroupFA->save();

        $de = Country::where('content_code', Helpers::LOCALE_DE)->first()->id;
        $ch = Country::where('content_code', Helpers::LOCALE_CH)->first()->id;
        $at = Country::where('content_code', Helpers::LOCALE_AT)->first()->id;

        $rde = Region::where(['country_id' => $de])->first()->id;
        $rch = Region::where(['country_id' => $ch])->first()->id;
        $rat = Region::where(['country_id' => $at])->first()->id;

        $product = Product::withoutGlobalScope('active')->where(['product_identifier_letter' => 'V'])->first();
        ProductRegion::create(['product_id' => $product->id,'region_id' => $rde, 'vat_group_id' => $vatGroupNG->id, 'price' => '0.00', 'active' => 1,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12']);
        ProductRegion::create(['product_id' => $product->id,'region_id' => $rch, 'vat_group_id' => $vatGroupNS->id, 'price' => '0.00', 'active' => 1,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12']);
        ProductRegion::create(['product_id' => $product->id,'region_id' => $rat, 'vat_group_id' => $vatGroupNA->id, 'price' => '0.00', 'active' => 1,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('vat_groups', function (Blueprint $table) {
            $table->dropColumn('category');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('public_discount');
            $table->dropColumn('public_discount_tax');
            $table->dropColumn('system_discount');
            $table->dropColumn('system_discount_tax');
            $table->dropColumn('wallet_discount');
            $table->dropColumn('wallet_discount_tax');
        });
        Schema::table('order_items', function (Blueprint $table) {
            $table->dropColumn('public_discount');
            $table->dropColumn('public_discount_tax');
            $table->dropColumn('system_discount');
            $table->dropColumn('system_discount_tax');
            $table->dropColumn('wallet_discount');
            $table->dropColumn('wallet_discount_tax');
        });
    }
}
