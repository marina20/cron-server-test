<?php

use App\Models\Translation;
use App\Models\VatGroup;
use App\Services\CreateOrder\CreateOrder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Models\Order;
use \App\Models\Product;
use \App\Models\ProductIngredient;
use Illuminate\Database\Migrations\Migration;
use App\Traits\FormatMoney;
use \App\Models\Ingredient;
use \App\Models\Voucher;
use \App\Models\ProductMedia;
use \App\Models\ProductRegion;


class InsertVoucherTranslations extends Migration
{
    use FormatMoney;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Translation::create(['key-key' => 'VOUCHER.CHECK.START_PRODUCTPERCENT_SUCCESS','de-de'=>'Bei deiner Bestellung wird dir kostenlos ein','de-ch'=>'Bei deiner Bestellung wird dir kostenlos ein','de-at'=>'Bei deiner Bestellung wird dir kostenlos ein','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'VOUCHER.CHECK.MIDDLE_PRODUCTPERCENT_SUCCESS','de-de'=>'hinzugefügt und bis zu','de-ch'=>'hinzugefügt und bis zu','de-at'=>'hinzugefügt und bis zu','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
        Translation::create(['key-key' => 'VOUCHER.CHECK.END_PRODUCTPERCENT_SUCCESS','de-de'=>'abgezogen.','de-ch'=>'abgezogen.','de-at'=>'abgezogen.','en-uk'=>NULL,'nl-nl'=>NULL,'notes'=>NULL,'type'=>NULL]);
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
