<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProfilesTableAddUsersForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function(Blueprint $table) {
            DB::statement("delete from profiles where user_id not in (select id from users)");
            DB::statement('alter table profiles modify user_id INT(10) unsigned');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('profiles', function(Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        DB::statement('alter table profiles modify user_id varchar(255)');
        Schema::enableForeignKeyConstraints();
    }
}
