<?php

use App\Models\Translation;
use App\Models\VatGroup;
use App\Services\CreateOrder\CreateOrder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use App\Models\Order;
use \App\Models\Product;
use \App\Models\ProductIngredient;
use Illuminate\Database\Migrations\Migration;
use App\Traits\FormatMoney;
use \App\Models\Ingredient;
use \App\Models\Media;
use \App\Models\ProductMedia;
use \App\Models\ProductRegion;


class InsertHeavyPouches extends Migration
{
    use FormatMoney;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $countryCH = \App\Models\Country::where(['iso_alpha_2' => \App\Models\Country::COUNTRY_SWITZERLAND])->first();
        $regionCH = \App\Models\Region::where(['country_id' => $countryCH->id])->first();
        $vatCH = VatGroup::where(['country_id' => $countryCH->id, 'category' => 'food'])->first();

        $countryDE = \App\Models\Country::where(['iso_alpha_2' => \App\Models\Country::COUNTRY_GERMANY])->first();
        $regionDE = \App\Models\Region::where(['country_id' => $countryDE->id])->first();
        $vatDE = VatGroup::where(['country_id' => $countryDE->id, 'category' => 'food'])->first();

        $countryAT = \App\Models\Country::where(['iso_alpha_2' => \App\Models\Country::COUNTRY_AUSTRIA])->first();
        $regionAT = \App\Models\Region::where(['country_id' => $countryAT->id])->first();
        $vatAT = VatGroup::where(['country_id' => $countryAT->id, 'category' => 'food'])->first();

        $cupsCategory = \App\Models\Category::where(['name_key' => Product::PRODUCT_CATEGORY_BREIL])->first();
        $appel = Ingredient::where(['name_key' => 'INGREDIENTS.APFEL'])->first();
        $spinach = Ingredient::where(['name_key' => 'INGREDIENTS.SPINAT'])->first();
        $linsen = Ingredient::where(['name_key' => 'INGREDIENTS.LINSEN'])->first();
        $birne = Ingredient::where(['name_key' => 'INGREDIENTS.BIRNE'])->first();
        $roteBeete = Ingredient::where(['name_key' => 'INGREDIENTS.ROTE-BEETE'])->first();
        $broccoli = Ingredient::where(['name_key' => 'INGREDIENTS.BROCCOLI'])->first();
        $banane = Ingredient::where(['name_key' => 'INGREDIENTS.BANANE'])->first();
        $grunkohl = Ingredient::where(['name_key' => 'INGREDIENTS.GRÜNKOHL'])->first();
        $cocosmilk = Ingredient::where(['name_key' => 'INGREDIENTS.KOKOSMILCH'])->first();
        $kichererbsen = Ingredient::where(['name_key' => 'INGREDIENTS.KICHERERBSEN'])->first();
        $karotte = Ingredient::where(['name_key' => 'INGREDIENTS.KAROTTE'])->first();
        $zucchini = Ingredient::where(['name_key' => 'INGREDIENTS.ZUCCHINI'])->first();
        $haferflocken = Ingredient::where(['name_key' => 'INGREDIENTS.HAFERFLOCKEN'])->first();
        $mango = Ingredient::where(['name_key' => 'INGREDIENTS.MANGO'])->first();
        $joghurt = Ingredient::where(['name_key' => 'INGREDIENTS.JOGHURT'])->first();


        $originalKey = 'BEETNEY-SPEARS';
        $product = Product::create(['name_key' => $originalKey.'-200', 'category_id' => $cupsCategory->id, 'product_family' => 'beetney-spears', 'product_identifier_letter' => 'LB', 'product_identifier_number' => '145', 'size' => '200', 'months' => '6', 'position' => '9200']);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $linsen->id]);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $birne->id]);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $roteBeete->id]);

        $media = Media::create(['name_key' => 'IMG-'.$product->name_key]);
        ProductMedia::create(['media_id' => $media->id, 'product_id' => $product->id]);
        ProductRegion::create(['region_id' => $regionCH->id,'product_id' => $product->id,'vat_group_id' => $vatCH->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '5.65']);
        ProductRegion::create(['region_id' => $regionDE->id,'product_id' => $product->id,'vat_group_id' => $vatDE->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '3.95']);
        ProductRegion::create(['region_id' => $regionAT->id,'product_id' => $product->id,'vat_group_id' => $vatAT->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '4.10']);

        $this->cloneTranslations($originalKey);

        $originalKey = 'BROCCOLY-BALBOA';
        $product = Product::create(['name_key' => $originalKey.'-200', 'category_id' => $cupsCategory->id, 'product_family' => 'broccoly-balboa', 'product_identifier_letter' => 'FB', 'product_identifier_number' => '144', 'size' => '200', 'months' => '6', 'position' => '9300']);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $broccoli->id]);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $banane->id]);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $grunkohl->id]);

        $media = Media::create(['name_key' => 'IMG-'.$product->name_key]);
        ProductMedia::create(['media_id' => $media->id, 'product_id' => $product->id]);
        ProductRegion::create(['region_id' => $regionCH->id,'product_id' => $product->id,'vat_group_id' => $vatCH->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '5.65']);
        ProductRegion::create(['region_id' => $regionDE->id,'product_id' => $product->id,'vat_group_id' => $vatDE->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '3.95']);
        ProductRegion::create(['region_id' => $regionAT->id,'product_id' => $product->id,'vat_group_id' => $vatAT->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '4.10']);

        $this->cloneTranslations($originalKey);

        $originalKey = 'COCOHONTAS';
        $product = Product::create(['name_key' => $originalKey.'-200', 'category_id' => $cupsCategory->id, 'product_family' => 'cocohontas', 'product_identifier_letter' => 'SB', 'product_identifier_number' => '146', 'size' => '200', 'months' => '6', 'position' => '9500']);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $kichererbsen->id]);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $cocosmilk->id]);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $spinach->id]);

        $media = Media::create(['name_key' => 'IMG-'.$product->name_key]);
        ProductMedia::create(['media_id' => $media->id, 'product_id' => $product->id]);
        ProductRegion::create(['region_id' => $regionCH->id,'product_id' => $product->id,'vat_group_id' => $vatCH->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '5.65']);
        ProductRegion::create(['region_id' => $regionDE->id,'product_id' => $product->id,'vat_group_id' => $vatDE->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '3.95']);
        ProductRegion::create(['region_id' => $regionAT->id,'product_id' => $product->id,'vat_group_id' => $vatAT->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '4.10']);

        $this->cloneTranslations($originalKey);

        $originalKey = 'DAVID-ZUCCHETTA';
        $product = Product::create(['name_key' => $originalKey.'-200', 'category_id' => $cupsCategory->id, 'product_family' => 'david-zucchetta', 'product_identifier_letter' => 'ZB', 'product_identifier_number' => '141', 'size' => '200', 'months' => '6', 'position' => '9400']);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $karotte->id]);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $zucchini->id]);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $haferflocken->id]);

        $media = Media::create(['name_key' => 'IMG-'.$product->name_key]);
        ProductMedia::create(['media_id' => $media->id, 'product_id' => $product->id]);
        ProductRegion::create(['region_id' => $regionCH->id,'product_id' => $product->id,'vat_group_id' => $vatCH->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '5.65']);
        ProductRegion::create(['region_id' => $regionDE->id,'product_id' => $product->id,'vat_group_id' => $vatDE->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '3.95']);
        ProductRegion::create(['region_id' => $regionAT->id,'product_id' => $product->id,'vat_group_id' => $vatAT->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '4.10']);

        $this->cloneTranslations($originalKey);

        $originalKey = 'MANGO-NO-5';
        $product = Product::create(['name_key' => $originalKey.'-200', 'category_id' => $cupsCategory->id, 'product_family' => 'mango-no-5', 'product_identifier_letter' => 'MB', 'product_identifier_number' => '142', 'size' => '200', 'months' => '6', 'position' => '9000']);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $appel->id]);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $banane->id]);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $mango->id]);

        $media = Media::create(['name_key' => 'IMG-'.$product->name_key]);
        ProductMedia::create(['media_id' => $media->id, 'product_id' => $product->id]);
        ProductRegion::create(['region_id' => $regionCH->id,'product_id' => $product->id,'vat_group_id' => $vatCH->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '5.65']);
        ProductRegion::create(['region_id' => $regionDE->id,'product_id' => $product->id,'vat_group_id' => $vatDE->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '3.95']);
        ProductRegion::create(['region_id' => $regionAT->id,'product_id' => $product->id,'vat_group_id' => $vatAT->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '4.10']);

        $this->cloneTranslations($originalKey);

        $originalKey = 'SWEET-HOME-ALBANANA';
        $product = Product::create(['name_key' => $originalKey.'-200', 'category_id' => $cupsCategory->id, 'product_family' => 'sweet-home-albanana', 'product_identifier_letter' => 'HB', 'product_identifier_number' => '143', 'size' => '200', 'months' => '6', 'position' => '9100']);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $joghurt->id]);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $banane->id]);
        ProductIngredient::create(['product_id' => $product->id, 'ingredient_id' => $haferflocken->id]);


        $media = Media::create(['name_key' => 'IMG-'.$product->name_key]);
        ProductMedia::create(['media_id' => $media->id, 'product_id' => $product->id]);
        ProductRegion::create(['region_id' => $regionCH->id,'product_id' => $product->id,'vat_group_id' => $vatCH->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '5.65']);
        ProductRegion::create(['region_id' => $regionDE->id,'product_id' => $product->id,'vat_group_id' => $vatDE->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '3.95']);
        ProductRegion::create(['region_id' => $regionAT->id,'product_id' => $product->id,'vat_group_id' => $vatAT->id,'active_start_date' => '2019-08-01 12:12:12','active_end_date' => '2020-08-01 12:12:12', 'active' => 1,'price' => '4.10']);

        $this->cloneTranslations($originalKey);
    }


    private function cloneTranslations($originalKey){
        echo $originalKey;
        $t = Translation::where(['key-key' => 'PRODUCTS.'.$originalKey])->first()->replicate();
        $t['key-key'] = 'PRODUCTS.'.$originalKey.'-200';
        $t->save();
        $t = Translation::where(['key-key' => 'PRODUCTS.'.$originalKey.'.TIME-OF-DAY'])->first()->replicate();
        $t['key-key'] = 'PRODUCTS.'.$originalKey.'-200.TIME-OF-DAY';
        $t->save();
        $t = Translation::where(['key-key' => 'PRODUCTS.'.$originalKey.'.PRODUCE-TYPE'])->first()->replicate();
        $t['key-key'] = 'PRODUCTS.'.$originalKey.'-200.PRODUCE-TYPE';
        $t->save();
        $t = Translation::where(['key-key' => 'PRODUCTS.'.$originalKey.'.PREP-TYPE'])->first()->replicate();
        $t['key-key'] = 'PRODUCTS.'.$originalKey.'-200.PREP-TYPE';
        $t->save();
        $t = Translation::where(['key-key' => 'PRODUCTS.'.$originalKey.'.TABLE-OF-CONTENTS'])->first()->replicate();
        $t['key-key'] = 'PRODUCTS.'.$originalKey.'-200.TABLE-OF-CONTENTS';
        $t->save();
        $t = Translation::where(['key-key' => 'PRODUCTS.'.$originalKey.'.INGREDIENTS-DESCRIPTION'])->first()->replicate();
        $t['key-key'] = 'PRODUCTS.'.$originalKey.'-200.INGREDIENTS-DESCRIPTION';
        $t->save();
        $t = Translation::where(['key-key' => 'PRODUCTS.'.$originalKey.'.INFORMATION'])->first()->replicate();
        $t['key-key'] = 'PRODUCTS.'.$originalKey.'-200.INFORMATION';
        $t->save();
        $t = Translation::where(['key-key' => 'PRODUCTS.'.$originalKey.'.PRODUCT-NAME-COLOR'])->first()->replicate();
        $t['key-key'] = 'PRODUCTS.'.$originalKey.'-200.PRODUCT-NAME-COLOR';
        $t->save();
        $t = Translation::where(['key-key' => 'PRODUCTS.'.$originalKey.'.CHARACTERISTIC'])->first()->replicate();
        $t['key-key'] = 'PRODUCTS.'.$originalKey.'-200.CHARACTERISTIC';
        $t->save();
        $t = Translation::where(['key-key' => 'MEDIA.IMG-'.$originalKey.'.ALT-TAGS'])->first()->replicate();
        $t['key-key'] = 'MEDIA.IMG-'.$originalKey.'-200.ALT-TAGS';
        $t->save();
        $t = Translation::where(['key-key' => 'MEDIA.IMG-'.$originalKey.'.IMG'])->first()->replicate();
        $t['key-key'] = 'MEDIA.IMG-'.$originalKey.'-200.IMG';
        $t['de-de'] = strtolower($originalKey).'-200.png';
        $t['de-ch'] = strtolower($originalKey).'-200.png';
        $t['de-at'] = strtolower($originalKey).'-200.png';
        $t->save();
        $t = Translation::where(['key-key' => 'MEDIA.IMG-'.$originalKey.'.IMG-THUMB'])->first()->replicate();
        $t['key-key'] = 'MEDIA.IMG-'.$originalKey.'-200.IMG-THUMB';
        $t['de-de'] = 'timg-'.strtolower($originalKey).'-200.png';
        $t['de-ch'] = 'timg-'.strtolower($originalKey).'-200.png';
        $t['de-at'] = 'timg-'.strtolower($originalKey).'-200.png';
        $t->save();
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
