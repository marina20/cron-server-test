<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTestimonialsTableExchangeOskarWithIsabell extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('testimonials', function (Blueprint $table) {
            DB::statement("update testimonials set image = 'isabell-testimonial-120x120.png', description = 'Yamo kann ich mit gutem Gewissen weiterempfehlen. Vor allem zur Entlastung in hektischen Phasen, in denen Zeit rar ist, ist er die beste Alternative.', customer_name = 'Isabell Ben Nescher', customer_details = 'Hebamme, Mami von 3 Kindern', updated_at = now() where id = 1;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('testimonials', function (Blueprint $table) {
            DB::statement("update testimonials set image = 'oskar-testimonial-120x120.png', description = 'yamo ist eine super Alternative zu selbstgemachtem Brei.', customer_name = 'Dr. med. Oskar Baenziger', customer_details = 'Facharzt für Kinder- und Jugendmedizin', updated_at = now() where id = 1;");
        });
    }
}
