<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHouseNr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function($table) {
            $table->string('shipping_street_name')->after('shipping_address_2')->nullable();
            $table->string('billing_street_name')->after('billing_address_2')->nullable();
            $table->string('shipping_street_nr')->after('shipping_street_name')->nullable();
            $table->string('billing_street_nr')->after('billing_street_name')->nullable();
        });
        Schema::table('orders', function($table) {
            $table->string('shipping_street_name')->nullable();
            $table->string('billing_street_name')->nullable();
            $table->string('shipping_street_nr')->nullable();
            $table->string('billing_street_nr')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function($table) {
            $table->dropColumn('shipping_street_name');
            $table->dropColumn('billing_street_name');
            $table->dropColumn('shipping_street_nr');
            $table->dropColumn('billing_street_nr');
        });
        Schema::table('orders', function($table) {
            $table->dropColumn('shipping_street_name');
            $table->dropColumn('billing_street_name');
            $table->dropColumn('shipping_street_nr');
            $table->dropColumn('billing_street_nr');
        });
    }
}
