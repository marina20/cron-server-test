<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMfgRequestType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mfg_request_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->timestamps();
        });

        DB::table('mfg_request_type')->insert([
            [
                'id' => 1,
                'type' => 'card_number_request',
                'created_at'=> DB::raw('now()'),
                'updated_at'=> DB::raw('now()')
            ],
            [
                'id' => 2,
                'type' => 'financial_request',
                'created_at'=> DB::raw('now()'),
                'updated_at'=> DB::raw('now()')
            ],
            [
                'id' => 3,
                'type' => 'confirmation_request',
                'created_at'=> DB::raw('now()'),
                'updated_at'=> DB::raw('now()')
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mfg_request_type');
    }
}
