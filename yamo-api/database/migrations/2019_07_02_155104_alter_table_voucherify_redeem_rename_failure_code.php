<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableVoucherifyRedeemRenameFailureCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('voucherify_redeemed', function(Blueprint $table) {
            $table->renameColumn('faliure_code', 'failure_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('voucherify_redeemed', function(Blueprint $table) {
            $table->renameColumn('failure_code', 'faliure_code');
        });
    }
}
