<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTestimonialsTableIsabell extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('testimonials', function (Blueprint $table) {
            DB::statement("update testimonials set description = 'Yamo kann ich mit gutem Gewissen weiterempfehlen. Vor allem zur Entlastung in hektischen Phasen, in denen Zeit rar ist, ist er eine super Alternative.', updated_at = now() where id = 1;");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('testimonials', function (Blueprint $table) {
            DB::statement("update testimonials set description = 'Yamo kann ich mit gutem Gewissen weiterempfehlen. Vor allem zur Entlastung in hektischen Phasen, in denen Zeit rar ist, ist er die beste Alternative.', updated_at = now() where id = 1;");
        });
    }
}
