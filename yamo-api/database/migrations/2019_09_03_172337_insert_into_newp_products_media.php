<?php

use App\Models\Product;
use App\Models\Media;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoNewpProductsMedia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $pirates = Product::withoutGlobalScope('active')->where('name_key',strtoupper('pirates-of-the-carrotean'))->first();
        $freshPrince = Product::withoutGlobalScope('active')->where('name_key',strtoupper('fresh-prince-of-bel-pear'))->first();
        $applecalypse = Product::withoutGlobalScope('active')->where('name_key',strtoupper('applecalypse-now'))->first();
        $david = Product::withoutGlobalScope('active')->where('name_key',strtoupper('david-zucchetta'))->first();
        $mango = Product::withoutGlobalScope('active')->where('name_key',strtoupper('mango-no-5'))->first();
        $beetney = Product::withoutGlobalScope('active')->where('name_key',strtoupper('beetney-spears'))->first();
        $broccoly = Product::withoutGlobalScope('active')->where('name_key',strtoupper('broccoly-balboa'))->first();
        $anthony = Product::withoutGlobalScope('active')->where('name_key',strtoupper('anthony-pumpkins'))->first();
        $sweetHome = Product::withoutGlobalScope('active')->where('name_key',strtoupper('sweet-home-albanana'))->first();
        $cocohontas = Product::withoutGlobalScope('active')->where('name_key',strtoupper('cocohontas'))->first();
        $inbanana = Product::withoutGlobalScope('active')->where('name_key',strtoupper('inbanana-jones'))->first();
        $avocado = Product::withoutGlobalScope('active')->where('name_key',strtoupper('avocado-di-caprio'))->first();
        $katy = Product::withoutGlobalScope('active')->where('name_key',strtoupper('katy-berry'))->first();
        $peach = Product::withoutGlobalScope('active')->where('name_key',strtoupper('peach-boys'))->first();
        $prince = Product::withoutGlobalScope('active')->where('name_key',strtoupper('prince-vanilliam'))->first();
        $nicki = Product::withoutGlobalScope('active')->where('name_key',strtoupper('nicki-spinaj'))->first();
        $quentin = Product::withoutGlobalScope('active')->where('name_key',strtoupper('quentin-carrotino'))->first();
        $cocofield = Product::withoutGlobalScope('active')->where('name_key',strtoupper('david-cocofield'))->first();
        $berry = Product::withoutGlobalScope('active')->where('name_key',strtoupper('berry-potter'))->first();
        $bib = Product::withoutGlobalScope('active')->where('name_key',strtoupper('bib'))->first();

        $piratesMedia = Media::where('name_key','IMG-'.strtoupper('pirates-of-the-carrotean'))->first();
        $freshPrinceMedia = Media::where('name_key','IMG-'.strtoupper('fresh-prince-of-bel-pear'))->first();
        $applecalypseMedia = Media::where('name_key','IMG-'.strtoupper('applecalypse-now'))->first();
        $davidMedia = Media::where('name_key','IMG-'.strtoupper('david-zucchetta'))->first();
        $mangoMedia = Media::where('name_key','IMG-'.strtoupper('mango-no-5'))->first();
        $beetneyMedia = Media::where('name_key','IMG-'.strtoupper('beetney-spears'))->first();
        $broccolyMedia = Media::where('name_key','IMG-'.strtoupper('broccoly-balboa'))->first();
        $anthonyMedia = Media::where('name_key','IMG-'.strtoupper('anthony-pumpkins'))->first();
        $sweetHomeMedia = Media::where('name_key','IMG-'.strtoupper('sweet-home-albanana'))->first();
        $cocohontasMedia = Media::where('name_key','IMG-'.strtoupper('cocohontas'))->first();
        $inbananaMedia = Media::where('name_key','IMG-'.strtoupper('inbanana-jones'))->first();
        $avocadoMedia = Media::where('name_key','IMG-'.strtoupper('avocado-di-caprio'))->first();
        $katyMedia = Media::where('name_key','IMG-'.strtoupper('katy-berry'))->first();
        $peachMedia = Media::where('name_key','IMG-'.strtoupper('peach-boys'))->first();
        $princeMedia = Media::where('name_key','IMG-'.strtoupper('prince-vanilliam'))->first();
        $nickiMedia = Media::where('name_key','IMG-'.strtoupper('nicki-spinaj'))->first();
        $quentinMedia = Media::where('name_key','IMG-'.strtoupper('quentin-carrotino'))->first();
        $cocofieldMedia = Media::where('name_key','IMG-'.strtoupper('david-cocofield'))->first();
        $berryMedia = Media::where('name_key','IMG-'.strtoupper('berry-potter'))->first();
        $bibMedia = Media::where('name_key','IMG-'.strtoupper('bib'))->first();

        DB::table('products_media')->insert([
            ['media_id'=>$piratesMedia->id, 'product_id'=>$pirates->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$freshPrinceMedia->id, 'product_id'=>$freshPrince->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$applecalypseMedia->id, 'product_id'=>$applecalypse->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$davidMedia->id, 'product_id'=>$david->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$mangoMedia->id, 'product_id'=>$mango->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$beetneyMedia->id, 'product_id'=>$beetney->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$broccolyMedia->id, 'product_id'=>$broccoly->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$anthonyMedia->id, 'product_id'=>$anthony->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$sweetHomeMedia->id, 'product_id'=>$sweetHome->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$cocohontasMedia->id, 'product_id'=>$cocohontas->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$inbananaMedia->id, 'product_id'=>$inbanana->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$avocadoMedia->id, 'product_id'=>$avocado->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$katyMedia->id, 'product_id'=>$katy->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$peachMedia->id, 'product_id'=>$peach->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$princeMedia->id, 'product_id'=>$prince->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$nickiMedia->id, 'product_id'=>$nicki->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$quentinMedia->id, 'product_id'=>$quentin->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$cocofieldMedia->id, 'product_id'=>$cocofield->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$berryMedia->id, 'product_id'=>$berry->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['media_id'=>$bibMedia->id, 'product_id'=>$bib->id, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('products_media')->truncate();
        Schema::enableForeignKeyConstraints();

    }
}
