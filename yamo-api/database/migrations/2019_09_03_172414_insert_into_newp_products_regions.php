<?php

use App\Models\Country;
use App\Models\Product;
use App\Models\VatGroup;
use App\Models\Region;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoNewpProductsRegions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $pirates = Product::withoutGlobalScope('active')->where('name_key',strtoupper('pirates-of-the-carrotean'))->first();
        $freshPrince = Product::withoutGlobalScope('active')->where('name_key',strtoupper('fresh-prince-of-bel-pear'))->first();
        $applecalypse = Product::withoutGlobalScope('active')->where('name_key',strtoupper('applecalypse-now'))->first();
        $david = Product::withoutGlobalScope('active')->where('name_key',strtoupper('david-zucchetta'))->first();
        $mangoNo5 = Product::withoutGlobalScope('active')->where('name_key',strtoupper('mango-no-5'))->first();
        $beetney = Product::withoutGlobalScope('active')->where('name_key',strtoupper('beetney-spears'))->first();
        $broccoly = Product::withoutGlobalScope('active')->where('name_key',strtoupper('broccoly-balboa'))->first();
        $anthony = Product::withoutGlobalScope('active')->where('name_key',strtoupper('anthony-pumpkins'))->first();
        $sweetHome = Product::withoutGlobalScope('active')->where('name_key',strtoupper('sweet-home-albanana'))->first();
        $cocohontas = Product::withoutGlobalScope('active')->where('name_key',strtoupper('cocohontas'))->first();
        $inbanana = Product::withoutGlobalScope('active')->where('name_key',strtoupper('inbanana-jones'))->first();
        $avocadoDiCaprio = Product::withoutGlobalScope('active')->where('name_key',strtoupper('avocado-di-caprio'))->first();
        $katy = Product::withoutGlobalScope('active')->where('name_key',strtoupper('katy-berry'))->first();
        $peach = Product::withoutGlobalScope('active')->where('name_key',strtoupper('peach-boys'))->first();
        $prince = Product::withoutGlobalScope('active')->where('name_key',strtoupper('prince-vanilliam'))->first();
        $nicki = Product::withoutGlobalScope('active')->where('name_key',strtoupper('nicki-spinaj'))->first();
        $quentin = Product::withoutGlobalScope('active')->where('name_key',strtoupper('quentin-carrotino'))->first();
        $cocofield = Product::withoutGlobalScope('active')->where('name_key',strtoupper('david-cocofield'))->first();
        $berry = Product::withoutGlobalScope('active')->where('name_key',strtoupper('berry-potter'))->first();

        $countryDe = Country::where('content_code',Helpers::LOCALE_DE)->first();
        $countryCh = Country::where('content_code',Helpers::LOCALE_CH)->first();
        $countryAt = Country::where('content_code',Helpers::LOCALE_AT)->first();

        $regionDe = Region::where('country_id',$countryDe->id)->first();
        $regionCh = Region::where('country_id',$countryCh->id)->first();
        $regionAt = Region::where('country_id',$countryAt->id)->first();

        $vatGroupDe = VatGroup::where('country_id',$countryDe->id)->where('value','7.00')->first();
        $vatGroupCh = VatGroup::where('country_id',$countryCh->id)->where('value','2.50')->first();
        $vatGroupAt = VatGroup::where('country_id',$countryAt->id)->where('value','10.00')->first();

        DB::table('products_regions')->insert([
            ['product_id'=>$pirates->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$pirates->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$pirates->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$freshPrince->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$freshPrince->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$freshPrince->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$applecalypse->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$applecalypse->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$applecalypse->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$david->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$david->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$david->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$mangoNo5->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$mangoNo5->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$mangoNo5->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$beetney->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$beetney->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$beetney->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$broccoly->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$broccoly->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$broccoly->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$anthony->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$anthony->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$anthony->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$sweetHome->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$sweetHome->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$sweetHome->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$cocohontas->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$cocohontas->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$cocohontas->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$inbanana->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$inbanana->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$inbanana->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$avocadoDiCaprio->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$avocadoDiCaprio->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$avocadoDiCaprio->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$katy->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$katy->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$katy->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$peach->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$peach->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$peach->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$prince->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$prince->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$prince->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$nicki->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$nicki->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$nicki->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$quentin->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$quentin->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$quentin->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$cocofield->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$cocofield->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$cocofield->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$berry->id,'region_id'=>$regionDe->id,'vat_group_id'=>$vatGroupDe->id,'price'=>2.50,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$berry->id,'region_id'=>$regionCh->id,'vat_group_id'=>$vatGroupCh->id,'price'=>4.25,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
            ['product_id'=>$berry->id,'region_id'=>$regionAt->id,'vat_group_id'=>$vatGroupAt->id,'price'=>2.65,'active_start_date'=>'2019-08-01 12:12:12', 'active_end_date'=>'2020-08-01 12:12:12','active'=>1, 'created_at'=>DB::raw('NOW()'), 'updated_at'=>DB::raw('NOW()')],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('products_regions')->truncate();
        Schema::enableForeignKeyConstraints();
    }
}
