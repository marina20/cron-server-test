<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdjustedColumnsToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function($table) {
            $table->string('pending_state')->after('delivery_date');
            $table->string('child_birthday')->after('pending_state');
            $table->string('billing_delivery_date')->after('child_birthday');
            $table->string('billing_gender')->after('billing_delivery_date');
            $table->string('cancelled_email_sent')->after('billing_gender');
            $table->string('order_discount')->after('cancelled_email_sent');
            $table->string('billing_child_birthday')->after('order_discount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function($table) {
            $table->dropColumn('pending_state');
            $table->dropColumn('child_birthday');
            $table->dropColumn('billing_delivery_date');
            $table->dropColumn('billing_gender');
            $table->dropColumn('cancelled_email_sent');
            $table->dropColumn('order_discount');
            $table->dropColumn('billing_child_birthday');
        });
    }
}
