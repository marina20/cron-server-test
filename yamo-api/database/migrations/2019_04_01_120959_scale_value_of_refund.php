<?php

use App\Models\OrderRefund;
use Illuminate\Database\Migrations\Migration;

class ScaleValueOfRefund extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        OrderRefund::chunk(100, function($refunds){
            foreach ($refunds as $refund) {
                $refund->value/=100;
                $refund->save();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        OrderRefund::chunk(100, function($refunds){
            foreach ($refunds as $refund) {
                $refund->value*=100;
                $refund->save();
            }
        });
    }
}
