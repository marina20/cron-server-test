<?php

use App\Models\Order;
use App\Models\Subscription;
use App\Models\OrderSubscription;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrderTableWithSubscriptionId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedInteger('subscription_id')->nullable()->after('user_id');
            $table->foreign('subscription_id')->references('id')->on('subscriptions');
        });

        Schema::create('order_subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('order_id')->nullable();
            $table->unsignedInteger('subscription_id')->nullable();
            $table->timestamps();
            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('subscription_id')->references('id')->on('subscriptions');
        });

        set_time_limit(0);
        $subscriptions = Subscription::get();

        Schema::disableForeignKeyConstraints();
        foreach($subscriptions as $s) {
            $o = $s->order_id;

            while($o !== null) {
                $os = new OrderSubscription();
                $os->order_id = $o;
                $os->subscription_id = $s->id;
                $os->save();
                $order = Order::find($o);
                if ($order === null) break;
                $order->subscription_id = $s->id;
                $order->save();
                $o = $order->parent_id;
            }
        }
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::table('orders', function ($table){
            $table->dropForeign(['subscription_id']);
            $table->dropColumn('subscription_id');
        });

        Schema::enableForeignKeyConstraints();

        Schema::dropIfExists('order_subscriptions');
    }
}
