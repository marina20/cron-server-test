<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrdersTablePaymentResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function($table) {
            $table->string('adyen_auth_result')->nullable()->after('coupon_id');
            $table->string('adyen_payment_method')->nullable()->after('adyen_auth_result');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function($table) {
            $table->dropColumn('adyen_auth_result');
            $table->dropColumn('adyen_payment_method');
        });
    }
}
