<?php

use App\Models\DeliveryFrequency as DFModel;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeliveryFrequency extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_frequencies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('interval');
            $table->string('period');
            $table->string('icon');
            $table->timestamps();
        });

        // Seed it
        DFModel::create(['id'=>1,'interval'=>1,'period'=>'week','icon'=>'clock-twelve']);
        DFModel::create(['id'=>2,'interval'=>2,'period'=>'week','icon'=>'clock-six']);
        DFModel::create(['id'=>4,'interval'=>4,'period'=>'week','icon'=>'clock']);

        Schema::table('subscriptions', function($table) {
            $table->unsignedInteger('delivery_frequency_id')->after('status')->default(1);
            $table->foreign('delivery_frequency_id')->references('id')->on('delivery_frequencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_frequencies');
    }
}
