<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsAltTagsToTableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('image_alt_tag_key')->nullable()->after('image_thumbnail');
            $table->string('image_alt_tag_text')->nullable()->after('image_alt_tag_key');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('image_alt_tag_text');
            $table->dropColumn('image_alt_tag_key');
        });
    }
}
