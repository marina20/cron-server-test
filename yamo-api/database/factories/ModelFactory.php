<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->email,
    ];
});

$factory->define(App\Models\Translation::class, function (Faker\Generator $faker) {
    return [
        'key-key' => $faker->unique()->key-key,
    ];
});

$factory->define(App\Models\Profile::class, function (Faker\Generator $faker) {
    return [
        'first_name'=>$faker->firstName,
        'last_name'=>$faker->lastName,
        'child_birthday'=>$faker->dateTimeBetween('-1 years')
    ];
});

$factory->define(App\Models\Order::class, function (Faker\Generator $faker) {
   return [
       'billing_first_name' => $faker->firstName,
       'billing_last_name' => $faker->lastName,
       'billing_company' => $faker->company,
       //'billing_address_1' => $faker->address,
       'billing_street_name' => 'Teststreet',
       'billing_street_nr' => '99',
       'billing_city' => $faker->city,
       'billing_postcode' => $faker->postcode,
       'billing_email' => $faker->email,
       'billing_phone' => $faker->phoneNumber,
       'billing_title' => $faker->title(),
       'shipping_first_name' => $faker->firstName,
       'shipping_last_name' => $faker->lastName,
       'shipping_company' => $faker->company,
       //'shipping_address_1' => $faker->address,
       'shipping_street_name' => 'Teststreet',
       'shipping_street_nr' => '99',
       'shipping_city' => $faker->city,
       'shipping_postcode' => $faker->postcode,
       'shipping_email' => $faker->email,
       'shipping_phone' => $faker->phoneNumber,
       'shipping_title' => $faker->title(),
       'country_id'=> '1'
   ];
});

$factory->define(App\Models\Subscription::class, function (Faker\Generator $faker) {
    return [
        'billing_period' => 'week',
        'billing_interval' => rand(1,3),
        'created_at' => $faker->dateTime('now'),
        'updated_at' => $faker->dateTime('now')
    ];
});

$factory->define(App\Models\OrderItem::class, function (Faker\Generator $faker) {
    return [];
});

$factory->define(App\Models\Product::class, function (Faker\Generator $faker) {
    return [];
});

$factory->define(App\Models\Box::class, function (Faker\Generator $faker) {
    return [];
});

$factory->define(App\Models\Country::class, function (Faker\Generator $faker) {
    return [];
});

$factory->define(App\Models\Price::class, function (Faker\Generator $faker) {
    return [];
});

$factory->define(App\Models\BoxContent::class, function (Faker\Generator $faker) {
    return [];
});

$factory->define(App\Models\Postcode::class, function (Faker\Generator $faker) {
    return [];
});

$factory->define(App\Models\ExtraProduct::class, function (Faker\Generator $faker) {
    return [
        'count' => 0,
        'used' => 0,
        'created_at' => $faker->dateTime('now'),
        'updated_at' => $faker->dateTime('now')
    ];
});

$factory->define(App\Models\Category::class, function (Faker\Generator $faker) {
    return [];
});

$factory->define(App\Models\DeliveryDate::class, function (Faker\Generator $faker) {
    return [];
});
$factory->define(App\Models\LoyaltyRule::class, function (Faker\Generator $faker) {
    return [];
});

$factory->define(App\Models\Voucher::class, function (Faker\Generator $faker) {
    return [];
});