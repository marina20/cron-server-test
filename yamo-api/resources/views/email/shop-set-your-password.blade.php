<?php use \App\Models\Translation; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" /><!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" /><!--<![endif]-->
    <!--[if (gte mso 9)|(IE)]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        body {width: 600px;margin: 0 auto;}
        table {border-collapse: collapse;}
        table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
        img {-ms-interpolation-mode: bicubic;}
    </style>
    <![endif]-->

    <style type="text/css">

      body, p, div {
        font-family: arial;
        font-size: 16px;
      }
      body {
        color: #000000;
      }
      body a {
        color: #1188E6;
        text-decoration: none;
      }
      p { margin: 0; padding: 0; }
      table.wrapper {
        width:100% !important;
        table-layout: fixed;
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: 100%;
        -moz-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
      }
      img.max-width {
        max-width: 100% !important;
      }
      .column.of-2 {
        width: 50%;
      }
      .column.of-3 {
        width: 33.333%;
      }
      .column.of-4 {
        width: 25%;
      }
      @media screen and (max-width:480px) {
        .preheader .rightColumnContent,
        .footer .rightColumnContent {
            text-align: left !important;
        }
        .preheader .rightColumnContent div,
        .preheader .rightColumnContent span,
        .footer .rightColumnContent div,
        .footer .rightColumnContent span {
          text-align: left !important;
        }
        .preheader .rightColumnContent,
        .preheader .leftColumnContent {
          font-size: 80% !important;
          padding: 5px 0;
        }
        table.wrapper-mobile {
          width: 100% !important;
          table-layout: fixed;
        }
        img.max-width {
          height: auto !important;
          max-width: 480px !important;
        }
        a.bulletproof-button {
          display: block !important;
          width: auto !important;
          font-size: 80%;
          padding-left: 0 !important;
          padding-right: 0 !important;
        }
        .columns {
          width: 100% !important;
        }
        .column {
          display: block !important;
          width: 100% !important;
          padding-left: 0 !important;
          padding-right: 0 !important;
          margin-left: 0 !important;
          margin-right: 0 !important;
        }
      }
    </style>
    <!--user entered Head Start-->
    
     <!--End Head user entered-->
  </head>
  <body>
    <center class="wrapper" data-link-color="#1188E6" data-body-style="font-size: 16px; font-family: arial; color: #000000; background-color: #ffffff;">
      <div class="webkit">
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#ffffff">
          <tr>
            <td valign="top" bgcolor="#ffffff" width="100%">
              <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
                <tr>
                  <td width="100%">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0">
                      <tr>
                        <td>
                          <!--[if mso]>
                          <center>
                          <table><tr><td width="600">
                          <![endif]-->
                          <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width:600px;" align="center">
                            <tr>
                              <td role="modules-container" style="padding: 0px 0px 0px 0px; color: #000000; text-align: left;" bgcolor="#ffffff" width="100%" align="left">
                                
    <table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%"
           style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
      <tr>
        <td role="module-content">
          <p>{{ ucfirst($name) }}, {{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.WEAWAITYOU') }}</p>
        </td>
      </tr>
    </table>
  
    <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tr>
        <td style="font-size:6px;line-height:10px;padding:0px 0px 0px 0px;" valign="top" align="center">
          <a href="https://www.yamo.bio/?utm_source=sendgrid&amp;utm_campaign=setyourpassword&amp;utm_medium=transactional&amp;utm_content=512&amp;utm_term=logo"><img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:20% !important;width:20%;height:auto !important;" src="https://marketing-image-production.s3.amazonaws.com/uploads/6212c4140074c6a8953f5ef8d8ae34052eba6b87f406d29f74d291f7ba604ba0161d4b04d88bca08143c69acd991ee5eb99156450cea007e5b82e83f28a18229.jpg" alt="" width="120"></a>
        </td>
      </tr>
    </table>
  
    <table class="module"
           role="module"
           data-type="spacer"
           border="0"
           cellpadding="0"
           cellspacing="0"
           width="100%"
           style="table-layout: fixed;">
      <tr>
        <td style="padding:0px 0px 30px 0px;"
            role="module-content"
            bgcolor="">
        </td>
      </tr>
    </table>
  
    <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tr>
        <td style="font-size:6px;line-height:10px;padding:0px 0px 0px 0px;" valign="top" align="center">
          <a href="https://www.yamo.bio/?utm_source=sendgrid&amp;utm_campaign=setyourpassword&amp;utm_medium=transactional&amp;utm_content=1024&amp;utm_term=yvetteusp"><img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:100% !important;width:100%;height:auto !important;" src="https://marketing-image-production.s3.amazonaws.com/uploads/acd8c3a83ab176d70f3436e89ecb88673459240bf82e43e561299495cf2b0c0f1685fb4ea6daa3cfb7bdd4443bb6b05e923b6905acf4f2dd22cc48189d985494.jpg" alt="" width="600"></a>
        </td>
      </tr>
    </table>
  
    <table class="module"
           role="module"
           data-type="spacer"
           border="0"
           cellpadding="0"
           cellspacing="0"
           width="100%"
           style="table-layout: fixed;">
      <tr>
        <td style="padding:0px 0px 5px 0px;"
            role="module-content"
            bgcolor="">
        </td>
      </tr>
    </table>
  
    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tr>
        <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:justify;"
            height="100%"
            valign="top"
            bgcolor="">
            <div style="text-align: center;"><span style="color: rgb(155, 188, 160); font-family: arial, helvetica, sans-serif; font-size: 22px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 700; text-align: center; white-space: pre-wrap;">{{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.HI') }}, {{ ucfirst($name) }}, {{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.CONFIRMYOURACCOUNT') }}</span></div>

<div style="text-align: center;">&nbsp;</div>

<div style="text-align: left;"><span style="font-size:16px;"><span style="color: rgb(0, 0, 0); font-family: Arial; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; text-align: left; white-space: pre-wrap;">{{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.WITHYOURACCOUNTYOUCAN') }}:</span></span></div>

<div style="text-align: left;"><br>
<span style="font-size:16px;"><span style="color: rgb(0, 0, 0); font-family: Arial; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; text-align: left; white-space: pre-wrap;">✔️ ️{{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.INVITEFRIENDS') }}</span></span></div>

<div style="text-align: left;"><br>
<span style="font-size:16px;"><span style="color: rgb(0, 0, 0); font-family: Arial; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; text-align: left; white-space: pre-wrap;">✔️ {{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.CUSTOMIZEYOURBOX') }}️</span></span></div>

<div style="text-align: left;"><br>
<span style="font-size:16px;"><span style="color: rgb(0, 0, 0); font-family: Arial; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; text-align: left; white-space: pre-wrap;">✔️ {{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.CUSTOMIZEYOURDELIVERYDATE') }}</span></span></div>
        </td>
      </tr>
    </table>
  <table border="0" cellPadding="0" cellSpacing="0" class="module" data-role="module-button" data-type="button" role="module" style="table-layout:fixed" width="100%"><tbody><tr><td align="center" class="outer-td" style="padding:0px 0px 0px 0px"><table border="0" cellPadding="0" cellSpacing="0" class="button-css__deep-table___2OZyb wrapper-mobile" style="text-align:center"><tbody><tr><td align="center" bgcolor="#F7A03C" class="inner-td" style="border-radius:6px;font-size:16px;text-align:center;background-color:inherit"><a style="background-color:#F7A03C;border:1px solid #333333;border-color:#F7A03C;border-radius:6px;border-width:1px;color:#ffffff;display:inline-block;font-family:arial,helvetica,sans-serif;font-size:16px;font-weight:normal;letter-spacing:0px;line-height:16px;padding:12px 18px 12px 18px;text-align:center;text-decoration:none" href="{{ $reset_password_uri . $token }}" target="_blank">Passwort setzen</a></td></tr></tbody></table></td></tr></tbody></table>
    <table class="module"
           role="module"
           data-type="spacer"
           border="0"
           cellpadding="0"
           cellspacing="0"
           width="100%"
           style="table-layout: fixed;">
      <tr>
        <td style="padding:0px 0px 30px 0px;"
            role="module-content"
            bgcolor="">
        </td>
      </tr>
    </table>
  
    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tr>
        <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:justify;"
            height="100%"
            valign="top"
            bgcolor="">
            <div>
<div data-end-index="13016" data-start-index="12918" style="font-family: arial; font-size: 14px; color: rgb(0, 0, 0); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; text-align: left;"><span data-end-index="13046" data-start-index="13016" style="font-size: 16px;"><span data-end-index="13100" data-start-index="13046"><span data-end-index="13302" data-start-index="13100" style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: Arial; white-space: pre-wrap;">{{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.MOREQUESTIONS') }} <a data-end-index="13578" data-start-index="13362" href="yamobaby.zendesk.com/hc/?utm_source=sendgrid&amp;utm_campaign=setyourpassword&amp;utm_medium=transactional&amp;utm_content=&amp;utm_term=" style="font-size: 14px;">{{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.FAQS') }}</a> {{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.MOREQUESTIONS2') }} </span></span></span></div>

<div data-end-index="13780" data-start-index="13682" style="font-family: arial; font-size: 14px; color: rgb(0, 0, 0); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; text-align: left;">&nbsp;</div>

<div data-end-index="13895" data-start-index="13797" style="font-family: arial; font-size: 14px; color: rgb(0, 0, 0); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; text-align: left;"><span data-end-index="13925" data-start-index="13895" style="font-size: 16px;"><span data-end-index="13979" data-start-index="13925"><span data-end-index="14181" data-start-index="13979" style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: Arial; white-space: pre-wrap;">{{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.BESTREGARDS') }}, </span></span></span></div>

<div data-end-index="14325" data-start-index="14227" style="font-family: arial; font-size: 14px; color: rgb(0, 0, 0); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; text-align: left;"><span data-end-index="14355" data-start-index="14325" style="font-size: 16px;"><span data-end-index="14409" data-start-index="14355"><span data-end-index="14611" data-start-index="14409" style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: Arial; white-space: pre-wrap;">{{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.YOURTEAM') }}</span></span></span></div>
</div>
        </td>
      </tr>
    </table>
  
    <table class="module"
           role="module"
           data-type="divider"
           border="0"
           cellpadding="0"
           cellspacing="0"
           width="100%"
           style="table-layout: fixed;">
      <tr>
        <td style="padding:0px 0px 0px 0px;"
            role="module-content"
            height="100%"
            valign="top"
            bgcolor="">
          <table border="0"
                 cellpadding="0"
                 cellspacing="0"
                 align="center"
                 width="100%"
                 height="2px"
                 style="line-height:2px; font-size:2px;">
            <tr>
              <td
                style="padding: 0px 0px 2px 0px;"
                bgcolor="#000000"></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  
    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
      <tr>
        <td style="padding:0px 0px 0px 0px;line-height:22px;text-align:justify;"
            height="100%"
            valign="top"
            bgcolor="">
            <table border="0" cellpadding="0" cellspacing="0" class="module" data-end-index="19043" data-start-index="18905" data-type="text" role="module" style="color: rgb(0, 0, 0); font-family: arial; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; table-layout: fixed;" width="100%">
	<tbody>
		<tr data-end-index="19054" data-start-index="19050">
			<td bgcolor="" data-end-index="19212" data-start-index="19063" height="100%" style="padding-top: 18px; padding-bottom: 18px; line-height: 22px; text-align: inherit;" valign="top">
			<div data-end-index="19258" data-start-index="19225" style="font-family: arial; font-size: 14px; text-align: center;">
			<div data-end-index="19447" data-start-index="19259" style="font-family: arial; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400;">
			<table border="0" cellpadding="0" cellspacing="0" class="module" data-end-index="15711" data-start-index="15573" data-type="text" role="module" style="color: rgb(0, 0, 0); font-family: arial; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; text-align: left; table-layout: fixed;" width="100%">
				<tbody>
					<tr data-end-index="15722" data-start-index="15718">
						<td bgcolor="" data-end-index="15881" data-start-index="15731" height="100%" style="padding-top: 20px; padding-bottom: 18px; line-height: 22px; text-align: inherit;" valign="top">
						<div data-end-index="15927" data-start-index="15894" style="font-family: arial; font-size: 14px; text-align: center;"><span data-end-index="16148" data-start-index="15927" style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; font-family: arial; font-size: inherit; text-decoration: inherit;">Falls dich interessiert, was wir auf Social Media Lustiges treiben, folge uns auf:</span><span data-end-index="16426" data-start-index="16237" style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: Arial; font-size: 16px;">&nbsp;</span></div>
						</td>
					</tr>
				</tbody>
			</table>

			<table align="center" border="0" cellpadding="0" cellspacing="0" class="module" data-end-index="16647" data-start-index="16492" data-type="social" role="module" style="color: rgb(0, 0, 0); font-family: arial; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; text-align: left; table-layout: fixed;" width="100%">
				<tbody data-end-index="16661" data-start-index="16654">
					<tr data-end-index="16674" data-start-index="16670">
						<td data-end-index="16766" data-start-index="16685" style="font-size: 6px; line-height: 10px;" valign="top">
						<div style="text-align: center;">&nbsp;</div>

						<table align="center" data-end-index="16801" data-start-index="16779">
							<tbody data-end-index="16823" data-start-index="16816">
								<tr data-end-index="16844" data-start-index="16840">
									<td data-end-index="16893" data-start-index="16863" style="padding-right: 5px; padding-left: 5px;"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2230%22%2C%22height%22%3A%2230%22%2C%22alt_text%22%3A%22Facebook%22%2C%22alignment%22%3A%22center%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/social/white/facebook.png%22%2C%22link%22%3A%22https%3A//www.facebook.com/yamobaby%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D" style="float: none; display: block; text-align: center;"><a alt="Facebook" data-end-index="17184" data-nolink="false" data-start-index="16902" href="https://www.facebook.com/yamobaby" role="social-icon-link" style="border-radius: 2px; display: inline-block; background-color: rgb(59, 87, 157);" target="_blank" title="Facebook "><img alt="Facebook" data-end-index="17443" data-start-index="17195" height="30" role="social-icon" src="https://marketing-image-production.s3.amazonaws.com/social/white/facebook.png" title="Facebook " width="30" /></a></span></td>
									<td data-end-index="17517" data-start-index="17487" style="padding-right: 5px; padding-left: 5px;"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2230%22%2C%22height%22%3A%2230%22%2C%22alt_text%22%3A%22Twitter%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/social/white/twitter.png%22%2C%22link%22%3A%22https%3A//www.twitter.com/yamobaby%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a alt="Twitter" data-end-index="17805" data-nolink="false" data-start-index="17526" href="https://www.twitter.com/yamobaby" role="social-icon-link" style="border-radius: 2px; display: inline-block; background-color: rgb(122, 196, 247);" target="_blank" title="Twitter "><img alt="Twitter" data-end-index="18061" data-start-index="17816" height="30" role="social-icon" src="https://marketing-image-production.s3.amazonaws.com/social/white/twitter.png" title="Twitter " width="30" /></a></span></td>
									<td data-end-index="18135" data-start-index="18105" style="padding-right: 5px; padding-left: 5px;"><span class="sg-image" data-imagelibrary="%7B%22width%22%3A%2230%22%2C%22height%22%3A%2230%22%2C%22alt_text%22%3A%22Instagram%22%2C%22alignment%22%3A%22%22%2C%22border%22%3A0%2C%22src%22%3A%22https%3A//marketing-image-production.s3.amazonaws.com/social/white/instagram.png%22%2C%22link%22%3A%22https%3A//www.instagram.com/yamobaby%22%2C%22classes%22%3A%7B%22sg-image%22%3A1%7D%7D"><a alt="Instagram" data-end-index="18429" data-nolink="false" data-start-index="18144" href="https://www.instagram.com/yamobaby" role="social-icon-link" style="border-radius: 2px; display: inline-block; background-color: rgb(127, 75, 48);" target="_blank" title="Instagram "><img alt="Instagram" data-end-index="18691" data-start-index="18440" height="30" role="social-icon" src="https://marketing-image-production.s3.amazonaws.com/social/white/instagram.png" title="Instagram " width="30" /></a></span></td>
								</tr>
							</tbody>
						</table>

						<div style="text-align: center;">&nbsp;</div>
						</td>
					</tr>
				</tbody>
			</table>
			</div>

			<div data-end-index="19447" data-start-index="19259" style="font-family: arial; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400;">&nbsp;</div>

			<div data-end-index="19447" data-start-index="19259" style="font-family: arial; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400;">
			<div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: arial; font-size: 14px; color: rgb(0, 0, 0); text-align: center;"><span class="ac-designer-copy" style="box-sizing: content-box; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: arial; font-size: 11px; border-style: none; color: rgb(0, 0, 0); text-align: center; outline: none; text-decoration-line: inherit; word-spacing: 0.015625px; overflow-wrap: break-word !important;">Copyright &copy; 2019&nbsp;| yamo food GmbH. All rights reserved.</span>

			<div data-end-index="17239" data-start-index="17044" style="box-sizing: content-box; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: arial; font-size: 14px; color: rgb(0, 0, 0); text-align: center;"><span class="ac-designer-copy" data-end-index="17590" data-start-index="17239" style="box-sizing: content-box; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: arial; font-size: 11px; border-style: none; outline: none; text-decoration-line: inherit; word-spacing: 0.015625px; overflow-wrap: break-word !important;"><a class="ac-designer-copy" data-ac-default-color="1" data-end-index="17886" data-start-index="17590" href="https://www.yamo.bio/impressum/" style="box-sizing: content-box; font-size: 11px; border-style: none; color: rgb(22, 100, 35); text-decoration-line: underline; overflow-wrap: break-word !important;"><span class="ac-designer-copy" data-end-index="18105" data-start-index="17886" style="box-sizing: content-box; font-size: 11px; border-style: none; outline: none; color: inherit; text-decoration: inherit; overflow-wrap: break-word !important;">{{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.IMPRESSUM') }}</span></a><br class="ac-designer-copy" data-end-index="18232" data-start-index="18125" style="box-sizing: content-box; line-height: inherit;" />
			<span class="ac-designer-copy" data-end-index="18497" data-start-index="18233" style="box-sizing: content-box; font-style: normal; font-weight: normal; font-family: arial; font-size: 11px; border-style: none; outline: none; text-decoration: inherit; overflow-wrap: break-word !important;">{{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.COMPANYNAME') }}</span></span><br class="ac-designer-copy" data-end-index="18642" data-start-index="18518" style="box-sizing: content-box; font-size: 11px; line-height: inherit;" />
			<span class="ac-designer-copy" data-end-index="19021" data-start-index="18643" style="box-sizing: content-box; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; font-family: arial; font-size: 11px; border-style: none; outline: none; color: rgb(93, 93, 93); text-decoration-line: inherit; word-spacing: 0.015625px; overflow-wrap: break-word !important;"><span class="ac-designer-copy" data-end-index="19244" data-start-index="19021" style="box-sizing: content-box; font-family: arial; font-size: 11px; border-style: none; outline: none; text-decoration: inherit; overflow-wrap: break-word !important;"><span class="ac-designer-copy" data-end-index="19509" data-start-index="19244" style="box-sizing: content-box; font-weight: normal; font-family: arial; font-size: 11px; border-style: none; outline: none; color: rgb(0, 0, 0); text-decoration: inherit; overflow-wrap: break-word !important;">{{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.COMPANYADDRESS') }}</span><span class="ac-designer-copy" data-end-index="19782" data-start-index="19538" style="box-sizing: content-box; font-weight: normal; font-family: arial; font-size: 11px; border-style: none; outline: none; text-decoration: inherit; overflow-wrap: break-word !important;">&nbsp;</span></span><br class="ac-designer-copy" data-end-index="19909" data-start-index="19802" style="box-sizing: content-box; line-height: inherit;" />
			<span class="ac-designer-copy" data-end-index="20154" data-start-index="19910" style="box-sizing: content-box; font-weight: normal; font-family: arial; font-size: 11px; border-style: none; outline: none; text-decoration: inherit; overflow-wrap: break-word !important;"><a class="ac-designer-copy" data-ac-default-color="1" data-end-index="20464" data-start-index="20154" href="mailto:support@yamo.bio" style="box-sizing: content-box; font-family: arial; font-size: 11px; border-style: none; color: rgb(22, 100, 35); text-decoration-line: underline; overflow-wrap: break-word !important;" target="_blank">support@yamo.bio</a></span></span><br class="ac-designer-copy" data-end-index="20622" data-start-index="20498" style="box-sizing: content-box; font-size: 11px; line-height: inherit;" />
			<a class="ac-designer-copy" data-ac-default-color="1" data-end-index="21091" data-start-index="20623" href="http://www.yamo.bio/" style="box-sizing: content-box; background-color: rgb(255, 255, 255); font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: arial; font-size: 12px; border-style: none; color: rgb(22, 100, 35); text-decoration-line: underline; word-spacing: 0.015625px; overflow-wrap: break-word !important;" target="_blank"><span class="ac-designer-copy" data-end-index="21310" data-start-index="21091" style="box-sizing: content-box; font-size: 12px; border-style: none; outline: none; color: inherit; text-decoration: inherit; overflow-wrap: break-word !important;">www.yamo.bio</span></a></div>

			<div data-end-index="21536" data-start-index="21341" style="box-sizing: content-box; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: arial; font-size: 14px; color: rgb(0, 0, 0); text-align: center;"><span data-end-index="21615" data-start-index="21536" style="box-sizing: content-box; font-size: 11px;"><span class="ac-designer-copy" data-end-index="21834" data-start-index="21615" style="box-sizing: content-box; font-size: 11px; border-style: none; outline: none; color: inherit; text-decoration: inherit; overflow-wrap: break-word !important;">{{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.COMMERCIALREGISTEROFFICE') }}</span></span></div>

			<div data-end-index="22109" data-start-index="21914" style="box-sizing: content-box; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: arial; font-size: 14px; color: rgb(0, 0, 0); text-align: center;"><span data-end-index="22188" data-start-index="22109" style="box-sizing: content-box; font-size: 11px;"><span class="ac-designer-copy" data-end-index="22407" data-start-index="22188" style="box-sizing: content-box; font-size: 11px; border-style: none; outline: none; color: inherit; text-decoration: inherit; overflow-wrap: break-word !important;">{{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.BUISINESSLEADER') }}</span></span></div>
			</div>
			</div>
			</div>
			</td>
		</tr>
	</tbody>
</table>

<div class="module unsubscribe-css__unsubscribe___2CDlR" data-end-index="24665" data-role="module-unsubscribe" data-start-index="24439" data-type="unsubscribe" role="module" style="font-family: arial; font-size: 12px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; color: rgb(68, 68, 68); line-height: 20px; padding: 16px; text-align: center;">
<p data-end-index="24734" data-start-index="24665" style="font-family: arial; font-size: 12px; line-height: 20px;"><a class="Unsubscribe--unsubscribeLink" data-end-index="24815" data-start-index="24734" href="https://%3C%25asm_group_unsubscribe_raw_url%25%3E/" target="_blank">{{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.UNSUBSCRIPE') }}</a>&nbsp;-&nbsp;<a class="Unsubscribe--unsubscribePreferences" data-end-index="24915" data-start-index="24833" href="https://%3C%25asm_preferences_raw_url%25%3E/" target="_blank">{{ Translation::trans('MAIL.SHOP-SET-YOUR-PASSWORD.UNSUBSCRIPEPREFERENCES') }}</a></p>
</div>

        </td>
      </tr>
    </table>
  
                              </td>
                            </tr>
                          </table>
                          <!--[if mso]>
                          </td></tr></table>
                          </center>
                          <![endif]-->
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </div>
    </center>
  </body>
</html>