<?php use \App\Models\Translation; ?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" /><!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" /><!--<![endif]-->
    <!--[if (gte mso 9)|(IE)]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        body {width: 600px;margin: 0 auto;}
        table {border-collapse: collapse;}
        table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
        img {-ms-interpolation-mode: bicubic;}
    </style>
    <![endif]-->

    <style type="text/css">
        body, p, div {
            font-family: verdana,geneva,sans-serif;
            font-size: 14px;
        }
        body {
            color: #747474;
        }
        body a {
            color: #7baa84;
            text-decoration: none;
        }
        p { margin: 0; padding: 0; }
        table.wrapper {
            width:100% !important;
            table-layout: fixed;
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: 100%;
            -moz-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }
        img.max-width {
            max-width: 100% !important;
        }
        .column.of-2 {
            width: 50%;
        }
        .column.of-3 {
            width: 33.333%;
        }
        .column.of-4 {
            width: 25%;
        }
        @media screen and (max-width:480px) {
            .preheader .rightColumnContent,
            .footer .rightColumnContent {
                text-align: left !important;
            }
            .preheader .rightColumnContent div,
            .preheader .rightColumnContent span,
            .footer .rightColumnContent div,
            .footer .rightColumnContent span {
                text-align: left !important;
            }
            .preheader .rightColumnContent,
            .preheader .leftColumnContent {
                font-size: 80% !important;
                padding: 5px 0;
            }
            table.wrapper-mobile {
                width: 100% !important;
                table-layout: fixed;
            }
            img.max-width {
                height: auto !important;
                max-width: 480px !important;
            }
            a.bulletproof-button {
                display: block !important;
                width: auto !important;
                font-size: 80%;
                padding-left: 0 !important;
                padding-right: 0 !important;
            }
            .columns {
                width: 100% !important;
            }
            .column {
                display: block !important;
                width: 100% !important;
                padding-left: 0 !important;
                padding-right: 0 !important;
                margin-left: 0 !important;
                margin-right: 0 !important;
            }
        }
    </style>
    <!--user entered Head Start-->

    <!--End Head user entered-->
</head>
<body>
<center class="wrapper" data-link-color="#7baa84" data-body-style="font-size: 14px; font-family: verdana,geneva,sans-serif; color: #747474; background-color: #ffffff;">

    <div class="webkit">
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#ffffff">
            <tr>
                <td valign="top" bgcolor="#ffffff" width="100%">
                    <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td width="100%">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td>
                                            <!--[if mso]>
                                            <center>
                                                <table><tr><td width="600">
                                            <![endif]-->
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width:600px;" align="center">
                                                <tr>
                                                    <td role="modules-container" style="padding: 0px 0px 0px 0px; color: #747474; text-align: left;" bgcolor="#ffffff" width="100%" align="left">

                                                        <table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%"
                                                               style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
                                                            <tr>
                                                                <td role="module-content">
                                                                    <p></p>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="font-size:6px;line-height:10px;padding:10px 0px 20px 0px;" valign="top" align="center">
                                                                    <img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;" src="https://marketing-image-production.s3.amazonaws.com/uploads/6212c4140074c6a8953f5ef8d8ae34052eba6b87f406d29f74d291f7ba604ba0161d4b04d88bca08143c69acd991ee5eb99156450cea007e5b82e83f28a18229.jpg" alt="" width="80" data-proportionally-constrained="true" height="86">
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="font-size:6px;line-height:10px;padding:0px 0px 0px 0px;" valign="top" align="center">
                                                                    <a href="#">
                                                                        <img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:100% !important;width:100%;height:auto !important;" src="https://marketing-image-production.s3.amazonaws.com/uploads/f9eeee97181cfbaa562344c269cd5b75d35b82be9528f42b3643cf01392f50dcb1d3969b71b13247b0108f30b08f6273917209a5b7ba1fddf6c5874e83772722.jpg" alt="" width="600">
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="background-color:#7baa84;padding:12px 0px 12px 0px;line-height:28px;text-align:inherit;"
                                                                    height="100%"
                                                                    valign="top"
                                                                    bgcolor="#7baa84">
                                                                    <h3 style="text-align: center;"><span style="font-size:16px;"><span style="font-family:verdana,geneva,sans-serif;"><span style="color:#FFFFFF;"><span id="docs-internal-guid-c2b6f8be-7fff-3460-ce06-6a33efff5811" style="font-weight:normal;"><span style="font-weight: 400; font-style: normal; font-variant: normal; white-space: pre-wrap;">{{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.CHOOSEYOUROWNBREIL") }} </span></span></span></span></span></h3>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:18px 0px 6px 20px;line-height:22px;text-align:inherit;"
                                                                    height="100%"
                                                                    valign="top"
                                                                    bgcolor="">
                                                                    <div><span style="font-size:14px;"><span style="color:#696969;"><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; white-space: pre-wrap;">{{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.HELLO") }} {{ $first_name }},</span></span></span></div>

                                                                    <div>&nbsp;</div>

                                                                    <div>
                                                                        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:14px;"><span style="color:#696969;"><span id="docs-internal-guid-e1f8db86-7fff-83c7-3d9b-ac83f5145b5e" style="font-weight:normal;"><span style="font-weight: 400; font-style: normal; font-variant: normal; white-space: pre-wrap;">{{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.TESTPACKAGEAWEEKAGO") }}</span></span></span></span></p>
                                                                        &nbsp;

                                                                        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:14px;"><span style="color:#696969;"><span id="docs-internal-guid-e1f8db86-7fff-83c7-3d9b-ac83f5145b5e" style="font-weight:normal;"><span style="font-weight: 400; font-style: normal; font-variant: normal; white-space: pre-wrap;">{{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.CUSTOMNEXTDELIVERY") }} </span></span></span><span style="color:#7BAA84;"><span style="font-weight:normal;"><span style="font-weight: bold; font-style: normal; font-variant: normal; white-space: pre-wrap;">{{ $cut_off_date }}</span></span></span><span style="color:#696969;"><span style="font-weight:normal;"><span style="font-weight: 400; font-style: normal; font-variant: normal; white-space: pre-wrap;"> {{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.CUSTOMNEXTDELIVERY2") }} </span></span></span></span></p>

                                                                        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;">&nbsp;</p>

                                                                        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;">&nbsp;</p>

                                                                        <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-size:14px;"><span style="color:#696969;"><span id="docs-internal-guid-0146a56a-7fff-de30-b84f-6c1a86ea114e" style="font-weight:normal;"><span style="background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; white-space: pre-wrap;">{{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.NEXTDELIVERY") }} </span></span></span><span style="color:#7BAA84;"><span style="font-weight:normal;"><span style="background-color: transparent; font-weight: bold; font-style: normal; font-variant: normal; white-space: pre-wrap;">{{ $delivery_date }}</span></span></span><span style="color:#696969;"><span style="font-weight:normal;"><span style="background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; white-space: pre-wrap;"> {{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.NEXTDELIVERY2") }}</span></span></span></span></p>

                                                                        <div>&nbsp;</div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="font-size:6px;line-height:10px;padding:0px 0px 0px 0px;" valign="top" align="center">
                                                                    <img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:100% !important;width:100%;height:auto !important;" src="https://marketing-image-production.s3.amazonaws.com/uploads/3a72de9bf7bbebeafbb697d35f7f6a25ff81d9064348a00aabdf5b7ba75f33bff9da2d9a80e846686ca9f8a2fdcc4938383181994b9864902c7faef76954b31d.png" alt="" width="600">
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module"
                                                               role="module"
                                                               data-type="spacer"
                                                               border="0"
                                                               cellpadding="0"
                                                               cellspacing="0"
                                                               width="100%"
                                                               style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:0px 0px 30px 0px;"
                                                                    role="module-content"
                                                                    bgcolor="">
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table  border="0"
                                                                cellpadding="0"
                                                                cellspacing="0"
                                                                align="center"
                                                                width="100%"
                                                                role="module"
                                                                data-type="columns"
                                                                data-version="2"
                                                                style="padding:0px 0px 0px 0px;box-sizing:border-box;"
                                                                bgcolor="">
                                                            <tr role='module-content'>
                                                                <td height="100%" valign="top">
                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                    <center>
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-spacing:0;border-collapse:collapse;table-layout: fixed;" >
                                                                            <tr>
                                                                    <![endif]-->

                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                    <td width="600.000px" valign="top" style="padding: 0px 0px 0px 0px;border-collapse: collapse;" >
                                                                    <![endif]-->

                                                                    <table  width="600.000"
                                                                            style="width:600.000px;border-spacing:0;border-collapse:collapse;margin:0px 0px 0px 0px;"
                                                                            cellpadding="0"
                                                                            cellspacing="0"
                                                                            align="left"
                                                                            border="0"
                                                                            bgcolor=""
                                                                            class="column column-0 of-1
empty"
                                                                    >
                                                                        <tr>
                                                                            <td style="padding:0px;margin:0px;border-spacing:0;">

                                                                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                                                    <tr>
                                                                                        <td style="padding:0px 0px 0px 20px;line-height:22px;text-align:inherit;"
                                                                                            height="100%"
                                                                                            valign="top"
                                                                                            bgcolor="">
                                                                                            <p dir="ltr" style="line-height:1.38;margin-top:0pt;margin-bottom:0pt;"><span style="font-family:verdana,geneva,sans-serif;"><span style="color:#7BAA84;"><span id="docs-internal-guid-e64c9817-7fff-7988-4fb6-f12709d3c8ab"><span style="font-size: 11pt; font-weight: 700; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">Jeweils bis 5 Tage vor der n&auml;chsten Lieferung kannst du in deinem Account</span></span></span></span></p>

                                                                                            <div>&nbsp;</div>

                                                                                            <div>
                                                                                                <ul style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: verdana, geneva, sans-serif; font-size: 14px; color: rgb(116, 116, 116);">
                                                                                                    <li style="font-size: 14px;">{{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.DOCUSTOMBREILS") }}</li>
                                                                                                    <li style="font-size: 14px;">{{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.DOCUSTOMDELIVERYFREQUENCIE") }}</li>
                                                                                                    <li style="font-size: 14px;">{{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.DOCUSTOMDELIVERYDAY") }}</li>
                                                                                                    <li style="font-size: 14px;">{{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.DOCUSTOMPAYMENTMETHOD") }}</li>
                                                                                                    <li style="font-size: 14px;">{{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.DOCUSTOMPAUSE") }} <span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: verdana, geneva, sans-serif; font-size: 14px;">{{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.QUIT") }}</span></li>
                                                                                                </ul>
                                                                                            </div>

                                                                                        </td>
                                                                                    </tr>
                                                                                </table>

                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                    </td>
                                                                    <![endif]-->
                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                    <tr>
                                                                        </table>
                                                                        </center>
                                                                    <![endif]-->
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table border="0" cellPadding="0" cellSpacing="0" class="module" data-role="module-button" data-type="button" role="module" style="table-layout:fixed" width="100%"><tbody><tr><td align="center" class="outer-td" style="padding:0px 0px 0px 0px"><table border="0" cellPadding="0" cellSpacing="0" class="button-css__deep-table___2OZyb wrapper-mobile" style="text-align:center"><tbody><tr><td align="center" bgcolor="#7baa84" class="inner-td" style="border-radius:6px;font-size:16px;text-align:center;background-color:inherit"><a style="background-color:#7baa84;border:1px solid #333333;border-color:#7baa84;border-radius:6px;border-width:1px;color:#ffffff;display:inline-block;font-family:arial,helvetica,sans-serif;font-size:16px;font-weight:normal;letter-spacing:0px;line-height:16px;padding:12px 18px 12px 18px;text-align:center;text-decoration:none" href="{{ $app_url . 'mein-account/meine-abos' }}" target="_blank">{{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.TOYOURACCOUNT") }}</a></td></tr></tbody></table></td></tr></tbody></table>
                                                        <table class="module"
                                                               role="module"
                                                               data-type="spacer"
                                                               border="0"
                                                               cellpadding="0"
                                                               cellspacing="0"
                                                               width="100%"
                                                               style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:0px 0px 30px 0px;"
                                                                    role="module-content"
                                                                    bgcolor="">
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table  border="0"
                                                                cellpadding="0"
                                                                cellspacing="0"
                                                                align="center"
                                                                width="100%"
                                                                role="module"
                                                                data-type="columns"
                                                                data-version="2"
                                                                style="padding:0px 0px 0px 0px;box-sizing:border-box;"
                                                                bgcolor="">
                                                            <tr role='module-content'>
                                                                <td height="100%" valign="top">
                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                    <center>
                                                                        <table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-spacing:0;border-collapse:collapse;table-layout: fixed;" >
                                                                            <tr>
                                                                    <![endif]-->

                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                    <td width="600.000px" valign="top" style="padding: 0px 0px 0px 0px;border-collapse: collapse;" >
                                                                    <![endif]-->

                                                                    <table  width="600.000"
                                                                            style="width:600.000px;border-spacing:0;border-collapse:collapse;margin:0px 0px 0px 0px;"
                                                                            cellpadding="0"
                                                                            cellspacing="0"
                                                                            align="left"
                                                                            border="0"
                                                                            bgcolor=""
                                                                            class="column column-0 of-1
empty"
                                                                    >
                                                                        <tr>
                                                                            <td style="padding:0px;margin:0px;border-spacing:0;">

                                                                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                                                    <tr>
                                                                                        <td style="padding:0px 0px 0px 20px;line-height:22px;text-align:inherit;"
                                                                                            height="100%"
                                                                                            valign="top"
                                                                                            bgcolor="">
                                                                                            <h3><font color="#7baa84">{{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.HERESTHEOVERVIEW") }}</font></h3>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>

                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                    </td>
                                                                    <![endif]-->
                                                                    <!--[if (gte mso 9)|(IE)]>
                                                                    <tr>
                                                                        </table>
                                                                        </center>
                                                                    <![endif]-->
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        @include('email.order-details')

                                                        <table class="module"
                                                               role="module"
                                                               data-type="spacer"
                                                               border="0"
                                                               cellpadding="0"
                                                               cellspacing="0"
                                                               width="100%"
                                                               style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:0px 0px 30px 0px;"
                                                                    role="module-content"
                                                                    bgcolor="">
                                                                </td>
                                                            </tr>
                                                        </table>






                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:30px 0px 6px 20px;line-height:22px;text-align:inherit;"
                                                                    height="100%"
                                                                    valign="top"
                                                                    bgcolor="">
                                                                    <div style="text-align: center;"><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; white-space: pre-wrap; background-color: rgb(255, 255, 255);">{{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.MOREQUESTIONS") }} <a href="mailto:support@yamo.bio">support@yamo.bio</a>. {{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.WEARE") }}</span></span></span><span style="color: rgb(116, 116, 116); font-family: verdana, geneva, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; text-align: center; white-space: pre-wrap;">{{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.THEREFORYOU") }}</span></div>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:30px 0px 6px 20px;line-height:22px;text-align:inherit;"
                                                                    height="100%"
                                                                    valign="top"
                                                                    bgcolor="">
                                                                    <div style="text-align: center;"><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; white-space: pre-wrap; background-color: rgb(255, 255, 255);">{{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.BESTREGARDS") }},</span></span></span></div>

                                                                    <div style="text-align: center;"><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; white-space: pre-wrap; background-color: rgb(255, 255, 255);">{{ Translation::trans("MAIL.SUBSCRIPTION-CHANGE.YOURTEAM") }}</span></span></span></div>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module"
                                                               role="module"
                                                               data-type="spacer"
                                                               border="0"
                                                               cellpadding="0"
                                                               cellspacing="0"
                                                               width="100%"
                                                               style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:0px 0px 30px 0px;"
                                                                    role="module-content"
                                                                    bgcolor="">
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module"
                                                               role="module"
                                                               data-type="divider"
                                                               border="0"
                                                               cellpadding="0"
                                                               cellspacing="0"
                                                               width="100%"
                                                               style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:15px 20px 0px 20px;"
                                                                    role="module-content"
                                                                    height="100%"
                                                                    valign="top"
                                                                    bgcolor="">
                                                                    <table border="0"
                                                                           cellpadding="0"
                                                                           cellspacing="0"
                                                                           align="center"
                                                                           width="100%"
                                                                           height="1px"
                                                                           style="line-height:1px; font-size:1px;">
                                                                        <tr>
                                                                            <td
                                                                                    style="padding: 0px 0px 1px 0px;"
                                                                                    bgcolor="#c5c5c5"></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <!-- include footer address section -->
                                                        @include('email.footer-address')

                                                        <table class="module" role="module" data-type="social" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                            <tbody>
                                                            <tr>
                                                                <td valign="top" style="padding:20px 0px 30px 0px;font-size:6px;line-height:10px;">
                                                                    <table align="center">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td style="padding: 0px 5px;">
                                                                                <a role="social-icon-link"  href="https://www.facebook.com/yamobaby/" target="_blank" alt="Facebook"
                                                                                   data-nolink="false"
                                                                                   title="Facebook "
                                                                                   style="-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;display:inline-block;background-color:#3B579D;">
                                                                                    <img role="social-icon" alt="Facebook" title="Facebook "
                                                                                         height="30"
                                                                                         width="30"
                                                                                         style="height: 30px, width: 30px"
                                                                                         src="https://marketing-image-production.s3.amazonaws.com/social/white/facebook.png" />
                                                                                </a>
                                                                            </td>
                                                                            <td style="padding: 0px 5px;">
                                                                                <a role="social-icon-link"  href="https://twitter.com/yamobaby?lang=en" target="_blank" alt="Twitter"
                                                                                   data-nolink="false"
                                                                                   title="Twitter "
                                                                                   style="-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;display:inline-block;background-color:#7AC4F7;">
                                                                                    <img role="social-icon" alt="Twitter" title="Twitter "
                                                                                         height="30"
                                                                                         width="30"
                                                                                         style="height: 30px, width: 30px"
                                                                                         src="https://marketing-image-production.s3.amazonaws.com/social/white/twitter.png" />
                                                                                </a>
                                                                            </td>
                                                                            <td style="padding: 0px 5px;">
                                                                                <a role="social-icon-link"  href="https://www.instagram.com/yamobaby/" target="_blank" alt="Instagram"
                                                                                   data-nolink="false"
                                                                                   title="Instagram "
                                                                                   style="-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;display:inline-block;background-color:#7F4B30;">
                                                                                    <img role="social-icon" alt="Instagram" title="Instagram "
                                                                                         height="30"
                                                                                         width="30"
                                                                                         style="height: 30px, width: 30px"
                                                                                         src="https://marketing-image-production.s3.amazonaws.com/social/white/instagram.png" />
                                                                                </a>
                                                                            </td>



                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                    </td>
                                                </tr>
                                            </table>
                                            <!--[if mso]>
                                            </td></tr></table>
                                            </center>
                                            <![endif]-->


                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</center>
</body>
</html>