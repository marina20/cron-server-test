<?php use \App\Models\Translation; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" /><!--[if !mso]><!-->
<meta http-equiv="X-UA-Compatible" content="IE=Edge" /><!--<![endif]-->
<!--[if (gte mso 9)|(IE)]>
<xml>
<o:OfficeDocumentSettings>
<o:AllowPNG/>
<o:PixelsPerInch>96</o:PixelsPerInch>
</o:OfficeDocumentSettings>
</xml>
<![endif]-->
<!--[if (gte mso 9)|(IE)]>
<style type="text/css">
body {width: 600px;margin: 0 auto;}
table {border-collapse: collapse;}
table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
img {-ms-interpolation-mode: bicubic;}
</style>
<![endif]-->

<style type="text/css">
body, p, div {
font-family: verdana,geneva,sans-serif;
font-size: 14px;
}
body {
color: #747474;
}
body a {
color: #7baa84;
text-decoration: none;
}
p { margin: 0; padding: 0; }
table.wrapper {
width:100% !important;
table-layout: fixed;
-webkit-font-smoothing: antialiased;
-webkit-text-size-adjust: 100%;
-moz-text-size-adjust: 100%;
-ms-text-size-adjust: 100%;
}
img.max-width {
max-width: 100% !important;
}
.column.of-2 {
width: 50%;
}
.column.of-3 {
width: 33.333%;
}
.column.of-4 {
width: 25%;
}
@media screen and (max-width:480px) {
.preheader .rightColumnContent,
.footer .rightColumnContent {
text-align: left !important;
}
.preheader .rightColumnContent div,
.preheader .rightColumnContent span,
.footer .rightColumnContent div,
.footer .rightColumnContent span {
text-align: left !important;
}
.preheader .rightColumnContent,
.preheader .leftColumnContent {
font-size: 80% !important;
padding: 5px 0;
}
table.wrapper-mobile {
width: 100% !important;
table-layout: fixed;
}
img.max-width {
height: auto !important;
max-width: 480px !important;
}
a.bulletproof-button {
display: block !important;
width: auto !important;
font-size: 80%;
padding-left: 0 !important;
padding-right: 0 !important;
}
.columns {
width: 100% !important;
}
.column {
display: block !important;
width: 100% !important;
padding-left: 0 !important;
padding-right: 0 !important;
}
}
</style>
<!--user entered Head Start-->

<!--End Head user entered-->
</head>
<body>
<center class="wrapper" data-link-color="#7baa84" data-body-style="font-size: 14px; font-family: verdana,geneva,sans-serif; color: #747474; background-color: #ffffff;">
<div class="webkit">
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#ffffff">
<tr>
<td valign="top" bgcolor="#ffffff" width="100%">
<table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
<tr>
<td width="100%">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
<td>
<!--[if mso]>
<center>
<table><tr><td width="600">
<![endif]-->
<table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width:600px;" align="center">
<tr>
<td role="modules-container" style="padding: 0px 0px 0px 0px; color: #747474; text-align: left;" bgcolor="#ffffff" width="100%" align="left">

<table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%"
style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
<tr>
<td role="module-content">
<p></p>
</td>
</tr>
</table>

<table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="font-size:6px;line-height:10px;padding:10px 0px 20px 0px;" valign="top" align="center">
<img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;" src="https://marketing-image-production.s3.amazonaws.com/uploads/6212c4140074c6a8953f5ef8d8ae34052eba6b87f406d29f74d291f7ba604ba0161d4b04d88bca08143c69acd991ee5eb99156450cea007e5b82e83f28a18229.jpg" alt="" width="80" data-proportionally-constrained="true" height="86">
</td>
</tr>
</table>

<table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="font-size:6px;line-height:10px;padding:0px 0px 0px 0px;" valign="top" align="center">
<img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:100% !important;width:100%;height:auto !important;" src="https://marketing-image-production.s3.amazonaws.com/uploads/e8ead1c216bc17a0e7a85dc7f3b8b58f75cd05746aef789cb0a68c764fc11bd2ec83fd9695f9e097404a1b77170e8ca787df8c1fdaeb5aa34ec8991826fa1b64.png" alt="" width="600">
</td>
</tr>
</table>

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="background-color:#7baa84;padding:12px 0px 12px 0px;line-height:28px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="#7baa84">
<h2 style="text-align: center;"><span style="color:#FFFFFF;">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.SADYOUGO") }}</span></h2>
</td>
</tr>
</table>

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:18px 0px 6px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; white-space: pre-wrap;">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.HELLO") }} {{ $name }}, {{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.YOUVECANCLED") }}</span></span></span></div>
</td>
</tr>
</table>

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:18px 0px 6px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; white-space: pre-wrap; background-color: rgb(255, 255, 255);">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.LETUSKNOW") }} <a href="mailto:{{ $hello_email }}">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.EMAIL") }}</a> {{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.ORCALL") }} <a style="white-space:nowrap;" href="tel:+41417110404">+41 (0)41 711 04 04</a>.</span></span></span></div>
</td>
</tr>
</table>

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:18px 0px 6px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; white-space: pre-wrap; background-color: rgb(255, 255, 255);">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.IFACCIDENT") }}:</span></span></span></div>
</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="module" data-role="module-button" data-type="button" role="module" style="table-layout:fixed;" width="100%"><tbody><tr><td align="center" bgcolor="#ffffff" class="outer-td" style="padding:20px 0px 30px 0px;background-color:#ffffff;"><table border="0" cellpadding="0" cellspacing="0" class="button-css__deep-table___2OZyb wrapper-mobile" style="text-align:center;"><tbody><tr><td align="center" bgcolor="#7baa84" class="inner-td" style="border-radius:6px;font-size:16px;text-align:center;background-color:inherit;"><a style="background-color:#7baa84;border:1px solid #333333;border-color:#ffffff;border-radius:4px;border-width:0px;color:#ffffff;display:inline-block;font-family:verdana,geneva,sans-serif;font-size:14px;font-weight:normal;letter-spacing:0px;line-height:18px;padding:14px 30px 14px 30px;text-align:center;text-decoration:none;" href="{{ $app_url . 'mein-account/meine-abos' }}" target="_blank">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.TOYOURACCOUNT") }}</a></td></tr></tbody></table></td></tr></tbody></table>
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
<tr>
<td style="padding:0px 0px 0px 0px;background-color:#ffffff;" bgcolor="#ffffff">

<!--[if mso]>
<table width="99%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-0 of-1 empty">
<tr>
<td class="columns--column-content">

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:0px 0px 0px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<h3><span style="font-family:verdana,geneva,sans-serif;"><span style="color:#338743;">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.YOURCANCLEDSUBSCRIPTION") }}:</span></span></h3>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

</td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
<tr>
<td style="padding:10px 0px 14px 0px;background-color:#c1c1c1;" bgcolor="#c1c1c1">

<!--[if mso]>
<table width="49%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-0 of-2 empty">
<tr>
<td class="columns--column-content">

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:6px 10px 6px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="color:#FFFFFF;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.BILLNR") }}</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">:&nbsp;</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;">{{ $order_id }}</span></span></span></div>
</td>
</tr>
</table>

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:6px 10px 6px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="color:#FFFFFF;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.CUSTOMERNR") }}</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">:&nbsp;</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;">{{ $customer_number }}</span></span></span></div>
</td>
</tr>
</table>

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:6px 10px 6px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="color:#FFFFFF;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.BILLDATE") }}</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">:&nbsp;</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;">{{ $date_created }}</span></span></span></div>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

<!--[if mso]>
<table width="49%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-1 of-2 empty">
<tr>
<td class="columns--column-content">

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:6px 10px 6px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="color:#FFFFFF;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.DELIVERYDATE") }}</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">:&nbsp;</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;">{{ $delivery_date }}</span></span></span></div>
</td>
</tr>
</table>

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:6px 10px 6px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="color:#FFFFFF;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.PAYMENTMETHOD") }}</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">:&nbsp;</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;">{{ $payment_method }}</span></span></span></div>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

</td>
</tr>
</table>

@foreach($order_items as $item)
@if($item->item_type == 'line_item')
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
<tr>
<td style="padding:20px 0px 20px 10px;background-color:#f7f7f7;" bgcolor="#f7f7f7">

<!--[if mso]>
<table width="49%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-0 of-2 empty">
<tr>
<td class="columns--column-content">

<table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="font-size:6px;line-height:10px;padding:0px 0px 0px 0px;" valign="top" align="center">
<img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;" src="{{ $item->product->image }}" alt="" width="182px">
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

<!--[if mso]>
<table width="49%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-1 of-2 empty">
<tr>
<td class="columns--column-content">

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:6px 10px 6px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; font-family: verdana, geneva, sans-serif; font-size: 14px; color: rgb(116, 116, 116); background-color: rgb(247, 247, 247);">Produkt:&nbsp;</span><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; font-family: verdana, geneva, sans-serif; font-size: 14px; color: rgb(116, 116, 116); background-color: rgb(247, 247, 247);">{{ $item->item_name }}</span></div>
</td>
</tr>
</table>

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:6px 10px 6px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.QUANTITY") }}</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">:&nbsp;</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;">{{ $item->qty }}</span></span></div>
</td>
</tr>
</table>

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:6px 10px 6px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">Preis</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">:&nbsp;</span></span>
@if($item->total != '0.00')
<span class="m_3202122474823768701m_3719169600630861421m_91549262569909069woocommerce-Price-currencySymbol" style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; font-family: verdana, geneva, sans-serif; font-size: 14px; color: rgb(116, 116, 116); background-color: rgb(247, 247, 247);">{{ $currency }}</span><span style="color: rgb(116, 116, 116); font-family: verdana, geneva, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(247, 247, 247);">&nbsp;{{ $item->total  }}</span><span style="font-size:12px;"><small style="color: rgb(51, 51, 51); font-family: arial, sans-serif; font-variant-ligatures: normal; font-variant-caps: normal;"> ({{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.INCL") }} {{ $currency }} {{ $item->total_tax }} {{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.MWST") }})</small></span>
@else
<span style="color: rgb(116, 116, 116); font-family: verdana, geneva, sans-serif; font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(247, 247, 247);">&nbsp;{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.GIFT") }}</span>
@endif

</div>
</td>
</tr>
</table>

@if(!empty($item->product->short_description))
<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:6px 10px 6px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.DESCRIPTION") }}:</span></span></div>
<ul style="padding:0; margin:0;">
@foreach ($item->product->ingredients as $ingredient)
<li><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;">{{ $ingredient }}</span></li>
@endforeach
</ul>
</td>
</tr>
</table>
@endif

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

</td>
</tr>
</table>
@endif
@endforeach

<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
<tr>
<td style="padding:0px 0px 0px 0px;" bgcolor="">

<!--[if mso]>
<table width="49%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-0 of-2 empty">
<tr>
<td class="columns--column-content">

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.SUBTOTAL") }}:</span></span></div>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

<!--[if mso]>
<table width="49%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-1 of-2 empty">
<tr>
<td class="columns--column-content">

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">{{ $currency }} {{ $subtotal }}&nbsp;</span><span style="font-size:12px;"><small style="color: rgb(51, 51, 51); font-family: arial, sans-serif; font-variant-ligatures: normal; font-variant-caps: normal; background-color: rgb(253, 253, 253);">(inkl. {{ $currency }} {{ $subtotal_tax }} {{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.MWST") }})</small></span></span></div>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

</td>
</tr>
</table>

    @if(isset($subscription_reduction))


        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
            <tr>
                <td style="padding:0px 0px 0px 0px;" bgcolor="">

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-0 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.ORDER-DETAILS.SUBSCRIPTIONREDUCTION") }}:</span></span></div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-1 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div>
                                                <span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">- {{ $subscription_reduction }}&nbsp;</span></span>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                </td>
            </tr>
        </table>



    @endif

@if($discount != '0.00')
<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
<tr>
<td style="padding:0px 0px 0px 0px;" bgcolor="">

<!--[if mso]>
<table width="49%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-0 of-2 empty">
<tr>
<td class="columns--column-content">

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.USEDVOUCHER") }}:</span></span></div>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

<!--[if mso]>
<table width="49%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-1 of-2 empty">
<tr>
<td class="columns--column-content">

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);"></span><span class="m_3202122474823768701m_3719169600630861421m_91549262569909069woocommerce-Price-amount m_3202122474823768701m_3719169600630861421m_91549262569909069amount" style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">{{ $coupon_code }}</span></span></div>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

<!--[if mso]>
<table width="49%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-0 of-2 empty">
<tr>
<td class="columns--column-content">

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">&nbsp;</span></span></div>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

<!--[if mso]>
<table width="49%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-1 of-2 empty">
<tr>
<td class="columns--column-content">

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);"></span><span class="m_3202122474823768701m_3719169600630861421m_91549262569909069woocommerce-Price-amount m_3202122474823768701m_3719169600630861421m_91549262569909069amount" style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);"><span class="m_3202122474823768701m_3719169600630861421m_91549262569909069woocommerce-Price-currencySymbol">{{ $currency }}</span>&nbsp;{{ $discount }}</span></span></div>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

</td>
</tr>
</table>
@endif

    @if($loyalty_cost != '0.00')
        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
            <tr>
                <td style="padding:0px 0px 0px 0px;" bgcolor="">

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-0 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">{{ Translation::trans("YAMO-CLUB") }}:</span></span></div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-1 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);"></span><span class="m_3202122474823768701m_3719169600630861421m_91549262569909069woocommerce-Price-amount m_3202122474823768701m_3719169600630861421m_91549262569909069amount" style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">{{ $coupon_code }}</span></span></div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-0 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">&nbsp;</span></span></div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-1 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);"></span><span class="m_3202122474823768701m_3719169600630861421m_91549262569909069woocommerce-Price-amount m_3202122474823768701m_3719169600630861421m_91549262569909069amount" style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);"><span class="m_3202122474823768701m_3719169600630861421m_91549262569909069woocommerce-Price-currencySymbol">{{ $loyalty }}</span>&nbsp</span></span></div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                </td>
            </tr>
        </table>
    @endif

    @if($referral_cost != "0.00")


        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
            <tr>
                <td style="padding:0px 0px 0px 0px;" bgcolor="">

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-0 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.ORDER-DETAILS.REFERRALDISCOUNT") }}:</span></span></div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-1 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div>
                                                <span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">- {{ $referral }}&nbsp;</span></span>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                </td>
            </tr>
        </table>



    @endif


    @if($wallet_cost != "0.00")


        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
            <tr>
                <td style="padding:0px 0px 0px 0px;" bgcolor="">

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-0 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.ORDER-DETAILS.WALLETDISCOUNT") }}:</span></span></div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-1 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div>
                                                <span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">- {{ $wallet }}&nbsp;</span></span>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                </td>
            </tr>
        </table>
    @endif


<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
<tr>
<td style="padding:0px 0px 0px 0px;" bgcolor="">

<!--[if mso]>
<table width="49%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-0 of-2 empty">
<tr>
<td class="columns--column-content">

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.COOLDELIVERY") }}:</span></span></div>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

<!--[if mso]>
<table width="49%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-1 of-2 empty">
<tr>
<td class="columns--column-content">

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div>
@if($shipping != '0.00')
<span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">{{ $currency }} {{ $shipping }}&nbsp;</span><span style="font-size:12px;"><small style="color: rgb(51, 51, 51); font-family: arial, sans-serif; font-variant-ligatures: normal; font-variant-caps: normal; background-color: rgb(253, 253, 253);">({{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.INCL") }} {{ $currency }} {{ $shipping_tax }} {{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.MWST") }})</small></span></span>
@else
<span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.FREE") }}&nbsp;</span></span>
@endif
</div>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

</td>
</tr>
</table>

<table class="module"
role="module"
data-type="divider"
border="0"
cellpadding="0"
cellspacing="0"
width="100%"
style="table-layout: fixed;">
<tr>
<td style="padding:15px 20px 0px 20px;"
role="module-content"
height="100%"
valign="top"
bgcolor="">
<table border="0"
cellpadding="0"
cellspacing="0"
align="center"
width="100%"
height="1px"
style="line-height:1px; font-size:1px;">
<tr>
<td
style="padding: 0px 0px 1px 0px;"
bgcolor="#c5c5c5"></td>
</tr>
</table>
</td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
<tr>
<td style="padding:15px 0px 20px 0px;" bgcolor="">

<!--[if mso]>
<table width="49%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-0 of-2 empty">
<tr>
<td class="columns--column-content">

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.TOTAL") }}:</span></span></div>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

<!--[if mso]>
<table width="49%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-1 of-2 empty">
<tr>
<td class="columns--column-content">

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">{{ $currency }} {{ $total }}</span><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">&nbsp;</span><span style="color: rgb(51, 51, 51); font-size: 11.6667px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.INCL") }} {{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.MWST") }}</span></span></div>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

</td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
<tr>
<td style="padding:0px 0px 0px 20px;background-color:#f1f1f1;" bgcolor="#f1f1f1">

<!--[if mso]>
<table width="99%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-0 of-1 empty">
<tr>
<td class="columns--column-content">

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:10px 0px 0px 0px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<h3><span style="color:#338743;">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.CUSTOMERDATA") }}</span></h3>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

</td>
</tr>
</table>

<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
<tr>
<td style="padding:0px 20px 15px 20px;background-color:#F1F1F1;" bgcolor="#F1F1F1">

<!--[if mso]>
<table width="49%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-0 of-2 empty">
<tr>
<td class="columns--column-content">

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:10px 0px 18px 0px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-family:verdana,geneva,sans-serif;"><strong><span style="color:#338743;">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.DELIVERYADDRESS") }}</span></strong></span></div>
@if(!empty($shipping_company))
<div><span style="font-family:verdana,geneva,sans-serif;">{{ $shipping_company }}</span></div>
@endif
<div><span style="font-family:verdana,geneva,sans-serif;">{{ $shipping_name }}</span></div>

<div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; text-align: inherit;">{{ $shipping_address }}</span></span></div>

<div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; text-align: inherit;">{{ $shipping_postnumber }} {{ $shipping_city }}</span></span></div>

<div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; text-align: inherit;">{{ $shipping_country }}</span></span></div>

<div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(116, 116, 116); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; background-color: rgb(241, 241, 241);">{{ $shipping_email }}</span></span></div>

<div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(116, 116, 116); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; background-color: rgb(241, 241, 241);">{{ $shipping_phone }}</span></span></div>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

<!--[if mso]>
<table width="49%" align="left"><tr><td>
<![endif]-->
<table style="padding: 0px 0px 0px 0px;"
align="left"
valign="top"
height="100%"
class="column column-1 of-2 empty">
<tr>
<td class="columns--column-content">

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:10px 0px 18px 0px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-family:verdana,geneva,sans-serif;"><font color="#338743"><span style="font-weight: 600;">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.BILLADDRESS") }}</span></font></span></div>

@if(!empty($billing_company))
<div><span style="font-family:verdana,geneva,sans-serif;">{{ $billing_company }}</span></div>
@endif

<div><span style="font-family:verdana,geneva,sans-serif;">{{ $billing_name }}</span></div>

<div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; text-align: inherit;"> {{ $billing_address }}</span></span></div>

<div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; text-align: inherit;">{{ $billing_postnumber }} {{ $billing_city }}</span></span></div>

<div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; text-align: inherit;">{{ $billing_country }}</span></span></div>

<div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(116, 116, 116); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; background-color: rgb(241, 241, 241);">{{ $billing_email }}</span></span></div>

<div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(116, 116, 116); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; background-color: rgb(241, 241, 241);">{{ $billing_phone }}</span></span></div>
</td>
</tr>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->

</td>
</tr>
</table>

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:30px 0px 6px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div style="text-align: center;">
<div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; font-family: verdana, geneva, sans-serif; font-size: 14px; color: rgb(116, 116, 116); background-color: rgb(255, 255, 255); text-align: center;"><span style="font-size: 14px;"><span style="font-family: verdana, geneva, sans-serif; font-size: 14px;"><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; font-size: 14px; white-space: pre-wrap;">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.BESTREGARDS") }},</span></span></span></div>

<div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; font-family: verdana, geneva, sans-serif; font-size: 14px; color: rgb(116, 116, 116); background-color: rgb(255, 255, 255); text-align: center;"><span style="font-size: 14px;"><span style="font-family: verdana, geneva, sans-serif; font-size: 14px;"><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; font-size: 14px; white-space: pre-wrap;">{{ Translation::trans("MAIL.SUBSCRIPTION-CANCEL.ZOEANDTEAM") }}</span></span></span></div>
</div>
</td>
</tr>
</table>

<table class="module"
role="module"
data-type="spacer"
border="0"
cellpadding="0"
cellspacing="0"
width="100%"
style="table-layout: fixed;">
<tr>
<td style="padding:0px 0px 30px 0px;"
role="module-content"
bgcolor="">
</td>
</tr>
</table>

<table class="module"
role="module"
data-type="divider"
border="0"
cellpadding="0"
cellspacing="0"
width="100%"
style="table-layout: fixed;">
<tr>
<td style="padding:15px 20px 0px 20px;"
role="module-content"
height="100%"
valign="top"
bgcolor="">
<table border="0"
cellpadding="0"
cellspacing="0"
align="center"
width="100%"
height="1px"
style="line-height:1px; font-size:1px;">
<tr>
<td
style="padding: 0px 0px 1px 0px;"
bgcolor="#c5c5c5"></td>
</tr>
</table>
</td>
</tr>
</table>

    <!-- include footer address section -->
    @include('email.footer-address')

<table class="module" role="module" data-type="social" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tbody>
<tr>
<td valign="top" style="padding:20px 0px 30px 0px;font-size:6px;line-height:10px;">
<table align="center">
<tbody>
<tr>
<td style="padding: 0px 5px;">
<a role="social-icon-link"  href="https://www.facebook.com/yamobaby/" target="_blank" alt="Facebook"
data-nolink="false"
title="Facebook "
style="-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;display:inline-block;background-color:#3B579D;">
<img role="social-icon" alt="Facebook" title="Facebook "
height="30"
width="30"
style="height: 30px, width: 30px"
src="https://marketing-image-production.s3.amazonaws.com/social/white/facebook.png" />
</a>
</td>
<td style="padding: 0px 5px;">
<a role="social-icon-link"  href="https://twitter.com/yamobaby?lang=en" target="_blank" alt="Twitter"
data-nolink="false"
title="Twitter "
style="-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;display:inline-block;background-color:#7AC4F7;">
<img role="social-icon" alt="Twitter" title="Twitter "
height="30"
width="30"
style="height: 30px, width: 30px"
src="https://marketing-image-production.s3.amazonaws.com/social/white/twitter.png" />
</a>
</td>
<td style="padding: 0px 5px;">
<a role="social-icon-link"  href="https://www.instagram.com/yamobaby/" target="_blank" alt="Instagram"
data-nolink="false"
title="Instagram "
style="-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;display:inline-block;background-color:#7F4B30;">
<img role="social-icon" alt="Instagram" title="Instagram "
height="30"
width="30"
style="height: 30px, width: 30px"
src="https://marketing-image-production.s3.amazonaws.com/social/white/instagram.png" />
</a>
</td>



</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
</center>
</body>
</html>