<?php use \App\Models\Translation; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" /><!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" /><!--<![endif]-->
    <!--[if (gte mso 9)|(IE)]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        body {width: 600px;margin: 0 auto;}
        table {border-collapse: collapse;}
        table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
        img {-ms-interpolation-mode: bicubic;}
    </style>
    <![endif]-->

    <style type="text/css">
        body, p, div {
            font-family: arial;
            font-size: 14px;
        }
        body {
            color: #000000;
        }
        body a {
            color: #1188E6;
            text-decoration: none;
        }
        p { margin: 0; padding: 0; }
        table.wrapper {
            width:100% !important;
            table-layout: fixed;
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: 100%;
            -moz-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }
        img.max-width {
            max-width: 100% !important;
        }
        .column.of-2 {
            width: 50%;
        }
        .column.of-3 {
            width: 33.333%;
        }
        .column.of-4 {
            width: 25%;
        }
        @media screen and (max-width:480px) {
            .preheader .rightColumnContent,
            .footer .rightColumnContent {
                text-align: left !important;
            }
            .preheader .rightColumnContent div,
            .preheader .rightColumnContent span,
            .footer .rightColumnContent div,
            .footer .rightColumnContent span {
                text-align: left !important;
            }
            .preheader .rightColumnContent,
            .preheader .leftColumnContent {
                font-size: 80% !important;
                padding: 5px 0;
            }
            table.wrapper-mobile {
                width: 100% !important;
                table-layout: fixed;
            }
            img.max-width {
                height: auto !important;
                max-width: 480px !important;
            }
            a.bulletproof-button {
                display: block !important;
                width: auto !important;
                font-size: 80%;
                padding-left: 0 !important;
                padding-right: 0 !important;
            }
            .columns {
                width: 100% !important;
            }
            .column {
                display: block !important;
                width: 100% !important;
                padding-left: 0 !important;
                padding-right: 0 !important;
                margin-left: 0 !important;
                margin-right: 0 !important;
            }
        }

    </style>
    <!--user entered Head Start-->

    <!--End Head user entered-->
</head>
<body>
<center class="wrapper" data-link-color="#1188E6" data-body-style="font-size: 14px; font-family: arial; color: #000000; background-color: #ffffff;">
    <div class="webkit">
        <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#ffffff">
            <tr>
                <td valign="top" bgcolor="#ffffff" width="100%">
                    <table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td width="100%">
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td>
                                            <!--[if mso]>
                                            <center>
                                                <table><tr><td width="600">
                                            <![endif]-->
                                            <table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width:600px;" align="center">
                                                <tr>
                                                    <td role="modules-container" style="padding: 0px 0px 0px 0px; color: #000000; text-align: left;" bgcolor="#ffffff" width="100%" align="left">

                                                        <table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%"
                                                               style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
                                                            <tr>
                                                                <td role="module-content">
                                                                    <p>{{ Translation::trans("MAIL.REFEREE-COMPLETED.GIVEBACK") }}</p>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="font-size:6px;line-height:10px;padding:0px 0px 0px 0px;" valign="top" align="center">
                                                                    <a href="https://www.yamo.bio/?utm_source=sendgrid&amp;utm_medium=email&amp;utm_campaign=leadswithcool"><img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:20% !important;width:20%;height:auto !important;" src="https://marketing-image-production.s3.amazonaws.com/uploads/6212c4140074c6a8953f5ef8d8ae34052eba6b87f406d29f74d291f7ba604ba0161d4b04d88bca08143c69acd991ee5eb99156450cea007e5b82e83f28a18229.jpg" alt="" width="120"></a>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module"
                                                               role="module"
                                                               data-type="spacer"
                                                               border="0"
                                                               cellpadding="0"
                                                               cellspacing="0"
                                                               width="100%"
                                                               style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:0px 0px 20px 0px;"
                                                                    role="module-content"
                                                                    bgcolor="">
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="font-size:6px;line-height:10px;padding:0px 0px 0px 0px;" valign="top" align="center">
                                                                    <a href="https://www.yamo.bio/shop/"><img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:100% !important;width:100%;height:auto !important;" src="https://marketing-image-production.s3.amazonaws.com/uploads/991601b1644495b206a2f92a5e136a51c017857e55adcb69a294ed3297e629d19ac8f76daff0ce88e104912226cc2a54087aaba68618e4a837d2cc05b2481f13.jpg" alt="" width="600"></a>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit;"
                                                                    height="100%"
                                                                    valign="top"
                                                                    bgcolor="">
                                                                    <div style="text-align: center;"><font color="#9bbca0" face="arial, helvetica, sans-serif"><span style="font-size: 22px; white-space: pre-wrap;"><b>Wohoo</b></span></font><span style="caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-family: arial, helvetica, sans-serif; font-size: 16px; font-style: normal; font-variant-caps: normal; text-align: center; white-space: pre-wrap;"> </span><font color="#9bbca0" face="arial, helvetica, sans-serif"><span style="font-size: 22px; white-space: pre-wrap;"><b>- Dein Discount wurde verwendet!</b></span></font></div>

                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:18px 0px 1px 0px;line-height:22px;text-align:justify;"
                                                                    height="100%"
                                                                    valign="top"
                                                                    bgcolor="">
                                                                    <div>
                                                                        <div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: arial; font-size: 14px; color: rgb(0, 0, 0); text-align: center;"><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:16px;"><span style="caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-style: normal; font-variant-caps: normal; text-align: start;">{{ Translation::trans("MAIL.REFEREE-COMPLETED.HELLO") }} {{ $name }}</span></span></span></div>

                                                                        <div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: arial; font-size: 14px; color: rgb(0, 0, 0); text-align: center;">&nbsp;</div>

                                                                        <div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: arial; font-size: 14px; color: rgb(0, 0, 0); text-align: center;"><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:16px;"><span style="caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-style: normal; font-variant-caps: normal; text-align: start;">{{ Translation::trans("MAIL.REFEREE-COMPLETED.CONCRATS") }}</span></span></span></div>

                                                                        <div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: arial; font-size: 14px; color: rgb(0, 0, 0); text-align: center;">&nbsp;</div>

                                                                        <div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: arial; font-size: 14px; color: rgb(0, 0, 0); text-align: center;"><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:16px;"><span style="caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-style: normal; font-variant-caps: normal; text-align: start;">{{ Translation::trans("MAIL.REFEREE-COMPLETED.YOURECEIVE") }} </span></span><strong><span style="font-size: 16px; font-style: normal; font-variant-caps: normal; caret-color: rgb(34, 34, 34); color: rgb(34, 34, 34); text-align: left;">{{ $price }}&nbsp;{{ Translation::trans("MAIL.REFEREE-COMPLETED.DISCOUNTFORNEXTORDER") }}</strong></span></div>

                                                                        <div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: arial; font-size: 14px; color: rgb(0, 0, 0); text-align: center;">&nbsp;</div>

                                                                        <div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; color: rgb(0, 0, 0); text-align: center;"><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size: 16px; white-space: pre-wrap;">{{ Translation::trans("MAIL.REFEREE-COMPLETED.WANTMORE") }}</span></span></div>

                                                                        <div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; color: rgb(0, 0, 0); text-align: center;">&nbsp;</div>
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <table border="0" cellPadding="0" cellSpacing="0" class="module" data-role="module-button" data-type="button" role="module" style="table-layout:fixed" width="100%"><tbody><tr><td align="center" class="outer-td" style="padding:0px 0px 0px 0px"><table border="0" cellPadding="0" cellSpacing="0" class="button-css__deep-table___2OZyb wrapper-mobile" style="text-align:center"><tbody><tr><td align="center" bgcolor="#ADC8B1" class="inner-td" style="border-radius:6px;font-size:16px;text-align:center;background-color:inherit"><a style="background-color:#ADC8B1;border:1px solid #333333;border-color:#ADC8B1;border-radius:6px;border-width:1px;color:#ffffff;display:inline-block;font-family:arial,helvetica,sans-serif;font-size:16px;font-weight:bold;letter-spacing:0px;line-height:16px;padding:12px 18px 12px 18px;text-align:center;text-decoration:none" href="https://yamo.bio/?ref={{ $referralUrl->url }}" target="_blank">{{ Translation::trans("MAIL.REFEREE-COMPLETED.MYPERSONALLINK") }}</td></tr></tbody></table></td></tr></tbody></table>
                                                        <table class="module"
                                                               role="module"
                                                               data-type="spacer"
                                                               border="0"
                                                               cellpadding="0"
                                                               cellspacing="0"
                                                               width="100%"
                                                               style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:0px 0px 30px 0px;"
                                                                    role="module-content"
                                                                    bgcolor="">
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit;"
                                                                    height="100%"
                                                                    valign="top"
                                                                    bgcolor="">
                                                                    <div style="font-family: arial; font-style: normal; font-variant-caps: normal; font-size: 14px; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); text-align: center;"><span style="font-family: arial, helvetica, sans-serif; font-size: 14px;"><span style="font-size: 16px;"><span style="font-size: 16px; text-decoration: inherit; background-color: transparent;">{{ Translation::trans("MAIL.REFEREE-COMPLETED.BESTREGARDS") }}</span></span></span></div>

                                                                    <div style="font-family: arial; font-style: normal; font-variant-caps: normal; font-size: 16px; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-variant-ligatures: normal; outline: none; text-align: center;"><span style="font-family: arial, helvetica, sans-serif; font-size: 16px;">{{ Translation::trans("MAIL.REFEREE-COMPLETED.YOURTEAM") }}</span></div>

                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module"
                                                               role="module"
                                                               data-type="divider"
                                                               border="0"
                                                               cellpadding="0"
                                                               cellspacing="0"
                                                               width="100%"
                                                               style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:0px 0px 0px 0px;"
                                                                    role="module-content"
                                                                    height="100%"
                                                                    valign="top"
                                                                    bgcolor="">
                                                                    <table border="0"
                                                                           cellpadding="0"
                                                                           cellspacing="0"
                                                                           align="center"
                                                                           width="100%"
                                                                           height="1px"
                                                                           style="line-height:1px; font-size:1px;">
                                                                        <tr>
                                                                            <td
                                                                                    style="padding: 0px 0px 1px 0px;"
                                                                                    bgcolor="#000000"></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit;"
                                                                    height="100%"
                                                                    valign="top"
                                                                    bgcolor="">
                                                                    <div style="text-align: center;"><span style="caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-family: arial, helvetica, sans-serif; font-size: 12.000000953674316px; font-style: normal; font-variant-caps: normal; text-align: center;">{{ Translation::trans("MAIL.REFEREE-COMPLETED.FURTHERQUESTIONS") }}&nbsp;</span><a href="https://yamobaby.zendesk.com/hc/de-ch" style="font-family: arial, helvetica, sans-serif; font-style: normal; font-variant-caps: normal; font-size: 12.000000953674316px; caret-color: rgb(0, 0, 0); text-align: center;">{{ Translation::trans("MAIL.REFEREE-COMPLETED.FAQS") }}</a><span style="caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); font-family: arial, helvetica, sans-serif; font-size: 12.000000953674316px; font-style: normal; font-variant-caps: normal; text-align: center;">&nbsp;{{ Translation::trans("MAIL.REFEREE-COMPLETED.ORDIRECTCONTACT") }}</span></div>

                                                                    <div>
                                                                        <div style="text-align: center;">&nbsp;</div>
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:020px 0px 18px 0px;line-height:22px;text-align:inherit;"
                                                                    height="100%"
                                                                    valign="top"
                                                                    bgcolor="">
                                                                    <div style="text-align: center;"><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; font-family: arial; font-size: inherit; color: rgb(0, 0, 0); text-align: center; text-decoration: inherit;">{{ Translation::trans("MAIL.REFEREE-COMPLETED.SOCIALMEDIAFUN") }}</span><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: Arial; font-size: 16px; color: rgb(0, 0, 0); text-align: center;">&nbsp;</span></div>
                                                                </td>
                                                            </tr>
                                                        </table>

                                                        <table class="module" role="module" data-type="social" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                            <tbody>
                                                            <tr>
                                                                <td valign="top" style="padding:0px 0px 0px 0px;font-size:6px;line-height:10px;">
                                                                    <table align="center">
                                                                        <tbody>
                                                                        <tr>
                                                                            <td style="padding: 0px 5px;">
                                                                                <a role="social-icon-link"  href="https://www.facebook.com/yamobaby" target="_blank" alt="Facebook"
                                                                                   data-nolink="false"
                                                                                   title="Facebook "
                                                                                   style="-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;display:inline-block;background-color:#3B579D;">
                                                                                    <img role="social-icon" alt="Facebook" title="Facebook "
                                                                                         height="30"
                                                                                         width="30"
                                                                                         style="height: 30px; width: 30px"
                                                                                         src="https://marketing-image-production.s3.amazonaws.com/social/white/facebook.png" />
                                                                                </a>
                                                                            </td>
                                                                            <td style="padding: 0px 5px;">
                                                                                <a role="social-icon-link"  href="https://www.twitter.com/yamobaby" target="_blank" alt="Twitter"
                                                                                   data-nolink="false"
                                                                                   title="Twitter "
                                                                                   style="-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;display:inline-block;background-color:#7AC4F7;">
                                                                                    <img role="social-icon" alt="Twitter" title="Twitter "
                                                                                         height="30"
                                                                                         width="30"
                                                                                         style="height: 30px; width: 30px"
                                                                                         src="https://marketing-image-production.s3.amazonaws.com/social/white/twitter.png" />
                                                                                </a>
                                                                            </td>
                                                                            <td style="padding: 0px 5px;">
                                                                                <a role="social-icon-link"  href="https://www.instagram.com/yamobaby" target="_blank" alt="Instagram"
                                                                                   data-nolink="false"
                                                                                   title="Instagram "
                                                                                   style="-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;display:inline-block;background-color:#7F4B30;">
                                                                                    <img role="social-icon" alt="Instagram" title="Instagram "
                                                                                         height="30"
                                                                                         width="30"
                                                                                         style="height: 30px; width: 30px"
                                                                                         src="https://marketing-image-production.s3.amazonaws.com/social/white/instagram.png" />
                                                                                </a>
                                                                            </td>



                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>

                                                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                                            <tr>
                                                                <td style="padding:18px 0px 18px 0px;line-height:22px;text-align:inherit;"
                                                                    height="100%"
                                                                    valign="top"
                                                                    bgcolor="">
                                                                    <div style="text-align: center;">
                                                                        <div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; font-family: arial; font-size: 14px; color: rgb(0, 0, 0); text-align: center;">
                                                                            <div style="font-family: arial; font-style: normal; font-variant-caps: normal; font-size: 14px; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); text-align: center; font-variant-ligatures: normal;"><span class="ac-designer-copy" style="font-family: arial; font-style: normal; font-variant-caps: normal; font-size: 11px; border-style: none; font-variant-ligatures: normal; outline: none; text-decoration-line: inherit; word-spacing: 0.015625px; overflow-wrap: break-word !important;">{{ Translation::trans("MAIL.REFEREE-COMPLETED.COPYRIGHT") }}<br class="ac-designer-copy" style="line-height: inherit;" />
                                                                            <a class="ac-designer-copy" data-ac-default-color="1" href="https://www.yamo.bio/impressum" style="font-size: 11px; border-style: none; color: rgb(22, 100, 35); text-decoration-line: underline; overflow-wrap: break-word !important;"><span class="ac-designer-copy" style="font-size: 11px; border-style: none; outline: none; color: inherit; text-decoration: inherit; overflow-wrap: break-word !important;">{{ Translation::trans("MAIL.REFEREE-COMPLETED.IMPRESSUM") }}</span></a><br class="ac-designer-copy" style="line-height: inherit;" />
                                                        <span class="ac-designer-copy" style="font-family: arial; font-style: normal; font-size: 11px; border-style: none; outline: none; text-decoration: inherit; overflow-wrap: break-word !important;">{{ Translation::trans("MAIL.REFEREE-COMPLETED.YAMONAME") }}</span></span><br class="ac-designer-copy" style="font-size: 11px; line-height: inherit;" />
                                                                                <span class="ac-designer-copy" style="font-family: arial; font-style: normal; font-variant-caps: normal; font-size: 11px; border-style: none; font-variant-ligatures: normal; outline: none; color: rgb(93, 93, 93); text-decoration-line: inherit; word-spacing: 0.015625px; overflow-wrap: break-word !important;"><span class="ac-designer-copy" style="font-family: arial; font-size: 11px; border-style: none; outline: none; text-decoration: inherit; overflow-wrap: break-word !important;"><span class="ac-designer-copy" style="font-family: arial; font-size: 11px; border-style: none; outline: none; color: rgb(0, 0, 0); text-decoration: inherit; overflow-wrap: break-word !important;">{{ Translation::trans("MAIL.REFEREE-COMPLETED.YAMOADDRESS") }}</span><span class="ac-designer-copy" style="font-family: arial; font-size: 11px; border-style: none; outline: none; text-decoration: inherit; overflow-wrap: break-word !important;">&nbsp;</span></span><br class="ac-designer-copy" style="line-height: inherit;" />
<span class="ac-designer-copy" style="font-family: arial; font-size: 11px; border-style: none; outline: none; text-decoration: inherit; overflow-wrap: break-word !important;"><a class="ac-designer-copy" data-ac-default-color="1" href="mailto:support@yamo.bio" style="font-family: arial; font-size: 11px; border-style: none; color: rgb(22, 100, 35); text-decoration-line: underline; overflow-wrap: break-word !important;">support@yamo.bio</a></span></span></div>

                                                                            <div style="font-family: arial; font-style: normal; font-variant-caps: normal; font-size: 14px; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); text-align: center; font-variant-ligatures: normal;"><span class="ac-designer-copy" style="font-family: arial; font-style: normal; font-variant-caps: normal; font-size: 14px; border-style: none; font-variant-ligatures: normal; color: inherit; text-decoration: inherit; word-spacing: 0.015625px; outline: none; overflow-wrap: break-word !important;"><a class="ac-designer-copy" data-ac-default-color="1" href="http://www.yamo.bio" style="background-color: rgb(255, 255, 255); font-family: arial; font-style: normal; font-variant-caps: normal; font-size: 12px; border-style: none; color: rgb(22, 100, 35); font-variant-ligatures: normal; text-decoration-line: underline; overflow-wrap: break-word !important;">www.yamo.bio</a></span><span style="font-size: 10px;"><span style="font-family: arial; font-style: normal; font-variant-caps: normal; font-size: 10px; font-variant-ligatures: normal; color: rgb(93, 93, 93); word-spacing: 0.015625px;">&nbsp;</span></span></div>

                                                                            <div style="font-family: arial; font-style: normal; font-variant-caps: normal; font-size: 14px; caret-color: rgb(0, 0, 0); color: rgb(0, 0, 0); text-align: center; font-variant-ligatures: normal;">
                                                                                <div style="font-family: arial; font-style: normal; font-variant-caps: normal; font-size: 14px; font-variant-ligatures: normal;"><span style="font-family: arial; font-style: normal; font-variant-caps: normal; font-size: 11px; font-variant-ligatures: normal; word-spacing: 0.015625px;">{{ Translation::trans("MAIL.REFEREE-COMPLETED.YAMOLAW") }}</span></div>

                                                                                <div style="font-family: arial; font-style: normal; font-variant-caps: normal; font-size: 14px; font-variant-ligatures: normal;"><span style="font-family: arial; font-style: normal; font-variant-caps: normal; font-size: 11px; font-variant-ligatures: normal; word-spacing: 0.015625px;">{{ Translation::trans("MAIL.REFEREE-COMPLETED.YAMOLEADERS") }}</span></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <!--[if mso]>
                                            </td></tr></table>
                                            </center>
                                            <![endif]-->
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</center>
</body>
</html>