<?php use \App\Models\Translation; ?>
<div style="padding:10px 0px 14px 0px;background-color:#c1c1c1" bgcolor="#c1c1c1">
    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
        <tr>
            <td style="padding:6px 10px 6px 20px;line-height:22px;text-align:inherit;"
                height="100%"
                valign="top"
                width="49%"
                bgcolor="">
                <div><span style="color:#FFFFFF;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">Rechnungsnummer</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">:&nbsp;</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;">{{ $order_id }}</span></span></span></div>
            </td>
            <td style="padding:6px 10px 6px 20px;line-height:22px;text-align:inherit;"
            height="100%"
            valign="top"
            bgcolor="">
                <div><span style="color:#FFFFFF;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">Lieferdatum</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">:&nbsp;</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;">{{ $delivery_date }}</span></span></span></div>
            </td>
        </tr>
    </table>

    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
        <tr>
            <td style="padding:6px 10px 6px 20px;line-height:22px;text-align:inherit;"
                height="100%"
                valign="top"
                width="49%"
                bgcolor="">
                <div><span style="color:#FFFFFF;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">Kundennummer</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">:&nbsp;</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;">{{ $customer_number }}</span></span></span></div>
            </td>
            <td style="padding:6px 10px 6px 20px;line-height:22px;text-align:inherit;"
                height="100%"
                valign="top"
                bgcolor="">
                <div><span style="color:#FFFFFF;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">Zahlungsart</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">:&nbsp;</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;">{{ $payment_method }}</span></span></span></div>
            </td>
        </tr>
    </table>

    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
        <tr>
            <td style="padding:6px 10px 6px 20px;line-height:22px;text-align:inherit;"
                height="100%"
                valign="top"
                bgcolor="">
                <div><span style="color:#FFFFFF;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">Rechnungsdatum</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">:&nbsp;</span><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;">{{ $date_created }}</span></span></span></div>
            </td>
        </tr>
    </table>

</div>
<div style="width: 100%; margin-left:auto; margin-right: auto;">
@foreach($order_items as $item)
    @if($item->item_type == 'line_item')
        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
            <tr>
                <td style="padding:20px 0px 20px 10px;background-color:#f7f7f7;" bgcolor="#f7f7f7">

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-0 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="font-size:6px;line-height:10px;padding:0px 0px 0px 0px;" valign="top" align="center">
                                            <img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;" src="{{ $item->product->image }}" alt="" width="182px">
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-1 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                @if(!empty($item->product))
                                    <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                        <tr>
                                            <td style="padding:6px 10px 6px 20px;line-height:22px;text-align:inherit;"
                                                height="100%"
                                                valign="top"
                                                bgcolor="">
                                                <div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold;">{{ $item->qty }}x {{ Translation::trans("PRODUCTS.".$item->product->name_key) }}{{ in_array($item->item_price,['0','0.00', null, '']) ?  ' - '.Translation::trans('LOYALTY.FREE') : ''}}:</span></span></div>
                                                <ul style="padding:0; margin:0;">
                                                    @foreach ($item->product->ingredients as $ingredient)
                                                        <li><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal;">{{ $ingredient }}</span></li>
                                                    @endforeach
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                @endif

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                </td>
            </tr>
        </table>
    @endif
@endforeach

<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
    <tr>
        <td style="padding:0px 0px 0px 0px;" bgcolor="">

            <!--[if mso]>
            <table width="49%" align="left"><tr><td>
            <![endif]-->
            <table style="padding: 0px 0px 0px 0px;"
                   align="left"
                   valign="top"
                   height="100%"
                   class="column column-0 of-2 empty">
                <tr>
                    <td class="columns--column-content">

                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                            <tr>
                                <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                    height="100%"
                                    valign="top"
                                    bgcolor="">
                                    <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.ORDER-DETAILS.SUBTOTAL") }} ({{ $box_tax_percentage }}%):</span></span></div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
            <!--[if mso]>
            </td></tr></table>
            </center>
            <![endif]-->

            <!--[if mso]>
            <table width="49%" align="left"><tr><td>
            <![endif]-->
            <table style="padding: 0px 0px 0px 0px;"
                   align="left"
                   valign="top"
                   height="100%"
                   class="column column-1 of-2 empty">
                <tr>
                    <td class="columns--column-content">

                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                            <tr>
                                <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                    height="100%"
                                    valign="top"
                                    bgcolor="">
                                    <div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">{{ $first_subtotal }}</span></span></div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
            <!--[if mso]>
            </td></tr></table>
            </center>
            <![endif]-->

        </td>
    </tr>
</table>

    @if(isset($subscription_reduction) && !empty($subscription_reduction))


        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
            <tr>
                <td style="padding:0px 0px 0px 0px;" bgcolor="">

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-0 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.ORDER-DETAILS.SUBSCRIPTIONREDUCTION") }}:</span></span></div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-1 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div>
                                                <span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">- {{ $subscription_reduction }}&nbsp;</span></span>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                </td>
            </tr>
        </table>
    @endif

@if($discount_cost != '0.00')
    <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
        <tr>
            <td style="padding:0px 0px 0px 0px;" bgcolor="">

                <!--[if mso]>
                <table width="49%" align="left"><tr><td>
                <![endif]-->
                <table style="padding: 0px 0px 0px 0px;"
                       align="left"
                       valign="top"
                       height="100%"
                       class="column column-0 of-2 empty">
                    <tr>
                        <td class="columns--column-content">

                            <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                <tr>
                                    <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                        height="100%"
                                        valign="top"
                                        bgcolor="">
                                        <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.ORDER-DETAILS.USEDCOUPON") }} {{ $coupon_code }}:</span></span></div>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
                <!--[if mso]>
                </td></tr></table>
                </center>
                <![endif]-->

                <!--[if mso]>
                <table width="49%" align="left"><tr><td>
                <![endif]-->
                <table style="padding: 0px 0px 0px 0px;"
                       align="left"
                       valign="top"
                       height="100%"
                       class="column column-1 of-2 empty">
                    <tr>
                        <td class="columns--column-content">

                            <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                <tr>
                                    <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                        height="100%"
                                        valign="top"
                                        bgcolor="">
                                        <div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);"></span><span class="m_3202122474823768701m_3719169600630861421m_91549262569909069woocommerce-Price-amount m_3202122474823768701m_3719169600630861421m_91549262569909069amount" style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">- {{ $discount }}</span></span></div>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
                <!--[if mso]>
                </td></tr></table>
                </center>
                <![endif]-->

            </td>
        </tr>
    </table>
@endif

    @if($loyalty_cost != '0.00')
        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
            <tr>
                <td style="padding:0px 0px 0px 0px;" bgcolor="">

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-0 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">{{ Translation::trans("YAMO-CLUB") }}:</span></span></div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-1 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);"></span><span class="m_3202122474823768701m_3719169600630861421m_91549262569909069woocommerce-Price-amount m_3202122474823768701m_3719169600630861421m_91549262569909069amount" style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">- {{ $loyalty }}</span></span></div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                </td>
            </tr>
        </table>
    @endif




    @if($referral_cost != "0.00")


        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
            <tr>
                <td style="padding:0px 0px 0px 0px;" bgcolor="">

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-0 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.ORDER-DETAILS.REFERRALDISCOUNT") }}:</span></span></div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-1 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div>
                                                <span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">- {{ $referral }}&nbsp;</span></span>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                </td>
            </tr>
        </table>



    @endif


    @if($wallet_cost != "0.00")


        <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
            <tr>
                <td style="padding:0px 0px 0px 0px;" bgcolor="">

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-0 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.ORDER-DETAILS.WALLETDISCOUNT") }}:</span></span></div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                    <!--[if mso]>
                    <table width="49%" align="left"><tr><td>
                    <![endif]-->
                    <table style="padding: 0px 0px 0px 0px;"
                           align="left"
                           valign="top"
                           height="100%"
                           class="column column-1 of-2 empty">
                        <tr>
                            <td class="columns--column-content">

                                <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                    <tr>
                                        <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                            height="100%"
                                            valign="top"
                                            bgcolor="">
                                            <div>
                                                <span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">- {{ $wallet }}&nbsp;</span></span>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                    </center>
                    <![endif]-->

                </td>
            </tr>
        </table>

    @endif


<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
    <tr>
        <td style="padding:0px 0px 0px 0px;" bgcolor="">

            <!--[if mso]>
            <table width="49%" align="left"><tr><td>
            <![endif]-->
            <table style="padding: 0px 0px 0px 0px;"
                   align="left"
                   valign="top"
                   height="100%"
                   class="column column-0 of-2 empty">
                <tr>
                    <td class="columns--column-content">

                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                            <tr>
                                <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                    height="100%"
                                    valign="top"
                                    bgcolor="">
                                    <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.ORDER-DETAILS.COOLSEND") }}:</span></span></div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
            <!--[if mso]>
            </td></tr></table>
            </center>
            <![endif]-->

            <!--[if mso]>
            <table width="49%" align="left"><tr><td>
            <![endif]-->
            <table style="padding: 0px 0px 0px 0px;"
                   align="left"
                   valign="top"
                   height="100%"
                   class="column column-1 of-2 empty">
                <tr>
                    <td class="columns--column-content">

                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                            <tr>
                                <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                    height="100%"
                                    valign="top"
                                    bgcolor="">
                                    <div>
                                        @if($shipping_cost != '0.00')
                                            <span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">{{ $shipping }}&nbsp;</span></span>
                                        @else
                                            <span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.ORDER-DETAILS.FREE") }}&nbsp;</span></span>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
            <!--[if mso]>
            </td></tr></table>
            </center>
            <![endif]-->

        </td>
    </tr>
</table>




    <table  border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
    <tr>
        <td bgcolor="">

            <!--[if mso]>
            <table width="49%" align="left"><tr><td>
            <![endif]-->
            <table style="padding: 0px 0px 0px 0px;"
                   align="left"
                   valign="top"
                   height="100%"
                   class="column column-0 of-2 empty">
                <tr>
                    <td class="columns--column-content">

                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                            <tr>
                                <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                    height="100%"
                                    valign="top"
                                    bgcolor="">
                                    <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.ORDER-DETAILS.TOTAL") }}:</span></span></div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
            <!--[if mso]>
            </td></tr></table>
            </center>
            <![endif]-->

            <!--[if mso]>
            <table width="49%" align="left"><tr><td>
            <![endif]-->
            <table style="padding: 0px 0px 0px 0px;"
                   align="left"
                   valign="top"
                   height="100%"
                   class="column column-1 of-2 empty">
                <tr>
                    <td class="columns--column-content">

                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                            <tr>
                                <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                    height="100%"
                                    valign="top"
                                    bgcolor="">
                                    <div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);"><b>{{ $total }}</b></span><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">&nbsp;</span></span></div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
            <!--[if mso]>
            </td></tr></table>
            </center>
            <![endif]-->

        </td>
    </tr>
</table>




    <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
        <tr>
            <td bgcolor="">

                <!--[if mso]>
                <table width="49%" align="left"><tr><td>
                <![endif]-->
                <table style="padding: 0px 0px 0px 0px;"
                       align="left"
                       valign="top"
                       height="100%"
                       class="column column-0 of-2 empty">
                    <tr>
                        <td class="columns--column-content">

                            <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                <tr>
                                    <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                        height="100%"
                                        valign="top"
                                        bgcolor="">
                                        <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: bold; background-color: rgb(253, 253, 253);">{{ Translation::trans("MAIL.ORDER-DETAILS.TAX") }} ({{ $box_tax_percentage }}%):</span></span></div>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
                <!--[if mso]>
                </td></tr></table>
                </center>
                <![endif]-->

                <!--[if mso]>
                <table width="49%" align="left"><tr><td>
                <![endif]-->
                <table style="padding: 0px 0px 0px 0px;"
                       align="left"
                       valign="top"
                       height="100%"
                       class="column column-1 of-2 empty">
                    <tr>
                        <td class="columns--column-content">

                            <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                                <tr>
                                    <td style="padding:5px 30px 5px 20px;line-height:22px;text-align:inherit;"
                                        height="100%"
                                        valign="top"
                                        bgcolor="">
                                        <div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">{{ $total_tax }}</span><span style="color: rgb(51, 51, 51); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; background-color: rgb(253, 253, 253);">&nbsp;</span></span></div>
                                    </td>
                                </tr>
                            </table>

                        </td>
                    </tr>
                </table>
                <!--[if mso]>
                </td></tr></table>
                </center>
                <![endif]-->

            </td>
        </tr>
    </table>





    <table class="module"
           role="module"
           data-type="divider"
           border="0"
           cellpadding="0"
           cellspacing="0"
           width="100%"
           style="table-layout: fixed;">
        <tr>
            <td
                    role="module-content"
                    height="100%"
                    valign="top"
                    bgcolor="">
                <table border="0"
                       cellpadding="0"
                       cellspacing="0"
                       align="center"
                       width="100%"
                       height="1px"
                       style="line-height:1px; font-size:1px;">
                    <tr>
                        <td
                                style="padding: 0px 0px 1px 0px;"
                                bgcolor="#c5c5c5"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>




    <table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
    <tr>
        <td style="padding:0px 0px 0px 20px;background-color:#f1f1f1;" bgcolor="#f1f1f1">

            <!--[if mso]>
            <table width="99%" align="left"><tr><td>
            <![endif]-->
            <table style="padding: 0px 0px 0px 0px;"
                   align="left"
                   valign="top"
                   height="100%"
                   class="column column-0 of-1 empty">
                <tr>
                    <td class="columns--column-content">

                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                            <tr>
                                <td style="padding:10px 0px 0px 0px;line-height:22px;text-align:inherit;"
                                    height="100%"
                                    valign="top"
                                    bgcolor="">
                                    <h3><span style="color:#338743;">{{ Translation::trans("MAIL.ORDER-DETAILS.CUSTOMERDATA") }}</span></h3>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
            <!--[if mso]>
            </td></tr></table>
            </center>
            <![endif]-->

        </td>
    </tr>
</table>












<table border="0" cellpadding="0" cellspacing="0" align="center" width="100%" role="module" data-type="columns">
    <tr>
        <td style="padding:0px 20px 15px 20px;background-color:#F1F1F1;" bgcolor="#F1F1F1">

            <!--[if mso]>
            <table width="49%" align="left"><tr><td>
            <![endif]-->
            <table style="padding: 0px 0px 0px 0px;"
                   align="left"
                   valign="top"
                   height="100%"
                   class="column column-0 of-2 empty">
                <tr>
                    <td class="columns--column-content">

                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                            <tr>
                                <td style="padding:10px 0px 18px 0px;line-height:22px;text-align:inherit;"
                                    height="100%"
                                    valign="top"
                                    bgcolor="">
                                    <div><span style="font-family:verdana,geneva,sans-serif;"><strong><span style="color:#338743;">{{ Translation::trans("MAIL.ORDER-DETAILS.SHIPADDRESS") }}</span></strong></span></div>

                                    @if(!empty($shipping_company))
                                        <div><span style="font-family:verdana,geneva,sans-serif;">{{ $shipping_company }}</span></div>
                                    @endif

                                    <div><span style="font-family:verdana,geneva,sans-serif;">{{ $shipping_name }}</span></div>

                                    <div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; text-align: inherit;">{{ $shipping_address }}</span></span></div>

                                    <div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; text-align: inherit;">{{ $shipping_postnumber }} {{ $shipping_city }}</span></span></div>

                                    <div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; text-align: inherit;">{{ $shipping_country }}</span></span></div>

                                    <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(116, 116, 116); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; background-color: rgb(241, 241, 241);">{{ $shipping_email }}</span></span></div>

                                    <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(116, 116, 116); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; background-color: rgb(241, 241, 241);">{{ $shipping_phone }}</span></span></div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
            <!--[if mso]>
            </td></tr></table>
            </center>
            <![endif]-->

            <!--[if mso]>
            <table width="49%" align="left"><tr><td>
            <![endif]-->
            <table style="padding: 0px 0px 0px 0px;"
                   align="left"
                   valign="top"
                   height="100%"
                   class="column column-1 of-2 empty">
                <tr>
                    <td class="columns--column-content">

                        <table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
                            <tr>
                                <td style="padding:10px 0px 18px 0px;line-height:22px;text-align:inherit;"
                                    height="100%"
                                    valign="top"
                                    bgcolor="">
                                    <div><span style="font-family:verdana,geneva,sans-serif;"><font color="#338743"><span style="font-weight: 600;">{{ Translation::trans("MAIL.ORDER-DETAILS.BILLADDRESS") }}</span></font></span></div>
                                    @if(!empty($billing_company))
                                        <div><span style="font-family:verdana,geneva,sans-serif;">{{ $billing_company }}</span></div>
                                    @endif

                                    <div><span style="font-family:verdana,geneva,sans-serif;">{{ $billing_name }}</span></div>

                                    <div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; text-align: inherit;"> {{ $billing_address }}</span></span></div>

                                    <div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; text-align: inherit;">{{ $billing_postnumber }} {{ $billing_city }}</span></span></div>

                                    <div><span style="font-family:verdana,geneva,sans-serif;"><span style="font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; text-align: inherit;">{{ $billing_country }}</span></span></div>

                                    <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(116, 116, 116); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; background-color: rgb(241, 241, 241);">{{ $billing_email }}</span></span></div>

                                    <div><span style="font-family:verdana,geneva,sans-serif;"><span style="color: rgb(116, 116, 116); font-size: 14px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; background-color: rgb(241, 241, 241);">{{ $billing_phone }}</span></span></div>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- THEEND ((COMPLETE ORDER DETAILS INCL. PACKSHOTS)) -->
</div>