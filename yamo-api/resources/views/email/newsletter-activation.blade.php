<?php use \App\Models\Translation; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" /><!--[if !mso]><!-->
<meta http-equiv="X-UA-Compatible" content="IE=Edge" /><!--<![endif]-->
<!--[if (gte mso 9)|(IE)]>
<xml>
<o:OfficeDocumentSettings>
<o:AllowPNG/>
<o:PixelsPerInch>96</o:PixelsPerInch>
</o:OfficeDocumentSettings>
</xml>
<![endif]-->
<!--[if (gte mso 9)|(IE)]>
<style type="text/css">
body {width: 600px;margin: 0 auto;}
table {border-collapse: collapse;}
table, td {mso-table-lspace: 0pt;mso-table-rspace: 0pt;}
img {-ms-interpolation-mode: bicubic;}
</style>
<![endif]-->

<style type="text/css">
body, p, div {
font-family: verdana,geneva,sans-serif;
font-size: 14px;
}
body {
color: #747474;
}
body a {
color: #7baa84;
text-decoration: none;
}
p { margin: 0; padding: 0; }
table.wrapper {
width:100% !important;
table-layout: fixed;
-webkit-font-smoothing: antialiased;
-webkit-text-size-adjust: 100%;
-moz-text-size-adjust: 100%;
-ms-text-size-adjust: 100%;
}
img.max-width {
max-width: 100% !important;
}
.column.of-2 {
width: 50%;
}
.column.of-3 {
width: 33.333%;
}
.column.of-4 {
width: 25%;
}
@media screen and (max-width:480px) {
.preheader .rightColumnContent,
.footer .rightColumnContent {
text-align: left !important;
}
.preheader .rightColumnContent div,
.preheader .rightColumnContent span,
.footer .rightColumnContent div,
.footer .rightColumnContent span {
text-align: left !important;
}
.preheader .rightColumnContent,
.preheader .leftColumnContent {
font-size: 80% !important;
padding: 5px 0;
}
table.wrapper-mobile {
width: 100% !important;
table-layout: fixed;
}
img.max-width {
height: auto !important;
max-width: 480px !important;
}
a.bulletproof-button {
display: block !important;
width: auto !important;
font-size: 80%;
padding-left: 0 !important;
padding-right: 0 !important;
}
.columns {
width: 100% !important;
}
.column {
display: block !important;
width: 100% !important;
padding-left: 0 !important;
padding-right: 0 !important;
}
}
</style>
<!--user entered Head Start-->

<!--End Head user entered-->
</head>
<body>
<center class="wrapper" data-link-color="#7baa84" data-body-style="font-size: 14px; font-family: verdana,geneva,sans-serif; color: #747474; background-color: #ffffff;">
<div class="webkit">
<table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#ffffff">
<tr>
<td valign="top" bgcolor="#ffffff" width="100%">
<table width="100%" role="content-container" class="outer" align="center" cellpadding="0" cellspacing="0" border="0">
<tr>
<td width="100%">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
<td>
<!--[if mso]>
<center>
<table><tr><td width="600">
<![endif]-->
<table width="100%" cellpadding="0" cellspacing="0" border="0" style="width: 100%; max-width:600px;" align="center">
<tr>
<td role="modules-container" style="padding: 0px 0px 0px 0px; color: #747474; text-align: left;" bgcolor="#ffffff" width="100%" align="left">

<table class="module preheader preheader-hide" role="module" data-type="preheader" border="0" cellpadding="0" cellspacing="0" width="100%"
style="display: none !important; mso-hide: all; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
<tr>
<td role="module-content">
<p></p>
</td>
</tr>
</table>

<table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="font-size:6px;line-height:10px;padding:10px 0px 20px 0px;" valign="top" align="center">
<img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;" src="https://marketing-image-production.s3.amazonaws.com/uploads/6212c4140074c6a8953f5ef8d8ae34052eba6b87f406d29f74d291f7ba604ba0161d4b04d88bca08143c69acd991ee5eb99156450cea007e5b82e83f28a18229.jpg" alt="" width="80" data-proportionally-constrained="true" height="86">
</td>
</tr>
</table>

<table class="wrapper" role="module" data-type="image" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="font-size:6px;line-height:10px;padding:0px 0px 0px 0px;" valign="top" align="center">
<img class="max-width" border="0" style="display:block;color:#000000;text-decoration:none;font-family:Helvetica, arial, sans-serif;font-size:16px;max-width:100% !important;width:100%;height:auto !important;" src="https://marketing-image-production.s3.amazonaws.com/uploads/6b7b825b1dc04a42b7788919d15590773a8566ba9d84e03256d675c0d703d0ada6dde904957127f8f110d0d484c3c9b04d8b9ef1a3398b801df992a5590fd59e.jpg" alt="" width="600">
</td>
</tr>
</table>

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="background-color:#7baa84;padding:12px 0px 12px 0px;line-height:28px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="#7baa84">
<h2 style="text-align: center;"><span style="color:#FFFFFF;">{{ Translation::trans("MAIL.NEWSLETTER-ACTIVATION.YAMONEWSLETTERACTIVATION") }}</span></h2>

</td>
</tr>
</table>

<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:18px 0px 6px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div><span style="font-size:14px;"><span style="font-family:verdana,geneva,sans-serif;"><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; white-space: pre-wrap;">{{ Translation::trans("MAIL.NEWSLETTER-ACTIVATION.CONFIRMREGISTER") }} </span></span></span></div>

<div>&nbsp;</div>

</td>
</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" class="module" data-role="module-button" data-type="button" role="module" style="table-layout:fixed;" width="100%"><tbody><tr><td align="center" bgcolor="#ffffff" class="outer-td" style="padding:20px 0px 30px 0px;background-color:#ffffff;"><table border="0" cellpadding="0" cellspacing="0" class="button-css__deep-table___2OZyb wrapper-mobile" style="text-align:center;"><tbody><tr><td align="center" bgcolor="#7baa84" class="inner-td" style="border-radius:6px;font-size:16px;text-align:center;background-color:inherit;"><a style="background-color:#7baa84;border:1px solid #333333;border-color:#ffffff;border-radius:4px;border-width:0px;color:#ffffff;display:inline-block;font-family:verdana,geneva,sans-serif;font-size:14px;font-weight:normal;letter-spacing:0px;line-height:18px;padding:14px 30px 14px 30px;text-align:center;text-decoration:none;" href="{{ $app_url . 'newsletter/anmeldung/' . $confirmation_code }}" target="_blank">{{ Translation::trans("MAIL.NEWSLETTER-ACTIVATION.CONFIRMHERE") }}</a></td></tr></tbody></table></td></tr></tbody></table>
<table class="module" role="module" data-type="text" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tr>
<td style="padding:30px 0px 6px 20px;line-height:22px;text-align:inherit;"
height="100%"
valign="top"
bgcolor="">
<div style="text-align: center;">
<div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; font-family: verdana, geneva, sans-serif; font-size: 14px; color: rgb(116, 116, 116); background-color: rgb(255, 255, 255); text-align: center;"><span style="font-size: 14px;"><span style="font-family: verdana, geneva, sans-serif; font-size: 14px;"><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; font-size: 14px; white-space: pre-wrap;">{{ Translation::trans("MAIL.NEWSLETTER-ACTIVATION.BESTREGARDS") }},</span></span></span></div>

<div style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 300; font-family: verdana, geneva, sans-serif; font-size: 14px; color: rgb(116, 116, 116); background-color: rgb(255, 255, 255); text-align: center;"><span style="font-size: 14px;"><span style="font-family: verdana, geneva, sans-serif; font-size: 14px;"><span style="font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; font-size: 14px; white-space: pre-wrap;">{{ Translation::trans("MAIL.NEWSLETTER-ACTIVATION.YOURTEAM") }}</span></span></span></div>
</div>
</td>
</tr>
</table>

<table class="module"
role="module"
data-type="spacer"
border="0"
cellpadding="0"
cellspacing="0"
width="100%"
style="table-layout: fixed;">
<tr>
<td style="padding:0px 0px 30px 0px;"
role="module-content"
bgcolor="">
</td>
</tr>
</table>

<table class="module"
role="module"
data-type="divider"
border="0"
cellpadding="0"
cellspacing="0"
width="100%"
style="table-layout: fixed;">
<tr>
<td style="padding:10px 20px 0px 20px;"
role="module-content"
height="100%"
valign="top"
bgcolor="">
<table border="0"
cellpadding="0"
cellspacing="0"
align="center"
width="100%"
height="1px"
style="line-height:1px; font-size:1px;">
<tr>
<td
    style="padding: 0px 0px 1px 0px;"
    bgcolor="#c5c5c5"></td>
</tr>
</table>
</td>
</tr>
</table>

    <!-- include footer address section -->
    @include('email.footer-address')

<table class="module" role="module" data-type="social" align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="table-layout: fixed;">
<tbody>
<tr>
<td valign="top" style="padding:20px 0px 30px 0px;font-size:6px;line-height:10px;">
<table align="center">
<tbody>
<tr>
<td style="padding: 0px 5px;">
<a role="social-icon-link"  href="https://www.facebook.com/yamobaby/" target="_blank" alt="Facebook"
   data-nolink="false"
   title="Facebook "
   style="-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;display:inline-block;background-color:#3B579D;">
    <img role="social-icon" alt="Facebook" title="Facebook "
         height="30"
         width="30"
         style="height: 30px, width: 30px"
         src="https://marketing-image-production.s3.amazonaws.com/social/white/facebook.png" />
</a>
</td>
<td style="padding: 0px 5px;">
<a role="social-icon-link"  href="https://twitter.com/yamobaby?lang=en" target="_blank" alt="Twitter"
   data-nolink="false"
   title="Twitter "
   style="-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;display:inline-block;background-color:#7AC4F7;">
    <img role="social-icon" alt="Twitter" title="Twitter "
         height="30"
         width="30"
         style="height: 30px, width: 30px"
         src="https://marketing-image-production.s3.amazonaws.com/social/white/twitter.png" />
</a>
</td>
<td style="padding: 0px 5px;">
<a role="social-icon-link"  href="https://www.instagram.com/yamobaby/" target="_blank" alt="Instagram"
   data-nolink="false"
   title="Instagram "
   style="-webkit-border-radius:2px;-moz-border-radius:2px;border-radius:2px;display:inline-block;background-color:#7F4B30;">
    <img role="social-icon" alt="Instagram" title="Instagram "
         height="30"
         width="30"
         style="height: 30px, width: 30px"
         src="https://marketing-image-production.s3.amazonaws.com/social/white/instagram.png" />
</a>
</td>



</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>

</td>
</tr>
</table>
<!--[if mso]>
</td></tr></table>
</center>
<![endif]-->
</td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
</center>
</body>
</html>