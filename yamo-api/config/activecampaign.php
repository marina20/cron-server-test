<?php
return [
    'host'=>env('APP_ACTIVECAMPAIGN_HOST', ''),
    'token'=>env('APP_ACTIVECAMPAIGN_TOKEN', ''),
];
