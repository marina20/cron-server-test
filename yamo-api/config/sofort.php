<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Sofort API ID
    |--------------------------------------------------------------------------
    |
    | Custom made for integration with Sofort service
    |
    */
	'default' => env('SOFORT_DEFAULT_CONNECTION'),
	'connections' => [
        'default' => [
            'key' => env('SOFORT_DEFAULT_CONFIG_KEY', 'your-config-key'),
        ],
    ]
];