<?php
return [
    /*
    |--------------------------------------------------------------------------
    | MFG Invoice Config
    |--------------------------------------------------------------------------
    |
    | Custom made for integration with MFG invoice service
    |
    */
    'country' => [
        'DE'=> [
            'user'=>env('MFG_DE_USER', ''),
            'password'=>env('MFG_DE_PASSWORD', ''),
            'merchant'=>env('MFG_DE_MERCHANT_ID',''),
            'filial'=>env('MFG_DE_FILIAL_ID',''),
            'terminal'=>env('MFG_DE_TERMINAL_ID',''),
	        'server' => env('MFG_DE_SERVER', ''),
        ],
        'CH'=> [
            'user'=>env('MFG_CH_USER', ''),
            'password'=>env('MFG_CH_PASSWORD', ''),
            'merchant'=>env('MFG_CH_MERCHANT_ID',''),
            'filial'=>env('MFG_CH_FILIAL_ID',''),
            'terminal'=>env('MFG_CH_TERMINAL_ID',''),
            'server' => env('MFG_CH_SERVER', ''),
        ],
        'AT'=> [
            'user'=>env('MFG_AT_USER', ''),
            'password'=>env('MFG_AT_PASSWORD', ''),
            'merchant'=>env('MFG_AT_MERCHANT_ID',''),
            'filial'=>env('MFG_AT_FILIAL_ID',''),
            'terminal'=>env('MFG_AT_TERMINAL_ID',''),
	        'server' => env('MFG_AT_SERVER', ''),
        ]
    ]
];