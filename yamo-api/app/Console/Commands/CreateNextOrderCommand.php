<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/5/18
 * Time: 2:18 PM
 */
namespace App\Console\Commands;

use App\Models\Order;
use App\Models\Subscription;
use App\Services\CreateNextOrderService;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class CreateNextOrderCommand extends Command
{
    protected $create_next_order_service;
    public function __construct(CreateNextOrderService $create_next_order_service)
    {
        parent::__construct();
        $this->create_next_order_service = $create_next_order_service;
    }
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "create:next_orders";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Run daily to create next orders that should be scheduled";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            // Subscription with order that is cancelled needs to be cancelled also
            // This case is not supposed to happen with code
            $subscriptions = Subscription::query()
                ->select('subscriptions.*')
                ->join('orders','subscriptions.order_id','=','orders.id')
                ->where('subscriptions.status', '!=', Subscription::STATUS_CANCELLED)
                ->where('orders.status', '=', Order::STATUS_CANCELLED)
                ->whereRaw('orders.id not in (select order_id from order_refunds)')
                ->get();

            $arrayOfSubscriptionIds = [];

            foreach ($subscriptions as $s) {
                $arrayOfSubscriptionIds[] = $s->id;
                $s->status = Subscription::STATUS_CANCELLED;
                $s->save();
            }

            $lb = "\r\n\r\n";
            $today = Carbon::now()->toDateString();

            if (count($subscriptions)) {
                $msg_string = 'FIX: Cancelled subscription ' . $lb . implode(', ', $arrayOfSubscriptionIds);
                Mail::raw($msg_string, function ($msg) use ($today){
                    $msg->from('cron@yamo.ch');
                    if (app()->environment('production')) {
                        $msg->bcc('francesco@yamo.bio');
                    }
                    $msg->to(env('MAIL_CRON_INFO_ADDRESS'));
                    $msg->subject('List of cancelled subscription ' . $today);
                });
            }

            $orders = Order::query()
                ->select('orders.*')
                ->join('subscriptions','subscriptions.order_id','=','orders.id')
                ->whereDate('orders.delivery_date','<=',$today)
                ->where('subscriptions.status', '!=', Subscription::STATUS_CANCELLED)
                ->get();

            $msg_string = 'create:next_orders' . $lb;

            if ($orders->isEmpty()) {
                $this->info('Nothing to send.');
                return;
            }

            $this->info('Number of orders to be processed: ' . count($orders));
            $msg_string .= 'Number of orders to be processed: ' . count($orders) . $lb;

            $this->info('Processing order ids: ' . count($orders));
            $msg_string .= 'Processing order ids: ' . count($orders) . $lb;

            if (!$orders->isEmpty()) {
                foreach ($orders as $order) {
                    try {
                        $response = $this->create_next_order_service->next_order($order);

                        $this->info($order->id);
                        $msg_string .= $order->id . ', ';
                    } catch (\Throwable $e)
                    {
                        app('sentry')->captureException($e);
                        $this->info('order id: ' . $order->id . ' had an error');
                        $msg_string .= 'order id: ' . $order->id . ' had an error' . $lb;
                    }
                }
            }

            $msg_string = rtrim($msg_string, ', ');

            Mail::raw($msg_string, function ($msg) use ($today){
                $msg->from('cron@yamo.ch');
                $msg->to(env('MAIL_CRON_INFO_ADDRESS'));
                $msg->subject('Cron job Create Next Order for ' . $today);
            });
        }
        catch
        (\Throwable $e) {
            $this->error("An error occurred: " . $e->getMessage());
            app('sentry')->captureException($e);
        }
    }
}