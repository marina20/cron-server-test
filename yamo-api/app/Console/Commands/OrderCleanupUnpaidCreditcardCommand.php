<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/5/18
 * Time: 2:18 PM
 */
namespace App\Console\Commands;

use App\Models\Order;
use App\Models\ReferralUrl;
use Exception;
use Illuminate\Support\Facades\Mail;
use Illuminate\Console\Command;

class OrderCleanupUnpaidCreditcardCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "delete:unpaid_orders_creditcard";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Cancel all orders that have creditcard set as payment method and are not paid and are first initial orders";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $payment_methods = [
                Order::PAYMENT_METHOD_CREDITCARD,
                Order::PAYMENT_METHOD_PAYPAL,
                Order::PAYMENT_METHOD_SOFORT
            ];

            $orders = Order::query()
                ->select('orders.*')
                ->leftJoin('subscriptions','subscriptions.order_id','=','orders.id')
                ->whereIn('orders.payment_method_id',$payment_methods)
                ->whereNull('orders.completed_date')
                ->whereNull('orders.paid_date')
                ->whereNull('orders.parent_id')
                ->whereRaw("orders.status in ('".Order::STATUS_PENDING."')")
                ->get();

            // cancel subscription for this order first

            if($orders->isEmpty())
            {
                $this->info('Nothing to cancel.');
                return;
            }

            $lb = "\r\n\r\n";
            $msg_string = 'command delete:unpaid_orders_creditcard'.$lb;

            $this->info('Number of orders to be processed: '.count($orders));

            $msg_string .= 'Number of orders to be processed: '.count($orders).$lb;

            if(!$orders->isEmpty())
            {
                foreach($orders as $order)
                {
                    try {
                        if (!empty($order->subscription->id)) {
                            $subscription = $order->subscription;
                            $subscription->status = Order::STATUS_CANCELLED;
                            $order = ReferralUrl::revertWallet($order);
                            $subscription->save();
                            $this->info('Subscription with id : ' . $order->subscription->id . ' is cancelled.');

                            $msg_string .= 'Subscription with id : ' . $order->subscription->id . ' is cancelled.' . $lb;
                        }
                        $order->status = Order::STATUS_CANCELLED;
                        $order = ReferralUrl::revertWallet($order);
                        $order->save();
                        $this->info('Order with id:  ' . $order->id . ' is cancelled.');

                        $msg_string .= 'Order with id:  ' . $order->id . ' is cancelled.' . $lb;
                    } catch (\Throwable $e)
                    {
                        app('sentry')->captureException($e);
                        $this->info('order id: ' . $order->id . ' had an error');
                        $msg_string .= 'order id: ' . $order->id . ' had an error' . $lb;
                    }
                }
            }
            $this->info("All orders have been processed.");

            $msg_string .= 'All orders have been processed.'.$lb;
            Mail::raw($msg_string, function($msg) {
                $msg->to(env('MAIL_CRON_INFO_ADDRESS'));
                $msg->subject('Cron job command report');
            });

        } catch (\Throwable $e) {
            $this->error("An error occurred:" . $e->getMessage());
        }
    }
}