<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/5/18
 * Time: 2:18 PM
 */
namespace App\Console\Commands;

use App\Models\AdyenNotification;
use App\Models\OrderRefund;
use Throwable;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Carbon;
use DateTime;

class AdyenNotificationsProcessRefundsCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "adyen:notifications_process_refunds";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Process historic adyen notifications refunds";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $refund_notifications = AdyenNotification::query()
                ->select('adyen_notifications.id','adyen_notifications.notification_body','adyen_notifications.created_at')
                ->leftJoin('order_refunds','order_refunds.adyen_notification_id','=','adyen_notifications.id')
                ->where('adyen_notifications.notification_body','like', '%'.'"eventCode":"REFUND"'.'%')
                ->whereNull('order_refunds.id')
                ->get();

            $lb = "\r\n\r\n";
            $msg_string = 'adyen:notifications_process_refunds'.$lb;

            if($refund_notifications->isEmpty())
            {
                $this->info('Nothing to process.');
                return;
            }

            $this->info('Number of notifications to be processed: '.count($refund_notifications));
            $msg_string .= 'Number of notifications to be processed: '.count($refund_notifications).$lb;

            if(!$refund_notifications->isEmpty())
            {
                foreach($refund_notifications as $notification)
                {
                    try {
                        $adyen_response = json_decode($notification->notification_body);
                        $order_refund = new OrderRefund();
                        $order_refund->live_environment = $adyen_response->live;
                        $notification_item = $adyen_response->notificationItems[0]->NotificationRequestItem;
                        if (isset($notification_item->additionalData->shopperCountry))
                            $order_refund->country = $notification_item->additionalData->shopperCountry;
                        $order_refund->value = $notification_item->amount->value / 100;
                        $order_refund->currency = $notification_item->amount->currency;
                        $order_refund->event_code = $notification_item->eventCode;
                        $order_refund->order_id = $notification_item->merchantReference;
                        $order_refund->success = $notification_item->success;
                        $order_refund->adyen_notification_id = $notification->id;
                        $order_refund->created_at = $notification->created_at;
                        $event_date = Carbon::createFromFormat(DateTime::ATOM, $notification_item->eventDate)->toDateTimeString();
                        $order_refund->event_date = $event_date;
                        $order_refund->save();

                        $this->info('Order refund created, id: ' . $order_refund->id);
                        $msg_string .= 'Order refund created, id: ' . $order_refund->id . $lb;
                    }
                    catch (Throwable $e)
                    {
                        app('sentry')->captureException($e);
                        $this->info('Order refund failed, notification id: ' . $notification->id);
                        $msg_string .= 'Order refund failed, notification id: ' . $notification->id . $lb;
                    }
                }
            }
            $this->info("All notifications have been processed.");
            $msg_string .= "All notifications have been processed.".$lb;
            Mail::raw($msg_string, function($msg) {
                $msg->to(env('MAIL_CRON_INFO_ADDRESS'));
                $msg->subject('Cron job adyen notifications refund process start command on '.Carbon::today()->toDateString());
            });
        } catch (Throwable $e) {
            $this->error($e->getMessage());
        }
    }
}