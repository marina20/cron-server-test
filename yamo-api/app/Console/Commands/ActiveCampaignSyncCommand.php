<?php

namespace App\Console\Commands;

use App\Models\AcEventDefinition;
use App\Models\AcEventLookup;
use App\Models\AcEventOccurred;
use App\Models\User;
use App\Services\ActiveCampaign\ActiveCampaignService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ActiveCampaignSyncCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "activecampaign:sync";

    const LOG_TITLE = 'ActiveCampaignSync command:';
    const LOG_FILE = 'ActiveCampaignSync.txt';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Upload the results to activecampaign";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Start executing the queries \n");

        // Put to only store the log of the last execution (rough debug)

        $queryACDefinitions = AcEventDefinition::where(['event_type' => 'SQL'])->whereNotNull('event_query')->get();
        Storage::disk('local')->put(self::LOG_FILE, '['.Carbon::now().'] Start to collect and sync the events ('.$queryACDefinitions->count().' queries)');
        $bar = $this->output->createProgressBar(count($queryACDefinitions));
        $bar->start();
        foreach ($queryACDefinitions as $definition){
            $queryResults = DB::select(DB::raw($definition->event_query));
            Storage::disk('local')->append(self::LOG_FILE, '['.Carbon::now().'] Query '.$definition->event_name.' had '.count($queryResults).' results');
            foreach($queryResults as $row){
                // Get the last event that has occurred
                if(!empty($row->email)) {
                    $lastEvent = AcEventOccurred::where(['email' => $row->email, 'event_name' => $definition->event_name, 'timestamp' => $row->timestamp])->latest()->first();
                    // and only create one if there was none or if it differs from the last one
                    if (empty($lastEvent)) {
                        AcEventOccurred::firstOrCreate(['event_name' => $definition->event_name, 'timestamp' => $row->timestamp, 'email' => $row->email, 'synced' => false]);
                    }
                } else {
                    $this->warn('Email was missing at occurred-event '.$definition->event_name);
                    Storage::disk('local')->append(self::LOG_FILE, '['.Carbon::now().'] Email was missing at occurred-event '.$definition->event_name);
                }
            }
            $bar->advance();
        }
        $bar->finish();

        $this->info("\n Start sync to activecampaign \n");
        $acService = new ActiveCampaignService();
        $occurredEvents = AcEventOccurred::where(['synced' => false])->orderBy('timestamp','asc')->get();
        $emptyEvents = [];
        $bar = $this->output->createProgressBar(count($occurredEvents));
        Storage::disk('local')->append(self::LOG_FILE, '['.Carbon::now().'] '.count($occurredEvents).' events start to sync');
        $bar->start();
        foreach($occurredEvents as $occurredEvent){
            $user = User::where(['email' => $occurredEvent->email])->first();
            if(empty($user)){
                $user = User::where('email','like', '%'.$occurredEvent->email.'%')->first();
                if(!empty($user) && strpos($user->email,'delete')!==false){
                    if(strpos($occurredEvent->event_name,'deleted')===false){
                        Storage::disk('local')->append(self::LOG_FILE, '[' . Carbon::now() . '] User ' . $user->email . '(found ' . $occurredEvent->email . ') deleted, ignore it (event: ' . $occurredEvent->event_name . ')');
                        continue;
                    } else{
                        $user->deleted = true;
                        Storage::disk('local')->append(self::LOG_FILE, '[' . Carbon::now() . '] User ' . $user->email . '(found ' . $occurredEvent->email . ') deleted, but handle it because its a delete-event (event: ' . $occurredEvent->event_name . ')');
                    }
                } else{
                    Storage::disk('local')->append(self::LOG_FILE, '[' . Carbon::now() . '] User ' . $occurredEvent->email . ' not existed in db (unregistered) (event: ' . $occurredEvent->event_name . ')');
                }
                $user = new User;
                $user->email = $occurredEvent->email;
                $user->incomplete = true;
            }
            $event = AcEventLookup::where(['event_name' => $occurredEvent->event_name])->first();
            if(empty($event)){
                if(!in_array($occurredEvent->event_name,$emptyEvents)) {
                    $emptyEvents[] = $occurredEvent->event_name;
                }
            } else {
                $addTags = explode(",", $event->tags_add);
                $removeTags = explode(",", $event->tags_remove);
                $result = $acService->addTagToUser($addTags, $removeTags, $user);
                // When the return-value is a empty array, it's all fine. if it's filled, those are failed requests
                if(!empty($result['failedMessages'])){
                    $this->info("Found errors on user ".$user->email.' ('.$occurredEvent->event_name.')');
                    foreach($result['failedMessages'] as $failLine){
                        $this->info($failLine);
                        Storage::disk('local')->append(self::LOG_FILE, $failLine);
                    }
                } else {
                    // Only mark as synced when nothing failed
                    // Storage::disk('local')->append(self::LOG_FILE, 'Successfull add user @'.$occurredEvent->event_name);
                    $occurredEvent->synced = true;
                    $occurredEvent->save();
                }
            }
            $bar->advance();
        }

        if(sizeof($emptyEvents)>0){
            Storage::disk('local')->append(self::LOG_FILE, '['.Carbon::now().'] Following events are empty and ignored:');
            $this->info('Following events are empty and ignored:');
            foreach ($emptyEvents as $emptyEvent){
                $this->info($emptyEvent);
                Storage::disk('local')->append(self::LOG_FILE, $emptyEvent);
            }
            Storage::disk('local')->append(self::LOG_FILE, 'End of empty events');
        }
        Storage::disk('local')->append(self::LOG_FILE, '['.Carbon::now().'] '.count($occurredEvents).' events get synced');
        $bar->finish();
    }
}
