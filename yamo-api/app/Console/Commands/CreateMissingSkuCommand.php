<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-07-08
 * Time: 14:51
 */

namespace App\Console\Commands;

use App\Models\Order;
use App\Services\OrderService;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Helpers;

class CreateMissingSkuCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "create:missing_sku";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generate missing SKU's for the orders that have delivery date in the future";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {

            $today = Carbon::tomorrow()->toDateString();
            $orders = Order::query()
                ->select('orders.*')
                ->whereDate('orders.delivery_date','>=',$today)
                ->whereIn('orders.status',[Order::STATUS_PAYED,Order::STATUS_PENDING])
                ->whereNull('orders.SKU')
                ->get();


            $lb = "\r\n\r\n";
            $msg_string = 'create:missing_sku'.$lb;

            if($orders->isEmpty())
            {
                $this->info('Nothing to send.');
                return;
            }

            $this->info('Number of orders to be processed: '.count($orders));
            $msg_string .= 'Number of orders to be processed: '.count($orders).$lb;

            if(!$orders->isEmpty())
            {
                foreach($orders as $order)
                {
                    try {
                        Helpers::set_locale($order->getCountryCode($order->shipping_country));
                        if ($code = OrderService::generateSKU($order)) {
                            $this->info('sku for order id: ' . $order->id . ' created');
                            $this->info('generated sku: ' . $code);
                            $msg_string .= 'sku for order id: ' . $order->id . ' created' . $lb;
                            $msg_string .= 'generated sku: ' . $code . $lb;
                        } else {
                            $this->info('sku for order id: ' . $order->id . ' had an error');
                            $msg_string .= 'sku for order id: ' . $order->id . ' had an error' . $lb;
                        }
                    } catch (\Throwable $e)
                    {
                        app('sentry')->captureException($e);
                        $this->info('sku for order id: ' . $order->id . ' had an error');
                        $msg_string .= 'sku for order id: ' . $order->id . ' had an error' . $lb;
                    }
                }
            }
            $this->info("All emails have been sent.");
            $msg_string .= "All emails have been sent.".$lb;
            Mail::raw($msg_string, function($msg) {
                $msg->to(env('MAIL_CRON_INFO_ADDRESS'));
                $msg->subject('Cron job Create missing SKUs for '. Carbon::now()->toDateString());
            });
        } catch (\Throwable $e) {
            $this->error("An error occurred");
        }
    }
}