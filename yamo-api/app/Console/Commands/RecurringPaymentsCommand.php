<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/5/18
 * Time: 2:18 PM
 */
namespace App\Console\Commands;

use App\Jobs\SendRecurringPaymentEmailJob;
use App\Models\Country;
use App\Models\Order;
use App\Models\Voucher;
use App\Services\Adyen\RecurringPayment;
use App\Services\MFGInvoice\Recurring\RecurringPaymentMFG;
use App\Services\OrderReportService;
use App\Services\Payment\Recurring;
use Throwable;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class RecurringPaymentsCommand extends Command
{
    protected $recurring_data;
    protected $recurring_payment;
    protected $recurring_payment_mfg;
    protected $report_service;

    public function __construct(Recurring $recurring_data,
                                RecurringPayment $recurring_payment,
                                RecurringPaymentMFG $recurring_payment_mfg,
                                OrderReportService $report_service)
    {
        parent::__construct();
        $this->recurring_data = $recurring_data;
        $this->recurring_payment = $recurring_payment;
        $this->recurring_payment_mfg = $recurring_payment_mfg;
        $this->report_service = $report_service;
     }
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "send:recurring_payments";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Check if some recurring payments needs to be triggered and trigger it";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $today = Carbon::today();

            $this->info($today);

            $dates = $this->report_service->getDeliveryDatesByCountryId([
                Country::COUNTRY_SWITZERLAND,
                Country::COUNTRY_GERMANY,
                Country::COUNTRY_AUSTRIA], $today);
            $orders = $this->recurring_data->list($dates);

            $lb = "\r\n\r\n";
            $msg_string = 'send:recurring_payments' . $lb;

            if ($orders->isEmpty()) {
                $this->info('Nothing to send.');
                return;
            }

            $this->info('Number of orders to be processed: ' . count($orders));
            $msg_string .= 'Number of orders to be processed: ' . count($orders) . $lb;

            $excel_orders = [];
            if (!$orders->isEmpty()) {
                foreach ($orders as $order) {
                    $this->info('payment for order id: ' . $order->id . ' processing');
                    $msg_string .= 'payment for order id: ' . $order->id . ' processing' . $lb;

                    if($order->payment_method_id === Order::PAYMENT_METHOD_CREDITCARD
                        || $order->payment_method_id === Order::PAYMENT_METHOD_PAYPAL
                        || $order->payment_method_id === Order::PAYMENT_METHOD_SOFORT){
	                    $response = $this->recurring_payment->pay($order);

	                    $this->info('payment for order id: ' . $order->id . ' processed. Message: '.$response['message']);
	                    $msg_string .= 'payment for order id: ' . $order->id . ' processed. Message: '.$response['message'] . $lb;
                    } else if($order->payment_method_id === Order::PAYMENT_METHOD_INVOICE){
	                    $response = $this->recurring_payment_mfg->pay($order);
	                    $this->info('payment for order id: ' . $order->id . ' processed. Message: '.$response['message']);
	                    $msg_string .= 'payment for order id: ' . $order->id . ' processed. Message: '.$response['message'] . $lb;
                    } else {
                    	//This should never happen, its either credit card/paypal(Adyen) or invoice(MFG).
                    }
                    $order = Voucher::redeemVoucher($order);
                    $excel_orders[] = [
                        'id' => $order->id,
                        'user_id' => $order->user_id,
                        'created_via' => $order->created_via,
                        'shipping_first_name' => $order->shipping_first_name,
                        'shipping_last_name' => $order->shipping_last_name,
                        'order_total' => $order->order_total,
                        'shipping_country' => $order->shipping_country,
                        'payment_method_id' =>$order->payment_method_id,
                        'paid_date'=>$order->paid_date,
                        'completed_date'=>$order->completed_date,
                        'status'=>$order->status,
                        'resultCode'=>$response['resultCode'],
                        'message'=>$response['message']
                    ];
                }
            }

            $excel = app('excel');
            $excel_file = $excel->create(
                'recurring-payment-report-' . $today->toDateString(),
                function ($excel) use ($excel_orders) {
                    $excel->sheet('Info', function ($sheet) use ($excel_orders) {
                        $sheet->fromArray($excel_orders);
                    });
                })->store("xlsx", false, true)['full'];

            dispatch(new SendRecurringPaymentEmailJob($excel_file));

            Mail::raw($msg_string, function ($msg) use ($today){
                $msg->from('cron@yamo.ch');
                $msg->to(env('MAIL_CRON_INFO_ADDRESS'));
                $msg->subject('Cron job Recurring Payment for ' . $today->toDateString());
            });
        }
        catch
        (Throwable $e) {
            app('sentry')->captureException($e);
            $this->error("An error occurred: " . $e->getMessage());
        }
    }
}