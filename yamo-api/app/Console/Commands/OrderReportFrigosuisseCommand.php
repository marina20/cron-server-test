<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/5/18
 * Time: 2:18 PM
 */
namespace App\Console\Commands;

use App\Jobs\SendOrderReportSwitzerlandEmailJob;
use App\Models\Country;
use App\Services\OrderReportService;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class OrderReportFrigosuisseCommand extends Command
{
    protected $report_service;

    public function __construct(OrderReportService $report_service)
    {
        parent::__construct();
        $this->report_service = $report_service;
    }
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "send:order_report_ch";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Send excel file to Frigosuisse";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $date = Carbon::today();

            $this->info($date);

            $dates = $this->report_service->getDeliveryDatesByCountryId([Country::COUNTRY_SWITZERLAND], $date);

            $files = $this->report_service->create(OrderReportService::FRIGOSUISSE, $dates);

            if(!empty($files))
            {
                dispatch(new SendOrderReportSwitzerlandEmailJob($files));
                $this->info('Report for '. $date.' has been sent');
            }

            $this->info("All reports have been sent.");
        } catch (\Throwable $e) {
            app('sentry')->captureException($e);
            $this->error("An error occurred");
        }
    }
}