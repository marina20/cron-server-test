<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Jobs\SendDeliveryReminderEmailJob;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Carbon;
use Helpers;

class DeliveryRemindersCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "send:delivery_reminders";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Send delivery reminders scheduled for tomorrow";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $date = Carbon::tomorrow()->toDateString();
            $orders = Order::query()
                ->select('orders.*')
                ->leftJoin('subscriptions','subscriptions.order_id','=','orders.id')
                ->whereDate('orders.delivery_date',$date)
                ->where('orders.status',Order::STATUS_PAYED)
                ->whereNotNull('orders.SKU')
                ->get();


            $lb = "\r\n\r\n";
            $msg_string = 'send:delivery_reminders'.$lb;

            if($orders->isEmpty())
            {
                $this->info('Nothing to send.');
                return;
            }

            $this->info('Number of orders to be processed: '.count($orders));
            $msg_string .= 'Number of orders to be processed: '.count($orders).$lb;

            if(!$orders->isEmpty())
            {
                foreach($orders as $order)
                {
                    try {
                        Helpers::set_locale($order->getCountryCode($order->shipping_country));
                        $this->info('email for order id: ' . $order->id . ' sent');
                        $msg_string .= 'email for order id: ' . $order->id . ' sent' . $lb;
                        dispatch(new SendDeliveryReminderEmailJob($order));
                    } catch (\Throwable $e)
                    {
                        app('sentry')->captureException($e);
                        $this->info('order id: ' . $order->id . ' had an error');
                        $msg_string .= 'order id: ' . $order->id . ' had an error' . $lb;
                    }
                }
            }
            $this->info("All emails have been sent.");
            $msg_string .= "All emails have been sent.".$lb;
            Mail::raw($msg_string, function($msg) {
                $msg->to(env('MAIL_CRON_INFO_ADDRESS'));
                $msg->subject('Cron job Delivery Reminder Info for '.Carbon::tomorrow()->toDateString());
            });
        } catch (\Throwable $e) {
            $this->error("An error occurred");
        }
    }
}