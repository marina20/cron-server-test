<?php
/**
 * Created by PhpStorm.
 * User: milenabasaragin
 * Date: 9/27/18
 * Time: 2:05 PM
 */

namespace App\Console\Commands;

use App\Models\Country;
use App\Models\Translation;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class TranslationCommand extends Command
{
    protected $signature = "send:translation {--local : Export local} {--all : Export all}";
    protected $description = "Generate translation-files on s3";

    private $completifyTmpArr = [];

    /**
     * This method create a multidimensional array by dots in the key
     * @param string $key
     * @param string $value
     * @return array
     */
    private function completify(string $key,string $value):array {
        try {
            $d = explode(".", $key);
            if (sizeof($d) > 1) {
                $this->completifyTmpArr = [$d[0] => $this->completify(substr($key, strlen($d[0]) + 1), $value)];
                return $this->completifyTmpArr;
            } else {
                return [$key => $value];
            }
        } catch (\Throwable $e)
        {
            app('sentry')->captureException($e);
        }
    }

    /**
     * The usual array_merge overwrite parent-nodes.
     * This modified method merges them properly.
     * Found @stackoverflow; https://stackoverflow.com/a/25712428
     * @param array $array1
     * @param array $array2
     * @return array merged result
     */
    private function array_merge_recursive_ex(array $array1, array $array2):array {
        try {
            $merged = $array1;
            foreach ($array2 as $key => & $value) {
                if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
                    $merged[$key] = $this->array_merge_recursive_ex($merged[$key], $value);
                } else if (is_numeric($key)) {
                    if (!in_array($value, $merged)) {
                        $merged[] = $value;
                    }
                } else {
                    $merged[$key] = $value;
                }
            }
            return $merged;
        } catch (\Throwable $e)
        {
            app('sentry')->captureException($e);
        }
    }

    public function handle():void {
        try {
            $languages = Country::select('language')->get()->pluck('language');
            $languages[] = 'key-key';
            foreach ($languages as $language) {
                $completeLanguage = [];
                $typeFilter = ["type" => "fe"];
                if (!empty($this->options()['all'])) {
                    $typeFilter = [];
                }
                if ($language != 'key-key') {
                    $translations = Translation::select('key-key', $language)->where($typeFilter)->get()->toArray();
                } else {
                    $translations = Translation::select('key-key')->where($typeFilter)->get()->toArray();
                }
                foreach ($translations as $translation) {
                    $this->completifyTmpArr = [];
                    // Add new array with merge-method
                    if (is_null($translation[$language])) {
                        echo "FAIL TO EXPORT FOR " . $language . ":";
                        var_dump($translation);
                    } else {
                        $completeLanguage = $this->array_merge_recursive_ex($completeLanguage, $this->completify($translation['key-key'], $translation[$language]));
                    }
                }
                $fs = env("filesystems.default");
                if (!empty($this->options()['local'])) {
                    $fs = "local";
                }
                Storage::disk($fs)->put('translations/' . $language . '.json', json_encode($completeLanguage));
            }
            $this->info('Generate translation complete');
        } catch (\Throwable $e)
        {
            app('sentry')->captureException($e);
        }
    }
}
