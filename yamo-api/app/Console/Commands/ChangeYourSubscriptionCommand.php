<?php
/**
 * Created by PhpStorm.
 * User: milenabasaragin
 * Date: 10/22/18
 * Time: 2:49 PM
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Order;
use Carbon\Carbon;
use App\Jobs\SendChangeSubscriptionEmailJob;
use Helpers;
use Illuminate\Support\Facades\Mail;
use Throwable;


class ChangeYourSubscriptionCommand extends Command
{
    protected $signature = 'change:subscription';

    protected $description = 'Send change subscription email';

    public function handle()
    {
        try {
            $orders = Order::query()
                ->select('orders.*')
                ->leftJoin('subscriptions','subscriptions.order_id','=','orders.id')
                ->whereDate('orders.delivery_date',
                    Carbon::today()->addDay(Order::CHANGE_SUBSCRIPTION_EMAIL_PERIOD)->toDateString())
                ->whereRaw('orders.status not in (\'' . Order::STATUS_CANCELLED . '\')')
                ->where('orders.created_via', Order::CATEGORY_SUBSCRIPTION)
                ->whereNotNull('orders.parent_id')
                ->get();

            $lb = "\r\n\r\n";
            $msg_string = 'change:subscription' . $lb;

            if(empty($orders))
            {
                $this->info('Nothing to send.');
                return;
            }

            $this->info('Number of orders to be processed: '.count($orders));
            $msg_string .= 'Number of orders to be processed: '.count($orders).$lb;

            if(!empty($orders))
            {
                foreach($orders as $order)
                {
                    try {
                        Helpers::set_locale($order->getCountryCode($order->shipping_country));
                        $this->info('email for order id: ' . $order->id . ' sent');
                        $msg_string .= 'email for order id: ' . $order->id . ' sent' . $lb;
                        dispatch(new SendChangeSubscriptionEmailJob($order));
                    }
                    catch (\Throwable $e)
                    {
                        app('sentry')->captureException($e);
                        $this->info('email for order id: ' . $order->id . ' had an error');
                        $msg_string .= 'email for order id: ' . $order->id . ' had an error' . $lb;
                    }
                }
            }
            $this->info("All emails have been sent.");
            $msg_string .= "All emails have been sent.".$lb;
            Mail::raw($msg_string, function($msg) {
                $msg->to(env('MAIL_CRON_INFO_ADDRESS'));
                $msg->subject('Cron job Delivery Reminder Info for '.Carbon::tomorrow()->toDateString());
            });
        } catch (Throwable $e) {
            $this->error("An error occurred");
        }
    }
}
