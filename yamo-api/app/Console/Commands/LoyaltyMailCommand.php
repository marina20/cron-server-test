<?php

namespace App\Console\Commands;

use App\Jobs\SendLoyalty1EmailJob;
use App\Jobs\SendLoyalty2EmailJob;
use App\Jobs\SendLoyalty3EmailJob;
use App\Jobs\SendLoyalty4EmailJob;
use App\Jobs\SendLoyalty5EmailJob;
use App\Models\Order;
use App\Jobs\SendDeliveryReminderEmailJob;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Carbon;
use Helpers;

class LoyaltyMailCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "send:loyalty_mail";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Send loyalty-mails";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            $date = Carbon::now()->subDays(3)->toDateString();
            $orders = Order::
            whereDate('delivery_date', $date)
                ->where('status', Order::STATUS_PAYED)
                ->get();

            if($orders->isEmpty()){
                $this->info('Nothing to send.');
                return;
            }

            $this->info('Number of orders to be processed: ' . count($orders));

            if($orders->isNotEmpty()){
                foreach($orders as $order){
                    try{
                        Helpers::set_locale($order->getCountryCode($order->shipping_country));
                        $nrOfPaidOrders = Order::where(['user_id' => $order->user_id])->whereNotNull('paid_date')->where('resent',0)->where('delivery_date','<',Carbon::now())->count();

                        $this->info("User " . $order->user_id . " loyalty message after " . $nrOfPaidOrders . " box");
                        switch($nrOfPaidOrders){
                            case 1:
                                dispatch(new SendLoyalty1EmailJob($order));
                                break;
                            case 3:
                                dispatch(new SendLoyalty2EmailJob($order));
                                break;
                            case 5:
                                dispatch(new SendLoyalty3EmailJob($order));
                                break;
                            case 9:
                                dispatch(new SendLoyalty4EmailJob($order));
                                break;
                        }
                    } catch(\Throwable $e){
                        app('sentry')->captureException($e);
                        $this->info('order id: ' . $order->id . ' had an error' . $e->getMessage());
                    }
                }
            }

            $date = Carbon::now()->subDays(1)->toDateString();
            $orders = Order::
            whereDate('delivery_date', $date)
                ->where('status', Order::STATUS_PAYED)
                ->get();

            if($orders->isNotEmpty()){
                foreach($orders as $order){
                    try{
                        Helpers::set_locale($order->getCountryCode($order->shipping_country));
                        $nrOfPaidOrders = Order::where(['user_id' => $order->user_id])->whereNotNull('paid_date')->where('resent',0)->count();

                        $this->info("User " . $order->user_id . " loyalty message after " . $nrOfPaidOrders . " box");
                        switch($nrOfPaidOrders){
                            case 10:
                                dispatch(new SendLoyalty5EmailJob($order));
                                break;
                        }
                    } catch(\Throwable $e){
                        app('sentry')->captureException($e);
                        $this->info('order id: ' . $order->id . ' had an error' . $e->getMessage());
                    }
                }
            }
            $this->info("All emails have been sent.");
        } catch(\Throwable $e){
            $this->error("An error occurred");
        }
    }
}