<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/5/18
 * Time: 2:18 PM
 */
namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Carbon;

class TestSchedulerCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "test:scheduler";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Test if scheduler is working";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            Mail::raw('ENV: ' . env('APP_ENV') . ' Lumen Scheduler is working ' . Carbon::now(), function($msg) {
                $msg->to(env('MAIL_CRON_INFO_ADDRESS'));
                $msg->subject('Test scheduler');
            });

            $this->info("We are in business!!");
        } catch (\Throwable $e) {
            $this->error("An error occurred: ".$e->getMessage());
        }
    }
}