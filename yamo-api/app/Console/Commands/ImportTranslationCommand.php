<?php
/**
 * Created by PhpStorm.
 * User: vinzenz
 * Date: 9/29/18
 * Time: 22:05 PM
 */

namespace App\Console\Commands;

use App\Models\Translation;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ImportTranslationCommand extends Command
{
    protected $signature = "import:translation 
    {jsonFile : The file for import} 
    {language : Target language}
    {type : Type, eg fe}
    {--notes= : Optional notes, use quotes}";
    protected $description = "Import translation-json into database. File-root is at storage/app. Need arguments: file language type. 
                              Optional: --notes='A note'";

    /**
     * Import tada ;)
     * @param $json json-object or array
     * @param string $key only used internal, current key
     * @param string $filler only used internal, temporary storage
     */
    private function importada($json,string $key='',string $filler=''):void{
        try {
            if (!empty($key)) {
                if (!empty($filler)) {
                    $filler .= "." . $key;
                } else {
                    $filler = $key;
                }
            }
            if (!is_object($json) && !is_array($json) && is_string($json) && !empty($filler)) {
                // We reached a endpoint, so save that stuff
                $this->info("Key: " . $filler . ", Value: " . $json);
                $translationModel = Translation::firstOrCreate(['key-key' => $filler]);
                $translationModel[$this->argument('language')] = $json;
                $translationModel->type = $this->argument('type');
                if (!empty($this->options()['notes'])) {
                    $translationModel->notes = $this->options()['notes'];
                }
                $translationModel->save();
            } else {
                // Array or json-object, so go through and check every of it's elements!
                foreach ($json as $key => $val) {
                    try {
                        $this->importada($val, $key, $filler);
                    } catch (\Throwable $e)
                    {
                        app('sentry')->captureException($e);
                    }
                }
            }
        } catch (\Throwable $e)
        {
            app('sentry')->captureException($e);
        }
    }



    public function handle():void {
        try {
            $this->info('Start import');
            $jsonRaw = Storage::disk('local')->get($this->argument('jsonFile'));
            $json = json_decode($jsonRaw);
            $this->importada($json);
            $this->info('Finished import');
        } catch (\Throwable $e)
        {
            app('sentry')->captureException($e);
        }
    }
}
