<?php


namespace App\Console\Commands;

use App\Models\MfgRequest;
use App\Models\Order;
use App\Services\MFGInvoice\MFGInvoice;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class MfgConfirmationProcessCommand extends Command
{
    protected $mfgInvoice;

    public function __construct(MFGInvoice $mfgInvoice)
    {
        parent::__construct();
        $this->mfgInvoice = $mfgInvoice;
    }
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "process:mfg_confirmation";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "For delivered orders trigger confirmation requests";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try
        {
            $date = Carbon::now()->toDateString();
            $orders = Order::select('orders.*')
                ->distinct()
                ->whereDate('orders.delivery_date', $date)
                ->join('mfg_request','mfg_request.order_id','=','orders.id')
                ->where('orders.status', Order::STATUS_PAYED)
                ->where('orders.payment_method_id',Order::PAYMENT_METHOD_INVOICE)
                ->whereIn('mfg_request.mfg_request_type_id',[MfgRequest::CARD_NUMBER_REQUEST_ID, MfgRequest::FINANCIAL_REQUEST_ID])
                ->whereNotIn('orders.id',function ($query) {
                    $query->select('order_id')
                        ->from('mfg_request')
                        ->where('mfg_request_type_id',MfgRequest::CONFIRMATION_REQUEST_ID);
                })
                ->get();

            $lb = "\r\n\r\n";
            $msg_string = 'process:mfg_confirmation'.$lb;

            if($orders->isEmpty()){
                $this->info('Nothing to send.');
                return;
            }

            $this->info('Number of orders to be processed: '.count($orders));
            $msg_string .= 'Number of orders to be processed: '.count($orders).$lb;

            foreach ($orders as $order) {
                $result = $this->mfgInvoice->processConfirmation($order);

                if(!$result)
                {
                    $this->info('order ' . $order->id . ' FAILED - mfg invoice service returned FALSE, pay attention to this order');
                    $msg_string .= 'order ' . $order->id . ' FAILED - mfg invoice service returned FALSE, pay attention to this order' . $lb;
                }
                else
                {
                    $this->info('order ' . $order->id . ' success');
                    $msg_string .= 'order ' . $order->id . ' success' . $lb;
                }
            }
            $this->info("All emails have been sent.");
            $msg_string .= "All emails have been sent.".$lb;
            Mail::raw($msg_string, function($msg) {
                $msg->to(env('MAIL_CRON_INFO_ADDRESS'));
                $msg->subject('Cron job Delivery Reminder Info for '.Carbon::tomorrow()->toDateString());
            });
        }
        catch
        (\Throwable $e) {
            $this->error("An error occurred: " . $e->getMessage());
            app('sentry')->captureException($e);
        }
    }
}