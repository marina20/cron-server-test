<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/5/18
 * Time: 2:18 PM
 */
namespace App\Console\Commands;

use App\Jobs\SendRecurringPaymentInfoEmailJob;
use App\Models\Country;
use App\Services\OrderReportService;
use App\Services\Payment\Recurring;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class RecurringPaymentsInfoCommand extends Command
{
    protected $recurring_data;
    protected $report_service;

    public function __construct(Recurring $recurring_data,
                                OrderReportService $report_service)
    {
        parent::__construct();
        $this->recurring_data = $recurring_data;
        $this->report_service = $report_service;
    }

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "send:recurring_payments_info";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Send info about recurring payments that will be triggered later";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $today = Carbon::today();

            $this->info($today);

            $dates = $this->report_service->getDeliveryDatesByCountryId([
                Country::COUNTRY_SWITZERLAND,
                Country::COUNTRY_GERMANY,
                Country::COUNTRY_AUSTRIA], $today);
            $orders = $this->recurring_data->list($dates);

            $lb = "\r\n\r\n";
            $msg_string = 'send:recurring_payments_info' . $lb;

            if ($orders->isEmpty()) {
                $this->info('Nothing to send.');
                return;
            }

            $this->info('Number of orders to be processed: ' . count($orders));
            $msg_string .= 'Number of orders to be processed: ' . count($orders) . $lb;

            $excel_orders = [];
            if (!$orders->isEmpty()) {
                foreach ($orders as $order) {
                    try {
                        $this->info('payment for order id: ' . $order->id . ' processing');
                        $msg_string .= 'payment for order id: ' . $order->id . ' processing' . $lb;
                        $excel_orders[] = [
                            'id' => $order->id,
                            'user_id' => $order->user_id,
                            'created_via' => $order->created_via,
                            'shipping_first_name' => $order->shipping_first_name,
                            'shipping_last_name' => $order->shipping_last_name,
                            'order_total' => $order->order_total,
                            'shipping_country' => $order->shipping_country,
                            'payment_method_id' => $order->payment_method_id,
                            'status' => $order->status
                        ];
                    }
                    catch (\Throwable $e)
                    {
                        $excel_orders[] = [
                            'id' => $order->id,
                            'user_id' => $order->user_id,
                            'created_via' => $order->created_via,
                            'shipping_first_name' => $order->shipping_first_name,
                            'shipping_last_name' => $order->shipping_last_name,
                            'order_total' => $order->order_total,
                            'shipping_country' => $order->shipping_country,
                            'payment_method_id' => $order->payment_method_id,
                            'paid_date' => $order->paid_date,
                            'completed_date' => $order->completed_date,
                            'status' => $order->status,
                            'resultCode' => '500',
                            'message' => 'An error has occured: '.$e->getMessage()
                        ];
                        app('sentry')->captureException($e);
                        $this->error("An error occurred: " . $e->getMessage());
                    }
                }
            }

            $excel = app('excel');
            $excel_file = $excel->create(
                'recurring_payment_report_info-' . $today->toDateString(),
                function ($excel) use ($excel_orders) {
                    $excel->sheet('Info', function ($sheet) use ($excel_orders) {
                        $sheet->fromArray($excel_orders);
                });
            })->store("xlsx", false, true)['full'];

            dispatch(new SendRecurringPaymentInfoEmailJob($excel_file));

            Mail::raw($msg_string, function ($msg) use ($today) {
                $msg->to(env('MAIL_CRON_INFO_ADDRESS'));
                $msg->subject('Cron job Recurring Payment Info for ' . $today->toDateString());
            });
        }
        catch
        (\Throwable $e) {
            app('sentry')->captureException($e);
            $this->error("An error occurred: " . $e->getMessage());
        }
    }
}