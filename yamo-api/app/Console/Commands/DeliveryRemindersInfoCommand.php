<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/5/18
 * Time: 2:18 PM
 */
namespace App\Console\Commands;

use App\Models\Order;
use App\Jobs\SendDeliveryReminderInfoEmailJob;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Carbon;
use Helpers;

class DeliveryRemindersInfoCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "send:delivery_reminders_info";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Send delivery reminders scheduled for tomorrow";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $date = Carbon::tomorrow()->toDateString();
            $orders = Order::query()
                ->select('orders.*')
                ->leftJoin('subscriptions','subscriptions.order_id','=','orders.id')
                ->whereDate('orders.delivery_date',$date)
                ->where('orders.status',Order::STATUS_PAYED)
                ->whereNotNull('orders.SKU')
                ->get();

            $lb = "\r\n\r\n";
            $msg_string = 'send:delivery_reminders_info'.$lb;

            if($orders->isEmpty())
            {
                $this->info('Nothing to send.');
                return;
            }

            $this->info('Number of orders to be processed: '.count($orders));
            $msg_string .= 'Number of orders to be processed: '.count($orders).$lb;

            $excel_orders = [];
            if(!$orders->isEmpty())
            {
                foreach($orders as $order)
                {
                    try {
                        Helpers::set_locale($order->getCountryCode($order->shipping_country));
                        $this->info('email for order id: ' . $order->id . ' sent');
                        $msg_string .= 'email for order id: ' . $order->id . ' sent' . $lb;
                        $excel_orders[] = [
                            'id' => $order->id,
                            'user_id' => $order->user_id,
                            'created_via' => $order->created_via,
                            'shipping_first_name' => $order->shipping_first_name,
                            'shipping_last_name' => $order->shipping_last_name,
                            'order_total' => $order->order_total,
                            'shipping_country' => $order->shipping_country
                        ];
//                    dispatch(new SendDeliveryReminderEmailJob($order));
                    } catch (\Throwable $e)
                    {
                        app('sentry')->captureException($e);
                        $this->info('order id: ' . $order->id . ' had an error');
                        $msg_string .= 'order id: ' . $order->id . ' had an error' . $lb;
                    }
                }
            }

            $excel = app('excel');
            $excel_file = $excel->create('delivery_reminder_info-'.Carbon::tomorrow()->toDateString(), function($excel) use ($excel_orders) {
                $excel->sheet('Info', function($sheet) use ($excel_orders) {
                    $sheet->fromArray($excel_orders);
                });
            })->store("xlsx",false,true)['full'];

            dispatch(new SendDeliveryReminderInfoEmailJob($excel_file));

            $this->info("All emails have been sent.");
            $msg_string .= "All emails have been sent.".$lb;
            Mail::raw($msg_string, function($msg) {
                $msg->to(env('MAIL_CRON_INFO_ADDRESS'));
                $msg->subject('Cron job Delivery Reminder Info for '.Carbon::tomorrow()->toDateString());
            });
        } catch (\Throwable $e) {
            $this->error("An error occurred: ".$e->getMessage());
        }
    }
}