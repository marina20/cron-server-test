<?php

namespace App\Console;

use App\Console\Commands\OrderReportProwitoCommand;
use Helpers;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\DeliveryRemindersCommand::class,
        Commands\DeliveryRemindersInfoCommand::class,
        Commands\OrderReportFrigosuisseCommand::class,
        Commands\OrderReportProwitoCommand::class,
        Commands\OrderCleanupUnpaidCreditcardCommand::class,
        Commands\TestSchedulerCommand::class,
        Commands\AdyenNotificationsProcessRefundsCommand::class,
        Commands\RecurringPaymentsInfoCommand::class,
        Commands\RecurringPaymentsCommand::class,
        Commands\CreateNextOrderCommand::class,
        Commands\ChangeYourSubscriptionCommand::class,
        Commands\TranslationCommand::class,
        Commands\ImportTranslationCommand::class,
        Commands\CreateMissingSkuCommand::class,
        Commands\ActiveCampaignSyncCommand::class,
        Commands\LoyaltyMailCommand::class,
        Commands\MfgConfirmationProcessCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        if (app()->environment('local')) {
            $schedule->command('test:scheduler')->everyMinute();
        }
        if (app()->environment('production')) {

            // Time should be written in Europe/Zurich time zone but server runs it in UTC regardless from daylight savings time
            $schedule->command('send:loyalty_mail')->dailyAt(Helpers::convertTimeFromCETtoUTC('07:00'));
            $schedule->command('send:delivery_reminders_info')->dailyAt(Helpers::convertTimeFromCETtoUTC('08:30'));
            $schedule->command('send:delivery_reminders')->dailyAt(Helpers::convertTimeFromCETtoUTC('16:30'));
            $schedule->command('delete:unpaid_orders_creditcard')->dailyAt(Helpers::convertTimeFromCETtoUTC('01:50'));
            $schedule->command('send:order_report_ch')->dailyAt(Helpers::convertTimeFromCETtoUTC('06:00'));
            $schedule->command('send:order_report_de_at')->dailyAt(Helpers::convertTimeFromCETtoUTC('07:00'));
            $schedule->command('send:order_report_de_at')->dailyAt(Helpers::convertTimeFromCETtoUTC(OrderReportProwitoCommand::PROWITO_CUT_OFF_TIME));
            $schedule->command('send:recurring_payments_info')->dailyAt(Helpers::convertTimeFromCETtoUTC('02:05'));
            $schedule->command('send:recurring_payments')->dailyAt(Helpers::convertTimeFromCETtoUTC('03:00'));
            $schedule->command('create:next_orders')->dailyAt(Helpers::convertTimeFromCETtoUTC('23:00'));
            $schedule->command('change:subscription')->dailyAt(Helpers::convertTimeFromCETtoUTC('13:00'));
            $schedule->command('send:translation')->everyFifteenMinutes();
            $schedule->command('activecampaign:sync')->dailyAt(Helpers::convertTimeFromCETtoUTC('04:00'));
            $schedule->command('process:mfg_confirmation')->dailyAt(Helpers::convertTimeFromCETtoUTC('08:00'));
        }
    }
}
