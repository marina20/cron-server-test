<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 11/23/17
 * Time: 10:24 AM
 */

namespace App\Traits;


trait FormatMoney
{
    /**
     * Format value to 2 decimals
     * @param null $value
     * @return float|null|string
     */
    public function roundMoney($value=NULL)
    {
        return $this->formatMoney(round($value,2));
    }

    /**
     * Format number to 2 decimals
     * @param $value
     * @return string
     */
    private function formatMoney($value)
    {
        return number_format($value,2);
    }
}