<?php
/**
 * Created by PhpStorm.
 * User: juanse254
 * Date: 8/28/18
 * Time: 2:18 PM
 */

namespace App\Traits;

use SimpleXMLElement;

trait XMLfromArray
{
	/*
	 * This function takes an array as a value, the string of the root tag and and array with the roottag elements.
	 * @params Array $value, String $roottag, Array $tagelements
	 * @return String $xml
	 * */
	public function encodetoXML($value, $roottag, $tagelements)
	{
		$xml = $this->encodetoSimpleXMLObject($value, $roottag, $tagelements);
		return $xml->asXML();
	}

	/*
	 * This function takes an array as a value, the string of the root tag and and array with the roottag elements.
	 * @params Array $value, String $roottag, Array $tagelements
	 * @return SimpleXMLObject $xml
	 * */
	public function encodetoSimpleXMLObject($value, $roottag, $tagelements)
	{
		if(!is_null($tagelements) && !is_null($roottag))
		{
			$elements = "";
			foreach ($tagelements as $name => $element){
				$elements = $elements . $name . "=" . "\"".$element ."\"" . " ";
			}
			$roottag = $roottag . " " . $elements;
		}
		$roottag = is_null($roottag) ? '<?xml version="1.0" encoding="UTF-8"?><root/>' : '<?xml version="1.0" encoding="UTF-8"?>'.'<' . $roottag . '/>';
		$xml = new SimpleXMLElement($roottag);
		$value= $this->array_to_xml($value, $xml);
		return $value;
	}

	/*
	 * This function takes an SimpleXMLObjects and coverts it into a string.
	 * @params SimpleXMLObject $xml
	 * @return array
	 * */
	public function decodeSimpleXMLObject($xml)
	{
		return (array)$xml;
	}

	/*
    * This function takes an SimpleXMLObjects and coverts it into a string.
    * @params string $xml
    * @return SimpleXMLObject
    * */
	public function decodeXML($xml)
	{
		return simplexml_load_string($xml);
	}

	/*
	 * This will parse the elements from the array and insert them into the xml.
	 * @params Array $data, XMLSimpleObject $xml_data
	 * @return XMLSimpleObject $xml_data
	 * */
	private function array_to_xml($data, $xml_data) {
		foreach ($data as $key => $value) {
			if (is_numeric($key)) {
				$key = 'item' . $key; //dealing with <0/>..<n/> issues
			}
			if (is_array($value)) {
				$elements = $this->fetchAttributes($key);
				if (count($elements) > 1) {
					$subnode = null;
					foreach ($elements as $title) {
						if ($title != $elements[0]) {
							$array = explode("=", $title);
							$subnode->addAttribute($array[0], $array[1]);
						}
						else { //This will always happen first, TODO: we must use a do-while here instead of a foreach. Since the else is executed first.
							$subnode = $xml_data->addChild($title);
						}
					}
				}
				else {
					$subnode = $xml_data->addChild($key);
				}
				$this->array_to_xml($value, $subnode);
			}
			else {
				$xml_data->addChild("$key", htmlspecialchars("$value"));
			}
		}
		return $xml_data;
	}

	private function fetchAttributes($key) {
		$elements = explode(" ", $key);
		return $elements;
	}

}