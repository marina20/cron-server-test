<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/24/18
 * Time: 2:58 PM
 */

namespace App\Traits;


trait MFGEncode
{
    private function encode($value)
    {
        return urlencode($value);
    }

    public function url($value)
    {
        return $this->encode($value);
    }

    public function xml($value)
    {
        return 'xml='.$this->encode($value);
    }
}