<?php

namespace App\Http\Resources;

use App\Traits\FormatMoney;
use Illuminate\Http\Resources\Json\Resource;

/**
 * Used in transformation of bundle needed to display in cart
 * Class BundleCartResource
 * @package App\Http\Resources
 */
class BundleCartResource extends Resource
{
    use FormatMoney;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $ingredients = [];
        foreach ($this->box->items as $item)
        {
            $categoryName = ucfirst(strtolower($item->product->categories->name));
            isset($ingredients[$categoryName]) ?:  $ingredients[$categoryName] = [];
            $ingredients[$categoryName][] = $item->quantity . ' x ' . $item->product->name . ' ' . $item->product->weight;
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'image_thumbnail' => $this->image_thumbnail,
            'product_name_color' => $this->product_name_color,
            'ingredients' => $ingredients
        ];
    }
}