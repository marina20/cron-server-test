<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class LoyaltyRuleResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'box' => $this->box_number,
            'description' => $this->description
        ];
    }
}