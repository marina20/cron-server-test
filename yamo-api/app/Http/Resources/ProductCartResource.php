<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Traits\FormatMoney;
use Helpers;

/**
 * Product moel transformed for cart
 * Class ProductCartResource
 * @package App\Http\Resources
 */
class ProductCartResource extends Resource
{
    use FormatMoney;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'category'=>$this->category,
            'product_family'=>$this->product_family,
            'name'=>$this->name,
            'image'=>$this->image,
            'image_thumbnail'=>$this->image_thumbnail,
            'image_alt_tag_key'=>$this->image_alt_tag_key,
            'image_alt_tag_text'=>$this->image_alt_tag_text,
            'original_price'=>$this->formatMoney($this->price),
            'price'=>$this->discounted_price,
            'quantity' => $this->quantity,
            'ingredients'=>$this->ingredients,
            'weight'=>$this->weight,
            'currency'=>Helpers::currency(),
            'months'=>$this->month,
            'produce_type'=>$this->produce_type,
            'product_name_color'=>$this->product_name_color
        ];
    }
}