<?php

namespace App\Http\Resources;

use App\Models\Category;
use App\Models\Product;
use App\Services\Voucher\VoucherService;
use App\Traits\FormatMoney;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\Resource;
use App\Models\Translation;


class BundleResource extends Resource
{
    use FormatMoney;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $ingredients = [];
        $products = new Collection();
        $infoCount = [];
        $items = $this->box->items;
        // Analyze and collect product-objects
        foreach($items as $item){
            $categoryName = ucfirst(strtolower($item->product->categories->name));
            isset($ingredients[$categoryName]) ?:  $ingredients[$categoryName] = [];
            $ingredients[$categoryName][] = $item->quantity . ' x ' . $item->product->name . ' ' . $item->product->weight;
            $item->product->quantity = $item->quantity;
            $products->add($item->product);
            $categoryNameKey = $item->product->categories->name_key;
            isset($infoCount[$categoryNameKey]) ? $infoCount[$categoryNameKey] += $item->quantity : $infoCount[$categoryNameKey] = $item->quantity;
        }

        // Mika doesn't want info text but info array...

        $info = [];
        foreach ($infoCount as $name=>$count)
        {
            $info[] = $count . ' x ' . Translation::trans('BUNDLES.'.strtoupper($name));
        }

        $discountedPrice = VoucherService::getFirstSubscriptionDiscountedSumAndTotal($items)['discounted'];
        return [
            'id' => $this->id,
            'category' => '',
            'name' => $this->name,
            'name_key' => $this->name_key,
            'description' => $this->description,
            'product_family' => strtolower($this->name_key),
            'product_name_color' => $this->product_name_color,
            'product_gradient_color' => $this->product_gradient_color,
            'price' => $this->price(),
            'discounted_price' => $discountedPrice,
            'position' => $this->position,
            'image_thumbnail' => $this->image_thumbnail,
            'image' => $this->image,
            'image_alt_tag_key' => $this->image_alt_tag_key,
            'image_alt_tag_text' => $this->image_alt_tag_text,
            'new' => $this->new,
            'is_bundle' => true,
            'shipping' => '0.00',
            'shipping_tax' => '0.00',
            'information' => $info,
            'ingredients' => $ingredients,
            'currency' => $this->currency,
            'items' => ProductResource::collection($products)
        ];
    }
}