<?php

namespace App\Http\Resources;

use App\Services\Voucher\VoucherService;
use Illuminate\Http\Resources\Json\Resource;
use App\Models\Translation;


class ProductResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'category' => $this->category,
            'product_identifier' => $this->product_identifier_letter,
            'product_family' => $this->product_family,
            'name' => $this->name,
            'name_key' => $this->name_key,
            'product_name_color' => $this->product_name_color,
            'product_gradient_color' => $this->product_gradient_color,
            'months' => $this->months,
            'price' => $this->price,
            'tax_percentage' => $this->tax_percentage,
            'position' => $this->position,
            'image_thumbnail' => $this->image_thumbnail,
            'image' => $this->image,
            'image_alt_tag_key' => $this->image_alt_tag_key,
            'image_alt_tag_text' => $this->image_alt_tag_text,
            'discounted_price' => VoucherService::getProductPriceFirstSubscriptionDiscounted($this),
            'details' => $this->details,
            'weight' => $this->size.'g',
            'shipping' => '0.00',
            'shipping_tax' => '0.00',
            'produce_type' => $this->produce_type,
            'information' => $this->information,
            'ingredients' => $this->ingredients,
            'currency' => $this->currency,
            'time_of_day' => $this->time_of_day,
            'prep_type' => $this->prep_type,
            'ingredients_description' => $this->ingredients_description,
            'table_of_contents' => $this->table_of_contents,
            'new' => $this->new,
            'quantity' => $this->quantity
        ];
    }
}