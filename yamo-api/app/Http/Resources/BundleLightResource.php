<?php

namespace App\Http\Resources;

use App\Models\Category;
use App\Models\Product;
use App\Services\Voucher\VoucherService;
use App\Traits\FormatMoney;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Resources\Json\Resource;
use App\Models\Translation;


class BundleLightResource extends Resource
{
    use FormatMoney;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $info = [];
        $infoCount = [];
        $items = $this->box->items;
        // Analyze and collect product-objects
        foreach($items as $item){
            $categoryNameKey = $item->product->categories->name_key;
            isset($infoCount[$categoryNameKey]) ? $infoCount[$categoryNameKey] += $item->quantity : $infoCount[$categoryNameKey] = $item->quantity;
        }

        foreach ($infoCount as $name=>$count)
        {
            $info[] = $count . ' x ' . Translation::trans('BUNDLES.'.strtoupper($name));
        }
        $discountedPrice = VoucherService::getFirstSubscriptionDiscountedSumAndTotal($items)['discounted'];
        return [
            'id' => $this->id,
            'category' => '',
            'name' => $this->name,
            'name_key' => $this->name_key,
            'product_family' => strtolower($this->name_key),
            'product_name_color' => $this->product_name_color,
            'product_gradient_color' => $this->product_gradient_color,
            'price' => $this->price(),
            'discounted_price' => $discountedPrice,
            'image_thumbnail' => $this->image_thumbnail,
            'image' => $this->image,
            'image_alt_tag_key' => $this->image_alt_tag_key,
            'image_alt_tag_text' => $this->image_alt_tag_text,
            'new' => $this->new,
            'is_bundle' => true,
            'information' => $info,
            'currency' => $this->currency,
        ];
    }
}