<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\Console\Helper\Helper;

class LocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // read the language from the request header
        $locale = $request->header('Content-Language');

        // if the header is missed
        if(!$locale){
            // take the default local language
            $locale = env('DEFAULT_LOCALE');
        }

        // check the languages defined is supported
        if (!in_array($locale, [\Helpers::LOCALE_DE,\Helpers::LOCALE_CH,\Helpers::LOCALE_AT])) {
            // respond with error
            $locale = env('DEFAULT_LOCALE');
        }

        // set the local language
        app('translator')->setLocale($locale);

        return $next($request);
    }
}
