<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/3/17
 * Time: 3:52 PM
 */

namespace App\Http\Middleware;

use Closure;

class CrossOriginMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        @header('Access-Control-Allow-Origin: *');
        @header('Access-Control-Allow-Headers: Content-Type, Authorization');
        @header('Access-Control-Allow-Methods: GET, HEAD, OPTIONS, PATCH, POST, PUT, DELETE');

        if ($request->getMethod() == "OPTIONS") {
            return response('', 200);
        }

        return $next($request);
    }
}