<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 9/21/17
 * Time: 1:03 PM
 */

namespace App\Http\Controllers;

use App\Jobs\SendSubscriptionCancelEmailJob;
use App\Models\Country;
use App\Models\Order;
use App\Services\CustomBox\CreateContent;
use App\Traits\FormatMoney;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Validator;


class OrderController extends Controller
{
    use FormatMoney;
    const CANCELLATION_STATUS = 'cancelled';
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $country = Country::where('content_code',app('translator')->getLocale())->first();
        $data = $this->order->query()->with('items')->with('subscription')->where('user_id',Auth::user()->id)
            ->where('country_id',$country->id)->get()->toArray();
        return response()->json(['data'=>$data]);
    }

    /**
     * List of Orders from customer.
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function list(){
	    $country = Country::where('content_code',app('translator')->getLocale())->first();
	    $data = $this->order->query()
            ->with('items')
            ->with('subscription')
            ->where('user_id',Auth::user()->id)
            ->where('country_id',$country->id)
            ->get()
            ->toArray();
	    $filtered_complete =  array();
	    foreach ($data as $order){

			//TODO: Use tracking information instead of using time, when we start adding tracking numbers to orders.
		    if($order['status'] == Order::STATUS_CANCELLED){
		    	$order['delivery_status'] = Order::STATUS_CANCELLED;
		    }
		    else {
			    $order['delivery_status'] = new DateTime() < new DateTime($order['delivery_date']) ? 'Pending' : 'Delivered';
		    }

            if($order['resent']){
                $order['order_total'] = $this->formatMoney(0);
            }
		    //This will be expanded according to profile_payment needs.
		    $needed = ['items', 'created_via', 'delivery_status', 'delivery_date', 'order_total', 'id', 'currency'];
		    $order = array_filter(
			    $order,
			    function ($key) use ($needed) {
				    return in_array($key, $needed);
			    },
			    ARRAY_FILTER_USE_KEY
		    );
		    array_push($filtered_complete,$order);
	    }
	    return response()->json(['data'=>$filtered_complete]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
//            'name' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json(['message'=>'Failure.', 'errors'=>$validator->errors()]);
        }



        return response()->json(['message'=>"it's unfinished"]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $order = $this->order->query()->with('items')->with('subscription')->find($id);

        if(!empty($order)) {
            $order = $order->toArray();
        };
        if(empty($order['shipping_street_name'])){
            $result = CreateContent::migrateAddress($order['shipping_address_1']);
            $order['shipping_street_name'] = $result[0];
            $order['shipping_street_nr'] = $result[1];
        }
        if(empty($order['billing_street_name'])){
            $result = CreateContent::migrateAddress($order['billing_address_1']);
            $order['billing_street_name'] = $result[0];
            $order['billing_street_nr'] = $result[1];
        }
        return response()->json(['data'=>$order]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
//            'name' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json(['message'=>'Failure.', 'errors'=>$validator->errors()]);
        }

        return response()->json(['message'=>"It's unfinished"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function delete($id)
    {

        $this->order->query()->findOrFail($id)->delete();

        return response()->json(['message'=>'Success. Product deleted.']);
    }

    public function cancel($id)
    {
        $order = $this->order->query()->with('items')->find($id);

        if(empty($order)) {
            return response()->json(['message'=>'Failure.', 'errors'=>'Order does not exist.'],400);
        }

        if(empty($order->cancellation_deadline)){
            return response()->json(['message'=>'Failure.', 'errors'=>'This should not happen but cancellation deadline does not exist.'],400);
        }

        if(Carbon::now()->gt($order->cancellation_deadline)) {
            return response()->json(['message'=>'Failure.', 'errors'=>'Cancellation deadline has passed.'],400);
        }
        $order->status = $this::CANCELLATION_STATUS;
        $order->save();

        $subscription = $order->subscription;
        if(!empty($subscription)) {
            $subscription->status = $this::CANCELLATION_STATUS;
            $subscription->save();

            dispatch(new SendSubscriptionCancelEmailJob($subscription));
        }

        return response()->json(['data'=>$order]);
    }
}