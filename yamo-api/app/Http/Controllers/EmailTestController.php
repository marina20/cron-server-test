<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 10/30/17
 * Time: 11:01 AM
 */

namespace App\Http\Controllers;

use App\Jobs\SendSubscriptionConfirmationEmailJob;
use App\Jobs\SendOrderConfirmationEmailJob;
use App\Jobs\SendSingleBoxConfirmationEmailJob;
use App\Jobs\SendResetEmailJob;
use App\Jobs\SendVerificationEmailJob;
use App\Jobs\SendDeliveryReminderEmailJob;
use App\Jobs\SendSubscriptionCancelEmailJob;
use App\Jobs\SendNewsletterActivationEmailJob;
use App\Jobs\SendOrderReportSwitzerlandEmailJob;
use App\Jobs\SendOrderReportGermanyEmailJob;
use App\Jobs\SendPaymentFailureEmailJob;
use App\Jobs\SendPaymentFailureCancelEmailJob;
use App\Jobs\SendChangeSubscriptionEmailJob;
use App\Models\Subscription;
use App\Models\User;
use App\Models\Order;
use App\Services\Adyen\RecurringPayment;
use App\Services\CreateAdyenLinkService;
use App\Services\CreateSofortLinkService;
use App\Services\OrderReportService;
use Illuminate\Http\Request;
use Helpers;

class EmailTestController extends Controller
{
    protected $recurring_payment;
    protected $order_report_service;
    public function __construct(OrderReportService $order_report_service,RecurringPayment $recurring_payment)
    {
        $this->order_report_service = $order_report_service;
        $this->recurring_payment = $recurring_payment;
    }
    public function SubscriptionConfirmation(Request $request)
    {
        $subscription = Subscription::find($request->input('subscription_id'));

        dispatch(new SendSubscriptionConfirmationEmailJob($subscription));
    }

    public function OrderConfirmation(Request $request)
    {
        $order = Order::find($request->input('order_id'));

        dispatch(new SendOrderConfirmationEmailJob($order));
    }

    public function SingleBoxConfirmation(Request $request)
    {
        $order = Order::find($request->input('order_id'));

        dispatch(new SendSingleBoxConfirmationEmailJob($order));
    }

    public function ResetPassword(Request $request)
    {
        $user = User::find($request->input('user_id'));
        dispatch(new SendResetEmailJob($user, '123'));
    }

    public function UserConfirmation(Request $request)
    {
        $user = User::find($request->input('user_id'));
        dispatch(new SendVerificationEmailJob($user));
    }

    public function DeliveryReminder(Request $request)
    {
        $order = Order::find($request->input('order_id'));
        dispatch(new SendDeliveryReminderEmailJob($order));
    }

    public function SubscriptionCancel(Request $request)
    {
        $subscription = Subscription::find($request->input('subscription_id'));
        dispatch(new SendSubscriptionCancelEmailJob($subscription));
    }

    public function NewsletterActivation(Request $request)
    {
        $email = $request->input('email');
        $code = $request->input('confirmation_code');
        dispatch(new SendNewsletterActivationEmailJob($email, $code));
    }

    public function MassDeliveryReminder(Request $request)
    {
        $orders = Order::query()
            ->select('orders.*')
            ->leftJoin('subscriptions','subscriptions.order_id','=','orders.id')
            ->whereDate('orders.delivery_date',$request->input('delivery_date'))
            ->whereRaw("orders.status not in ('cancelled')")
//            ->whereRaw("orders.status not in ('cancelled')
//                        and ((subscriptions.id is not null
//                        and subscriptions.status not in ('cancelled'))
//                        or subscriptions.id is null)")
            ->get();

        if(!empty($orders))
        {
            foreach($orders as $order)
            {
                Helpers::set_locale($order->getCountryCode($order->shipping_country));
                dispatch(new SendDeliveryReminderEmailJob($order));
            }
        }
    }

    public function GetAdyenLink(Request $request)
    {
        $adyen_link_service = new CreateAdyenLinkService();
        $order = Order::find($request->input('order_id'));
        return $adyen_link_service->make($order);
    }

    public function TestAdyenSdk(Request $request)
    {
        $order = Order::find($request->input('order_id'));
        return $this->recurring_payment->pay($order);
    }

    public function TestExcelAttachment(Request $request)
    {
        $country_code = ['DE','CH'];
        if($request->has('country_code'))
        {
            $country_code = [$request->input('country_code')];
        }
        $date = date('Y-m-d');
        if($request->has('delivery_date'))
        {
            $date = $request->input('delivery_date');
        }
        foreach($country_code as $code)
        {
            $files = $this->order_report_service->create($code, $date);
            if($code == 'DE' && !empty($files))
            {
                dispatch(new SendOrderReportGermanyEmailJob($files));
            }
            if($code == 'CH' && !empty($files))
            {
                dispatch(new SendOrderReportSwitzerlandEmailJob($files));
            }
            // call send email for appropriate country
        }
//        $this->order_report_service->create($country_code, $date);
        //dispatch(new SendExcelTestEmailJob());
    }

    public function GetSofortLink(Request $request)
    {
        $sofort_link_service = new CreateSofortLinkService();
        $order = Order::find($request->input('order_id'));
        return $sofort_link_service->make($order);
    }

    public function RecurringFailureCancel(Request $request)
    {
        $order = Order::find($request->input('order_id'));

        dispatch(new SendPaymentFailureCancelEmailJob($order));
    }

    public function RecurringFailure(Request $request)
    {
       $order = Order::find($request->input('order_id'));

       dispatch(new SendPaymentFailureEmailJob($order));
    }

    public function SubscriptionChange(Request $request)
    {
        $order = Order::find($request->input('order_id'));

        dispatch(new SendChangeSubscriptionEmailJob($order));
    }
}