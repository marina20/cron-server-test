<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 10/4/17
 * Time: 4:40 PM
 */

namespace App\Http\Controllers;

use App\Traits\ResetsPasswords;
use Validator;

class PasswordController extends Controller
{
    use ResetsPasswords;

    protected $broker;

    public function __construct()
    {
        $this->broker = 'users';
    }
}