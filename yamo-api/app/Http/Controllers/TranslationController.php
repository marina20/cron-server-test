<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/11/17
 * Time: 3:24 PM
 */

namespace App\Http\Controllers;
use \Illuminate\Support\Facades\Lang;
use App\Models\Translation;
use Illuminate\Http\Request;
use Validator;

class TranslationController extends Controller
{
    public function __construct() {
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $translations = [];
        $queriedTranslations = Translation::all();
        foreach($queriedTranslations as $translation){
            if(empty($translations[$translation->language])){
                $translations[$translation->language] = [];
            }
            $translations[$translation->language][] = $translation;
        }
        return response()->json(['data'=>$translations]);
    }

    public function testMethod(Request $request){
        app('translator')->setLocale("de-de");
        echo Translation::trans('HELLOWORLD');
        echo "\n\n".app('translator')->getLocale();
        die();
    }
/*
    public function setLocale(Request $request,$locale){
        app('translator')->setLocale($locale);
        echo "Locale set to ".$locale; die();
    }
*/
}