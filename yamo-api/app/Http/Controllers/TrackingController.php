<?php

namespace App\Http\Controllers;


use App\Models\TrackingWizard;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use Validator;

class TrackingController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request){

        $validator = Validator::make($request->all(), [
            'uuid' => 'required',
            'data' => 'required',
            'step_identifier' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['message'=>'Failure.', 'errors'=>$validator->errors()],400);
        }
        try {
            $uuid = $request->input('uuid');
            $tracking = new TrackingWizard();
            $tracking->uuid = $request->input('uuid');
            $tracking->user_id = $request->input('user_id');
            $tracking->step_identifier = $request->input('step_identifier');
            $tracking->data = $request->input('data');
            $tracking->save();

            return response()->json(['data' => ['uuid' => $uuid]]);
        }
        catch (\Exception $e)
        {
            app('sentry')->captureException($e);
            return response()->json(['message'=>'Failure.', 'errors'=>$e->getMessage()],400);
        }
    }
}
