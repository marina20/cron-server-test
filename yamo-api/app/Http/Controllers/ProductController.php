<?php

namespace App\Http\Controllers;

use App\Http\Resources\BundleLightResource;
use App\Http\Resources\BundleResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductCartResource;
use App\Http\Resources\BundleCartResource;
use App\Models\Box;
use App\Models\Category;
use App\Models\Country;
use App\Models\Region;
use App\Models\DeliveryDate;
use App\Models\DeliveryFrequency;
use App\Models\Order;
use App\Models\Bundle;
use App\Models\OrderItem;
use App\Models\OrderType;
use App\Models\Product;
use App\Models\ReferralUrl;
use App\Models\Translation;
use App\Models\Subscription;
use App\Models\SuggestedBox;
use App\Models\Voucher;
use App\Models\VoucherProduct;
use App\Services\CustomBox\Create;
use App\Services\CustomBox\CreateContent;
use App\Services\CustomBox\Delete;
use App\Services\MFGInvoice\MFGInvoice;
use App\Services\Voucher\VoucherService;
use App\Traits\FormatMoney;
use Auth;
use Carbon\Carbon;
use Helpers;
use Loyalty;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Validator;


class ProductController extends Controller
{
    use FormatMoney;

    protected $create_custom_box_service;
    protected $create_custom_box_content_service;
    protected $delete_custom_box_service;
    protected $mfg_invoice;
    const DEFAULT_POSITION = 9000;
    const DEFAULT_IS_COUPON_APPLICABLE = false;
    const DEFAULT_COUPON_REDUCTION_PERCENTAGE = 20;
    const ERROR_KEY_TECHNICAL_PROBLEMS = 'TECHNICAL_PROBLEMS';

    public function __construct(Product $product
        , Create $create_custom_box_service, CreateContent $create_custom_box_content_service
        , Delete $delete_custom_box_service, MFGInvoice $mfg_invoice)
    {
        $this->product = $product;
        $this->create_custom_box_service = $create_custom_box_service;
        $this->create_custom_box_content_service = $create_custom_box_content_service;
        $this->delete_custom_box_service = $delete_custom_box_service;
        $this->mfg_invoice = $mfg_invoice;
    }

    public function deliveryDatesEndpoint(){
        $now = Carbon::now(Helpers::TIMEZONE_ZURICH);

        $country_id = Country::where('content_code',app('translator')->getLocale())->first()->id;
        $deliveryDates = DeliveryDate::where('cut_off_date','>',$now)->where(['country_id' => $country_id])->where(['disabled' => 0])->get();
        $deliveryDatesArr = [];
        foreach($deliveryDates as $deliveryDate){
            $deliveryDatesArr[] = explode(" ",$deliveryDate->delivery_date)[0];
        }
        return response()->json(['data'=>$deliveryDatesArr]);
    }

    /**
     * Display a listing of the resource where category = breil
     * List is for custom box configuration
     *
     * @param Request $request
     * @return Response
     */
    public function listCustomizeSave(Request $request)
    {
        // use save to update also
        $data = $request->input('items');
        if(empty($data)){
            return response()->json(['error'=>'Input array is not in correct format.'],400);
        }

        if($request->filled('custom_box_id'))
        {
            try
            {
                $box = Box::findOrFail($request->input('custom_box_id'));
            }
            catch (ModelNotFoundException $ex) {
                return response()->json(['error' => 'Box with id:' . $request->input('custom_box_id') . ' does not exist.'], 400);
            }
            $this->delete_custom_box_service->remove_items($box);
        }
        else
        {
            $box = $this->create_custom_box_service->create();
            if(!empty($request->input('bundle_id'))){
                $bundle = Bundle::find($request->input('bundle_id'));
                $box->original_box_id = $bundle->box_id;
                $box->save();
            }
        }

        $this->create_custom_box_content_service->create($box,$data);

        return response()->json(['custom_box_id'=>$box->id]);
    }

    /**
     * Get all products that belong in categories
     * @param array $categories - array of strings which are category name constants
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    private function getProductsByCategory(array $categories, $useVisible=false)
    {
        $data = $this->product->query()
            ->whereHas('categories',function ($query) use ($categories) {
                $query->whereIn('name_key', $categories);
            });
        if($useVisible){
            $country = Helpers::country();
            $region = Region::where('country_id',$country->id)->first();
            $data = $data->whereHas('productRegions',function ($query) use($region) {
                $query->where('product_visible', 1)->where('region_id',$region->id);
            });
        }
        $data = $data->orderBy('position')
            ->get();
        return $data;
    }

    public function bundleContent(Request $request,$id)
    {
        $bundle = Bundle::find($id);
        $products = [];
        foreach($bundle->box->items as $item){
            $products[] = $item->product;
        }
    }

    /**
     * Get all cups and pouches products and transform the output to desired array
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function products(Request $request)
    {
        // get all products with categories cups and pouches into array and sort them by position
        $categories = $this->getFoodCategories(true);
        $country = Helpers::country();
        $region = Region::where(['country_id' => $country->id])->first();
        $data = [];
        foreach($categories as $category){
            $products = Product::where(['category_id' => $category->id])->whereHas('productRegions',function ($query) use($region) {
                $query->where('product_visible', 1)->where('region_id',$region->id);
            })->get();
            if($products->isNotEmpty())
            {
                $processData = [
                    'id' => $category->name_key,
                    'title' => $category->name,
                    'description' => $category->description,
                    'items' => ProductResource::collection($products)
                ];
            }

            if(Category::CATEGORY_BUNDLES_NAME_KEY === $category->name_key)
            {
                $processData = [
                    'id' => $category->name_key,
                    'title' => $category->name,
                    'description' => $category->description,
                    'items' => BundleLightResource::collection(Bundle::getAllVisible())
                ];
            }

            $data[] = $processData;
        }
        return response()->json(['data' => $data]);
    }

    // TODO this method is to be replaced with the one from VoucherService!
    private function getProductPriceDiscounted($product)
    {
        if($product->size===200) {
            //echo $product->size;
            $voucherArray = Voucher::getDiscountedAmount('FIRST_SUBSCRIPTION_200_' . Helpers::country()->iso_alpha_2, $product->price);
        } else {
            $voucherArray = Voucher::getDiscountedAmount('FIRST_SUBSCRIPTION_' . Helpers::country()->iso_alpha_2, $product->price);
        }
        $discountedPrice = $product->price - $voucherArray['discount'];
        return $this->roundMoney($this->formatProductPrice($discountedPrice));
    }

    private function formatProductPrice($price)
    {
        return strval($price);
    }

    public function categories()
    {
        // TODO translate those words..
        $data = [
            [
                'id' => Product::PRODUCT_CATEGORY_BREIL,
                'name' => 'Becher',
                'thumbnail' => Helpers::getImageThumbnail(Product::IMAGE_LINK_BREIL),
                'alt_text' => 'Becher'
            ],
            [
                'id' => Product::PRODUCT_CATEGORY_POUCH,
                'name' => 'Quetschies',
                'thumbnail' => Helpers::getImageThumbnail(Product::IMAGE_LINK_POUCHES),
                'alt_text' => 'Quetschies'
            ],
            [
                'id' => Product::PRODUCT_CATEGORY_ALL,
                'name' => 'Beides',
                'thumbnail' => Helpers::getImageThumbnail(Product::IMAGE_LINK_ALL),
                'alt_text' => 'Beides'
            ]
        ];
        return response()->json(['data' => $data]);
    }

    /**
     * Main categories should be categories displayed at shop
     * @return \Illuminate\Http\JsonResponse
     */
    public function mainCategories()
    {
        // get food categories and order it by position
        $categories = $this->getFoodCategories();

        $data = [];
        foreach ($categories as $category)
        {
            $data[] = [
                'id' => $category->name_key,
                'name' => $category->name,
                'thumbnail' => $category->image_thumbnail,
                'alt_text' => $category->image_alt_tag_text,
                'is_new' => (bool)$category->new
            ];
        }

        return response()->json(['data' => $data]);
    }

    private function getFoodCategories($skipAll=false)
    {
        $query =  Category::whereHas('parent', function($q){
            $q->where('name_key', Category::CATEGORY_FOOD_NAME_KEY);
        });
        if($skipAll){
            $query->where('name_key', '!=', Category::CATEGORY_ALL_NAME_KEY);
        }

        return $query->orderBy('position')->get();
    }

    public function ingredients()
    {
        $data = [
            [
                'id' => Product::PRODUCT_INGREDIENTS_FRUIT,
                'name' => Translation::trans('CONTROLLER.PRODUCT.INGREDIENTS.FRUITS'),
                'thumbnail' => Helpers::getImageThumbnail('fruchte.png'),
                'alt_text' => Translation::trans('CONTROLLER.PRODUCT.INGREDIENTS.FRUITS')
            ],
            [
                'id' => Product::PRODUCT_INGREDIENTS_VEGETABLE,
                'name' => Translation::trans('CONTROLLER.PRODUCT.INGREDIENTS.VEGGIES'),
                'thumbnail' => Helpers::getImageThumbnail('gemuse.png'),
                'alt_text' => Translation::trans('CONTROLLER.PRODUCT.INGREDIENTS.VEGGIES')
            ],
            [
                'id' => 'all',
                'name' => Translation::trans('CONTROLLER.PRODUCT.INGREDIENTS.BOTH'),
                'thumbnail' => Helpers::getImageThumbnail('ingredient_beides.png'),
                'alt_text' => Translation::trans('CONTROLLER.PRODUCT.INGREDIENTS.BOTH')
            ]
        ];
        return response()->json(['data' => $data]);
    }

    /**
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function filteredProducts(Request $request){
        $country = Helpers::country();
        $region = Region::where(['country_id' => $country->id])->first();
        $searchedCategory = Category::where(['name_key' => $request->input('filter')['category']])->first();
        if(Category::CATEGORY_BUNDLES_NAME_KEY === $searchedCategory->name_key)
        {
            $data = [[
                'id' => $searchedCategory->name_key,
                'title' => $searchedCategory->name,
                'description' => $searchedCategory->description,
                'items' => BundleLightResource::collection(Bundle::getAllVisible())
            ]];
            return response()->json(['data' => $data]);
        }

        if(Category::CATEGORY_ALL_NAME_KEY === $searchedCategory->name_key)
        {
            $categories = $this->getFoodCategories(true);
            $bundlesCategory = Category::where(['name_key' => 'bundles'])->first();
            $data = [];
            $data[] = [
                'id' => $bundlesCategory->name_key,
                'title' => $bundlesCategory->name,
                'description' => $bundlesCategory->description,
                'items' => BundleLightResource::collection(Bundle::getAllVisible())
            ];
            foreach($categories as $category){
                if($category->name_key=='bundles'){
                    continue;
                }
                $products = Product::where(['category_id' => $category->id])->whereHas('productRegions',function ($query) use($region) {
                    $query->where('product_visible', 1)->where('region_id',$region->id);
                })->get();
                $data[] = [
                    'id' => $category->name_key,
                    'title' => $category->name,
                    'description' => $category->description,
                    'items' => ProductResource::collection($products)
                ];
            }
            return response()->json(['data' => $data]);
        }
        $products = Product::where(['category_id' => $searchedCategory->id])->whereHas('productRegions',function ($query) use($region) {
            $query->where('product_visible', 1)->where('region_id',$region->id);
        })->get();
        $data = [[
            'id' => $searchedCategory->name_key,
            'title' => $searchedCategory->name,
            'description' => $searchedCategory->description,
            'items' => ProductResource::collection($products)]
        ];
        return response()->json(['data' => $data]);
    }

    /**
     * Return filters with parent category food
     * @return \Illuminate\Http\JsonResponse
     */
    public function filters()
    {
        $data = [];
        $categories = $this->getFoodCategories();
        foreach($categories as $category){
            $data[] = [
                'id' => $category->name_key,
                'title' => $category->name
            ];
        }
        return response()->json(['data' => ['category' => $data]]);
    }

    public function frequency(Request $request){
        $frequencies = DeliveryFrequency::where(['period'=>Subscription::SUBSCRIPTION_DELIVERY_FREQUENCY_PERIOD])->orderBy('id','desc')->get();
        $data = [];
        foreach($frequencies as $key => $frequency){
            $data[$key] = ['id'=>strval($frequency->id),'value'=>strval($frequency->interval),
                'icon'=>$frequency->icon];
        }
        return response()->json(['data' => $data],200);
    }

    private function isNewUserCondition(){
        if(Auth::check() && Auth::user()->isNew()){
            return true;
        }
        return false;
    }

    /**
     * Check with FE if they still use products/default-packages
     * @deprecated
     * @return \Illuminate\Http\JsonResponse
     */
    public function defaultPackages()
    {
        $data = [
            [
                'category'=>'single-box-custom-box',
                'currency'=>'€',
                'name'=>'Deine individuelle Box',
                'original_price'=>'40.00',
                'price'=>'40.00'
            ],[
                'category'=>'subscription-custom-box',
                'currency'=>'€',
                'name'=>'Deine individuelle Box',
                'original_price'=>'40.00',
                'price'=>'28.00'
            ]
        ];

        return response()->json(['data' => $data]);
    }

    public function trackingFilters(Request $request)
    {
        return response()->json();
    }

    /**
     * List all products (cups and pouches)
     * Receive wizard_box_id as custom box id to find correct quantities of product items
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function listCustomizeAllProducts(Request $request)
    {
        $dataCustom = null;
        if($request->filled('wizard_box_id'))
        {
            try
            {
                $box = Box::findOrFail($request->input('wizard_box_id'));
                $dataCustom = $this->product->transformItems($box->items);
            }
            catch (ModelNotFoundException $ex)
            {
                $dataCustom = null;
            }
        }
        else if($request->filled('product_id'))
        {
            try
            {
                $product = Product::findOrFail($request->input('product_id'));
                $dataCustom = $product->items;
            }
            catch (ModelNotFoundException $ex)
            {
                $dataCustom = null;
            }
        }
        $all_products = $this->getProductsByCategory([Product::PRODUCT_CATEGORY_BREIL, Product::PRODUCT_CATEGORY_POUCH],true);

        if(empty($all_products))
            return [];

        $data = $this->product->transformItemsEmptyAllSecond($all_products,$dataCustom);
        foreach ($data as &$item) {
            $product = Product::find($item['id']);
            $item['price'] = $this->getProductPriceDiscounted($product);
            $item['currency'] = Helpers::currency();
        }

        return response()->json(['data'=>$data]);
    }

    public function getFirstPackageContent(Request $request)
    {
        $wizardBoxId = $request->input("wizard_box_id");
        $notification_message = null;

        if(empty($wizardBoxId)) {
            if(empty($request->input("wizard_baby_birthday"))){
                return response()->json(['data' => []]);
            }
            $diff_in_months = $this->getBabyAge($request->input("wizard_baby_birthday"));
            $months = "4";

            // Warnings
            if($request->input("wizard_ingredients")==Product::PRODUCT_INGREDIENTS_VEGETABLE && $request->input("wizard_category")==Product::PRODUCT_CATEGORY_POUCH){
                $notification_message = Translation::trans("CONTROLLER.PRODUCT.FIRSTPACKAGECONTENT.NOPOUCHESWITHVEGGIESYET");
            }
            if($diff_in_months < 6 && $request->input("wizard_category")==Product::PRODUCT_CATEGORY_POUCH){
                $notification_message = Translation::trans("CONTROLLER.PRODUCT.FIRSTPACKAGECONTENT.POUCHESARERECOMMENDEDWITH6MONTHS");
            }
            // End of warnings

            if ($diff_in_months > 5) {
                $months = "6";
            }

            $suggestedBox = $this->getSuggestedBox($request->input("wizard_category"), $request->input("wizard_ingredients"), $months);
            $products = $this->getSuggestedBoxProductsCollection($suggestedBox->box);
            $wizardBoxId = $suggestedBox->box->id;
        } else {
            $box = Box::findOrFail($wizardBoxId);
            $products = $this->getSuggestedBoxProductsCollection($box);
        }
        $sumPrice = 0;
        foreach ($products as $product) {
            $sumPrice += $product['price']*$product['quantity'];
        }
        $isNewCustomerCondition = $this->isNewUserCondition();

        $discountedPrice = $sumPrice;

        $testphaseAllowed = false;
        if($isNewCustomerCondition){
            $testphaseAllowed = true;
            // @TODO apply new discount first price - Voucher model - getDiscountAmount method
            $discountedPrice = $sumPrice - (($sumPrice/100)*20);
        }
        $currency = Country::where('content_code',app('translator')->getLocale())->first()->currency;

        $responseData = [
            "testphase_allowed" => $testphaseAllowed,
            "products" => $products,
            "wizard_box_id"=>$wizardBoxId,
            'default_product'=>
                [
                    'original_price'=>$this->formatMoney(($sumPrice)/16),
                    'discounted_price'=>$this->formatMoney(($discountedPrice)/16),
                    'currency'=> $currency
                ],
            'notification_message' => $notification_message
        ];

        return response()->json(['data' => $responseData]);
    }

    /**
     * Baby age in months
     * @param $birthday
     * @return int
     */
    private function getBabyAge($birthday)
    {
        return Carbon::createFromFormat('Y-m-d', $birthday)->diffInMonths(Carbon::now());
    }

    /**
     * Find suggested box or return default one
     * @param $wizardCategory
     * @param $wizardIngrediens
     * @param $months
     * @return mixed
     */
    private function getSuggestedBox($wizardCategory, $wizardIngrediens, $months)
    {
        if($months == 4 && ($wizardCategory=='cups'||$wizardCategory=='all')){
            $wizardIngrediens = 'all';
        }
        $suggestedBox = SuggestedBox::where(["product_type" => $wizardCategory, "product_content" => $wizardIngrediens, "children_age" => $months . "+"])->first();

        if (empty($suggestedBox)) {
            $suggestedBox = SuggestedBox::where(["default_box" => 1])->first();
        }
        return $suggestedBox;
    }

    /**
     * Return collection of product objects with additional attribute - quantity
     * @param $suggestedBox
     * @return Collection
     */
    private function getSuggestedBoxProductsCollection($suggestedBox)
    {
        $products = new Collection();
        foreach ($suggestedBox->items as $i => $content) {
            $product = $content->product;
            $product->quantity = $content->quantity;
            $countryCode = Helpers::country()->iso_alpha_2;
            if ($product->size == 200) {
                $result = Voucher::getDiscountedAmount("SUBSCRIPTION_200_" . $countryCode, $product->price);
            } else {
                $result = Voucher::getDiscountedAmount('SUBSCRIPTION_' . $countryCode, $product->price);
            }
            $product = $product->toArray();
            $product['price'] = $this->formatMoney($product['price'] - $result['discount']);
            $products->push($product);
        }
        return $products;
    }

    /**
     * Cart wizard product - specific endpoint to handle data on wizard checkout 2nd step
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cartFunnelProducts(Request $request)
    {
        if (!$request->has('custom_box_id')) {
            return response()->json([
                'error' =>
                    [
                        'message' => Translation::trans('CONTROLLER.PRODUCT.CARTWIZARDPRODUCTS.WIZARDBOXIDISEMPTY'),
                        'key' => self::ERROR_KEY_TECHNICAL_PROBLEMS
                    ]
            ], 400);
        }

        try {
            $user = Auth::user();
            if (empty($user) || empty($user->profile)){
                return response()->json([
                    'error' => [
                        'message' => Translation::trans('CONTROLLER.PRODUCT.CARTWIZARDPRODUCTS.USERISEMPTY'),
                        'key' => self::ERROR_KEY_TECHNICAL_PROBLEMS
                    ]
                ], 404);
            }

            $discount = 0;

            $deliveryDate = Carbon::createFromFormat('Y-m-d', $request->input('delivery_date'));
            $deliveryFrequency = DeliveryFrequency::where('interval', $request->input('delivery_frequency'))->first();

            $babyName = $user->profile->child_name;

            // hardcoded name and short name for testphase and images
            $productName = Translation::trans('CONTROLLER.PRODUCT.CARTWIZARDPRODUCTS.TESTSTAGE');
            $productShortName = Translation::trans('CONTROLLER.PRODUCT.CARTWIZARDPRODUCTS.TESTPRODUCTNAME');
            $defaultPackageImageThumbnail = Helpers::getImageThumbnail(Product::SUBSCRIPTION_BOX_IMAGE_THUMB);
            $singleBoxPackageImageThumbnail = Helpers::getImageThumbnail(Product::SINGLE_BOX_IMAGE_THUMB);

            $calculatedNextDeliveryDate = '';
            if (!empty($deliveryFrequency)) {
                $calculatedNextDeliveryDate = $deliveryDate->copy()->addWeek($deliveryFrequency->interval);
                if (!$user->isNew()) {
                    // TODO translate that!
                    $productName = 'Deine Abo-Box';
                    $productShortName = 'Abo-Box';
                }
            } else {
                // TODO translate that!
                $productName = 'Deine Individuelle Box';
                $productShortName = 'Individuelle Box';
            }

            //$country = Helpers::country();
            $country = Country::where('content_code',app('translator')->getLocale())->first();
            $countryIsoAlpha2 = $country->iso_alpha_2;
            $box = Box::findOrFail($request->input('custom_box_id'));

            $defaultPackagePrice = 0;
            $productItems = [];
            $discountedPrice = 0;
            foreach ($box->items as $item) {
                $defaultPackagePrice += (float)$this->formatMoney($item->product->price) * $item->quantity;
                $productItems[] = [
                    'id' => $item->product->id,
                    'image' => $item->product->image,
                    'image_alt_tag_key' => $item->product->image_alt_tag_key,
                    'image_alt_tag_text' => $item->product->image_alt_tag_text,
                    'image_thumbnail' => $item->product->image_thumbnail,
                    'ingredients' => $item->product->ingredients,
                    'months' => $item->product->month,
                    'name' => $item->product->name,
                    'quantity' => $item->quantity,
                    'weight' => $item->product->weight,
                    'category' => $item->product->topParentCategoryName()
                ];
            }
            $originalPrice = $defaultPackagePrice;

            $packagePrice = null;
            $packageShipping = 0;
            $packageTotalPrice = null;
            $packageDiscount = null;
            $thePriceObject = $defaultPackagePrice;
            $packageTotalPrice = $defaultPackagePrice;

            // check if mfg eligible
            $ipAddress = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $request->ip();
            $order = $this->createOrder($user, $country, $ipAddress, $packageTotalPrice, true);
            $mfgEligible = $this->isMfgEligible($order);
            $orderType = OrderType::where(['type' => "B2C"])->first();
            $sub = null;
            if (!empty($deliveryFrequency)) {
                $sub = Subscription::create(['order_id' => $order->id, 'billing_interval' => $request->input('delivery_frequency')]);
                $order->subscription_id = $sub->id;
                $order->is_subscription = 1;
                $order->save();
            }
            $order->order_type_id = $orderType->id;
            $total_price = $packageTotalPrice;
            $customBoxId = $request->input('custom_box_id');
            $box = Box::find($customBoxId);
            $products = [];
            if(!empty($box->items)) // validation where there will be an error if empty
            {
                foreach ($box->items as $item)
                {
                    $products[] = ['id'=>$item->product->id,'quantity'=>$item->quantity];
                }
            }

            $order = Voucher::temporaryCreateOrderItems($order, $products);
            $defaultPackagePrice = 0;

            foreach ($products as $item) {
                $product = Product::find($item['id']);
                $price = $this->formatMoney($product->price);
                if (!empty($deliveryFrequency)) {
                    if (!Auth::check() || Auth::user()->isNew()) {
                        // TODO do use VoucherService getProductPriceFirstSubscriptionDiscounted
                        if ($product->size == 200) {
                            $result = Voucher::getDiscountedAmount("FIRST_SUBSCRIPTION_200_" . $countryIsoAlpha2, $price);
                        } else {
                            $result = Voucher::getDiscountedAmount('FIRST_SUBSCRIPTION_' . $countryIsoAlpha2, $price);
                        }
                        // $order->order_total += VoucherService::getProductPriceFirstSubscriptionDiscounted($product) * $item['quantity'];
                    } else {
                        // TODO use VoucherService getProductPriceSubscriptionDiscounted
                        if ($product->size == 200) {
                            $result = Voucher::getDiscountedAmount("SUBSCRIPTION_200_" . $countryIsoAlpha2, $price);
                        } else {
                            $result = Voucher::getDiscountedAmount('SUBSCRIPTION_' . $countryIsoAlpha2, $price);
                        }
                        //$order->order_total += VoucherService::getProductPriceSubscriptionDiscounted($product) * $item['quantity'];
                    }
                    // TODO use VoucherService getProductPriceSubscriptionDiscounted
                    if ($product->size == 200) {
                        $nextResult = Voucher::getDiscountedAmount('SUBSCRIPTION_200_' . $countryIsoAlpha2, $price);
                    } else {
                        $nextResult = Voucher::getDiscountedAmount('SUBSCRIPTION_' . $countryIsoAlpha2, $price);
                    }

                    $defaultPackagePrice +=  ($this->formatMoney($price - $nextResult['discount'])) * $item['quantity']; // VoucherService::getProductPriceSubscriptionDiscounted($product) * $item['quantity'];
                    $order->order_total += ($this->formatMoney($price - $result['discount'])) * $item['quantity'];
                } else {
                    $defaultPackagePrice += ($this->formatMoney($price)) * $item['quantity'];
                    $order->order_total += ($this->formatMoney($price)) * $item['quantity'];
                }

            }
            $originalPrice = $defaultPackagePrice;

            $thePriceObject = $order->order_total;
            $total_price = $order->order_total;
            $coupon_applied = false;
            $ref_applied = false;
            $respondedError = null;

            $freeProductItems = [];

            $voucherDiscount = 0;
            $referralDiscount = 0;
            if (!empty($request->input('coupon_code')) && empty($request->input('ref_code'))) {
                $voucherData = Voucher::checkVoucher($request->input('coupon_code'), $order);
                $coupon_applied = $voucherData['success'];

                $freeProducts = Voucher::getFreeItems($request->input('coupon_code'));
                foreach ($freeProducts as $freeProduct)
                {
                    $freeProductItems[] = $this->transformProductToItemForFunnel($freeProduct);
                }

                $order = $voucherData['order'];
                if(!$coupon_applied){
                    $respondedError = "THATSWRONGDUDE";
                }
                //$order->save();
                $voucherDiscount = ((float)$order->discount);
                $total_price = $order->order_total;
            } else if (!empty($request->input('ref_code'))) {
                $result = ReferralUrl::checkReferral($request->toArray(), $order);
                $order = $result['order'];
                $ref_applied = $result['success'];
                $referralDiscount = ((float)$order->discount);
                $total_price = $order->order_total;
            }

            // Check if Loyalty programme could be applied
            $order->user_id = $user->id;
            $order = Loyalty::onlyDiscount($order);

            foreach (Loyalty::getFreeItems($order) as $freeProduct)
            {
                $freeProductItems[] = $this->transformProductToItemForFunnel($freeProduct);
            }

            $discount = ((float)$order->discount);
            $total_price = $order->order_total;

            if ($discount > $thePriceObject) {
                $discount = $thePriceObject;
                $total_price = 0;
            }
            $responseData = [];

            $responseData['additional'] = [
                'baby_name' => $babyName,
                'mfg_eligible' => $mfgEligible,
                'currency' => Helpers::currency(),
                'error' => $respondedError
            ];

            // Wallet amount means price-reduction by the wallet here and is only to be shown on the bill
            $wallet_amount = Auth::user()->profile->wallet_amount_cents;
            if($total_price < Auth::user()->profile->wallet_amount_cents){
                $wallet_amount = (float)$total_price;
                $total_price = "0.00";
            } else {
                $total_price = (float)$total_price - $wallet_amount;
            }

            //$wallet_amount_currency = Auth::user()->profile->wallet_amount_currency;
            //if(empty($wallet_amount_currency)){
            $wallet_amount_currency = Helpers::currency();
            //}

            $responseData['recent_package'] = [
                'id' => null,
                'short_name' => $productShortName,
                'items' => $productItems,
                'original_package_price' => $this->formatMoney($originalPrice),
                'testphase_allowed' => $user->isNew(),
                'coupon_applied' => $coupon_applied,
                'ref_applied' => $ref_applied,
                'delivery_date' => $deliveryDate->format('Y-m-d'),
                'image_thumbnail' => $defaultPackageImageThumbnail,
                'price' => $this->formatMoney((float)$thePriceObject),
                'shipping' => $this->formatMoney($packageShipping),
                'discount' => $this->formatMoney($discount),
                'referral_discount' => $this->formatMoney($referralDiscount),
                'public_discount' => $this->formatMoney($voucherDiscount),
                'loyalty_discount' => $this->formatMoney($order->loyalty_discount),
                'total_price' => $this->formatMoney((float)$total_price),
                'currency' => Helpers::currency(),
                'rewards_wallet' => $this->formatMoney($wallet_amount),
                'rewards_wallet_currency' => $wallet_amount_currency
            ];
            if(!empty($freeProductItems))
            {
                $responseData['recent_package']['free_items'] = $freeProductItems;
            }

            $responseData['shipping_data'] = [
                'address' => $user->profile->getShippingStreet(),
                'postcode' => $user->profile->shipping_postcode,
                'city' => $user->profile->shipping_city
            ];
            if (!empty($deliveryFrequency)) {
                $responseData['next_package'] = [
                    'name' => $productName,
                    'short_name' => $productShortName,
                    'delivery_date' => $deliveryDate->format('Y-m-d'),
                    'price' => $this->formatMoney((float)$defaultPackagePrice),
                    'shipping' => $this->formatMoney($packageShipping),
                    'image_thumbnail' => $singleBoxPackageImageThumbnail,
                    'total_price' => $this->formatMoney((float)$packageTotalPrice),
                ];
                $responseData['next_package']['delivery_date'] = $calculatedNextDeliveryDate->format('Y-m-d');
                $responseData['next_package']['delivery_frequency'] = [
                    'interval' => $deliveryFrequency->interval,
                    'icon_name' => $deliveryFrequency->icon,
                ];

            }
            foreach ($order->items as $item){
                $item->delete();
            }
            $order->delete();
            if(!empty($sub)){
                $sub->delete();
            }
            return response()->json(['data' => $responseData]);

        } catch (\Throwable $ex) {
            if(isset($order) && get_class($order)=='App\Models\Order'){
                foreach ($order->items as $item){
                    $item->delete();
                }
                if(!empty($sub)){
                    $sub->delete();
                }
                $order->delete();
            }
            app('sentry')->captureException($ex);
            throw $ex;
        }
    }

    private function transformProductToItemForFunnel(VoucherProduct $voucherProduct)
    {
        return [
            'id' => $voucherProduct->product->id,
            'image' => $voucherProduct->product->image,
            'image_alt_tag_key' => $voucherProduct->product->image_alt_tag_key,
            'image_alt_tag_text' => $voucherProduct->product->image_alt_tag_text,
            'image_thumbnail' => $voucherProduct->product->image_thumbnail,
            'ingredients' => $voucherProduct->product->ingredients,
            'months' => $voucherProduct->product->month,
            'name' => $voucherProduct->product->name,
            'quantity' => $voucherProduct->qty,
            'weight' => $voucherProduct->product->weight,
            'category' => $voucherProduct->product->topParentCategoryName()
        ];
    }

    /**
     * create an order object with all only neccessary information to check if it is mfg eligible
     * do not save the order yet
     * @param $order
     * @return bool
     */
    private function isMfgEligible(Order $order)
    {
        $order->payment_method_id = Order::PAYMENT_METHOD_INVOICE;
        $order->payment_method_title = Order::PAYMENT_METHOD_INVOICE_DE;
        $order->country = Country::where(['content_code' => Helpers::get_locale()])->first();
        return $this->mfg_invoice->checkCardRequest($order);
    }

    /**
     * @param $user
     * @param $country
     * @param $ipAddress
     * @param $packageTotalPrice
     * @return Order
     */
    private function createOrder($user, $country, $ipAddress, $packageTotalPrice, $notSave=false){
        $order = new Order();
        $order->notSave = $notSave;
        // pls not do this :((
        // $order->id = $user->id . Carbon::now()->timestamp;
        $order->customer_ip_address = $ipAddress;
        $order->shipping_title = $user->profile->shipping_title;
        $order->billing_first_name = $user->profile->billing_first_name;
        $order->billing_last_name = $user->profile->billing_last_name;
        $order->country_id = $country->id;
        $order->billing_street_name = $user->profile->billing_street_name;

        $order->billing_street_nr = $user->profile->billing_street_nr;
        $order->billing_city = $user->profile->billing_city;
        $order->billing_postcode = $user->profile->billing_postcode;
        $order->save(); // that save is a temporary solution

        $order->country = $country;
        $order->billing_email = $user->profile->billing_email;
        $order->customer_birthday = $user->profile->customer_birthday;
        $order->order_total = $packageTotalPrice;
        $order->public_discount = 0;
        $order->loyalty_discount = 0;
        $order->currency =  Helpers::currency();

        $order->billing_phone = $user->profile->billing_phone; //TODO: We should eventually use a real mobile number or ask if they can take it off.
        return $order;
    }

    /** endpoint: /public/products/cart route returns products or bundle based on given items array
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cart(Request $request)
    {
        $products = [];
        $totalPrice = 0;
        $discountedPrice = 0;
        $subscriptionSystemDiscount = 0;
        if(!empty($request->input('bundle_id')))
        {
            $bundle = Bundle::find($request->input('bundle_id'));
            if(empty($bundle))
                return $this->resultNotFoundError();
            $result = VoucherService::getProductPriceFirstOrRegularSubscriptionDiscountedAndTotal($bundle->box->items);
            // price with applied system and public discount
            $discountedPrice = $result['discounted'];
            $totalPrice = $result['total'];
            $subscriptionSystemDiscount = $totalPrice - $discountedPrice;
            $products[] = new BundleResource($bundle);
            $discountedPrice = $result['discounted'];
            $totalPrice = $result['total'];

        }
        else
        {
            foreach ($request->input('items') as $item) {
                $product = Product::find($item['id']);
                if(empty($product))
                    return $this->resultNotFoundError();
                $result = VoucherService::getProductPriceFirstOrRegularSubscriptionDiscounted($product);
                $product->discounted_price = $this->formatMoney($result);
                $product->quantity = $item['quantity'];
                $discountedPrice += ($this->formatMoney($result)) * $item['quantity'];
                $totalPrice += (float)$this->formatMoney($product->price) * $item['quantity'];
                $products[] = new ProductCartResource($product);

            }
        }

        $order = new Order;
        $order->notSave = true;
        $discount = $totalPrice - $discountedPrice;
        $totalPriceSingle = $totalPrice;
        $singleDiscount = 0;
        $subscriptionDiscount = $totalPrice - $discountedPrice;
        $totalPriceSubscription = $totalPrice - $subscriptionDiscount;
        $order->order_total = $discountedPrice;
        $order->system_discount = $discount;
        $order->discount = $discount;
        $orderSingle = new Order;
        $orderSingle->notSave = true;
        $orderSingle->order_total = $discountedPrice;
        $orderSingle->system_discount = 0;
        $orderSingle->discount = 0;
        $voucherDiscountSingle = 0;
        $voucherDiscountSubscription = 0;
        $referralDiscount = 0;
//        $discountedPrice -= $publicDiscount;
        if (!empty($request->input('coupon_code')) && empty($request->input('ref_code'))) {
            $voucherDiscountSubscription = Voucher::getDiscountedAmount($request->input('coupon_code'),$discountedPrice)['discount'];
            $voucherDiscountSingle = Voucher::getDiscountedAmount($request->input('coupon_code'),$totalPrice)['discount'];
            $subscriptionDiscount += $voucherDiscountSubscription;
            $singleDiscount = $voucherDiscountSingle;
            $totalPriceSingle = $totalPrice - $voucherDiscountSingle;
            $totalPriceSubscription = $discountedPrice - $voucherDiscountSubscription;
        } else if (!empty($request->input('ref_code')) && (!Auth::check() || Auth::user()->isNew())) {
            $result = ReferralUrl::checkReferral($request->toArray(), $order);
            $order = $result['order'];
            $ref_applied = $result['success'];
            $referralDiscount = ((float)$order->discount);
            $totalPriceSingle = $totalPrice - $order->discount;
            $totalPriceSubscription = $discountedPrice - $order->discount;
            $subscriptionDiscount += $referralDiscount;
            $singleDiscount = $referralDiscount;
        }
        $singleWalletDiscount = 0;
        $subscriptionWalletDiscount = 0;
        if(Auth::check()){
            if(Auth::user()->profile->wallet_amount_cents > $totalPriceSingle){
                $singleWalletDiscount = $totalPriceSingle;
                $singleDiscount = $totalPrice;
                $totalPriceSingle = '0.00';
            } else {
                $singleWalletDiscount = Auth::user()->profile->wallet_amount_cents;
                $singleDiscount += Auth::user()->profile->wallet_amount_cents;
                $totalPriceSingle = $totalPriceSingle - Auth::user()->profile->wallet_amount_cents;
            }

            if(Auth::user()->profile->wallet_amount_cents > $totalPriceSubscription){
                $subscriptionWalletDiscount = $totalPriceSubscription;
                $subscriptionDiscount = $totalPrice;
                $totalPriceSubscription = '0.00';
            } else {
                $subscriptionWalletDiscount = Auth::user()->profile->wallet_amount_cents;
                $subscriptionDiscount += Auth::user()->profile->wallet_amount_cents;
                $totalPriceSubscription = $totalPriceSubscription - Auth::user()->profile->wallet_amount_cents;
            }
        }
        $packages = [
            [
                'category' => Product::PRODUCT_CATEGORY_SINGLE_BOX_CUSTOM,
                'currency' => Helpers::currency(),
                'name' => 'Deine individuelle Box',
                'original_price' => $this->formatMoney($totalPrice),
                'price' => $this->formatMoney($totalPriceSingle),
                'discount' => $this->formatMoney($singleDiscount),
                'system_discount' => '0.00',
                'wallet_discount' => $this->formatMoney($singleWalletDiscount),
                'public_discount' => $this->formatMoney($voucherDiscountSingle),
                'referral_discount' => $this->formatMoney($referralDiscount)
            ],
            [
                'category' => Product::PRODUCT_CATEGORY_CUSTOM,
                'currency' => Helpers::currency(),
                'name' => 'Deine individuelle Box',
                'original_price' => $this->formatMoney($totalPrice),
                'price' => $this->formatMoney($totalPriceSubscription),
                'discount' => $this->formatMoney($subscriptionDiscount),
                'system_discount' => $this->formatMoney($subscriptionSystemDiscount),
                'wallet_discount' => $this->formatMoney($subscriptionWalletDiscount),
                'public_discount' => $this->formatMoney($voucherDiscountSubscription),
                'referral_discount' => $this->formatMoney($referralDiscount)
            ]
        ];

        return response()->json(['data'=>['items'=>$products,'packages'=>$packages]]);
    }

    /**
     * Error response when record has not been found
     * @return \Illuminate\Http\JsonResponse
     */
    private function resultNotFoundError()
    {
        return response()->json([
            'error' =>
                [
                    'message' => Translation::trans('ERROR.NO_RESULTS_FOUND'),
                    'key' => self::ERROR_KEY_TECHNICAL_PROBLEMS
                ]
        ], 400);
    }

    /**
     * Check if cart items array has bundle category anywhere
     * @param array $items
     * @return bool
     */
    private function hasBundleCategory(array $items)
    {
        return (false !== array_search(Product::PRODUCT_CATEGORY_BUNDLE, array_column($items, 'category'))) ? true : false;
    }

    /**
     * Get bundle
     * @param array $items
     * @return mixed
     */
    private function getBundleFromCartItemArray(array $items)
    {
        if(empty($items))
            return null;

        $id = null;

        foreach ($items as $item)
        {
            if(isset($item['category']) && Product::PRODUCT_CATEGORY_BUNDLE == $item['category'])
                $id = $item['id'];
        }
        if(empty($id))
            return null;

        try
        {
            return Bundle::find($id);
        }
        catch (ModelNotFoundException $mex)
        {
            app('sentry')->captureException($mex);
        }
        catch (\Throwable $e)
        {
            app('sentry')->captureException($e);
        }
        return null;
    }

    /**
     * Get Bundle resource for endpoint: /public/bundles/{nameKey}
     * @param $nameKey
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBundle($nameKey)
    {
        $bundle = Bundle::where('name_key',$nameKey)->first();

        if(empty($bundle))
            return response()->json([
                'error' =>
                    [
                        'message' => Translation::trans('ERROR.NO_RESULTS_FOUND'),
                        'key' => self::ERROR_KEY_TECHNICAL_PROBLEMS
                    ]
            ], 400);

        return response()->json(['data'=> new BundleResource($bundle)]);
    }

}
