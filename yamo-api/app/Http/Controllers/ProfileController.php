<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/11/17
 * Time: 3:24 PM
 */

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Postcode;
use App\Models\Profile;
use App\Models\NewsletterSignup;
use App\Services\CustomBox\CreateContent;
use Illuminate\Http\Request;
use Validator;

class ProfileController extends Controller
{
    public function __construct(Profile $profile) {
        $this->profile = $profile;
    }

    private function checkNewsletterAccept($profile){
        $newsletterSignUp = NewsletterSignup::where('email','=',$profile->user->email)->first();
        /*if (!empty($newsletterSignUp) && $newsletterSignUp->confirmed != true) {
            $profile->accepts_newsletter = false;
        }*/
        if(!empty($newsletterSignUp)){
            $profile->accepts_newsletter = $newsletterSignUp->confirmed;
        }

        return $profile;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $profile = $this->profile->query()->find($id);

        $profile->first_name = $request->input('first_name',$profile->first_name);
        $profile->last_name = $request->input('last_name',$profile->last_name);
        $profile->gender = $request->input('gender',$profile->gender);
        $profile->child_birthday = $request->input('child_birthday',$profile->child_birthday);
        $profile->shipping_title = $request->input('shipping_title',$profile->shipping_title);
        $profile->shipping_first_name = $request->input('shipping_first_name',$profile->shipping_first_name);
        $profile->shipping_last_name = $request->input('shipping_last_name',$profile->shipping_last_name);
        $profile->shipping_company = $request->input('shipping_company',$profile->shipping_company);

        $profile->shipping_country = $request->input('shipping_country',$profile->shipping_country);
        $profile->shipping_email = $request->input('shipping_email',$profile->shipping_email);
        $profile->shipping_phone = $request->input('shipping_phone',$profile->shipping_phone);
        $profile->shipping_state = $request->input('shipping_state',$profile->shipping_state);
        $profile->billing_title = $request->input('billing_title',$profile->billing_title);
        $profile->billing_first_name = $request->input('billing_first_name',$profile->billing_first_name);
        $profile->billing_last_name = $request->input('billing_last_name',$profile->billing_last_name);
        $profile->billing_company = $request->input('billing_company',$profile->billing_company);

        $profile->shipping_street_name = $request->input('shipping_street_name',$profile->shipping_street_name);
        $profile->shipping_street_nr = $request->input('shipping_street_nr',$profile->shipping_street_nr);

        $profile->billing_street_name = $request->input('billing_street_name',$profile->billing_street_name);
        $profile->billing_street_nr = $request->input('billing_street_nr',$profile->billing_street_nr);

        $profile->billing_country = $request->input('billing_country',$profile->billing_country);
        $profile->billing_email = $request->input('billing_email',$profile->billing_email);
        $profile->billing_phone = $request->input('billing_phone',$profile->billing_phone);
        $profile->billing_state = $request->input('billing_state',$profile->billing_state);
        $profile->payment_method = $request->input('payment_method',$profile->payment_method);
        if(!empty($request->input('billing_postcode_id'))){
            $postcode = Postcode::where(['id' => $request->input('billing_postcode_id')])->first();
            $profile->billing_city = $postcode->city;
            $profile->billing_postcode = $postcode->postcode;
        }
        if(!empty($request->input('shipping_postcode_id'))){
            $postcode = Postcode::where(['id' => $request->input('shipping_postcode_id')])->first();
            $profile->shipping_city = $postcode->city;
            $profile->shipping_postcode = $postcode->postcode;
        }
        if(!empty($request->input('shipping_street_nr_checkbox'))){
            $profile['shipping_street_nr'] = 0;
        }
        if(!empty($request->input('billing_street_nr_checkbox'))){
            $profile['billing_street_nr'] = 0;
        }
        $profile->save();
        $profile = $this->checkNewsletterAccept($profile);
        $profile = $profile->toArray();
        $country = Country::where('content_code',app('translator')->getLocale())->first();
        $postcode = Postcode::where(['postcode' => $profile['billing_postcode']])->where(["country" => $country->id])->where(["city" => $profile['billing_city']])->first();
        $profile['billing_zip_city'] = ['id' => 0, 'zip_city' => ''];
        if(!empty($postcode)){
            $profile['billing_zip_city'] = ['id' => $postcode->id, 'zip_city' => $postcode->postcode.", ".$postcode->city];
        }
        $postcode = Postcode::where(['postcode' => $profile['shipping_postcode']])->where(["city" => $profile['shipping_city']])->where(["country" => $country->id])->first();
        $profile['shipping_zip_city'] = ['id' => 0, 'zip_city' => ''];
        if(!empty($postcode)){
            $profile['shipping_zip_city'] = ['id' => $postcode->id, 'zip_city' => $postcode->postcode.", ".$postcode->city];
        }
        $profile['billing_street_nr_checkbox'] = false;
        if(empty($profile['billing_street_nr'])){
            $profile['billing_street_nr_checkbox'] = true;
            $profile['billing_street_nr'] = '';
        }
        $profile['shipping_street_nr_checkbox'] = false;
        if(empty($profile['shipping_street_nr'])){
            $profile['shipping_street_nr_checkbox'] = true;
            $profile['shipping_street_nr'] = '';
        }

        if(empty($profile['shipping_street_name'])){
            $result = CreateContent::migrateAddress($profile['shipping_address_1']);
            $profile['shipping_street_name'] = $result[0];
            $profile['shipping_street_nr'] = $result[1];
        }
        if(empty($profile['billing_street_name'])){
            $result = CreateContent::migrateAddress($profile['billing_address_1']);
            $profile['billing_street_name'] = $result[0];
            $profile['billing_street_nr'] = $result[1];
        }

        return response()->json(['message'=>'Success. Profile updated.','data'=>$profile]);
    }
}