<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/11/17
 * Time: 3:24 PM
 */

namespace App\Http\Controllers;

use App\Models\Testimonial;
use Illuminate\Http\Request;
use Validator;

class TestimonialController extends Controller
{
    public function __construct(Testimonial $testimonial) {
        $this->testimonial = $testimonial;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $testimonials = $this->testimonial->query()->get();
        return response()->json(['data'=>$testimonials]);
    }
}