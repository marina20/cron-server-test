<?php

namespace App\Http\Controllers;

use App\Models\NewsletterSignup;
use App\Models\User;
use App\Jobs\SendNewsletterActivationEmailJob;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Validator;
use Auth;

class NewsletterController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);
        if ($validator->fails()) {
            return response()->json(['message'=>'Failure.', 'errors'=>$validator->errors()],400);
        }

        $confirmation_code = str_random(30);

        $signup = NewsletterSignup::whereEmail($request->input('email'))->first();

        if(empty($signup)) {
            $signup = new NewsletterSignup();
            $signup->email = $request->input('email');
        }

        $signup->confirmation_code = $confirmation_code;
        $signup->save();

        dispatch(new SendNewsletterActivationEmailJob($signup->email, $signup->confirmation_code));

        return response()->json('success',200);
    }

    /**
     * @param $confirmation_code
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws InvalidConfirmationCodeException
     */
    public function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
//            throw new InvalidConfirmationCodeException;
            return response()->json(['message'=>'Failure.', 'errors'=>'Confirmation code is not provided.'],400);
        }

        $signup = NewsletterSignup::whereConfirmationCode($confirmation_code)->first();

        if ( ! $signup)
        {
//            throw new InvalidConfirmationCodeException;
            return response()->json(['message'=>'Failure.', 'errors'=>'Invalid confirmation code.'],400);
        }

        $user = User::whereEmail($signup->email)->first();
        $signup->confirmed = 1;
        $signup->confirmation_code = null;
        $signup->save();

        $client = new Client();
        $client->post('https://api.sendgrid.com/v3/contactdb/recipients', [
            'headers' =>[
                'Authorization'=>'Bearer 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',
                'Content-Type'=>'application/json'
            ],
            'json' => [
                ['email'=>$signup->email]
            ]
        ]);

        return response('user confirmed!');
    }

    public function add()
    {
        $user = Auth::user();
        $confirmation_code = str_random(30);

        $signup = NewsletterSignup::whereEmail($user->email)->first();

        if(empty($signup)) {
            $signup = new NewsletterSignup();
            $signup->email = $user->email;
        }

        $signup->confirmation_code = $confirmation_code;
        $signup->save();

        dispatch(new SendNewsletterActivationEmailJob($signup->email, $signup->confirmation_code));

        return response()->json('success',200);
    }

	/**
	 * Will move the email to the unsubscribed group in Sendgrid.
	 * @param $email
	 * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
	 */
    public function remove()
    {
        $user = Auth::user();
        $client = new Client();
        $res = $client->post('https://api.sendgrid.com/v3/contactdb/recipients/search', [
            'headers' =>[
                'Authorization'=>'Bearer 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',
                'Content-Type'=>'application/json'
            ],
            'json' => [
                "conditions"=>[[
                    "and_or"=>"",
                    "field"=>"email",
                    "value"=>$user->email,
                    "operator"=>"eq"
                    ]
                ]
            ]
        ]);
        $customer_id = null;
        $result = json_decode($res->getBody()->getContents());
        if($result->recipient_count == 0)
        {
            return response('email not found!',404);
        }
        if(isset($result->recipients[0]) && isset($result->recipients[0]->id))
        {
            $customer_id = $result->recipients[0]->id;
        }
        if(!empty($customer_id))
        {
            $res_remove = $client->post('https://api.sendgrid.com/v3/asm/suppressions/global', [
                'headers' =>[
                    'Authorization'=>'Bearer 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',
                    'Content-Type'=>'application/json'
                ],
	            'json' => [
	            	"recipient_emails" => [$result->recipients[0]->email
		            ]
	            ]
            ]);

            if($res_remove->getStatusCode() == 201)
            {
                $signup = NewsletterSignup::whereEmail($user->email)->first();
                $signup->confirmed = false;
                $signup->save();
                return response('email removed!', 200);
            }
        }
        return response('action failed!',404);
    }
}