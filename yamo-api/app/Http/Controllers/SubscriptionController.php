<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 9/21/17
 * Time: 1:03 PM
 */

namespace App\Http\Controllers;

use App\Models\ReferralUrl;
use App\Models\Subscription;
use App\Models\Order;
use App\Models\Country;
use App\Models\Product;
use App\Models\Box;
use App\Jobs\SendSubscriptionCancelEmailJob;
use App\Models\Translation;
use App\Services\CustomBox\CreateContent;
use App\Services\FindNextDeliveryDate;
use App\Services\SetNextDeliveryDate;
use App\Services\MyAccount\SubscriptionDataTransformer;
use App\Services\MyAccount\SubscriptionCustomBoxSave;
use App\Services\CancelSubscriptionService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Carbon;
use Auth;
use DB;

class SubscriptionController extends Controller
{
    protected $subscription;
    protected $my_account_subscription_data_transformer;
    protected $my_account_subscription_custom_box_save;
    protected $cancel_subscription_service;


    public function __construct(Subscription $subscription,
                                SubscriptionDataTransformer $my_account_subscription_data_transformer,
                                SubscriptionCustomBoxSave $my_account_subscription_custom_box_save,
                                CancelSubscriptionService $cancel_subscription_service)
    {
        $this->subscription = $subscription;
        $this->my_account_subscription_data_transformer = $my_account_subscription_data_transformer;
        $this->my_account_subscription_custom_box_save = $my_account_subscription_custom_box_save;
        $this->cancel_subscription_service = $cancel_subscription_service;
    }

    /**
     * Cancel subscription
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel($id)
    {
        try
        {
            $subscription = $this->subscription->with('order')->with('user')->with('product')->findOrFail($id);
        }
        catch (ModelNotFoundException $ex)
        {
            return $this->errorResponse('Subscription does not exist', Translation::trans('ERROR.MY_ACCOUNT.SUBSCRIPTION_NOT_FOUND'), 404);
        }

        if(empty($subscription->order)) {
            return $this->errorResponse('Order does not exist.', Translation::trans('ERROR.MY_ACCOUNT.ORDER_NOT_FOUND'), 404);
        }

        if(empty($subscription->user)) {
            return $this->errorResponse('User does not exist.', Translation::trans('ERROR.USER_NOT_FOUND'), 404);
        }

        if($subscription->status == Subscription::STATUS_CANCELLED) {
            return $this->errorResponse('Subscription is already cancelled.', Translation::trans('ERROR.MY_ACCOUNT.SUBSCRIPTION_ALREADY_CANCELLED'), 400); // need to change translate_msg
        }

        $this->cancel_subscription_service->cancel($subscription);

        $oldestOrder = Order::where([["subscription_id","=",$subscription->id],["paid_date","!=",'']])->whereNotNull('paid_date')->whereNull('parent_id')->orderBy("created_at","asc")->first();
        if(empty($oldestOrder)){
            $subscription->cancelled_at = null;
        } else {
            $subscription->cancelled_at = DB::raw('now()');
        }
        $subscription->save();
        dispatch(new SendSubscriptionCancelEmailJob($subscription));
        if(!empty($subscription->order->cancellation_deadline)
            && !Carbon::now()->gt($subscription->order->cancellation_deadline)
            && $subscription->order->status != Order::STATUS_PAYED){
            $order = $subscription->order;
            $order->status = Order::STATUS_CANCELLED;
            $order = ReferralUrl::revertWallet($order);
            $order->save();
        }

        return response()->json(['data'=>['id'=>$subscription->id]]);
    }

    /**
     * Obsolete - we will rewrite this one soon
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function pause($id)
    {
        $subscription = $this->subscription->query()->with('order')->with('product')->find($id);

        if(empty($subscription)) {
            return $this->errorResponse('Subscription does not exist', Translation::trans('ERROR.MY_ACCOUNT.SUBSCRIPTION_NOT_FOUND'), 400);
        }

        if(empty($subscription->order)) {
            return $this->errorResponse('Order does not exist', Translation::trans('ERROR.MY_ACCOUNT.ORDER_NOT_FOUND'), 400);

        }

        if(empty($subscription->order->cancellation_deadline)){
            return $this->errorResponse('This should not happen but cancellation deadline does not exist.', Translation::trans('ERROR.TECHNICAL_PROBLEMS'), 400); // for this response we put technical problems error, since this is on our side

        }

        if(Carbon::now()->gt($subscription->order->cancellation_deadline)) {
            $message = 'Order cancellation deadline has passed. You cannot cancel current order. Subscription is paused.';
        } else {
            $order = $subscription->order;
            $order->status = Subscription::STATUS_CANCELLED;
            $order->save();
        }
        $subscription->status = Subscription::STATUS_PAUSED;
        $subscription->save();
        $data = [
            'data'=>$this->my_account_subscription_data_transformer->transform($subscription)
        ];
        if(isset($message)) {
            $data['message'] = $message;
        }
        return response()->json($data);
    }

    /**
     * Change frequency for delivery
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function frequency(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'delivery_frequency' => 'required|numeric|in:'.implode(',',Subscription::ENABLED_DELIVERY_FREQUENCIES)
        ]);

        if($validator->fails()) {
            return $this->errorResponse($validator->errors(), Translation::trans('ERROR.INVALID_VALUE'), 400);
        }

        $subscription = $this->subscription->query()->with('order')->with('product')->find($id);

        if(empty($subscription)) {
            return $this->errorResponse('Subscription does not exist.', Translation::trans('ERROR.MY_ACCOUNT.SUBSCRIPTION_NOT_FOUND'), 400);
        }
        $subscription->delivery_frequency = $request->input('delivery_frequency');
        $subscription->billing_interval = $request->input('delivery_frequency');
        $subscription->save();

        return response()->json(['data'=>$this->my_account_subscription_data_transformer->single($subscription)]);
    }

    /**
     * List subscriptions for logged in user with only specific data needed for my account pages
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function myAccountSubscriptions()
    {
        $country = Country::where('content_code',app('translator')->getLocale())->first();
        $data = $this->subscription->query()->with('order')->with('product')
            ->where('user_id',Auth::user()->id)
            ->whereHas('order',function($query) use ($country)
            {
                $query->where('orders.country_id',$country->id);
            })
            ->where('status','!=', Subscription::STATUS_CANCELLED)
            ->get();

        return response()->json(['data'=>$this->my_account_subscription_data_transformer->transform($data)]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function getNextDeliveryDate($id)
    {
        $user = Auth::user();

        $find_next_delivery_date = new FindNextDeliveryDate();

        $next_date = $find_next_delivery_date->next($user,$id);

        if(empty($next_date))
        {
            return $this->errorResponse('Subscription does not exist.', Translation::trans('ERROR.MY_ACCOUNT.SUBSCRIPTION_NOT_FOUND'), 404);
        }

        return response($next_date);
    }

    /**
     * Change delivery date of active order in subscription
     *
     * @param Request $request
     * @param $id - Subscription id
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function setNextDeliveryDate(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'delivery_date' => 'required|date'
        ]);

        if($validator->fails()) {
            return $this->errorResponse('Delivery date is not in valid format.', Translation::trans('ERROR.MY_ACCOUNT.NOT_VALID_FORMAT'), 400);
        }

        try
        {
            $subscription = Subscription::with('product')->findOrFail($id);

            if ($subscription->order->cancellation_deadline->lt(Carbon::now())) {
                return $this->errorResponse('Order delivery date can not be changed after cancellation deadline.', Translation::trans('ERROR.MY_ACCOUNT.TESTBOX_CANCELLATION_DEADLINE_PASSED'), 400);
            }

            $delivery_date = Carbon::createFromFormat('Y-m-d H:i:s',$request->input('delivery_date'));

            if($delivery_date->lt(Carbon::now())) {
                return $this->errorResponse('Delivery date is not in future.', Translation::trans('ERROR.MY_ACCOUNT.NOT_VALID_DATE'), 400);
            }

            $set_new_delivery_date = new SetNextDeliveryDate();
            $res = $set_new_delivery_date->set($subscription, $delivery_date);

            if(!$res) {
                return $this->errorResponse('Pause is not accepted.', Translation::trans('ERROR.MY_ACCOUNT.REFUSED'), 500);
            }

            return response()->json(['data'=>$this->my_account_subscription_data_transformer->single($subscription)]);

        }
        catch(ModelNotFoundException $ex)
        {
            return $this->errorResponse('Subscription was not found.', Translation::trans('ERROR.MY_ACCOUNT.SUBSCRIPTION_NOT_FOUND'), 404);
        }
        catch (\Exception $ex){
            app('sentry')->captureException($ex);
            return $this->errorResponse($ex->getMessage(), 'ERROR.TECHNICAL_PROBLEMS', 400);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function myAccountBoxList(Request $request,$id)
    {
        $dataCustom = null;
        try
        {
            $subscription = Subscription::findOrFail($id);
            $product = new Product();
            if(!is_null($subscription->order->custom_box_id))
            {
                $box = Box::findOrFail($subscription->order->custom_box_id);
                $dataCustom = $product->transformItems($box->items);
            }
            else
            {
                $tempArray = [];
                foreach ($subscription->order->items as $item) {
                    $tempArray[] = $item->product;
                }
                $dataCustom = $tempArray;
            }
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json([],404);
        }

        $all_products = Product::query()
            ->whereHas('categories',function ($query) {
                $query->whereIn('name_key', [Product::PRODUCT_CATEGORY_BREIL, Product::PRODUCT_CATEGORY_POUCH]);
            })
            ->orderBy('position')->get();

        if(empty($all_products))
            return [];

        $data = $product->transformItemsEmptyAllSecond($all_products,$dataCustom);
        return response()->json(['data'=>$data]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function myAccountBoxSave(Request $request, $id)
    {
        // use save to update also
        $data = $request->input('items');
        if(empty($data)){
            return $this->errorResponse('Input array is not in correct format.', Translation::trans('ERROR.INVALID_VALUE'), 404);
        }
        foreach ($data as $input_element) {
            if(!is_array($input_element)){
                return $this->errorResponse('Input array is not in correct format.', Translation::trans('ERROR.INVALID_VALUE'), 404);
            }

            if(!array_key_exists(CreateContent::ITEM_INPUT_ARRAY_KEY_ID, $input_element)){
                return $this->errorResponse(CreateContent::ITEM_INPUT_ARRAY_KEY_ID. ' input element is not present in array.', Translation::trans('ERROR.INVALID_VALUE'), 400);
            }

            if(!array_key_exists(CreateContent::ITEM_INPUT_ARRAY_KEY_QUANTITY, $input_element)){
                return $this->errorResponse(CreateContent::ITEM_INPUT_ARRAY_KEY_QUANTITY. ' input element is not present in array.', Translation::trans('ERROR.INVALID_VALUE'), 400);
            }
        }

        try
        {
            $subscription = Subscription::findOrFail($id);

            if (Order::STATUS_PAYED === $subscription->status) {
                return $this->errorResponse('Your last box has already been paid. You can not change the content.', Translation::trans('ERROR.CUSTOMIZE_BOX_CANCELLATION_ALREADY_PAYED'), 400);
            }

            if ($subscription->order->cancellation_deadline->lt(Carbon::now())) {
                return $this->errorResponse('Changing the products in your box is only possible up to 5 days before shipment.', Translation::trans('ERROR.CUSTOMIZE_BOX_CANCELLATION_DEADLINE_PASSED'), 400);
            }

            $subscription = $this->my_account_subscription_custom_box_save->save($subscription, $data);

            if(false === $subscription)
                return $this->errorResponse('Box customization failed.', Translation::trans('ERROR.MY_ACCOUNT.BOX_CUSTOMIZATION_FAILED'), 400);

            return response()->json(['data'=> $this->my_account_subscription_data_transformer->single($subscription)]);
        }
        catch (ModelNotFoundException $ex)
        {
            return response()->json([],404);
        }
    }

    /**
     * @param $statusMsg
     * @param $translateMsg
     * @param $errorCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function errorResponse($statusMsg, $translateMsg, $errorCode)
    {
        return response()->json(['error' => [
            'status_message' => $statusMsg,
            'message' => $translateMsg]
        ], $errorCode);
    }
}