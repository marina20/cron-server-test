<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\Region;
use Helpers;
use App\Models\Translation;
use App\Models\Voucher;
use App\Models\VoucherProduct;
use Auth;
use Illuminate\Http\Request;
use Validator;
use App\Traits\FormatMoney;

class VoucherController extends Controller
{
    use FormatMoney;
    public function checkVoucher(Request $request){
        $checkText = $this->voucherIsOk($request->toArray());
        return response()->json(['success' => $checkText['success'],'msg' => $checkText['msg']]);
    }

    public function voucherIsOk(array $request):array {
        if(!empty($request['coupon_code'])){
            $voucher = Voucher::getVoucher($request['coupon_code']);
            if(!empty($voucher)){
                $order = new Order;
                $order->notSave = true;
                $country = Helpers::country();
                $order->country_id = $country->id;
                $region = Region::where(['country_id' => $country->id])->first();
                $order->region_id = $region->id;
                if(Auth::check()){
                    if(empty($request['delivery_frequency'])){
                        $order->is_subscription = 0;
                    } else {
                        $order->is_subscription = 1;
                    }
                    $order->user_id = Auth::id();
                    if(!Voucher::checkRestrictions($voucher,$order,true)){
                        return ['success' => false,'msg' => Translation::trans('VOUCHER.CHECK.RESTRICTIONFAIL')];
                    }
                } else {
                    if(!Voucher::preFilter($voucher,$order)){
                        return ['success' => false,'msg' => Translation::trans('VOUCHER.CHECK.COUNTRYFAIL')];
                    }
                }
                $order->delete();
                $val = "";
                $startKey = "VOUCHER.CHECK.START_VALUE_SUCCESS";
                $endKey = "VOUCHER.CHECK.END_VALUE_SUCCESS";
                if($voucher->value_type==Voucher::PRODUCT){
                    $val = self::getProductNamesCommaSeparatedString($voucher);
                    $startKey = "VOUCHER.CHECK.START_PRODUCT_SUCCESS";
                    $endKey = "VOUCHER.CHECK.END_PRODUCT_SUCCESS";
                } elseif ($voucher->value_type==Voucher::PRODUCTPERCENT || $voucher->value_type==Voucher::PRODUCTAMOUNT) {
                    $voucherProduct = VoucherProduct::where(['voucher_id' => $voucher->id])->first();
                    $product = Product::find($voucherProduct->product_id);
                    $productNames = self::getProductNamesCommaSeparatedString($voucher);
                    $startKey = "VOUCHER.CHECK.START_PRODUCTPERCENT_SUCCESS";
                    $middleKey = "VOUCHER.CHECK.MIDDLE_PRODUCTPERCENT_SUCCESS";
                    $endKey = "VOUCHER.CHECK.END_PRODUCTPERCENT_SUCCESS";
                    $voucherValue = $voucher->value . "%";
                    if ($voucher->value_type == Voucher::PRODUCTAMOUNT) {
                        $country = Helpers::country();
                        if ($country->iso_alpha_2 == "CH") {
                            $voucherValue = $country->currency." " . $this->formatMoney($voucher->value) . ".-";
                        } else {
                            $voucherValue = $this->formatMoney($voucher->value) . " ".$country->currency;
                        }
                    }
                    return  ['success' => true,'msg' => Translation::trans($startKey)." ".$productNames." ".Translation::trans($middleKey)." ".$voucherValue." ".Translation::trans($endKey)];
                } else {
                    $val = $voucher->value . "%";
                    if ($voucher->value_type == Voucher::AMOUNT) {
                        $country = Helpers::country();
                        if ($country->iso_alpha_2 == "CH") {
                            $val = $country->currency." " . $this->formatMoney($voucher->value) . ".-";
                        } else {
                            $val = $this->formatMoney($voucher->value) . " ".$country->currency;
                        }
                    }
                }
                return  ['success' => true,'msg' => Translation::trans($startKey)." ".$val." ".Translation::trans($endKey)];
            }
            return ['success' => false,'msg' => Translation::trans('VOUCHER.CHECK.EXISTFAIL')];
        }
        return ['success' => false,'msg' => Translation::trans('VOUCHER.CHECK.PARAMFAIL')];
    }


    private static function getProductNamesCommaSeparatedString($voucher){
        $voucherProducts = VoucherProduct::where(['voucher_id' => $voucher->id])->get();
        $val = '';
        $i = 0;
        $voucherProductsCount = count($voucherProducts);
        foreach($voucherProducts as $voucherProduct){
            $product = Product::find($voucherProduct->product_id);
            $val .= Translation::trans("PRODUCTS.".$product->name_key).' ('.$voucherProduct->qty.'x)';
            // if not last and prelast element, add comma.
            if(++$i!=$voucherProductsCount && $i!=($voucherProductsCount-1)){
                $val .= ', ';
            } elseif ($i==($voucherProductsCount-1)){
                $val .= ' '.Translation::trans("VOUCHER.CHECK.AND").' ';
            }
        }
        return $val;
    }
}