<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 10/17/17
 * Time: 4:12 PM
 */

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\NewsletterSignup;
use App\Models\Order;
use App\Models\Postcode;
use App\Models\Profile;
use App\Models\ReferralUrl;
use App\Models\User;
use App\Services\CustomBox\CreateContent;
use App\Services\FindNextDeliveryDate;
use App\Traits\FormatMoney;
use Auth;
use Carbon\Carbon;
use Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class UserProfileController extends Controller
{
    use FormatMoney;
    public function __construct(Profile $profile) {
        $this->profile = $profile;
    }

    private function checkNewsletterAccept($profile){
        $newsletterSignUp = NewsletterSignup::where('email','=',$profile->user->email)->first();

        if(!empty($newsletterSignUp)) {
            $profile->accepts_newsletter = $newsletterSignUp->confirmed;
        }
        return $profile;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($user_id)
    {
        $profile = $this->profile->where('user_id',$user_id)->with('user')->first();
        $profile = $this->checkNewsletterAccept($profile);
        $profile = $profile->toArray();
        $country = Country::where('content_code',app('translator')->getLocale())->first();
        $postcode = Postcode::where(['postcode' => $profile['billing_postcode']])->where(["country" => $country->id])->where(["city" => $profile['billing_city']])->first();
        $profile['billing_zip_city'] = ['id' => 0, 'zip_city' => ''];
        if(!empty($postcode)){
            $profile['billing_zip_city'] = ['id' => $postcode->id, 'zip_city' => $postcode->postcode.", ".$postcode->city];
        }
        $postcode = Postcode::where(['postcode' => $profile['shipping_postcode']])->where(["city" => $profile['shipping_city']])->where(["country" => $country->id])->first();
        $profile['shipping_zip_city'] = ['id' => 0, 'zip_city' => ''];
        if(!empty($postcode)){
            $profile['shipping_zip_city'] = ['id' => $postcode->id, 'zip_city' => $postcode->postcode.", ".$postcode->city];
        }

        if(empty($profile['shipping_street_name'])){
            $result = CreateContent::migrateAddress($profile['shipping_address_1']);
            $profile['shipping_street_name'] = $result[0];
            $profile['shipping_street_nr'] = $result[1];
        }
        if(empty($profile['billing_street_name'])){
            $result = CreateContent::migrateAddress($profile['billing_address_1']);
            $profile['billing_street_name'] = $result[0];
            $profile['billing_street_nr'] = $result[1];
        }
        $profile['billing_street_nr_checkbox'] = false;
        if(empty($profile['billing_street_nr'])){
            $profile['billing_street_nr_checkbox'] = true;
            $profile['billing_street_nr'] = '';
        }
        $profile['shipping_street_nr_checkbox'] = false;
        if(empty($profile['shipping_street_nr'])){
            $profile['shipping_street_nr_checkbox'] = true;
            $profile['shipping_street_nr'] = '';
        }
        return response()->json(['data'=>$profile]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $user_id)
    {
//        $this->authorize('update',UserProfile::class);

        $validator = Validator::make($request->all(), [
//            'first_name' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message'=>'Failure.', 'errors'=>$validator->errors()],400);
        }

        if($request->has('current_password') || $request->has('new_password') || $request->has('repeat_new_password')) {
            if($request->input('new_password') != $request->input('repeat_new_password')) {
                return response()->json(['message'=>'Failure.', 'errors'=>"New password doesn't match."],403);
            }
            $user = User::find($user_id);
            if(!app('hash')->check($request->input('current_password'), $user->password)) {
                return response()->json(['message'=>'Failure.', 'errors'=>"Wrong password."],403);
            }
            $user->password = app('hash')->make($request->input('new_password'));
            $user->save();
        }

        $profile = $this->profile->where('user_id',$user_id)->first();

        $profile->first_name = $request->input('first_name',$profile->first_name);
        $profile->last_name = $request->input('last_name',$profile->last_name);
        $profile->gender = $request->input('gender',$profile->gender);
        $profile->child_birthday = $request->input('child_birthday',$profile->child_birthday);
        $profile->shipping_title = $request->input('shipping_title',$profile->shipping_title);
        $profile->shipping_first_name = $request->input('shipping_first_name',$profile->shipping_first_name);
        $profile->shipping_last_name = $request->input('shipping_last_name',$profile->shipping_last_name);
        $profile->shipping_company = $request->input('shipping_company',$profile->shipping_company);
        //$profile->shipping_address_2 = $request->input('shipping_address_2',$profile->shipping_address_2);
        $profile->shipping_country = $request->input('shipping_country',$profile->shipping_country);
        $profile->shipping_email = $request->input('shipping_email',$profile->shipping_email);
        $profile->shipping_phone = $request->input('shipping_phone',$profile->shipping_phone);
        $profile->shipping_state = $request->input('shipping_state',$profile->shipping_state);
        $profile->billing_title = $request->input('billing_title',$profile->billing_title);
        $profile->billing_first_name = $request->input('billing_first_name',$profile->billing_first_name);
        $profile->billing_last_name = $request->input('billing_last_name',$profile->billing_last_name);
        $profile->billing_company = $request->input('billing_company',$profile->billing_company);

        $profile->shipping_street_name = $request->input('shipping_street_name',$profile->shipping_street_name);
        $profile->shipping_street_nr = $request->input('shipping_street_nr',$profile->shipping_street_nr);

        $profile->billing_street_name = $request->input('billing_street_name',$profile->billing_street_name);
        $profile->billing_street_nr = $request->input('billing_street_nr',$profile->billing_street_nr);

        if(!empty($request->input('billing_postcode_id'))){
            $postcode = Postcode::where(['id' => $request->input('billing_postcode_id')])->first();
            $profile->billing_city = $postcode->city;
            $profile->billing_postcode = $postcode->postcode;
        }
        if(!empty($request->input('shipping_postcode_id'))){
            $postcode = Postcode::where(['id' => $request->input('shipping_postcode_id')])->first();
            $profile->shipping_city = $postcode->city;
            $profile->shipping_postcode = $postcode->postcode;
        }

        $profile->billing_country = $request->input('billing_country',$profile->billing_country);
        $profile->billing_email = $request->input('billing_email',$profile->billing_email);
        $profile->billing_phone = $request->input('billing_phone',$profile->billing_phone);
        $profile->billing_state = $request->input('billing_state',$profile->billing_state);
        $profile->payment_method = $request->input('payment_method',$profile->payment_method);
        //$profile->accepts_newsletter = $request->input('accepts_newsletter',$profile->accepts_newsletter);
        $profile->customer_birthday = $request->input('customer_birthday',$profile->customer_birthday);

        if(!empty($request->input('shipping_street_nr_checkbox'))){
            $profile['shipping_street_nr'] = 0;
        }
        if(!empty($request->input('billing_street_nr_checkbox'))){
            $profile['billing_street_nr'] = 0;
        }
        $profile->save();

        $this->updateOrdersAddress($profile);


        $profile = $this->checkNewsletterAccept($profile);
        $profile = $profile->toArray();
        $country = Country::where('content_code',app('translator')->getLocale())->first();
        $postcode = Postcode::where(['postcode' => $profile['billing_postcode']])->where(["country" => $country->id])->where(["city" => $profile['billing_city']])->first();
        $profile['billing_zip_city'] = ['id' => 0, 'zip_city' => ''];
        if(!empty($postcode)){
            $profile['billing_zip_city'] = ['id' => $postcode->id, 'zip_city' => $postcode->postcode.", ".$postcode->city];
        }
        $postcode = Postcode::where(['postcode' => $profile['shipping_postcode']])->where(["city" => $profile['shipping_city']])->where(["country" => $country->id])->first();
        $profile['shipping_zip_city'] = ['id' => 0, 'zip_city' => ''];
        if(!empty($postcode)){
            $profile['shipping_zip_city'] = ['id' => $postcode->id, 'zip_city' => $postcode->postcode.", ".$postcode->city];
        }
        $profile['billing_street_nr_checkbox'] = false;
        if(empty($profile['billing_street_nr'])){
            $profile['billing_street_nr_checkbox'] = true;
            $profile['billing_street_nr'] = '';
        }
        $profile['shipping_street_nr_checkbox'] = false;
        if(empty($profile['shipping_street_nr'])){
            $profile['shipping_street_nr_checkbox'] = true;
            $profile['shipping_street_nr'] = '';
        }
        return response()->json(['message'=>'Success. Profile updated.','data'=>$profile]);
    }

    public function delete()
    {
        $user = Auth::user();
        $user->email = $user->email . 'DELETED'.Carbon::now();
        $user->save();

        return response()->json(['message'=>'Success. User deleted.']);
    }

    public function getNextDeliveryDate()
    {
        $user = Auth::user();

        $find_next_delivery_date = new FindNextDeliveryDate();

        $next_date = $find_next_delivery_date->next($user);

        if(empty($next_date))
        {
            return response('subscription not found',404);
        }

        return response($next_date);
    }

	/**
	 * Update upcoming order's billing and shipping address.
	 *
	 * @param  Profile  $profile
	 *
	 */
    private function updateOrdersAddress(Profile $profile){
	    $country = Country::where('content_code',app('translator')->getLocale())->first();
	    $data = Order::query()->where('user_id',Auth::user()->id)
		    ->where('country_id',$country->id)->where('cancellation_deadline','>', date('Y-m-d'))->get();
	    foreach ($data as $order) {
		    //TODO: create mutator for Order Model and replace direct access here.
		    $order->shipping_title = $profile->shipping_title;
		    $order->shipping_first_name = $profile->shipping_first_name;
		    $order->shipping_last_name = $profile->shipping_last_name;
		    $order->shipping_company = $profile->shipping_company;
		    // $order->shipping_address_1 = $profile->shipping_address_1;
		    // $order->shipping_address_2 = $profile->shipping_address_2;
		    $order->shipping_city = $profile->shipping_city;
		    $order->shipping_postcode = $profile->shipping_postcode;
		    $order->shipping_country = $profile->shipping_country;
		    $order->shipping_email = $profile->shipping_email;
		    $order->shipping_phone = $profile->shipping_phone;
		    $order->shipping_state = $profile->shipping_state;
		    $order->billing_title = $profile->billing_title;
		    $order->billing_first_name = $profile->billing_first_name;
		    $order->billing_last_name = $profile->billing_last_name;
		    $order->billing_company = $profile->billing_company;
		    // $order->billing_address_1 = $profile->billing_address_1;

            $order->billing_street_nr = $profile->billing_street_nr;

            $order->billing_street_name = $profile->billing_street_name;
            $order->shipping_street_nr = $profile->shipping_street_nr;
            $order->shipping_street_name = $profile->shipping_street_name;

		    // $order->billing_address_2 = $profile->billing_address_2;
		    $order->billing_city = $profile->billing_city;
		    $order->billing_postcode = $profile->billing_postcode;
		    $order->billing_country = $profile->billing_country;
		    $order->billing_email = $profile->billing_email;
		    $order->billing_phone = $profile->billing_phone;
		    $order->billing_state = $profile->billing_state;

		    $order->save();
	    }
    }

    public function getInviteFriendData()
    {
        $referralUrl = ReferralUrl::generateReferral();
        // TODO add baseurl to .env?
        //$wallet_amount_currency = Auth::user()->profile->wallet_amount_currency;
        //if(empty($wallet_amount_currency)){
        $wallet_amount_currency = Country::where('content_code', Helpers::get_locale())->first()->currency;
        //}
        $data = [
            'rewards_link' => "https://yamo.bio/?ref=".$referralUrl->url,
            'rewards_currency' => Country::where('content_code', Helpers::get_locale())->first()->currency,
            'rewards_value' => $referralUrl->region->referrer_reward_cents,
            'rewards_wallet' => $this->formatMoney(Auth::user()->profile->wallet_amount_cents),
            'rewards_wallet_currency' => $wallet_amount_currency
        ];
        return response($data);
    }

    public function getZipCity(Request $request, $search)
    {
        if(strlen($search) < 2)
        {
            return response()->json(['data'=> []]);
        }
        $usedCountryId = Country::where('content_code',app('translator')->getLocale())->first()->id;
        $search = urldecode($search);
        $search = strtolower($search);

        $postcodes = Postcode::select('id',DB::raw("CONCAT(postcode,', ',city) as zip_city"))
            ->where([['city','like',\DB::raw('"%'.$search.'%" COLLATE utf8_unicode_ci')],['country',$usedCountryId]])
            ->orWhere([['postcode','like','%'.$search.'%'],['country',$usedCountryId]])
            ->get();

        return response()->json(['data'=> $postcodes]);
    }
}
