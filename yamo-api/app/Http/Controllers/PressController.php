<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/11/17
 * Time: 3:24 PM
 */

namespace App\Http\Controllers;

use App\Models\Press;
use Helpers;
use Validator;

class PressController extends Controller
{
    public function __construct(Press $press)
    {
        $this->press = $press;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $press_clips = $this->press->query()->orderBy('position')->get();
        return response()->json(['data' => $press_clips]);
    }
}