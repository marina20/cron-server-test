<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 10/23/17
 * Time: 11:12 AM
 */

namespace App\Http\Controllers;

use App\Models\AdyenNotification;
use App\Models\Configuration;
use App\Models\Order;
use App\Models\ReferralUrl;
use App\Models\Subscription;
use App\Models\Voucher;
use App\Services\Adyen\Notification;
use App\Services\CancelSubscriptionService;
use App\Services\CreateAdyenLinkService;
use App\Services\CreateInvoiceLinkService;
use App\Services\CreateOrderService;
use App\Services\CreatePaypalLinkService;
use App\Services\CreateSofortLinkService;
use App\Services\GAProductsService;
use App\Services\MFGInvoice\MFGInvoice;
use App\Services\OrderService;
use App\Services\UpdateProfileService;
use App\Services\Validation\CutOffTimeInterface;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Validator;
use Helpers;

class CheckoutController extends Controller
{
    const LOG_TITLE = 'CheckoutController: ';
    protected $create_order;
    protected $create_invoice_link_service;
    protected $create_adyen_link_service;
    protected $create_paypal_link_service;
    protected $create_sofort_link_service;
    protected $cancel_subscription_service;
    protected $update_profile_service;
    protected $ga_products_service;
    protected $adyen_notification_service;
    protected $mfg_invoice;
    protected $validationCutOffService;

    const VALIDATE_AVAILABLE_DELIVERY_DAY_OF_THE_WEEK_AT = [3,5];
    const VALIDATE_AVAILABLE_DELIVERY_DAY_OF_THE_WEEK_DE = [4,5];
    const CHECKOUT_VALIDATION_STEP_REGULAR = 1;
    const CHECKOUT_VALIDATION_STEP_WIZARD_1 = 2;
    const CHECKOUT_VALIDATION_STEP_WIZARD_2 = 3;

    const CHECKOUT_VALIDATION_STEP_SHOP_1 = 111;
    const ERROR_KEY_TECHNICAL_PROBLEMS = 'TECHNICAL_PROBLEMS';

    public function __construct(CreateOrderService $create_order,
                                CreateInvoiceLinkService $create_invoice_link_service,
                                CreateAdyenLinkService $create_adyen_link_service,
                                CreatePaypalLinkService $create_paypal_link_service,
                                CreateSofortLinkService $create_sofort_link_service,
                                CancelSubscriptionService $cancel_subscription_service,
                                UpdateProfileService $update_profile_service,
                                GAProductsService $ga_products_service,
                                Notification $adyen_notification_service,
                                MFGInvoice $mfg_invoice,
                                CutOffTimeInterface $validationCutOffService)
    {
        $this->create_order = $create_order;
        $this->create_invoice_link_service = $create_invoice_link_service;
        $this->create_adyen_link_service = $create_adyen_link_service;
        $this->create_paypal_link_service = $create_paypal_link_service;
        $this->create_sofort_link_service = $create_sofort_link_service;
        $this->cancel_subscription_service = $cancel_subscription_service;
        $this->update_profile_service = $update_profile_service;
        $this->ga_products_service = $ga_products_service;
            
        $this->adyen_notification_service = $adyen_notification_service;
        $this->mfg_invoice = $mfg_invoice;
        $this->validationCutOffService = $validationCutOffService;
    }

    public function adyenNotification(Request $request)
    {
        $adyen_notification = new AdyenNotification();
        $adyen_notification->notification_body = json_encode($request->all());
        $items = $request->notificationItems;
        $notification_item = reset($items);
        $notification_item = $notification_item['NotificationRequestItem'];
        $adyen_notification->success = $notification_item['success'] === 'true' ? true : false;
        $adyen_notification->event_code = $notification_item['eventCode'];
        $adyen_notification->psp_reference = $notification_item['pspReference'];
        $adyen_notification->reason = $notification_item['reason'] === 'null' ? NULL : $notification_item['reason'];
        if (isset($notification_item['merchantReference'])) {
            $order = Order::find($notification_item['merchantReference']);
            if ($order) {
                $adyen_notification->order_id = $notification_item['merchantReference'];
                $order->adyen_auth_result = $notification_item['eventCode'];
                if (empty($order->transaction_id))
                {
                    $order->transaction_id = $notification_item['pspReference'];
                }
                $order->save();
            }
        }
        $adyen_notification->save();
        $this->adyen_notification_service->process($adyen_notification);
        return response()->json(['notificationResponse'=>'[accepted]']);
    }

    public function refundMFG (Request $request){
        $amount = $request->input('amount');
        $reference = $request->input('reference');
        $result = $this->mfg_invoice->refund($amount, $reference);
        return response()->json($result);
    }

    //TODO:add a comment block for this function and all the god functions in this class.
    public function result(Request $request)
    {
        // validate that all necessary data is there
        $validator = Validator::make($request->all(),[
            'authResult' => 'required',
            'merchantReference' => 'required',
//            'paymentMethod' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json(['message'=>'Failure.', 'errors'=>$validator->errors()],400);
        }

        $order_status = Order::STATUS_PENDING;

        $order = Order::find($request->input('merchantReference'));

        Mail::raw('Order id: '.$request->input('merchantReference') . '; Auth result: '.$request->input('authResult'), function($msg) {
            $msg->to(['nenad@yamo.ch']);
            $msg->subject('Adyen responds');
        });

        if($order->status != Order::STATUS_PENDING && $order->status != Order::STATUS_PAYED) {
            Mail::raw('Order id: '.$order->id . '; Auth result: '.$request->input('authResult'), function($msg) {
                $msg->to(['nenad@yamo.ch']);
                $msg->subject('Adyen tries to return to order that is not pending.');
            });
            return response()->json(['message'=>'Failure. Order is not scheduled for payment. Status is not pending.'],400);
        }

        switch($request->input('authResult')) {
            case 'AUTHORISED':
                $order_status = Order::STATUS_PAYED;
                if(Order::PAYMENT_METHOD_INVOICE != $request->input('paymentMethod') && Order::PAYMENT_METHOD_FREE != $request->input('paymentMethod'))
                {
                    $order->completed_date = Carbon::now()->format('Y-m-d H:i:s');
                    $order->paid_date = Carbon::now()->format('Y-m-d H:i:s');
                }
                $order = Voucher::redeemVoucher($order);
                break;
            case 'REFUSED':
                $order_status = Order::STATUS_CANCELLED;
                $order->completed_date = Carbon::now()->format('Y-m-d H:i:s');
                // also cancel subscription as payment only happens on first buying
                if($order->subscription instanceof Subscription)
                {
                    $this->cancel_subscription_service->cancel($order->subscription);
                }
                $order = ReferralUrl::revertWallet($order);
                break;
            case 'CANCELLED':
                $order_status = Order::STATUS_CANCELLED;
                $order->completed_date = Carbon::now()->format('Y-m-d H:i:s');
                // also cancel subscription as payment can happen only on first buying
                if($order->subscription instanceof Subscription)
                {
                    $this->cancel_subscription_service->cancel($order->subscription);
                }
                $order = ReferralUrl::revertWallet($order);
                break;
            case 'PENDING':
                $order_status = Order::STATUS_PENDING;
                break;
            case 'ERROR':
                $order_status = Order::STATUS_PENDING;
                break;
        }


        if($order->payment_method_id != Order::PAYMENT_METHOD_CREDITCARD)
        {
            $order->adyen_auth_result = $request->input('authResult');
            if (empty($order->transaction_id))
            {
                $order->transaction_id = $request->input('pspReference');
            }
        }

        $order->adyen_payment_method = $request->input('paymentMethod');
        $order->status = $order_status;
        $order->save();

        OrderService::generateSKU($order);
        $coupon = '';
        if(!empty($order->coupon_code)) {
            $coupon = $order->coupon_code;
        }

        $typeFormAfterCheckoutEnabled = false;
        $config = Configuration::where(['key' => 'typeform_after_checkout'])->first();
        if(!empty($config) && '1' == $config->value){
            $typeFormAfterCheckoutEnabled = true;
        }

        return response()->json(['message'=>'Success. Order updated.',
            'currency'=>$order->getCurrencyCode($order->currency),
            'value'=>$order->order_total,
            'id'=>$order->id,
            'tax' => $order->order_tax,
            'shipping' => $order->shipping,
            'coupon' => $coupon,
            'typeform_after_checkout' => $typeFormAfterCheckoutEnabled,
            'products'  => $this->ga_products_service->products($order),
            'customer_ip_address' => $order->customer_ip_address,
            'customer_email' => $order->shipping_email,
            'payment_type' => $this->translatePaymentMethod($order->payment_method_id),
            'delivery_date' => $order->delivery_date->format('Y-m-d'),
            'ts_product' => $this->ga_products_service->trustedShopProduct($order)
        ]);
    }

    public function resultProducts(Request $request,$id)
    {
        try
        {
            $order = Order::find($id);
            if(empty($order))
            {
                return response()->json('Order not found',404);
            }

            $products = $this->ga_products_service->products($order);

            return response()->json([
                'products'  => $products
            ]);
        }
        catch(\Exception $e)
        {
            return response()->json($e->getMessage(),404);
        }
    }

    /**
     * Wizard Funnel first step
     * @param Request $request
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function funnelCheckoutFirstStep(Request $request)
    {
        $validator = $this->checkoutValidation($request, self::CHECKOUT_VALIDATION_STEP_WIZARD_1);
        if($validator!==true){
            return $validator;
        }

        return $this->create_order->createProfile($request,true);
    }


    public function shopCheckoutFirstStep(Request $request)
    {
        $validator = $this->checkoutValidation($request, self::CHECKOUT_VALIDATION_STEP_SHOP_1);
        if($validator!==true){
            return $validator;
        }
        return $this->create_order->createProfile($request, false);
    }

    private function checkoutValidation(Request $request, $step=self::CHECKOUT_VALIDATION_STEP_REGULAR)
    {
        $criteria = [];
        // validate that all necessary data is there
        switch ($step)
        {
            case self::CHECKOUT_VALIDATION_STEP_REGULAR:
                $criteria = [
                    'user_id' => 'required',
                    'delivery_date' => 'required|date',
                    'billing_first_name' => 'required',
                    'billing_last_name' => 'required',
                    'billing_street_name' => 'required',
                    'billing_country' => 'required',
                    'billing_email' => 'required|email',
                    'billing_phone' => 'required',
                    'billing_postcode_id' => 'required',
                    'shipping_first_name' => 'required',
                    'shipping_last_name' => 'required',
                    'shipping_street_name' => 'required',
                    'shipping_country' => 'required',
                    'shipping_postcode_id' => 'required',
                    'shipping_email' => 'required|email',
                    'shipping_phone' => 'required',
                    'payment_method' => 'required'
                ];
                break;
            case self::CHECKOUT_VALIDATION_STEP_WIZARD_1:
                    $criteria = [
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'gender' => 'required',
                        'baby_name' => 'required',
                        'baby_birthday' => 'required',
                        'customer_birthday' => 'required',
                        'billing_first_name' => 'required',
                        'billing_last_name' => 'required',
                        'billing_street_name' => 'required',
                        'billing_postcode_id' => 'required',
                        'billing_country' => 'required',
                        'billing_email' => 'required|email',
                        'billing_phone' => 'required',
                        'shipping_first_name' => 'required',
                        'shipping_last_name' => 'required',
                        'shipping_street_name' => 'required',
                        'shipping_country' => 'required',
                        'shipping_postcode_id' => 'required',
                        'shipping_email' => 'required|email',
                        'shipping_phone' => 'required',
                    ];
                    break;
            case self::CHECKOUT_VALIDATION_STEP_SHOP_1:
                    $criteria = [
                        'first_name' => 'required',
                        'last_name' => 'required',
                        'gender' => 'required',
                        'customer_birthday' => 'required',
                        'billing_first_name' => 'required',
                        'billing_last_name' => 'required',
                        'billing_street_name' => 'required',
                        'billing_postcode_id' => 'required',
                        'billing_country' => 'required',
                        'billing_email' => 'required|email',
                        'billing_phone' => 'required',
                        'shipping_first_name' => 'required',
                        'shipping_last_name' => 'required',
                        'shipping_street_name' => 'required',
                        'shipping_country' => 'required',
                        'shipping_postcode_id' => 'required',
                        'shipping_email' => 'required|email',
                        'shipping_phone' => 'required',
                    ];
                    break;
            case self::CHECKOUT_VALIDATION_STEP_WIZARD_2:
                $criteria = [
                    'order_id' => 'required',
                ];
                break;
        }

        $validator = Validator::make($request->all(),$criteria);

        /*
         * Validating input data and products
         */
        if($validator->fails()) {
            return response()->json([
                'message'=>'Failure.',
                'errors'=>$validator->errors()->first()
            ],400);
        }
        return true;
    }

    /**
     * Second step of the checkout saving payment method
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function funnelCheckoutSecondStep(Request $request)
    {
        if(!$request->has('payment_method')) {
            return response()->json([
                'error' =>
                    [
                        'message'=>'Payment method must be present',
                        'key' => self::ERROR_KEY_TECHNICAL_PROBLEMS
                    ]
            ], 400);
        }

        if($this->validationCutOffService->validate($request->input('delivery_date'), Helpers::get_country_code(Auth::user()->profile->shipping_country)) !== true)
            return response()->json([
                'error' =>
                    [
                        'message'=>'Cut off time already expired.',
                        'key' => self::ERROR_KEY_TECHNICAL_PROBLEMS
                    ]
            ],403);

        $adyenResponse = null;
        if(empty($request->input('delivery_frequency'))){
            $adyenResponse = $this->create_order->single($request);
        } else {

            $adyenResponse = $this->create_order->subscription($request);
        }

        return $adyenResponse;
    }

    protected function translatePaymentMethod($payment_method_input)
    {
        switch($payment_method_input) {
            case Order::PAYMENT_METHOD_CREDITCARD:
                return Order::PAYMENT_METHOD_CREDITCARD_DE;
                break;
            case Order::PAYMENT_METHOD_PAYPAL:
                return Order::PAYMENT_METHOD_PAYPAL_DE;
                break;
            case Order::PAYMENT_METHOD_SOFORT:
                return Order::PAYMENT_METHOD_SOFORT_DE;
                break;
            case Order::PAYMENT_METHOD_INVOICE:
                return Order::PAYMENT_METHOD_INVOICE_DE;
                break;
        }
        return $payment_method_input;
    }
}
