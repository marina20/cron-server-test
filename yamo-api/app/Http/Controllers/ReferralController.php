<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\ReferralAttempts;
use App\Models\ReferralUrl;
use App\Models\Translation;
use Auth;
use Illuminate\Http\Request;
use Validator;

class ReferralController extends Controller
{

    public function checkReferral(Request $request){
        $checkText = $this->referralIsOk($request->toArray());
        return response()->json(['success' => $checkText['success'],'msg' => $checkText['msg']]);
    }

    public function referralIsOk(array $request):array {
        if(Auth::check()){
            if(!Auth::user()->isNew()){
                return ['success' => false,'msg' => Translation::trans('REFERRAL.CHECK.NOTNEW')];
            }
        }
        if(!empty($request['ref_code'])){
            $referralUrl = ReferralUrl::where(['url' => $request['ref_code']])->where(['enabled' => 1])->first();
            if(!empty($referralUrl)){
                $country = Country::where('content_code',app('translator')->getLocale())->first();
                if($referralUrl->region->country->iso_alpha_2===$country->iso_alpha_2){
                    ReferralAttempts::create(['referrer_url_id' => $referralUrl->id,'order_id' => NULL,'user_id' => Auth::id(),'status' => 'Precheck: success']);
                    return ['success' => true,'msg' => Translation::trans('REFERRAL.CHECK.SUCCESS')];
                }
                ReferralAttempts::create(['referrer_url_id' => $referralUrl->id,'order_id' => NULL,'user_id' => Auth::id(),'status' => 'Precheck: referral not found']);
                return  ['success' => false,'msg' => Translation::trans('REFERRAL.CHECK.COUNTRYFAIL')];
            }
            ReferralAttempts::create(['referrer_url_id' => NULL,'order_id' => NULL,'user_id' => Auth::id(),'status' => 'Precheck: referral not found']);
            return ['success' => false,'msg' => Translation::trans('REFERRAL.CHECK.EXISTFAIL')];
        }
        ReferralAttempts::create(['referrer_url_id' => NULL,'order_id' => NULL,'user_id' => Auth::id(),'status' => 'Precheck: param not found']);
        return ['success' => false,'msg' => Translation::trans('REFERRAL.CHECK.PARAMFAIL')];
    }
}