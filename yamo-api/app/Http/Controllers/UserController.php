<?php

namespace App\Http\Controllers;

use App\Jobs\SendNewsletterActivationEmailJob;
use App\Models\AcEventDefinition;
use App\Models\AcEventOccurred;
use App\Models\Country;
use App\Models\Postcode;
use App\Models\ReferralUrl;
use App\Models\User;
use App\Models\Profile;
use App\Models\NewsletterSignup;
use App\Models\UTMTracker;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Validator;
use Auth;
use Tymon\JWTAuth\JWTAuth;
use App\Traits\ResetsPasswords;

class UserController extends Controller
{
    use ResetsPasswords;
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;

    protected $user;

    public function __construct(User $user, JWTAuth $jwt) {
        $this->user = $user;
        $this->jwt = $jwt;
    }

    public function addTagToUser(Request $request){
        $definition = AcEventDefinition::where(['event_name' => $request->input('eventName')])->first();
        if(Auth::check() && !empty($definition)){
            // $lastEvent = AcEventOccurred::where(['user_id' => Auth::id()])->latest()->first();
            $lastEvent = AcEventOccurred::where(['user_id' => Auth::id(), 'event_name' => $request->input('eventName'), 'synced' => false])->latest()->first();
            if(empty($lastEvent)){
                AcEventOccurred::firstOrCreate(['event_name' => $request->input('eventName'), 'user_id' => Auth::id(), 'timestamp' => Carbon::now(), 'synced' => false]);
            }
            return response()->json(["ok" => 1]);
        }
        return response()->json(["ok" => 0]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = $this->user->query()->find($id);
        $profile = Profile::where(['user_id' => $id])->first()->toArray();
        $profile['billing_street_nr_checkbox'] = false;
        if(empty($profile['billing_street_nr'])){
            $profile['billing_street_nr_checkbox'] = true;
            $profile['billing_street_nr'] = '';
        }
        $profile['shipping_street_nr_checkbox'] = false;
        if(empty($profile['shipping_street_nr'])){
            $profile['shipping_street_nr_checkbox'] = true;
            $profile['shipping_street_nr'] = '';
        }
        $country = Country::where('content_code',app('translator')->getLocale())->first();
        $postcode = Postcode::where(['postcode' => $profile['billing_postcode']])->where(["country" => $country->id])->where(["city" => $profile['billing_city']])->first();
        $profile['billing_zip_city'] = ['id' => 0, 'zip_city' => ''];
        if(!empty($postcode)){
            $profile['billing_zip_city'] = ['id' => $postcode->id, 'zip_city' => $postcode->postcode.", ".$postcode->city];
        }
        $postcode = Postcode::where(['postcode' => $profile['shipping_postcode']])->where(["city" => $profile['shipping_city']])->where(["country" => $country->id])->first();
        $profile['shipping_zip_city'] = ['id' => 0, 'zip_city' => ''];
        if(!empty($postcode)){
            $profile['shipping_zip_city'] = ['id' => $postcode->id, 'zip_city' => $postcode->postcode.", ".$postcode->city];
        }
        $profile['billing_street_nr_checkbox'] = false;
        if(empty($profile['billing_street_nr'])){
            $profile['billing_street_nr_checkbox'] = true;
            $profile['billing_street_nr'] = '';
        }
        $profile['shipping_street_nr_checkbox'] = false;
        if(empty($profile['shipping_street_nr'])){
            $profile['shipping_street_nr_checkbox'] = true;
            $profile['shipping_street_nr'] = '';
        }
        $user = $user->toArray();
        $user['profile'] = $profile;
        return response()->json(['data'=> $user]);
    }

    /**
     * Register new user
     *
     * @param Request $request
     * @return Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|confirmed|unique:users',
            'password' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'child_birthday' => 'required|date',
            'gender' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()->first()],403);
        }

        $confirmation_code = str_random(30);
        $email = $request->input('email');
        $nameArr = explode('@',$email);
        $name = reset($nameArr);
        $password = app('hash')->make($request->input('password'));
        $user = User::create([
            'email' => $email,
            'password' => $password,
            'name' => $name,
            'confirmation_code' => $confirmation_code,
            'registered_at'=>date('Y-m-d H:m:i')
        ]);


        $profile = Profile::create([
            'user_id' => $user->id,
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'child_birthday' => $request->input('child_birthday'),
            'customer_birthday' => $request->input('customer_birthday'),
            // 'accepts_newsletter' =>$request->input('accepts_newsletter'),
            'gender' => $request->input('gender'),
            'url_referrer' => $request->input('referrer')
        ]);
        $profile->save();
        ReferralUrl::generateReferral($user);
        if($request->input('accepts_newsletter')) {
            $signup = NewsletterSignup::whereEmail($request->input('email'))->first();
            if(empty($signup)) {
                $signup = new NewsletterSignup();
                $signup->email = $request->input('email');
            }
            if($signup->confirmed!=1) {
                $signup->confirmation_code = $confirmation_code;
                $signup->save();
                dispatch(new SendNewsletterActivationEmailJob($signup->email, $signup->confirmation_code));
            }
        }
        if(!empty($request->input('utm'))){
            $utmTracker = new UTMTracker();
            $utmTracker->type = 'user';
            $utmTracker->reference_id = $user->id;
            foreach($request->input('utm') as $key => $val){
                if(!empty($val)) {
                    $utmTracker[$key] = $val;
                }
            }
            $utmTracker->save();
        }

        $res['success'] = true;
        $res['message'] = 'User registered!';
        $res['data'] = $user;

        try {
            if (! $token = $this->jwt->attempt($request->only('email', 'password'))) {
                return response()->json(['error'=>'user_not_found'], 404);
            }
        } catch (TokenExpiredException $e) {
            return response()->json(['error'=>'token_expired'], $e->getStatusCode());
        } catch (TokenInvalidException $e) {
            return response()->json(['error'=>'token_invalid'], $e->getStatusCode());
        } catch (JWTException $e) {
            return response()->json(['token_absent' => $e->getMessage()], $e->getStatusCode());
        }
        $res['token'] = $token;

        return response($res);
    }

    /**
     * @param $confirmation_code
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     * @throws InvalidConfirmationCodeException
     */
    public function confirm($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if ( ! $user)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        return response('user confirmed!');
    }

    /**
     * For new wizard flow. We are going to check if user exists or is new
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function registerWithEmail(Request $request)
    {
        try {
            $responseData = [
                'isNewUser' => true,
                'message' => 'User is new.',
                'data' => ['id'=>null]
            ];

            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()->first()], 403);
            }
            $email = $request->input('email');

            if($request->input('accepts_newsletter')) {
                $signup = NewsletterSignup::whereEmail($request->input('email'))->first();
                if(empty($signup)) {
                    $signup = new NewsletterSignup();
                    $signup->email = $request->input('email');
                }
                if($signup->confirmed!=1) {
                    $confirmation_code = str_random(30);
                    $signup->confirmation_code = $confirmation_code;
                    $signup->save();
                    dispatch(new SendNewsletterActivationEmailJob($signup->email, $signup->confirmation_code));
                }
            }

            $existingUser = User::where('email', $email)->first();

            if (!empty($existingUser)) {
                $responseData['isNewUser'] = false;
                $responseData['message'] = 'User already exists!';
                $responseData['data']['id'] = $existingUser->id;
                return response($responseData);
            }
            if(!empty($request->input('utm'))){

                $utmTracker = new UTMTracker();
                $utmTracker->type = 'email';
                $utmTracker->reference_id = $request->input('email');
                foreach($request->input('utm') as $key => $val){
                    if(!empty($val)) {
                        $utmTracker[$key] = $val;
                    }
                }
                $utmTracker->save();
            }

            return response($responseData);
        }
        catch (\Exception $e)
        {
            app('sentry')->captureException($e);
            return response()->json(['error'=>'You have an error. ' . $e->getMessage()],404);
        }
    }
}