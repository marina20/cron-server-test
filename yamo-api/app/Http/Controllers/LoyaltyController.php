<?php

namespace App\Http\Controllers;

use App\Models\LoyaltyRule;
use App\Http\Resources\LoyaltyRuleResource;
use Illuminate\Support\Facades\Auth;
use Loyalty;

/**
 * Endpoints which will be used for Loyalty Programme in new my-account
 * Class LoyaltyController
 * @package App\Http\Controllers
 */
class LoyaltyController extends Controller
{
    /**
     * Return transformed loyalty rules, box number and reward description is returned in collection
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $rules = LoyaltyRule::orderBy('box_number')->get();

        return LoyaltyRuleResource::collection($rules);
    }

    /**
     * Get number of next box that logged in customer would receive
     * @return \Illuminate\Http\JsonResponse
     */
    public function box()
    {
        if(Auth::check()){
            return response()->json(['data'=>['box'=>Loyalty::currentBoxNumber(Auth::user())]]);
        }
        return response()->json(['error' => 'unauthorized'], 401);
    }
}