<?php
namespace App\Services\ActiveCampaign;

use App\Models\NewsletterSignup;
use App\Models\ReferralUrl;
use App\Models\TrackingWizard;
use GuzzleHttp\Client;
use Carbon\Carbon;
use App\Models\Country;
use Illuminate\Support\Facades\Storage;

class ActiveCampaignService
{
    private $tags = [];
    private $customFields = [];
    private $acHost;
    private $acToken;
    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_UNPROCESSABLE = 422;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_INTERNAL_ERROR = 500;
    const UNREGISTERED_LABEL = 'No registration';
    const NO_PROFILE_LABEL = 'No profile';
    const CHILDBIRTHDAY_LABEL = 'Birth of the baby';
    const NEWSLETTER_LABEL = 'Opt-In Newsletter';
    const COUNTRY_LABEL = 'Country';
    const USERBIRTHDAY_LABEL = 'Userbirthday';
    const DELETED_LABEL = 'Deleted';
    const REFLINK_LABEL = 'Referral Link';
    const LOG_FILE = 'ActiveCampaignSyncInvalidEmails.txt';

    public function __construct()
    {
        $this->acHost = config('activecampaign.host');
        $this->acToken = config('activecampaign.token');
        $this->tags = $this->getAllTags();
        $this->customFields = $this->getAllCustomFields();

        // check if custom-fields exists, if not create them (it's a general thing, NOT per user)
        if(!$this->getCustomFieldsExists(self::USERBIRTHDAY_LABEL)){
            $this->createCustomField(self::USERBIRTHDAY_LABEL);
        }
        if(!$this->getCustomFieldsExists(self::CHILDBIRTHDAY_LABEL)){
            $this->createCustomField(self::CHILDBIRTHDAY_LABEL);
        }
        if(!$this->getCustomFieldsExists(self::NEWSLETTER_LABEL)){
            $this->createCustomField(self::NEWSLETTER_LABEL);
        }
        if(!$this->getCustomFieldsExists(self::COUNTRY_LABEL)){
            $this->createCustomField(self::COUNTRY_LABEL);
        }
        if(!$this->getCustomFieldsExists(self::REFLINK_LABEL)){
            $this->createCustomField(self::REFLINK_LABEL);
        }
    }


    public function addTagToUser($tags,$tagsToRemove,$user){
        try{
        $client = new Client();
        $failedMessages = [];
        $acUserId = 0;
        $acTagId = 0;
        if (strpos($user->email, '+') !== false){
            // found plus (+) in email - this is simply not accepted by AC, but for eg by gmail..
            // since there's nothing we can do, we skip without error, what flips the flag synced
            Storage::append(self::LOG_FILE, '['.Carbon::now().'] '.$user->email);
            return [];
        }
        // search for the user in AC
        $result = $client->get($this->acHost.'/api/3/contacts?email='.$user->email, [
            'headers' => $this->getHeader()
        ]);
        if(self::HTTP_OK == $result->getStatusCode()) {
            // a very few users don't have a profile, so we check that
            $firstName = self::NO_PROFILE_LABEL;
            $lastName = self::NO_PROFILE_LABEL;
            $phone = self::NO_PROFILE_LABEL;
            $childBirthday = self::NO_PROFILE_LABEL;
            $country = self::NO_PROFILE_LABEL;
            $newsletterConfirmed = "0";
            $refLink = self::NO_PROFILE_LABEL;
            $userBirthday = self::NO_PROFILE_LABEL;
            $newsletterSignup = NewsletterSignup::where(['email' => $user->email])->first();
            if(!empty($newsletterSignup)){
                $newsletterConfirmed = "1";
                if($newsletterSignup->created_at<Carbon::createFromDate(2019, 8, 5, 'Europe/Zurich')){
                    if(empty($newsletterSignup->confirmed)){
                        $newsletterConfirmed = "0";
                    }
                }
            }
            if(!empty($user->profile)) {
                $firstName = $user->profile->first_name;
                $lastName = $user->profile->last_name;
                $phone = $user->profile->shipping_phone;
                $userBirthday = explode(' ',$user->profile->customer_birthday)[0];
                $childBirthday = explode(' ',$user->profile->child_birthday)[0];

                $country = ReferralUrl::beautifyCountry($user->profile->billing_country);
                if(!empty($user->profile->billing_country)) {
                    $refLink = 'https://yamo.bio/?ref=' . ReferralUrl::generateReferral($user)->url;
                }
            } else {
                if(!empty($user->incomplete)){
                    $lastName = self::UNREGISTERED_LABEL;
                }
                if(!empty($user->deleted)){
                    $lastName = self::DELETED_LABEL;
                }
            }
            //extract country in a alternate variant if user has no account
            if($user->incomplete){
                $tracking = TrackingWizard::where(['data' => '{"email":"'.$user->email.'"}'])->first();
                if(!empty($tracking)) {
                    $countryCode = explode('/', $tracking->step_identifier)[1];
                    $country = Country::where(['content_code' => $countryCode])->first()->iso_alpha_2;
                }
            }
            if($refLink==self::NO_PROFILE_LABEL && !empty($user->id)){
                $refLinkObject = ReferralUrl::where(['user_id' => $user->id])->first();
                if(!empty($refLinkObject)){
                    $refLink = $refLinkObject->url;
                }
            }
            $newUserDebugMsg = "User is new";
            // if there is no such user we need to create it
            $content = json_decode($result->getBody(), true)["contacts"];
            if(empty($content[0])){
                // create a user!
                $result1 = $this->syncUser($user->email,$firstName,$lastName,$phone);
                if(self::HTTP_UNPROCESSABLE == $result1->getStatusCode()) {
                    Storage::append(self::LOG_FILE, '['.Carbon::now().'] New user '.$user->email. '(422)');
                    return [];
                }
                if(self::HTTP_INTERNAL_ERROR == $result1->getStatusCode()) {
                    Storage::append(self::LOG_FILE, '['.Carbon::now().'] New user '.$user->email.' (500)');
                    return ['failedMessages' => ['Reached a 500-error. Maybe its from AC-side, we retry next time']];
                }
                $newUserDebugMsg = "User is new - ".$result1->getStatusCode();
                if (self::HTTP_CREATED == $result1->getStatusCode() || self::HTTP_OK == $result1->getStatusCode()) {
                    $result = json_decode($result1->getBody(), true)["contact"];
                    // and set the right id (still if non-existed before)
                    $acUserId = $result['id'];
                }
            } else {
                $content = $content[0];
                $newUserDebugMsg = "User is old";
                // if the user existed, set the id
                $acUserId = $content['id'];
                // if something for the user changed, resync user
                if(!empty($user->profile) && ($firstName != $content['firstName'] || $lastName != $content['lastName'] || $phone != $content['phone'])){
                    $result2 = $this->syncUser($user->email,$firstName,$lastName,$phone);
                    if(self::HTTP_UNPROCESSABLE == $result2->getStatusCode()) {
                        Storage::append(self::LOG_FILE, '['.Carbon::now().'] Old user '.$user->email);
                        return [];
                    }
                    if(self::HTTP_INTERNAL_ERROR == $result2->getStatusCode()) {
                        Storage::append(self::LOG_FILE, '['.Carbon::now().'] Old user '.$user->email.' (500)');
                        return ['failedMessages' => ['Reached a 500-error. Maybe its from AC-side, we retry next time']];
                    }
                }

            }
            if(empty($acUserId)){
                app('sentry')->captureMessage('[ActiveCampaignService] No AC-id for email ' . $user->email . ', skip');
                return ['failedMessages' => ['No AC-id for email ' . $user->email . ', skip ('.$newUserDebugMsg.')']];
            }
            // get the user-endpoint with the custom-fields, but by id
            $result = $client->get($this->acHost.'/api/3/contacts/'.$acUserId, [
                'headers' => $this->getHeader()
            ]);

            if(self::HTTP_OK == $result->getStatusCode()) {
                $fields = json_decode($result->getBody(), true)['fieldValues'];
                $userHasFields = [self::USERBIRTHDAY_LABEL => 0, self::CHILDBIRTHDAY_LABEL => 0, self::NEWSLETTER_LABEL => 0, self::COUNTRY_LABEL => 0, self::REFLINK_LABEL => 0];
                foreach($fields as $field){
                    $customFieldName = $this->getCustomFieldNameFromList($field['field']);
                    if(self::USERBIRTHDAY_LABEL==$customFieldName){
                        $userHasFields[self::USERBIRTHDAY_LABEL] = 1;
                        if($field['value']!=$userBirthday){
                            // update that field
                            $this->updateCustomFieldValue($userBirthday,$field['field'],$field['id'],$acUserId);
                        }
                    }

                    if(self::CHILDBIRTHDAY_LABEL==$customFieldName){
                        $userHasFields[self::CHILDBIRTHDAY_LABEL] = 1;
                        if($field['value']!=$childBirthday){
                            // update that field
                            $this->updateCustomFieldValue($childBirthday,$field['field'],$field['id'],$acUserId);
                        }
                    }

                    if(self::NEWSLETTER_LABEL==$customFieldName){
                        $userHasFields[self::NEWSLETTER_LABEL] = 1;
                        if($field['value']!=$newsletterConfirmed){
                            // update that field
                            $this->updateCustomFieldValue($newsletterConfirmed,$field['field'],$field['id'],$acUserId);
                        }
                    }

                    if(self::COUNTRY_LABEL==$customFieldName){
                        $userHasFields[self::COUNTRY_LABEL] = 1;
                        if($field['value']!=$country){
                            // update that field
                            $this->updateCustomFieldValue($country,$field['field'],$field['id'],$acUserId);
                        }
                    }

                    if(self::REFLINK_LABEL==$customFieldName){
                        $userHasFields[self::REFLINK_LABEL] = 1;
                        if($field['value']!=$refLink){
                            // update that field
                            $this->updateCustomFieldValue($refLink,$field['field'],$field['id'],$acUserId);
                        }
                    }
                }

                foreach ($userHasFields as $key => $userHasField) {
                    // If the field was not found last time, we need to create it now!
                    if($userHasField==0){
                        if($key==self::USERBIRTHDAY_LABEL) {
                            $this->createCustomFieldValue($userBirthday, $this->getCustomFieldIdFromList($key), $acUserId);
                        }
                        if($key==self::CHILDBIRTHDAY_LABEL) {
                            $this->createCustomFieldValue($childBirthday, $this->getCustomFieldIdFromList($key), $acUserId);
                        }
                        if($key==self::NEWSLETTER_LABEL) {
                            $this->createCustomFieldValue($newsletterConfirmed, $this->getCustomFieldIdFromList($key), $acUserId);
                        }
                        if($key==self::COUNTRY_LABEL) {
                            $this->createCustomFieldValue($country, $this->getCustomFieldIdFromList($key), $acUserId);
                        }
                        if($key==self::REFLINK_LABEL) {
                            $this->createCustomFieldValue($refLink, $this->getCustomFieldIdFromList($key), $acUserId);
                        }
                    }
                }

            }

        } else {
            return ['failedMessages' => ['Search for contact by email failed, weird status-code: '.$result->getStatusCode()]];
        }

        $tagIds = [];
        // fetch all tag-id's we want to delete afterwards
        foreach($tagsToRemove as $tagToRemove){
            $tagId = $this->getTagIdFromList($tagToRemove);
            if(!empty($tagId)){
                array_push($tagIds, $tagId);
            }
        }
        // allow strings and arrays as addTag-input, propaply not needed anymore..
        $tagArray = $tags;
        if(!is_array($tags)){
            $tagArray = [$tags];
        }

        foreach($tagArray as $tagTitle) {
            // search for the tag
            $tagTitle = trim($tagTitle);
            $acTagId = $this->getTagIdFromList($tagTitle);
            // if tag was not found...
            if (empty($acTagId)) {
                // then create it!
                try{
                    $result = $client->post($this->acHost . '/api/3/tags', [
                        'headers' => $this->getHeader(),
                        'json' => [
                            "tag" => [
                                'tag' => $tagTitle,
                                'tagType' => "contact",
                                "description" => "Automatic created by BE"
                            ]
                        ]
                    ]);
                } catch(\Throwable $ex){
                    echo "Fail at create tag ".$tagTitle;
                    app('sentry')->captureException($ex);
                    $failedMessages[] = "Fail at create tag ".$tagTitle;

                }
                if (self::HTTP_CREATED == $result->getStatusCode()) {
                    $content = json_decode($result->getBody(), true)["tag"];
                    $acTagId = $content['id'];
                    // Add the new created tag to our tag-list so we will not recreate it next round
                    array_push($this->tags,$content);
                }
            }

            // End of tag-handling

            // if all the id's are fetched with success or created, we can do the connection
            if (!empty($acUserId) && !empty($acTagId)) {
                $result = $client->get($this->acHost . '/api/3/contacts/'.$acUserId.'/contactTags', [
                    'headers' => $this->getHeader()
                ]);
                if (self::HTTP_OK == $result->getStatusCode()) {
                    $content = json_decode($result->getBody(), true)['contactTags'];
                    if(!empty($content)){
                        // go through every connection of user and tag
                        foreach($content as $contentpiece){
                            foreach($tagIds as $tagId){
                                if($contentpiece['contact'] == $acUserId && $contentpiece['tag'] == $tagId){
                                    $client->delete($this->acHost . '/api/3/contactTags/' . $contentpiece['id'], [
                                        'headers' => $this->getHeader(),
                                        'http_errors' => false
                                    ]);
                                }
                            }
                        }
                    }
                }
                    $result = $client->post($this->acHost . '/api/3/contactTags', [
                        'headers' => $this->getHeader(),
                        'json' => [
                            "contactTag" => [
                                "contact" => $acUserId,
                                "tag" => $acTagId
                            ]
                        ],
                        'http_errors' => false
                    ]);
                    if (self::HTTP_BAD_REQUEST == $result->getStatusCode()) {
                        $content = json_decode($result->getBody(), true)['errors'][0]['detail'];
                        if(strpos($content, 'contactTag object already exists')!==false){
                            echo "tag-connection already existed, ignoreable: ".$content;
                        } else {
                            echo "another error: ".$content;
                        }
                    }
            }
        }
        } catch(\Throwable $ex){
            echo "[addTagToUser] Generic catch: ".$ex->getMessage();
            app('sentry')->captureException($ex);
            $failedMessages[] = "[addTagToUser] Generic catch:  ".$ex->getMessage();

        }
        return ['failedMessages' => $failedMessages];
    }

    public function getAllTags($offset=0,$reusedTags=[]){
        $client = new Client(); //GuzzleHttp\Client
        // Get all avaible tags from AC, so we search internal for it's id's and save a lot of requests
        $result = $client->get($this->acHost.'/api/3/tags?offset='.$offset, [
            'headers' => $this->getHeader()
        ]);
        $tags = [];
        $internalTags = [];
        if(self::HTTP_OK == $result->getStatusCode()) {
            // if the result was ok, set the tags
            $internalTags = json_decode($result->getBody(), true)["tags"];
            if(!empty($reusedTags)){
                $tags = array_merge($reusedTags,$internalTags);
            } else {
                $tags = $internalTags;
            }
            $total = json_decode($result->getBody(), true)["meta"]["total"];
            if($total>sizeof($tags)){
                $tags = $this->getAllTags(sizeof($tags),$tags);
            }
        }
        return $tags;
    }

    public function getAllCustomFields(){
        $client = new Client(); //GuzzleHttp\Client
        // Get all avaible tags from AC, so we search internal for it's id's and save a lot of requests
        $result = $client->get($this->acHost.'/api/3/fields', [
            'headers' => $this->getHeader()
        ]);
        $fields = [];
        if(self::HTTP_OK == $result->getStatusCode()) {
            // if the result was ok, set the tags
            $fields = json_decode($result->getBody(), true)["fields"];
        }
        return $fields;
    }

    private function getTagIdFromList($tagName){
        $tagName = trim($tagName);
        foreach ($this->tags as $tag){
            if($tag['tag']==$tagName){
                return $tag['id'];
            }
        }
        return null;
    }

    private function updateCustomFieldValue($value, $fieldId, $fieldValueId, $contactId){
        $client = new Client();
        $result = $client->put($this->acHost . '/api/3/fieldValues/'.$fieldValueId, [
            'headers' => $this->getHeader(),
            'json' => [
                "fieldValue" => [
                    "contact" => $contactId,
                    "field" => $fieldId,
                    "value" => $value
                ]
            ]
        ]);
    }

    private function createCustomFieldValue($value, $fieldId, $contactId){
        $client = new Client();
        $result = $client->post($this->acHost . '/api/3/fieldValues', [
            'headers' => $this->getHeader(),
            'json' => [
                "fieldValue" => [
                    "contact" => $contactId,
                    "field" => $fieldId,
                    "value" => $value
                ]
            ]
        ]);
    }

    private function createCustomField($customFieldName){
        $client = new Client();
        $customFieldName = trim($customFieldName);
        $result = $client->post($this->acHost . '/api/3/fields', [
            'headers' => $this->getHeader(),
            'json' => [
                "field" => [
                    'type' => "text",
                    'title' => $customFieldName,
                    "descript" => "Automatic created by BE",
                    "isrequired" => 1,
		            "perstag" => $customFieldName,
		            "defval" => "",
		            "visible" => 1,
		            "ordernum" => 1
                ]
            ]
        ]);
        if(self::HTTP_CREATED == $result->getStatusCode()) {
            // can be done lighter! fast-forward!
            $this->customFields = $this->getAllCustomFields();
        }
    }

    private function syncUser($email,$firstName,$lastName,$phone){
        $client = new Client();
        return $client->post($this->acHost . '/api/3/contact/sync', [
            'headers' => $this->getHeader(),
            'json' => [
                "contact" => [
                    'email' => trim($email),
                    'firstName' => trim($firstName),
                    'lastName' => trim($lastName),
                    'phone' => trim($phone)
                ]
            ],
            'http_errors' => false
        ]);
    }

    private function getCustomFieldsExists($customFieldName){
        foreach ($this->customFields as $customField){
            if($customField['title']==$customFieldName){
                return true;
            }
        }
        return false;
    }

    private function getCustomFieldNameFromList($id){
        foreach ($this->customFields as $customField){
            if($customField['id']==$id){
                return $customField['title'];
            }
        }
        return null;
    }

    private function getCustomFieldIdFromList($name){
        foreach ($this->customFields as $customField){
            if($customField['title']==$name){
                return $customField['id'];
            }
        }
        return null;
    }

    private function getHeader(){
        return [
            'API-Token' => $this->acToken,
            'Content-Type' => 'application/json'
        ];
    }
}