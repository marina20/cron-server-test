<?php


namespace App\Services\LoyaltyProgramme;

use Illuminate\Support\Facades\Facade;

class LoyaltyFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'loyalty';
    }
}