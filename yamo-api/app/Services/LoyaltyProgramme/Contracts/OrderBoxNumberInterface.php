<?php


namespace App\Services\LoyaltyProgramme\Contracts;


use App\Models\User;

/**
 * Interface for Order Box number in Loyalty Programme
 * We want to find current and next valid order box for the customer
 * Interface OrderBoxNumberInterface
 * @package App\Services\LoyaltyProgramme
 */
interface OrderBoxNumberInterface
{
    /**
     * Get current number of boxes that customer has
     * @param User $user
     * @return mixed
     */
    public function current(User $user) : int ;

    /**
     * Calculate number of boxes that customer will have when she buys new one
     * @param User $user
     * @return mixed
     */
    public function next(User $user) : int ;
}