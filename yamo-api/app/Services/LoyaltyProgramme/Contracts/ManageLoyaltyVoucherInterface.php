<?php


namespace App\Services\LoyaltyProgramme\Contracts;


use App\Models\Voucher;

/**
 * Get loyalty voucher from RoyaltyRules
 * Interface ManageLoyaltyVoucherInterface
 * @package App\Services\LoyaltyProgramme
 */
interface ManageLoyaltyVoucherInterface
{
    /**
     * Get Voucher reading royalty rules where criteria is box number
     * @param $boxNumber
     * @return Voucher | null
     */
    public function get(int $boxNumber) : ?Voucher;
}