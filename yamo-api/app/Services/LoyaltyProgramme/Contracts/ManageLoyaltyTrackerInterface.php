<?php


namespace App\Services\LoyaltyProgramme\Contracts;


use App\Models\Order;
use App\Models\Voucher;


/**
 * Save Loyalty Tracker
 * Interface ManageLoyaltyTrackerInterface
 * @package App\Services\LoyaltyProgramme
 */
interface ManageLoyaltyTrackerInterface
{
    /**
     * Save LoyaltyTracker object
     * @param Order $order
     * @param Voucher $voucher
     * @param $boxNumber
     * @return mixed
     */
    public function save(Order $order, Voucher $voucher, int $boxNumber) : void ;
}