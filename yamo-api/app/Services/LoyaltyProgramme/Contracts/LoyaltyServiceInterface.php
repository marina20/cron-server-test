<?php


namespace App\Services\LoyaltyProgramme\Contracts;


use App\Models\Order;
use App\Models\User;

/**
 * Service where we'll manage all aspects of Loyalty Programme
 * Interface LoyaltyServiceInterface
 * @package App\Services\LoyaltyProgramme
 */
interface LoyaltyServiceInterface
{
    /**
     * Apply discount voucher to order and return order
     * @param Order $order
     * @return Order
     */
    public function apply(Order $order) : Order ;

    /**
     * Apply discount to virtual order don't redeem and return order
     * @param Order $order
     * @return Order
     */
    public function onlyDiscount(Order $order) : Order ;

    /**
     * Number of valid boxes for the user
     * @param User $user
     * @return integer
     */
    public function currentBoxNumber(User $user) : int ;

    /**
     * Number of valid boxes incremented by one practically
     * @param User $user
     * @return integer
     */
    public function nextBoxNumber(User $user) : int ;
}