<?php


namespace App\Services\LoyaltyProgramme;

use App\Models\Order;
use App\Models\User;
use App\Models\Voucher;
use App\Services\LoyaltyProgramme\Contracts\LoyaltyServiceInterface;
use Illuminate\Support\Collection;

class LoyaltyService implements LoyaltyServiceInterface
{
    protected $orderBoxNumber;
    protected $manageLoyaltyTracker;
    protected $manageLoyaltyVoucher;

    public function __construct(
        OrderBoxNumber $orderBoxNumber,
        ManageLoyaltyTracker $manageLoyaltyTracker,
        ManageLoyaltyVoucher $manageLoyaltyVoucher)
    {
        $this->orderBoxNumber = $orderBoxNumber;
        $this->manageLoyaltyTracker = $manageLoyaltyTracker;
        $this->manageLoyaltyVoucher = $manageLoyaltyVoucher;
    }

    public function apply(Order $order) : Order
    {
        $nextBoxNumber = $this->nextBoxNumber($order->user);
        $voucher = $this->manageLoyaltyVoucher->get($nextBoxNumber);

        if(empty($voucher))
            return $order;

        // if we want to skip adding loyalty voucher and we already had it in loyalty tracker
        // we'll check if status of tracker is rollback
        if($this->manageLoyaltyTracker->isRollback($order, $voucher))
            return $order;

        // so if the loyalty voucher already was used for this order reapply it
        // only if it's still pending in the loyalty tracker
        // idea behind this is to practically check if voucher is still pending, rollback and confirmed would be skipped
        // and to avoid creating new loyalty tracker entry for the same order
        if(empty($this->manageLoyaltyTracker->getPending($order, $voucher)))
            $result = Voucher::applyVoucher($voucher->code, $order);
        else
            return $this->reapply($order, $voucher);

        if($result['success'])
            $this->manageLoyaltyTracker->save($order, $voucher, $nextBoxNumber);

        return $result['order'];
    }

    public function reapply(Order $order, Voucher $voucher) : Order
    {
        if(empty($voucher))
            return $order;

        $result = Voucher::applyVoucher($voucher->code, $order);

        return $result['order'];
    }

    public function onlyDiscount(Order $order) : Order
    {
        $voucher = $this->manageLoyaltyVoucher->get($this->nextBoxNumber($order->user));

        if(empty($voucher))
            return $order;

        $result = Voucher::checkVoucher($voucher->code, $order);

        return $result['order'];
    }

    public function currentBoxNumber(User $user) : int
    {
        return $this->orderBoxNumber->current($user);
    }

    public function nextBoxNumber(User $user) : int
    {
        return $this->orderBoxNumber->next($user);
    }

    public function getFreeItems(Order $order) : ?Collection
    {
        $nextBoxNumber = $this->nextBoxNumber($order->user);
        $voucher = $this->manageLoyaltyVoucher->get($nextBoxNumber);
        if(empty($voucher))
            return new Collection();

        return $voucher::getFreeItems($voucher->code);
    }
}