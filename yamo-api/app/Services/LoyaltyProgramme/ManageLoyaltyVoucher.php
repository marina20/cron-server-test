<?php


namespace App\Services\LoyaltyProgramme;

use App\Models\Voucher;
use App\Models\LoyaltyRule;
use App\Services\LoyaltyProgramme\Contracts\ManageLoyaltyVoucherInterface;

/**
 * Manage loyalty vouchers
 * Class ManageLoyaltyVoucher
 * @package App\Services\LoyaltyProgramme
 */
class ManageLoyaltyVoucher implements ManageLoyaltyVoucherInterface
{
    /**
     * Get loyalty voucher from RoyaltyRules
     * @param int $boxNumber
     * @return Voucher|null
     */
    public function get(int $boxNumber) : ?Voucher
    {
        $loyaltyRule = $this->getRule($boxNumber);

        if(empty($loyaltyRule))
            return null;

        return $loyaltyRule->voucher;
    }

    /**
     * Get Loyalty Rule object
     * @param int $boxNumber
     * @return LoyaltyRule|null
     */
    private function getRule(int $boxNumber) : ?LoyaltyRule
    {
        return LoyaltyRule::where('box_number', $boxNumber)->first();
    }
}