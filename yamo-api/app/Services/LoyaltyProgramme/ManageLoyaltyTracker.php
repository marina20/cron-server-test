<?php


namespace App\Services\LoyaltyProgramme;

use App\Models\Order;
use App\Models\Voucher;
use App\Models\LoyaltyTracker;
use App\Models\VoucherRedemption;
use App\Services\LoyaltyProgramme\Contracts\ManageLoyaltyTrackerInterface;

/**
 * Track loyalty voucher redemptions
 * Class ManageLoyaltyTracker
 * @package App\Services\LoyaltyProgramme
 */
class ManageLoyaltyTracker implements ManageLoyaltyTrackerInterface
{
    /**
     * Save order, voucher and box number into LoyaltyTracker table
     * @param Order $order
     * @param Voucher $voucher
     * @param int $boxNumber
     */
    public function save(Order $order, Voucher $voucher, int $boxNumber) : void
    {
        $tracker = new LoyaltyTracker();
        $tracker->order_id = $order->id;
        $tracker->user_id = $order->user_id;
        $tracker->voucher_id = $voucher->id;
        $tracker->box_number = $boxNumber;
        $tracker->redemption_id = $this->getRedemptionId($order->id, $voucher->id);
        $tracker->status = LoyaltyTracker::LOYALTY_TRACKER_STATUS_PENDING;
        $tracker->save();
    }

    /**
     * Get Voucher redemption id
     * @param int $orderId
     * @param int $voucherId
     * @return int|null
     */
    protected function getRedemptionId(int $orderId, int $voucherId) : ?int
    {
        $voucherRedemption = VoucherRedemption::where('order_id',$orderId)
            ->where('voucher_id', $voucherId)
            ->first();

        if(!isset($voucherRedemption->id))
            return null;

        return $voucherRedemption->id;
    }

    /**
     * Find and return LoyaltyTracker object
     * @param Order $order
     * @param Voucher $voucher
     * @return LoyaltyTracker|null
     */
    public function get(Order $order, Voucher $voucher) : ?LoyaltyTracker
    {
        return LoyaltyTracker::where('order_id',$order->id)
            ->where('voucher_id', $voucher->id)
            ->first();
    }

    /**
     * Find and return LoyaltyTracker object
     * @param Order $order
     * @param Voucher $voucher
     * @return LoyaltyTracker|null
     */
    public function getPending(Order $order, Voucher $voucher) : ?LoyaltyTracker
    {
        return LoyaltyTracker::where('order_id',$order->id)
            ->where('voucher_id', $voucher->id)
            ->where('status', LoyaltyTracker::LOYALTY_TRACKER_STATUS_PENDING)
            ->first();
    }

    /**
     * Check for rollback in loyalty tracker
     * @param Order $order
     * @param Voucher $voucher
     * @return bool
     */
    public function isRollback(Order $order, Voucher $voucher) : bool
    {
        $trackedLoyalty = LoyaltyTracker::where('order_id',$order->id)
            ->where('voucher_id', $voucher->id)
            ->where('status', LoyaltyTracker::LOYALTY_TRACKER_STATUS_ROLLBACK)
            ->get()
            ->count();

        if($trackedLoyalty == 0)
            return false;

        return true;
    }
}