<?php


namespace App\Services\LoyaltyProgramme;

use App\Models\User;
use App\Services\LoyaltyProgramme\Contracts\OrderBoxNumberInterface;
use App\Models\Order;

/**
 * Class OrderBoxNumber
 * @package App\Services\LoyaltyProgramme
 */
class OrderBoxNumber implements OrderBoxNumberInterface
{
    /**
     * Get current order number of valid bought boxes for the user
     * @param User $user
     * @return int
     */
    public function current(User $user) : int
    {
        $count =  Order::where('user_id',$user->id)
            ->whereNotNull('paid_date')
            ->where('resent',0)
            ->count();

        return $count;
    }

    /**
     * Get order number of the next box when customer buys it
     * @param User $user
     * @return int
     */
    public function next(User $user) : int
    {
        $count = self::current($user);
        return ++$count;
    }
}