<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 11/27/17
 * Time: 1:55 PM
 */

namespace App\Services;

use App\Models\Order;
use Auth;
use Helpers;

class CreateAdyenLinkService
{
    public function make(Order $order, $rurl=null)
    {
        $shipping_address_calc = $this->splitStreet($order->getShippingStreet());
        $billing_address_calc  = $this->splitStreet($order->getBillingStreet());

        $params = [
            "merchantReference" => $order->id,
            "paymentAmount"     => $order->order_total*100,
            "sessionValidity"   => date('c', strtotime('+12 hours')), // one hour from now, YYYY-MM-DDThh:mm:ssTZD
            "shipBeforeDate"    => $order->delivery_date->format('Y-m-d'),
            "shopperEmail"      => $order->user->email,
            "shopperReference"  => $order->user->id,
            "recurringContract" => "RECURRING,ONECLICK",
            "allowedMethods"    => env('ADYEN_DEFAULT_ALLOWED_METHODS'),

            // Shopper information
            "shopper.firstName"=> $order->shipping_first_name,
            "shopper.lastName"=> $order->shipping_last_name,

            // Billing Address fields (used for AVS checks)
            "billingAddress.street" =>$billing_address_calc['street'],
            "billingAddress.houseNumberOrName" => env('ADYEN_DEFAULT_HOUSE_NUMBER'),
            "billingAddress.city" => $order->billing_city,
            "billingAddress.postalCode" => $order->billing_postcode,
            "billingAddress.country" => $order->getCountryCode($order->billing_country), // country must be country code
            "billingAddressType" => env('ADYEN_DEFAULT_ADDRESS_TYPE'),// there are types "", 1 and 2

            // Delivery/Shipping Address fields
            "deliveryAddress.street" => $shipping_address_calc['street'],
            "deliveryAddress.houseNumberOrName" => env('ADYEN_DEFAULT_HOUSE_NUMBER'),
            "deliveryAddress.city" => $order->shipping_city,
            "deliveryAddress.postalCode" => $order->shipping_postcode,
            "deliveryAddress.country" => $order->getCountryCode($order->shipping_country),// country must be country code
            "deliveryAddressType" => env('ADYEN_DEFAULT_ADDRESS_TYPE'), // there are types "", 1 and 2

            "currencyCode"      => $order->getCurrencyCode($order->currency),
        ];

        if(!empty($billing_address_calc['street-number'])) {
            $params["billingAddress.houseNumberOrName"] = $billing_address_calc['street-number'];
        }
        if(!empty($shipping_address_calc['street-number'])) {
            $params["deliveryAddress.houseNumberOrName"] = $shipping_address_calc['street-number'];
        }

        return $this->returnAdyenUrl($params, $rurl);
    }

    public function once(Order $order, $rurl=null)
    {
        $shipping_address_calc = $this->splitStreet($order->getShippingStreet());
        $billing_address_calc  = $this->splitStreet($order->getBillingStreet());

        $params = [
            "merchantReference" => $order->id,
            "paymentAmount"     => $order->order_total*100,
            "sessionValidity"   => date('c', strtotime('+12 hours')), // one hour from now, YYYY-MM-DDThh:mm:ssTZD
            "shipBeforeDate"    => $order->delivery_date->format('Y-m-d'),
            "shopperEmail"      => $order->user->email,
            "shopperReference"  => $order->user->id,
            "recurringContract" => "ONECLICK",
            "allowedMethods"    => env('ADYEN_DEFAULT_ALLOWED_METHODS'),

            // Shopper information
            "shopper.firstName"=> $order->shipping_first_name,
            "shopper.lastName"=> $order->shipping_last_name,

            // Billing Address fields (used for AVS checks)
            "billingAddress.street" =>$billing_address_calc['street'],
            "billingAddress.houseNumberOrName" => env('ADYEN_DEFAULT_HOUSE_NUMBER'),
            "billingAddress.city" => $order->billing_city,
            "billingAddress.postalCode" => $order->billing_postcode,
            "billingAddress.country" => $order->getCountryCode($order->billing_country), // country must be country code
            "billingAddressType" => env('ADYEN_DEFAULT_ADDRESS_TYPE'),// there are types "", 1 and 2

            // Delivery/Shipping Address fields
            "deliveryAddress.street" => $shipping_address_calc['street'],
            "deliveryAddress.houseNumberOrName" => env('ADYEN_DEFAULT_HOUSE_NUMBER'),
            "deliveryAddress.city" => $order->shipping_city,
            "deliveryAddress.postalCode" => $order->shipping_postcode,
            "deliveryAddress.country" => $order->getCountryCode($order->shipping_country),// country must be country code
            "deliveryAddressType" => env('ADYEN_DEFAULT_ADDRESS_TYPE'), // there are types "", 1 and 2

            "currencyCode"      => $order->getCurrencyCode($order->currency),
        ];

        if(!empty($billing_address_calc['street-number'])) {
            $params["billingAddress.houseNumberOrName"] = $billing_address_calc['street-number'];
        }
        if(!empty($shipping_address_calc['street-number'])) {
            $params["deliveryAddress.houseNumberOrName"] = $shipping_address_calc['street-number'];
        }

        return $this->returnAdyenUrl($params, $rurl);
    }

    protected function returnAdyenUrl($params, $rurl=null)
    {
        $skinCode        = env('ADYEN_SKIN_CODE');
        $merchantAccount = env('ADYEN_MERCHANT_ACCOUNT');
        $hmacKey         = env('ADYEN_HMAC');

        // payment-specific details

        $paramsToAdd = array(
            "merchantAccount"   =>  $merchantAccount,
            "shopperLocale"     => env('ADYEN_DEFAULT_LOCALE'),
            "skinCode"          => $skinCode,
            "skipSelection"     => env('ADYEN_SKIP_SELECTION'),
            "resURL"            => empty($rurl) ? Helpers::adyen_rurl() : $rurl
        );

        $params = array_merge($params, $paramsToAdd);

        // The character escape function
        $escapeval = function($val) {
            return str_replace(':','\\:',str_replace('\\','\\\\',(string)$val));
        };

        // Sort the array by key using SORT_STRING order
        ksort($params, SORT_STRING);

        // Generate the signing data string
        $signData = implode(":",array_map($escapeval,array_merge(array_keys($params), array_values($params))));

        // base64-encode the binary result of the HMAC computation
        $merchantSig = base64_encode(hash_hmac('sha256',$signData,pack("H*" , $hmacKey),true));

        $params["merchantSig"] = $merchantSig;

        $paramsString = http_build_query($params);

        return env('ADYEN_URL') . '?' . $paramsString;
    }

    protected function splitStreet($street){
        $address = $street;
        $matches = array();

        $regex = '
           /\A\s*
           (?: #########################################################################
               # Option A: [<Addition to address 1>] <House number> <Street name>      #
               # [<Addition to address 2>]                                             #
               #########################################################################
               (?:(?P<A_Addition_to_address_1>.*?),\s*)? # Addition to address 1
           (?:No\.\s*)?
               (?P<A_House_number>\pN+[a-zA-Z]?(?:\s*[-\/\pP]\s*\pN+[a-zA-Z]?)*) # House number
           \s*,?\s*
               (?P<A_Street_name>(?:[a-zA-Z]\s*|\pN\pL{2,}\s\pL)\S[^,#]*?(?<!\s)) # Street name
           \s*(?:(?:[,\/]|(?=\#))\s*(?!\s*No\.)
               (?P<A_Addition_to_address_2>(?!\s).*?))? # Addition to address 2
           |   #########################################################################
               # Option B: [<Addition to address 1>] <Street name> <House number>      #
               # [<Addition to address 2>]                                             #
               #########################################################################
               (?:(?P<B_Addition_to_address_1>.*?),\s*(?=.*[,\/]))? # Addition to address 1
               (?!\s*No\.)(?P<B_Street_name>[^0-9# ]\s*\S(?:[^,#](?!\b\pN+\s))*?(?<!\s)) # Street name
           \s*[\/,]?\s*(?:\sNo\.)?\s*
               (?P<B_House_number>\pN+\s*-?[a-zA-Z]?(?:\s*[-\/\pP]?\s*\pN+(?:\s*[\-a-zA-Z])?)*|
               [IVXLCDM]+(?!.*\b\pN+\b))(?<!\s) # House number
           \s*(?:(?:[,\/]|(?=\#)|\s)\s*(?!\s*No\.)\s*
               (?P<B_Addition_to_address_2>(?!\s).*?))? # Addition to address 2
           )
           \s*\Z/xu';
        $result = preg_match($regex, $address, $matches);
        if ($result === 0 || $result === false) {
            return array(
                'street-addition-1' => "",
                'street' => $street,
                'street-number' => "",
                'street-addition-2' => ""
            );
        }
        if (!empty($matches['A_Street_name'])) {
            return array(
                'street-addition-1' => $matches['A_Addition_to_address_1'],
                'street' => $matches['A_Street_name'],
                'street-number' => $matches['A_House_number'],
                'street-addition-2' => (isset($matches['A_Addition_to_address_2'])) ? $matches['A_Addition_to_address_2'] : ''
            );
        }
        else {
            return array(
                'street-addition-1' => $matches['B_Addition_to_address_1'],
                'street' => $matches['B_Street_name'],
                'street-number' => $matches['B_House_number'],
                'street-addition-2' => isset($matches['B_Addition_to_address_2']) ? $matches['B_Addition_to_address_2'] : ''
            );
        }
    }
}