<?php

namespace App\Services\Voucher;

use App\Http\Resources\ProductResource;
use App\Models\Country;
use App\Models\Voucher;
use App\Traits\FormatMoney;
use Auth;
use Helpers;

class VoucherService
{
    use FormatMoney;

    const BIG_CUP_SIZE = 200;
    const BIG_FIRST_SUBSCRIPTION_PREFIX = 'FIRST_SUBSCRIPTION_200_';
    const FIRST_SUBSCRIPTION_PREFIX = 'FIRST_SUBSCRIPTION_';
    const BIG_SUBSCRIPTION_PREFIX = 'SUBSCRIPTION_200_';
    const SUBSCRIPTION_PREFIX = 'SUBSCRIPTION_';
    /**
     * This method does get the percent and discount by size
     * @param int $size
     * @param float $price
     * @param Country|null $country
     * @return array
     */
    public static function applyFirstSubscriptionDiscount(int $size, float $price, Country $country=null) : array {
        if(empty($country)){
            $country = Helpers::country();
        }
        $countryIsoAlpha2 = $country->iso_alpha_2;
        if(self::BIG_CUP_SIZE===$size){
            $result = Voucher::getDiscountedAmount(self::BIG_FIRST_SUBSCRIPTION_PREFIX.$countryIsoAlpha2,$price);
        } else {
            $result = Voucher::getDiscountedAmount(self::FIRST_SUBSCRIPTION_PREFIX.$countryIsoAlpha2,$price);
        }
        return $result;
    }

    public static function applySubscriptionDiscount(int $size, float $price, Country $country=null) : array {
        if(empty($country)){
            $country = Helpers::country();
        }
        $countryIsoAlpha2 = $country->iso_alpha_2;
        if(self::BIG_CUP_SIZE===$size){
            $result = Voucher::getDiscountedAmount(self::BIG_SUBSCRIPTION_PREFIX.$countryIsoAlpha2,$price);
        } else {
            $result = Voucher::getDiscountedAmount(self::SUBSCRIPTION_PREFIX.$countryIsoAlpha2,$price);
        }
        return $result;
    }

    /**
     * Go through box-items and calculate total and, more important, discounted price.
     * @param $items
     * @return array ['total','discounted']
     */
    public static function getFirstSubscriptionDiscountedSumAndTotal($items) : array {
        $total = 0;
        $discounted = 0;
        foreach($items as $item){
            $product = $item->product;
            $total += $product->price * $item->quantity;
            $discounted += self::getProductPriceFirstSubscriptionDiscounted($product) * $item->quantity;
        }
        return ['total' => $total, 'discounted' => self::formatMoney($discounted)];
    }


    /**
     * Go through box-items and calculate total and, more important, discounted price.
     * @param $items
     * @return array ['total','discounted']
     */
    public static function getSubscriptionDiscountedSumAndTotal($items) : array {
        $total = 0;
        $discounted = 0;
        foreach($items as $item){
            $product = $item->product;
            $total += $product->price * $item->quantity;
            $discounted += self::getProductPriceSubscriptionDiscounted($product) * $item->quantity;
        }
        return ['total' => $total, 'discounted' => self::formatMoney($discounted)];
    }

    /**
     * Get first-subscription-discount by a product
     * @param Product|ProductResource $product
     * @return string
     */
    public static function getProductPriceFirstSubscriptionDiscounted($product)
    {
        $result = self::applyFirstSubscriptionDiscount($product->size,$product->price);
        $discountedPrice = $product->price - $result['discount'];
        return self::formatMoney($discountedPrice);
    }

    public static function getProductPriceSubscriptionDiscounted($product)
    {
        $result = self::applySubscriptionDiscount($product->size,$product->price);
        $discountedPrice = $product->price - $result['discount'];
        return self::formatMoney($discountedPrice);
    }

    /**
     * Get first or regular subscription discount including total
     * @param $product
     * @return string
     */
    public static function getProductPriceFirstOrRegularSubscriptionDiscountedAndTotal($items)
    {
        if (!Auth::check() || Auth::user()->isNew()) {
            return VoucherService::getFirstSubscriptionDiscountedSumAndTotal($items);
        } else {
            return VoucherService::getSubscriptionDiscountedSumAndTotal($items);
        }
    }

    /**
     * Get first or regular subscription discount
     * @param $product
     * @return string
     */
    public static function getProductPriceFirstOrRegularSubscriptionDiscounted($product)
    {
        if (!Auth::check() || Auth::user()->isNew()) {
            return VoucherService::getProductPriceFirstSubscriptionDiscounted($product);
        } else {
            return VoucherService::getProductPriceSubscriptionDiscounted($product);
        }
    }

    private static function formatMoney($value)
    {
        return number_format($value,2);
    }
}