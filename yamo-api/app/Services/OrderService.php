<?php
/**
 * Created by PhpStorm.
 * User: milenabasaragin
 * Date: 10/4/18
 * Time: 3:23 PM
 */

namespace App\Services;

use App\Models\Country;
use App\Models\Order;
use App\Models\ReferralUrl;
use App\Models\Subscription;
use App\Services\OrderReport\OrderReportBox;
use App\Services\OrderReport\Transformer\TransformerGermany;
use App\Services\OrderReport\Transformer\TransformerSwitzerland;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class OrderService
{
    /**
     * Change delivery date of given order
     *
     * @param Order $order
     * @param Carbon $deliveryDate update current delivery date with new delivery date
     */
    static public function changeDeliveryDate(Order $order, Carbon $deliveryDate)
    {
        $order->delivery_date = $deliveryDate;
        $order->cancellation_deadline = $deliveryDate->copy()
            ->subDays(Order::ORDER_SUBSCRIPTION_CANCELLATION_PERIOD)->endOfDay()->format('Y-m-d H:i:s');
        if ($order->subscription)
            SubscriptionService::changeNextPayment($order->subscription, $deliveryDate->copy()
                ->subDays(Subscription::SCHEDULE_NEXT_PAYMENT_DAYS));
        $order->save();
    }

    static public function findAndCancelOrderDueToRefund($order_id, $event_code)
    {
        $order = Order::find($order_id);
        if ($order) {
            $order->adyen_auth_result = $event_code;
            $order->save();
        }
    }

    static public function generateSKU(Order $order)
    {
        if ($order->country->iso_alpha_2 === Country::COUNTRY_SWITZERLAND) {
            $transformer = new TransformerSwitzerland(new Collection(), new OrderReportBox());
        }
        else {
            $transformer = new TransformerGermany(new Collection(), new OrderReportBox());
        }
        //@TODO fix sku generate
        // return 'test';
        return $transformer->generateAndSaveSKU($order);
    }
}
