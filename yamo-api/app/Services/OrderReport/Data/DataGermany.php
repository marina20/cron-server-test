<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/31/18
 * Time: 12:17 PM
 */
namespace App\Services\OrderReport\Data;

use App\Models\Order;

class DataGermany extends Data implements DataInterface
{
    const ISO_ALPHA_2 = 'DE';
}