<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/31/18
 * Time: 12:52 PM
 */

namespace App\Services\OrderReport\Data;

class DataFactory
{
    public function get($iso_alpha_2)
    {
        switch($iso_alpha_2)
        {
            case DataGermany::ISO_ALPHA_2 :
                return new DataGermany();
                break;
            case DataSwitzerland::ISO_ALPHA_2 :
                return new DataSwitzerland();
                break;
            case DataAustria::ISO_ALPHA_2 :
                return new DataAustria();
                break;
        }
    }
}