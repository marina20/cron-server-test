<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/31/18
 * Time: 12:17 PM
 */
namespace App\Services\OrderReport\Data;

class DataAustria extends Data implements DataInterface
{
    const ISO_ALPHA_2 = 'AT';
}