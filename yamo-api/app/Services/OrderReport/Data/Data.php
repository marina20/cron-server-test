<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/31/18
 * Time: 12:24 PM
 */
namespace App\Services\OrderReport\Data;

use App\Models\Country;
use App\Models\Order;

class Data implements DataInterface
{
    public $country_id;
    protected $date;
    protected $created_via_methods_to_get_date = [
        Order::CREATED_VIA_SUBSCRIPTION,
        Order::CREATED_VIA_SINGLE_BOX
    ];
    const ISO_ALPHA_2 = 'DE';

    public function __construct()
    {
        $country = Country::where('iso_alpha_2',$this::ISO_ALPHA_2)->first();
        $this->country_id = $country->id;
    }

    public function get()
    {
        $orders = Order::query()
            ->select('orders.*')
            ->leftJoin('subscriptions','subscriptions.order_id','=','orders.id')
            ->whereDate('orders.delivery_date',$this->date)
            ->where('country_id',$this->country_id)
            ->where('orders.status', Order::STATUS_PAYED)
            ->whereIn('orders.created_via', $this->created_via_methods_to_get_date)
            ->orderBy('orders.created_via','desc')
            ->orderBy('orders.id','asc')
            ->get();

        return $orders;
    }

    public function set_date($date)
    {
        $this->date = $date;
        return $this;
    }
}