<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/31/18
 * Time: 12:14 PM
 */
namespace App\Services\OrderReport\Data;

interface DataInterface
{
    public function get();
}