<?php

namespace App\Services\OrderReport\Transformer;

use App\Models\Country;
use App\Models\Order;
use App\Models\Postcode;
use App\Models\PostcodesTransformation;
use Illuminate\Database\Eloquent\Collection;

class TransformerSwitzerland extends Transformer implements TransformerInterface
{
    const COUNTRY = Country::COUNTRY_SWITZERLAND;

    public function __construct(Collection $order_report_data) {
        parent::__construct($order_report_data);
    }

    /**
     * Transform array of orders into array suitable to display in excel file template template-frigosuisse-addresslabel.xlsx
     * @return array|Collection
     */
    public function transform()
    {
        try {
            $code_formula_array = [
                'K3' => 0,
                'K4' => 0,
                'K5' => 0,
                'R' => 0,
                'B' => 0,
                'A' => 0,
                'P' => 0,
                'E' => 0,
                'Z' => 0,
                'M' => 0,
                'N' => 0,
                'H' => 0,
                'F' => 0,
                'L' => 0,
                'S' => 0,
                'O' => 0
            ];

            $adresslabel_array = [];

            foreach ($this->order_data as $item) {
                $summary_row = [];

                $box_content = $this->order_report_box->content($item);

                $summary_row['name'] = Order::ORDER_BOX_PRODUCT_NAME;
                $summary_row['brei amount'] = 0;
                $summary_row['Cooling bag'] = $this->hasCoolingBag($item) ? $this::COOLING_BAG_POSITIVE : $this::COOLING_BAG_NEGATIVE;
                $summary_row['order number'] = $this->selectOrderNumber($item);

                $summary_row = array_replace($summary_row, $box_content);
                // Cooling bag
                if ($summary_row['Cooling bag'] == $this::COOLING_BAG_POSITIVE) {
                    $summary_row['Q'] = '1';
                    $box_content['Q'] = '1';
                } else {
                    $summary_row['Q'] = '0';
                    $box_content['Q'] = '0';
                }

                // Communication / Flyer
                if ($summary_row['order number'] == $this::FIRST_ORDER_NUMBER) {
                    $summary_row['O'] = '1';
                    $box_content['O'] = '1';

                    $code_formula_array['O'] += 1;
                } else {
                    $summary_row['O'] = '0';
                    $box_content['O'] = '0';
                }
                $summary_row['N'] = '1';
                $box_content['N'] = '1';
                foreach ($box_content as $key => $value) {
                    if (isset($code_formula_array[$key]) && !in_array($key, ['O'])) {
                        $code_formula_array[$key] += $value;
                    }
                }
                $code = $this->generateAddressCode($summary_row, $item);

                // implement ExtraProducts service to change SKU only to add additional products
                $code = $this->extraProductsService->transform($code, $item->id);

                $meal_swap_1 = $this->generateMealSwapCode($code); //if returns an array then get the second chunk, otherwise is done.
	            $meal_swap_2 = null;
	            if(is_array($meal_swap_1)){
		            $meal_swap_2 = $meal_swap_1[1];
	            	$meal_swap_1 = $meal_swap_1[0];
	            }

	            $city = $item->shipping_city;
	            $postCode = Postcode::where(['city' => $city,'postcode' => $item->shipping_postcode])->first();
	            if(!empty($postCode)){
                    $postcodeTransformation = PostcodesTransformation::where(['postcode_id' => $postCode->id])->first();
                    if(!empty($postcodeTransformation)){
                        $city = $postcodeTransformation->alias;
                    }
                }



                $adresslabel_row = [
                	'date' => $item->delivery_date->subDay(1)->format('d.m.Y'), //Day when frigosuisse should have the item ready.
	                'cod' => $this->generateCod($item),
	                'meal_swap_1' => $meal_swap_1,
	                'meal_swap_2' => $meal_swap_2,
                    'code' => $code,
	                'customer_id' => $item->user_id,
                    'company' => $item->shipping_company,
                    'firstname' => $item->shipping_first_name,
                    'lastname' => $item->shipping_last_name,
                    'street' => $item->getShippingStreet(),
                    'zip_code' => $item->shipping_postcode,
                    'city' => $city,
                    'email' => $item->shipping_email
                ];
                $adresslabel_array[] = $adresslabel_row;
            }

            return [
                'addresslabel' => $adresslabel_array
            ];
        }
        catch (\Exception $exception)
        {
            app('sentry')->captureException($exception);
        }
    }
}