<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/31/18
 * Time: 2:28 PM
 */

namespace App\Services\OrderReport\Transformer;

use App\Services\OrderReport\Data\DataInterface;

interface TransformerInterface
{
    public function transform();
}