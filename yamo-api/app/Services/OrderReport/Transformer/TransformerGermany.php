<?php


namespace App\Services\OrderReport\Transformer;

use App\Models\Country;
use App\Models\Order;
use Illuminate\Database\Eloquent\Collection;

class TransformerGermany extends Transformer implements TransformerInterface
{
	const COUNTRY = Country::COUNTRY_GERMANY;
	const NUMBER_OF_BOXES = '1';
    const LOCATION_AT_THE_DOOR = 'vor der Haustüre';
    public function __construct(Collection $order_report_data) {
        parent::__construct($order_report_data);
    }

    public function transform()
    {
        try {
            $template_row = [
                'A' => '',
                'B' => '',
                'C' => '',
                'D' => '',
                'E' => '',
                'F' => '',
                'G' => '',
                'H' => '',
                'I' => '',
                'J' => '',
                'K' => '',
                'L' => '',
                'M' => '',
                'N' => '',
                'O' => '',
            ];

            $transformed = [];

            $columns_to_fill = [
                'A' => '', // ext_id : order ID
                'B' => '', // name1 : shipping first name + shipping last name
                'C' => '', // name2 : shipping company
                'D' => '', // adresse : shipping address
                'E' => '', // plz : shipping postcode
                'F' => '', // ort : shipping city
                'G' => '', // land_iso2 : shipping country ISO → DE, AT
                'H' => '', // CHartikelnummer : SKU →  we will use SWISS format, so now long SKU format for all countries
                'I' => '', // artikelname: product name
                'J' => '', // email : shipping email
                'K' => '', // telefon : shipping phone
                'L' => '', // kundenreferenz : user ID
                'M' => '', // deposit
                'N' => '', // deposit-place
                'O' => '', // delivery_date
            ];

            foreach ($this->order_data as $item) {
                $item_fill = $columns_to_fill;
                $item_fill['A'] = 'B2C' . $item->id;
                $item_fill['B'] = $item->shipping_first_name . ' ' . $item->shipping_last_name;
                $item_fill['C'] = $item->shipping_company;
                $item_fill['D'] = $item->shipping_street_name." ".$item->shipping_street_nr;
                if(empty($item->shipping_street_nr)){
                    $item_fill['D'] = $item->shipping_street_name;
                }
                if(empty($item->shipping_street_name)){
                    $item_fill['D'] = $item->shipping_address_1;
                }
                $item_fill['E'] = $item->shipping_postcode;
                $item_fill['F'] = $item->shipping_city;
                $item_fill['G'] = $item->shipping_country === 'Deutschland' ? 'DE' : 'AT';
                $item_fill['I'] = Order::ORDER_BOX_PRODUCT_NAME;
                $item_fill['J'] = $item->shipping_email;
                $item_fill['K'] = $item->shipping_phone;
                $item_fill['L'] = $item->user_id;
                $item_fill['M'] = $item->deposit_authorisation;
                $item_fill['N'] = !empty($item->deposit_location) ? $item->deposit_location : self::LOCATION_AT_THE_DOOR;
                $item_fill['O'] = $item->delivery_date->format('d.m.Y');;
                $summary_row = [];
                $box_content = $this->order_report_box->content($item);

                $summary_row['name'] = Order::ORDER_BOX_PRODUCT_NAME;
                $summary_row['brei amount'] = 0;
                $summary_row['Cooling bag'] = $this->hasCoolingBag($item) ? $this::COOLING_BAG_POSITIVE : $this::COOLING_BAG_NEGATIVE;
                $summary_row['order number'] = $this->selectOrderNumber($item);

                $summary_row = array_replace($summary_row, $box_content);
                if ($summary_row['Cooling bag'] == $this::COOLING_BAG_POSITIVE) {
                    $summary_row['Q'] = '1';
                } else {
                    $summary_row['Q'] = '0';
                }

                if ($summary_row['order number'] == $this::FIRST_ORDER_NUMBER) {
                    $summary_row['O'] = '1';
                } else {
                    $summary_row['O'] = '0';
                }


                if (empty($item->SKU)) {
                    $item_fill['H'] = parent::generateAddressCode($summary_row, $item);
                } else {
                    $item_fill['H'] = $item->SKU;
                }

                // implement ExtraProducts service to change SKU only to add additional products
                $item_fill['H'] = $this->extraProductsService->transform($item_fill['H'], $item->id);

                parent::generateCod($item);
                $transformed[] = array_replace($template_row, $item_fill);
            }

            return [
                'addresslabel' => $transformed
            ];
        }
        catch (\Exception $exception)
        {
            app('sentry')->captureException($exception);
        }
    }
}