<?php


namespace App\Services\OrderReport\Transformer;

use App\Models\Country;
use Illuminate\Database\Eloquent\Collection;

class TransformerAustria extends Transformer implements TransformerInterface
{
	const COUNTRY = Country::COUNTRY_AUSTRIA;

	public function __construct(Collection $order_report_data) {
        parent::__construct($order_report_data);
    }

    public function transform()
    {
        return $this->order_report_data;
    }
}