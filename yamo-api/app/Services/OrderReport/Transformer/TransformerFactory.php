<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/31/18
 * Time: 2:34 PM
 */

namespace App\Services\OrderReport\Transformer;

use App\Services\OrderReportService;
use Illuminate\Database\Eloquent\Collection;

class TransformerFactory
{
    public function get(Collection $data, string $factory)
    {
        if($factory === OrderReportService::PROWITO)
        {
            return new TransformerGermany($data);
        }
        else
        {
            return new TransformerSwitzerland($data);
        }
    }
}