<?php


namespace App\Services\OrderReport\Transformer;

use App\Models\Order;
use App\Models\Product;
use App\Services\ExtraProducts;
use App\Services\OrderReport\OrderReportBox;
use function GuzzleHttp\Psr7\str;
use Illuminate\Database\Eloquent\Collection;

class Transformer
{
    protected $order_report_data;
    protected $order_data;
    protected $extraProductsService;
	const COUNTRY = '';
	const B2C = 1;
	const B2B = 2;
	const VIP = 3;
	const GENERATION_IDENTIFIER_AUTOMATIC = 'A';

    const FIRST_ORDER_NUMBER = 'first';
    const SECOND_ORDER_NUMBER = 'second +';
    const PRODUCT_CATEGORY_BAG = 'tasche';
    const COOLING_BAG_POSITIVE = 'yes';
    const COOLING_BAG_NEGATIVE = '';

    public function __construct(Collection $order_report_data)
    {
        $this->order_report_data = $order_report_data;
        $this->order_data = $this->order_report_data;
        $this->order_report_box =new OrderReportBox();
        // implement ExtraProducts service to change SKU only to add additional products
        $this->extraProductsService = new ExtraProducts();
    }

    public function getOrderData()
    {
        return $this->order_data;
    }

    public function transform()
    {
        return $this->order_report_data;
    }

    /**
     * K3 - Verpackung K3  270x185x165
     * K4 - Verpackung K4  425x350x165
     * R - Pirates of the Carrotean
     * B - Fresh Prince of Bel Pear
     * A - Applecalypse Now
     * P - Bio Anthony Pumpkins
     * E - Bio Peach Boys
     * Z - David Zucchetta
     * M - Mango No. 5
     * H - Bio Sweet Home Albanana
     * F - Broccoly Balboa
     * L - Beetney Spears
     * S - Bio Cocohontas
     * Q - Taschen
     * X - Loyalty stage
     * C - Booklet
     * D - Advertisement gifts - NEW this represents Latz
     * J - Inbanana Jones - NEW C
     * V - Avocado di Caprio - NEW D
     * Y - Berry Potter - new name Katy Berry
     * G - Pouch 4
     * K - Pouch 5
     * N - Pouch 6
     * W - Product 1
     * O - Product 2
     * T - Product 3
     * U - Product 4
     * I - Product 5
     * Q - Kuhltasche
     * C - Flyer - NEW O
     * X - TBD - NEW MARKETING
     * D - TBD
     * V - Latz (Bib) NEW
     * @param $summary_row
     * @param Order $order
     * @return string
     */
    protected function generateAddressCode($summary_row, Order $order)
    {
        try {
            $code = '';
            $box_size = 'K4';
            $code .= $box_size;

            if($this->hasAdvertisementGift($order))
                $summary_row['V'] = '1';

            $content = '';
            $content_helper = ['R', 'B', 'A', 'P', 'E', 'Z', 'M', 'H', 'F', 'L', 'S', 'T', 'C', 'D', 'Y', 'J', 'G', 'I',
                'W', 'K', 'U', 'N', 'Q', 'O', 'X', 'V', 'ZB', 'MB', 'HB', 'FB', 'LB', 'SB'];
            foreach ($content_helper as $item) {
                if (isset($summary_row[$item]) && $summary_row[$item]) {
	                if (strlen(strtoupper(base_convert($summary_row[$item], 10, 36))) > 1) {
		                $content .= $item . strtoupper(base_convert($summary_row[$item], 10, 36));
	                }
	                else {
	                	$content .= $item . str_pad(strtoupper(base_convert($summary_row[$item], 10, 36)), 2, '0', STR_PAD_LEFT);
	                }
                }
                else
                    $content .= $item . '00';
            }

            $code .= $content;

            $order->SKU = $code;
            $order->save();
            return $code;
        }
        catch (\Exception $exception)
        {
            app('sentry')->captureException($exception);
        }
    }

    /**
     * Generate SKU and save it
     * @param Order $order
     * @return string|null
     */
    public function generateAndSaveSKU(Order $order) {
        $box_content = $this->order_report_box->content($order);

        $summary_row['name'] = Order::ORDER_BOX_PRODUCT_NAME;
        $summary_row['brei amount'] = 0;
        $summary_row['Cooling bag'] = $this->hasCoolingBag($order) ? $this::COOLING_BAG_POSITIVE : $this::COOLING_BAG_NEGATIVE;
        $summary_row['order number'] = $this->selectOrderNumber($order);

        $summary_row = array_replace($summary_row, $box_content);
        if ($summary_row['Cooling bag'] == $this::COOLING_BAG_POSITIVE) {
            $summary_row['Q'] = '1';
        } else {
            $summary_row['Q'] = '0';
        }

        if ($summary_row['order number'] == $this::FIRST_ORDER_NUMBER) {
            $summary_row['O'] = '1';
        } else {
            $summary_row['O'] = '0';
        }
        $summary_row['N'] = '1';

        return $this->generateAddressCode($summary_row, $order);
    }

    /**
     * Generaet COD value
     * @param Order $order
     * @return string
     */
    protected function generateCod(Order $order) {
        try
        {
            $kind = $this::B2C; //TODO: must be changed when we implement b2b or customers in the system.
            $date = $order->delivery_date->format('ymd');
            $id = $order->id;
            $generation_identifier = $this::GENERATION_IDENTIFIER_AUTOMATIC; //This means automatic.
            $COD = self::COUNTRY . $kind . $date . $id . $generation_identifier;
            $order->COD = $COD;
            $order->order_type_id = $kind;
            $order->save();
            return $COD;
        }
        catch (\Exception $exception)
        {
            app('sentry')->captureException($exception);
        }
    }

    /**
     * Generate Meal swap code, used only by Frigosuisse
     * @param $code
     * @return array|string
     */
	protected function generateMealSwapCode($code){
        try
        {
            $codeBody  = substr($code, 2, 78);
            $codeBody2 = substr($code, 80);
            $pairs = str_split($codeBody, 3);
            $pairs2 = str_split($codeBody2, 4);
            $pairs = array_merge($pairs, $pairs2);
            $matches = [];

            foreach ($pairs as $pair) {
                if($this->checkIfEmptyProductCount($pair))
                    continue;
                else
                    array_push($matches, $this->getConvertedBase36ToDecimalString($pair));
            }

            $assambled_code = '';
            foreach ($matches as $pair){
                $assambled_code .= $pair . '-';
            }
            $assambled_code = rtrim($assambled_code, "-");
            $code_lenght = strlen($assambled_code);
            if($code_lenght > 34){
                $dash_index_cut  = 0;
                $strcut = 0;
                foreach ($matches as $pair) {
                    if($strcut > 34){
                        break;
                    }
                    $strcut += strlen($pair) + 1;
                    $dash_index_cut++;
                }
                $container_array = explode('-', $assambled_code, $dash_index_cut);
                $last_part = array_pop($container_array);
                $first_part = array(implode('-', $container_array),  '-' . $last_part);
                return $first_part;
            }
            return $assambled_code;
        }
        catch (\Exception $exception)
        {
            app('sentry')->captureException($exception);
        }

	}

    /**
     * Convert base36 value to decimal
     * @param $value
     * @return string
     */
    private function convertBase36ToDecimal($value)
    {
        return base_convert($value, 36, 10);
	}

    /**
     * Get number from product indentifier string
     * @param $pairString
     * @return string
     */
    private function getConvertedBase36ToDecimalString($pairString)
    {
        $countBase36 = $this->getItemCountString($pairString);
        $identifier = substr($pairString, 0, strlen($pairString)-ExtraProducts::SKU_IDENTIFIER_ITEM_COUNT_STRING_LENGTH);
        return $identifier . $this->convertBase36ToDecimal($countBase36);
	}

    /**
     * Check if product count in string is zero
     * @param $pairString
     * @return bool
     */
    private function checkIfEmptyProductCount($pairString)
    {
        $countBase36 = $this->getItemCountString($pairString);

        if($this->convertBase36ToDecimal($countBase36) == 0)
            return true;
        return false;
	}

    /**
     * Get count string
     * @param $pairString
     * @return bool|string
     */
    private function getItemCountString($pairString)
    {
        return substr($pairString, strlen($pairString) - ExtraProducts::SKU_IDENTIFIER_ITEM_COUNT_STRING_LENGTH, ExtraProducts::SKU_IDENTIFIER_ITEM_COUNT_STRING_LENGTH);
	}

    /**
     * Check if order has cooling bag
     * @param Order $order
     * @return bool
     */
    protected function hasCoolingBag(Order $order)
    {
        try
        {
            foreach ($order->line_items() as $order_item) {
                if ($order_item->product->category == $this::PRODUCT_CATEGORY_BAG) {
                    return true;
                }
            }

            return false;
        }
        catch (\Exception $exception)
        {
            app('sentry')->captureException($exception);
        }
    }

    /**
     * get order number based if box is first in subscription
     * @param Order $order
     * @return string
     */
    protected function selectOrderNumber(Order $order)
    {
        try
        {
            if (empty($order->parent_id)) {
                return $this::FIRST_ORDER_NUMBER;
            }
            return $this::SECOND_ORDER_NUMBER;
        }

        catch (\Exception $exception)
        {
            app('sentry')->captureException($exception);
        }
    }

    /**
     * Check if order has promo gift product
     * @param Order $order
     * @return bool
     */
    protected function hasAdvertisementGift(Order $order)
    {
        try
        {
            foreach ($order->line_items() as $order_item) {
                if ($order_item->product->category == Product::PRODUCT_CATEGORY_PROMO_GIFT) {
                    return true;
                }
            }

            return false;
        }
        catch (\Exception $exception)
        {
            app('sentry')->captureException($exception);
        }
    }

}