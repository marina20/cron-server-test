<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2/1/18
 * Time: 12:29 PM
 */

namespace App\Services\OrderReport;

use App\Models\Order;

class OrderReportBox
{
    /**
     * Get array with product identifiers as indexes and quantity as value
     * @param Order $order
     * @return array
     */
    public function getIdentifiersAndQuantities(Order $order)
    {
        if(empty($order->items))
            return [];

        $mappedProductArray = [];

        foreach ($order->items as $item) {
            if(array_key_exists($item->product->product_identifier_letter, $mappedProductArray)){
                $mappedProductArray[$item->product->product_identifier_letter] = (string) ((int)$mappedProductArray[$item->product->product_identifier_letter] + $item->qty);
            } else {
                $mappedProductArray[$item->product->product_identifier_letter] = (string) $item->qty;
            }
        }

        if(!empty($mappedProductArray))
            return $mappedProductArray;

        return [];
    }

    /**
     * Content of the order box
     * @param Order $order
     * @return array
     */
    public function content(Order $order)
    {
        return $this->getIdentifiersAndQuantities($order);
    }
} 