<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-07-18
 * Time: 16:10
 */

namespace App\Services;

use App\Models\Order;
use App\Models\Product;
use App\Models\ExtraProduct;
use App\Models\ExtraProductLog;
use Illuminate\Support\Facades\Log;

class ExtraProducts
{
    const LOG_TITLE = 'ExtraProducts service ';
    const SKU_ORIGINAL_LENGTH = 80;
    const SKU_LENGTH = 104;
    const SKU_DEFAULT_NUMBER_OF_PRODUCTS = 16;
    const SKU_IDENTIFIER_ITEM_COUNT_STRING_LENGTH = 2;
    const SKU_IDENTIFIER_STRING_LENGTH = 3;
    const SKU_LONG_IDENTIFIER_STRING_LENGTH = 4;
    const SKU_IDENTIFIER_ITEM_COUNT_POSITION_ADJUSTMENT = 1;
    const SKU_TOTAL_OF_EXTRA_PRODUCTS = 2;
    const SKU_IDENTIFIER_ITEM_COUNT_DEFAULT_ADDITIONAL_VALUE = 1;

    const SKU_SINGLE_ADDITIONAL_PRODUCT = 1;
    const SKU_ACTIVE_IDENTIFIER_COUNT_COLUMN_DEFAULT_VALUE = 0;

    const SKU_IDENTIFIER_ALWAYS_ADD_FLYER = 'X';
    const SKU_BOX_IDENTIFIER_LENGTH = 2;
    const SKU_BOX_IDENTIFIER_POSITION = 0;

    const SKU_BASE_DECIMAL = 10;
    const SKU_BASE_36 = 36;

    protected $identifiersArray;
    protected $skuConvertedToDecimalArray;
    protected $skuBoxIdentifierString;


    /**
     * We immediately get all the identifiers for pouches and cups products
     * ExtraProducts constructor.
     */
    public function __construct()
    {
        $this->identifiersArray = $this->getIdentifiersArray();
    }

    /**
     * Validate sku and add extra products to it from randomized extra products array
     * @param $sku
     * @param null $orderId
     * @return string transformed sku with additional products
     */
    public function transform($sku, $orderId=null)
    {
        if(!$this->validateSku($sku))
            return $sku;

        // for each transform run we randomize available extra products
        $randomExtraProducts = $this->randomize();
        if(empty($randomExtraProducts))
            return $sku;
        // Sku is a string and convert it to array with item count converted from base36 to decimal
        $this->skuConvertedToDecimalArray = $this->convertToDecimalArray($sku);
        // if is not needed here!
        if(!empty($randomExtraProducts))
            // add Flyer identifier to random extra products array
            // could we actually add this in randomize() method?
            $randomExtraProducts[] = ['identifier'=>$this::SKU_IDENTIFIER_ALWAYS_ADD_FLYER, 'value'=>$this::SKU_IDENTIFIER_ITEM_COUNT_DEFAULT_ADDITIONAL_VALUE];

        foreach ($randomExtraProducts as $randomProduct)
        {
            // so we are updating array which is property in this class?
            $this->incrementProduct($randomProduct);
            // name of this method is not ok, I don't understand what it mean
            $this->incrementExtraProductCount($randomProduct['identifier'], $this::SKU_SINGLE_ADDITIONAL_PRODUCT);
        }

        // get transformed (sku with incremented extra products)
        $transformedSku = $this->convertDecimalArrayToSku($this->skuConvertedToDecimalArray);
        // name of this method is not ok, maybe we need repository class or another service for these read and write to
        // extra_products and extra_products_log table
        $this->saveLog($sku, $transformedSku, $orderId);
        $this->saveSku($orderId, $transformedSku);

        return $transformedSku;
    }

    /**
     * Increments extra product and updates converted decimal array
     * @param $randomProduct
     * @return mixed
     */
    public function incrementProduct($randomProduct)
    {
        // we want to change this array so we're passing by refence
        foreach ($this->skuConvertedToDecimalArray as &$skuOriginal)
        {
            // maybe there is easier way to find correct array item?
            if($skuOriginal['identifier'] == $randomProduct['identifier'])
            {
                $skuOriginal['value'] += $randomProduct['value'];
                // creating again identifier string looks complicated and is doubled code from Transformer class. We need a service which does only this
                // both pad_length and pad_string are magic numbers and strings here, we should use external service which sets it's own constants
                $skuOriginal['identifier_string'] = $skuOriginal['identifier'] . str_pad($this->convertDecimalToBase36($skuOriginal['value']),2,'0',STR_PAD_LEFT);
            }
        }
        return $this->skuConvertedToDecimalArray;
    }

    /**
     * Randomize active identifier array and number of items up to total count of extra products
     * @return array
     */
    public function randomize()
    {
        $activeIdentifiersArray = $this->getActiveIdentifiersArray();
        $randomizedArray = [];
        if(empty($activeIdentifiersArray))
            return $randomizedArray;
        if(count($activeIdentifiersArray) == $this::SKU_SINGLE_ADDITIONAL_PRODUCT)
        {
            $firstIdentifier = array_pop($activeIdentifiersArray);
            if(empty($firstIdentifier) || $firstIdentifier === $this::SKU_IDENTIFIER_ALWAYS_ADD_FLYER)
                return $randomizedArray;

            $randomizedArray[] = ['identifier'=>$firstIdentifier, 'value'=>$this::SKU_TOTAL_OF_EXTRA_PRODUCTS];
        }
        else {
            for($i=0;$i<$this::SKU_TOTAL_OF_EXTRA_PRODUCTS;$i++)
            {
                shuffle($activeIdentifiersArray);
                $randomizedArray[] = ['identifier'=>array_pop($activeIdentifiersArray), 'value'=>$this::SKU_IDENTIFIER_ITEM_COUNT_DEFAULT_ADDITIONAL_VALUE];
            }
        }

        return $randomizedArray;
    }

    /**
     * Increment used column in extra_products table for given identifier
     * @param $identifier
     * @param $incrementValue
     * @return null
     */
    public function incrementExtraProductCount($identifier, $incrementValue)
    {
        $extraProduct = ExtraProduct::where('product_code',$identifier)->first();
        if(empty($extraProduct))
        {
            return null;
        }
        $extraProduct->used += $incrementValue;
        $extraProduct->save();
    }

    /**
     * Save original sku and transformed sku to extra_products_log table
     * @param $originalSku
     * @param $transformedSku
     * @param null $orderId
     */
    public function saveLog($originalSku, $transformedSku, $orderId=null)
    {
        $extraProductLog = new ExtraProductLog();
        $extraProductLog->order_id = $orderId;
        $extraProductLog->original_sku = $originalSku;
        $extraProductLog->transformed_sku = $transformedSku;
        $extraProductLog->save();
    }

    /**
     * Save sku to orders table
     * @param $orderId
     * @param $sku
     * @return bool
     */
    public function saveSku($orderId, $sku)
    {
        $order = Order::find($orderId);
        if(empty($order))
            return false;
        $order->SKU = $sku;
        $order->save();
        return true;
    }

    /**
     * Return array of product identifier letters from products table
     * @return mixed
     */
    public function getIdentifiersArray()
    {
        return Product::whereHas('categories',function ($query) {
                $query->whereIn('name_key', [Product::PRODUCT_CATEGORY_POUCH,Product::PRODUCT_CATEGORY_BREIL]);
            })
            ->pluck('product_identifier_letter')
            ->toArray();
    }

    /**
     * Return array of product identifier letters from extra_products table which are existing and valid in products table
     * @return mixed
     */
    public function getActiveIdentifiersArray()
    {
        return ExtraProduct::whereHas('product', function ($query){
                $query->whereHas('categories',function ($query) {
                    $query->whereIn('name_key', [Product::PRODUCT_CATEGORY_POUCH,Product::PRODUCT_CATEGORY_BREIL]);
                });
            })
            ->where('count','>',$this::SKU_ACTIVE_IDENTIFIER_COUNT_COLUMN_DEFAULT_VALUE)
            ->pluck('product_code')
            ->toArray();
    }

    /**
     * Validate if sku holds default number of products, if it has all the product indentifiers and if it is correct string
     * @param null $sku
     * @return bool
     */
    public function validateSku($sku=null)
    {
        try {
            if (empty($sku))
                return false;

            if(empty($this->getActiveIdentifiersArray())) {
                // we don't know if they activated or not, we should return false so we don't go further
                // we don't log this becase this can happen a lot of times any time
                return false;
            }

            if (!is_string($sku))
                throw new \Exception('Sku is not string. SKU: '.$sku);

            if (strlen($sku) != self::SKU_LENGTH)
                throw new \Exception('Sku length is suspicious. SKU: '.$sku);

            if (false === $this->validateSkuProductIdentifiers($sku)) {
                throw new \Exception('Sku should have all identifiers. SKU: '.$sku);
            }

            if (false == $this->validateSkuNumberOfProducts($sku))
            {
                Log::info($this::LOG_TITLE . ' validaSku: Sku does not have exactly ' . $this::SKU_DEFAULT_NUMBER_OF_PRODUCTS . ' products. SKU: '.$sku);
                return false;
            }

        }catch (\Throwable $e)
        {
            app('sentry')->captureException($e);
            Log::info($this::LOG_TITLE . ' exception in validaSku: '.$e->getMessage());
            return false;
        }
        return true;
    }

    /**
     * Validate if product identifiers in SKU are in valid product identifier array
     * @param $sku
     * @return bool
     */
    public function validateSkuProductIdentifiers($sku)
    {
        $this->skuConvertedToDecimalArray = $this->convertToDecimalArray($sku);
        foreach ($this->getIdentifiersArray() as $identifier)
        {
            // if one valid product identifier is not in sku validation fails
            if(!in_array($identifier,array_column($this->skuConvertedToDecimalArray, 'identifier')))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Count number of products in SKU and confirm if they match default number of products
     * @param $sku
     * @return bool
     */
    public function validateSkuNumberOfProducts($sku)
    {
        try {
            if($this::SKU_DEFAULT_NUMBER_OF_PRODUCTS !== $this->countNumberOfProductsInSku($sku))
                return false;
        }
        catch (\Throwable $e)
        {
            app('sentry')->captureException($e);
            return false;
        }
        return true;
    }

    /**
     * Count number of products in SKU
     * @param $sku
     * @return int
     */
    public function countNumberOfProductsInSku($sku)
    {
        $totalCount = 0;

        $this->skuConvertedToDecimalArray = $this->convertToDecimalArray($sku);

        foreach ($this->skuConvertedToDecimalArray as $identifier) {
            if(in_array($identifier['identifier'], $this->getIdentifiersArray()))
                $totalCount += $identifier['value'];
        }
        return $totalCount;
    }

    /**
     * Convert SKU to array with item count as decimal value
     * @param $sku
     * @return array
     */
    public function convertToDecimalArray($sku)
    {
        $convertedArray = [];
        // save box identifier to property
        $this->skuBoxIdentifierString = substr($sku, $this::SKU_BOX_IDENTIFIER_POSITION, $this::SKU_BOX_IDENTIFIER_LENGTH);

        // continue conversion without box identifier string
        $skuRemovedBox = substr($sku, $this::SKU_BOX_IDENTIFIER_LENGTH);
        $skuPart1 = substr($skuRemovedBox, 0, $this::SKU_ORIGINAL_LENGTH - $this::SKU_BOX_IDENTIFIER_LENGTH);
        $skuPart2 = substr($sku, $this::SKU_ORIGINAL_LENGTH);

        // split the resto of SKU string based on identifier string length
        $skuSplitted1 = str_split($skuPart1, $this::SKU_IDENTIFIER_STRING_LENGTH);
        $skuSplitted2 = str_split($skuPart2, $this::SKU_LONG_IDENTIFIER_STRING_LENGTH);
        $skuSplitted = array_merge($skuSplitted1, $skuSplitted2);

        foreach ($skuSplitted as $item) {
            $convertedArray[] = [
                'identifier_string' => $item, // save whole identifier string
                'identifier' => substr($item, $this::SKU_ACTIVE_IDENTIFIER_COUNT_COLUMN_DEFAULT_VALUE, strlen($item) - $this::SKU_IDENTIFIER_ITEM_COUNT_STRING_LENGTH),
                'value' => $this->convertBase36ToDecimal(substr($item, strlen($item) - $this::SKU_IDENTIFIER_ITEM_COUNT_STRING_LENGTH))
            ];
        }
        $this->skuConvertedToDecimalArray = $convertedArray;
        return $this->skuConvertedToDecimalArray;
    }

    /**
     * Convert existing array with decimal values back into SKU string
     * @param $decimalArray
     * @return string
     */
    public function convertDecimalArrayToSku($decimalArray)
    {
        // add box identifier to the beginning of the string
        $sku = $this->skuBoxIdentifierString;
        foreach ($decimalArray as $item) {
            $sku .= $item['identifier_string'];
        }
        return $sku;
    }

    /**
     * Convert from base36 number to decimal
     * @param $base36
     * @return int
     */
    public function convertBase36ToDecimal($base36)
    {
        return (int) base_convert($base36,$this::SKU_BASE_36, $this::SKU_BASE_DECIMAL);
    }

    /**
     * Convert from decimal number to base36
     * @param $decimal
     * @return string
     */
    public function convertDecimalToBase36($decimal)
    {
        return strtoupper(base_convert($decimal,$this::SKU_BASE_DECIMAL, $this::SKU_BASE_36));
    }
}