<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 7/2/18
 * Time: 1:00 PM
 */

namespace App\Services;

use App\Models\User;
use App\Models\Order;
use App\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class FindNextDeliveryDate
{
    const DATE_FORMAT = 'd.m.Y';

    /**
     * Find next delivery date for user in exact subscription or first date in general
     * @param User $user
     * @param null $id
     * @return null|string
     */
    public function next(User $user, $id=null)
    {
        try {
            return $this->find($this->subscription($user,$id));
        }
        catch (\Exception $ex)
        {
            app('sentry')->captureException($ex);
            return null;
        }
    }

    /**
     * Find subscription with next delivery date
     * Subscription must be active and cancellation deadline shouldn't have passed
     * @param User $user
     * @param null $id
     * @return mixed
     */
    private function subscription(User $user, $id=null)
    {
        $subscription = Subscription::query()
            ->join('orders','orders.id','=','subscriptions.order_id')
            ->where('subscriptions.user_id',$user->id)
            ->when(!empty($id), function($query) use ($id){
                return $query->where('subscriptions.id',$id);
            })
            ->where('subscriptions.status','!=',Order::STATUS_CANCELLED)
            ->where('orders.delivery_date','>=',DB::raw('now()'))
            ->where('cancellation_deadline','>', date('Y-m-d'))
            ->orderBy('subscriptions.schedule_next_payment')
            ->first();

        return $subscription;
    }

    /**
     * Return delivery date for subscription
     * @param Subscription|null $subscription
     * @return null|string
     */
    private function find(Subscription $subscription=null)
    {
        if(empty($subscription))
        {
            return null;
        }

        return $this->format($subscription->order->delivery_date);
    }

    /**
     * Format the date
     * @param Carbon $date
     * @return string
     */
    private function format(Carbon $date)
    {
        return $date->format($this::DATE_FORMAT);
    }
}