<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 4/12/18
 * Time: 3:01 PM
 */

namespace App\Services\CreateNextOrderFrom;

use App\Models\Order;

class CreateNextOrderFromFactory
{
    protected $create_next_order_from_subscription;

    public function __construct(CreateNextOrderFromSubscription $create_next_order_from_subscription)
    {
        $this->create_next_order_from_subscription = $create_next_order_from_subscription;
    }

    public function create(Order $order)
    {
        // we are creating next order only from subscription
        $create_next_from = null;
        switch ($order->created_via)
        {
            case Order::CREATED_VIA_SUBSCRIPTION:
                $create_next_from = $this->create_next_order_from_subscription;
                break;
        }
        if(is_null($create_next_from))
            return null;
        return $create_next_from->create($order);
    }
}