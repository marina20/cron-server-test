<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/26/18
 * Time: 2:52 PM
 */

namespace App\Services\CreateNextOrderFrom;

use App\Models\Order;

interface CreateNextOrderFromInterface
{
    public function create(Order $order);
}