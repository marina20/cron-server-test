<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/3/19
 * Time: 3:13 PM
 */

namespace App\Services\CreateNextOrderFrom;

use App\Models\Order;

class CreateNextOrderFromSubscription extends CreateNextOrderFrom
{
    protected $duplicate_order;
    protected $create_order_box;
    protected $billing_interval;
    protected $order_cancellation_period = Order::ORDER_SUBSCRIPTION_CANCELLATION_PERIOD;

    const LOG_TITLE = 'Service CreateNextOrderFromSubscription: ';

    const DEFAULT_BILLING_INTERVAL = 2;

    protected function getBillingInterval(Order $order)
    {
        return $order->subscription->billing_interval;
    }
}