<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/3/19
 * Time: 3:12 PM
 */

namespace App\Services\CreateNextOrderFrom;

use App\Models\Box;
use App\Models\DeliveryDate;
use App\Models\Order;
use App\Models\OrderSubscription;
use App\Models\Subscription;
use App\Models\Voucher;
use App\Services\CreateOrder\CreateOrderSubscription;
use App\Services\DuplicateOrder\DuplicateOrder;
use App\Services\OrderService;
use App\Traits\FormatMoney;
use Auth;
use Carbon\Carbon;
use Helpers;
use Illuminate\Support\Facades\Log;

class CreateNextOrderFrom implements CreateNextOrderFromInterface
{
    use FormatMoney;
    protected $duplicate_order;
    protected $create_order_box;
    protected $billing_interval;
    protected $order_cancellation_period = Order::ORDER_SUBSCRIPTION_CANCELLATION_PERIOD;

    const LOG_TITLE = 'Service CreateNextOrderFrom: ';

    public function __construct(DuplicateOrder $duplicate_order,
                                CreateOrderSubscription $create_order_box)
    {
        $this->duplicate_order = $duplicate_order;
        $this->create_order_box = $create_order_box;
    }

    protected function getBillingInterval(Order $order)
    {
        return $order->subscription->billing_interval;
    }

    /**
     * When we create next order from subscription order we need to copy all items and to change and recalculate only
     * some values. We assume that items in subscription will never change e.g. box and shipping only. Things to change:
     * delivery,cancellation and created_at dates, applying discounts, total recalculation, subscription dates change
     * @param Order $order
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function createNEXT(Order $order)
    {
        try {
            // set locale
            Helpers::set_locale($order->getCountryCode($order->shipping_country));
            $subscription = $order->subscription;
            // we duplicate original order and then nullify and change fields
            $next_order = $this->duplicate_order->duplicate($order);
            // set parent_id, created_via and payment fields that should be null at the beginning
            $next_order->parent_id = $order->id;
            $next_order->transaction_id = null;
            $next_order->adyen_auth_result = null;
            $next_order->adyen_payment_method = null;

            $next_order->discount = '0.00';
            $next_order->discount_tax = '0.00';
            $next_order->public_discount = '0.00';
            $next_order->public_discount_tax = '0.00';
            $next_order->loyalty_discount = '0.00';
            $next_order->loyalty_discount_tax = '0.00';
            $next_order->wallet_discount = '0.00';
            $next_order->wallet_discount_tax = '0.00';
            $next_order->system_discount = '0.00';
            $next_order->system_discount_tax = '0.00';
            $next_order->completed_date = null;
            $next_order->paid_date = null;

            $next_order->resent = 0;
            $next_order->from_wizard = null;
            $next_order->mfg_eligible = null;

            $next_order->is_subscription = true;
            $next_order->created_via = Order::CREATED_VIA_SUBSCRIPTION;
            $next_order->status = Order::STATUS_PENDING;
            $next_order->SKU = null;
            $next_order->COD = null;
            $next_order->note = null;

            if(is_null($subscription))
            {
                Log::info($this::LOG_TITLE . ' subscription object does not exist');
                app('sentry')->captureMessage($this::LOG_TITLE . ' subscription object does not exist');

                return null;
            }

            // create new order?
            $next_order->save();

            $box = Box::find($order->custom_box_id);
            if(empty($box))
            {
                return response()->json([
                    'error' => [
                        'message' => 'Custom box does not exist.',
                        'key' => self::ERROR_KEY_TECHNICAL_PROBLEMS
                    ]
                ], 404);
            }

            $products = [];
            if(!empty($box->items)) // validation where there will be an error if empty
            {
                foreach ($box->items as $item)
                {
                    //2echo "hui".$item->product->id."|".$item->quantity;
                    $products[] = ['id'=>$item->product->id,'quantity'=>$item->quantity];
                }
            }

            $next_order = $this->create_order_box->createOrderItems($next_order,$products);
            // this will also apply subscription-discount
            $next_order = $this->create_order_box->getOrderTotal($next_order);

            $next_order = Voucher::applyRecurringDiscounts($next_order);
            $billing_interval_for_calculation = $this->getBillingInterval($order);

            $currentDeliveryDate = $order->delivery_date;

            if(!empty($order->original_delivery_date)) {
                $currentDeliveryDate = $order->original_delivery_date;
            }

            $calculatedDeliveryDate = Carbon::parse($currentDeliveryDate)->addDays(($billing_interval_for_calculation * 7))->format('Y-m-d H:i:s');
            $nextDeliveryDateResult = DeliveryDate::getNextDeliveryDate($calculatedDeliveryDate,$order->country_id);
            $next_order->delivery_date = $nextDeliveryDateResult[0];
            $next_order->original_delivery_date = $nextDeliveryDateResult[1];
            // new cancellation deadline is set to static number of days before delivery date

            $next_order->cancellation_deadline = $next_order->delivery_date
                ->subDays(Order::ORDER_SUBSCRIPTION_CANCELLATION_PERIOD)->endOfDay()->format('Y-m-d H:i:s');
            $next_order->subscription_id = $subscription->id;
            OrderService::generateSKU($next_order);

            return $next_order;
        }
        catch (\Exception $e)
        {
            Log::info($this::LOG_TITLE . ' exception in create method: '.$e->getMessage());
            app('sentry')->captureException($e);
        }
    }


    /**
     * When we create next order from subscription order we need to copy all items and to change and recalculate only
     * some values. We assume that items in subscription will never change e.g. box and shipping only. Things to change:
     * delivery,cancellation and created_at dates, applying discounts, total recalculation, subscription dates change
     * @param Order $order
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(Order $order)
    {
        try {
            Helpers::set_locale($order->getCountryCode($order->shipping_country));
            $subscription = $order->subscription;

            $next_order = $this->createNEXT($order);

            $subscription->order_id = $next_order->id;
            $subscription->schedule_next_payment = $next_order->delivery_date
                ->subDays(Subscription::SCHEDULE_NEXT_PAYMENT_DAYS)->endOfDay()->format('Y-m-d H:i:s');
            $subscription->save();

            $os = new OrderSubscription();
            $os->subscription_id = $subscription->id;
            $os->order_id = $subscription->order_id;
            $os->save();
            return $next_order;
        }
        catch (\Exception $e)
        {
            Log::info($this::LOG_TITLE . ' exception in create method: '.$e->getMessage());
            app('sentry')->captureException($e);
        }
    }
}