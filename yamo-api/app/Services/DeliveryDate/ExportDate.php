<?php


namespace App\Services\DeliveryDate;

use App\Models\DeliveryDate;
use App\Models\Country;

class ExportDate
{
    const EMPTY_DATE_FORMAT = '0000-00-00 00:00:00';
    /**
     * Get export date by checking country and delivery date
     * @param $countryCode ISO_ALPHA_2
     * @param $deliveryDate Y-m-d format
     * @return string|null
     */
    public function getByCountryAndDeliveryDate($countryCode, $deliveryDate)
    {
        if(empty($countryCode) || empty($deliveryDate) || ($deliveryDate == self::EMPTY_DATE_FORMAT))
            return null;

        $country = Country::where('iso_alpha_2', $countryCode)->first();

        if(empty($country))
            return null;

        $countryId = $country->id;

        $deliveryDate = DeliveryDate::where('country_id', $countryId)
            ->whereDate('delivery_date', $deliveryDate)
            ->where('disabled',0)
            ->first();

        if(empty($deliveryDate))
            return null;

        if(isset($deliveryDate->export_date) && $deliveryDate->export_date != self::EMPTY_DATE_FORMAT)
        {
            return $deliveryDate->export_date;
        }
        return null;
    }
}