<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 7/9/18
 * Time: 11:14 AM
 */

namespace App\Services;

use App\Models\Order;
use Helpers;

class CreateSofortLinkService
{
    const BRAND_CODE = 'directEbanking';

    public function make(Order $order, $rurl=null)
    {
        $params = [
            "paymentAmount"      => $order->order_total*100,
            "sessionValidity"    => date('Y-m-d\TH:i:s\Z', strtotime('+12 hours')), // one hour from now, YYYY-MM-DDThh:mm:ssTZD
            "shopperReference"   => $order->user->id,
            "reference"          => $order->id,
            "merchantReference"  => $order->id,
            "recurringContract"  => "RECURRING",
            "shopperInteraction" => "Ecommerce",
            "shopperEmail"       => $order->shipping_email,

            "countryCode"       => $order->getCountryCode($order->billing_country),

            "currencyCode"       => $order->getCurrencyCode($order->currency),
            "brandCode"          => $this::BRAND_CODE,
        ];

        return $this->returnAdyenUrl($params, $rurl);
    }

    public function once(Order $order, $rurl=null)
    {
        $params = [
            "paymentAmount"      => $order->order_total*100,
            "sessionValidity"    => date('Y-m-d\TH:i:s\Z', strtotime('+12 hours')), // one hour from now, YYYY-MM-DDThh:mm:ssTZD
            "shopperReference"   => $order->user->id,
            "reference"          => $order->id,
            "merchantReference"  => $order->id,
            "shopperInteraction" => "Ecommerce",
            "shopperEmail"       => $order->shipping_email,

            "countryCode"       => $order->getCountryCode($order->billing_country),

            "currencyCode"       => $order->getCurrencyCode($order->currency),
            "brandCode"          => $this::BRAND_CODE,
        ];

        return $this->returnAdyenUrl($params,$rurl);
    }

    protected function returnAdyenUrl($params, $rurl=null)
    {
        $skinCode        = env('ADYEN_SKIN_CODE');
        $merchantAccount = env('ADYEN_MERCHANT_ACCOUNT');
        $hmacKey         = env('ADYEN_HMAC');

        // payment-specific details
        $paramsToAdd = [
            "merchantAccount"   => $merchantAccount,
            "shopperLocale"     => env('ADYEN_DEFAULT_LOCALE'),
            "skinCode"          => $skinCode,
            "resURL"            => empty($rurl) ? Helpers::adyen_rurl() : $rurl
        ];

        $params = array_merge($params, $paramsToAdd);

        // The character escape function
        $escapeval = function($val) {
            return str_replace(':','\\:',str_replace('\\','\\\\',(string)$val));
        };

        // Sort the array by key using SORT_STRING order
        ksort($params, SORT_STRING);

        // Generate the signing data string
        $signData = implode(":",array_map($escapeval,array_merge(array_keys($params), array_values($params))));

        // base64-encode the binary result of the HMAC computation
        $merchantSig = base64_encode(hash_hmac('sha256',$signData,pack("H*" , $hmacKey),true));

        $params["merchantSig"] = $merchantSig;

        $paramsString = http_build_query($params);

        return env('ADYEN_SOFORT_URL') . '?' . $paramsString;
    }
}