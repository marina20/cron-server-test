<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/27/18
 * Time: 3:32 PM
 */

namespace App\Services\MFGInvoice\Resource;


use App\Services\MFGInvoice\Resource\BasicRequest;

class FinancialResponse extends BasicRequest
{
    protected $mfg_request_type = 'financial_request';
    protected $xml_root_elemement_name = 'Response';
    public $response_code;
    public $response_date;
    public $authorization_code;
    public $currency;
    public $balance;
    public $card_number;
    public $expiration_date;
    public $msgnum;

    //TODO:create a migration to save this and other requests in DB.

	public function __construct($ResponseArray) {
		parent::__construct();
		$this->response_code = $ResponseArray['ResponseCode'];
		$this->response_date = $ResponseArray['ResponseDate'];
		if(!empty($ResponseArray['AuthorizationCode'])) {
            $this->authorization_code = $ResponseArray['AuthorizationCode'];
        }
		if(!empty($ResponseArray['Currency'])) {
            $this->currency = $ResponseArray['Currency'];
        }
        if(!empty($ResponseArray['Balance'])) {
            $this->balance = $ResponseArray['Balance'];
        }
        if(!empty($ResponseArray['CardNumber'])) {
            $this->card_number = $ResponseArray['CardNumber'];
        }
        if(!empty($ResponseArray['ExpirationDate'])) {
            $this->expiration_date = $ResponseArray['ExpirationDate'];
        }
        if(!empty($ResponseArray['@attributes'])) {
            $this->msgnum = $ResponseArray['@attributes']['msgnum'];
        }
	}
}