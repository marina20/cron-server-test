<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/27/18
 * Time: 3:32 PM
 */
namespace App\Services\MFGInvoice\Resource;

use App\Models\MfgRequest;
use App\Models\MfgRequestType;
use App\Traits\MFGEncode;
use App\Traits\XMLfromArray;


class ConfirmationRequest extends BasicRequest
{
	use XMLfromArray;
	use MFGEncode;

	const TYPE_ID = '3';
	protected $mfg_request_type = 'confirmation_request';
    protected $xml_root_elemement_name = 'Confirmation';
    protected $msgnum;
    protected $gendate;
    protected $protocol = 'PaymentServer_V2_9';
    protected $response;
    protected $financialRequest;
    protected $financialResponse;
    protected $confirmationRequest;
    protected $reference;
    protected $conversation;

	public function __construct(FinancialResponse $response, FinancialRequestObject $request, $country) {
		parent::__construct($country);
		$this->gendate = date('YmdHms');
		$this->financialRequest = $request;
		$this->financialResponse = $response;
            $this->reference = $this->financialRequest->reference;
		$this->generateID();
	}

	/**
	 * Create a RequestType and MfgRequest to generate a unique ID for the msgnum.
	 */
	private function generateID() {
		$this->confirmationRequest = new MfgRequest();
		$this->confirmationRequest->order_id = $this->reference;
		$this->confirmationRequest->user_id = \App\Models\Order::find($this->reference)->user->id; //TODO:Do something if not found.
		$this->confirmationRequest->raw_request = '';
		$this->confirmationRequest->mfg_request_type_id = ConfirmationRequest::TYPE_ID;
		$this->confirmationRequest->raw_response = '';
		$this->confirmationRequest->save();
		$this->msgnum = $this->confirmationRequest->id;
	}

	/**
	 * Updates the generated objects with the correct Response and Request raw strings to be saved in the DB.
	 *
	 * @params String $Response, String $Request
	 */
	public function update(String $Response, String $Request) {
		$this->confirmationRequest->raw_request = $Request;
		$this->confirmationRequest->raw_response = $Response;
		$this->confirmationRequest->split();
		$this->confirmationRequest->save();
	}

	/**
	 * Transforms the Array to and XML String.
	 * @return String Encoded XML String
	 */
	public function getXML() {
		if (isset($this->msgnum)) {
			return $this->encodetoXML($this->fillRequest(),'Confirmation',['msgnum' => $this->msgnum,'gendate' => $this->gendate, 'protocol' => $this->protocol]);
		}
		else {
			return null;
		}
	}

	/**
	 * Extracts the information necesary to create a Confirmation Request from the FinancialRequest and the Financial Response.
	 * @return Array $array
	 */
	public function fillRequest() {
		$array = ['Conversation' =>[
			'FinancialRequest protocol=' . $this->protocol . ' msgnum=' . $this->financialRequest->msgnum => [
				'RequestDate' => $this->financialRequest->request_date,
				'TransactionType' => $this->financialRequest->transaction_type,
				'Currency' => $this->financialRequest->currency,
				'Amount' => $this->financialRequest->amount,
				'ExternalReference' => $this->financialRequest->reference,
				'MerchantId' => $this->financialRequest->merchant_id,
				'FilialId' => $this->financialRequest->filial_id,
				'TerminalId' => $this->financialRequest->terminal_id
			],
			'Response msgnum='. $this->financialResponse->msgnum => [
				'ResponseCode' => $this->financialResponse->response_code,
				'ResponseDate' => $this->financialResponse->response_date,
				'AuthorizationCode' => $this->financialResponse->authorization_code,
				'Currency' => $this->financialResponse->currency,
				'Balance' => $this->financialResponse->balance,
				'CardNumber' => $this->financialResponse->card_number,
				'ExpirationDate' => $this->financialResponse->expiration_date
			]
		]];
		return $array;
	}


}