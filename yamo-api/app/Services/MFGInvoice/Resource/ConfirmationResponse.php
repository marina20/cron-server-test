<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/27/18
 * Time: 3:32 PM
 */

namespace App\Services\MFGInvoice;


class ConfirmationResponse
{
    protected $mfg_request_type = 'confirmation_request';
    protected $xml_root_elemement_name = 'ConfirmationResponse';
    protected $response_date;
    protected $card_statistics;

	public function __construct(Order $order)
	{
		//add stuff
	}

}