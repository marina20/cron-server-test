<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/27/18
 * Time: 3:31 PM
 */

namespace App\Services\MFGInvoice\Resource;

use App\Models\MfgRequest;
use App\Models\MfgRequestType;
use App\Models\Order;
use App\Models\Postcode;
use App\Traits\MFGEncode;
use App\Traits\XMLfromArray;
use Illuminate\Support\Facades\Log;


class CardNumberRequest extends BasicRequest
{
	const TYPE_ID = '1';
	use XMLfromArray;
	use MFGEncode;

    protected $mfg_request_type = 'card_number_request';
    protected $xml_root_elemement_name = 'cardNumberRequest';
    protected $response;
    protected $order;
    public $external_reference;
    protected $order_ip_address;
    protected $gender;
    protected $first_name;
    protected $last_name;
    protected $street;
    protected $city;
    protected $zip;
    protected $country;
    protected $language = 'de'; // TODO: change this when we launch more languages.
    protected $phoneNumber;
    protected $mobileNumber;
	protected $email;
    protected $birthday;
    public $amount;
    public $currency_code;

	public function __construct(Order $order) {
		parent::__construct($order->country->iso_alpha_2);
		$this->order = $order;
		$this->external_reference = $order->id;
		$this->order_ip_address = $order->customer_ip_address;
		$this->gender = $order->shipping_title;
		$this->first_name = $order->billing_first_name;
		$this->last_name = $order->billing_last_name;
		$this->street = $order->getBillingStreet();
        $this->city = $order->billing_city;
        $this->zip = $order->billing_postcode;
		$this->country = $order->country;
		$this->email = $order->billing_email;
		$this->birthday = $order->customer_birthday;
		$this->amount = $order->order_total * 100;
		$this->currency_code =  $order->getCurrencyCode($order->currency);
		$this->phoneNumber = $order->billing_phone; //TODO: We should eventually use a real mobile number or ask if they can take it off.
		$this->mobileNumber = $order->billing_phone;
	}

	/**
	 * This function Stores the Response and the Request of the Virtual Card Number in a Raw format in the database.
	 * @param String $Response, String $Request
	 * @return void
	 */
	public function store(String $Response, String $Request, String $cardNumber) {
	    try {
            $CardNumberRequest = new MfgRequest();
            $CardNumberRequest->order_id = $this->external_reference;
            $CardNumberRequest->user_id = $this->order->user->id;
            $CardNumberRequest->raw_request = $Request;
            $CardNumberRequest->mfg_request_type_id = CardNumberRequest::TYPE_ID;
            $CardNumberRequest->raw_response = $Response;
            $CardNumberRequest->card_number = $cardNumber;
            $CardNumberRequest->split();
            $CardNumberRequest->save();
        }
        catch (\Exception $e)
        {
            Log::info('CardNumberRequest - store: ' . $e->getMessage());
            app('sentry')->captureException($e);
        }
	}

	/**
	 * Extracts the information necesary to create a cardRequest Array from the Object.
	 * @return Array $array
	 */
	public function getCardRequest() {
	    try {
            $array = ['externalReference' => $this->external_reference];
            if (!is_null($this->order_ip_address)) {
                $array = array_add($array, 'orderIpAddress', $this->order_ip_address);
            }
            $array = array_merge($array, [
                //'orderIpAddress' => $this->order_ip_address, //Not Needed Now
                'gender' => $this->gender == 'Frau' ? 'female' : 'male', //TODO: Eventually we will have to set to female everywhere instead of Frau or Herr.
                'firstName' => $this->first_name,
                'lastName' => $this->last_name,
                'street' => $this->street,
                'city' => $this->city,
                'zip' => $this->zip,
                'country' => $this->country->iso_alpha_2,
                'language' => $this->language,
                'email' => $this->email
            ]);
            if (!is_null($this->birthday)) {
                $array = array_add($array, 'birthdate', $this->birthday);
            }
            $array = array_merge($array, [
                //'birthdate' => $this->birthday,
                //'phoneNumber' => $this->phoneNumber, //Phone numbers are in the MFG docs but are not needed aparently.
                //'mobileNumber' => $this->mobileNumber,
                'merchantId' => $this->merchant_id,
                'filialId' => $this->filial_id,
                'terminalId' => $this->terminal_id,
                'amount' => $this->amount,
                'currencyCode' => $this->currency_code
            ]);
            return $array;
        }
        catch (\Exception $e)
        {
            Log::info('CardNumberRequest - getCardRequest: ' . $e->getMessage());
            app('sentry')->captureException($e);
            return [];
        }
	}

	/**
	 * Transforms the Array to and XML String.
	 * @return String Encoded XML String
	 */
	public function getXML() {
		return $this->encodetoXML($this->getCardRequest(),'cardNumberRequest',['protocol' => 'getVirtualCardNumber', 'version' => '2.9']);
	}
}