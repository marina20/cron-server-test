<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/27/18
 * Time: 3:31 PM
 */

namespace App\Services\Model\MFGInvoice;


class CardNumberResponse
{
    protected $mfg_request_type = 'card_number_request';
    protected $xml_root_elemement_name = 'cardNumberResponse';
    protected $mf_reference;
    protected $available_credit;
    protected $maximal_credit;
    protected $credit_refusal_reason;
    protected $card_number;
    protected $response_code;
}