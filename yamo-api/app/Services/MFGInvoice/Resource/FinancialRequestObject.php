<?php


namespace App\Services\MFGInvoice\Resource;


/**
 * Exclusively used in Confirmation request since we can't use ConfirmationRequest object for that purpose
 * Class FinancialRequestObject
 * @package App\Services\MFGInvoice\Resource
 */
class FinancialRequestObject extends BasicRequest
{
    public $reference;
    public $msgnum;
    public $request_date;
    public $transaction_type;
    public $currency;
    public $amount;
    public $merchant_id;

    public function __construct(array $financialRequestArray)
    {
        parent::__construct();
        $this->reference = $financialRequestArray['ExternalReference'];
        $this->msgnum = $financialRequestArray['@attributes']['msgnum'];
        $this->request_date = $financialRequestArray['RequestDate'];
        $this->transaction_type = $financialRequestArray['TransactionType'];
        $this->currency = $financialRequestArray['Currency'];
        $this->amount = $financialRequestArray['Amount'];
        $this->merchant_id = $financialRequestArray['MerchantId'];
    }
}