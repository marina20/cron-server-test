<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/27/18
 * Time: 3:32 PM
 */

namespace App\Services\MFGInvoice\Resource;

use App\Models\MfgRequest;
use App\Traits\MFGEncode;
use App\Traits\XMLfromArray;

class FinancialRequest extends BasicRequest
{

	const TYPE_ID = '2';
	use XMLfromArray;
	use MFGEncode;
    protected $mfg_request_type = 'financial_request';
    protected $xml_root_elemement_name = 'FinancialRequest';
    protected $response;
    protected $card_number;
    public $request_date;
    public $transaction_type = 'debit';
    protected $transaction_type_id = FinancialRequest::TYPE_ID;
    public $currency;
    public $amount;
    protected $payment_model; //Not needed aparently.
    public $reference;
    public $msgnum;
    protected $financialRequest;

    public function __construct($cardNumber, $currency, $amount, $reference, $country) {
        parent::__construct($country);
        $this->card_number= $cardNumber;
        $this->request_date = date('YmdHms');
        $this->currency = $currency;
        $this->amount = $amount;
        $this->reference = $reference;
        $this->generateID();
    }

	/**
	 * Transforms the Array to and XML String.
	 * @return String Encoded XML String
	 */
	public function getXML() {
		if (isset($this->msgnum)) { //Validate if msgnum is present.
			return $this->encodetoXML($this->getCardRequest(),'FinancialRequest',['protocol' => $this->protocol, 'msgnum' => $this->msgnum]);
		}
		else {
			return null;
		}
	}

	/**
	 * Create a RequestType and MfgRequest to generate a unique ID for the msgnum.
	 */
	protected function generateID() {
		$this->financialRequest = new MfgRequest();
		$this->financialRequest->order_id = $this->reference;
		$this->financialRequest->user_id = \App\Models\Order::find($this->reference)->user->id; //TODO:Do something if not found.
		$this->financialRequest->raw_request = '';
		$this->financialRequest->mfg_request_type_id = $this->transaction_type_id;
		$this->financialRequest->raw_response = '';
		$this->financialRequest->save();
		$this->msgnum = $this->financialRequest->id;
	}

	/**
	 * Updates the generated objects with the correct Response and Request raw strings to be saved in the DB.
	 *
	 * @params String $Response, String $Request
	 */
	public function update(String $Response, String $Request) {
		$this->financialRequest->raw_request = $Request;
		$this->financialRequest->raw_response = $Response;
		$this->financialRequest->split();
		$this->financialRequest->save();
	}

	/**
	 * Extracts the information necesary to create a Virtual Credit Card Array from the Card Response.
	 * @return Array $array
	 */
	public function getCardRequest() {
		$array = [
			'CardNumber' => $this->card_number,
			'RequestDate' => $this->request_date,
			'TransactionType' => $this->transaction_type,
			'Currency' => $this->currency,
			'Amount' => $this->amount,
			'ExternalReference' => $this->reference,
			'MerchantId' => $this->merchant_id,
			'FilialId' => $this->filial_id,
			'TerminalId' => $this->terminal_id
		];
		return $array;
	}

}