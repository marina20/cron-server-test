<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/31/18
 * Time: 9:39 AM
 */

namespace App\Services\MFGInvoice\Resource;

class BasicRequest
{
    protected $merchant_id;
    protected $filial_id;
    protected $terminal_id;
    protected $protocol;

    public function __construct($country='ch') {
        $this->protocol = 'PaymentServer_V2_9'; //TODO:add to env.
	    $this->merchant_id = config('mfgroup.country.'.strtoupper($country).'.merchant');
	    $this->filial_id = config('mfgroup.country.'.strtoupper($country).'.filial');
	    $this->terminal_id = config('mfgroup.country.'.strtoupper($country).'.terminal');
    }
}