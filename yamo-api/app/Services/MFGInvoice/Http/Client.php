<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/30/18
 * Time: 1:14 PM
 */

namespace App\Services\MFGInvoice\Http;

use Illuminate\Support\Facades\Log;

class Client
{
    protected $server;
    protected $user;
    protected $psw;
    const LOG_TITLE = 'MFG client request: ';

    /**
     * Pass country so params from correct config can be set
     * Client constructor.
     * @param string $country - default value is Switzerland
     */
    public function __construct($country='ch')
    {
        $this->server = config('mfgroup.country.'.strtoupper($country).'.server');
        $this->user = config('mfgroup.country.'.strtoupper($country).'.user');
        $this->psw = config('mfgroup.country.'.strtoupper($country).'.password');
    }

    /**
     * Send request to MFG server and return response
     *
     * @param $prepared_xml - it should be already prepared with 'xml=' as prefix and urlencoded string from xml structure
     * @return mixed|void
     */
    public function request($prepared_xml)
    {
        try
        {

            $ch= curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->server);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_USERPWD, $this->user . ":" . $this->psw);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLAUTH_BASIC, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $prepared_xml);

            $data= curl_exec($ch);

            if (curl_errno($ch))    {
                Log::info($this::LOG_TITLE . ' curl error - '. curl_error($ch));
                app('sentry')->captureMessage( $this::LOG_TITLE . ' curl error - '. curl_error($ch) );
                return;
            }

            curl_close($ch);
            return $data;
        }
        catch (\Exception $ex)
        {
            Log::info($this::LOG_TITLE . ' error '. $ex->getMessage());
            app('sentry')->captureMessage( $this::LOG_TITLE . ' error '. $ex->getMessage() );
            return;
        }
    }
}