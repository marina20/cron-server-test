<?php
/**
 * Created by PhpStorm.
 * User: juanse254
 * Date: 9/10/18
 * Time: 5:12 PM
 */

namespace App\Services\MFGInvoice\Recurring;


use App\Jobs\SendPaymentFailureCancelEmailJob;
use App\Models\Order;
use App\Services\Adyen\RecurringPaymentCheckOrder;
use App\Services\Adyen\RecurringPaymentSaveOrder;
use App\Services\MFGInvoice\MFGInvoice;



class RecurringPaymentMFG
{

	protected $check_order;
	protected $order;
	protected $mfg_invoice;

	public function __construct(RecurringPaymentCheckOrder $check_order, MFGInvoice $mfg_invoice) {
		$this->check_order = $check_order;
		$this->mfg_invoice = $mfg_invoice;
	}
	public function pay(Order $order) {
		$this->order = $order;
		if (!$this->check_order->can_be_processed($order)) {
			return [
				'success'=>'Failed',
				'message'=>'FAILURE! Order ' . $order->id . ' is not eligible for processing!'
			];
		}

		$result = $this->mfg_invoice->process($this->order);

		if ($result) {
			return [
				'resultCode' => 200, //TODO return the response code.
				'message' => 'OK'
			];
		}
		else {
			dispatch(new SendPaymentFailureCancelEmailJob($order));
			return [
				'resultCode' => 400, //TODO return the response code.
				'message' => 'ERROR PROCESSING PAYMENT AT MFG SERVICE'
			];
		}

	}

}