<?php
/**
 * Created by PhpStorm.
 * User: juanse254
 * Date: 9/17/18
 * Time: 4:53 PM
 */

namespace App\Services\MFGInvoice\Refund;

use App\Models\MfgRequest;
use App\Services\MFGInvoice\Resource\CardNumberRequest;
use App\Services\MFGInvoice\Resource\FinancialRequest;

class FinancialRequestCredit extends FinancialRequest
{
	const TYPE_ID = '4';
	public $transaction_type = 'credit';
	protected $transaction_type_id = FinancialRequestCredit::TYPE_ID;


	public function __construct($currency, $amount, $reference, $country) {
		$this->cardNumber = $this->fetchCardNumber($reference);
		parent::__construct($this->cardNumber, $currency, $amount, $reference, $country);
	}

	private function fetchCardNumber($id) {
		try {
			$mfg_request =  MfgRequest::query()->where('order_id', $id)->where('mfg_request_type_id', CardNumberRequest::TYPE_ID)->firstOrFail();
			return $mfg_request->card_number;
		}
		catch (ModelNotFoundException $ex) {
			app(‘sentry’)->captureException($ex);
		}
	}


}