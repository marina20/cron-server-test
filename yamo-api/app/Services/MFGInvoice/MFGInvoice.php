<?php

namespace App\Services\MFGInvoice;

use App\Models\MfgRequest;
use App\Models\Order;
use App\Models\ReferralUrl;
use App\Models\Voucher;
use App\Services\CancelSubscriptionService;
use App\Services\MFGInvoice\Http\Client as MFGClient;
use App\Services\MFGInvoice\Refund\FinancialRequestCredit;
use App\Services\MFGInvoice\Resource\CardNumberRequest;
use App\Services\MFGInvoice\Resource\ConfirmationRequest;
use App\Services\MFGInvoice\Resource\FinancialRequest;
use App\Services\MFGInvoice\Resource\FinancialRequestObject;
use App\Services\MFGInvoice\Resource\FinancialResponse;
use App\Traits\MFGEncode;
use App\Traits\XMLfromArray;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class MFGInvoice
{
	use MFGEncode;
	use XMLfromArray;
    protected $client;
    protected $CardNumberRequest;
    protected $FinancialRequest;
    protected $FinancialResponse;
    protected $ConfirmationRequest;
    protected $country;
    protected $cancel_subscription_service;

    const REFUSAL_REASON_NONE = 'None';

    public function __construct(CancelSubscriptionService $cancel_subscription_service)
    {
        $this->cancel_subscription_service = $cancel_subscription_service;
    }

    /**
     * Check if customer is eligible to pay via MFG invoice
     * @param Order $order
     * @return bool
     */
    public function checkCardRequest(Order $order):bool{
        try
        {
            $this->country = $order->country->iso_alpha_2;
            $this->set_client($this->country);
            $cardResult = $this->cardRequestTest($order);
            if ($cardResult !== false && $cardResult["creditRefusalReason"] === "None") {
                return true;
            } else {
                return false;
            }
        } catch (\Throwable $e)
        {
            Log::info('MFGInvoice checkCardRequest: ' . $e->getMessage());
            app('sentry')->captureException($e);
            return false;
        }
    }

    /**
     *
     * @param Order $order
     * @return array|bool
     */
    private function cardRequestTest(Order $order) {
        try{
            $this->CardNumberRequest = new CardNumberRequest($order);
            $cardRequest = $this->CardNumberRequest->getXML();

            $cardRequestResult = $this->client->request($this->xml($cardRequest));

            if (is_null($cardRequestResult)) {
                throw new \Exception('MFG server connection failed');
            }

            $cardResult = $this->decodeSimpleXMLObject(simplexml_load_string($cardRequestResult)); //TODO:add in case null error handling. Should we create an cardResponse object from this? YES Fill cardResponse
            if (isset($cardResult) && !in_array('XML CANNOT BE VALIDATED', $cardResult, true) && !in_array('INTERNAL ERROR', $cardResult, true)) { //Shall we always save this info or just if the customer can actually pay with this method. we should validate that the transaction was good here.
                if(!isset($cardResult['cardNumber'])) { //TODO:Remove and fix properly, patched like this to keep functionality. !!!!
                    $cardResult['cardNumber'] = '0';
                }
            }
            else {
                return false;
            }
            return $cardResult;
        } catch (\Throwable $e)
        {
            Log::info('MFGInvoice cardRequestTestMEthod: ' . $e->getMessage());
            app('sentry')->captureException($e);
            return false;
        }
    }

	/**
	 * Sets the client for the Requests according to country.
	 * @param String $country
	 * @return void
	 */
    protected function set_client($country) {
        try {
            switch ($country) {
                case 'CH':
                    $this->client = new MFGClient('ch');
                    break;
                case 'DE':
                    $this->client = new MFGClient('de');
                    break;
                case 'AT':
                    $this->client = new MFGClient('at');
                    break;
                default:
                    $this->client = new MFGClient('ch');
                    break;
            }
        }
        catch (\Throwable $e)
        {
            Log::info('MFGInvoice set_client: ' . $e->getMessage());
            app('sentry')->captureException($e);
            return false;
        }

    }

    /**
     * Processing flow of the MFG from cardRequest to Confirmation.
     * @TODO for checkout flow should be cardRequest and financialRequest, confirmation should be extracted to separate call
     * @param Order $order
     * @return bool
     */
    public function process(Order $order) {
        try
        {
            $financialApproved = false;

            if($order->order_total <= 0) {
                $this->freeOrderEnable($order);
                return true;
            }
            $this->country = $order->country->iso_alpha_2;
            $this->set_client($this->country);

            $cardResult = $this->cardRequestProcess($order);

            if (isset($cardResult['creditRefusalReason']) && self::REFUSAL_REASON_NONE == $cardResult['creditRefusalReason']) {
                $financialApproved = $this->financialRequestProcess($cardResult['cardNumber'], $this->CardNumberRequest->currency_code, $this->CardNumberRequest->amount, $this->CardNumberRequest->external_reference);
            }

            if ($financialApproved) {
                $order->completed_date = Carbon::now()->toDateTimeString();
                $order->paid_date = Carbon::now()->toDateTimeString();
                $order->status = Order::STATUS_PAYED;
                $order->adyen_auth_result = Order::STATUS_AUTHORISED;
                $order = Voucher::redeemVoucher($order);
                $order->save();
                return true;
            }

            $this->cancelOrderSubscriptionMFG($order);
            return false;
        }
        catch (\Throwable $e)
        {
            Log::info('MFGInvoice process: ' . $e->getMessage());
            app('sentry')->captureException($e);
            return false;
        }
    }

	/**
	 * Request a Virtual CreditCard Number with the Order Information
	 * @param Order $order
	 * @return bool
	 */
    private function cardRequestProcess(Order $order)
    {
        try {
            $this->CardNumberRequest = new CardNumberRequest($order);
            $cardRequest = $this->CardNumberRequest->getXML();
            $cardRequestResult = $this->client->request($this->xml($cardRequest));
            if (is_null($cardRequestResult)) {
                throw new \Exception('MFG server connection failed');
            }
            $cardResult = $this->decodeSimpleXMLObject(simplexml_load_string($cardRequestResult)); //TODO:add in case null error handling. Should we create an cardResponse object from this? YES Fill cardResponse
            if (isset($cardResult) && !in_array('XML CANNOT BE VALIDATED', $cardResult, true) && !in_array('INTERNAL ERROR', $cardResult, true)) { //Shall we always save this info or just if the customer can actually pay with this method. we should validate that the transaction was good here.
                if (!isset($cardResult['cardNumber'])) { //TODO:Remove and fix properly, patched like this to keep functionality. !!!!
                    $cardResult['cardNumber'] = '0';
                }
                $this->CardNumberRequest->store($cardRequestResult, $cardRequest, $cardResult['cardNumber']);
            } else {
                return false;
            }
            return $cardResult;
        }catch (\Throwable $e)
        {
            Log::info('MFGInvoice cardRequestProcess: ' . $e->getMessage());
            app('sentry')->captureException($e);
            return false;
        }
    }

	/**
	 * Request a Financial Approval using the virtual cardNumber previously obtained and the amount.
	 * @param String $cardNumber, String $currencyCode, Integer $amount, Integer $reference
	 * @return bool
	 */
    private function financialRequestProcess($cardNumber, $currencyCode, $amount, $reference) {
        try
        {
            $this->FinancialRequest = new FinancialRequest($cardNumber,$currencyCode,$amount, $reference, $this->country);
            $financialRequest = $this->FinancialRequest->getXML();

            $financialRequestResult = $this->client->request($this->xml($financialRequest));
            if (is_null($financialRequestResult)) {
                throw new \Exception('MFG server connection failed');
            }

            $financialResult = $this->decodeSimpleXMLObject(simplexml_load_string($financialRequestResult));
            $this->FinancialResponse = new FinancialResponse($financialResult);
            $this->FinancialRequest->update($financialRequestResult, $financialRequest);
            if (isset($financialResult)) {
                switch ($financialResult['ResponseCode']) //TODO:would be nice to handle this error codes. Save them somewhere with.
                {
                    case '00': //Approved
                        return true;
                        break;
                    case '01': //Unknown card
                        return false;
                        break;
                    case '03': //Unknown Merchant
                        return false;
                        break;
                    case '04': //Unknown Filial
                        return false;
                        break;
                    case '05': //Unknown terminal
                        return false;
                        break;
                    case '06': //Funds too low
                        return false;
                        break;
                    case '07': //Funds too high
                        return false;
                        break;
                    case '08': //Invalid authorization code
                        return false;
                        break;
                    case '11': //Blocked Card
                        return false;
                        break;
                    case '12': //Expired Card
                        return false;
                        break;
                    case '13': //Validation Error
                        return false;
                        break;
                    case '14': //Internal Error
                        return false;
                        break;
                    case '16': //Forbidden operation
                        return false;
                        break;
                    case '17': //Token Verification Failed
                        return false;
                        break;
                    default:
                        return false;
                }
            }
            return false;
        } catch (\Throwable $e) {
            Log::info('MFGInvoice financialRequestProcess: ' . $e->getMessage());
            app('sentry')->captureException($e);
            return false;
        }
    }


    /**
     * Process MFG order confirmation, this request triggers sending the invoice to customer
     * @param Order $order
     * @return bool
     */
    public function processConfirmation(Order $order)
    {
        try
        {
            // check if Confirmation Request was already created
            $confirmationRequest = MfgRequest::where('order_id',$order->id)
                ->where('mfg_request_type_id',ConfirmationRequest::TYPE_ID)
                ->first();

            // confirmation request was already triggered
            if(!empty($confirmationRequest))
                return false;

            $financialRequestMfgRequest = MfgRequest::where('order_id',$order->id)
                ->where('mfg_request_type_id',FinancialRequest::TYPE_ID)
                ->orderBy('created_at','desc')
                ->first();

            if(empty($financialRequestMfgRequest))
                return false;

            $this->set_client($order->country->iso_alpha_2);

            // maybe we can get normal FinancialResponse but not normal FinancialRequest without creating new entry
            $financialRequest = new FinancialRequestObject((array)simplexml_load_string($financialRequestMfgRequest->raw_request));
            $financialResponse = new FinancialResponse((array)simplexml_load_string($financialRequestMfgRequest->raw_response));
            return $this->confirmationRequestProcessing($financialResponse, $financialRequest);
        } catch (\Throwable $e) {
            Log::info('MFGInvoice processConfirmation: ' . $e->getMessage());
            app('sentry')->captureException($e);
            return false;
        }
    }

	/**
	 * Confirm the checkout of the previously approved financial request. This will charge customers.
	 * @param FinancialRequest $request, FinancialResponse $response
	 * @return bool
	 */
    private function confirmationRequestProcessing(FinancialResponse $response, FinancialRequestObject $request) {
        try
        {
            $this->ConfirmationRequest = new ConfirmationRequest($response, $request, $this->country);

            $confirmationRequest = $this->ConfirmationRequest->getXML();


            $confirmationRequestResult = $this->client->request($this->xml($confirmationRequest));
            if (is_null($confirmationRequestResult)) {
                throw new \Exception('MFG server connection failed');
            }

            $confirmationResult = $this->decodeSimpleXMLObject(simplexml_load_string($confirmationRequestResult));

            $this->ConfirmationRequest->update($confirmationRequestResult, $confirmationRequest);
            if (isset($confirmationResult)) {
                $confirmed = array_key_exists( 'CardStatistics' ,$confirmationResult);
            }
            if ($confirmed) {
                return true;
            }
            return false;
        } catch (\Throwable $e) {
            Log::info('MFGInvoice confirmationRequestProcessing: ' . $e->getMessage());
            app('sentry')->captureException($e);
            return false;
        }
    }

	/**
	 * This function cancels the order and the subscription in case there was a refusal from MFG.
	 * TODO:this should be done in the checkout controller, temporarily here.
	 *
	 * @param Order $order
	 * @return void
	 */
    private function cancelOrderSubscriptionMFG(Order $order) {
        try {
            $order->status = Order::STATUS_CANCELLED;
            $order = ReferralUrl::revertWallet($order);
            $order->save();
            if (isset($order->subscription)) {
                $this->cancel_subscription_service->cancel($order->subscription);
            } else {
                Log::info('MFG CANCEL SUBSCRIPTION: ' . 'order id: ' . $order->id . ' without subscription');
            }
        } catch (\Throwable $e) {
            Log::info('MFGInvoice cancelOrderSubscriptionMFG: ' . $e->getMessage());
            app('sentry')->captureException($e);
            return false;
        }
    }

	/**
	 * When an order has a value of 0 because is free or they used a cupon and there is 100% disscount we dont need MFG.
	 *
	 * @param Order $order
	 * @return void
	 */
	private function freeOrderEnable(Order $order) {
	    try {

            $order->status = Order::STATUS_PAYED;
            $order->save();
        } catch (\Throwable $e) {
            Log::info('MFGInvoice freeOrderEnable: ' . $e->getMessage());
            app('sentry')->captureException($e);
            return false;
        }
	}

	public function refund($amount, $reference) {
		try {
			$order = Order::query()->findOrFail($reference);
		}
		catch (ModelNotFoundException $ex) {
			app(‘sentry’)->captureException($ex);
		}

		try {
            $this->country = $order->country->iso_alpha_2;
            $this->set_client($this->country);
            $currency = $order->getCurrencyCode($order->currency);
            $FinancialRequestCredit = new FinancialRequestCredit($currency, $amount, $reference, $this->country);
            $financialRequest = $FinancialRequestCredit->getXML();

            $financialRequestResult = $this->client->request($this->xml($financialRequest));
            if (is_null($financialRequestResult)) {
                throw new \Exception('MFG server connection failed');
            }

            $financialResult = $this->decodeSimpleXMLObject(simplexml_load_string($financialRequestResult));
            $FinancialRequestCredit->update($financialRequestResult, $financialRequest);
            if (isset($financialResult)) {
                switch ($financialResult['ResponseCode']) //TODO:would be nice to handle this error codes. Save them somewhere with.
                {
                    case '00': //Approved
                        $error = 'Approved';
                        break;
                    case '01': //Unknown card
                        $error = 'Unknown card';
                        break;
                    case '03': //Unknown Merchant
                        $error = 'Unknown Merchant';
                        break;
                    case '04': //Unknown Filial
                        $error = 'Unknown Filial';
                        break;
                    case '05': //Unknown terminal
                        $error = 'Unknown terminal';
                        break;
                    case '06': //Funds too low
                        $error = 'Funds too low';
                        break;
                    case '07': //Funds too high
                        $error = 'Funds too high';
                        break;
                    case '08': //Invalid authorization code
                        $error = 'Invalid authorization code';
                        break;
                    case '11': //Blocked Card
                        $error = 'Blocked Card';
                        break;
                    case '12': //Expired Card
                        $error = 'Expired Card';
                        break;
                    case '13': //Validation Error
                        $error = 'Validation Error';
                        break;
                    case '14': //Internal Error
                        $error = 'Internal Error';
                        break;
                    case '16': //Forbidden operation
                        $error = 'Forbidden operation';
                        break;
                    case '17': //Token Verification Failed
                        $error = 'Token Verification Failed';
                        break;
                    default:
                        $error = 'Unknown Error code';
                }
            }
            return [
                'error' => [
                    'message' => $error
                ]
            ];
        } catch (\Throwable $e) {
            Log::info('MFGInvoice refund: ' . $e->getMessage());
            app('sentry')->captureException($e);
            return false;
        }
	}
}