<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/23/18
 * Time: 11:04 AM
 */

namespace App\Services;

use App\Models\Order;

class UpdateProfileService
{
    public function save_shipping_and_billing_data(Order $order)
    {
        if(empty($order->user->profile))
        {
            return false;
        }

        $profile = $order->user->profile;
        $profile->shipping_title = $order->shipping_title;
        $profile->shipping_first_name = $order->shipping_first_name;
        $profile->shipping_last_name = $order->shipping_last_name;
        $profile->shipping_company = $order->shipping_company;
        // $profile->shipping_address_1 = $order->shipping_address_1;
        // $profile->shipping_address_2 = $order->shipping_address_2;


        $profile->shipping_street_nr = $order->shipping_street_nr;
        $profile->shipping_street_name = $order->shipping_street_name;
        $profile->billing_street_nr = $order->billing_street_nr;
        $profile->billing_street_name = $order->billing_street_name;
        $profile->shipping_city = $order->shipping_city;
        $profile->shipping_postcode = $order->shipping_postcode;
        $profile->shipping_city = $order->shipping_city;
        $profile->shipping_state = $order->shipping_state;
        $profile->shipping_country = $order->shipping_country;
        $profile->shipping_email = $order->shipping_email;
        $profile->shipping_phone = $order->shipping_phone;
        $profile->billing_title = $order->billing_title;
        $profile->billing_first_name = $order->billing_first_name;
        $profile->billing_last_name = $order->billing_last_name;
        $profile->billing_company = $order->billing_company;
        // $profile->billing_address_1 = $order->billing_address_1;
        // $profile->billing_address_2 = $order->billing_address_2;
        $profile->billing_city = $order->billing_city;
        $profile->billing_state = $order->billing_state;
        $profile->billing_postcode = $order->billing_postcode;
        $profile->billing_country = $order->billing_country;
        $profile->billing_email = $order->billing_email;
        $profile->billing_phone = $order->billing_phone;
	    $profile->customer_birthday = $order->customer_birthday;
        if(empty($order->shipping_street_nr)){
            $profile['shipping_street_nr'] = 0;
        }
        if(empty($order->billing_street_nr)){
            $profile['billing_street_nr'] = 0;
        }
        $profile->save();

        return $order->user->profile;
    }
}