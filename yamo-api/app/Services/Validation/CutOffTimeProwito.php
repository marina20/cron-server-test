<?php


namespace App\Services\Validation;

use App\Services\DeliveryDate\ExportDate;
use App\Console\Commands\OrderReportProwitoCommand;
use Carbon\Carbon;
use Helpers;

class CutOffTimeProwito implements CutOffTimeInterface
{
    const ALLOWED_COUNTRIES = [
        Helpers::LOCALE_DOMAIN_DE,
        Helpers::LOCALE_DOMAIN_AT
    ];
    const DATE_STRING_LENGTH = 10;
    const ISO_ALPHA_2_STRING_LENGTH = 2;

    protected $exportDateService;
    public function __construct(ExportDate $exportDate)
    {
        $this->exportDateService = $exportDate;
    }
    /**
     * Validate if current date time is greater than cut off date and time for given delivery date and country
     * @param $deliveryDate in Y-m-d format (or substring it to it)
     * @param $deliveryCountry locale is in ISO_ALPHA_2 format
     * @return bool true - allow to proceed, false - don't
     */
    public function validate($deliveryDate, $deliveryCountry)
    {
        // do not let this proceed if country ISO_ALPHA2 is in wrong format
        if(strlen($deliveryCountry) !== self::ISO_ALPHA_2_STRING_LENGTH)
            return false;

        // if country isn't DE or AT we validate this condition
        if(!in_array(strtolower($deliveryCountry),self::ALLOWED_COUNTRIES))
            return true;

        // if delivery date is empty this is invalid request
        if(empty($deliveryDate) || $deliveryDate == '0000-00-00 00:00:00')
            return false;

        // if delivery date string is less than it should be it's not valid
        if(strlen($deliveryDate) < self::DATE_STRING_LENGTH)
            return false;

        if(strlen($deliveryDate) > self::DATE_STRING_LENGTH)
            $deliveryDate = substr($deliveryDate, 0, self::DATE_STRING_LENGTH);

        $exportDate = $this->getExportDateTime($deliveryDate, $deliveryCountry);

        if(empty($exportDate))
            return false;

        if(!empty($exportDate))
        {
            $checkNow = Carbon::now();
            if($checkNow->gte($exportDate))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Get export date as Carbon object with set cut off time
     * @param $deliveryDate in YYYY-MM-DD format
     * @param $deliveryCountry in ISO_ALPHA_2 format
     * @return Carbon|null
     */
    private function getExportDateTime($deliveryDate, $deliveryCountry)
    {
        $exportDate = $this->exportDateService->getByCountryAndDeliveryDate(strtolower($deliveryCountry), $deliveryDate);

        if(!empty($exportDate))
        {
            try
            {
                // export date is from db and it's always in full date time format
                // or we can expect wrong format returned and we should catch and exception
                return Carbon::createFromFormat('Y-m-d H:i:s', $exportDate)->setTimeFromTimeString(Helpers::convertTimeFromCETtoUTC(OrderReportProwitoCommand::PROWITO_CUT_OFF_TIME));
            }
            catch (\Exception $exception)
            {
                app('sentry')->captureException($exception);
                return null;
            }
        }
        return null;
    }
}