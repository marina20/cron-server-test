<?php


namespace App\Services\Validation;

interface CutOffTimeInterface
{
    /**
     * Validate cut off time based on delivery date and delivery country
     * @param $deliveryDate
     * @param $deliveryCountry
     * @return mixed
     */
    public function validate($deliveryDate, $deliveryCountry);
}