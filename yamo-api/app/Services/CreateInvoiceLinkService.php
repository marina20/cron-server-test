<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 11/27/17
 * Time: 2:41 PM
 */

namespace App\Services;

use App\Models\Order;
use Helpers;

class CreateInvoiceLinkService
{

    /**
     * @param Order $order
     * @param null $rurl redirect url
     * @return string
     */
    public function make(Order $order, $rurl = null)
    {
        $data = [
            'merchantReference'=>$order->id,
            'skinCode'=>'',
            'shopperLocale'=>'',
            'authResult'=>'AUTHORISED',
            'pspReference'=>'',
            'merchantSig'=>'',
            'paymentMethod'=>'invoice',
        ];

        $link = empty($rurl) ? Helpers::adyen_rurl() : $rurl;
        return $link
        . '?' . http_build_query($data);
    }
}