<?php

namespace App\Services\Sofort;

use Illuminate\Support\Facades\Facade;

class SofortFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'sofort';
    }
}
