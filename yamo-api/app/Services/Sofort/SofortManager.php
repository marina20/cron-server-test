<?php

namespace App\Services\Sofort;

use App\Services\Sofort\Manager\AbstractManager;
use Illuminate\Contracts\Config\Repository;
use Sofort\SofortLib\Sofortueberweisung;

class SofortManager extends AbstractManager
{
    /**
     * The factory instance.
     *
     * @var \App\Services\Sofort\SofortFactory
     */
    private $factory;

    /**
     * Create a new Sofort manager instance.
     *
     * @param \Illuminate\Contracts\Config\Repository $config
     * @param \App\Services\Sofort\SofortFactory      $factory
     */
    public function __construct(Repository $config, SofortFactory $factory)
    {
        parent::__construct($config);

        $this->factory = $factory;
    }

    /**
     * Create the connection instance.
     *
     * @param array $config
     *
     * @return mixed
     */
    protected function createConnection(array $config)
    {
        return $this->factory->make($config);
    }

    /**
     * Get the configuration name.
     *
     * @return string
     */
    protected function getConfigName()
    {
        return 'yamo-sofort';
    }

    /**
     * Get the factory instance.
     *
     * @return \App\Services\Sofort\SofortFactory
     */
    public function getFactory()
    {
        return $this->factory;
    }
}
