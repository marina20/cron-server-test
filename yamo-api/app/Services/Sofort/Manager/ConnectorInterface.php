<?php

namespace App\Services\Sofort\Manager;

/**
 * This is the connector interface.
 */
interface ConnectorInterface
{
    /**
     * Establish a connection.
     *
     * @param array $config
     *
     * @return object
     */
    public function connect(array $config);
}
