<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 12/12/17
 * Time: 4:34 PM
 */

namespace App\Services;

use App\Models\Order;
use App\Services\CreateNextOrderFrom\CreateNextOrderFromFactory;

class CreateNextOrderService
{
    protected $create_next_order_from_factory;
    public function __construct(CreateNextOrderFromFactory $create_next_order_from_factory)
    {
        $this->create_next_order_from_factory = $create_next_order_from_factory;
    }

    public function next_order(Order $order)
    {
        return $this->create_next_order_from_factory->create($order);
    }
}