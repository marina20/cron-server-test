<?php

namespace App\Services;
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 11/27/17
 * Time: 12:55 PM
 */

use App\Services\CreateOrder\CreateOrderSingle;
use App\Services\CreateOrder\CreateOrderSubscription;
use Illuminate\Http\Request;

class CreateOrderService
{
    protected $create_order_single;
    protected $create_order_subscription;
	const LOG_TITLE = 'Service create order: ';

    public function __construct(CreateOrderSingle $create_order_single,
                                CreateOrderSubscription $create_order_wizard)
    {
        $this->create_order_single = $create_order_single;
        $this->create_order_subscription = $create_order_wizard;
	}

    public function single(Request $request)
    {
        return $this->create_order_single->create($request);
    }

    public function createProfile(Request $request, $wizard)
    {
        return $this->create_order_subscription->createProfile($request,$wizard);
    }

    public function subscription(Request $request)
    {
        return $this->create_order_subscription->subscription($request);
    }

}