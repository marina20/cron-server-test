<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/23/18
 * Time: 11:04 AM
 */

namespace App\Services;

use App\Models\Subscription;

class CancelSubscriptionService
{
    public function cancel(Subscription $subscription)
    {
        $subscription->status = Subscription::STATUS_CANCELLED;
        $subscription->save();
    }
}