<?php
/**
 * Created by PhpStorm.
 * User: milenabasaragin
 * Date: 10/4/18
 * Time: 3:34 PM
 */

namespace App\Services;

use App\Models\Order;
use App\Models\OrderSubscription;
use App\Models\Subscription;
use App\Models\DeliveryFrequency;
use Carbon\Carbon;

class SubscriptionService
{
    /**
     * @param Subscription $subscription
     * @param Carbon $nextPayment
     */
    static public function changeNextPayment(Subscription $subscription, Carbon $nextPayment)
    {
        $subscription->schedule_next_payment = $nextPayment;
        $subscription->save();
    }

    /**
     * @param Order $order
     * @param null $deliveryFrequencyInterval
     * @return Subscription|mixed|null
     */
    public function create(Order $order, $deliveryFrequency=null)
    {
        if(empty($deliveryFrequency))
        {
            $deliveryFrequency = DeliveryFrequency::where('interval',Subscription::DEFAULT_DELIVERY_FREQUENCY)->first();
        }

        $subscription = $order->subscription ?? new Subscription();

        $subscription->delivery_frequency_id = $deliveryFrequency->id;

        // set deliveries and statuses
        $subscription->order_id = $order->id;
        $subscription->delivery_frequency_period = Subscription::SUBSCRIPTION_DELIVERY_FREQUENCY_PERIOD;
        $subscription->delivery_frequency = $deliveryFrequency->interval;
        $subscription->billing_period = Subscription::SUBSCRIPTION_DELIVERY_FREQUENCY_PERIOD;
        $subscription->billing_interval = $deliveryFrequency->interval;
        $subscription->user_id = $order->user->id;
        $subscription->status = Order::STATUS_PENDING;

        // calculate when next payment should be
        if(!empty($order->delivery_date)){
            $subscription->schedule_next_payment =
                $order->delivery_date->subDay(Subscription::SCHEDULE_NEXT_PAYMENT_DAYS)->endOfDay()->toDateTimeString();
        }

        $subscription->save();

        return $subscription;
    }

    /**
     * Create order subscription object - should be moved to it's own service
     * @param Subscription $subscription
     */
    public function createOrderSubscription(Subscription $subscription)
    {
        $orderSubscription = new OrderSubscription();
        $orderSubscription->subscription_id = $subscription->id;
        $orderSubscription->order_id = $subscription->order_id;
        $orderSubscription->save();
    }
}
