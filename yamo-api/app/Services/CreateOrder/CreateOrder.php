<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 12/4/18
 * Time: 2:06 PM
 */

namespace App\Services\CreateOrder;

use App\Models\Country;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\ReferralUrl;
use App\Models\User;
use App\Models\Profile;
use App\Models\Postcode;
use App\Models\Voucher;
use Illuminate\Http\Request;
use App\Models\UTMTracker;
use App\Services\CreateAdyenLinkService;
use App\Services\CreateInvoiceLinkService;
use App\Services\CreatePaypalLinkService;
use App\Services\CreateSofortLinkService;
use App\Services\MFGInvoice\MFGInvoice;
use App\Services\SubscriptionService;
use App\Services\UpdateProfileService;
use App\Traits\FormatMoney;
use App\Traits\ResetsPasswords;
use Laravel\Lumen\Routing\ProvidesConvenienceMethods;
use Carbon\Carbon;
use Tymon\JWTAuth\JWTAuth;
use Helpers;
use Log;
use Auth;

class CreateOrder
{
    use FormatMoney, ResetsPasswords, ProvidesConvenienceMethods;

    protected $response_status;
    protected $response_message;
    protected $response_redirect_link;
    protected $response_errors;
    protected $create_adyen_link_service;
    protected $create_invoice_link_service;
    protected $mfg_invoice;
    protected $create_paypal_link_service;
    protected $create_sofort_link_service;
    protected $update_profile_service;
    protected $subscription_service;
    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;

    const LOG_TITLE = 'Service create order: ';
    const RETURN_URI = 'menueplan/vielen-dank';
    const RETURN_URI_SHOP = 'shop/vielen-dank';
    const ERROR_KEY_TECHNICAL_PROBLEMS = 'TECHNICAL_PROBLEMS';
    const ERROR_KEY_EXISTING_USER_CHANGED_EMAIL = 'USER_ALREADY_EXISTS';

    public function __construct(CreateAdyenLinkService $create_adyen_link_service,
                                CreateInvoiceLinkService $create_invoice_link_service,
                                MFGInvoice $mfg_invoice,
                                CreatePaypalLinkService $create_paypal_link_service,
                                CreateSofortLinkService $create_sofort_link_service,
                                UpdateProfileService $update_profile_service,
                                SubscriptionService $subscription_service,
                                JWTAuth $jwt)
    {
        $this->response_status = true;
        $this->response_message = 'Order successfully created.';
        $this->response_redirect_link = Helpers::adyen_rurl();
        $this->response_errors = null;
        $this->create_adyen_link_service = $create_adyen_link_service;
        $this->create_invoice_link_service = $create_invoice_link_service;
        $this->mfg_invoice = $mfg_invoice;
        $this->create_paypal_link_service = $create_paypal_link_service;
        $this->create_sofort_link_service = $create_sofort_link_service;
        $this->update_profile_service = $update_profile_service;
        $this->subscription_service = $subscription_service;
        $this->jwt = $jwt;
    }

    protected function translatePaymentMethod($payment_method_input)
    {
        switch($payment_method_input) {
            case Order::PAYMENT_METHOD_CREDITCARD:
                return Order::PAYMENT_METHOD_CREDITCARD_DE;
                break;
            case Order::PAYMENT_METHOD_PAYPAL:
                return Order::PAYMENT_METHOD_PAYPAL_DE;
                break;
            case Order::PAYMENT_METHOD_INVOICE:
                return ORDER::PAYMENT_METHOD_INVOICE_DE;
                break;
        }
        return $payment_method_input;
    }

    /*
     *
     * This function validates that the customer is allowed to pay by invoice using the birthday and profile params.
     * @param Order $order
     * @return string
     */
    protected function validateBirthday( Order $order)
    {
        $valid = false;
        $customer_birthday = $order->customer_birthday; //YYYY-MM-DD is the format here.
        $ip_address = $order->customer_ip_address;
        if(!is_null($customer_birthday) && !is_null($ip_address)){
            $valid = $this->mfg_invoice->process($order);
        }
        if($valid) {
            return Order::STATUS_PAYED;
        }
        else {
            return 'MFG_DEFAULT_ERROR';
        }

    }

    /**
     * Calculate totals for the order
     *
     * @param Order $order
     * @return Order
     */
    public function getOrderTotal(Order $order)
    {
        try
        {
            $total = 0;
            $tax = 0;
            foreach ($order->items as $item) {
                $total += $item->total;
                $tax += $item->total_tax;
            }

            $subtotal = 0;
            $subtotal_tax = 0;
            $taxRate = 0;
            foreach ($order->line_items() as $line_item)
            {
                $taxRate = $line_item->tax_rate;
                //echo "total ".$line_item->total_tax;
                $subtotal += $line_item->total;
                $subtotal_tax += $line_item->total_tax;
            }


            $order->order_total = $this->formatMoney($total);
            $order->order_tax = CreateOrder::calculateVAT($order->order_total,$taxRate);
            $order->order_total_without_tax = $this->formatMoney($total-$tax);

            // Order subtotal is price of all line items - it is without shipping
            $order->order_subtotal = $this->formatMoney($subtotal);
            //$order->order_subtotal_tax = $this->formatMoney($subtotal_tax);
            $order->order_subtotal_tax = CreateOrder::calculateVAT($order->order_subtotal,$taxRate);
            $order->discount = $this->formatMoney(0); // number_format($discount,2);
            $order->discount_tax = $this->formatMoney(0);
            $user = User::find($order->user_id);
            $country = Country::find($order->country_id);
            $countryIsoAlpha2 = $country->iso_alpha_2;
            // condition for first subscription discount
            if ($user->isNew() && !empty($order->is_subscription)) {

                $result = Voucher::applyVoucher('FIRST_SUBSCRIPTION_'.$countryIsoAlpha2,$order);
                $order = $result['order'];
            } else {
                // condition for subscription discount
                if (!empty($order->is_subscription)) {
                    //echo "apply subscription-discount";
                    $result = Voucher::applyVoucher('SUBSCRIPTION_'.$countryIsoAlpha2,$order);
                    $order = $result['order'];
                }
            }

            $order->save();

            return $order;
        }
        catch (\Throwable $ex)
        {
            Log::info($this::LOG_TITLE . ' exception in getOrderTotal: '.$ex->getMessage());
            app('sentry')->captureException($ex);
            // $order->coupon_code = null;
            //throw $ex;
            return $order;
        }
    }

    /**
     * Save UTM object
     * @param $utm
     * @param $order
     * @return mixed
     */
    protected function saveUTM($utm, $order)
    {
        try {
            $utmTracker = new UTMTracker();
            $utmTracker->type = 'order';
            $utmTracker->reference_id = $order->id;
            foreach ($utm as $key => $val) {
                if (!empty($val)) {
                    $utmTracker[$key] = $val;
                }
            }
            $utmTracker->save();
        }
        catch (\Throwable $ex)
        {
            Log::info($this::LOG_TITLE . ' exception in saveUTM: '.$ex->getMessage());
            app('sentry')->captureException($ex);
        }
    }


    /**
     * @param Order $order
     * @return Carbon|null
     */
    protected function getChildBirthdayCarbonObjectFromProfileOrNull(Order $order, $user_id=null)
    {
        try {
            if(empty($order->user) && !is_null($user_id))
            {
                $profile = Profile::where('user_id', $user_id)->first();
            }
            else
            {
                $profile = $order->user->profile;
            }
            // if we try to set null as child_birthday carbon parse throws error ...
            // and this is important because we used child_birthday from order in Subscription, we need to find why we did it
            if (!empty($profile->child_birthday))
            {
                return Carbon::parse($profile->child_birthday);
            }
        }
        catch (\Exception $exception)
        {
            Log::info($this::LOG_TITLE . 'child birthday error: '. $exception->getMessage());
        }
        return null;
    }


    public static function calculateVAT($price, $vatPercent){
        return round((($price / (100 + $vatPercent)) * $vatPercent),2);
    }

    /**
     * @param Order $order
     * @param $products
     * @return Order
     */
    public function createOrderItems(Order $order, $products)
    {
        try
        {
            // loop items
            foreach ($products as $item) {
                $product = Product::find($item['id']);
                // price helper
                $price = $this->formatMoney($product->price);
                // let's create order item
                $order_item = new OrderItem();
                $order_item->product_id = $product->id;
                $order_item->order_id = $order->id;
                $order_item->item_name = $product->name;
                $order_item->item_type = OrderItem::ITEM_TYPE_LINE;
                $order_item->qty = $item['quantity'];
                $order_item->item_price = $price;
                $order_item->tax_rate = $product->tax_percentage;

                // discounts will be added in different method
                $order_item->total_discount = 0;
                $order_item->total_discount_tax = 0;

                // discounts are subtracted from total
                $order_item->total = $this->formatMoney(($price - $order_item->total_discount) * $order_item->qty);
                $order_item->total_tax = $this->formatMoney(CreateOrder::calculateVAT($order_item->total, $order_item->tax_rate));

                // we are going to save subtotal before we start calculating discount
                $order_item->subtotal = $this->formatMoney($order_item->qty * $price);
                $order_item->subtotal_tax = $this->formatMoney(CreateOrder::calculateVAT($order_item->subtotal, $order_item->tax_rate));

                $order->items->add($order_item);
                $order_item->save();
            }

            return $order;
        }
        catch (\Throwable $ex)
        {
            Log::info($this::LOG_TITLE . ' exception in createOrderItems: '.$ex->getMessage());
            app('sentry')->captureException($ex);
            $order->coupon_code = null;
            return $order;
        }
    }

    /**
     * First step in checkout where we create user's profile
     * @param Request $request
     * @param $wizard
     * @return \Illuminate\Http\JsonResponse
     */
    public function createProfile(Request $request,$wizard)
    {
        try {
            $customerGender = $request->input('gender');
            $customerFirstName = $request->input('first_name');
            $customerLastName = $request->input('last_name');
            $customerBirthday = $request->input('customer_birthday');

            $babyName = $request->input('baby_name');
            $babyBirthday = $request->input('baby_birthday');

            $user = Auth::user();
            $token = null;

            if(empty($user))
            {
                $email = $request->input('shipping_email');

                $existingUser = User::where('email', $email)->first();

                if (!empty($existingUser)) {
                    return response()->json([
                        'error' => [
                            'message' => 'User already exists.',
                            'key' => self::ERROR_KEY_EXISTING_USER_CHANGED_EMAIL
                        ]
                    ], 404);
                }

                $confirmation_code = str_random(30);
                $nameArr = explode('@', $email);
                $name = reset($nameArr);
                $password = app('hash')->make($request->input('uuid'));
                $user = User::create([
                    'email' => $email,
                    'password' => $password,
                    'name' => $name,
                    'confirmation_code' => $confirmation_code,
                    'registered_at' => date('Y-m-d H:m:i'),
                    'from_wizard' => $wizard
                ]);

                if(!empty($user))
                {
                    $profile = Profile::create([
                        'user_id' => $user->id,
                        'first_name' => $name
                    ]);
                    ReferralUrl::generateReferral($user);
                }

                try {
                    $credentials = ['email' => $email, 'password' => $request->input('uuid')];
                    if (!$token = $this->jwt->attempt($credentials)) {
                        return response()->json(['error' => 'user_not_found'], 404);
                    }
                } catch (TokenExpiredException $e) {
                    return response()->json(['error' => 'token_expired'], $e->getStatusCode());
                } catch (TokenInvalidException $e) {
                    return response()->json(['error' => 'token_invalid'], $e->getStatusCode());
                } catch (JWTException $e) {
                    return response()->json(['token_absent' => $e->getMessage()], $e->getStatusCode());
                }
                $this->sendResetLinkEmail(new Request(['email'=>$user->email]));
            }

            if(!empty($user)) {
                $profile = $user->profile;
                $profile->gender = $customerGender;
                $profile->first_name = $customerFirstName;
                $profile->last_name = $customerLastName;
                $profile->customer_birthday = $customerBirthday;
                $profile->child_name = $babyName;
                $profile->child_birthday = $babyBirthday;
                $profile->shipping_title = $request->input('shipping_title', $customerGender);
                $profile->shipping_first_name = $request->input('shipping_first_name');
                $profile->shipping_last_name = $request->input('shipping_last_name');
                $profile->shipping_company = $request->input('shipping_company');
                $profile->shipping_street_nr = $request->input('shipping_street_nr');
                $profile->shipping_street_name = $request->input('shipping_street_name');

                $profile->billing_street_nr = $request->input('billing_street_nr');
                $profile->billing_street_name = $request->input('billing_street_name');

                $profile->shipping_country = $request->input('shipping_country');
                $profile->shipping_email = $request->input('shipping_email');
                $profile->shipping_phone = $request->input('shipping_phone');
                $profile->shipping_state = $request->input('shipping_state');
                $profile->billing_title = $request->input('billing_title',$customerGender);
                $profile->billing_first_name = $request->input('billing_first_name');
                $profile->billing_company = $request->input('billing_company');
                $profile->billing_last_name = $request->input('billing_last_name');
                $profile->billing_country = $request->input('billing_country');
                $profile->billing_email = $request->input('billing_email');
                $profile->billing_phone = $request->input('billing_phone');
                $profile->billing_state = $request->input('billing_state');

                $postcode = Postcode::where(['id'=>$request->input('billing_postcode_id')])->first();
                if(!empty($postcode) && $postcode->exists()) {
                    $profile->billing_postcode = $postcode->postcode;
                    $profile->billing_city = $postcode->city;
                }

                $postcode = Postcode::where(['id'=>$request->input('shipping_postcode_id')])->first();
                if(!empty($postcode) && $postcode->exists()) {
                    $profile->shipping_postcode = $postcode->postcode;
                    $profile->shipping_city = $postcode->city;
                }
                if(!empty($request->input('shipping_street_nr_checkbox'))){
                    $profile['shipping_street_nr'] = 0;
                }
                if(!empty($request->input('billing_street_nr_checkbox'))){
                    $profile['billing_street_nr'] = 0;
                }
                $profile->save();
            }
            else
            {
                return response()->json([
                    'error' => [
                        'message' => 'User does not exist.',
                        'key' => self::ERROR_KEY_TECHNICAL_PROBLEMS
                    ]
                ], 404);
            }

            return response()->json([
                'user_id' => $user->id,
                'token' => $token
            ]);
        }
        catch (\Throwable $ex)
        {
            Log::info($this::LOG_TITLE . ' exception in create method: '.$ex->getMessage());
            app('sentry')->captureException($ex);
            return response()->json([
                'error' =>
                    [
                        'message'=>$ex->getMessage(),
                        'key' => self::ERROR_KEY_TECHNICAL_PROBLEMS
                    ]
            ],500);
        }
    }
}