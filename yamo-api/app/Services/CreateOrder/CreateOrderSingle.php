<?php

namespace App\Services\CreateOrder;

use App\Jobs\SendSingleBoxConfirmationEmailJob;
use App\Models\Box;
use App\Models\Country;
use App\Models\Order;
use App\Models\ReferralUrl;
use App\Models\Region;
use App\Models\Voucher;
use App\Services\CustomBox\CreateContent;
use App\Services\OrderService;
use App\Traits\FormatMoney;
use Auth;
use Helpers;
use Loyalty;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class CreateOrderSingle extends CreateOrder
{
    use FormatMoney;
    const LOG_TITLE = 'Service CreateOrderSingle: ';
    const ERROR_KEY_TECHNICAL_PROBLEMS = 'TECHNICAL_PROBLEMS';

    /**
     * Create single box order
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        try
        {
            $user = Auth::user();
            if($user->id != $request->input('user_id'))
            {
                return response()->json([
                    'error' => [
                        'message' => 'User id does not match.',
                        'key' => self::ERROR_KEY_TECHNICAL_PROBLEMS
                    ]
                ], 404);
            }
            $customBoxId = $request->input('custom_box_id');

            $order = new Order();

            // fill the order data
            $order->user_id = $user->id;
            $order->is_subscription = false;
            $order->payment_method_id = $request->input('payment_method');
            $order->payment_method_title = parent::translatePaymentMethod($order->payment_method_id);
            $order->shipping_title = $user->profile->shipping_title;
            $order->shipping_first_name = $user->profile->shipping_first_name;
            $order->shipping_last_name = $user->profile->shipping_last_name;
            $order->shipping_company = $user->profile->shipping_company;

            $order->country_id = Helpers::country()->id;

            $order->region_id = Region::where(['country_id' => $order->country_id])->first()->id;
            $order->shipping_street_nr = $user->profile->shipping_street_nr;
            $order->shipping_street_name = $user->profile->shipping_street_name;
            $order->shipping_city = $user->profile->shipping_city;
            $order->shipping_postcode = $user->profile->shipping_postcode;
            $order->shipping_country = $user->profile->shipping_country;
            $order->shipping_email = $user->profile->shipping_email;
            $order->shipping_phone = $user->profile->shipping_phone;
            $order->shipping_state = $user->profile->shipping_state;

            $order->billing_title = $user->profile->billing_title;
            $order->billing_first_name = $user->profile->billing_first_name;
            $order->billing_company = $user->profile->billing_company;
            $order->billing_last_name = $user->profile->billing_last_name;
            $order->billing_street_nr = $user->profile->billing_street_nr;
            $order->billing_street_name = $user->profile->billing_street_name;
            $order->billing_city = $user->profile->billing_city;
            $order->billing_postcode = $user->profile->billing_postcode;
            $order->billing_country = $user->profile->billing_country;
            $order->billing_email = $user->profile->billing_email;
            $order->billing_phone = $user->profile->billing_phone;
            $order->billing_state = $user->profile->billing_state;

            $order->delivery_date = $request->input('delivery_date');

            $box = Box::find($customBoxId);
            if(empty($box))
            {
                return response()->json([
                    'error' => [
                        'message' => 'Custom box does not exist.',
                        'key' => self::ERROR_KEY_TECHNICAL_PROBLEMS
                    ]
                ], 404);
            }

            $box = CreateContent::replicateBoxIfSuggested($box);

            $order->custom_box_id = $box->id;
            $order->customer_birthday = $user->profile->customer_birthday;
            $order->customer_user_agent = $request->input('user_agent');
            $order->customer_ip_address = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $request->ip();
            $order->child_birthday = $this->getChildBirthdayCarbonObjectFromProfileOrNull($order, $request->input('user_id'));
            $order->created_via = Order::CREATED_VIA_SINGLE_BOX;
            $order->currency = Helpers::currency();
            $order->from_wizard = true;



            if(empty($request->input('from_wizard'))){
                $order->from_wizard = false;
            }

            if(!empty($request->input('shipping_street_nr_checkbox'))){
                $order['shipping_street_nr'] = 0;
            }
            if(!empty($request->input('billing_street_nr_checkbox'))){
                $order['billing_street_nr'] = 0;
            }
            $products = [];
            if(!empty($box->items)) // validation where there will be an error if empty
            {
                foreach($box->items as $item)
                {
                    $products[] = ['id'=>$item->product->id,'quantity'=>$item->quantity];
                }
            }

            $this->update_profile_service->save_shipping_and_billing_data($order);
            // needed to apply correct id!!
            $order->save();

            if(!empty($request->input('utm')))
                $this->saveUTM($request->input('utm'),$order);

            // we need order id next so save it
            $order = $this->createOrderItems($order, $products);
            // this resets order_total to 0.00!!!
            $order = $this->getOrderTotal($order);
            $checkOrder = $order->replicate();
            // we don'nt know payment method yet so we replicate order and set this
            $checkOrder->payment_method_id = Order::PAYMENT_METHOD_INVOICE;
            $checkOrder->payment_method_title = Order::PAYMENT_METHOD_INVOICE_DE;
            $checkOrder->country = Country::where(['content_code' => Helpers::get_locale()])->first();
            $invoiceCheck = $this->mfg_invoice->checkCardRequest($checkOrder);
            // Avoid double-save
            $checkOrder->delete();
            $order->mfg_eligible = $invoiceCheck;
            $order->save();

            if(!empty($request->input('coupon_code')) && empty($request->input('ref_code')))
            {
                $voucherData = Voucher::applyVoucher($request->input('coupon_code'), $order, true);
                $coupon_applied = $voucherData['success'];
                if($coupon_applied){
                    $order->coupon_code = $request->input('coupon_code');
                }
                $order = $voucherData['order'];
            } else if(!empty($request->input('ref_code')))
            {
                $result = ReferralUrl::applyReferral($request->toArray(), $order);
                $order = $result['order'];
            }

            // apply loyalty programme
            $order = Loyalty::apply($order);

            $order = ReferralUrl::applyWallet($order);

            // set initial order status = PENDING
            $order->status = Order::STATUS_PENDING;
            $order->save();

            if($order->payment_method_id == Order::PAYMENT_METHOD_FREE){
                $order->status = Order::STATUS_PAYED;
                $order->paid_date = NULL;
            }

            OrderService::generateSKU($order);

            $rurl = Helpers::app_url() . self::RETURN_URI;
            if(!$order->from_wizard){
                $rurl = Helpers::app_url() . self::RETURN_URI_SHOP;
            }
            return $this->getPaymentResponse($request->input('payment_method'), $order, $rurl);
        }
        catch (\Exception $ex)
        {
            Log::info($this::LOG_TITLE . ' exception in create method: '.$ex->getMessage());
            app('sentry')->captureException($ex);
            return response()->json([
                'error' =>
                    [
                        'message' => $ex->getMessage(),
                        'key' => self::ERROR_KEY_TECHNICAL_PROBLEMS
                    ]
            ],500);
        }
    }

    /**
     * Select payment method and return correct response
     * @param $paymentMethod
     * @param Order $order
     * @param $rurl
     * @return \Illuminate\Http\JsonResponse
     */
    private function getPaymentResponse($paymentMethod, Order $order, $rurl)
    {
        try
        {
            switch($paymentMethod)
            {
                case Order::PAYMENT_METHOD_INVOICE:
                    {
                        return $this->processInvoicePayment($order, $rurl);
                    }
                    break;
                case Order::PAYMENT_METHOD_FREE:
                    dispatch(new SendSingleBoxConfirmationEmailJob($order));
                    return response()->json([
                        'message' => "Single-order successfully created.",
                        'adyen_link' => env('FREE_RURL') . '&merchantReference=' . $order->id
                    ]);
                    break;
                case Order::PAYMENT_METHOD_PAYPAL:
                    return response()->json([
                        'message' => "Single-order successfully created.",
                        'adyen_link' => $this->create_paypal_link_service->once($order, $rurl)
                    ]);
                    break;
                case Order::PAYMENT_METHOD_SOFORT:
                    return response()->json([
                        'message' => "Single-order successfully created.",
                        'adyen_link' => $this->create_sofort_link_service->once($order, $rurl)
                    ]);
                    break;
                default:
                    return response()->json([
                        'message' => "Single-order successfully created.",
                        'adyen_link' => $this->create_adyen_link_service->once($order, $rurl)
                    ]);
                    break;
            }
        }
        catch (\Throwable $ex)
        {
            Log::info($this::LOG_TITLE . ' exception in getPaymentResponse: '.$ex->getMessage());
            app('sentry')->captureException($ex);
            return response()->json([
                'error' => [
                    'message' => $ex->getMessage(),
                    'key' => self::ERROR_KEY_TECHNICAL_PROBLEMS
                ]
            ], 404);
        }
    }


    /**
     * Process invoice payment
     * @param $order
     * @param $rurl
     * @return \Illuminate\Http\JsonResponse
     */
    private function processInvoicePayment($order, $rurl)
    {
        try
        {
            $processable = $this->mfg_invoice->process($order);

            if($processable == Order::STATUS_PAYED){


                dispatch(new SendSingleBoxConfirmationEmailJob($order));

                return response()->json([
                    'message' => 'Success. Order updated.',
                    'adyen_link' => $this->create_invoice_link_service->make($order, $rurl)], 200);
            }

            return response()->json([
                'error' => [
                    'message' => 'Not valid birthday for invoice method.',
                    'key' => $processable
                ]
            ], 404);
        }
        catch (\Throwable $ex)
        {
            Log::info($this::LOG_TITLE . ' exception in processInvoicePayment: '.$ex->getMessage());
            app('sentry')->captureException($ex);
            return response()->json([
                'error' => [
                    'message' => $ex->getMessage(),
                    'key' => self::ERROR_KEY_TECHNICAL_PROBLEMS
                ]
            ], 404);
        }
    }
}