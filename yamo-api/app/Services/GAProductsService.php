<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/23/18
 * Time: 11:04 AM
 */

namespace App\Services;

use App\Models\Order;
use App\Models\Product;
use App\Traits\FormatMoney;
use Helpers;

class GAProductsService
{
    use FormatMoney;

    const TS_CATEGORY_SUBSCRIPTION = 'subscription';
    const TS_CATEGORY_SINGLE_BOX = 'single-box';

    const GA_CATEGORY_SUBSCRIPTION = 'Abo';
    const GA_CATEGORY_SINGLE_BOX = 'Einzelbestellung';
    const GA_CATEGORY_OTHER = 'Other';

    /**
     * Return array with data for google analytics ecommerce products array
     * @param Order $order
     * @return array
     */
    public function products(Order $order)
    {
        $products = [];
        foreach ($order->line_items() as $line_item) {
            $products[] = [
                'id'=>$line_item->id,
                'name'=>$line_item->item_name,
                'category'=>$this->getGACategory($line_item->product->category),
                'price'=>$this->formatMoney($line_item->total),
                'quantity'=>$line_item->qty
            ];
        }
        return $products;
    }

    /**
     * Return array with main product data for TrustedShop checkout product dataLayer
     * @param Order $order
     * @return array
     */
    public function trustedShopProduct(Order $order)
    {
        $product = [
            'ts_checkout_product_url'=>[
                'base_url' => \Helpers::base_url(),
                'category' => $this->getTrustedShopsUrlCategory($order->created_via)
            ],
            'ts_checkout_product_image_url'=>
                Helpers::getImageThumbnail(($order->created_via == Order::CREATED_VIA_SUBSCRIPTION) ? Product::SUBSCRIPTION_BOX_IMAGE_THUMB : Product::SINGLE_BOX_IMAGE_THUMB),
            'ts_checkout_product_name'=>$order->created_via,
            'ts_checkout_product_sku'=>$order->created_via
        ];


        return $product;
    }

    /**
     * Hardcoded FE will know which url to create based on category, we will use routes from frontend
     * @param $category
     * @return string
     */
    private function getTrustedShopsUrlCategory($category)
    {
        switch ($category)
        {
            case false !== strpos($category,Order::CREATED_VIA_SINGLE_BOX):
                return $this::TS_CATEGORY_SINGLE_BOX;
                break;
            default:
                return $this::TS_CATEGORY_SUBSCRIPTION;
                break;
        }
    }

    private function getGACategory($category)
    {
        switch ($category)
        {
            case false !== strpos($category,Order::CREATED_VIA_SINGLE_BOX):
                return $this::GA_CATEGORY_SINGLE_BOX;
                break;
            case false !== strpos($category,Order::CREATED_VIA_SUBSCRIPTION):
                return $this::GA_CATEGORY_SUBSCRIPTION;
                break;
            default:
                return $this::GA_CATEGORY_OTHER;
                break;
        }
    }
}