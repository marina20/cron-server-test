<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/3/18
 * Time: 9:47 AM
 */

namespace App\Services\MyAccount;

use App\Models\Product;
use App\Models\Subscription;
use App\Models\Box;
use App\Services\CustomBox\CreateContent;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SubscriptionDataTransformer
{
    /**
     * Trasform collection of subscriptions into data suitable for my-account subscriptions page
     *
     * @param $subscriptions
     * @return array
     */
    public function transform($subscriptions)
    {
        $ret = [];
        foreach ($subscriptions as $sub)
        {
            $ret[] = $this->single($sub);
        }
        return $ret;
    }

    /**
     * Transform single subscription
     *
     * @param Subscription $subscription
     * @return array
     */
    public function single(Subscription $subscription)
    {

        $street_name = $subscription->order->shipping_street_name;
        $street_nr = $subscription->order->shipping_street_nr;
        if(empty($street_name)){
           $result = CreateContent::migrateAddress($subscription->order['shipping_address_1']);
           $street_name = $result[0];
           $street_nr = $result[1];
        }


        $zip_city = $subscription->order->shipping_postcode.", ".$subscription->order->shipping_city;
        if(empty($subscription->order->shipping_postcode) && empty($subscription->order->shipping_city)){
            $zip_city = '';
        }
        $transformedSubscription = [
            'id' => $subscription->id,
            'product' => [
                'image' => 'https://yamo.s3.eu-central-1.amazonaws.com/public/products/yamobox-abonew.png',
                'name' => 'yamo box',
                'id' => null,
                'items' => []
            ],
            'delivery_frequency' => (string) $subscription->billing_interval,
            'delivery_date' => $subscription->delivery_date,
            'order' => [
                'shipping_first_name' => $subscription->order->shipping_first_name,
                'shipping_last_name' => $subscription->order->shipping_last_name,
                'shipping_street_name' => $street_name,
                'shipping_street_nr' => $street_nr,
                'shipping_zip_city' => ['id' => 0, 'zip_city' => $zip_city],
                'shipping_country' => $subscription->order->shipping_country,
                'order_total' => $subscription->order->order_total,
                'order_currency' => $subscription->order->currency,
                'created_via' => $subscription->order->created_via
            ]
        ];

        $items = $subscription->order->items;

        if(!empty($subscription->order->custom_box_id))
        {
            try
            {
                $box = Box::findOrFail($subscription->order->custom_box_id);
                $items = $box->items;

                foreach ($items as $item) {
                    $transformedSubscription['product']['items'][] = [
                        'name' => $item->product->name,
                        'weight' => $item->product->weight,
                        'quantity' => $item->quantity,
                        'ingredients' => $item->product->ingredients
                    ];
                }
                return $transformedSubscription;
            }
            catch (ModelNotFoundException $ex)
            {
                $items = $subscription->order->items;
            }
        }

        foreach ($items as $item) {
            $transformedSubscription['product']['items'][] = [
                'name' => $item->product->name,
                'weight' => $item->product->weight,
                'quantity' => $item->qty,
                'ingredients' => $item->product->ingredients
            ];
        }
        return $transformedSubscription;
    }
}