<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/16/18
 * Time: 3:53 PM
 */

namespace App\Services\MyAccount;

use App\Models\Box;
use App\Models\Subscription;
use App\Models\OrderItem;
use App\Services\CustomBox\Create;
use App\Services\CustomBox\CreateContent;
use App\Services\CustomBox\DefaultProduct;
use App\Services\CustomBox\Delete;
use App\Services\OrderService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Carbon\Carbon;
use Log;

class SubscriptionCustomBoxSave
{
    const LOG_TITLE = 'Service SubscriptionCustomBoxSave: ';

    protected $create_custom_box_service;
    protected $create_custom_box_content_service;
    protected $delete_custom_box_service;
    protected $default_custom_box_product_service;

    public function __construct(Create $create_custom_box_service,
                                CreateContent $create_custom_box_content_service,
                                Delete $delete_custom_box_service,
                                DefaultProduct $default_custom_box_product_service)
    {
        $this->create_custom_box_service = $create_custom_box_service;
        $this->create_custom_box_content_service = $create_custom_box_content_service;
        $this->delete_custom_box_service = $delete_custom_box_service;
        $this->default_custom_box_product_service = $default_custom_box_product_service;
    }

    /**
     * @param Subscription $subscription
     * @param $data
     * @return Subscription|bool
     */
    public function save(Subscription $subscription, $data)
    {
        try
        {
            $box = $this->box($subscription);
            // for old users, if box was a suggested by bug.
            $box = CreateContent::replicateBoxIfSuggested($box);
            if(!empty($box->items))
                $this->delete_custom_box_service->remove_items($box);
            $box = $this->create_custom_box_content_service->create($box,$data);

            OrderItem::destroy($subscription->order->items->pluck('id')->toArray());

            $this->create_custom_box_content_service->recreateOrder($subscription->order, $data);

            $subscription->order->custom_box_id = $box->id;
            $subscription->order->save();

            $subscription->save();

            //$subscription->order->load('items'); // after this lines, wall-discount on order-item was gone
                                                    // i guess, the issue was another, but this line doesn't make
                                                    // sense, as we create them right before. need retest if enable

            if($subscription->order->cancellation_deadline->gt(Carbon::now()))
                OrderService::generateSKU($subscription->order);

            return $subscription;
        }
        catch (\Exception $e)
        {
            Log::info($this::LOG_TITLE . ' exception in save: '.$e);
            app('sentry')->captureException($e);
            throw $e;
            //return false;
        }
        return false;
    }

    /**
     * Find the box or create new one if now exist
     * @param Subscription $subscription
     * @return \App\Models\Box
     */
    private function box(Subscription $subscription)
    {
        try
        {
            return Box::findOrFail($subscription->order->custom_box_id);
        }
        catch (ModelNotFoundException $ex)
        {
            return $this->create_custom_box_service->create();
        }
    }
}