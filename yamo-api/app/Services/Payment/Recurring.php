<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 3/26/18
 * Time: 3:49 PM
 */
namespace App\Services\Payment;

use Illuminate\Support\Collection;
use App\Models\Order;

class Recurring
{
    public function list($dates_per_country)
    {
        $orders = new Collection();
        foreach ($dates_per_country as $country_id => $dates)
        {
            foreach($dates as $date) {
                $orders = $orders->merge($this->orders($date,[$country_id]));
            }
        }

        return $orders;
    }

    protected function orders($date, $countries)
    {
        $payment_methods = [
            Order::PAYMENT_METHOD_CREDITCARD,
            Order::PAYMENT_METHOD_PAYPAL,
            Order::PAYMENT_METHOD_INVOICE,
            Order::PAYMENT_METHOD_SOFORT
        ];

        $orders = Order::query()
            ->select('orders.*')
            ->leftJoin('subscriptions','subscriptions.order_id','=','orders.id')
            ->whereDate('orders.delivery_date',$date)
            ->whereIn('orders.country_id',$countries)
            ->whereRaw("orders.status not in ('".Order::STATUS_CANCELLED."')")
            ->WhereIn('orders.payment_method_id',$payment_methods) // Should paypal be here? this might be a bug, for some reason we never get paypal items here.
            ->where('orders.created_via','subscription')
            ->whereNull('orders.paid_date')
            ->whereNull('orders.completed_date')
            ->whereNotNull('orders.parent_id')
            ->where('orders.status','pending')
            ->orderBy('orders.created_via','desc')
            ->orderBy('orders.id','asc')
            ->get();
        return $orders;
    }
}