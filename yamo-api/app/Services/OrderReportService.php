<?php


namespace App\Services;

use App\Models\Country;
use App\Models\DeliveryDate;
use App\Services\OrderReport\Data\DataFactory;
use App\Services\OrderReport\Transformer\TransformerFactory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class OrderReportService
{
    const PROWITO = 'PROWITO';
    const FRIGOSUISSE = 'FRIGOSUISSE';
    protected $order_report_data_factory;
    protected $order_report_transformer_factory;
    protected $order_data;

    /**
     * OrderReportService constructor.
     */
    public function __construct()
    {
        $this->order_report_data_factory = new DataFactory();
        $this->order_report_transformer_factory = new TransformerFactory();
    }

    /**
     * @param $dates_per_country
     * @param $factory
     * @return OrderReport\Transformer\TransformerAustria|OrderReport\Transformer\TransformerGermany|OrderReport\Transformer\TransformerSwitzerland
     */
    public function setOrderData($dates_per_country, $factory)
    {
        $orders = new Collection();
        foreach ($dates_per_country as $country_id => $dates)
        {
            $country_iso_alpha_2 = Country::find($country_id)->iso_alpha_2;
            foreach($dates as $date) {
                $orders = $orders->merge($this->order_report_data_factory->get($country_iso_alpha_2)->set_date($date)->get());
            }
        }

        $this->order_data = $this->order_report_transformer_factory->get($orders, $factory);
        return $this->order_data;
    }

    /**
     * @param $dates_per_country
     * @param $factory
     * @return mixed
     */
    public function getData($dates_per_country, $factory)
    {
        $this->order_data = $this->setOrderData($factory, $dates_per_country);
        return $this->order_data->getOrderData();
    }

    /**
     * @param $factory
     * @param $dates
     * @return array
     */
    public function create($factory, $dates)
    {
        $files = [];

        $filename = '';
        $template_path = '';
        $store_format = $factory === self::PROWITO ? 'csv' : 'xlsx';
        switch ($factory)
        {
            case self::PROWITO:
                $filename = 'Adresslabel-Prowito';
                $template_path = 'app/template-prowito.xlsx';
                break;
            case self::FRIGOSUISSE:
                $filename = 'Addresslabel-Frigosuisse';
                $template_path = 'app/template-frigosuisse-addresslabel.xlsx';
                break;
        }

        $this->order_data = $this->setOrderData($dates, $factory);
        $ret = $this->order_data->transform();

        // now get the file filled with transformed data from somewhere...
        // another factory?
        $excel = app('excel');
        $adreslabel_data = $ret['addresslabel'];
        if(empty($adreslabel_data)){
            return $files;
        }

        $excel_file = $excel->load(storage_path($template_path), function ($file) use ($adreslabel_data) {
            // find sheet named Delivery
            $sheet = $file->sheet(0);
            // append one empty row as in this version it doesn't see header row at all and overrides it
            // like this we are able to skip header row and start appending after it
            $sheet->appendRow(2, []);
            foreach ($adreslabel_data as $row) {
                $sheet->appendRow($row);
            }
        })
            ->setFilename($filename)
            ->store($store_format,false,true)['full'];

        $files[] = $excel_file;
        return $files;
    }

    /**
     * @param $list_of_country_codes
     * @param $date
     * @return array
     */
    public function getDeliveryDatesByCountryId($list_of_country_codes, $date) {
        $dates = [];
        foreach ($list_of_country_codes as $country_code) {
            $country_id = Country::where('iso_alpha_2', $country_code)->first()->id;
            $delivery_dates = DeliveryDate::where('country_id', $country_id)->whereDate('export_date', $date)->get()->toArray();
            if (count($delivery_dates)) {
                $dates[$country_id] = array_map(function ($item) {return $item['delivery_date'];}, $delivery_dates);
            }
        }

        return $dates;
    }
}