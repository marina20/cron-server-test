<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/26/18
 * Time: 2:56 PM
 */

namespace App\Services\DuplicateOrder;

use App\Models\Order;

class DuplicateOrder
{
    /**
     * We are duplicating the order with it's user, country and subscription relations
     * @param Order $order
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function duplicate(Order $order)
    {
        // replicate relations user, subscription, country
        $order->load('user','subscription', 'country');

        return $order->replicate();
    }
}