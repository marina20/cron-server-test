<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/26/18
 * Time: 2:56 PM
 */

namespace App\Services\DuplicateOrder;

use App\Models\OrderItem;

class DuplicateOrderItem
{
    public function duplicate(OrderItem $item)
    {
        try
        {
            //replicate relation product
            $item->load('product');

            return $item->replicate();
        }
        catch (\Exception $e)
        {
            app('sentry')->captureException($e);
        }

    }
}