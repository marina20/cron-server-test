<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/6/18
 * Time: 3:21 PM
 */

namespace App\Services;


use App\Models\Subscription;
use App\Models\Order;
use Carbon\Carbon;
use DB;
class SetNextDeliveryDate
{
    /**
     * Check if new delivery date is not before already existing delivery date and if it is not in the past and change
     * order delivery date, order cancellation_deadline and subscription schedule_next_payment date
     *
     * @param Subscription $subscription
     * @param Carbon $date
     * @return bool
     */
    public function set(Subscription $subscription, Carbon $date)
    {
        try {
            $subscription->order->delivery_date = $date->toDateTimeString();
            $subscription->order->cancellation_deadline = $date->subDay(Order::ORDER_SUBSCRIPTION_CANCELLATION_PERIOD)->endOfDay()->toDateTimeString();
            $subscription->order->save();
            $subscription->schedule_next_payment = $date->subDay(Subscription::SCHEDULE_NEXT_PAYMENT_DAYS)->endOfDay()->toDateTimeString();
            $oldestOrder = Order::where([["subscription_id","=",$subscription->id],["paid_date","!=",'']])->whereNotNull('paid_date')->whereNull('parent_id')->orderBy("created_at","asc")->first();
            if(empty($oldestOrder)){
              $subscription->cancelled_at = null;
            } else {
              $subscription->cancelled_at = DB::raw('now()');
            }
            $subscription->save();
            return true;
        } catch (\Exception $ex) {
            app('sentry')->captureException($ex);
            return false;
        }
    }
}