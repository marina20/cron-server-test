<?php

namespace App\Services\Adyen;
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2/26/18
 * Time: 2:36 PM
 */
use App\Models\Order;

class RecurringPaymentData
{
    protected $value;
    protected $currency;
    protected $reference;
    protected $merchant_account;
    protected $shopper_email;
    protected $shopper_reference;
    protected $shopper_ip;

    public function __construct(Order $order)
    {
        $this->value = $order->order_total * 100;
        $this->currency = $order->getCurrencyCode($order->currency);
        $this->reference = $order->id;
        $this->merchant_account = env('ADYEN_MERCHANT_ACCOUNT');
        $this->shopper_email = $order->shipping_email;
        $this->shopper_reference = $order->user_id;
        $this->shopper_ip = $order->customer_ip_address;
    }

    public function value()
    {
        return $this->value;
    }

    public function currency()
    {
        return $this->currency;
    }

    public function reference()
    {
        return $this->reference;
    }

    public function merchant_account()
    {
        return $this->merchant_account;
    }

    public function shopper_email()
    {
        return $this->shopper_email;
    }

    public function shopper_reference()
    {
        return $this->shopper_reference;
    }

    public function shopper_ip()
    {
        return $this->shopper_ip;
    }
}