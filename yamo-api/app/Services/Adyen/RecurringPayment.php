<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2/28/18
 * Time: 10:20 AM
 */

namespace App\Services\Adyen;

use App\Services\Adyen\AdyenService;
use App\Services\Adyen\RecurringPaymentCheckOrder;
use App\Services\Adyen\RecurringPaymentData;
use App\Services\Adyen\RecurringPaymentSaveOrder;
use App\Models\Order;
use Illuminate\Http\Response;

class RecurringPayment
{
    protected $adyen_service;
    protected $save_order;
    protected $check_order;
    protected $order;

    public function __construct(AdyenService $adyen_service,
                                RecurringPaymentSaveOrder $save_order,
                                RecurringPaymentCheckOrder $check_order)
    {
        $this->adyen_service = $adyen_service;
        $this->save_order = $save_order;
        $this->check_order = $check_order;
    }

    public function pay(Order $order)
    {
        $this->order = $order;
        if(!$this->check_order->can_be_processed($order))
        {
	        return [
                'success'=>'Failed',
                'message'=>'FAILURE! Order ' . $order->id . ' is not eligible for processing!'
            ];
        }
        $recurringPaymentData = new RecurringPaymentData($order);

        $result = $this->adyen_service->recurring($recurringPaymentData);

        $result_save_order = $this->save_order->save($order, $result);

        return [
            'resultCode' => $result['resultCode'],
            'message' => $result_save_order
        ];
    }
}