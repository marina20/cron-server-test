<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 4/23/18
 * Time: 1:02 PM
 */

namespace App\Services\Adyen;

use App\Models\AdyenNotification;
use App\Models\Order;
use App\Models\Voucher;
use App\Services\Adyen\Notification\Authorisation;
use App\Services\Adyen\Notification\Refund;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class Notification
{
    const ADYEN_NOTIFICATION_VALID_EVENT_CODES = ['AUTHORISATION','REFUND'];
    const ADYEN_NOTIFICATION_EMAILS = ['NOTIFICATION_OF_CHARGEBACK','CHARGEBACK'];
    const LOG_TITLE = 'Adyen notification processing:';
    const ADYEN_NOTIFICATION_EVENT_CODE_REFUND = 'REFUND';
    const ADYEN_NOTIFICATION_EVENT_CODE_AUTHORISATION = 'AUTHORISATION';
    const ADYEN_NOTIFICATION_EVENT_CODE_CHARGEBACK = 'CHARGEBACK';
    const ADYEN_NOTIFICATION_EVENT_CODE_NOTIFICATION_OF_CHARGEBACK = 'NOTIFICATION_OF_CHARGEBACK';
    const TRUE_AS_A_STRING = 'true';

    protected $adyen_notification_authorisation;
    protected $adyen_notification_refund;

    public function __construct( Authorisation $authorisation, Refund $refund)
    {
        $this->adyen_notification_authorisation = $authorisation;
        $this->adyen_notification_refund = $refund;
    }

    public function process(AdyenNotification $notification)
    {
        $data = json_decode($notification->notification_body);

        if(count($data->notificationItems) > 1)
        {
            Log::info($this::LOG_TITLE . ' This one has suspiocious notification items, notification id: ' . $notification->id . ' count of them: ' . count($data->notificationItems));
            app('sentry')->captureException( new \Exception($this::LOG_TITLE . ' This one has suspicious notification items, notification id: ' . $notification->id . ' count of them: ' . count($data->notificationItems) ));
            return false;
        }

        if(count($data->notificationItems) == 0)
        {
            Log::info($this::LOG_TITLE .' This one does not have notification items, notification id: ' . $notification->id);
            app('sentry')->captureException( new \Exception($this::LOG_TITLE . ' This one does not have notification items, notification id: ' . $notification->id));
            return false;
        }

        $notification_item = reset($data->notificationItems);

        if($notification_item->NotificationRequestItem->eventCode) {
            $notification_request_item = $notification_item->NotificationRequestItem;
            $event_code = $notification_request_item->eventCode;
            if(!in_array($event_code, $this::ADYEN_NOTIFICATION_VALID_EVENT_CODES))
            {
                Log::info($this::LOG_TITLE .' This one does not have correct event code, notification id: ' . $notification->id . ', code: '. $event_code);
            }
            if (in_array($event_code, $this::ADYEN_NOTIFICATION_EMAILS))
            {
                $this->sendEmailToCustomerService($notification_request_item, $notification->id);
            }
            if($event_code == $this::ADYEN_NOTIFICATION_EVENT_CODE_REFUND){
                $order = $this->adyen_notification_refund->process($notification_request_item, $notification->id);
            }
            if($event_code == $this::ADYEN_NOTIFICATION_EVENT_CODE_CHARGEBACK){
                $order = $this->adyen_notification_refund->process($notification_request_item, $notification->id);
                $order->status = Order::STATUS_CANCELLED;
                $order->save();
            }
            else if($event_code == $this::ADYEN_NOTIFICATION_EVENT_CODE_AUTHORISATION){
                $order = $this->adyen_notification_authorisation->process($notification_request_item, $notification->id);
            }
            else{
                Log::info($this::LOG_TITLE . 'we will never reach this as its filtered before, we should filter better though.');
            }
            return true;
        } else {
            return false;
        }
    }

    private function sendEmailToCustomerService($notification_request_item, $notification_id)
    {
        $order = Order::find($notification_request_item->merchantReference);
        $lb = "\r\n\r\n";
        $today = Carbon::now()->toDateString();

        $msg_string = 'Check chargeback event happend with notification id: ' . $notification_id;
        if ($order) {
            $msg_string = 'Please send an email to the customer: ' .
                $order->billing_email .
                ' regarding order id: ' .
                $order->id .
                ' and find out the reason of the chargeback.';
        }

        Mail::raw($msg_string, function ($msg) use ($today){
            $msg->from('cron@yamo.ch');
            $msg->to(env('MAIL_FROM_ADDRESS'));
            $msg->subject('Chargeback');
        });
    }
}
