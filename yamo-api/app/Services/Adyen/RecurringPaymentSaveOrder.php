<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2/28/18
 * Time: 11:07 AM
 */

namespace App\Services\Adyen;

use App\Jobs\SendPaymentFailureCancelEmailJob;
use App\Models\Order;
use App\Models\ReferralUrl;
use App\Models\Subscription;
use App\Models\Voucher;
use App\Models\VoucherRestriction;
use App\Services\CancelSubscriptionService;
use Carbon\Carbon;
use Auth;

class RecurringPaymentSaveOrder
{
    const RESULT_KEY_ERROR = 'error';
    const RESULT_KEY_PSP_REFERENCE = 'pspReference';
    const RESULT_KEY_RESULT_CODE = 'resultCode';
    const RESULT_KEY_AUTH_CODE = '';
    const RESULT_CODE_SUCCESS = 'Authorised';
    const RESULT_CODE_RECEIVED = 'Received';

    protected $cancel_subscription_service;

    public function __construct(CancelSubscriptionService $cancel_subscription_service)
    {
        $this->cancel_subscription_service = $cancel_subscription_service;
    }

    public function save(Order $order, $result)
    {
        try
        {
            if(array_key_exists($this::RESULT_KEY_ERROR,$result)){
                return 'FAILURE!' . $result[$this::RESULT_KEY_ERROR];
            }

            if (empty($order->transaction_id))
            {
                $order->transaction_id = $result[$this::RESULT_KEY_PSP_REFERENCE];
            }
            $order->adyen_auth_result = $result[$this::RESULT_KEY_RESULT_CODE];
            $order->completed_date = Carbon::now()->toDateTimeString();

            switch ($result[$this::RESULT_KEY_RESULT_CODE])
            {
                case $this::RESULT_CODE_SUCCESS:
                    {
                        $order->paid_date = Carbon::now()->toDateTimeString();
                        $order->status = Order::STATUS_PAYED;
                    }
                    break;
                case $this::RESULT_CODE_RECEIVED:
                    $order->status = Order::STATUS_PENDING;
                    break;
                default:
                    dispatch(new SendPaymentFailureCancelEmailJob($order));
                    $order->status = Order::STATUS_CANCELLED;
                    $order = ReferralUrl::revertWallet($order);
                    if($order->subscription instanceof Subscription) {
                        $this->cancel_subscription_service->cancel($order->subscription);
                    }
                    break;
            }

            $order->save();
        }
        catch(\Exception $ex)
        {
            app('sentry')->captureException($ex);
            return 'FAILURE! Order ' . $order->id . ' saving has failed. ' . $ex->getMessage();
        }

        return 'SUCCESS! Order ' . $order->id . ' has been processed and saved. Order status: ' . $order->status;
    }
}