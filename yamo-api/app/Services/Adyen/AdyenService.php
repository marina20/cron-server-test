<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2/26/18
 * Time: 12:20 PM
 */

namespace App\Services\Adyen;

use \Adyen\Client;
use \Adyen\Service;
use \Adyen\Environment;
use \Adyen\Contract;
use \Adyen\AdyenException;
use Illuminate\Http\Response;
use App\Services\Adyen\RecurringPaymentData;
use App\Models\RecurringAdyenRequest;

class AdyenService
{
    protected $client;
    protected $service;

    public function __construct()
    {

        $client = new Client();
        $client->setApplicationName("Adyen PHP Api Library Example");
        $client->setUsername(env('ADYEN_RECURRING_USERNAME'));
        $client->setPassword(env('ADYEN_RECURRING_PASSWORD'));
        $client->setEnvironment(env('ADYEN_RECURRING_ENVIRONMENT'));
        $this->client = $client;

        $this->service = new Service\Payment($client);
    }

    public function recurring(RecurringPaymentData $data)
    {
        $json = '{
              "amount": {
                "value": ' . $data->value() . ',
                "currency": "' . $data->currency() . '"
              },
              "reference": "' . $data->reference() . '",
              "merchantAccount": "' . $data->merchant_account() . '",
               "shopperEmail":"' . $data->shopper_email() . '",
               "shopperReference":"' . $data->shopper_reference() . '",
               "shopperIP":"' . $data->shopper_ip() . '",
               "selectedRecurringDetailReference":"LATEST",
               "recurring":{
                  "contract":"RECURRING"
               },
               "shopperInteraction":"ContAuth"
                    }';

        $params = json_decode($json, true);
        $adyen = new RecurringAdyenRequest();
        $adyen->request = json_encode($params);
        $adyen->order_id = $data->reference();
        $adyen->user_id = $data->shopper_reference();

        try
        {
            $result = $this->service->authorise($params);
        }
        catch (AdyenException $aex)
        {
            $result['resultCode'] = 'FAILURE';
            $result['error'] = $aex->getMessage();
        }

        $adyen->response = json_encode($result);
        $adyen->save();
        return $result;
    }
}
