<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2/28/18
 * Time: 11:11 AM
 */

namespace App\Services\Adyen;

use App\Models\Order;

class RecurringPaymentCheckOrder
{
    public function can_be_processed(Order $order)
    {
        if($order->status != Order::STATUS_PENDING)
            return false;

        if(!empty($order->completed_date))
            return false;

        if(!empty($order->paid_date))
            return false;

        return true;
    }
}