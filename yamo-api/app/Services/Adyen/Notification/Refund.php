<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 4/23/18
 * Time: 12:58 PM
 */

namespace App\Services\Adyen\Notification;

use App\Models\Order;
use App\Models\OrderRefund;
use App\Services\Adyen\Notification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class Refund
{
    const LOG_TITLE = 'Adyen notification refund processing:';

    public function process($notification_request_item, $notification_id=null)
    {
        try
        {
            $order = Order::find($notification_request_item->merchantReference);

            if(empty($order))
            {
                Log::info($this::LOG_TITLE . ' This order does not exists, notification id: ' . $notification_id . ', order id: '. $notification_request_item->merchantReference);
                app('sentry')->captureException( new \Exception($this::LOG_TITLE . ' This order does not exists,  notification id: ' . $notification_id. ', order id: '. $notification_request_item->merchantReference));
                return false;
            }

            $event_date = Carbon::createFromFormat(Carbon::ATOM, $notification_request_item->eventDate)->format('Y-m-d H:i:s');
            $event_code = $notification_request_item->eventCode;
            $psp_reference = $notification_request_item->pspReference;

            $success = $notification_request_item->success;

            $orderRefund = new OrderRefund();
            $orderRefund->order_id = $order->id;
            $orderRefund->adyen_notification_id = $notification_id;
            $orderRefund->live_environment = Notification::TRUE_AS_A_STRING;
            $orderRefund->event_date = $event_date;
            $orderRefund->event_code = $event_code;
            $orderRefund->success = $success;
            if ($notification_request_item->amount->value) {
                $orderRefund->value = $notification_request_item->amount->value/100;
            }
            $orderRefund->currency = $notification_request_item->amount->currency;
            if(isset($notification_request_item->additionalData->shopperCountry))
                $orderRefund->country = $notification_request_item->additionalData->shopperCountry;
            $orderRefund->save();

            if($success != Notification::TRUE_AS_A_STRING)
            {
                Log::info($this::LOG_TITLE . ' This order have REFUND success state FALSE, notification id: ' . $notification_id . ', order id: '. $notification_request_item->merchantReference);
                app('sentry')->captureException( new \Exception($this::LOG_TITLE . ' This order have REFUND success state FALSE,  notification id: ' . $notification_id. ', order id: '. $notification_request_item->merchantReference));
                return false;
            }

            $order->completed_date = $event_date;
            $order->adyen_auth_result = $event_code;
            if (empty($order->transaction_id))
            {
                $order->transaction_id = $psp_reference;
            }
            $order->save();

            return $order;
        } catch (\Exception $e)
        {
            // it's sent to sentry...
            app('sentry')->captureException($e);
            Log::info($this::LOG_TITLE . ' exception triggered: '.$e->getMessage());
            return false;
        }
    }
}
