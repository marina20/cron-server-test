<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 4/23/18
 * Time: 12:58 PM
 */

namespace App\Services\Adyen\Notification;

use App\Jobs\SendOrderConfirmationEmailJob;
use App\Jobs\SendSingleBoxConfirmationEmailJob;
use App\Jobs\SendSubscriptionConfirmationEmailJob;
use App\Models\Order;
use App\Models\ReferralUrl;
use App\Models\Voucher;
use App\Services\Adyen\Notification;
use App\Services\CancelSubscriptionService;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class Authorisation
{
    const LOG_TITLE = 'Adyen notification authorisation processing:';
    const AUTHORISED_STATUS = 'AUTHORISED';

    protected $cancel_subscription_service;

    public function __construct(CancelSubscriptionService $cancel_subscription_service)
    {
        $this->cancel_subscription_service = $cancel_subscription_service;
    }

    public function process($notification_request_item, $notification_id=null)
    {
        try
        {
            $order = Order::find($notification_request_item->merchantReference);

            // we don't process recurring payment notifications and we don't send them confirmation emails
            // delivery reminder emails are enough

            if(empty($order))
            {
                Log::info($this::LOG_TITLE . ' This order does not exists, notification id: ' . $notification_id . ', order id: '. $notification_request_item->merchantReference);
                app('sentry')->captureException( new \Exception($this::LOG_TITLE . ' This order does not exists,  notification id: ' . $notification_id. ', order id: '. $notification_request_item->merchantReference));
                return false;
            }

            $user_id = $notification_request_item->additionalData->shopperReference;
            if($user_id != $order->user_id)
            {
                Log::info($this::LOG_TITLE . ' This order has wrong user id, notification id: ' . $notification_id . ', order id: '. $notification_request_item->merchantReference);
                app('sentry')->captureException( new \Exception($this::LOG_TITLE . ' This order has wrong user id,  notification id: ' . $notification_id. ', order id: '. $notification_request_item->merchantReference));
                return false;
            }

            $amount = number_format($notification_request_item->amount->value / 100,2);
            if($amount != number_format($order->order_total,2))
            {
                Log::info($this::LOG_TITLE . ' This order has wrong paid amount, notification id: ' . $notification_id . ', order id: '. $notification_request_item->merchantReference);
                app('sentry')->captureException( new \Exception($this::LOG_TITLE . ' This order has wrong paid amount,  notification id: ' . $notification_id. ', order id: '. $notification_request_item->merchantReference));
                return false;
            }

            $currency = $notification_request_item->amount->currency;
            if($currency != $order->getCurrencyCode($order->currency))
            {
                Log::info($this::LOG_TITLE . ' This order has wrong currency, notification id: ' . $notification_id . ', order id: '. $notification_request_item->merchantReference);
                app('sentry')->captureException( new \Exception($this::LOG_TITLE . ' This order has wrong currency,  notification id: ' . $notification_id. ', order id: '. $notification_request_item->merchantReference));
                return false;
            }

            $psp_reference = $notification_request_item->pspReference;
            $payment_method = $notification_request_item->paymentMethod;

            if (empty($order->transaction_id))
            {
                $order->transaction_id = $psp_reference;
            }

            $order->adyen_payment_method = $payment_method;

            $success = $notification_request_item->success;

            $event_date = Carbon::createFromFormat(Carbon::ATOM, $notification_request_item->eventDate)->format('Y-m-d H:i:s');

            if($success == Notification::TRUE_AS_A_STRING)
            {
                if(is_null($order->parent_id)) {
                    if(!empty($order->subscription)){
                        if(false !== strpos($order->created_via, Order::CREATED_VIA_SUBSCRIPTION)){
                            dispatch(new SendSubscriptionConfirmationEmailJob($order->subscription));
                        }
                    }
                    else if($order->created_via == Order::CREATED_VIA_SINGLE_BOX) {
                        dispatch(new SendSingleBoxConfirmationEmailJob($order));
                    }
                    else{
                        // if there is no subscription and the product is bought somewhere rather
                        // than the single box theres something fishy going on here. At least they paid.
                        //TODO: make sure SendOrderConfirmationEmailJob is up to date.
                        dispatch(new SendOrderConfirmationEmailJob($order));
                    }
                }
                $order->paid_date = $event_date;
                $order->completed_date = $event_date;
                $order->status = Order::STATUS_PAYED;
                $order->adyen_auth_result = $notification_request_item->eventCode;
                $order = Voucher::redeemVoucher($order);
            }
            else
            {
                $reason = $notification_request_item->reason;
                $order->status = Order::STATUS_CANCELLED;
                $order = ReferralUrl::revertWallet($order);
                $order->completed_date = $event_date;
                $order->adyen_auth_result = $reason;
                if($order->subscription) {
                    $this->cancel_subscription_service->cancel($order->subscription);
                }
            }
            $order->save();

            return $order;
        } catch (\Exception $e)
        {
            // it's sent to sentry...
            app('sentry')->captureException($e);
            Log::info($this::LOG_TITLE . ' exception triggered: '.$e->getMessage());
            return false;
        }
    }
}
