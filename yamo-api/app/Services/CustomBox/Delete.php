<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/6/18
 * Time: 1:58 PM
 */

namespace App\Services\CustomBox;

use App\Models\Box;
use App\Models\BoxContent;

class Delete
{
    /**
     * Remove items from the Box and then remove the Box too
     *
     * @param Box $box
     * @return int
     */
    public function delete(Box $box)
    {
        $this->remove_items($box);
        return Box::destroy($box->id);
    }

    /**
     * Remove all items from Box
     *
     * @param Box $box
     * @return int
     */
    public function remove_items(Box $box)
    {
        return BoxContent::destroy($box->items->pluck('id')->toArray());
    }
}