<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/6/18
 * Time: 1:29 PM
 */

namespace App\Services\CustomBox;

use App\Models\Box;

class Create
{
    public function create()
    {
        $box = new Box();
        $box->product_id = null;
        $box->type = Box::TYPE_CUSTOM;
        $box->status = Box::STATUS_ACTIVE;
        $box->save();
        return $box;
    }
}