<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/17/18
 * Time: 9:40 AM
 */

namespace App\Services\CustomBox;

use App\Models\Product;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class DefaultProduct
{
    public function get()
    {
        try
        {
            $custom_box_product = Product::where('category',Product::PRODUCT_CATEGORY_CUSTOM)->firstOrFail();
            return $custom_box_product;
        } catch (ModelNotFoundException $ex)
        {
            return null;
        }
    }
}