<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/6/18
 * Time: 1:32 PM
 */

namespace App\Services\CustomBox;


use App\Models\VoucherRedemption;
use App\Traits\FormatMoney;
use Auth;
use App\Models\BoxContent;
use App\Models\Box;
use App\Models\User;
use App\Models\Country;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\Voucher;
use App\Services\CreateOrder\CreateOrder;
use App\Models\SuggestedBox;
use Log;

class CreateContent
{
    use FormatMoney;
    const LOG_TITLE = 'Service CreateContent: ';
    const ITEM_INPUT_ARRAY_KEY_ID = 'id';
    const ITEM_INPUT_ARRAY_KEY_QUANTITY = 'quantity';
    /**
     * Create BoxContent
     *
     * @param Box $box
     * @param $items
     * @return mixed
     */
    public function create(Box $box, $items)
    {
        foreach ($items as $item) {
            $box_content = new BoxContent();
            $box_content->box_id = $box->id;
            $box_content->product_id = $item[$this::ITEM_INPUT_ARRAY_KEY_ID];
            $box_content->quantity = $item[$this::ITEM_INPUT_ARRAY_KEY_QUANTITY];
            $box_content->save();
        }
        return $box;
    }


    /**
     * Check if string contains a digit
     * @param $input
     * @return bool
     */
    public static function includeDigits($input):bool{
        $slicedInput = str_split($input);
        foreach($slicedInput as $char){
            if(is_numeric($char)){
                return true;
            }
        }
        return false;
    }

    /**
     * This function is for migrating the old formatted address_1 to street_name and nr.
     * It is meant to be read-only
     * @param $address
     * @return array, pos 0: street_name, pos 1: street_nr
     */
    public static function migrateAddress($address){
        $address = trim($address);
        $streetArray = explode(" ",$address);
        $streetName = $address;
        $streetNr = ''; // '' because this is wanted by FE. 0 would be save into db.
        if(sizeof($streetArray)==1){
            return [$streetName,$streetNr];
        }
        $conditionNr = array_pop($streetArray);
        // Housenr can be max 5 chars, defined by Prowito/DHL. Also for guessing,
        // we check if the last element contains any digit to be a bit more shure, it's the street_nr.
        if(strlen($conditionNr) <= 5 && CreateContent::includeDigits($conditionNr)){
           $streetNr = $conditionNr;
        } else {
           array_push($streetArray, $conditionNr);
        }
        $streetName = implode(" ",$streetArray);
        return [$streetName,$streetNr];
    }

    public static function replicateBoxIfSuggested($box){
        $usedBox = $box;
        if($box->type=="Suggested"){
            $usedBox = $box->replicate();
            $usedBox->type = "custom";
            $suggestedBox = SuggestedBox::where(['box_id' => $box->id])->first();
            if(!empty($suggestedBox)) {
                $usedBox->original_box_id = $suggestedBox->box_id;
            }
            $usedBox->save();
            $usedItems = $box->items;
            foreach($usedItems as $item){
                $newItem = $item->replicate();
                $newItem->box_id = $usedBox->id;
                $newItem->save();
            }
        }
        return $usedBox;
    }

    /**
     * This method had to be altered!
     * The only use is for SubscriptionCustomBoxSave.php, which use it to save after customing
     * NOW other function uses it too, but still it needs to be altered
     * @param Order $order
     * @param $products
     * @return Order
     */
    public function recreateOrder(Order $order, $products)
    {
        try
        {
            // loop items
            $total = 0;
            $fullTotal = 0;
            $totalDiscount = 0;
            $systemDiscount = 0;
            $publicDiscount = 0;
            $taxRate = 0;
            $systemDiscounts = [];
                foreach ($products as $item) {
                $product = Product::find($item['id']);
                // price helper
                $country = Country::find($order->country_id);
                $countryIsoAlpha2 = $country->iso_alpha_2;
                $fullPrice = $product->price;
                $fullTotal += ($fullPrice * $item['quantity']);
                $price = $product->price;
                $order_item = new OrderItem();
                $discount = 0;
                $systemVoucherCode = "";
                if(!empty($order->is_subscription)){
                    if ($order->user->isNew()) {
                        if ($product->size == 200) {
                            $systemVoucherCode = "FIRST_SUBSCRIPTION_200_" . $countryIsoAlpha2;
                            $result = Voucher::getDiscountedAmount("FIRST_SUBSCRIPTION_200_" . $countryIsoAlpha2, $price);
                        } else {
                            $systemVoucherCode = "FIRST_SUBSCRIPTION_" . $countryIsoAlpha2;
                            $result = Voucher::getDiscountedAmount('FIRST_SUBSCRIPTION_' . $countryIsoAlpha2, $price);
                        }
                    } else {
                        if ($product->size == 200) {
                            $systemVoucherCode = "SUBSCRIPTION_200_" . $countryIsoAlpha2;
                            $result = Voucher::getDiscountedAmount("SUBSCRIPTION_200_" . $countryIsoAlpha2, $price);
                        } else {
                            $systemVoucherCode = "SUBSCRIPTION_" . $countryIsoAlpha2;
                            $result = Voucher::getDiscountedAmount('SUBSCRIPTION_' . $countryIsoAlpha2, $price);
                        }
                    }
                    $price = ($price - $result['discount']);
                    $discount = ($result['discount'] * $item['quantity']);
                    if(empty($systemDiscounts[$systemVoucherCode])) {
                        $systemDiscounts[$systemVoucherCode] = $discount;
                    } else {
                        $systemDiscounts[$systemVoucherCode] = $systemDiscounts[$systemVoucherCode] + $discount;
                    }
                    $order_item->system_discount = $result['discount'] * $item['quantity'];
                    $order_item->system_discount_tax = CreateOrder::calculateVAT($discount, $product->tax_percentage);
                    $systemDiscount += $order_item->system_discount;
                }
/*
                if(!empty($order->coupon_code)){
                    $result = Voucher::getDiscountedAmount($order->coupon_code, $price);
                    $price = ($price - $result['discount']);
                    $discount += $result['discount'] * $item['quantity'];
                    $order_item->public_discount = $result['discount'] * $item['quantity'];
                    $order_item->public_discount_tax = CreateOrder::calculateVAT(($result['discount'] * $item['quantity']), $product->tax_percentage);
                    $publicDiscount += $order_item->public_discount;
                }
*/
                // let's create order item
                $order_item->product_id = $product->id;
                $order_item->order_id = $order->id;
                $order_item->item_name = $product->name;
                $order_item->item_type = OrderItem::ITEM_TYPE_LINE;
                $order_item->qty = $item['quantity'];
                $order_item->item_price = $price;
                $order_item->tax_rate = $product->tax_percentage;
                // discounts will be added in different method - that comment is deprecated - we need to do that here, cause this is used by the save-method
                $order_item->total_discount = $this->formatMoney($discount);
                $order_item->total_discount_tax = CreateOrder::calculateVAT($discount, $product->tax_percentage);
                // discounts are subtracted from total
                $order_item->subtotal = $this->formatMoney($order_item->qty * $fullPrice);
                $order_item->total = $this->formatMoney($order_item->item_price * $order_item->qty);
                $order_item->total_tax = CreateOrder::calculateVAT($order_item->total, $order_item->tax_rate);
                // we are going to save subtotal before we start calculating discount
                $order_item->subtotal_tax = CreateOrder::calculateVAT($order_item->subtotal, $order_item->tax_rate);
                $order_item->save();
                //
                // $order->items->add($order_item);
                $taxRate = $order_item->tax_rate;
                $total += $this->formatMoney($order_item->item_price) * $order_item->qty;
                $totalDiscount += $discount;
            }
            $order->load('items');
            $order->order_subtotal = $this->formatMoney($fullTotal);
            $order->order_subtotal_tax = $this->formatMoney(CreateOrder::calculateVAT($fullTotal, $taxRate));

            // Delete all vouchers and add them.
            VoucherRedemption::where(['order_id' => $order->id])->delete();
            if(!empty($systemVoucherCode)) {
                // because there can be various system-vouchers, we loop through them.
                foreach($systemDiscounts as $key => $val){
                    $systemVoucher = Voucher::where(['code' => $key])->first()->id;
                    $systemVoucherRedemption = VoucherRedemption::firstOrCreate(['user_id' => Auth::id(), 'order_id' => $order->id, 'voucher_id' => $systemVoucher]);
                    $systemVoucherRedemption->amount_cents = $val;
                    $systemVoucherRedemption->save();
                };
            }
            $order->system_discount = $systemDiscount;
            $order->system_discount_tax = CreateOrder::calculateVAT($systemDiscount, $taxRate);

            /*if(!empty($order->coupon_code)) {
                $publicVoucher = Voucher::where(['code' => $order->coupon_code])->first()->id;
                $publicVoucherRedemption = VoucherRedemption::firstOrCreate(['user_id' => Auth::id(), 'order_id' => $order->id, 'voucher_id' => $publicVoucher]);
                $publicVoucherRedemption->amount_cents = $publicDiscount;
                $publicVoucherRedemption->save();
            }
            $order->public_discount = $publicDiscount;
            $order->public_discount_tax = CreateOrder::calculateVAT($publicDiscount, $taxRate);*/
            $order->discount = $totalDiscount;
            $order->order_tax = CreateOrder::calculateVAT($totalDiscount, $taxRate);
            $order->order_total = $this->formatMoney($total);
            $order->order_tax = CreateOrder::calculateVAT($total, $taxRate);
            $order->order_total_without_tax = $order->order_total - $order->order_tax;
            if(!empty($order->wallet_discount)) {
                $user = User::find($order->user_id);
                $user->profile->wallet_amount_cents += $order->wallet_discount;
                $user->profile->save();
            }
            $order->wallet_discount = 0.00;
            $order->wallet_discount_tax = 0.00;
            $order->public_discount = 0.00;
            $order->public_discount_tax = 0.00;
            $order->loyalty_discount = 0.00;
            $order->loyalty_discount_tax = 0.00;

            $order = Voucher::applyRecurringDiscounts($order);

            $order->discount = $this->formatMoney($order->system_discount + $order->public_discount + $order->loyalty_discount + $order->wallet_discount);
            $order->discount_tax = CreateOrder::calculateVAT($order->discount, $taxRate);
            $order->save();

            return $order;
        }
        catch (\Throwable $ex)
        {
            Log::info($this::LOG_TITLE . ' exception in createOrderItems: '.$ex->getMessage());
            app('sentry')->captureException($ex);
            throw $ex;
        }
    }
}