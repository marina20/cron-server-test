<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-04-05
 * Time: 15:21
 */

namespace App\Services\ZipCity;

use App\Models\Postcode;
use Illuminate\Database\Eloquent\Model;

class ZipCityUpdateShipping extends ZipCityUpdate
{
    protected $postcode;

    /**
     * Update shipping postcode and city for given model
     *
     * @param Model $model
     * @return Model
     */
    function update(Model $model)
    {
        try
        {
            if(!isset($model->shipping_postcode_id))
                return $model;

            $this->getPostcode($model->shipping_postcode_id);

            if($this->postcode instanceof Postcode)
            {
                $model->shipping_postcode = $this->postcode->postcode;
                $model->shipping_city = $this->postcode->city;
            }
        }
        catch (\Exception $ex)
        {
            app('sentry')->captureException($ex);
        }

        return $model;
    }
}