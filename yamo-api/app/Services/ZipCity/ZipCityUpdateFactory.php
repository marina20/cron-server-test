<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-04-08
 * Time: 14:23
 */

namespace App\Services\ZipCity;

use Illuminate\Database\Eloquent\Model;

class ZipCityUpdateFactory
{
    const ZIP_CITY_TYPE_BILLING = 'Billing';
    const ZIP_CITY_TYPE_SHIPPING = 'Shipping';

    protected $zipCityUpdateBilling;
    protected $zipCityUpdateShipping;

    /**
     * ZipCityUpdateFactory constructor.
     * @param ZipCityUpdateBilling $zipCityUpdateBilling
     * @param ZipCityUpdateShipping $zipCityUpdateShipping
     */
    public function __construct(ZipCityUpdateBilling $zipCityUpdateBilling, ZipCityUpdateShipping $zipCityUpdateShipping)
    {
        $this->zipCityUpdateBilling = $zipCityUpdateBilling;
        $this->zipCityUpdateShipping = $zipCityUpdateShipping;
    }

    /**
     * Call update method of correct class, shipping or billing
     *
     * @param Model $model
     * @param $type
     * @return Model
     */
    public function update(Model $model, $type)
    {
        try
        {
            if(!in_array($type, [self::ZIP_CITY_TYPE_BILLING, self::ZIP_CITY_TYPE_SHIPPING]))
                return $model;

            $propertyName = 'zipCityUpdate'.$type;
            return $this->$propertyName->update($model);
        }
        catch (\Exception $ex)
        {
            app('sentry')->captureException($ex);
        }
        return $model;
    }
}