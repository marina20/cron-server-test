<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-04-05
 * Time: 15:20
 */

namespace App\Services\ZipCity;

use Illuminate\Database\Eloquent\Model;

interface ZipCityInterface
{
    public function update(Model $model);
}