<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-04-05
 * Time: 15:21
 */

namespace App\Services\ZipCity;

use App\Models\Postcode;
use Illuminate\Database\Eloquent\Model;

class ZipCityUpdateBilling extends ZipCityUpdate
{
    protected $postcode;

    /**
     * Update billing postcode and city for given model
     *
     * @param Model $model
     * @return Model
     */
    function update(Model $model)
    {
        try
        {
            if(!isset($model->billing_postcode_id))
                return $model;

            $this->getPostcode($model->billing_postcode_id);

            if($this->postcode instanceof Postcode)
            {
                $model->billing_postcode = $this->postcode->postcode;
                $model->billing_city = $this->postcode->city;
            }
        }
        catch (\Exception $ex)
        {
            app('sentry')->captureException($ex);
        }

        return $model;
    }
}