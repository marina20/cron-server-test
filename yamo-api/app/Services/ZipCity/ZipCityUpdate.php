<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-04-05
 * Time: 15:21
 */

namespace App\Services\ZipCity;

use App\Models\Postcode;
use Illuminate\Database\Eloquent\Model;

abstract class ZipCityUpdate implements ZipCityInterface
{
    protected $postcode;

    /**
     * Update properties of the model
     *
     * @param Model $model
     * @return mixed
     */
    abstract function update(Model $model);

    /**
     * Find postcode model
     *
     * @param $id
     */
    public function getPostcode($id)
    {
        $this->postcode = Postcode::find($id);
    }
}