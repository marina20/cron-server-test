<?php

namespace App\Events;

use App\Models\Profile;
use Illuminate\Queue\SerializesModels;

class ProfileSaving extends Event
{
    use SerializesModels;

    public $profile;

    /**
     * Create a new event instance.
     *
     * @param Profile $profile
     */
    public function __construct(Profile $profile)
    {
        $this->profile = $profile;
    }
}