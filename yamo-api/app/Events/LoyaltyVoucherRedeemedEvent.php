<?php


namespace App\Events;

use App\Models\VoucherRedemption;
use Illuminate\Queue\SerializesModels;

class LoyaltyVoucherRedeemedEvent extends Event
{
    use SerializesModels;

    public $voucherRedemption;

    /**
     * Create a new event instance.
     *
     * @param VoucherRedemption $voucherRedemption
     */
    public function __construct(VoucherRedemption $voucherRedemption)
    {
        $this->voucherRedemption = $voucherRedemption;
    }

}