<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 9/28/17
 * Time: 12:00 AM
 */

namespace App\Jobs;

use App\Mail\EmailNewsletterActivation;
use Illuminate\Support\Facades\Mail;

class SendNewsletterActivationEmailJob extends Job
{
    public $tries = 10;
    protected $email_address;
    protected $confirmation_code;

    public function __construct($email_address, $confirmation_code)
    {
        $this->email_address = $email_address;
        $this->confirmation_code = $confirmation_code;
    }

    /**
     *
     */
    public function handle()
    {
        $email = new EmailNewsletterActivation($this->confirmation_code);
        Mail::to($this->email_address)->send($email);
    }
}
