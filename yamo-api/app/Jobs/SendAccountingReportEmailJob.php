<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/31/18
 * Time: 11:12 AM
 */

namespace App\Jobs;

use App\Mail\EmailAccountingReport;
use Illuminate\Support\Facades\Mail;

class SendAccountingReportEmailJob extends Job
{
    public $tries = 10;

    protected $files;

    public function __construct($files)
    {
        $this->files = $files;
    }
    /**
     *
     */
    public function handle()
    {
        $email = new EmailAccountingReport($this->files);
        Mail::to(env('MAIL_CRON_INFO_ADDRESS'))->send($email);
    }
}