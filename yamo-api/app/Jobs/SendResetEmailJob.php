<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 10/5/17
 * Time: 12:55 PM
 */

namespace App\Jobs;

use App\Models\User;
use App\Mail\EmailResetPassword;
use Illuminate\Support\Facades\Mail;


class SendResetEmailJob extends Job
{
    protected $user;
    protected $token;
    public $tries = 10;

    /**
     * SendResetEmailJob constructor.
     * @param User $user
     */
    public function __construct(User $user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    /**
     *
     */
    public function handle()
    {
        $email = new EmailResetPassword($this->user, $this->token);
        Mail::to($this->user->email)->send($email);
    }
}