<?php
/**
 * Created by PhpStorm.
 * User: juanse254
 * Date: 9/27/18
 * Time: 3:45 PM
 */

namespace App\Jobs;

use App\Mail\EmailPaymentFailureCancel;
use Illuminate\Support\Facades\Mail;
use App\Models\Order;

class SendPaymentFailureCancelEmailJob extends Job
{
	protected $order;
	public $tries = 10;

	/**
	 * Constructor.
	 * @param Order $order
	 */
	public function __construct(Order $order)
	{
		$this->order = $order;
	}

	/**
	 *
	 */
	public function handle()
	{
		$email = new EmailPaymentFailureCancel($this->order);
		Mail::to($this->order->user->email)->send($email);
	}

}