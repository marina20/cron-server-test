<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 10/5/17
 * Time: 12:55 PM
 */

namespace App\Jobs;

use App\Mail\EmailRefereeCompleted;
use App\Models\ReferralUrl;
use App\Models\User;
use App\Models\Order;
use App\Mail\EmailResetPassword;
use Illuminate\Support\Facades\Mail;


class SendReferreeCompletedEmailJob extends Job
{
    protected $referralUrl;
    protected $order;
    public $tries = 10;

    /**
     * SendResetEmailJob constructor.
     * @param User $user
     */
    public function __construct(ReferralUrl $referralUrl,Order $order)
    {
        $this->referralUrl = $referralUrl;
        $this->order = $order;
    }

    /**
     *
     */
    public function handle()
    {
        $email = new EmailRefereeCompleted($this->referralUrl, $this->order);
        Mail::to($this->referralUrl->user->email)->send($email);
    }
}