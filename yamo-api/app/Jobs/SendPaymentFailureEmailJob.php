<?php
/**
 * Created by PhpStorm.
 * User: juanse254
 * Date: 9/26/18
 * Time: 4:54 PM
 */

namespace App\Jobs;


use App\Mail\EmailPaymentFailure;
use Illuminate\Support\Facades\Mail;
use App\Models\Order;


class SendPaymentFailureEmailJob extends Job
{
	protected $order;
	public $tries = 10;

	/**
	 * SendSubscriptionConfirmationEmailJob constructor.
	 * @param User $user
	 */
	public function __construct(Order $order)
	{
		$this->order = $order;
	}

	/**
	 *
	 */
	public function handle()
	{
		$email = new EmailPaymentFailure($this->order);
		Mail::to($this->order->user->email)->send($email);
	}

}