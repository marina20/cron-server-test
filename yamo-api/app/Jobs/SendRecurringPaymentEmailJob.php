<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/31/18
 * Time: 11:12 AM
 */

namespace App\Jobs;

use App\Mail\EmailRecurringPayment;
use Illuminate\Support\Facades\Mail;

class SendRecurringPaymentEmailJob extends Job
{
    public $tries = 10;

    protected $files;

    public function __construct($files)
    {
        $this->files = $files;
    }
    /**
     *
     */
    public function handle()
    {
        $email = new EmailRecurringPayment($this->files);
        Mail::to(env('MAIL_CRON_INFO_ADDRESS'))->bcc(env('MAIL_CRON_BCC_INFO_ADDRESS'))->cc(env('MAIL_CRON_BCC_INFO_ADDRESS_RECURRING_PAYMENTS'))->send($email);
    }
}