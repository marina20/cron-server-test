<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 10/5/17
 * Time: 12:55 PM
 */

namespace App\Jobs;

use App\Models\Subscription;
use App\Models\User;
use App\Mail\EmailSubscriptionConfirmation;
use Illuminate\Support\Facades\Mail;


class SendSubscriptionConfirmationEmailJob extends Job
{
    protected $subscription;
    public $tries = 10;

    /**
     * SendSubscriptionConfirmationEmailJob constructor.
     * @param User $user
     */
    public function __construct(Subscription $subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     *
     */
    public function handle()
    {
        $email = new EmailSubscriptionConfirmation($this->subscription);
        Mail::to($this->subscription->user->email)->send($email);
    }
}