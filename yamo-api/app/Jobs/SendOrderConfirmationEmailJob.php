<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 10/5/17
 * Time: 12:55 PM
 */

namespace App\Jobs;

use App\Models\Order;
use App\Mail\EmailOrderConfirmation;
use Illuminate\Support\Facades\Mail;


class SendOrderConfirmationEmailJob extends Job
{
    protected $order;
    public $tries = 10;

    /**
     * SendOrderConfirmationEmailJob constructor.
     * @param User $user
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     *
     */
    public function handle()
    {
        $email = new EmailOrderConfirmation($this->order);
        Mail::to($this->order->user->email)->send($email);
    }
}