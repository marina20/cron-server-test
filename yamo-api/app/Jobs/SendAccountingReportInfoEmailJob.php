<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/31/18
 * Time: 11:12 AM
 */

namespace App\Jobs;

use App\Mail\EmailAccountingReportInfo;
use Illuminate\Support\Facades\Mail;

class SendAccountingReportInfoEmailJob extends Job
{
    public $tries = 10;

    protected $files;

    public function __construct($files)
    {
        $this->files = $files;
    }
    /**
     *
     */
    public function handle()
    {
        $email = new EmailAccountingReportInfo($this->files);
        Mail::to(env('MAIL_CRON_INFO_ADDRESS'))->send($email);
    }
}