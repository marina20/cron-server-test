<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 10/5/17
 * Time: 12:55 PM
 */

namespace App\Jobs;

use App\Mail\EmailCreatePassword;
use App\Models\User;
use Illuminate\Support\Facades\Mail;


class SendCreatePasswordEmailJob extends Job
{
    protected $user;
    protected $token;
    public $tries = 10;

    /**
     * SendSubscriptionConfirmationEmailJob constructor.
     * @param User $user
     */
    public function __construct($user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    /**
     *
     */
    public function handle()
    {
        $email = new EmailCreatePassword($this->user, $this->token);
        Mail::to($this->user->email)->send($email);
    }
}