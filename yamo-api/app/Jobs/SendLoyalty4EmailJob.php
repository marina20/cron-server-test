<?php

namespace App\Jobs;

use App\Mail\EmailLoyalty4;
use App\Models\Order;
use Illuminate\Support\Facades\Mail;

class SendLoyalty4EmailJob extends Job
{
    protected $order;
    public $tries = 10;

    /**
     * SendDeliveryReminderEmailJob constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     *
     */
    public function handle()
    {
        $email = new EmailLoyalty4($this->order);
        Mail::to($this->order->user->email)->send($email);
    }
}