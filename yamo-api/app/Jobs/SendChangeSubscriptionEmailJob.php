<?php
/**
 * Created by PhpStorm.
 * User: milenabasaragin
 * Date: 10/23/18
 * Time: 3:00 PM
 */

namespace App\Jobs;

use App\Mail\EmailChangeSubscription;
use Illuminate\Support\Facades\Mail;
use App\Models\Order;

class SendChangeSubscriptionEmailJob extends Job
{
    protected $order;
    public $tries = 10;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }
    public function handle()
    {
        $email = new EmailChangeSubscription($this->order);
        Mail::to($this->order->user->email)->send($email);
    }
}
