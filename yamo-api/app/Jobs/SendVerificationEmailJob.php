<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 9/28/17
 * Time: 12:00 AM
 */

namespace App\Jobs;

use App\Models\User;
use App\Mail\EmailVerification;
use Illuminate\Support\Facades\Mail;

class SendVerificationEmailJob extends Job
{
    protected $user;
    public $tries = 10;

    /**
     * SendVerificationEmailJob constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     *
     */
    public function handle()
    {
        $email = new EmailVerification($this->user);
        Mail::to($this->user->email)->send($email);
    }
}
