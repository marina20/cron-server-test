<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/3/17
 * Time: 4:19 PM
 */

namespace App\Policies;

use App\Models\Testimonial;
use App\Models\User;

class TestimonialPolicy
{
    /**
     * Determine if the given user can update posts.
     *
     * @param  \App\Models\User  $user
     * @return bool
     */
    public function update(User $user)
    {
        // As long as the user is real, allowed
        return $user->id != null;
    }
}