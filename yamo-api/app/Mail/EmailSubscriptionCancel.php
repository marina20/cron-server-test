<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 9/28/17
 * Time: 11:53 AM
 */
namespace  App\Mail;

use App\Models\Price;
use App\Models\Product;
use App\Models\Box;
use App\Models\ReferralsUsed;
use App\Traits\FormatMoney;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Subscription;
use App\Models\Translation;
use Helpers;


class EmailSubscriptionCancel extends Mailable
{
    use Queueable, SerializesModels, FormatMoney;
    protected $subscription;

    const VIEW_PATH_CH = 'email.menuplan-cancel-confirmation';

    const SUBJECT = 'APPMAIL.EMAILSUBSCRIPTIONCANCEL.SUBJECT';


    public function __construct(Subscription $subscription)
    {
        $this->subscription = $subscription;
    }

    public function build()
    {
        // we must set locale according to order country as email will be sent out of the system without logged in user
        Helpers::set_locale($this->subscription->order->country->iso_alpha_2);
        $view_path = self::VIEW_PATH_CH;
        //$singleDefaultPackage = Product::where('slug',Product::PRODUCT_CATEGORY_SINGLE_BOX_CUSTOM)->first();
        //$singleDefaultPackagePrice = Price::where(['product_id' => $singleDefaultPackage->id])->where(['country_id' => $this->subscription->order->country->id])->first();

        $order_items = $this->subscription->order->items->sortBy('position');
        if(!empty($this->subscription->order->custom_box_id))
        {
            $order_items->map(function($item) {
                if(false !== strpos($item->product->category,Product::PRODUCT_CATEGORY_CUSTOM))
                {
                    $custom_box = Box::find($this->subscription->order->custom_box_id);
                    $product_ingredients_override = [];
                    foreach ($custom_box->items as $box_item)
                    {
                        $product_ingredients_override[] = $box_item->quantity . ' x ' . implode(', ',$box_item->product->ingredients);
                    }
                    $item->product->boxes->push($custom_box);
                    $item->product->ingredients = $product_ingredients_override;
                }
            });
        }
        $box_tax_percentage = $this->subscription->order->getDefaultTaxPercentage();
        $wallet_discount = ($this->formatMoney($this->subscription->order->wallet_discount))." ".$this->subscription->order->currency;
        $ref = ReferralsUsed::where(['order_id' => $this->subscription->order->id])->first();
        $ref_discount = 0;
        if(!empty($ref)){
            $ref_discount = $ref['amount_referree'];
        }
        $referral_discount = ($this->formatMoney($ref_discount))." ".$this->subscription->order->currency;

        $subscription_reduction = ($this->formatMoney($this->subscription->order->system_discount))." ".$this->subscription->order->currency;
        $subtotal = ($this->formatMoney($this->subscription->order->order_subtotal))." ".$this->subscription->order->currency;
        $first_subtotal = ($this->formatMoney($this->subscription->order->order_subtotal))." ".$this->subscription->order->currency;
        $total = ($this->formatMoney($this->subscription->order->order_total))." ".$this->subscription->order->currency;
        $total_tax = ($this->formatMoney($this->subscription->order->order_tax))." ".$this->subscription->order->currency;
        $discount = ($this->formatMoney($this->subscription->order->public_discount))." ".$this->subscription->order->currency;
        $shipping = ($this->formatMoney($this->subscription->order->shipping))." ".$this->subscription->order->currency;
        $loyalty = ($this->formatMoney($this->subscription->order->loyalty_discount))." ".$this->subscription->order->currency;
        if($this->subscription->order->currency=="CHF"){
            $referral_discount = $this->subscription->order->currency." ".($this->formatMoney($ref_discount));
            $wallet_discount = $this->subscription->order->currency." ".($this->formatMoney($this->subscription->order->wallet_discount));
            $subscription_reduction = $this->subscription->order->currency." ".$this->formatMoney($this->subscription->order->system_discount);
            $total = $this->subscription->order->currency." ".($this->formatMoney($this->subscription->order->order_total));
            $subtotal = $this->subscription->order->currency." ".($this->formatMoney($this->subscription->order->order_subtotal));
            $first_subtotal = $this->subscription->order->currency." ".($this->formatMoney($this->subscription->order->order_subtotal));
            $total_tax = $this->subscription->order->currency." ".($this->formatMoney($this->subscription->order->order_tax));
            $discount = $this->subscription->order->currency." ".($this->formatMoney($this->subscription->order->public_discount));
            $shipping = $this->subscription->order->currency." ".($this->formatMoney($this->subscription->order->shipping));
            $loyalty = $this->subscription->order->currency." ".($this->formatMoney($this->subscription->order->loyalty_discount));
        }
        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->bcc(explode(',', env('MAIL_BCC_CANCELLATION_RECIPIENTS')))
            ->subject(Translation::trans(self::SUBJECT))
            ->view($view_path)
            ->with([
                'name' => $this->subscription->user->profile->first_name,
                'id' => $this->subscription->id,
                'order_id' => $this->subscription->order->id,
                'user_id' =>$this->subscription->user->id,
                'customer_number' =>$this->subscription->user->profile->customer_number,
                'date_created' => $this->subscription->created_at->format('d.m.Y'),
                'delivery_date' => $this->subscription->order->delivery_date->format('d.m.Y'),
                'payment_method' => $this->subscription->order->payment_method_title,
                'img_uri' => env('S3_BASE'),
                'order_items' => $order_items,
                'subscription_reduction' => $subscription_reduction,
                'currency' => $this->subscription->order->currency,
                'referral' => $referral_discount,
                'referral_cost' => $this->formatMoney($ref_discount),
                'wallet' => $wallet_discount,
                'wallet_cost' => $this->formatMoney($this->subscription->order->wallet_discount),
                'discount' => $discount,
                'discount_cost' => $this->formatMoney($this->subscription->order->public_discount),
                'first_subtotal' => $first_subtotal,
                'subtotal' => $subtotal,
                'subtotal_tax' => $this->formatMoney($this->subscription->order->order_subtotal_tax),
                'shipping' => $shipping,
                'shipping_cost' => $this->formatMoney($this->subscription->order->shipping),
                'shipping_tax' => $this->formatMoney($this->subscription->order->shipping_tax),
                'total' => $total,
                'total_tax' => $total_tax,
                'shipping_name' => $this->subscription->order->shipping_first_name . ' ' . $this->subscription->order->shipping_last_name,
                'shipping_address' => $this->subscription->order->getShippingStreet(),
                'shipping_postnumber' => $this->subscription->order->shipping_postcode,
                'shipping_city' => $this->subscription->order->shipping_city,
                'shipping_country' => $this->subscription->order->shipping_country,
                'shipping_email' => $this->subscription->order->shipping_email,
                'shipping_phone' => $this->subscription->order->shipping_phone,
                'billing_name' => $this->subscription->order->billing_first_name . ' ' . $this->subscription->order->billing_last_name,
                'billing_address' => $this->subscription->order->getBillingStreet(),
                'billing_postnumber' => $this->subscription->order->billing_postcode,
                'billing_city' => $this->subscription->order->billing_city,
                'billing_country' => $this->subscription->order->billing_country,
                'billing_email' => $this->subscription->order->billing_email,
                'billing_phone' => $this->subscription->order->billing_phone,
                'app_url' => Helpers::app_url(),
                'shipping_company' => $this->subscription->order->shipping_company,
                'billing_company' => $this->subscription->order->billing_company,
                'hello_email' => Helpers::default_email(),
                'clean_url' => Helpers::website_url(),
                'vat_number' => Helpers::vat_number(),
                'box_tax_percentage' => $box_tax_percentage,
                'coupon_code' => $this->subscription->order->coupon_code,
                'loyalty' => $loyalty,
                'loyalty_cost' => $this->formatMoney($this->subscription->order->loyalty_discount)
            ]);
    }
}