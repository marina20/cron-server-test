<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/31/18
 * Time: 11:14 AM
 */

namespace App\Mail;

use App\Models\Translation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailExcelTest extends Mailable
{
    use Queueable, SerializesModels;

    public function build()
    {
        $content = 'Email testing excel attachment in Yamo app';
        $excel = app('excel');
        $excel_file = $excel->create('test', function($excel) {
            $excel->sheet('Excel sheet', function($sheet) {

                $sheet->setOrientation('landscape');

            });
        })->store("xlsx",false,true)['full'];
        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject(Translation::trans('APPMAIL.EMAILEXCELTEST.SUBJECT'))
            ->view('email.email-template')
            ->with(['content'=>$content])
            ->attach($excel_file,
                [
                    'as' => 'test.xlsx',
                    'mime' => 'application/vnd.ms-excel',
            ]);
    }
}