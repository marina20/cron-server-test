<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/31/18
 * Time: 11:14 AM
 */

namespace App\Mail;

use Illuminate\Support\Carbon;
use Illuminate\Bus\Queueable;
use App\Models\Translation;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailRecurringPayment extends Mailable
{
    use Queueable, SerializesModels;

    protected $files;
    public function __construct($files)
    {
        $this->files = $files;
    }

    public function build()
    {
        $content = Translation::trans('APPMAIL.EMAILRECURRINGPAYMENT.SUBJECT').': '. Carbon::tomorrow()->toDateString();

        $email= $this->from(env('MAIL_FROM_CRON_ADDRESS'), env('MAIL_FROM_CRON_NAME'))
            ->subject($content)
            ->view('email.email-template')
            ->with(['content'=>$content]);

        $email->attach($this->files,
            [
                'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            ]);

        return $email;
    }
}