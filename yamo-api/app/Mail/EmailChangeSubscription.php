<?php
/**
 * Created by PhpStorm.
 * User: milenabasaragin
 * Date: 10/23/18
 * Time: 3:52 PM
 */


namespace App\Mail;

use App\Models\Box;
use App\Models\Order;
use App\Models\Product;
use App\Models\ReferralsUsed;
use App\Models\Translation;
use App\Traits\FormatMoney;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Helpers;

class EmailChangeSubscription extends Mailable
{
    use Queueable, SerializesModels, FormatMoney;
    protected $order;

    const SUBJECT = 'APPMAIL.EMAILCHANGESUBSCRIPTION.SUBJECT';

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function build()
    {
        Helpers::set_locale($this->order->getCountryCode($this->order->shipping_country));
        $view_path = 'email.subscription-change';

        $order_items = $this->order->items->sortBy('position');
        if(!empty($this->order->custom_box_id))
        {
            $order_items->map(function($item) {
                if(false !== strpos($item->product->category,Product::PRODUCT_CATEGORY_CUSTOM))
                {
                    $custom_box = Box::find($this->order->custom_box_id);
                    $product_ingredients_override = [];
                    foreach ($custom_box->items as $box_item)
                    {
                        $product_ingredients_override[] = $box_item->quantity . ' x ' . implode(', ',$box_item->product->ingredients);
                    }
                    $item->product->boxes->push($custom_box);
                    $item->product->ingredients = $product_ingredients_override;
                }
            });
        }
        $box_tax_percentage = $this->order->getDefaultTaxPercentage();
        $subtotal = ($this->formatMoney($this->order->order_subtotal))." ".$this->order->currency;
        $total_tax = ($this->formatMoney($this->order->order_tax))." ".$this->order->currency;
        $discount = ($this->formatMoney($this->order->public_discount))." ".$this->order->currency;
        $wallet_discount = ($this->formatMoney($this->order->wallet_discount))." ".$this->order->currency;
        $ref = ReferralsUsed::where(['order_id' => $this->order->id])->first();
        $ref_discount = 0;
        $subscription_reduction = 0;
        if(!empty($this->order->is_subscription)){
            $subscription_reduction = ($this->formatMoney($this->order->system_discount))." ".$this->order->currency;
        }
        if(!empty($ref)){
            $ref_discount = $ref['amount_referree'];
        }
        $referral_discount = ($this->formatMoney($ref_discount))." ".$this->order->currency;
        $shipping = ($this->formatMoney($this->order->shipping))." ".$this->order->currency;
        $delivery_frequency = $this->order->find_subscription($this->order)->delivery_frequency;
        $total = ($this->formatMoney($this->order->order_total))." ".$this->order->currency;
        $loyalty = ($this->formatMoney($this->order->loyalty_discount))." ".$this->order->currency;
        if($this->order->currency=="CHF"){
            if(!empty($this->order->is_subscription)){
                $subscription_reduction = $this->order->currency." ".($this->formatMoney($this->order->system_discount));
            }
            $referral_discount = $this->order->currency." ".($this->formatMoney($ref_discount));
            $total = $this->order->currency." ".($this->formatMoney($this->order->order_total));
            $subtotal = $this->order->currency." ".($this->formatMoney($this->order->order_subtotal));
            $total_tax = $this->order->currency." ".($this->formatMoney($this->order->order_tax));
            $discount = $this->order->currency." ".($this->formatMoney($this->order->public_discount));
            $wallet_discount = $this->order->currency." ".($this->formatMoney($this->order->wallet_discount));
            $shipping = $this->order->currency." ".($this->formatMoney($this->order->shipping));
            $loyalty = $this->order->currency." ".($this->formatMoney($this->order->loyalty_discount));
        }
        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject(Translation::trans(self::SUBJECT))
            ->view($view_path)
            ->with([
                'cut_off_date' => $this->order->delivery_date->copy()->subDays(Order::ORDER_SUBSCRIPTION_CANCELLATION_PERIOD)->format('d.m.Y'),
                'first_name' => trim($this->order->user->profile->first_name),
                'order_id' => $this->order->id,
                'currency' => $this->order->currency,
                'referral' => $referral_discount,
                'subscription_reduction' => $subscription_reduction,
                'referral_cost' => $this->formatMoney($ref_discount),
                'wallet' => $wallet_discount,
                'wallet_cost' => $this->formatMoney($this->order->wallet_discount),
                'discount' => $discount,
                'discount_cost' => $this->formatMoney($this->order->public_discount),
                'first_subtotal' => $subtotal,
                'subtotal' => $subtotal,
                'subtotal_tax' => $this->formatMoney($this->order->order_subtotal_tax),
                'shipping_cost' => $this->formatMoney($this->order->shipping),
                'shipping' => $shipping,
                'shipping_tax' => $this->formatMoney($this->order->shipping_tax),
                'total' => $total,
                'total_tax' => $total_tax,
                'coupon_code' => $this->order->coupon_code,
                'box_tax_percentage' => $box_tax_percentage,
                'shipping_name' => $this->order->shipping_first_name . ' ' . $this->order->shipping_last_name,
                'shipping_address' => $this->order->getShippingStreet(),
                'shipping_postnumber' => $this->order->shipping_postcode,
                'shipping_city' => $this->order->shipping_city,
                'shipping_country' => $this->order->shipping_country,
                'shipping_email' => $this->order->shipping_email,
                'shipping_phone' => $this->order->shipping_phone,
                'billing_name' => $this->order->billing_first_name . ' ' . $this->order->billing_last_name,
                'billing_address' => $this->order->getBillingStreet(),
                'billing_postnumber' => $this->order->billing_postcode,
                'billing_city' => $this->order->billing_city,
                'billing_country' => $this->order->billing_country,
                'billing_email' => $this->order->billing_email,
                'billing_phone' => $this->order->billing_phone,
                'box_tax_percentage' => $box_tax_percentage,
                'customer_number' => $this->order->user->profile->customer_number,
                'date_created' => $this->order->created_at->format('d.m.Y'),
                'delivery_date' => $this->order->delivery_date->format('d.m.Y'),
                'payment_method' => $this->order->payment_method_title,
                'order_items' => $order_items,
                'currency' => $this->order->currency,
                'total' => $total,
                'total_tax' => $this->formatMoney($this->order->order_tax),
                'delivery_frequency' => trans_choice('email.change-subscription.weeks', $delivery_frequency, ['value' => $delivery_frequency]),
                'app_url' => Helpers::app_url(),
                'loyalty' => $loyalty,
                'loyalty_cost' => $this->formatMoney($this->order->loyalty_discount)
            ]);
    }
}