<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 9/28/17
 * Time: 11:53 AM
 */
namespace  App\Mail;

use App\Models\Price;
use App\Models\Product;
use App\Models\Box;
use App\Models\ReferralsUsed;
use App\Traits\FormatMoney;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Order;
use App\Models\Translation;
use Helpers;


class EmailSingleBoxConfirmation extends Mailable
{
    use Queueable, SerializesModels, FormatMoney;
    protected $order;

    const VIEW_PATH_CH = 'email.single-box-confirmation';


    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function build()
    {
        $order_items = $this->order->items->sortBy('position');
        if(!empty($this->order->custom_box_id))
        {
            $order_items->map(function($item) {
                // Temporary disable this condition as it restrict order-details in single-order
               // if(false !== strpos($item->product->category,Product::PRODUCT_CATEGORY_CUSTOM))
               // {
                    $custom_box = Box::find($this->order->custom_box_id);
                    $product_ingredients_override = [];
                    foreach ($custom_box->items as $box_item)
                    {
                        $product_ingredients_override[] = $box_item->quantity . ' x ' . implode(', ',$box_item->product->ingredients);
                    }
                    $item->product->boxes->push($custom_box);
                    $item->product->ingredients = $product_ingredients_override;
               // }
            });
        }



        Helpers::set_locale($this->order->country->iso_alpha_2);

        $view_path = self::VIEW_PATH_CH;

        $box_tax_percentage = $this->order->getDefaultTaxPercentage();
        $box_name = Order::ORDER_BOX_PRODUCT_NAME;
        $box_price = $this->order->order_total;

        $subtotal = ($this->formatMoney($this->order->order_subtotal))." ".$this->order->currency;
        $total = ($this->formatMoney($this->order->order_total))." ".$this->order->currency;
        $total_tax = ($this->formatMoney($this->order->order_tax))." ".$this->order->currency;
        $discount = ($this->formatMoney($this->order->public_discount))." ".$this->order->currency;
        $wallet_discount = ($this->formatMoney($this->order->wallet_discount))." ".$this->order->currency;
        $ref = ReferralsUsed::where(['order_id' => $this->order->id])->first();
        $ref_discount = 0;
        if(!empty($ref)){
            $ref_discount = $ref['amount_referree'];
        }
        $referral_discount = ($this->formatMoney($ref_discount))." ".$this->order->currency;
        $shipping = ($this->formatMoney($this->order->shipping))." ".$this->order->currency;
        $loyalty = ($this->formatMoney($this->order->loyalty_discount))." ".$this->order->currency;
        if($this->order->currency=="CHF"){
            $referral_discount = $this->order->currency." ".($this->formatMoney($ref_discount));
            $total = $this->order->currency." ".($this->formatMoney($this->order->order_total));
            $subtotal = $this->order->currency." ".($this->formatMoney($this->order->order_subtotal));
            $total_tax = $this->order->currency." ".($this->formatMoney($this->order->order_tax));
            $discount = $this->order->currency." ".($this->formatMoney($this->order->public_discount));
            $wallet_discount = $this->order->currency." ".($this->formatMoney($this->order->wallet_discount));
            $shipping = $this->order->currency." ".($this->formatMoney($this->order->shipping));
            $loyalty = $this->order->currency." ".($this->formatMoney($this->order->loyalty_discount));
        }
        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->bcc(explode(',', env('MAIL_BCC_ORDERS_RECIPIENTS')))
            ->subject(Translation::trans('APPMAIL.EMAILSINGLEBOXCONFIRM.SUBJECT'))
            ->view($view_path)
            ->with([
                'name' => $this->order->user->profile->first_name,
                'id' => $this->order->id,
                'order_id' => $this->order->id,
                'user_id' =>$this->order->user->id,
                'customer_number' =>$this->order->user->profile->customer_number,
                'date_created' => $this->order->created_at->format('d.m.Y'),
                'delivery_date' => $this->order->delivery_date->format('d.m.Y'),
                'payment_method' => $this->order->payment_method_title,
                'img_uri' => env('S3_BASE'),
                'order_items' => $order_items,
                'currency' => $this->order->currency,
                'referral' => $referral_discount,
                'referral_cost' => $this->formatMoney($ref_discount),
                'wallet' => $wallet_discount,
                'wallet_cost' => $this->formatMoney($this->order->wallet_discount),
                'discount' => $discount,
                'discount_cost' => $this->formatMoney($this->order->public_discount),
                'first_subtotal' => $subtotal,
                'subtotal' => $subtotal,
                'subtotal_tax' => $this->formatMoney($this->order->order_subtotal_tax),
                'shipping_cost' => $this->formatMoney($this->order->shipping),
                'shipping' => $shipping,
                'shipping_tax' => $this->formatMoney($this->order->shipping_tax),
                'total' => $total,
                'total_tax' => $total_tax,
                'shipping_name' => $this->order->shipping_first_name . ' ' . $this->order->shipping_last_name,
                'shipping_address' => $this->order->getShippingStreet(),
                'shipping_postnumber' => $this->order->shipping_postcode,
                'shipping_city' => $this->order->shipping_city,
                'shipping_country' => $this->order->shipping_country,
                'shipping_email' => $this->order->shipping_email,
                'shipping_phone' => $this->order->shipping_phone,
                'billing_name' => $this->order->billing_first_name . ' ' . $this->order->billing_last_name,
                'billing_address' => $this->order->getBillingStreet(),
                'billing_postnumber' => $this->order->billing_postcode,
                'billing_city' => $this->order->billing_city,
                'billing_country' => $this->order->billing_country,
                'billing_email' => $this->order->billing_email,
                'billing_phone' => $this->order->billing_phone,
                'app_url' => Helpers::app_url(),
                'shipping_company' => $this->order->shipping_company,
                'billing_company' => $this->order->billing_company,
                'hello_email' => Helpers::default_email(),
                'clean_url' => Helpers::website_url(),
                'vat_number' => Helpers::vat_number(),
                'shipping_tax_percentage' => $this->order->country->shippings->first()->tax_percentage,
                'box_tax_percentage' => $box_tax_percentage,
                'coupon_code' => $this->order->coupon_code,
                'box_name' => $box_name,
                'box_price' => $box_price,
                'loyalty' => $loyalty,
                'loyalty_cost' => $this->formatMoney($this->order->loyalty_discount)
            ]);
    }
}