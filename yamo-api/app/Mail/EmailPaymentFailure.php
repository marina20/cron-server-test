<?php
/**
 * Created by PhpStorm.
 * User: juanse254
 * Date: 9/26/18
 * Time: 5:22 PM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Order;
use App\Models\Translation;
use Helpers;

class EmailPaymentFailure extends Mailable
{
	use Queueable, SerializesModels;
	protected $order;

	public function __construct(Order $order)
	{
		$this->order = $order;
	}

	public function build()
	{
        Helpers::set_locale($this->order->country->iso_alpha_2);

        $view_path = 'email.payment-failure';


		return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
			->bcc(explode(',',env('MAIL_BCC_CANCELLATION_RECIPIENTS')))
			->subject(Translation::trans("APPMAIL.EMAILPAYMENTFAILURE.SUBJECT"))
			->view($view_path)
			->with([
				'delivery_date' => $this->order->delivery_date->format('d.m.Y'),
				'name' => $this->order->user->profile->first_name,
                'app_url' => Helpers::app_url()
			]);
	}

}