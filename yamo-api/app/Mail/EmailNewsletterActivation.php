<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 9/28/17
 * Time: 11:53 AM
 */
namespace  App\Mail;

use App\Models\Translation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Helpers;


class EmailNewsletterActivation extends Mailable
{
    use Queueable, SerializesModels;
    protected $confirmation_code;

    const SUBJECT = 'APPMAIL.EMAILNEWSLETTERACTIVATION.SUBJECT';

    public function __construct($confirmation_code)
    {
        $this->confirmation_code = $confirmation_code;
    }

    public function build()
    {
        $view_path = 'email.newsletter-activation';

        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->bcc(explode(',',env('MAIL_BCC_DEFAULT_RECIPIENTS')))
            ->subject(Translation::trans(self::SUBJECT))
            ->view($view_path)
            ->with([
                'app_url' => Helpers::app_url(),
                'confirmation_code' => $this->confirmation_code,
                'hello_email' => Helpers::default_email(),
                'clean_url' => Helpers::website_url(),
                'vat_number' => Helpers::vat_number()
            ]);
    }
}