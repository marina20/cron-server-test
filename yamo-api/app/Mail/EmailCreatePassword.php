<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/31/18
 * Time: 11:14 AM
 */

namespace App\Mail;

use Helpers;
use Illuminate\Support\Carbon;
use App\Models\Translation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailCreatePassword extends Mailable
{
    use Queueable, SerializesModels;

    protected $token;

    protected $user;
    public function __construct($user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    public function build()
    {

        if($this->user->from_wizard)
        {
            $subject = Translation::trans("APPMAIL.EMAILCREATEPASSWORD.SUBJECT");

            $view_path = 'email.shop-set-your-password';
            $email= $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
                ->bcc(explode(',', env('MAIL_BCC_DEFAULT_RECIPIENTS')))
                ->subject($subject)
                ->view($view_path)
                ->with([
                    'content'=>$subject,
                    'name' => $this->user->name,
                    'reset_password_uri' => Helpers::reset_password_url(),
                    'token' => $this->token,
                ]);

            return $email;
        }
        else
        {
            $subject = Translation::trans("APPMAIL.EMAILCREATEPASSWORD.SUBJECT");
            $view_path = 'email.change-password';
            return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
                ->bcc(explode(',',env('MAIL_BCC_DEFAULT_RECIPIENTS')))
                ->subject($subject)
                ->view($view_path)
                ->with([
                    'app_url' => Helpers::app_url(),
                    'reset_password_uri' => Helpers::reset_password_url(),
                    'name' => $this->user->name,
                    'email' => $this->user->email,
                    'token' => $this->token,
                    'hello_email' => Helpers::default_email(),
                    'clean_url' => Helpers::website_url(),
                    'vat_number' => Helpers::vat_number()
                ]);
        }
    }
}