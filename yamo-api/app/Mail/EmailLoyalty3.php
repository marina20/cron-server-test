<?php

namespace App\Mail;

use App\Models\Country;
use App\Models\Region;
use App\Traits\FormatMoney;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Order;
use App\Models\Translation;
use Helpers;

class EmailLoyalty3 extends Mailable
{
	use Queueable, SerializesModels;
	use FormatMoney;
	protected $order;

	public function __construct(Order $order)
	{
		$this->order = $order;
	}

	public function build()
	{
        Helpers::set_locale($this->order->country->iso_alpha_2);
        $region = Region::where(['country_id' => $this->order->country->id])->first();
        $referralDiscount = ($this->formatMoney($region->referrer_reward_cents))." ".$this->order->currency;
        if($this->order->country->iso_alpha_2==Country::COUNTRY_SWITZERLAND){
            $referralDiscount = $this->order->currency." ".($this->formatMoney($region->referrer_reward_cents));
        }
		return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
			->bcc(explode(',',env('MAIL_BCC_CANCELLATION_RECIPIENTS')))
			->subject(Translation::trans("MAIL.LOYALTY.13.3.SUBJECT"))
			->view('email.loyalty.Loyalty3Programme')
			->with([
				'firstName' => $this->order->user->profile->first_name,
                'referralDiscount' => $referralDiscount
			]);
	}

}