<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/31/18
 * Time: 11:14 AM
 */

namespace App\Mail;

use App\Models\ReferralUrl;
use Helpers;
use Illuminate\Support\Carbon;
use App\Models\Translation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailRefereeCompleted extends Mailable
{
    use Queueable, SerializesModels;

    protected $token;

    protected $user;
    protected $referralUrl;
    protected $order;
    public function __construct($referralUrl,$order)
    {
        $this->referralUrl = $referralUrl;
        $this->user = $referralUrl->user;
        $this->order = $order;
    }

    public function build()
    {
        $currency = $this->order->currency;
        if(empty($currency)){
            $currency = $this->order->country->currency;
        }
        $priceString = $currency . " " . ($this->order->region->referrer_reward_cents).".-";
        if($this->order->country->iso_alpha_2 !== "CH"){
            $priceString = ($this->order->region->referrer_reward_cents).$currency;
        }
            $subject = Translation::trans("MAIL.REFEREE-COMPLETED.SUBJECT");
            $view_path = 'email.referee-completed';
            $email= $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
                ->bcc(explode(',', env('MAIL_BCC_DEFAULT_RECIPIENTS')))
                ->subject($subject)
                ->view($view_path)
                ->with([
                    'price' => $priceString,
                    'name' => $this->user->profile->first_name,
                    'referralUrl' => $this->referralUrl
                ]);
            return $email;
    }
}