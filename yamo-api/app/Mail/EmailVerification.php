<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 9/28/17
 * Time: 11:53 AM
 */
namespace  App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;
use App\Models\Translation;
use Helpers;


class EmailVerification extends Mailable
{
    use Queueable, SerializesModels;
    protected $user;

    const SUBJECT = 'APPMAIL.EMAILVERIFICATION.SUBJECT';


    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function build()
    {
        $view_path = 'email.welcome';

        $name = $this->user->name;
        if(!empty($this->user->profile)) {
            $name = $this->user->profile->first_name;
        }
        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->bcc(explode(',',env('MAIL_BCC_DEFAULT_RECIPIENTS')))
            ->subject(Translation::trans(self::SUBJECT))
            ->view($view_path)
            ->with([
                'app_url' => Helpers::app_url(),
                'name' => $name,
                'email' => $this->user->email,
                'hello_email' => Helpers::default_email(),
                'clean_url' => Helpers::website_url(),
                'vat_number' => Helpers::vat_number()
            ]);
    }
}