<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 10/5/17
 * Time: 12:46 PM
 */


namespace  App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;
use App\Models\Translation;
use Helpers;


class EmailResetPassword extends Mailable
{
    use Queueable, SerializesModels;
    protected $user;
    protected $token;

    const VIEW_PATH_CH = 'email.change-password';

    public function __construct(User $user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    public function build()
    {
        $subject = Translation::trans('APPMAIL.EMAILRESETPASSWORD.SUBJECT');
        $view_path = 'email.change-password';

        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->bcc(explode(',',env('MAIL_BCC_DEFAULT_RECIPIENTS')))
            ->subject($subject)
            ->view($view_path)
            ->with([
                'app_url' => Helpers::app_url(),
                'reset_password_uri' => Helpers::reset_password_url(),
                'name' => $this->user->name,
                'email' => $this->user->email,
                'token' => $this->token,
                'hello_email' => Helpers::default_email(),
                'clean_url' => Helpers::website_url(),
                'vat_number' => Helpers::vat_number()
            ]);
    }
}