<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 1/31/18
 * Time: 11:14 AM
 */

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Translation;

class EmailOrderReportGermany extends Mailable
{
    use Queueable, SerializesModels;

    protected $files;
    public function __construct($files)
    {
        $this->files = $files;
    }
    public function build()
    {
        $content = Translation::trans('APPMAIL.EMAILORDERREPORTSGERMANY.SUBJECT');
        $email =  $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->subject('Yamo orders for Prowito')
            ->view('email.email-template')
            ->with(['content'=>$content]);

        foreach($this->files as $file)
        {
            $email->attach($file,
                [
                    'mime' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                ]);
        }

        return $email;
    }
}