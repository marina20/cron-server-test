<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BundleMedia extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bundles_media';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['media_id','bundle_id'];
    protected $with = ['media'];
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function media()
    {
        return $this->belongsTo('App\Models\Media');
    }
}