<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LoyaltyTracker extends Model
{
    const LOYALTY_TRACKER_STATUS_PENDING = 'pending';
    const LOYALTY_TRACKER_STATUS_CONFIRMED = 'confirmed';
    const LOYALTY_TRACKER_STATUS_ROLLBACK = 'rollback';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id','box_number','order_id','voucher_id'];

    /**
     * Voucher relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function voucher()
    {
        return $this->belongsTo('App\Models\Voucher', 'voucher_id', 'id');
    }

    /**
     * User relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    /**
     * Order relation
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id', 'id');
    }

    /**
     * VoucherRedemption
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function voucherRedemption()
    {
        return $this->belongsTo('App\Models\VoucherRedemption', 'redemption_id', 'id');
    }
}