<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Vouchercategory extends Model
{

    protected $table = 'vouchercategories';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

}
