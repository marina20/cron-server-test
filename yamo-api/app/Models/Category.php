<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/7/17
 * Time: 4:37 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Helpers;

class Category extends Model
{
    const CATEGORY_FOOD_NAME_KEY = 'food';
    const CATEGORY_BUNDLES_NAME_KEY = 'bundles';
    const CATEGORY_ALL_NAME_KEY = 'all';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'categories';

    protected $appends = ['name','description', 'image', 'image_thumbnail',
        'image_alt_tag_key', 'image_alt_tag_text'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name_key', 'category_id', 'description', 'new', 'position'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    /**
     * Get parent category
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return Translation::trans('CATEGORIES.'.strtoupper($this->name_key));
    }

    /**
     * @return string
     */
    public function getImageAttribute()
    {
        return Helpers::getImage(Translation::trans('CATEGORIES.'.strtoupper($this->name_key).'.IMG'));
    }

    /**
     * @return string
     */
    public function getImageThumbnailAttribute()
    {
        return Helpers::getImageThumbnail(Translation::trans('CATEGORIES.'.strtoupper($this->name_key).'.IMG-THUMB'));
    }

    /**
     * @return string|null
     */
    public function getImageAltTagKeyAttribute()
    {
        return 'CATEGORIES.'.strtoupper($this->name_key).'.ALT-TAGS';
    }

    /**
     * @return string|null
     */
    public function getImageAltTagTextAttribute()
    {
        return Translation::trans('CATEGORIES.'.strtoupper($this->name_key).'.ALT-TAGS');
    }

    /**
     * @return string
     */
    public function getDescriptionAttribute()
    {
        return Translation::trans('CATEGORIES.'.strtoupper($this->name_key).'.DESCRIPTION');
    }
}