<?php


namespace App\Models;

use App\Traits\FormatMoney;
use Illuminate\Database\Eloquent\Model;
use Helpers;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;

class Bundle extends Model
{
    use FormatMoney;
    const TRANSLATION_PREFIX = 'BUNDLES';
    const TRANSLATION_PREFIX_MEDIA = 'MEDIA';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bundles';

    protected $with = ['box','media','regions'];
    protected $appends = ['name','currency', 'description', 'image', 'image_thumbnail',
        'image_alt_tag_key', 'image_alt_tag_text', 'product_name_color', 'product_gradient_color'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['box_id','name_key','voucher_id','position'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('active', function (Builder $builder) {
            try {
                    $builder->whereHas('bundleRegions', function ($query) {
                        $query->where('active', 1)
                            ->whereHas('regions', function ($query) {
                                $query->where('country_id', Helpers::country()->id);
                            });
                    });
            } catch (\Throwable $ex)
            {
                Log::info($ex);
                app('sentry')->captureException($ex);
                throw $ex;
            }
        });
    }

    /**
     * Get box attached to bundle
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function box()
    {
        return $this->belongsTo("App\Models\Box","box_id");
    }

    public function price(){
        $box = Box::find($this->box_id);
        $totalPrice = 0;
        foreach($box->items as $item){
            $totalPrice += ($item->product->price * $item->quantity);
        }
        return $this->formatMoney($totalPrice);
    }

    /**
     * Get voucher
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function voucher()
    {
        return $this->belongsTo("App\Models\Voucher","voucher_id");
    }

    public static function getAllVisible(){
        $countryId = Country::where('content_code',app('translator')->getLocale())->first()->id;
        return Bundle::whereHas('regions',function ($query) use($countryId) {
            $query->where('country_id', $countryId)->where('visible', 1);
        })->get();
    }
    /**
     * Get media for the bundle
     */
    public function media()
    {
        return $this->belongsToMany('App\Models\Media','bundles_media');
    }

    /**
     * Get bundle regions for the bundle.
     */
    public function bundleRegions()
    {
        return $this->hasMany('App\Models\BundleRegion','bundle_id');
    }

    /**
     * Get the regions for the product.
     */
    public function regions()
    {
        return $this->belongsToMany('App\Models\Region','bundles_regions', 'bundle_id', 'region_id');
    }

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return Translation::trans(self::TRANSLATION_PREFIX . '.'.$this->name_key);
    }

    public function getCurrencyAttribute()
    {
        $country = Helpers::country();
        if(empty($country))
            return null;

        return $country->currency;
    }
    /**
     * @return string
     */
    public function getImageAttribute()
    {
        if(empty($this->media->first()))
            return Helpers::getImage(env('DEFAULT_PRODUCT_IMAGE'));
        return Helpers::getImage(Translation::trans(self::TRANSLATION_PREFIX_MEDIA . '.'.$this->media->first()->name_key.'.IMG'));
    }

    /**
     * @return string
     */
    public function getImageThumbnailAttribute()
    {
        if(empty($this->media->first()))
            return Helpers::getImageThumbnail(env('DEFAULT_PRODUCT_IMAGE'));
        return Helpers::getImageThumbnail(Translation::trans(self::TRANSLATION_PREFIX_MEDIA . '.'.$this->media->first()->name_key.'.IMG-THUMB'));
    }

    /**
     * @return string|null
     */
    public function getImageAltTagKeyAttribute()
    {
        if(empty($this->media->first()))
            return null;
        return self::TRANSLATION_PREFIX_MEDIA . '.'.Translation::trans($this->media->first()->name_key).'.ALT-TAGS';
    }

    /**
     * @return string|null
     */
    public function getImageAltTagTextAttribute()
    {
        if(empty($this->media->first()))
            return null;
        return Translation::trans(self::TRANSLATION_PREFIX_MEDIA . '.'.$this->media->first()->name_key.'.ALT-TAGS');
    }

    /** Bundle name color attribute
     * @return string
     */
    public function getProductNameColorAttribute()
    {
        return Translation::trans(self::TRANSLATION_PREFIX . '.'.$this->name_key.'.PRODUCT-NAME-COLOR');
    }

    /** Bundle gradient color array attribute
     * @return array
     */
    public function getProductGradientColorAttribute()
    {
        $colorOneKeyKey = self::TRANSLATION_PREFIX . ".".$this->name_key.".PRODUCT-GRADIENT-COLOR-ONE";
        $colorTwoKeyKey = self::TRANSLATION_PREFIX . ".".$this->name_key.".PRODUCT-GRADIENT-COLOR-TWO";
        return [
            'first' => (Translation::trans($colorOneKeyKey) === $colorOneKeyKey) ? null : Translation::trans($colorOneKeyKey),
            'second' => (Translation::trans($colorTwoKeyKey) === $colorTwoKeyKey) ? null : Translation::trans($colorTwoKeyKey)
        ];
    }

    /**
     * @return string
     */
    public function getDescriptionAttribute()
    {
        return Translation::trans(self::TRANSLATION_PREFIX . '.'.$this->name_key.'.DESCRIPTION');
    }
}