<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Order extends Model
{
    const DEFAULT_LINE_ITEM_TYPE = 'line_item';
    const CREATED_VIA_SUBSCRIPTION = 'subscription';
    const CREATED_VIA_SINGLE_BOX = 'single-box';
    const CREATED_VIA_WP_REORDER = 'wp_reorder';
    const PAYMENT_METHOD_CREDITCARD = 'creditcard';
    const PAYMENT_METHOD_PAYPAL = 'paypal';
    const PAYMENT_METHOD_FREE = 'free';
    const PAYMENT_METHOD_INVOICE = 'invoice';
    const PAYMENT_METHOD_SOFORT = 'sofort';
    const PAYMENT_METHOD_CREDITCARD_DE = 'Kreditkarte';
    const PAYMENT_METHOD_PAYPAL_DE = 'Paypal';
    const PAYMENT_METHOD_INVOICE_DE = 'Rechnung';
    const PAYMENT_METHOD_SOFORT_DE = 'Sofortüberweisung';
    const DEFAULT_DELIVERY_FREQUENCY = 2;
    const ORDER_FIRSTBOX_CANCELLATION_PERIOD = 7;
    const ORDER_SUBSCRIPTION_CANCELLATION_PERIOD = 5;
    const STATUS_PAYED = 'payed';
    const STATUS_PENDING = 'pending';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_REFUND = 'refund';
    const STATUS_CHARGEBACK = 'chargeback';
    const STATUS_AUTHORISED = 'AUTHORISED';
    const CATEGORY_SUBSCRIPTION = 'subscription';
    const CATEGORY_SINGLE_BOX = 'single-box';
    const CATEGORY_COOLING_BAG = 'tasche';
    const SUBSCRIPTION_DELIVERY_FREQUENCY_PERIOD = 'week';
    const CHANGE_SUBSCRIPTION_EMAIL_PERIOD = 7;
    const LOCATION_AT_THE_DOOR = 'vor der Haustüre';

    const ORDER_BOX_PRODUCT_NAME = 'Deine individuelle Box';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['shipping_street_name','shipping_street_nr','billing_street_name','billing_street_nr'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $appends = ['products','billing_zip_city','shipping_zip_city'];

    protected $dates = ['child_birthday', 'delivery_date', 'cancellation_deadline'];

    public $notSave = false;


    // This boot-method is only for non-save when we need the object for simulating
    // We already have a same part in the saving-listener, but with this we have one uneeded save less.. whatever works.
    public static function boot()
    {
        parent::boot();
        static::saving(function ($someModel) {
            if($someModel->notSave) {
                return false;
            }
        });
    }

    protected $dispatchesEvents = [
        'saving' => \App\Events\OrderSaving::class,
    ];

    protected $attributes = [
        'deposit_location' => self::LOCATION_AT_THE_DOOR
    ];

    /**
     * Get the items for the order.
     */
    public function items()
    {
        return $this->hasMany('App\Models\OrderItem');
    }

    /**
     * Get the order_refunds for the order.
     */
    public function refunds()
    {
        return $this->hasMany('App\Models\OrderRefund');
    }
    public function getShippingStreet(){
        $shipping_address = $this->shipping_street_name." ".$this->shipping_street_nr;
        if(empty($this->shipping_street_nr)){
            $shipping_address = $this->shipping_street_name;
        }
        if(empty($this->shipping_street_name)){
            $shipping_address = $this->shipping_address_1;
        }
        return $shipping_address;
    }

    public function getBillingStreet(){
        $billing_address = $this->billing_street_name ." ".$this->billing_street_nr;
        if(empty($this->billing_street_nr)){
            $billing_address = $this->billing_street_name;
        }
        if(empty($this->billing_street_name)){
            $billing_address = $this->billing_address_1;
        }
        return $billing_address;
    }
    /**
     * Get all order items with type line item: boxes, colling bags and similar
     * @return mixed
     */
    public function line_items()
    {
        $items = OrderItem::where('order_id',$this->id)->where('item_type',self::DEFAULT_LINE_ITEM_TYPE)->get();
        return $items;
    }

    public function getDefaultTaxPercentage()
    {
        if($this->items->first() && $this->items->first()->product)
            return $this->items->first()->product->tax_percentage;
        return null;
    }

    /**
     * Get the subscription for the order
     */
    public function subscription()
    {
        return $this->hasOne('App\Models\Subscription');
    }

    /**
     * Get the coupon for the order
     */
    public function coupon()
    {
        return $this->belongsTo('App\Models\Coupon', 'coupon_id');
    }

    /**
     * Get the user for the order
     */
    public function user()
    {
        return $this->BelongsTo('App\Models\User');
    }

    /**
     * Get parent order
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\Order', 'parent_id');
    }

    /**
     * Get all children orders
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany('App\Models\Order', 'parent_id');
    }

    public function box()
    {
        return $this->BelongsTo('App\Models\Box', 'custom_box_id');
    }

    /**
     * Get all product ids which are order items in the order
     * @return array
     */
    public function getProductsAttribute()
    {
        $products = [];
        foreach ($this->items as $item) {
            if(!empty($item->product_id)) {
                $products[] = $item->product_id;
            }
        }
        return $products;
    }

    /**
     * Helper to translate contry to correct locale
     * @param $country
     * @return mixed
     */
    public function getCountryCode($country)
    {
        $locale = app('translator')->getLocale();
        switch($country)
        {
            case 'CH':
            case 'Schweiz':
                $locale = \Helpers::LOCALE_CH;
                break;
            case 'DE':
            case 'Germany':
            case 'Deutschland':
                $locale = \Helpers::LOCALE_DE;
                break;
            case 'AT':
            case 'Austria':
            case 'österreich':
            case 'Österreich':
                $locale = \Helpers::LOCALE_AT;
                break;
        }
        return Country::where('content_code',$locale)->first()->iso_alpha_2;
    }

    /**
     * Helper to translate currency
     * @param $currency
     * @return mixed|string
     */
    public function getCurrencyCode($currency)
    {
        switch($currency)
        {
            case '€':
                return 'EUR';
                break;
            case 'EUR':
                return 'EUR';
                break;
            case 'CHF':
                return 'CHF';
                break;
            default:
	            return env('DEFAULT_CURRENCY'); //TODO: Add to config instead of calling the env here.
	            break;
        }
    }

    /**
     * Get the country that owns the shipping.
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }
    /**
     * Get subscription for the order
     * since next order will take over subscription we need to search order's children and find their subscription
     * @TODO improve parent-child relationship in db
     * @param Order|null $order
     * @return mixed|null
     */
    public function find_subscription(Order $order=null)
    {
        if(is_null($order))
            return null;
        $subscription = $order->subscription;
        if(!is_null($subscription))
        {
            return $subscription;
        }
        else
        {
            $children = $order->children;
            if(empty($children))
                return null;
            else
            {
                if(empty($children->first()->subscription))
                {
                    return $this->find_subscription($children->first());
                }
                else
                    return $children->first()->subscription;
            }
        }
        return $subscription;
    }

    /**
     * Get shipping postcode
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function shippingPostcode()
    {
        return $this->hasOne('App\Models\Postcode','id','shipping_postcode_id');
    }

    /**
     * Get billing postcode
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function billingPostcode()
    {
        return $this->hasOne('App\Models\Postcode','id','billing_postcode_id');
    }

    /**
     * @return array
     */
    public function getBillingZipCityAttribute()
    {
        $zipCity = $this->billingPostcode;
        if($zipCity instanceof Postcode)
        {
            return [
                'id' => $zipCity->id,
                'zip_city' => $zipCity->postcode . ', ' . $zipCity->city
            ];
        }
        return [];
    }

    /**
     * @return array
     */
    public function getShippingZipCityAttribute()
    {
        $zipCity = $this->shippingPostcode;
        if($zipCity instanceof Postcode)
        {
            return [
                'id' => $zipCity->id,
                'zip_city' => $zipCity->postcode . ', ' . $zipCity->city
            ];
        }
        return [];
    }

    public static function getCurrentOrder($subscription){
        return $subscription->order;
    }

    public static function getFutureOrders($subscription){
        $current = self::getCurrentOrder($subscription);
        return Order::where('delivery_date','>',$current->delivery_date)->where('subscription_id',$subscription->id)->get();
    }
    public static function getNrOfFutureOrders($subscription){
        $current = self::getCurrentOrder($subscription);
        return Order::where('delivery_date','>',$current->delivery_date)->where('subscription_id',$subscription->id)->count();
    }
}