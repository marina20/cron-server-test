<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class AcEventDefinition extends Model
{

    protected $table = 'ac_event_definitions';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_name', 'event_type','event_query'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

}
