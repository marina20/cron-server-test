<?php
/**
 * Created by PhpStorm.
 * User: milenabasaragin
 * Date: 2019-03-12
 * Time: 16:16
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecurringAdyenRequest extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'recurring_adyen_request';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

}