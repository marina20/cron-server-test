<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Postcode extends Model
{
    /**
     * This model is without timestamp fields
     * @var bool
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'postcodes';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get the items for the order.
     */
    public function shippingOrders()
    {
        return $this->hasMany('App\Models\Order', 'shipping_postcode_id');
    }

    /**
     * Get the items for the order.
     */
    public function billingOrders()
    {
        return $this->hasMany('App\Models\Order','billing_postcode_id');
    }

    /**
     * Get the items for the order.
     */
    public function shippingProfiles()
    {
        return $this->hasMany('App\Models\Profile', 'shipping_postcode_id');
    }

    /**
     * Get the items for the order.
     */
    public function billingProfiles()
    {
        return $this->hasMany('App\Models\Profile', 'billing_postcode_id');
    }
}