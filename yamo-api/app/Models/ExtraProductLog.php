<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-07-26
 * Time: 12:00
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExtraProductLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'extra_products_log';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}