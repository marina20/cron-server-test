<?php
/**
 * Created by PhpStorm.
 * User: milenabasaragin
 * Date: 2019-02-05
 * Time: 15:17
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderSubscription extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_subscriptions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['order_id', 'subscription_id'];

    /**
     * Get the subscription for the order
     */
    public function subscription()
    {
        return $this->belongsTo('App\Models\Subscription', 'subscription_id');
    }

    /**
     * Get the coupon for the order
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }

}