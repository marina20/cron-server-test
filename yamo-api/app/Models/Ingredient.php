<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-08-14
 * Time: 12:44
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ingredients';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_key'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get the order that owns the item.
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product','products_ingredients');
    }
}