<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-08-13
 * Time: 14:56
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VatGroup extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vat_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['value','description','country_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get all the regions where Vat groups are applied
     */
    public function regions()
    {
        return $this->belongsToMany('App\Models\Regions','products_regions', 'vat_group_id', 'region_id');
    }

    /**
     * Get the country that owns the region.
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }
}