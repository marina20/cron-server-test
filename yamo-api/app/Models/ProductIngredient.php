<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-08-13
 * Time: 15:33
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductIngredient extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products_ingredients';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_id','ingredient_id'];

}