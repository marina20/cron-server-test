<?php

namespace App\Models;
use App\Events\LoyaltyVoucherRedeemedEvent;
use App\Models\Subscription;
use App\Services\Adyen\RecurringPaymentSaveOrder;
use App\Services\CreateOrder\CreateOrder;
use App\Services\OrderService;
use Illuminate\Database\Eloquent\Model;
use Auth;
use Carbon\Carbon;
use App\Traits\FormatMoney;
use Illuminate\Support\Facades\Log;
use Loyalty;
use Illuminate\Support\Collection;

class Voucher extends Model {
    use FormatMoney;

    public const PERCENT = "percent";
    public const AMOUNT = "amount";
    public const PRODUCT = "product";
    public const PRODUCTPERCENT = "productPercent";
    public const PRODUCTAMOUNT = "productAmount";

    protected $table = 'vouchers';
    protected $fillable = [
        'id', 'code', 'value_type', 'voucher_type', 'value','active','start_at','end_at','campaign','import_counter'
    ];
    protected $with = ['restrictions','products'];
    public function restrictions()
    {
        return $this->hasMany('App\Models\VoucherRestriction',"voucher_id","id");
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Vouchercategory');
    }
    public function products()
    {
        return $this->belongsToMany('App\Models\Product','voucher_products', 'voucher_id', 'product_id');
    }

    public function voucherProducts()
    {
        return $this->hasMany('App\Models\VoucherProduct','voucher_id');
    }

    public static function getDiscountByPercentOf($total, $percent){
        return ($total/100) * ($percent);
    }
    public static function getDiscountByAmountOf($total, $amount){
        $restFullPrice = $total - $amount;
        return $total - $restFullPrice;
    }


    /**
     * Check the voucher
     * @param String $code
     * @param Order $order
     * @return array success,order
     */
    public static function checkVoucher(String $code, Order $order):array{
        if(!Auth::check()){
            // If the user isn't logged in, we don't accept vouchers.
           return ['success' => false, 'order' => $order];
        }
        // Check for code, start- and enddate. If they are null, they are timeless true's.
        $voucher = self::getVoucher($code);
        if(empty($voucher)){
            return ['success' => false, 'order' => $order];
        }
        $success = false;
        if(self::checkRestrictions($voucher,$order,true)){
            $discount = 0;
            if($voucher->value_type==self::PERCENT || $voucher->value_type==self::PRODUCTPERCENT){
                $discount = self::getDiscountByPercentOf($order->order_total,$voucher->value);
            } else if($voucher->value_type==self::AMOUNT || $voucher->value_type==self::PRODUCTAMOUNT){
                $discount = $voucher->value;
            }

           $success = true;
            if ($voucher->value_type==self::PRODUCT) {
                // TODO add products in a temporary way to order, for future previews
            } else {

                if($discount > $order->order_total)
                    $discount = $order->order_total;

                $order->order_total_without_tax = $order->order_total_without_tax - $discount;
                $order->order_total = $order->order_total - $discount;

            }
            $order->discount += $voucher->formatMoney($discount);

            $order = self::checkVoucherSeparateDiscounts($order, $voucher->voucher_type, $discount);
        }
        return ['success' => $success, 'order' => $order];
    }

    /**
     * Array of free items product ids
     * @param $code
     * @return array
     */
    public static function getFreeItems($code) : ?Collection
    {
        $voucher = self::getVoucher($code);
        if(empty($voucher)){
            return new Collection();
        }
        if ($voucher->value_type==self::PRODUCT)
        {
            return $voucher->voucherProducts;
        }
        return new Collection();
    }

    /**
     * Add discount value to proper column based on voucher_type
     * Temporary order for checking prices
     * @param Order $order
     * @param string $voucher_type
     * @param string $discount
     * @return Order
     */
    private static function checkVoucherSeparateDiscounts(Order $order, string $voucher_type, string $discount) : Order
    {
        if(!isset($order->{$voucher_type . '_discount'}))
            return $order;

        $order->{$voucher_type . '_discount'} += $discount;
        return $order;
    }

    /**
     * Finally apply the voucher
     * @param String $code
     * @param Order $order
     * @param bool $publicInput
     * @return array ['success','order']
     */
    public static function applyVoucher(String $code, Order $order,bool $publicInput=false):array{
        $user = Auth::user();
        if(empty($user)){
            $user = User::find($order->user_id);
        }
        if(!Auth::check() && $publicInput){
            // If the user isn't logged in, we don't accept vouchers.
            VoucherAttempts::create(['code' => $code,'voucher_id' => 0,'order_id' => $order->id,'user_id' => $user->id,'status' => 'notloggedin']);
            return ['success' => false, 'order' => $order];
        }

        $voucher = self::getVoucher($code);
        if(empty($voucher)){
            VoucherAttempts::create(['code' => $code,'voucher_id' => 0,'order_id' => $order->id,'user_id' => $user->id,'status' => 'vouchernotfound']);
            return ['success' => false, 'order' => $order];
        }
        if($voucher->voucher_type!=='public' && $publicInput){
            VoucherAttempts::create(['code' => $code,'voucher_id' => 0,'order_id' => $order->id,'user_id' => $user->id,'status' => 'trytoapplysystemvoucher']);
            return ['success' => false, 'order' => $order];
        }
        // if only second or so apply, but not first. we need to save coupon-code, still!

        $success = false;
        if(self::checkRestrictions($voucher,$order)){
            $discount = 0;
            if($voucher->value_type==self::PERCENT || $voucher->value_type==self::PRODUCTPERCENT){
                $discount = self::getDiscountByPercentOf($order->order_total,$voucher->value);
            } else if($voucher->value_type==self::AMOUNT || $voucher->value_type==self::PRODUCTAMOUNT){
                $discount = $voucher->value;

            }

            VoucherAttempts::create(['code' => $code,'voucher_id' => $voucher->id,'order_id' => $order->id,'user_id' => $user->id,'status' => 'success']);
            $success = true;

            if ($voucher->value_type == self::PRODUCTPERCENT || $voucher->value_type==self::PRODUCTAMOUNT) {
                $order = self::addProduct($voucher,$order,$user);
            }

            // If the voucher does cover the whole order...
            if($order->order_total<=$discount && $voucher->value_type !== 'product'){
                // ... we can discount all of the order ...
                $discount = $order->order_total;

                // and set all totals to 0
                $order->order_total_without_tax = 0;
                $order->order_total = 0;

                // if the voucher is more worth than the order-price
                $taxRate = $order->items->first()->tax_rate;
                foreach($order->items as $item){
                    // Write discount into proper column based on voucher_type
                    $item->{$voucher->voucher_type.'_discount'} = $voucher->formatMoney($item->total);
                    $item->{$voucher->voucher_type.'_discount_tax'} = $voucher->formatMoney($item->total_tax);
                    $item->total_discount = $voucher->formatMoney($item->subtotal);
                    $item->total_discount_tax = $voucher->formatMoney($item->subtotal_tax);
                    $item->total = $voucher->formatMoney(0);
                    $item->total_tax = $voucher->formatMoney(0);
                    $item->item_price = $voucher->formatMoney(0);
                    $item->save();
                }
                if(empty($order->discount)){
                    $order->discount = $voucher->formatMoney($discount);
                } else {
                    $order->discount = $voucher->formatMoney((float)$order->discount + $discount);
                }
                $order->discount_tax = $voucher->formatMoney(CreateOrder::calculateVAT((float)$order->discount,$taxRate));
                $order->order_tax = 0;
                if($voucher->voucher_type=='public') {
                    $order->public_discount = $voucher->formatMoney($discount);
                    $order->public_discount_tax = CreateOrder::calculateVAT((float)$discount, $taxRate);
                } elseif($voucher->voucher_type=='system') {
                    $order->system_discount = $voucher->formatMoney($discount);
                    $order->system_discount_tax = CreateOrder::calculateVAT(((float)$discount),$taxRate);
                } elseif($voucher->voucher_type=='loyalty') {
                    $order->loyalty_discount = $voucher->formatMoney($discount);
                    $order->loyalty_discount_tax = CreateOrder::calculateVAT(((float)$discount),$taxRate);
                }
                $order->save();
                // end of if the voucher is more worth than the order-price
                // add redemption or we won't have it for total discounts
                VoucherRedemption::create(['voucher_id' => $voucher->id, 'order_id' => $order->id, 'user_id' => $user->id, 'amount_cents' => $discount]);


            // If the voucher is less than the order ...
            } else {
                if ($voucher->value_type == 'product') {
                    $order = self::addProduct($voucher,$order,$user);
                    VoucherRedemption::create(['voucher_id' => $voucher->id, 'order_id' => $order->id, 'user_id' => $user->id, 'amount_cents' => 0]);
                } else {
                    // ... and it's amount or percent, we need to calculate
                    // 20% -> 0.2, so rdy to multiply
                    $discount = 0;
                    $taxRate = 0;
                    $total = 0;
                    $publicDiscount = 0;
                    $systemDiscount = 0;
                    $loyaltyDiscount = 0;
                    foreach($order->items as $item){
                        $voucherValue = $voucher->value;
                        if($voucher->voucher_type=='system') {
                            if($item->product->size==200){
                                $countryCode = ReferralUrl::beautifyCountry($order->shipping_country);
                                if(self::startsWith($voucher->code,"SUBSCRIPTION_")) {
                                    $voucherValue = Voucher::getVoucher("SUBSCRIPTION_200_" . $countryCode)->value;
                                } elseif (self::startsWith($voucher->code,"FIRST_SUBSCRIPTION_")) {
                                    $voucherValue = Voucher::getVoucher("FIRST_SUBSCRIPTION_200_" . $countryCode)->value;
                                }
                            }
                        }
                        $singleProductPercentage = $voucherValue / 100;
                        if(empty($item->item_price)){
                            $item->item_price = (float)$item->total / $item->qty;
                        }
                        $taxRate = $item->tax_rate;
                        if ($voucher->value_type == self::AMOUNT || $voucher->value_type == self::PRODUCTAMOUNT) {
                            $singleProductPercentage = $voucherValue / $order->order_total;
                        }

                        $singleItemDiscount = ((float)$item->item_price * $singleProductPercentage);
                        $item->item_price = $item->item_price - $singleItemDiscount;
                        $discount += $singleItemDiscount*$item->qty;
                        //$item->total_discount_tax = $voucher->formatMoney(CreateOrder::calculateVAT((float)$item->total_discount + ($singleItemDiscount * $item->qty), $item->tax_rate));
                        $item->total_discount = $voucher->formatMoney($item->total_discount + ($singleItemDiscount * $item->qty));
                        if($voucher->voucher_type!=='system') {
                            if($voucher->voucher_type == VoucherType::TYPE_LOYALTY)
                            {
                                $item->loyalty_discount = $voucher->formatMoney($singleItemDiscount * $item->qty);
                                $loyaltyDiscount += ($singleItemDiscount * $item->qty);
                                $item->loyalty_discount_tax = $voucher->formatMoney(CreateOrder::calculateVAT(((float)$singleItemDiscount * $item->qty), $item->tax_rate));
                            }
                            else {
                                $item->public_discount = $voucher->formatMoney($singleItemDiscount * $item->qty);
                                $publicDiscount += ($singleItemDiscount * $item->qty);
                                $item->public_discount_tax = $voucher->formatMoney(CreateOrder::calculateVAT(((float)$singleItemDiscount * $item->qty), $item->tax_rate));
                            }
                        } else {
                            $item->system_discount = $voucher->formatMoney($singleItemDiscount * $item->qty);
                            $systemDiscount += ($singleItemDiscount * $item->qty);
                            $item->system_discount_tax = $voucher->formatMoney(CreateOrder::calculateVAT(((float)$singleItemDiscount * $item->qty),$item->tax_rate));
                        }
                        $item->total = $voucher->formatMoney($item->item_price * $item->qty);
                        if($voucher->voucher_type==='public') {
                            $total += $item->item_price * $item->qty;
                        } else {
                            $total += $voucher->formatMoney($item->item_price) * $item->qty;
                        }
                        $item->total_discount_tax = $voucher->formatMoney($item->system_discount_tax + $item->public_discount_tax + $item->loyalty_discount_tax);
                        $item->total_tax = $voucher->formatMoney(CreateOrder::calculateVAT(((float)$item->total),$item->tax_rate));
                        $item->save();
                    }
                    VoucherRedemption::create(['voucher_id' => $voucher->id, 'order_id' => $order->id, 'user_id' => $user->id, 'amount_cents' => $discount]);
                    $order->order_total = $voucher->formatMoney($total);
                    $order->order_tax = $voucher->formatMoney(CreateOrder::calculateVAT((float)$order->order_total,$taxRate));
                    $order->order_total_without_tax = $voucher->formatMoney($order->order_total - $order->order_tax);
                    if(empty($order->discount)){
                        $order->discount = $voucher->formatMoney($discount);
                        $order->discount_tax = $voucher->formatMoney(CreateOrder::calculateVAT($discount,$taxRate));
                    } else {
                        $order->discount = $voucher->formatMoney((float)$order->discount + $discount);
                        $order->discount_tax = $voucher->formatMoney($order->discount_tax + CreateOrder::calculateVAT($discount,$taxRate));
                    }
                    $order->discount = $voucher->formatMoney($order->discount);
                    
                    if($voucher->voucher_type!=='system') {
                        if($voucher->voucher_type == VoucherType::TYPE_LOYALTY)
                        {
                            if(empty($order->loyalty_discount)){
                                $order->loyalty_discount = $voucher->formatMoney($loyaltyDiscount);
                            } else {
                                $order->loyalty_discount = $voucher->formatMoney((float)$order->loyalty_discount + $loyaltyDiscount);
                            }
                            if(empty($order->loyalty_discount_tax)){
                                $order->loyalty_discount_tax = $voucher->formatMoney(CreateOrder::calculateVAT($loyaltyDiscount,$taxRate));
                            } else {
                                $order->loyalty_discount_tax = $voucher->formatMoney(((float)$order->loyalty_discount_tax+CreateOrder::calculateVAT($loyaltyDiscount,$taxRate)));
                            }
                        }
                        else
                        {
                            if(empty($order->public_discount)){
                                $order->public_discount = $voucher->formatMoney($publicDiscount);
                            } else {
                                $order->public_discount = $voucher->formatMoney((float)$order->public_discount + $publicDiscount);
                            }
                            if(empty($order->public_discount_tax)){
                                $order->public_discount_tax = $voucher->formatMoney(CreateOrder::calculateVAT($publicDiscount,$taxRate));
                            } else {
                                $order->public_discount_tax = $voucher->formatMoney(((float)$order->public_discount_tax+CreateOrder::calculateVAT($publicDiscount,$taxRate)));
                            }
                        }

                    } else {
                        // system, first in order
                        $order->system_discount = $voucher->formatMoney($systemDiscount);
                        $order->system_discount_tax = $voucher->formatMoney(CreateOrder::calculateVAT((float)$systemDiscount,$taxRate));
                    }
                }
            }

        } else {
            // test failed, but now we check if he can pass later!
            // TODO what to return? success true, cause it will apply, and unmodified order, except coupon_code?
            if(!empty($order->is_subscription) && ($voucher->containRestrictions('on_order_number') || $voucher->containRestrictions('on_subscription_order_number'))){
                $foundRecurring = false;
                $foundFirst = false;
                // We check if a restriction is ment to be first and for recurring
                foreach($voucher->restrictions as $restriction){
                    if($restriction->type->type == 'on_order_number' || $restriction->type->type == 'on_subscription_order_number'){
                        if($restriction->restriction_payload=="0" || $restriction->restriction_payload=="1"){
                            $foundFirst = true;
                        }
                        if($restriction->restriction_payload!="0" && $restriction->restriction_payload!="1"){
                            $foundRecurring = true;
                        }
                    }
                }
                // if it hasn't applied on first round but it's recurring, then we need to apply the code at least
                // (it's the most-used value), so we find the voucher again on recurring
                if(!$foundFirst && $foundRecurring){
                    // not applied first round but for later - lets save coupon-code only
                    if($voucher->voucher_type==='public') {
                        $order->coupon_id = $voucher->id;
                        $order->coupon_type = $voucher->value_type;
                        $order->coupon_code = strtoupper($voucher->code);
                        $order->save();
                    }

                }
            }
            VoucherAttempts::create(['code' => $code,'voucher_id' => $voucher->id,'order_id' => $order->id,'user_id' => $user->id,'status' => 'restrictionfail']);
            return ['success' => false, 'order' => $order];
        }
        if($voucher->voucher_type!=='system' && $voucher->voucher_type!==VoucherType::TYPE_LOYALTY) {
            $order->coupon_id = $voucher->id;
            $order->coupon_type = $voucher->value_type;
            $order->coupon_code = strtoupper($voucher->code);
        }

        $order->save();
        Log::info(json_encode($voucher));
        return ['success' => $success, 'order' => $order];
    }

    private static function addProduct(Voucher $voucher, Order $order, User $user){
        $voucherProducts = $voucher->voucherProducts->toArray();
        $products = [];
        foreach ($voucherProducts as $voucherProduct) {
            $products[] = ['id'=>$voucherProduct['product_id'], 'quantity'=>$voucherProduct['qty']];
        }
        $order = $voucher->createOrderItems($order, $products, true);
        OrderService::generateSKU($order);
        return $order;
    }

    /**
     * Only used in this class, may also ReferralUrl - propaply should be moved to helpers.
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    /**
     * Redeem the vouchers. Only for payment-confirmations.
     * @param $order
     * @return mixed
     */
    public static function redeemVoucher($order){
        if(Order::STATUS_PENDING != $order->status) {
            $vrs = VoucherRedemption::where(['order_id' => $order->id])->get();
            foreach($vrs as $vr) {
                if (!empty($vr)) {
                    $vr->confirmed = true;
                    $vr->save();
                    // if voucher is loyalty, look for it's copies in different orders
                    // and remove it from there and recalculate orders
                    // let's trigger an event which will handle this
                    if(VoucherType::TYPE_LOYALTY == $vr->voucher->voucher_type)
                        event(new LoyaltyVoucherRedeemedEvent($vr));
                }
            }
        }
        return $order;
    }

    public static $voucherCache = [];

    /**
     * Get the voucher and respect active, start and enddate
     * Can be empty!
     * @param $code
     * @return mixed
     */
    public static function getVoucher($code){
        if(array_key_exists($code,self::$voucherCache)){
            return self::$voucherCache[$code];
        }
        $voucher = Voucher::where(['code' => strtoupper($code)])->where(function ($query) {
            $now = Carbon::now();
            $query->where('start_at', '<', $now)
                ->orWhereNull('start_at');
        })->where(function ($query) {
            $now = Carbon::now();
            $query->where('end_at', '>', $now)
                ->orWhereNull('end_at');
        })->where(['active' => 1])->first();
        self::$voucherCache[$code] = $voucher;
        return $voucher;
    }

    /**
     * Only get the value of a voucher without change anything.
     * Propaply we should use a such method more central to have the calculations better centralised!
     * @param $code
     * @param $total
     * @return array
     */
    public static function getDiscountedAmount($code,$total){
        $voucher = self::getVoucher($code);
        if(empty($voucher)){
            return ['discount' => 0,'percent' => 0];
        }
        $value = $voucher->value;
        $discount = 0;
        $percent = 0;
        if($voucher->value_type==self::PERCENT || $voucher->value_type==self::PRODUCTPERCENT){
            $discount = self::getDiscountByPercentOf($total,$value);
            $percent = $value;
        } else if($voucher->value_type==self::AMOUNT || $voucher->value_type==self::PRODUCTAMOUNT){
            // $discount = self::getDiscountByAmountOf($order->order_total,$value);
            // The result of the above line should be exactly same as the one
            $discount = $value;
            $percent = ($total / 100) * $value;

        }
        return ['discount' => $discount,'percent' => $percent];
    }

    /**
     * Check if the voucher contains a restriction
     * @param $restrictionName
     * @return array
     */
    public function containRestrictions($restrictionName){
        $restrictions = [];
        foreach($this->restrictions as $restriction){
            if($restriction->type->type==$restrictionName){
                $restrictions[] = $restriction;
            }
        }
        return $restrictions;
    }

    /**
     * This is mostly used for the ontop-banner and also there only for begin..
     * As we don't have a user at this stage, we can only guess stuff
     * @param Voucher $voucher
     * @param Order $order
     * @return bool
     */
    public static function preFilter(Voucher $voucher, Order $order):bool
    {
        foreach ($voucher->restrictions as $restriction) {
            if ($restriction->type->type === 'region') {
                if ($order->region_id !== (int)$restriction->restriction_payload) {
                    return false;
                }
            }
            if ($restriction->type->type === 'country') {
                if ($order->country_id !== (int)$restriction->restriction_payload && $order->country->iso_alpha_2 !== $restriction->restriction_payload) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * The most-central method for check all the conditions in one voucher
     * check = true means we can use a 'incomplete' order object - usefull for various precheck-situations
     * @param Voucher $voucher
     * @param Order $order
     * @param bool $check
     * @return bool
     */
    public static function checkRestrictions(Voucher $voucher, Order $order,$check=false):bool {
        $user = Auth::user();
        if(empty($user)){
            $user = User::find($order->user_id);
        }
        foreach($voucher->restrictions as $restriction){
            if($restriction->type->type==='region'){
                if($order->region_id!==(int)$restriction->restriction_payload){
                    return false;
                }
            }
            if($restriction->type->type==='country'){
                if($order->country_id!==(int)$restriction->restriction_payload && strtoupper($order->country->iso_alpha_2) !== strtoupper($restriction->restriction_payload)){
                    return false;
                }
            }
            if($restriction->type->type==='age'){
                $years = \Carbon\Carbon::parse($user->profile->customer_birthday)->age;
                if($years<(int)$restriction->restriction_payload){
                    return false;
                }
            }
            if($restriction->type->type==='order_type'){
                if(strtoupper($restriction->restriction_payload)=="SINGLE" && $order->is_subscription==1){
                    return false;
                } else if(strtoupper($restriction->restriction_payload)=="SUBSCRIPTION" && empty($order->is_subscription)){
                    return false;
                }
            }
            if($restriction->type->type==='new_user'){
                if(!Auth::check()){
                    return false;
                }
                if(!$user->isNew()){
                    return false;
                }
            }
            if($restriction->type->type==='customer_type'){
                // confusion in front! also when the table is called order_type, restriction customer_type is more correct!
                $orderType = OrderType::where(['type' => strtoupper($restriction->restriction_payload)])->first();
                //var_dump($orderType->type); die();
                if(empty($orderType)){
                    // is this alright? basicly i would except a type here, so a fail behind admin-panel
                    return false;
                }
                if(empty($order->order_type_id)){
                    return false;
                }
                if($orderType->id != $order->order_type_id){
                    return false;
                }
            }

            if($restriction->type->type==='on_subscription_order_number'){
                // skip test if it's a check and subscription-flag is set. reason: at this moment, we have no subscription_id and the count will be 0 when user check for new order.
                if($check && !empty($order->is_subscription)){
                    continue;
                }
                // check for sub-id cause it can lead to very ugly bugs when this is set to null
                if(empty($order->subscription_id)){
                    return false;
                }
                $subscriptionOrderCount = Order::where(['subscription_id' => $order->subscription_id])->whereNotNull('paid_date')->where(['resent' => 0])->count()+1;
                $ok = false;
                $orderNumberRestrictions = VoucherRestriction::where(['voucher_id' => $voucher->id,'restriction_type_id' => $restriction->type->id])->get();
                foreach ($orderNumberRestrictions as $orderNumberRestriction){
                    // basicly needs to be 1
                    //echo $orderNumberRestriction->restriction_payload."|".$subscriptionOrderCount.",";
                    if($subscriptionOrderCount==(int)$orderNumberRestriction->restriction_payload || (int)$orderNumberRestriction->restriction_payload==0){
                        $ok = true;
                    }
                    if($check){
                        $ok = true;
                    }
                }
                if(!$ok){
                    return false;
                }
            }

            if($restriction->type->type==='frequency'){
                if(empty($order->subscription_id)) {
                    return false;
                }
                $subscription = Subscription::find($order->subscription_id);
                if(empty($subscription) || $subscription->billing_interval != (int)$restriction->restriction_payload) {
                    return false;
                }
            }

            if($restriction->type->type==='first_x_subscription_boxes'){
                // skip test if it's a check and subscription-flag is set. reason: at this moment, we have no subscription_id and the count will be 0 when user check for new order.
                if($check && !empty($order->is_subscription)){
                    continue;
                }
                if(empty($order->subscription_id)){
                    return false;
                }
                $subscriptionOrderCount = Order::where(['subscription_id' => $order->subscription_id])->whereNotNull('paid_date')->where(['resent' => 0])->count()+1;
                /* Would be always 1, not useable for this scenario
                 * if(empty($order->subscription_id)){
                    $subscriptionOrderCount = Subscription::where(['order_id' => $order->id])->count();
                }*/
                $ok = false;
                $orderNumberRestrictions = VoucherRestriction::where(['voucher_id' => $voucher->id,'restriction_type_id' => $restriction->type->id])->get();
                // does that make sense for this restriction? should work at least

                foreach ($orderNumberRestrictions as $orderNumberRestriction){
                    //echo $subscriptionOrderCount."|".$orderNumberRestriction->restriction_payload;
                    if($subscriptionOrderCount<=(int)$orderNumberRestriction->restriction_payload){
                        $ok = true;
                    }
                }
                if(!$ok){
                    return false;
                }
            }

            if($restriction->type->type==='on_order_number'){
                $overallOrderCount = Order::where(['user_id' => $user->id])->whereNotNull('paid_date')->where(['resent' => 0])->count()+1;
                $ok = false;
                //echo $overallOrderCount; die();
                $orderNumberRestrictions = VoucherRestriction::where(['voucher_id' => $voucher->id,'restriction_type_id' => $restriction->type->id])->get();
                foreach ($orderNumberRestrictions as $orderNumberRestriction){
                    if($overallOrderCount==(int)$orderNumberRestriction->restriction_payload || (int)$orderNumberRestriction->restriction_payload==0){
                        $ok = true;
                    }
                    if(1==(int)$orderNumberRestriction->restriction_payload&&$user->isNew()){
                        $ok = true;
                    }
                    if($check){
                        $ok = true;
                    }
                }
                if(!$ok){
                    return false;
                }
            }



            if($restriction->type->type==='first_x_boxes'){
                $overallOrderCount = Order::where(['user_id' => $user->id])->whereNotNull('paid_date')->where(['resent' => 0])->count()+1;
                $ok = false;
                $orderNumberRestrictions = VoucherRestriction::where(['voucher_id' => $voucher->id,'restriction_type_id' => $restriction->type->id])->get();
                // does that make sense for this restriction? should work at least
                foreach ($orderNumberRestrictions as $orderNumberRestriction){
                    if($overallOrderCount<=(int)$orderNumberRestriction->restriction_payload){
                        $ok = true;
                    }
                }
                if(!$ok){
                    return false;
                }
            }

            if($restriction->type->type==='uses_per_user'){
                // check how many redemptions by that user have been made
                $redemptionCount = VoucherRedemption::where(['voucher_id' => $voucher->id])->where(['invalid' => 0])->where(['confirmed' => 1])->where(['user_id' => $user->id])->count();
                if($redemptionCount>=(int)$restriction->restriction_payload){
                    return false;
                }
            }
            if($restriction->type->type==='product'){
                $found = false;
                foreach($order->items as $item){
                    // Accept id or sku-identifier
                    $product = Product::find($item->product_id);
                    if($product->product_identifier_number==$restriction->restriction_payload || $product->product_identifier_letter==$restriction->restriction_payload){
                        $found = true;
                    }
                }
                if(!$found){
                    return false;
                }
            }

            if($restriction->type->type==='min_products_needed'){
                $splitted = explode(":", $restriction->restriction_payload);
                if(count($splitted)==1){
                    $counter = 0;
                    foreach ($order->items as $item) {
                        $counter += $item->qty;
                    }
                    if($counter<(int)$splitted[0]){
                        return false;
                    }
                } else {
                    $counter = 0;
                    foreach ($order->items as $item) {
                        // Accept id or sku-identifier
                        $product = Product::find($item->product_id);
                        if ($product->product_identifier_number == $splitted[0] || $product->product_identifier_letter == $splitted[0]) {
                            $counter = $item->qty;
                        }
                    }
                    if ($counter<(int)$splitted[1]) {
                        return false;
                    }
                }
            }


            if($restriction->type->type==='category'){
                $found = false;
                foreach($order->items as $item){
                    $product = Product::find($item->product_id);
                    if(strtoupper($product->categories->name_key)==strtoupper($restriction->restriction_payload)){
                        $found = true;
                    }
                }
                if(!$found){
                    return false;
                }
            }
            if($restriction->type->type==='max_total_uses'){
                // check how many redemptions by that user have been made
                $redemptionCount = VoucherRedemption::where(['voucher_id' => $voucher->id])->where(['confirmed' => 1])->where(['invalid' => 0])->count();
                if($redemptionCount>=(int)$restriction->restriction_payload){
                    return false;
                }
            }

        }
        return true;
    }




    public static function temporaryCreateOrderItems(Order $order, $products)
    {
        try
        {
        // loop items
        $country = Country::where('content_code',app('translator')->getLocale())->first();
        $countryIsoAlpha2 = $country->iso_alpha_2;
        $v = new Voucher;
        $order->order_total = 0;
        foreach ($products as $item) {
            $product = Product::find($item['id']);
            $v = new Voucher; // just to have format-money
            $price = $v->formatMoney($product->price);
            $order_item = new OrderItem();
            $order_item->notSave = true;
            $order_item->product_id = $product->id;
            $order_item->order_id = $order->id;
            $order_item->item_name = $product->name;
            $order_item->item_type = OrderItem::ITEM_TYPE_LINE;
            $order_item->qty = $item['quantity'];
            $order_item->total = $price*$order_item->qty;
            $order_item->total_tax = $price - (($price / (100 + $product->tax_percentage)));
            $order_item->subtotal = $order_item->total;
            $order_item->subtotal_tax = $order_item->total_tax;
            $order->items->add($order_item);
        }

         return $order;
         }
         catch (\Throwable $ex)
         {
             Log::info('Voucher exception in createOrderItems: '.$ex->getMessage());
             app('sentry')->captureException($ex);
             $order->coupon_code = null;
             return $order;
         }
    }

    // TODO this method should be moved and generalized. It will be gone here after refactoring
    // It's mainly copied from create-order - therefore deprecated stuff can be included.
    public function createOrderItems(Order $order, $products, $free=false)
    {
        try
        {
            // loop items
            foreach ($products as $item) {
                $product = Product::find($item['id']);
                // price helper
                $price = $product->price;
                // let's create order item
                $order_item = new OrderItem();
                $order_item->product_id = $product->id;
                $order_item->order_id = $order->id;
                $order_item->item_name = $product->name;
                $order_item->item_type = OrderItem::ITEM_TYPE_LINE;
                $order_item->qty = $item['quantity'];
                $order_item->tax_rate = $product->tax_percentage;
                if($free){
                    $order_item->total = "0.00";
                    $order_item->total_tax = "0.00";
                    $order_item->item_price = "0.00";
                    $order_item->subtotal = "0.00";
                    $order_item->subtotal_tax = "0.00";
                } else {
                    $order_item->total = $price * $item['quantity'];
                    $order_item->item_price = $price;
                    $order_item->total_tax = $price - (($price / (100 + $product->tax_percentage)));
                    $order_item->subtotal = $order_item->total;
                    $order_item->subtotal_tax = $order_item->total_tax;
                }

                $order->items->add($order_item);
                $order_item->save();
            }
            return $order;
        }
        catch (\Throwable $ex)
        {
            Log::info('Voucher exception in createOrderItems: '.$ex->getMessage());
            app('sentry')->captureException($ex);
            return $order;
        }
    }

    /**
     * This method is used for both creating new recurring order and customizing existing box
     * @param Order $order
     * @return Order
     */
    public static function applyRecurringDiscounts(Order $order):Order{
        // TODO test the discount-part
        // This save is for the findability in the tests
        $order->save();
        $subscriptionOrderCount = Order::where(['subscription_id' => $order->subscription_id])->whereNotNull('paid_date')->where(['resent' => 0])->count()+1;
        // $firstXBoxesRestrictions = VoucherRestriction::where(['restriction_type' => 'first_x_boxes'])->where($subscriptionOrderCount, "<", 'restriction_payload')->get();
        $firstXBoxesRestrictions = VoucherRestriction::whereHas('type', function ($query) {
            $query->where('type', 'first_x_boxes');
            $query->orWhere('type', 'first_x_subscription_boxes');
            $query->orWhere('type', 'on_order_number');
            $query->orWhere('type', 'on_subscription_order_number');
            $query->orWhere('type', 'frequency');
        })->get();

        foreach($firstXBoxesRestrictions as $restriction){
            $ok = false;
            if($restriction->type->type == 'first_x_subscription_boxes' && $subscriptionOrderCount <= (int)$restriction->restriction_payload){
                $ok = true;
            } else if($restriction->type->type == 'on_order_number'){
                $overallOrderCount = Order::where(['user_id' => $order->user_id])->whereNotNull('paid_date')->where(['resent' => 0])->count()+1;
                if($overallOrderCount==(int)$restriction->restriction_payload || (int)$restriction->restriction_payload==0){
                    $ok = true;
                }
            } else if($restriction->type->type == 'first_x_boxes'){
                $overallOrderCount = Order::where(['user_id' => $order->user_id])->whereNotNull('paid_date')->where(['resent' => 0])->count()+1;
                if($overallOrderCount<=(int)$restriction->restriction_payload){
                    $ok = true;
                }
            } else if($restriction->type->type == 'on_subscription_order_number' && ($subscriptionOrderCount==(int)$restriction->restriction_payload || (int)$restriction->restriction_payload==0)){
                $ok = true;
            } else if($restriction->type->type == 'frequency' && (!empty(Subscription::find($order->subscription_id))) && (Subscription::find($order->subscription_id)->billing_interval==(int)$restriction->restriction_payload)) {
                $ok = true;
            }
            if($ok){
                $voucher = Voucher::where(['id' => $restriction->voucher_id, 'voucher_type' => 'public','code' => $order->coupon_code])->first();
                if(!empty($voucher)) {
                    // all other restrictions are checked by applyVoucher
                    $result = Voucher::applyVoucher($voucher->code, $order);
                    $order = $result['order'];
                }
            }
        }

        // apply loyalty discount
        $order = Loyalty::apply($order);

        $order = ReferralUrl::applyWallet($order);
        $order->save();
        return $order;
    }
}
