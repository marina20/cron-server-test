<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class VoucherRedemption extends Model {

    protected $table = 'voucher_redemptions';
    protected $fillable = [
        'id', 'voucher_id', 'order_id','user_id','amount_cents','invalid','confirmed'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id', 'id');
    }

    public function voucher()
    {
        return $this->belongsTo('App\Models\Voucher', 'voucher_id', 'id');
    }

}
