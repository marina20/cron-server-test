<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-07-16
 * Time: 17:29
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExtraProduct extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'extra_products';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id','product_code'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}