<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BundleRegion extends Model
{
    protected $with = ['regions'];
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'bundles_regions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['bundle_id','region_id','active_start_date','active_end_date','active','out_of_stock','visible'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get regions for the BundleRegion.
     */
    public function regions()
    {
        return $this->hasMany('App\Models\Region','id','region_id');
    }
}