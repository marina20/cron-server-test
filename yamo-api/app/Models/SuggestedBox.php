<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\Order;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Jobs\SendResetEmailJob;
use Tymon\JWTAuth\Contracts\JWTSubject;

class SuggestedBox extends Model
{
    protected $fillable = [
        'children_age', 'product_type', 'product_content','default_box','registered_at','box_id'
    ];

    public function box(){
        return $this->belongsTo("App\Models\Box","box_id");
    }
}