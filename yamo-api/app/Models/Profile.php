<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/7/17
 * Time: 4:36 PM
 */

namespace App\Models;

use App\Events\ProfileSaving;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'user_id', 'child_birthday', 'accepts_newsletter', 'gender','url_referrer', 'customer_birthday','shipping_street_name','shipping_street_nr','billing_street_name','billing_street_nr','wallet_amount_cents','wallet_amount_currency'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['bc_capabilities','bc_user_level','last_update','shipping_method','_order_count','_money_spent','billing_delivery', 'shippingPostcode', 'billingPostcode'];

    protected $appends = ['customer_number','billing_zip_city','shipping_zip_city'];

    protected $dispatchesEvents = [
        'saving' => ProfileSaving::class,
    ];

    public function getShippingStreet(){
        $shipping_address = $this->shipping_street_name." ".$this->shipping_street_nr;
        if(empty($this->shipping_street_nr)){
            $shipping_address = $this->shipping_street_name;
        }
        if(empty($this->shipping_street_name)){
            $shipping_address = $this->shipping_address_1;
        }
        return $shipping_address;
    }

    public function getBillingStreet(){
        $billing_address = $this->billing_street_name ." ".$this->billing_street_nr;
        if(empty($this->billing_street_nr)){
            $billing_address = $this->billing_street_name;
        }
        if(empty($billing_address)){
            $billing_address = $this->billing_address_1;
        }
        return $billing_address;
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function getCustomerNumberAttribute()
    {
        return $this->user_id;
    }

    public function getAcceptsNewsletterAttribute($value)
    {
        if($value == 1)
        {
            return true;
        }
        return false;
    }

    /**
     * Get shipping postcode
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function shippingPostcode()
    {
        return $this->hasOne('App\Models\Postcode','id','shipping_postcode_id');
    }

    /**
     * Get billing postcode
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function billingPostcode()
    {
        return $this->hasOne('App\Models\Postcode','id','billing_postcode_id');
    }

    /**
     * @return array
     */
    public function getBillingZipCityAttribute()
    {
        $zipCity = $this->billingPostcode;

        if($zipCity instanceof Postcode)
        {
            return [
                'id' => $zipCity->id,
                'zip_city' => $zipCity->postcode . ', ' . $zipCity->city
            ];
        }

        return [];
    }

    /**
     * @return array
     */
    public function getShippingZipCityAttribute()
    {
        $zipCity = $this->shippingPostcode;

        if($zipCity instanceof Postcode)
        {
            return [
                'id' => $zipCity->id,
                'zip_city' => $zipCity->postcode . ', ' . $zipCity->city
            ];
        }

        return [];
    }
}