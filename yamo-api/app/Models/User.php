<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use App\Jobs\SendCreatePasswordEmailJob;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;
    const STATUS_PAYED = 'payed';
    const STATUS_PENDING = 'pending';
    const STATUS_CANCELLED = 'cancelled';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','confirmation_code','registered_at','from_wizard'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password','confirmed','confirmation_code','remember_token'
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'eml'=>$this->email
        ];
    }

    /**
     * Get user's profile
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne('App\Models\Profile');
    }

    /**
     * Checking if user is new. We are checking if user don't have any pending or paid orders
     * @return bool
     */
    public function isNew()
    {
        try {
            $orders = DB::table('orders')
                ->where('user_id', '=', $this->id)
                ->whereNotNull('paid_date')
                ->get();

            if ($orders->isEmpty()) {
                return true;
            }
            return false;
        }
        catch (\Exception $exception)
        {
            app('sentry')->captureException($exception);
            return false;
        }
    }

    public function hadSubscription(){
        $sub = Subscription::where(['user_id' => $this->id])->first();
        if(empty($sub)){
            return false;
        }
        return true;
    }

    /**
     * @param string $token
     */
    public function sendPasswordResetNotification($token) {
        dispatch(new SendCreatePasswordEmailJob($this,$token));
    }
}
