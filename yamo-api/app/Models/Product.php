<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/3/17
 * Time: 4:15 PM
 */

namespace App\Models;

use Helpers;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Product extends Model
{
    const PRODUCT_TYPE_SLUG_SHIPPING = 'transport';
    const PRODUCT_CATEGORY_CUSTOM = 'subscription-custom-box';
    const PRODUCT_FIRST_CATEGORY_CUSTOM = 'subscription-first-custom-box';
    const PRODUCT_CATEGORY_SUBSCRIPTION = 'subscription';
    const PRODUCT_CATEGORY_COOLING_BAG = 'tasche';
//    const PRODUCT_CATEGORY_BREIL = 'breil';
//    const PRODUCT_CATEGORY_POUCH = 'pouch';
    const PRODUCT_CATEGORY_BREIL = 'cups';
    const PRODUCT_CATEGORY_POUCH = 'pouches';
    const PRODUCT_CATEGORY_FOOD = 'food';
    const PRODUCT_CATEGORY_ALL = 'all';
    const PRODUCT_CATEGORY_BUNDLE = 'bundles';
    const PRODUCT_INGREDIENTS_FRUIT = 'fruchtig';
    const PRODUCT_INGREDIENTS_VEGETABLE = 'gemusig';
    const PRODUCT_CATEGORY_SINGLE_BOX = 'single-box';
    const PRODUCT_CATEGORY_SINGLE_BOX_CUSTOM = 'single-box-custom-box';
    const PRODUCT_ABO_SLUG_4_MONTHS = 'brei-abo-monate-4';
    const PRODUCT_ABO_SLUG_6_MONTHS = 'brei-abo-monate-6';

    const PRODUCT_CATEGORY_PROMO_GIFT = 'promo-gift';
    const PRODUCT_CATEGORY_FLYER = 'flyer';

    const IMAGE_LINK_BREIL = 'cups.png';
    const IMAGE_LINK_POUCHES = 'quetschies.png';
    const IMAGE_LINK_ALL = 'beides.png';
    const IMAGE_LINK_FOLDER = 'products';
    const IMAGE_THUMBNAIL_LINK_FOLDER = 'thumbnails';

    const SINGLE_BOX_IMAGE = 'yamobox-newv1.png';
    const SINGLE_BOX_IMAGE_THUMB = 'yamobox-newv1.png';
    const SUBSCRIPTION_BOX_IMAGE = 'yamobox-abonew.png';
    const SUBSCRIPTION_BOX_IMAGE_THUMB = 'yamobox-abonew.png';

    const TRANSLATION_PREFIX = 'PRODUCTS';
    const TRANSLATION_PREFIX_MEDIA = 'MEDIA';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('active', function (Builder $builder) {
            try {
                if(!empty(Helpers::country()))
                    $builder->whereHas('productRegions', function ($query) {
                        $query->where('active', 1)
                            ->whereHas('regions', function ($query) {
                                $query->where('country_id', Helpers::country()->id);
                            });
                    });
            } catch (\Throwable $ex)
            {
                Log::info($ex);
                app('sentry')->captureException($ex);
            }
        });
    }

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    protected $appends = ['name','category','details','weight','ingredients','price','visible','tax_percentage',
        'currency','shipping','shipping_tax', 'produce_type', 'items', 'product_name_color', 'information',
        'time_of_day', 'prep_type', 'ingredients_description', 'table_of_contents', 'image', 'image_thumbnail',
        'image_alt_tag_text', 'image_alt_tag_text','months', 'product_gradient_color'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','category_id','name_key', 'product_family', 'product_identifier_letter', 'product_identifier_number',
        'size', 'months', 'position'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['boxes'];

    protected $with = ['media','regions','categories'];
    /**
     * Get the regions for the product.
     */
    public function regions()
    {
        return $this->belongsToMany('App\Models\Region','products_regions', 'product_id', 'region_id');
    }

    /**
     * Get vat groups for the product.
     */
    public function vatGroups()
    {
        return $this->belongsToMany('App\Models\VatGroup','products_regions', 'product_id', 'vat_group_id');
    }

    /**
     * Get the items for the product.
     */
    public function boxes()
    {
        return $this->hasMany('App\Models\Box','product_id');
    }

    /**
     * Get product's category
     */
    public function categories()
    {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    /**
     * Get the ingredients for the product.
     */
    public function ingredientList()
    {
        return $this->belongsToMany('App\Models\Ingredient','products_ingredients');
    }

    /**
     * Get product regions for the order.
     */
    public function productRegions()
    {
        return $this->hasMany('App\Models\ProductRegion','product_id');
    }

    /**
     * Get media for the product
     */
    public function media()
    {
        return $this->belongsToMany('App\Models\Media','products_media');
    }

    public function getNameAttribute()
    {
        return Translation::trans(self::TRANSLATION_PREFIX . '.'.$this->name_key);
    }

    public function getCategoryAttribute()
    {
        return $this->categories->name_key;
    }

    public function getImageAttribute()
    {
        if(empty($this->media->first()))
            return env('S3_BASE').$this::IMAGE_LINK_FOLDER . '/' . env('DEFAULT_PRODUCT_IMAGE');
        return env('S3_BASE').$this::IMAGE_LINK_FOLDER . '/' . Helpers::get_locale() . '/' . Translation::trans(self::TRANSLATION_PREFIX_MEDIA . '.'.$this->media->first()->name_key.'.IMG');
    }

    public function getImageThumbnailAttribute()
    {
        if(empty($this->media->first()))
            return env('S3_BASE').$this::IMAGE_LINK_FOLDER . '/'. $this::IMAGE_THUMBNAIL_LINK_FOLDER . '/' . env('DEFAULT_PRODUCT_IMAGE');
        return env('S3_BASE').$this::IMAGE_LINK_FOLDER . '/' . Helpers::get_locale() . '/'. $this::IMAGE_THUMBNAIL_LINK_FOLDER . '/' . Translation::trans(self::TRANSLATION_PREFIX_MEDIA . '.'.$this->media->first()->name_key.'.IMG-THUMB');
    }

    public function getImageAltTagKeyAttribute()
    {
        if(empty($this->media->first()))
            return null;
        return self::TRANSLATION_PREFIX_MEDIA . '.'.Translation::trans($this->media->first()->name_key).'.ALT-TAGS';
    }

    public function getImageAltTagTextAttribute()
    {
        if(empty($this->media->first()))
            return null;
        return Translation::trans(self::TRANSLATION_PREFIX_MEDIA . '.'.$this->media->first()->name_key.'.ALT-TAGS');
    }

    public function getDetailsAttribute()
    {
        $details = Translation::trans(self::TRANSLATION_PREFIX . '.'.$this->name_key.'.CHARACTERISTIC');
        if(empty($details))
            return [];
        return json_decode($details);
    }

    public function getWeightAttribute()
    {
        if(empty($this->size))
            return null;
        return $this->size .'g';
    }

    public function getIngredientsAttribute()
    {
        $retValue = [];
        foreach ($this->ingredientList as $ingredient) {
            $retValue[] = Translation::trans($ingredient->name_key);
        }
        return $retValue;
    }

    public function getProduceTypeAttribute()
    {
        return Translation::trans(self::TRANSLATION_PREFIX . '.'.$this->name_key.'.PRODUCE-TYPE');
    }

    public function getProductNameColorAttribute()
    {
        return Translation::trans(self::TRANSLATION_PREFIX . '.'.$this->name_key.'.PRODUCT-NAME-COLOR');
    }

    public function getInformationAttribute()
    {
        return Translation::trans(self::TRANSLATION_PREFIX . '.'.$this->name_key.'.INFORMATION');
    }

    public function getTimeOfDayAttribute()
    {
        return Translation::trans(self::TRANSLATION_PREFIX . '.'.$this->name_key.'.TIME-OF-DAY');
    }

    public function getPrepTypeAttribute()
    {
        return Translation::trans(self::TRANSLATION_PREFIX . '.'.$this->name_key.'.PREP-TYPE');
    }

    public function getIngredientsDescriptionAttribute()
    {
        return Translation::trans(self::TRANSLATION_PREFIX . '.'.$this->name_key.'.INGREDIENTS-DESCRIPTION');
    }

    public function getTableOfContentsAttribute()
    {
        $tableOfContents = Translation::trans(self::TRANSLATION_PREFIX . '.'.$this->name_key.'.TABLE-OF-CONTENTS');
        if(empty($tableOfContents))
            return [];
        return json_decode($tableOfContents);
    }

    public function getCurrencyAttribute()
    {
        $country = Helpers::country();
        if(empty($country))
            return null;

        return $country->currency;
    }

    public $productRegionCache = [];

    public function getPriceAttribute($value)
    {
        $countryId = Helpers::country()->id;
        if(!array_key_exists($countryId,$this->productRegionCache)){
            $this->productRegionCache[Helpers::country()->id] = $this->productRegions()->whereHas('regions',function ($query) use($countryId) {
                $query->where('country_id', $countryId);
            })->first();
        }
        $productRegion = $this->productRegionCache[$countryId];

        if(!empty($productRegion))
            return $productRegion->price;

        return $value;
    }

    public function getVisibleAttribute($value)
    {
        $countryId = Helpers::country()->id;
        if(!array_key_exists($countryId,$this->productRegionCache)){
            $this->productRegionCache[Helpers::country()->id] = $this->productRegions()->whereHas('regions',function ($query) use($countryId) {
                $query->where('country_id', $countryId);
            })->first();
        }
        $productRegion = $this->productRegionCache[$countryId];
        if(!empty($productRegion))
            // it's product_visible cause visible-only is a predefined property for models
            return $productRegion->product_visible;
        return $value;
    }

    public function getTaxPercentageAttribute($value)
    {
        $productRegion = $this->productRegions()->whereHas('regions',function ($query) {
            $query->where('country_id', Helpers::country()->id);
        })->first();

        if(!empty($productRegion) && !$productRegion->vat_groups->isEmpty())
            return $productRegion->vat_groups->first()->value;

        return $value;
    }

    public function getShippingAttribute()
    {
        return '0.00';
    }

    public function getShippingTaxAttribute()
    {
        return '0.00';
    }

    public function getShippingTaxPercentageAttribute()
    {
        return '0.00';
    }

    public function getMonthsAttribute()
    {
        return $this->attributes['months'];
    }

    public function setItemsAttribute($value)
    {
        $this->attributes['items'] = $value;
    }

    /** Product gradient color array attribute
     * @return array
     */
    public function getProductGradientColorAttribute()
    {
        $colorOneKeyKey = self::TRANSLATION_PREFIX . ".".$this->name_key.".PRODUCT-GRADIENT-COLOR-ONE";
        $colorTwoKeyKey = self::TRANSLATION_PREFIX . ".".$this->name_key.".PRODUCT-GRADIENT-COLOR-TWO";
        return [
            'first' => (Translation::trans($colorOneKeyKey) === $colorOneKeyKey) ? null : Translation::trans($colorOneKeyKey),
            'second' => (Translation::trans($colorTwoKeyKey) === $colorTwoKeyKey) ? null : Translation::trans($colorTwoKeyKey)
        ];
    }

    public function getItemsAttribute($value)
    {
        try
        {
            if(is_null($value))
                $value = [];

            if($this->boxes->isEmpty())
                return $value;

            $box = $this->boxes->first();

            if (empty($box))
                return [];

            $retValue = $this->transformItems($box->items);
            if(is_null($retValue))
                return [];

            return $retValue;
        } catch (\Exception $e)
        {
            app('sentry')->captureException($e);
            return [];
        }
    }

    public function transformItems($data)
    {
        // $data is collection of BoxContent objects
        if($data->isEmpty())
            return [];
        $retValue = [];
        foreach ($data as $item) {
            $retValue[] = [
                "id" => $item->product->id,
                "name" => $item->product->name,
                "image" => $item->product->image,
                "image_thumbnail" => $item->product->image_thumbnail,
                "image_alt_tag_key" => $item->product->image_alt_tag_key,
                "image_alt_tag_text" => $item->product->image_alt_tag_text,
                "ingredients" => $item->product->ingredients,
                "quantity" => $item->quantity,
                "months" => $item->product->month,
                "product_name_color" => $item->product->product_name_color,
                "weight" => $item->product->weight
            ];
        }
        return $retValue;
    }

    public function transformItemsEmptyAllSecond($products, $data_custom=null)
    {
        if($products->isEmpty())
            return [];

        $retValue = [];
        $retSelectedValue = [];
        foreach ($products as $item) {
            $itemWasFound = false;
            $item_data = [
                "id" => $item->id,
                "name" => $item->name,
                "image" => $item->image,
                "image_thumbnail" => $item->image_thumbnail,
                "ingredients" => $item->ingredients,
                "quantity" => 0,
                "months" => $item->month,
                "product_name_color" => $item->product_name_color,
                "weight" => $item->weight,
                "position" => $item->position
            ];

            if (!empty($data_custom)) {
                foreach ($data_custom as $custom_item) {
                    if ($item['id'] == $custom_item['id']) {
                        $item_data['quantity'] = $custom_item['quantity'];
                        $itemWasFound = true;
                    }
                }
            }
            if($itemWasFound){
                $retSelectedValue[] = $item_data;
            } else{
                $retValue[] = $item_data;
            }
        }
        usort($retSelectedValue,
            function ($a, $b) {
                if($a['quantity']==$b['quantity']){
                    if($a['position']==$b['position']) return 0;
                    return $a['position'] > $b['position']?1:-1;
                }
                return $a['quantity'] < $b['quantity'];
            });
        usort($retValue,
            function ($a, $b) {
                if($a['position']==$b['position']) return 0;
                return $a['position'] > $b['position']?1:-1;
            });

        $finalRetValue = array_merge($retSelectedValue,$retValue);
        return $finalRetValue;
    }

    public function transformItemsEmptyAll($products, $data_custom=null)
    {
        if($products->isEmpty())
            return [];
        $retValue = [];
        foreach ($products as $item) {
                $item_data = [
                    "id" => $item->id,
                    "name" => $item->name,
                    "image" => $item->image,
                    "image_thumbnail" => $item->image_thumbnail,
                    "ingredients" => $item->ingredients,
                    "quantity" => 0,
                    "months" => $item->month,
                    "product_name_color" => $item->product_name_color,
                    "weight" => $item->weight
                ];

                if (!empty($data_custom)) {
                    foreach ($data_custom as $custom_item) {
                        if ($item['id'] == $custom_item['id']) {
                            $item_data['quantity'] = $custom_item['quantity'];
                        }
                    }
                }
                $retValue[] = $item_data;
        }
        return $retValue;
    }

    /**
     * Look for parent, if empty return itself
     * works only with one level
     * @return mixed
     */
    public function topParentCategoryName() : string
    {
        $default = 'none';
        if(empty($this->categories))
            return $default;

        if(empty($this->categories->parent))
            return $this->categories->name_key;

        if(empty($this->categories->parent->name_key))
            return $default;

        return $this->categories->parent->name_key;
    }

    /**
     * Get the relationships for the entity.
     *
     * @return array
     */
    public function getQueueableRelations()
    {
        // TODO: Implement getQueueableRelations() method.
    }
}