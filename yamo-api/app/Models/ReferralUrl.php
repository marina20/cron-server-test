<?php

namespace App\Models;

use App\Models\ReferralsUsed;
use App\Services\CreateOrder\CreateOrder;
use Auth;
use Illuminate\Database\Eloquent\Model;
use App\Jobs\SendReferreeCompletedEmailJob;
use App\Traits\FormatMoney;


class ReferralUrl extends Model {

    use FormatMoney;
    protected $table = 'referral_urls';
    protected $fillable = [
        'id', 'user_id', 'url','enabled','region_id','share_facebook','share_twitter','share_instagram','share_whatsapp'
    ];

    /**
     * If the order contained any wallet-discount, it will be nulled and the money will be moved to the wallet
     * This method is mostly used for order-cancle-events as it would be unfair to not give the money back if a user like to buy later.
     * @param $order
     * @return Order
     */
    public static function revertWallet($order){
        $user = User::find($order->user_id);
        $profile = $user->profile;
        if(!empty($order->wallet_discount)) {
            // TODO IMPORTANT: redo order-items!
            if ($order->order_total == "0.00" && $order->payment_method_id == "paypal" && $profile->wallet_amount_cents > 0.60) {
                $profile->wallet_amount_cents -= 0.60;
            }
            $order->discount = $order->discount - $order->wallet_discount;
            $order->discount_tax = $order->discount_tax - $order->wallet_discount_tax;
            $profile->wallet_amount_cents += $order->wallet_discount;
            $order->order_total = round($order->order_total + $order->wallet_discount,2);
            $order->order_tax = CreateOrder::calculateVAT($order->order_total,$order->items->first()->tax_rate);
            $order->order_total_without_tax = $order->order_total-$order->order_tax;
            $order->wallet_discount = 0;
            $order->wallet_discount_tax = 0;
            $profile->save();
            $order->save();
        }
        return $order;
    }

    private static function removeSpecialChars($input){
        if(empty($input)){
            return '';
        }
        return preg_replace('/[^a-zA-Z0-9]+/', '', $input);
    }

    private static function getNewReferralUrl($name){
        return self::removeSpecialChars(strtolower($name.substr(sha1(str_random(30)),0,8)));
    }


    private static function getRegion($user){
        $countryCode = self::beautifyCountry($user->profile->shipping_country);
        $country = Country::where(['iso_alpha_2' => $countryCode])->first();
        if(empty($country)){
            $country = \Helpers::country();
        }
        $region = Region::where(['country_id' => $country->id])->first();
        return $region;
    }

    private static function setUserCurrency($user,$country){
        if(empty($user->profile->wallet_amount_currency)){
            $profile = $user->profile;
            $profile->wallet_amount_currency = $country['currency'];
            $profile->save();
        }
    }

    private static function generateReferralCode($user){
        $foundCode = true;
        $code = self::getNewReferralUrl($user->profile->first_name);
        while($foundCode){
            $exist = ReferralUrl::where(['url' => $code])->first();
            if(!empty($exist) || empty($code)){
                $code = self::getNewReferralUrl($user->profile->first_name);
            } else {
                $foundCode = false;
            }
        }
        return $code;
    }

    private static function getUservoucher($user){
        // Check if user has already a url
        $userCheck = ReferralUrl::where(['user_id' => $user->id])->first();
        if(!empty($userCheck)){
            // Update url if special-chars included
            if(self::removeSpecialChars($userCheck->url)!=$userCheck->url){
                $userCheck->url = self::removeSpecialChars($userCheck->url);
                $userCheck->save();
            }
            if($userCheck->enabled){
                // If the user has already one which is enabled, we return it.
                return $userCheck;
            }
            // If the user has one, but it's not enabled, return null
            return null;
        } else {
            // Cause the force of methoding i need to add any weird value here to know when to skip return
            $fakeRefUrl = new ReferralUrl;
            $fakeRefUrl->url=-1;
            return $fakeRefUrl;
        }
    }
    /**
     *
     * @return ReferralUrl
     */
    public static function generateReferral($user=null):ReferralUrl{
        if(empty(Auth::check()) && empty($user)) {
            return null;
        }
            if(empty($user)){
                $user = Auth::user();
            }
            $uservoucher = self::getUservoucher($user);
            // Voucher was found but disabled, this is a blocked user.
            if(empty($uservoucher)){
                // Forward null
                return null;
            }
            // If voucher was found
            if($uservoucher->url!=-1){
                return $uservoucher;
            }
            $region = self::getRegion($user);
            self::setUserCurrency($user,$region->country);
            return ReferralUrl::create(['user_id' => $user->id, 'url' => self::generateReferralCode($user), 'region_id' => $region->id, 'enabled' => 1]);
    }

    public static function getWalletValue() {
        ['amount' => Auth::user()->wallet_amount_cents, 'currency' => Auth::user()->wallet_amount_currency];
    }

    public static function checkReferral(array $request,Order $order):array {
        $referralUrl = ReferralUrl::where(['url' => strtolower($request['ref_code'])])->where(['enabled' => 1])->first();
        if(empty($referralUrl)){
            return ['success' => false, 'order' => $order];
        }
        if($referralUrl->user_id===Auth::id()){
            //ReferralAttempts::create(['referrer_url_id' => $referralUrl->id,'order_id' => $order->id,'user_id' => Auth::id(),'status' => 'referral can not be applied on owner']);
            return ['success' => false, 'order' => $order];
        }
        $region = Region::where(['id' => $referralUrl->region_id])->first();
        // TODO Remove this on refactoring (not brave enough in front of release)
        $referralUrlCountry = Country::where(['id' => $region->country_id])->first();
        $refereeCountry = Country::where('content_code',app('translator')->getLocale())->first();
        $refererCountry = $referralUrl->region->country;

        if($refereeCountry->iso_alpha_2!==$refererCountry->iso_alpha_2){
            return ['success' => false, 'order' => $order];
        }
        $referralUse = ReferralsUsed::where(['referrer_url_id' => $referralUrl->id, 'user_id' => Auth::id()])->first();
        if(!empty($referralUse)) {
            return ['success' => false, 'order' => $order];
        }
        if(Auth::check() && !Auth::user()->isNew()){
            return ['success' => false, 'order' => $order];
        }
        $region = Region::where(['id' => $referralUrl->region_id])->first();

        if(((double)$order['order_total'])<$region['referree_reward_cents']){
            $order->discount = $order['order_total'];
            $order['order_total'] = 0;
            $order->order_total_without_tax = 0;
        }
        else {
            $order->discount = $region['referree_reward_cents'];
            $order['order_total'] = $order['order_total']-$region['referree_reward_cents'];
            $order->order_total_without_tax = $order['order_total_without_tax']-$region['referree_reward_cents'];
        }
        return ['success' => true, 'order' => $order];
    }


    /**
     * Hopefully this method will be removeable one day.
     * It's mostly for the profile-table, where the country is saved as german name
     * @param $countryCode german name
     * @return string iso_alpha_2-format
     */
    public static function beautifyCountry($countryCode){
        // TODO remove that ugly workaround, FE-change needed!!
        if($countryCode=="Schweiz"){
            $countryCode = "CH";
        }
        if($countryCode=="Deutschland"){
            $countryCode = "DE";
        }
        if($countryCode=="Österreich"){
            $countryCode = "AT";
        }
        return $countryCode;
    }

    /**
     * Input your order and get the modified back!
     * This method will take the money from the wallet and apply it to order - if there is money
     * If there was too much, the rest remains
     * @param Order $order
     * @return Order
     */
    public static function applyWallet(Order $order):Order{
        $profile = Profile::where(['user_id' => $order->user_id])->first();
        if(empty($profile)){
            $profile = Profile::where(['user_id' => Auth::id()])->first();
        }
        $wallet_total = $profile->wallet_amount_cents;
        $emptyReferralUrlForFormatMoney = new ReferralUrl;
        if($wallet_total==0){
            return $order;
        }
        if((float)$order->order_total < $wallet_total){


            // TODO tread also here order_items
            $order->wallet_discount = $emptyReferralUrlForFormatMoney->formatMoney((float)$order->order_total);
            //$order->wallet_discount_tax = CreateOrder::calculateVAT((float)$order->order_total);
            $profile->wallet_amount_cents = $profile->wallet_amount_cents - (float)$order->order_total;

            $order->order_total = "0.00";
            $order->order_total_without_tax = "0.00";
            $order->order_tax = "0.00";
            $taxRate = 0;
            foreach($order->items as $item){

                $taxRate = $item->tax_rate;
                $singleProductPercentage = $item->total / $item->qty;
                $singleItemDiscount = ($item->qty * $singleProductPercentage);
                $item->total_discount = $emptyReferralUrlForFormatMoney->formatMoney((float)$item->total_discount + $singleItemDiscount);
                $item->total_discount_tax = $emptyReferralUrlForFormatMoney->formatMoney(CreateOrder::calculateVAT((float)$item->total_discount, $item->tax_rate));
                $item->wallet_discount = $emptyReferralUrlForFormatMoney->formatMoney($singleItemDiscount);
                $item->wallet_discount_tax = $emptyReferralUrlForFormatMoney->formatMoney(CreateOrder::calculateVAT((float)$item->wallet_discount, $item->tax_rate));
                $item->total = $emptyReferralUrlForFormatMoney->formatMoney(0);
                $item->item_price = $emptyReferralUrlForFormatMoney->formatMoney(0);
                $item->total_tax = $emptyReferralUrlForFormatMoney->formatMoney(0);
                $item->save();
            }
            if(empty($order->discount)){
                $order->discount = $order->wallet_discount;
            } else {
                $order->discount = ((float)$order->wallet_discount + (float)$order->discount);
                $order->discount_tax = CreateOrder::calculateVAT($order->discount, $taxRate);
            }
            $order->wallet_discount_tax = CreateOrder::calculateVAT((float)$order->wallet_discount,$taxRate);
        } else {
            $singlePercent = $wallet_total / $order->order_total;
            $tax_rate = 0;
            foreach($order->items as $item){
                $itemTotal = $item->total;
                $tax_rate = $item->tax_rate;
                $item->wallet_discount = ($itemTotal * $singlePercent);
                $item->total_discount_tax = $emptyReferralUrlForFormatMoney->formatMoney(CreateOrder::calculateVAT((float)$item->total_discount + $item->wallet_discount,$item->tax_rate));
                $item->total_discount = $emptyReferralUrlForFormatMoney->formatMoney((float)$item->total_discount + $item->wallet_discount);
                $item->wallet_discount_tax = CreateOrder::calculateVAT($item->wallet_discount, $item->tax_rate);
                $item->total = $emptyReferralUrlForFormatMoney->formatMoney($item->total - $item->wallet_discount);
                $item->item_price = $item->total / $item->qty;
                $item->total_tax = CreateOrder::calculateVAT($item->total, $item->tax_rate);

                $item->save();
            }

            $order->order_total = $emptyReferralUrlForFormatMoney->formatMoney((float)$order->order_total - $wallet_total);
            $order->order_tax = $emptyReferralUrlForFormatMoney->formatMoney(CreateOrder::calculateVAT((float)$order->order_total,$tax_rate));
            $order->order_total_without_tax = ((float)$order->order_total-(float)$order->order_tax);
            $order->wallet_discount = $wallet_total;
            $order->wallet_discount_tax = CreateOrder::calculateVAT($wallet_total,$tax_rate);
            $order->discount = $emptyReferralUrlForFormatMoney->formatMoney((float)$order->discount + $wallet_total);

            $order->discount_tax = $emptyReferralUrlForFormatMoney->formatMoney(CreateOrder::calculateVAT((float)$order->discount,$tax_rate));
            $order->save();
            $profile->wallet_amount_cents = 0;
        }
        $profile->save();
        return $order;
    }

    /**
     * Apply a referral-code from request
     * It adds money to the referrer's wallet and add a discount to the order of the referree
     * @param array $request Request-object->toArray()
     * @param Order $order
     * @return array success, order
     */
    public static function applyReferral(array $request,Order $order):array {
        $emptyReferralUrlForFormatMoney = new ReferralUrl;
        $referralUrl = ReferralUrl::where(['url' => strtolower($request['ref_code'])])->where(['enabled' => 1])->first();
        if(empty($referralUrl)){
            ReferralAttempts::create(['referrer_url_id' => 0,'order_id' => $order->id,'user_id' => Auth::id(),'status' => 'referral '.strtolower($request['ref_code']).' not found']);
            return ['success' => false, 'order' => $order];
        }
        if(Order::STATUS_PENDING != $order->status){
            ReferralAttempts::create(['referrer_url_id' => $referralUrl->id,'order_id' => $order->id,'user_id' => Auth::id(),'status' => 'order '.strtolower($request['ref_code']).' is unpayed']);
            return ['success' => false, 'order' => $order];
        }
        if($referralUrl->user_id===Auth::id()){
            ReferralAttempts::create(['referrer_url_id' => $referralUrl->id,'order_id' => $order->id,'user_id' => Auth::id(),'status' => 'referral '.strtolower($request['ref_code']).' can not be applied on owner']);
            return ['success' => false, 'order' => $order];
        }
        $region = Region::where(['id' => $referralUrl->region_id])->first();
        $referralUrlCountry = Country::where(['id' => $region->country_id])->first();

        //$refereeCountry = Auth::user()->profile->shipping_country;
        $refereeCountry = Country::where('content_code',app('translator')->getLocale())->first();
        $refererCountry = $referralUrl->region->country;

        if($refereeCountry->iso_alpha_2!==$refererCountry->iso_alpha_2){
            ReferralAttempts::create(['referrer_url_id' => $referralUrl->id,'order_id' => $order->id,'user_id' => Auth::id(),'status' => 'country mismatch for '.strtolower($request['ref_code'])]);
            return ['success' => false, 'order' => $order];
        }
        $wasUsedAlready = ReferralsUsed::where(['user_id' => Auth::id()])->first();
        if(!empty($wasUsedAlready)){
            ReferralAttempts::create(['referrer_url_id' => $referralUrl->id,'order_id' => $order->id,'user_id' => Auth::id(),'status' => 'user already did apply a referral '.strtolower($request['ref_code'])]);
            return ['success' => false, 'order' => $order];
        }


        if(!Auth::user()->isNew()){
             ReferralAttempts::create(['referrer_url_id' => $referralUrl->id,'order_id' => $order->id,'user_id' => Auth::id(),'status' => 'user is not new']);
             return ['success' => false, 'order' => $order];
        }
        if(!empty($request['utm_source'])) {
            if(in_array($request['utm_source'], ['facebook','whatsapp','twitter','instagram'])) {
                $counterSrc = $request['utm_source'];
                $referralUrl['share_' . $counterSrc] = $referralUrl['share_' . $counterSrc] + 1;
            } else {
                $referralUrl['share_other'] = $referralUrl['share_other'] + 1;
            }
        } else {
            $referralUrl['share_none'] = $referralUrl['share_none'] + 1;
        }
        $referralUrl->save();
        $region = Region::where(['id' => $referralUrl->region_id])->first();
        $profile = Profile::firstOrCreate(['user_id' => $referralUrl->user_id]);
        $profile->wallet_amount_cents = $profile->wallet_amount_cents + $region['referrer_reward_cents'];
        $profile->wallet_amount_currency = $referralUrlCountry['currency'];
        $profile->save();
        if(((double)$order['order_total'])<$region['referree_reward_cents']){

            // TODO very rare situation, marketing would need to increase a single referree-amount over order-price
            // still needs being tested, but low prio
            $order->discount = $order['order_total'];
            $order['order_total'] = 0;
            $order['order_tax'] = 0;
            $order->order_total_without_tax = 0;
            foreach($order->items as $item){
                $taxRate = $item->tax_rate;
                $singleProductPercentage = $item->total / $item->qty;
                $singleItemDiscount = ($item->qty * $singleProductPercentage);
                //$item->total_discount = $referralUrl->formatMoney((float)$item->total_discount + $singleItemDiscount);
                //$item->total_discount_tax = $referralUrl->formatMoney(CreateOrder::calculateVAT((float)$item->total_discount, $item->tax_rate));
                if(empty($item->total_discount)){
                    $item->total_discount = $referralUrl->formatMoney($item->total * $singleProductPercentage);
                } else {
                    $item->total_discount = $referralUrl->formatMoney((float)$item->total_discount + ($item->total * $singleProductPercentage));
                }
                if(empty($item->total_discount_tax)) {
                    $item->total_discount_tax = $referralUrl->formatMoney(CreateOrder::calculateVAT(($item->total * $singleProductPercentage), $item->tax_rate));
                } else {
                    $item->total_discount_tax = $referralUrl->formatMoney((float)$item->total_discount_tax + CreateOrder::calculateVAT(($item->total * $singleProductPercentage), $item->tax_rate));
                }
                $item->total = $referralUrl->formatMoney(0);
                $item->total_tax = $referralUrl->formatMoney(0);
                $item->item_price = $referralUrl->formatMoney(0);
                $item->save();
            }
            $order->discount_tax = CreateOrder::calculateVAT((float)$order->discount, $item->tax_rate);
        }
        else {

            $discount = 0;
            $taxRate = 0;
            foreach($order->items as $item){
                $taxRate = $item->tax_rate;
                $singleProductPercentage = $region['referree_reward_cents'] / $order->order_total;
                if(empty($item->total_discount)){
                    $item->total_discount = $referralUrl->formatMoney($item->total * $singleProductPercentage);
                } else {
                    $item->total_discount = $referralUrl->formatMoney((float)$item->total_discount + ($item->total * $singleProductPercentage));
                }
                if(empty($item->total_discount_tax)) {
                    $item->total_discount_tax = $referralUrl->formatMoney(CreateOrder::calculateVAT(($item->total * $singleProductPercentage), $item->tax_rate));
                } else {
                    $item->total_discount_tax = $referralUrl->formatMoney((float)$item->total_discount_tax + CreateOrder::calculateVAT(($item->total * $singleProductPercentage), $item->tax_rate));
                }

                $item->total = $referralUrl->formatMoney($item->total - ($item->total * $singleProductPercentage));
                $item->total_tax = $referralUrl->formatMoney(CreateOrder::calculateVAT($item->total,$item->tax_rate));
                $item->item_price = $item->total / $item->qty;
                //$item->total_without_discount_tax = $item->total - $item->total_discount;
                $item->save();
            }
            if(empty($order->discount)){
                $order->discount = $referralUrl->formatMoney($region['referree_reward_cents']);
            } else {
                $order->discount =  $referralUrl->formatMoney((float)$order->discount + $region['referree_reward_cents']);
            }
            if(empty($order->discount_tax)){
                $order->discount_tax = $referralUrl->formatMoney(CreateOrder::calculateVAT($region['referree_reward_cents'],$taxRate));
            } else {
                $order->discount_tax = $referralUrl->formatMoney((float)$order->discount_tax + CreateOrder::calculateVAT($region['referree_reward_cents'],$taxRate));
            }
            $order['order_total'] = $referralUrl->formatMoney($order['order_total']-$region['referree_reward_cents']);
            $order['order_tax'] = CreateOrder::calculateVAT((float)$order['order_total'],$taxRate);
            $order['order_total_without_tax'] = $referralUrl->formatMoney($order['order_total']-$order['order_tax']);
        }
        // Send email
        dispatch(new SendReferreeCompletedEmailJob($referralUrl,$order));
        ReferralAttempts::create(['referrer_url_id' => $referralUrl->id,'order_id' => $order->id,'user_id' => Auth::id(),'status' => 'success']);
        ReferralsUsed::create(['referrer_url_id' => $referralUrl->id,'user_id' => Auth::id(), 'amount_referrer' => $region['referrer_reward_cents'], 'amount_referree' =>  $region['referree_reward_cents'],'currency' => $referralUrlCountry['currency'],'order_id' => $order->id]);

        // utm_source is defined in wikipedia

        return ['success' => true, 'order' => $order];
    }


    public function user()
    {
        return $this->BelongsTo('App\Models\User');
    }

    public function region()
    {
        return $this->BelongsTo('App\Models\Region');
    }
}
