<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 10/20/17
 * Time: 9:58 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryFrequency extends Model
{
    protected $fillable = [
        'id', 'interval', 'period','icon'
    ];
}