<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    protected $fillable = ['key-key','de-de','en-de','de-ch','en-ch','de-at','en-at','en-uk','fr-fr','en-fr','nl-nl','en-nl','type','notes'];
    protected $table = 'translations';

    private static $translationCache = [];
    /**
     * Translate a single word by a unique key
     * @param string $key
     * @return string translation, if not found empty (production) or key
     */
    public static function trans(string $key,$forcedLocale=null):string{
        $translatedText = "";
        if (!app()->environment('production')) {
            $translatedText = $key;
        }
        $row = null;
        if(!array_key_exists($key,self::$translationCache)){
            self::$translationCache[$key] = Translation::where(["key-key"=>$key])->first();
        }
        $row = self::$translationCache[$key];
        if(!empty($row)) {
            $alteredLocale = app('translator')->getLocale();
            if(!is_null($forcedLocale)){
                $alteredLocale = $forcedLocale;
            }
            if (!empty($row[$alteredLocale])) {
                $translatedText = $row[$alteredLocale];
            } else {
                $translatedText = $row['de-de'];
            }
        }
        return $translatedText;
    }
}
