<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ReferralsUsed extends Model {

    protected $table = 'referrals_used';
    protected $fillable = [
        'id', 'referrer_url_id', 'amount_referrer','amount_referree','currency','order_id','user_id'
    ];
    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id', 'id');
    }
}
