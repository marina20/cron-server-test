<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-08-13
 * Time: 13:19
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'regions';
    // protected $with = ['country'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['region','country_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get the country that owns the region.
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }
}