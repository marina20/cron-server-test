<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class UTMTracker extends Model
{

    protected $table = 'utm_tracking';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'utm_source', 'utm_medium', 'utm_term','utm_content','reference_id','type'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

}
