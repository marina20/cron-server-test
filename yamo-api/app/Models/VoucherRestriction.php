<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class VoucherRestriction extends Model {

    protected $table = 'voucher_restrictions';
    protected $fillable = [
        'id', 'voucher_id', 'restriction_type_id','restriction_payload','start_at','end_at'
    ];
    public function type()
    {
        return $this->belongsTo('App\Models\VoucherRestrictionType', 'restriction_type_id', 'id');
    }
    public function voucher()
    {
        return $this->belongsTo('App\Models\Voucher', 'voucher_id', 'id');
    }
}
