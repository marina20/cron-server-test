<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 10/20/17
 * Time: 9:58 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    const STATUS_SUBSCRIPTION = 'subscription';
    const STATUS_ACTIVE = 'active';
    const STATUS_PENDING = 'pending';
    const STATUS_CANCELLED = 'cancelled';
    const SCHEDULE_NEXT_PAYMENT_DAYS = 4;
    const SUBSCRIPTION_DELIVERY_FREQUENCY_PERIOD = 'week';

    const STATUS_PAUSED = 'paused';
    const ENABLED_DELIVERY_FREQUENCIES = [1,2,4];
    const DEFAULT_DELIVERY_FREQUENCY = 2;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'subscriptions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['order_id', 'billing_interval'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $appends = ['child_birthday','delivery_date'];

    /**
     * Get the items for the order.
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order', 'order_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function deliveryFrequency()
    {
        return $this->belongsTo('App\Models\DeliveryFrequency', 'delivery_frequency_id');
    }

    public function getChildBirthdayAttribute()
    {
        if(!empty($this->order->child_birthday)) {
            return $this->order->child_birthday->format('Y-m-d H:m:i');
        }
        return null;
    }

    public function getDeliveryDateAttribute()
    {
        if(empty($this->order)) {
            return null;
        }
        return $this->order->delivery_date->format('Y-m-d H:m:i');
    }
}