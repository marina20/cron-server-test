<?php


namespace App\Models;


class VoucherType
{
    const TYPE_SYSTEM = 'system';
    const TYPE_LOYALTY = 'loyalty';
    const TYPE_PUBLIC = 'public';

    public static function all()
    {
        return [
            self::TYPE_PUBLIC,
            self::TYPE_LOYALTY
        ];
    }
}