<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 10/25/17
 * Time: 4:12 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCoupon extends Model
{
    /**
 * The table associated with the model.
 *
 * @var string
 */
    protected $table = 'users_coupons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}