<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class AcEventOccurred extends Model
{

    protected $table = 'ac_event_occurred';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_name', 'email', 'timestamp'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

}
