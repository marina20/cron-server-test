<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 10/20/17
 * Time: 2:34 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    const ITEM_TYPE_LINE = 'line_item';
    const ITEM_TYPE_SHIPPING = 'shipping';
    const ITEM_TYPE_SHIPPING_NAME = 'Gekühlter Transport';

    public $notSave = false;
    public static function boot()
    {
        parent::boot();

        static::saving(function ($someModel) {
            if($someModel->notSave) {
                return false;
            }
        });
    }
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['wallet_discount'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get the order that owns the item.
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}