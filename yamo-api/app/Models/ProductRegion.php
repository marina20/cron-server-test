<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-08-13
 * Time: 15:33
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductRegion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products_regions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_id','region_id','vat_group_id','active_start_date','active_end_date','active','price','out_of_stock'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get regions for the ProductRegion.
     */
    public function regions()
    {
        return $this->hasMany('App\Models\Region','id','region_id');
    }

    /**
     * Get vat groups for the ProductRegion.
     */
    public function vat_groups()
    {
        return $this->hasMany('App\Models\VatGroup','id','vat_group_id');
    }
}