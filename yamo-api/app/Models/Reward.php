<?php
/**
 * Created by PhpStorm.
 * User: milenabasaragin
 * Date: 11/23/18
 * Time: 12:10 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reward extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'rewards';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get the order that owns the item.
     */
    public function advocate()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function referral()
    {
        return $this->belongsTo('App\Models\Order');
    }
}
