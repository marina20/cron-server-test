<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class VoucherAttempts extends Model
{

    protected $table = 'voucher_attempts';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'voucher_id', 'user_id','order_id','status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

}
