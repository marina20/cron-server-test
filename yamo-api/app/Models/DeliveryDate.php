<?php
/**
 * Created by PhpStorm.
 * User: milenabasaragin
 * Date: 2019-05-29
 * Time: 10:17
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class DeliveryDate extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'delivery_dates';
    protected $fillable = [
        'id', 'delivery_date', 'country_id','disabled','export_date','cut_off_date','alternate_date'
    ];
    public static function getNextDeliveryDate($currentOrderDeliveryDate,$country_id){
        $deliveryDate = DeliveryDate::where('delivery_date','>=',$currentOrderDeliveryDate)->where(['country_id'=>$country_id])->first();
        $nextDeliveryDate = $deliveryDate->delivery_date;
        $originalDeliveryDate = null;
        if($deliveryDate->disabled){
            $nextDeliveryDate = $deliveryDate->alternate_date;
            $originalDeliveryDate = $deliveryDate->delivery_date;
        }
        return [$nextDeliveryDate,$originalDeliveryDate];
    }
}