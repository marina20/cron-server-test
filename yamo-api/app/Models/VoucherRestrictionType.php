<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class VoucherRestrictionType extends Model {

    protected $table = 'voucher_restriction_types';
    protected $fillable = [
        'id', 'type'
    ];

}
