<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 11/27/17
 * Time: 3:54 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    const DEFAULT_CURRENCY = 'CHF';
    const COUNTRY_GERMANY = 'DE';
    const COUNTRY_SWITZERLAND = 'CH';
    const COUNTRY_AUSTRIA = 'AT';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'countries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'content_code',
        'language',
        'currency',
        'iso_alpha_2',
        'iso_alpha_3',
        'iso_alpha_numeric'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get the shippings for the country.
     */
    public function shippings()
    {
        return $this->hasMany('App\Models\Shipping');
    }

    /**
     * Get the products for the country.
     */
    public function products()
    {
        return $this->belongsToMany('App\Models\Product','prices', 'country_id', 'product_id')->withPivot('value', 'tax_percentage', 'tax')->withTimestamps();
    }

    /**
     * Get the hellofresh boxes for the country.
     */
    public function hellofresh_totals()
    {
        return $this->hasMany('App\Models\HellofreshCountry');
    }

    /**
     * Get regions for the country.
     */
    public function regions()
    {
        return $this->hasMany('App\Models\Region');
    }
}