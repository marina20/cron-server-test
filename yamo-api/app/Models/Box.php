<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 5/10/18
 * Time: 9:37 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    const TYPE_SUBSCRIPTION = 'subscription';
    const TYPE_CUSTOM = 'custom';
    const STATUS_ACTIVE = 'active';
    const STATUS_DELETED = 'deleted';

    protected $with = ['items'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'boxes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type','status','original_box_id','product_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get the items for the box.
     */
    public function items()
    {
        return $this->hasMany('App\Models\BoxContent');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}