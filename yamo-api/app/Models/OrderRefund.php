<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 11/17/17
 * Time: 3:40 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderRefund extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'order_refunds';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get the order that owns the refund.
     */
    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }
}