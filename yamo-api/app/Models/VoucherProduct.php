<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class VoucherProduct extends Model {

    protected $table = 'voucher_products';
    protected $fillable = [
        'id', 'voucher_id', 'product_id'
    ];
    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }
    public function voucher()
    {
        return $this->belongsTo('App\Models\Voucher', 'voucher_id', 'id');
    }
}
