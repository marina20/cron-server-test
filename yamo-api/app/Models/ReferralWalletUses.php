<?php

namespace App\Models;

use App\Services\CreateOrder\CreateOrder;
use Auth;
use Illuminate\Database\Eloquent\Model;
use App\Jobs\SendReferreeCompletedEmailJob;

class ReferralWalletUses extends Model {

    protected $table = 'referral_wallet_uses';
    protected $fillable = [
        'id', 'user_id', 'order_id','wallet_discount','wallet_discount_tax'
    ];


    public function user()
    {
        return $this->BelongsTo('App\Models\User');
    }

    public function order()
    {
        return $this->BelongsTo('App\Models\Order');
    }
}
