<?php

namespace App\Models;

use App\Services\Adyen\Notification;
use App\Services\OrderService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class MfgRequest extends Model
{
    const CARD_NUMBER_REQUEST_ID = '1';
    const FINANCIAL_REQUEST_ID = '2';
    const CONFIRMATION_REQUEST_ID = '3';
    const FINANCIAL_CREDIT_REQUEST_ID = '4';
    const FINANCIAL_CREDIT_REQUEST = 'Financial Request Credit';
    const REFUND_RESPONSE_CODE = '00';
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mfg_request';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function split()
    {
        try {
            $request = simplexml_load_string($this->raw_request);
            $response = simplexml_load_string($this->raw_response);
            switch ($this->mfg_request_type_id) {
                case self::CARD_NUMBER_REQUEST_ID:
                    $this->external_reference = $request->externalReference;
                    $this->ip_address = $request->orderIpAddress;
                    $this->gender = $request->gender;
                    $this->first_name = $request->firstName;
                    $this->last_name = $request->lastName;
                    $this->street = $request->street;
                    $this->city = $request->city;
                    $this->zip = $request->zip;
                    $this->country = $request->country;
                    $this->language = $request->language;
                    $this->email = $request->email;
                    $this->birthdate = $request->birthdate;
                    $this->merchant_id = $request->merchantId;
                    $this->filial_id = $request->filialId;
                    $this->terminal_id = $request->terminalId;
                    $this->amount = $request->amount/100;
                    $this->currency_code = $request->currencyCode;
                    $this->available_credit = $response->availableCredit/100;
                    $this->maximal_credit = $response->maximalCredit/100;
                    $this->credit_refusal_reason = $response->creditRefusalReason;
                    $this->card_number = $response->cardNumber;
                    $this->response_code = $response->responseCode;
                    if(!empty($response->acceptanceInfo)) {
                        $this->acceptance_info = $response->acceptanceInfo;
                    }

                    break;
                case self::FINANCIAL_REQUEST_ID:
                    $this->card_number = $request->cardNumber;
                    $this->request_date = Carbon::parse($request->RequestDate)->toDateTimeString();
                    $this->transaction_type = $request->TransactionType;
                    $this->currency = $request->Currency;
                    $this->amount = $request->Amount/100;
                    $this->external_reference = $request->ExternalReference;
                    $this->merchant_id = $request->MerchantId;
                    $this->filial_id = $request->FilialId;
                    $this->terminal_id = $request->TerminalId;
                    $this->response_code = $response->ResponseCode;
                    $this->response_date = Carbon::parse($response->ResponseDate)->toDateTimeString();
                    $this->authorization_code = $response->AuthorizationCode;
                    $this->currency = $response->Currency;
                    $this->balance = $response->Balance/100;
                    $this->expiration_date = Carbon::parse($response->ExpirationDate)->toDateTimeString();
                    break;
                case self::CONFIRMATION_REQUEST_ID:
                    if ($request->Conversation && $request->Conversation->FinancialRequest) {
                        $conversation = $request->Conversation->FinancialRequest;
                        $this->request_date = Carbon::parse($conversation->RequestDate)->toDateTimeString();
                        $this->transaction_type = $conversation->TransactionType;
                        $this->currency = $conversation->Currency;
                        $this->amount = $conversation->Amount/100;
                        $this->external_reference = $conversation->ExternalReference;
                        $this->merchant_id = $conversation->MerchantId;
                        $this->filial_id = $conversation->FilialId;
                        $this->terminal_id = $conversation->TerminalId;
                    }
                    if ($request->Conversation && $request->Conversation->Response) {
                        $response_previous = $request->Conversation->Response;
                        $this->response_code = $response_previous->ResponseCode;
                        $this->response_date = Carbon::parse($response_previous->ResponseDate)->toDateTimeString();
                        $this->authorization_code = $response_previous->AuthorizationCode;
                        $this->currency = $response_previous->Currency;
                        $this->balance = $response_previous->Balance/100;
                        $this->expiration_date = Carbon::parse($response_previous->ExpirationDate)->toDateTimeString();
                    }
                    $this->response_date = Carbon::parse($response->ResponseDate)->toDateTimeString();
                    $card_statistics = $response->CardStatistics;
                    if ($card_statistics) {
                        $this->total = $card_statistics->Total['amount']/100;
                        $this->purchase = $card_statistics->Purchase['amount']/100;
                        $this->credit = $card_statistics->Credit['amount']/100;
                        $this->reversal = $card_statistics->Reversal['amount']/100;
                    }
                    break;
                case self::FINANCIAL_CREDIT_REQUEST_ID:
                    $this->card_number = $request->cardNumber;
                    $this->request_date = Carbon::parse($request->RequestDate)->toDateTimeString();
                    $this->transaction_type = $request->TransactionType;
                    $this->currency = $request->Currency;
                    $this->amount = $request->Amount/100;
                    $this->external_reference = $request->ExternalReference;
                    $this->merchant_id = $request->MerchantId;
                    $this->filial_id = $request->FilialId;
                    $this->terminal_id = $request->TerminalId;
                    $this->response_code = $response->ResponseCode;
                    $this->response_date = Carbon::parse($response->ResponseDate)->toDateTimeString();
                    $this->authorization_code = $response->AuthorizationCode;
                    $this->balance = $response->Balance/100;
                    $this->expiration_date = Carbon::parse($response->ExpirationDate)->toDateTimeString();

                    if (reset($this->response_code) === self::REFUND_RESPONSE_CODE) {
                        $orderRefund = new OrderRefund();
                        $orderRefund->mfg_request_id = $this->id;
                        $orderRefund->order_id = $this->order_id;
                        $orderRefund->live_environment = Notification::TRUE_AS_A_STRING;
                        $orderRefund->event_date = Carbon::parse($response->ResponseDate)->toDateTimeString();
                        $orderRefund->event_code = self::FINANCIAL_CREDIT_REQUEST;
                        $orderRefund->success = Notification::TRUE_AS_A_STRING;
                        $orderRefund->value = $this->amount;
                        $orderRefund->currency = $this->currency;
                        $orderRefund->save();
                    }

                    OrderService::findAndCancelOrderDueToRefund($this->order_id, self::FINANCIAL_CREDIT_REQUEST);
                    break;
            }
            $this->save();

        } catch (\Exception $exception){
            Log::info('Split mfg request for id: ' . $this->id . ' error: ' . $exception->getMessage());
        }
    }

}