<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/7/17
 * Time: 4:37 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    protected $imageLinkFolder = 'testimonials';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function getImageAttribute($value)
    {
        return env('S3_BASE').$this->imageLinkFolder.'/'.$value;
    }
}