<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-06-03
 * Time: 20:08
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrackingWizard extends Model
{
    protected $table = 'tracking_wizard';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid','user_id','step_identifier','data'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}