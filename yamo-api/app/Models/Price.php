<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 11/27/17
 * Time: 3:54 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'prices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get the country that owns the price.
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    /**
     * Get the product that owns the price.
     */
    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }
}