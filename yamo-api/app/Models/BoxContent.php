<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 5/10/18
 * Time: 9:40 AM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class BoxContent extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'box_content';
    protected $with = ['product'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_id','box_id','quantity'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    protected $appends = ['ingredients'];

    /**
     * Get the orders for the coupon.
     */
    public function box()
    {
        return $this->belongsTo('App\Models\Box');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function getIngredientsAttribute()
    {
       if(isset($this->product->ingredients))
           return $this->product->ingredients;

        return null;
    }
}