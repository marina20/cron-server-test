<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 10/25/17
 * Time: 4:12 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    const TYPE_PERCENT = 'percent';
    const TYPE_FIXED = 'fixed';
    const TYPE_AUTOGENERATED = 'autogenerated';
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'coupons';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get the orders for the coupon.
     */
    public function order()
    {
        return $this->hasOne('App\Models\Order');
    }
}