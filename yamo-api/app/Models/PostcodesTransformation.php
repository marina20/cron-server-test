<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostcodesTransformation extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'postcodes_transformations';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['postcode_id','alias'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

}