<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class AcEventLookup extends Model
{

    protected $table = 'ac_event_lookups';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_name', 'tags_add','tags_remove'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

}
