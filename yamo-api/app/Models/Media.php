<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-08-14
 * Time: 23:15
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'media';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name_key'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Get products that have this media
     */
    public function advocate()
    {
        return $this->belongsToMany('App\Models\Product','products_images');
    }
}