<?php

namespace App\Listeners;

use App\Events\OrderSaving as OrderSavingEvent;
use App\Services\ZipCity\ZipCityUpdateFactory;

class OrderSaving
{

    protected $zipCityUpdateFactory;

    /**
     * Create the event listener.
     * OrderSaving constructor.
     * @param ZipCityUpdateFactory $zipCityUpdateFactory
     * @return void
     */
    public function __construct(ZipCityUpdateFactory $zipCityUpdateFactory)
    {
        $this->zipCityUpdateFactory = $zipCityUpdateFactory;
    }

    /**
     * Handle the event.
     *
     * @param  OrderSavingEvent  $event
     * @return void
     */
    public function handle(OrderSavingEvent $event)
    {
        if($event->order->notSave) {
            return false;
        }
        // Call update factory only if value has changed
        if($event->order->isDirty('billing_postcode_id'))
        {
            $event->order = $this->zipCityUpdateFactory->update($event->order, ZipCityUpdateFactory::ZIP_CITY_TYPE_BILLING);
        }

        // Call update factory only if value has changed
        if($event->order->isDirty('shipping_postcode_id'))
        {
            $event->order = $this->zipCityUpdateFactory->update($event->order, ZipCityUpdateFactory::ZIP_CITY_TYPE_SHIPPING);
        }
    }
}
