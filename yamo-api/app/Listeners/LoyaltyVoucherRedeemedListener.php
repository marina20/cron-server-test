<?php


namespace App\Listeners;

use App\Events\LoyaltyVoucherRedeemedEvent;
use App\Models\LoyaltyTracker;
use App\Models\OrderItem;
use App\Models\Voucher;
use App\Models\VoucherRedemption;
use App\Models\Order;
use App\Models\User;
use App\Services\CustomBox\CreateContent;
use App\Services\OrderService;
use Illuminate\Support\Collection;
use Helpers;

class LoyaltyVoucherRedeemedListener
{
    protected $createContent;

    public function __construct(CreateContent $createContent)
    {
        $this->createContent = $createContent;
    }
    /**
     * Handle the event.
     *
     * @param  LoyaltyVoucherRedeemedEvent  $event
     * @throws \Throwable
     */
    public function handle(LoyaltyVoucherRedeemedEvent $event)
    {

        $voucherRedemption = $event->voucherRedemption;
        // based on redemption
        $voucher = $voucherRedemption->voucher;
        $order = $voucherRedemption->order;

        Helpers::set_locale($order->getCountryCode($order->shipping_country));

        // it's imperative to first set loyalty trackers to rollback so loyalty voucher won't be called if ever
        // it was created for the same order with same voucher
        // isn't this bullshit design? everything is so coupled and if we want to force loyalty voucher on this order
        // it will be automatically avoided

        // we need to rollback all others with same user and voucher id but different order
        $this->rollbackLoyaltyTracker($order, $voucher);

        $this->confirmLoyaltyTracker($order, $voucher);


        $otherUnconfirmedRedemptions = $this->getUnconfirmedRedemptions($voucher, $voucherRedemption->user);

        // this will always be a collection
        if($otherUnconfirmedRedemptions->isNotEmpty()) 
        {
            $productsToRemove = $this->getProductsToRemove($voucher);

            foreach ($otherUnconfirmedRedemptions as $unconfirmedRedemption) {
                $products = [];
                // remove all items from order items if neccessary which are in voucher
                // wallet discount must stay the same so we just need to remove loyalty and not to touch other vouchers and discounts
                foreach ($unconfirmedRedemption->order->items as $orderItem) {
                    // check and transform if necessary
                    $orderItem = $this->checkLoyaltyOrderItem($orderItem, $productsToRemove);

                    if (empty($orderItem))
                        continue;

                    $products[] = [
                        'id' => $orderItem->product_id,
                        'quantity' => $orderItem->qty
                    ];
                }
                foreach($unconfirmedRedemption->order->items as $item){
                    $item->delete();
                }
                if(empty($products))
                    $products = $this->recreateProductsFromTheBox($unconfirmedRedemption->order);

                // Let's use recreate content where we removed loyalty voucher from order
                $this->createContent->recreateOrder($unconfirmedRedemption->order, $products);

                OrderService::generateSKU($unconfirmedRedemption->order);
            }
        }
    }

    /**
     * Recreate products array from box items
     * @TODO move to OrderRepository when it's created (or other appropriate place)
     * @param Order $order
     * @return array
     */
    private function recreateProductsFromTheBox(Order $order) : array
    {
        $products = [];
        if(empty($order->box->items))
            return $products;
        foreach ($order->box->items as $item)
        {
            $products[] = [
                'id' => $item->product_id,
                'quantity' => $item->quantity
            ];
        }
        return $products;
    }

    /**
     * Not only check but also remove whole item or reduce quantity if needed
     * @param OrderItem $orderItem
     * @param array $productsToRemove
     * @return OrderItem|null
     * @throws \Exception
     */
    private function checkLoyaltyOrderItem(OrderItem $orderItem, array $productsToRemove) : ?OrderItem
    {
        if (false !== array_search($orderItem->product_id, array_column($productsToRemove, 'id')) &&
            empty((float) $orderItem->item_price)) {
            foreach ($productsToRemove as $remove) {
                if ($remove['id'] == $orderItem->product_id) {
                    return $this->removeLoyaltyProductFromOrderItem($orderItem, $remove['quantity']);
                }
            }
        }
        return $orderItem;
    }

    /**
     * Get an array of products to remove from order items
     * @param Voucher $voucher
     * @return array
     */
    private function getProductsToRemove(Voucher $voucher)
    {
        $productsToRemove = [];
        if(in_array($voucher->value_type,[Voucher::PRODUCT, Voucher::PRODUCTAMOUNT, Voucher::PRODUCTPERCENT]))
        {
            foreach ($voucher->voucherProducts as $voucherProduct) {
                $productsToRemove[] = [
                    'id' => $voucherProduct->product->id,
                    'quantity' => $voucherProduct->qty
                ];
            }
        }
        return $productsToRemove;
    }

    /**
     * Rollback all loyalty trackers with this order, user and id
     * @param Order $order
     * @param Voucher $voucher
     */
    private function rollbackLoyaltyTracker(Order $order, Voucher $voucher) : void
    {
         LoyaltyTracker::where('user_id',$order->user_id)
            ->where('voucher_id',$voucher->id)
            ->where('order_id','!=',$order->id)
            ->whereHas('order', function ($query) {
                $query->where('status',Order::STATUS_PENDING);
            })
            ->update(['status' => LoyaltyTracker::LOYALTY_TRACKER_STATUS_ROLLBACK]);
    }

    /**
     * Confirm loyalty tracker
     * @param Order $order
     * @param Voucher $voucher
     */
    private function confirmLoyaltyTracker(Order $order, Voucher $voucher) : void
    {
         LoyaltyTracker::where('order_id',$order->id)
            ->where('voucher_id',$voucher->id)
            ->update(['status' => LoyaltyTracker::LOYALTY_TRACKER_STATUS_CONFIRMED]);
    }

    /**
     * Get unconfirmed redemptions
     * @param Voucher $voucher
     * @param User $user
     * @return Collection
     */
    private function getUnconfirmedRedemptions(Voucher $voucher, User $user) : Collection
    {
         return VoucherRedemption::where('voucher_id',$voucher->id)
            ->where('confirmed',0)
            ->where('user_id',$user->id)
            ->whereHas('order', function ($query) {
                $query->where('status',Order::STATUS_PENDING);
            })
            ->get();
    }

    /**
     * @param OrderItem $orderItem
     * @param int $loyaltyQuantity
     * @return OrderItem|null
     * @throws \Exception
     */
    private function removeLoyaltyProductFromOrderItem(OrderItem $orderItem, int $loyaltyQuantity) : ?OrderItem
    {
        if ($orderItem->qty == $loyaltyQuantity)
        {
            $orderItem->delete();
            return null;
        }
        else if ($orderItem->qty > $loyaltyQuantity)
        {
            $orderItem->qty -= $loyaltyQuantity;
            $orderItem->save();
        }
        return $orderItem;
    }
}