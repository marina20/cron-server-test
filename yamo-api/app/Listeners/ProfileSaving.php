<?php

namespace App\Listeners;

use App\Events\ProfileSaving as ProfileSavingEvent;
use App\Services\ZipCity\ZipCityUpdateFactory;

class ProfileSaving
{

    protected $zipCityUpdateFactory;

    /**
     * Create the event listener.
     * ProfileSaving constructor.
     * @param ZipCityUpdateFactory $zipCityUpdateFactory
     * @return void
     */
    public function __construct(ZipCityUpdateFactory $zipCityUpdateFactory)
    {
        $this->zipCityUpdateFactory = $zipCityUpdateFactory;
    }

    /**
     * Handle the event.
     *
     * @param  ProfileSavingEvent  $event
     * @return void
     */
    public function handle(ProfileSavingEvent $event)
    {
        // Call update factory only if value has changed
        if($event->profile->isDirty('billing_postcode_id'))
        {
            $event->profile = $this->zipCityUpdateFactory->update($event->profile,ZipCityUpdateFactory::ZIP_CITY_TYPE_BILLING);
        }

        // Call update factory only if value has changed
        if($event->profile->isDirty('shipping_postcode_id'))
        {
            $event->profile = $this->zipCityUpdateFactory->update($event->profile,ZipCityUpdateFactory::ZIP_CITY_TYPE_SHIPPING);
        }
    }
}
