<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 12/28/17
 * Time: 4:02 PM
 */

use App\Models\Country;
use App\Models\Product;
use Illuminate\Support\Arr;
use Carbon\Carbon;

class Helpers
{
    const LOCALE_DE = 'de-de';
    const LOCALE_CH = 'de-ch';
    const LOCALE_AT = 'de-at';
    const LOCALE_DOMAIN_DE = 'de';
    const LOCALE_DOMAIN_CH = 'ch';
    const LOCALE_DOMAIN_AT = 'at';

    const TIMEZONE_ZURICH = 'Europe/Zurich';
    const TIMEZONE_UTC = 'UTC';

    public static $countriesCache = [];
    public static $currenciesCache = [];

    public static function get_locale()
    {
        return app('translator')->getLocale();
    }

    public static function app_url()
    {
        switch(Helpers::get_locale())
        {
            case Helpers::LOCALE_DE:
                return env('APP_URL_DE');
                break;
            case Helpers::LOCALE_CH:
                return env('APP_URL_CH');
                break;
            case Helpers::LOCALE_AT:
                return env('APP_URL_AT');
                break;
        }
    }

    public static function base_url()
    {
        return env('APP_URL');
    }

    public static function adyen_rurl()
    {
        return Helpers::app_url() . env('ADYEN_RURL');
    }

    public static function invoice_rurl()
    {
        return Helpers::app_url() . env('INVOICE_RURL');
    }

    public static function reset_password_url()
    {
        return Helpers::app_url() . env('RESET_PASSWORD_URI');
    }

    public static function website_url()
    {
        return rtrim(ltrim(env('APP_URL'), 'https://'), '/');
    }

    public static function default_email()
    {
        /*
        $first_part_of_email_address = explode('.',env('MAIL_FROM_ADDRESS'));
        $email_without_domain = reset($first_part_of_email_address);
        $email_without_domain = $email_without_domain . '.';
        switch(Helpers::get_locale())
        {
            case Helpers::LOCALE_DE:
                return $email_without_domain . Helpers::LOCALE_DOMAIN_DE;
                break;
            case Helpers::LOCALE_CH:
                return $email_without_domain . Helpers::LOCALE_DOMAIN_CH;
                break;
            case Helpers::LOCALE_AT:
                return $email_without_domain . Helpers::LOCALE_DOMAIN_AT;
                break;
        }*/
        return env('MAIL_FROM_ADDRESS');
    }

    public static function set_locale($country)
    {
        switch(strtolower($country))
        {
            case Helpers::LOCALE_DOMAIN_DE:
                app('translator')->setLocale(Helpers::LOCALE_DE);
                break;
            case Helpers::LOCALE_DOMAIN_CH:
                app('translator')->setLocale(Helpers::LOCALE_CH);
                break;
            case Helpers::LOCALE_DOMAIN_AT:
                app('translator')->setLocale(Helpers::LOCALE_AT);
                break;
        }
    }

    public static function split_street($street){
        $address = $street;
        $matches = array();

        $regex = '
           /\A\s*
           (?: #########################################################################
               # Option A: [<Addition to address 1>] <House number> <Street name>      #
               # [<Addition to address 2>]                                             #
               #########################################################################
               (?:(?P<A_Addition_to_address_1>.*?),\s*)? # Addition to address 1
           (?:No\.\s*)?
               (?P<A_House_number>\pN+[a-zA-Z]?(?:\s*[-\/\pP]\s*\pN+[a-zA-Z]?)*) # House number
           \s*,?\s*
               (?P<A_Street_name>(?:[a-zA-Z]\s*|\pN\pL{2,}\s\pL)\S[^,#]*?(?<!\s)) # Street name
           \s*(?:(?:[,\/]|(?=\#))\s*(?!\s*No\.)
               (?P<A_Addition_to_address_2>(?!\s).*?))? # Addition to address 2
           |   #########################################################################
               # Option B: [<Addition to address 1>] <Street name> <House number>      #
               # [<Addition to address 2>]                                             #
               #########################################################################
               (?:(?P<B_Addition_to_address_1>.*?),\s*(?=.*[,\/]))? # Addition to address 1
               (?!\s*No\.)(?P<B_Street_name>[^0-9# ]\s*\S(?:[^,#](?!\b\pN+\s))*?(?<!\s)) # Street name
           \s*[\/,]?\s*(?:\sNo\.)?\s*
               (?P<B_House_number>\pN+\s*-?[a-zA-Z]?(?:\s*[-\/\pP]?\s*\pN+(?:\s*[\-a-zA-Z])?)*|
               [IVXLCDM]+(?!.*\b\pN+\b))(?<!\s) # House number
           \s*(?:(?:[,\/]|(?=\#)|\s)\s*(?!\s*No\.)\s*
               (?P<B_Addition_to_address_2>(?!\s).*?))? # Addition to address 2
           )
           \s*\Z/xu';
        $result = preg_match($regex, $address, $matches);
        if ($result === 0 || $result === false) {
            return array(
                'street-addition-1' => "",
                'street' => $street,
                'street-number' => "",
                'street-addition-2' => ""
            );
        }
        if (!empty($matches['A_Street_name'])) {
            return array(
                'street-addition-1' => $matches['A_Addition_to_address_1'],
                'street' => $matches['A_Street_name'],
                'street-number' => $matches['A_House_number'],
                'street-addition-2' => (isset($matches['A_Addition_to_address_2'])) ? $matches['A_Addition_to_address_2'] : ''
            );
        }
        else {
            return array(
                'street-addition-1' => $matches['B_Addition_to_address_1'],
                'street' => $matches['B_Street_name'],
                'street-number' => $matches['B_House_number'],
                'street-addition-2' => isset($matches['B_Addition_to_address_2']) ? $matches['B_Addition_to_address_2'] : ''
            );
        }
    }

    public static function vat_number()
    {
        switch(Helpers::get_locale())
        {
            case Helpers::LOCALE_DE:
                return env('VAT_NUMBER_DE');
                break;
            case Helpers::LOCALE_AT:
                return env('VAT_NUMBER_DE');
                break;
            case Helpers::LOCALE_CH:
                return env('VAT_NUMBER_CH');
                break;
            default:
                return '';
                break;
        }
    }

    public static function get_country_code($country)
    {
        $locale = app('translator')->getLocale();
        switch($country)
        {
            case 'CH':
            case 'Schweiz':
                $locale = 'de-ch';
                break;
            case 'DE':
            case 'Germany':
            case 'Deutschland':
                $locale = 'de-de';
                break;
            case 'AT':
            case 'Austria':
            case 'österreich':
            case 'Österreich':
                $locale = 'de-at';
                break;
            default:
                $locale = 'de-ch';
                break;
        }
        if(!array_key_exists($locale,self::$countriesCache)){
            self::$countriesCache[$locale] = Country::where('content_code',$locale)->first();
        }
        return self::$countriesCache[$locale]->iso_alpha_2;
    }

    /**
     * Get the configuration path.
     *
     * @param  string $path
     * @return string
     */
    public static function config_path($path = '')
    {
        return app()->basePath() . '/config' . ($path ? '/' . $path : $path);
    }

    /**
     * Get an item from an array using "dot" notation.
     *
     * @param  \ArrayAccess|array  $array
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    public static function array_get($array, $key, $default = null)
    {
        return Arr::get($array, $key, $default);
    }

    /**
     * Return currency of selected country from header
     * @return mixed
     */
    public static function currency()
    {
        if(!array_key_exists(Helpers::get_locale(),self::$currenciesCache)){
            self::$currenciesCache[Helpers::get_locale()] = Country::where('content_code',Helpers::get_locale())->first()->currency;
        }
        return self::$currenciesCache[Helpers::get_locale()];
    }


    /**
     * Return country object reading content code from the header
     * @return mixed
     */
    public static function country()
    {
        if(!array_key_exists(Helpers::get_locale(),self::$countriesCache)){
            self::$countriesCache[Helpers::get_locale()] = Country::where('content_code',Helpers::get_locale())->first();
        }
        return self::$countriesCache[Helpers::get_locale()];
    }

    /**
     * Get image path for given image name
     * @param $imageName
     * @return string
     */
    public static function getImage($imageName)
    {
        return env('S3_BASE') . Product::IMAGE_LINK_FOLDER . '/' . Helpers::get_locale() . '/' . $imageName;
    }

    /**
     * Get thumbnail image path for given image name
     * @param $imageName
     * @return string
     */
    public static function getImageThumbnail($imageName)
    {
        return env('S3_BASE') . Product::IMAGE_LINK_FOLDER . '/' . Helpers::get_locale() . '/'. Product::IMAGE_THUMBNAIL_LINK_FOLDER . '/' . $imageName;
    }

    /**
     * Get hour and minute in current UTC time from given time and timezone
     * @param $time should be in H:i format
     * @param string $timezone
     * @return string
     */
    public static function convertTimeFromCETtoUTC($time, $timezone=self::TIMEZONE_ZURICH)
    {
        try
        {
            $tagetTimezone = self::TIMEZONE_UTC;
            // default is UTC, we're writing times in Europe/Zurich
            $now = Carbon::now($timezone)->setTimeFromTimeString($time);
            return $now->setTimezone($tagetTimezone)->format('H:i');
        } catch (\Throwable $exception)
        {
            app('sentry')->captureException($exception);
        }
    }
}
