<?php


namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class LoyaltyServiceProvider extends ServiceProvider
{
    /**
     * Register Loyalty programme services.
     *
     * @return void
     */
    public function register()
    {
        // bind LoyaltyService to loyalty, this is used in facade to bind correct service
        $this->app->bind('loyalty', \App\Services\LoyaltyProgramme\LoyaltyService::class);
    }
}