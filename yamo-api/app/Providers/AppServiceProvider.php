<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        require_once __DIR__ . '/../Helpers.php';

        $this->app->configure('mfgroup');
        $this->app->configure('activecampaign');
        $this->app->singleton('mailer', function ($app) {
            return $app->loadComponent('mail', 'Illuminate\Mail\MailServiceProvider', 'mailer');
        });
        $this->app->bind('App\Services\Validation\CutOffTimeInterface', 'App\Services\Validation\CutOffTimeProwito');
    }
}
