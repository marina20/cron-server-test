<?php

use App\Models\LoyaltyRule;
use App\Models\Voucher;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Lumen\Testing\DatabaseTransactions;

class LoyaltyControllerTest extends TestCase
{
    use  DatabaseTransactions;
    public function testIndex()
    {
        $expectedResult = [
            'data'=>[
                ['box'=>2, 'description'=>'Free Bib'],
                ['box'=>4, 'description'=>'2 Surprise products'],
                ['box'=>6, 'description'=>'50% discount']
            ]
        ];
        $response = $this->json('GET','/api/v1/public/loyalty');
        $response->assertResponseOk();
        $response->seeJsonEquals($expectedResult);
    }
}