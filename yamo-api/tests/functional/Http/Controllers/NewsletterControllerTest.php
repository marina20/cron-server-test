<?php
/**
 * Created by PhpStorm.
 * User: juanse254
 * Date: 8/17/18
 * Time: 12:12 PM
 */
use Illuminate\Foundation\Testing\WithoutMiddleware;
use database\factories\ModelFactory;
use App\Models\User;
use App\Models\Profile;



class NewsletterControllerTest extends TestCase
{
	protected $user;
	public function setUp()
	{
		parent::setUp();
		$this->user = factory(User::class)->create();
		$profile = factory(Profile::class)->create(['user_id' =>$this->user->id]);
		$profile->save();
	}
	public function testNewsletterRemove()
	{
		$response = $this->actingAs($this->user)->call('post','/api/v1/protected/newsletter/remove', ['email' => 'thismailwontexist@nonexistent.com'] );
		$this->assertEquals('email not found!', $response->content());
		$this->assertEquals(404, $response->status());
		$this->assertEquals($this->user->profile->accepts_newsletter, false);
	}

}
