<?php
/**
 * Created by PhpStorm.
 * User: juanse254
 * Date: 8/28/18
 * Time: 3:04 PM
 */

namespace Tests\Traits;

use App\Traits\XMLfromArray;
use TestCase;

class XMLfromArrayTest extends TestCase
{
	protected $XMLtoArray;
	protected $givenValue;
	protected $expectedValue;
	public function setUp()
	{
		parent::setUp();
		$this->XMLtoArray = $this->getMockForTrait(XMLfromArray::class);
	}
	public function testEncode()
	{
		$this->givenValue = [
			"externalReference" => "1234987",
			"orderIpAddress" => "127.0.0.1",
			"gender" => "male",
			"firstName" => "Mann",
			"lastName" => "Muster",
			"street" => "am Bohl 14B",
			"city" => "St. Gallen",
			"zip" => "9004",
			"country" => "CH",
			"language" => "de",
			"email" => "mustermann@muster.ch",
			"birthdate" => "1984-02-06",
			"merchantId" => "17933",
			"filialId" => "100",
			"terminalId" => "3323",
			"amount" => "20400",
			"currencyCode" => "CHF"
		];
		$this->expectedValue = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<cardNumberRequest protocol=\"getVirtualCardNumber\" version=\"2.9\"><externalReference>1234987</externalReference><orderIpAddress>127.0.0.1</orderIpAddress><gender>male</gender><firstName>Mann</firstName><lastName>Muster</lastName><street>am Bohl 14B</street><city>St. Gallen</city><zip>9004</zip><country>CH</country><language>de</language><email>mustermann@muster.ch</email><birthdate>1984-02-06</birthdate><merchantId>17933</merchantId><filialId>100</filialId><terminalId>3323</terminalId><amount>20400</amount><currencyCode>CHF</currencyCode></cardNumberRequest>\n";
		$elements= ['protocol' => 'getVirtualCardNumber', 'version' => '2.9'];
		$this->assertEquals($this->XMLtoArray->encodetoXML($this->givenValue, 'cardNumberRequest', $elements),$this->expectedValue);
	}

	public function testDecodeSimpleXML()
	{
		$this->givenValue = $this->getSimpleObject();
		$this->expectedValue = ['availableCredit' => '49900',
			'maximalCredit' => '49900',
			'creditRefusalReason' => 'None',
			'cardNumber' => '6004516517802595138',
			'responseCode' => 'OK'
		];
		$this->assertArrayHasKey(key($this->expectedValue), $this->XMLtoArray->decodeSimpleXMLObject($this->givenValue));
	}

	public function testEncodeDecode()
	{
		$this->givenValue = ['this_is_for_yamo' => '158819',
			'moreElements' => '77777'
			];
		$this->assertEquals($this->XMLtoArray->decodeSimpleXMLObject($this->XMLtoArray->encodetoSimpleXMLObject($this->givenValue, null, null)), $this->givenValue );
	}

	private function getSimpleObject()
	{
		$server = 'https://testgateway.mfgroup.ch';
		$user = 'yamoTest';
		$psw = 'YHfzuY3K';
		$xml = '<?xml version="1.0" encoding="UTF-8"?>
<cardNumberRequest protocol="getVirtualCardNumber" version="2.9">
  <externalReference>1234987</externalReference>
  <orderIpAddress>127.0.0.1</orderIpAddress>
  <gender>male</gender>
  <firstName>Mann</firstName>
  <lastName>Muster</lastName>
  <street>am Bohl 14B</street>
  <city>St. Gallen</city>
  <zip>9004</zip>
  <country>CH</country>
  <language>de</language>
  <email>mustermann@muster.ch</email>
  <birthdate>1984-02-06</birthdate>
  <merchantId>17933</merchantId>
  <filialId>100</filialId>
  <terminalId>3323</terminalId>
  <amount>20400</amount>
  <currencyCode>CHF</currencyCode>
</cardNumberRequest>';
		$credentials = "$user:$psw";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $server);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		curl_setopt($ch, CURLOPT_USERPWD, $user . ":" . $psw);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLAUTH_BASIC, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 'xml=' . urlencode($xml));

		$data = curl_exec($ch);

		if (curl_errno($ch)) {
			echo "Error:    " . curl_error($ch);
			return;
		}

		$result = simplexml_load_string($data);
		curl_close($ch);
		return $result;
	}

}
