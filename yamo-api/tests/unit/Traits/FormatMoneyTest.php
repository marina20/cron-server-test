<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 11/23/17
 * Time: 11:40 AM
 */

use App\Traits\FormatMoney;

class FormatMoneyTest extends TestCase
{
    protected $formatMoney;
    protected $givenValue;
    protected $expectedValue;
    public function setUp()
    {
        $this->formatMoney = $this->getMockForTrait(FormatMoney::class);
    }

    /** @test */
    public function round_float_to_two_decimals()
    {
        $this->givenValue = 22.0;
        $this->expectedValue = '22.00';
        $this->run_test();
    }

    /** @test */
    public function round_string_to_two_decimals()
    {
        $this->givenValue = '22';
        $this->expectedValue = '22.00';
        $this->run_test();
    }

    /** @test */
    public function round_integer_to_two_decimals()
    {
        $this->givenValue = 22;
        $this->expectedValue = '22.00';
        $this->run_test();
    }

    /** @test */
    public function round_bool_to_two_decimals()
    {
        $this->givenValue = true;
        $this->expectedValue = '1.00';
        $this->run_test();
    }

    /** @test */
    public function return_zero_when_is_string()
    {
        $this->givenValue = 'gfdfdf';
        $this->expectedValue = '0.00';
        $this->run_test();
    }

    /** @test */
    public function return_zero_when_is_null()
    {
        $this->givenValue = NULL;
        $this->expectedValue = '0.00';
        $this->run_test();
    }

    /** @test */
    public function return_zero_when_is_empty_string()
    {
        $this->givenValue = '';
        $this->expectedValue = '0.00';
        $this->run_test();
    }

    /** @test */
    public function round_string_many_decimal_points_to_two_decimals()
    {
        $this->givenValue = '78.564518481841';
        $this->expectedValue = '78.56';
        $this->run_test();
    }

    /** @test */
    public function round_already_formatted_to_two_decimals()
    {
        $this->givenValue = '78.56';
        $this->expectedValue = '78.56';
        $this->run_test();
    }

    protected function run_test()
    {
        $this->assertEquals($this->formatMoney->roundMoney($this->givenValue),$this->expectedValue);
    }
}