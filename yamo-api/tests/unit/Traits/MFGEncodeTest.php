<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/24/18
 * Time: 2:54 PM
 */
namespace Tests\Traits;

use TestCase;
use App\Traits\MFGEncode;

class MFGEncodeTest extends TestCase
{
    protected $MFGEncode;
    protected $givenValue;
    protected $expectedValue;
    public function setUp()
    {
        parent::setUp();
        $this->MFGEncode = $this->getMockForTrait(MFGEncode::class);
    }

    public function test_url_encode()
    {
        $this->givenValue = 'test+string&to$encode';
        $this->expectedValue = 'test%2Bstring%26to%24encode';
        $this->assertEquals($this->MFGEncode->url($this->givenValue),$this->expectedValue);
    }

    public function test_xml_encode()
    {
        $this->givenValue = '<check the “Scenarios” chapter>';
        $this->expectedValue = 'xml=%3Ccheck+the+%E2%80%9CScenarios%E2%80%9D+chapter%3E';
        $this->assertEquals($this->MFGEncode->xml($this->givenValue),$this->expectedValue);
    }
}