<?php

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Subscription;
use App\Models\Box;
use App\Models\Product;
use App\Models\Category;
use App\Models\BoxContent;
use App\Models\Profile;
use App\Models\User;
use App\Services\CreateNextOrderFrom\CreateNextOrderFromSubscription;
use App\Services\DuplicateOrder\DuplicateOrder;
use App\Services\CreateOrder\CreateOrderSubscription;
use App\Services\CreateAdyenLinkService;
use App\Services\CreateInvoiceLinkService;
use App\Services\CreatePaypalLinkService;
use App\Services\CreateSofortLinkService;
use App\Services\MFGInvoice\MFGInvoice;
use App\Services\SubscriptionService;
use App\Services\UpdateProfileService;
use App\Services\CancelSubscriptionService;
use Carbon\Carbon;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Tymon\JWTAuth\JWTAuth;

class CreateNextOrderFromSubscriptionTest extends TestCase
{
    use  DatabaseTransactions;

    protected $create_next_order_from_subscription;
    Protected $discount = '0';

    private function createNewOrder() {
        $categoryPouches = Category::where('name_key','pouches')->first() ?? factory(Category::class)->create(['name_key'=>'test']);
        $subscriptionProduct = Product::where('category_id',$categoryPouches->id)->first();
        $custom_box = factory(Box::class)->create(['type'=>'custom']);
        $custom_box_content = factory(BoxContent::class)->create([
            'box_id'=>$custom_box->id,
            'product_id' => $subscriptionProduct->id,
            'quantity' => 16
        ]);
        $custom_box->items->add($custom_box_content);
        $user = factory(User::class)->create();
        \App\Models\Profile::create(['user_id' => $user->id]);

        $order = factory(Order::class)->create([
            'user_id'=>$user->id,
            "payment_method_id"=> "invoice",
            "payment_method_title"=> "Rechnung",
            "created_via"=> Order::CREATED_VIA_SUBSCRIPTION,
            "completed_date"=> "2018-08-07 10:47:09",
            "paid_date"=> "2018-08-07 10:47:09",
            "billing_first_name"=> "shipping first name test",
            "billing_last_name"=> "shipping last name test",
            "billing_company"=> "",
            "billing_address_1"=> "shipping 8998",
            "billing_city"=> "L",
            "billing_postcode"=> "1004",
            "billing_country"=> "Schweiz",
            "billing_email"=> "milena@yamo.ch",
            "billing_phone"=> "90099",
            "shipping_first_name"=> "shipping first name test",
            "shipping_last_name"=> "shipping last name test",
            "shipping_company"=> "",
            "shipping_address_1"=> "shipping 8998",
            "shipping_city"=> "L",
            "shipping_postcode"=> "1004",
            "shipping_country"=> "Schweiz",
            "shipping_email"=> "milena@yamo.ch",
            "shipping_phone"=> "90099",
            "currency"=> "CHF",
            "discount"=> "20.00",
            "discount_tax"=> "0",
            "shipping"=> "0.00",
            "shipping_tax"=> "0.00",
            "order_total_without_tax"=> "60.49",
            "order_tax"=> "1.51",
            "order_total"=> "42.00",
            "order_subtotal_tax"=> "1.51",
            "order_subtotal"=> "62.00",
            "delivery_date"=> "2018-07-27 15:17:06",
            "cancellation_deadline"=> "2018-07-22 23:59:59",
            "child_birthday"=> "2016-05-05 00:00:00",
            "billing_title"=> "Frau",
            "shipping_title"=> "Frau",
            "is_subscription"=> 1,
            "parent_id"=> null,
            "status"=> "pending",
            "coupon_id"=> null,
            "coupon_code"=> null,
            "redemption_code"=> "test",
            "country_id"=> "2",
            "adyen_auth_result"=> "AUTHORISED",
            "adyen_payment_method"=> "invoice",
            "custom_box_id"=> $custom_box->id,
            "created_at"=> "2018-07-06 13:17:15",
            "updated_at"=> "2018-07-06 13:17:40"
        ]);

        Helpers::set_locale(Helpers::get_country_code($order->shipping_country));



        $given_subscription = factory(Subscription::class)->create([
            "order_id"=> $order->id,
            "billing_period"=> "week",
            "billing_interval"=> "2",
            "suspension_count"=> "0",
            "requires_manual_renewal"=> "false",
            "schedule_next_payment"=> "2018-08-10",
            "status"=> "cancelled",
            "delivery_frequency"=> 2,
            "delivery_frequency_period"=> "week",
            "product_id"=> $subscriptionProduct->id,
            "created_at"=> "2018-07-06 13:17:16",
            "updated_at"=> "2018-08-08 14:45:11",
            'user_id' => $user->id
        ]);

        $order_items_1 = factory(OrderItem::class)->create([
            "item_name"=> $subscriptionProduct->name,
            "item_type"=> "line_item",
            "order_id"=> $order->id,
            "product_id"=> $subscriptionProduct->id,
            "qty"=> "16",
            "item_price"=>$subscriptionProduct->price,
            "total"=> 16 * $subscriptionProduct->price,
            "total_tax"=> "1.51",
            "subtotal"=> "62.00",
            "subtotal_tax"=> "1.51",
            "total_discount"=> "0",
            "total_discount_tax"=> "0",
            "tax_rate"=> $subscriptionProduct->tax_percentage,
            "created_at"=> "2018-07-06 13:17:16",
            "updated_at"=> "2018-07-06 13:17:16"
        ]);

        return $order;
    }

    public function setUp()
    {
        parent::setUp();
        $this->create_next_order_from_subscription = new CreateNextOrderFromSubscription(new DuplicateOrder(), new CreateOrderSubscription(
            new CreateAdyenLinkService(),
            new CreateInvoiceLinkService(),
            new MFGInvoice(new CancelSubscriptionService()),
            new CreatePaypalLinkService(),
            new CreateSofortLinkService(),
            new UpdateProfileService(),
            new SubscriptionService(),
            $this->createMock(JWTAuth::class)
        ));
    }

    public function test_successfull_create_next_order()
    {
        $given_order = $this->createNewOrder();
        $order = $this->create_next_order_from_subscription->create($given_order);
        $totalOrder = 0;
        $totalGivenOrder = 0;
        foreach ($order->items as $item) {
            $totalOrder += $item->total;
        }
        foreach ($given_order->items as $item) {
            $totalGivenOrder += $item->total;
        }
        
        $calculatedDeliveryDate = Carbon::parse("2020-01-10 00:00:00")->addDays((1 * 7))->format('Y-m-d H:i:s');
        $result = \App\Models\DeliveryDate::getNextDeliveryDate($calculatedDeliveryDate,2);
        $this->assertEquals($result[0], $calculatedDeliveryDate);
        $this->assertNull($result[1]);
        $calculatedDeliveryDate = Carbon::parse("2019-10-25 00:00:00")->addDays((1 * 7))->format('Y-m-d H:i:s');
        $result = \App\Models\DeliveryDate::getNextDeliveryDate($calculatedDeliveryDate,2);
        $this->assertEquals($result[1], $calculatedDeliveryDate);
        $this->assertEquals($result[0], "2019-11-05 00:00:00");
        $this->assertNull($order->transaction_id);
        $this->assertNull($order->completed_date);
        $this->assertNull($order->paid_date);
        $this->assertNull($order->coupon_id);
        $this->assertEquals($order->is_subscription, 1);
        $this->assertEquals($order->created_via, Order::CREATED_VIA_SUBSCRIPTION);
        $this->assertEquals($order->status, Order::STATUS_PENDING);

        // TODO fix those assertions
        // loyalty is applied automatically to this order
//         $this->assertEquals(count($order->items), count($given_order->items));
        // -8 because of first subscription vs subscription-discount
        $this->assertEquals($totalOrder, $totalGivenOrder-8);
        $this->assertEquals($totalOrder - $this->discount, $order->order_total);
    }

}