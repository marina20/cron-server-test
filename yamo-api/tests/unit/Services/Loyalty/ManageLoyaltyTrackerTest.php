<?php


namespace Tests\Services;

use App\Models\LoyaltyTracker;
use App\Models\Order;
use App\Models\Voucher;
use App\Models\User;
use App\Services\LoyaltyProgramme\ManageLoyaltyTracker;
use Laravel\Lumen\Testing\DatabaseTransactions;
use TestCase;

class ManageLoyaltyTrackerTest extends TestCase
{
    use DatabaseTransactions;

    protected $manageLoyaltyTracker;

    public function setUp()
    {
        parent::setUp();
        $this->manageLoyaltyTracker = new ManageLoyaltyTracker();
    }

    public function test_loyalty_tracker_save()
    {
        $user = factory(User::class)->create();
        $order = factory(Order::class)->create(['user_id' => $user->id,'delivery_date'=>'2019-11-11 11:11:11']);
        $voucher = factory(Voucher::class)->create(['code'=>'test']);
        $boxNumber = 1;

        $this->manageLoyaltyTracker->save($order, $voucher, $boxNumber);

        $loyaltyTracked = LoyaltyTracker::where('order_id',$order->id)->where('voucher_id', $voucher->id)->where('box_number', $boxNumber)->first();

        $this->assertTrue($loyaltyTracked instanceof LoyaltyTracker);
        $this->assertEquals($loyaltyTracked->order_id, $order->id);
    }
}