<?php


namespace Tests\Services;

use App\Models\User;
use App\Services\LoyaltyProgramme\LoyaltyService;
use App\Services\LoyaltyProgramme\ManageLoyaltyTracker;
use App\Services\LoyaltyProgramme\ManageLoyaltyVoucher;
use App\Services\LoyaltyProgramme\OrderBoxNumber;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Mockery as m;
use TestCase;

class LoyaltyServiceTest extends TestCase
{
    use DatabaseTransactions;

    protected $loyaltyService;
    protected $mockOrderBoxNumber;
    protected $mockManageLoyaltyTracker;
    protected $mockManageLoyaltyVoucher;

    public function setUp()
    {
        parent::setUp();
        $this->mockOrderBoxNumber = m::mock(OrderBoxNumber::class);
        $this->mockManageLoyaltyTracker = m::mock(ManageLoyaltyTracker::class);
        $this->mockManageLoyaltyVoucher = m::mock(ManageLoyaltyVoucher::class);

        $this->loyaltyService = new LoyaltyService($this->mockOrderBoxNumber,
            $this->mockManageLoyaltyTracker,
            $this->mockManageLoyaltyVoucher);
    }

    public function test_box_current()
    {
        $user = factory(User::class)->create();
        $expectedCurrentBoxNumber = 3;
        $this->mockOrderBoxNumber->shouldReceive('current')->andReturn($expectedCurrentBoxNumber);
        $this->assertEquals($expectedCurrentBoxNumber, $this->loyaltyService->currentBoxNumber($user));
    }

    public function test_box_next()
    {
        $user = factory(User::class)->create();
        $expectedCurrentBoxNumber = 4;
        $this->mockOrderBoxNumber->shouldReceive('next')->andReturn($expectedCurrentBoxNumber);
        $this->assertEquals($expectedCurrentBoxNumber, $this->loyaltyService->nextBoxNumber($user));
    }
}