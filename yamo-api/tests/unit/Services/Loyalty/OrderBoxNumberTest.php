<?php
namespace Tests\Services;

use App\Models\Order;
use App\Models\User;
use App\Services\LoyaltyProgramme\OrderBoxNumber;
use Carbon\Carbon;
use Laravel\Lumen\Testing\DatabaseTransactions;
use TestCase;

class OrderBoxNumberTest extends TestCase
{
    use DatabaseTransactions;

    protected $boxNumberService;

    public function setUp()
    {
        parent::setUp();
        $this->boxNumberService = new OrderBoxNumber();
    }

    /**
     * test success case of current box order number
     */
    public function test_get_current_box_number()
    {
        $user = factory(User::class)->create();
        factory(Order::class)->create(['user_id'=>$user->id, 'status'=> Order::STATUS_PAYED, 'paid_date'=>Carbon::now(), 'resent'=>0]);
        factory(Order::class)->create(['user_id'=>$user->id, 'status'=> Order::STATUS_PAYED, 'paid_date'=>Carbon::now(), 'resent'=>0]);
        $ordersPerUser = Order::where('user_id',$user->id)->where('status',Order::STATUS_PAYED)->count();
        $this->assertEquals($ordersPerUser, $this->boxNumberService->current($user));
    }

    /**
     * test success cast of next box number
     */
    public function test_get_next_box_number()
    {
        $user = factory(User::class)->create();
        factory(Order::class)->create(['user_id'=>$user->id, 'status'=> Order::STATUS_PAYED, 'paid_date'=>Carbon::now(), 'resent'=>0]);
        factory(Order::class)->create(['user_id'=>$user->id, 'status'=> Order::STATUS_PAYED, 'paid_date'=>Carbon::now(), 'resent'=>0]);
        $ordersPerUser = Order::where('user_id',$user->id)->where('status',Order::STATUS_PAYED)->count();
        $ordersPerUser++;
        $this->assertEquals($ordersPerUser, $this->boxNumberService->next($user));
    }

    /**
     * test current order box number if user doesn't have orders
     */
    public function test_get_current_box_number_null_orders_for_user()
    {
        $ordersPerUser = 0;
        $user = factory(User::class)->create();
        $this->assertEquals($ordersPerUser, $this->boxNumberService->current($user));
    }

    /**
     * test current order box number if user has only cancelled orders
     */
    public function test_get_current_box_number_cancelled_orders_for_user()
    {
        $ordersPerUser = 0;
        $user = factory(User::class)->create();
        factory(Order::class)->create(['user_id'=>$user->id, 'status'=> Order::STATUS_CANCELLED]);
        factory(Order::class)->create(['user_id'=>$user->id, 'status'=> Order::STATUS_CANCELLED]);
        $this->assertEquals($ordersPerUser, $this->boxNumberService->current($user));
    }

    /**
     * test current order box number if user has cancelled and pending orders
     */
    public function test_get_current_box_number_cancelled_and_pending_orders_for_user()
    {
        $ordersPerUser = 0;
        $user = factory(User::class)->create();
        factory(Order::class)->create(['user_id'=>$user->id, 'status'=> Order::STATUS_PENDING]);
        factory(Order::class)->create(['user_id'=>$user->id, 'status'=> Order::STATUS_CANCELLED]);
        $this->assertEquals($ordersPerUser, $this->boxNumberService->current($user));
    }

    /**
     * test current order box number if user has cancelled, pending and valid orders
     */
    public function test_get_current_box_number_cancelled_and_pending_orders_and_valid_for_user()
    {
        $user = factory(User::class)->create();
        factory(Order::class)->create(['user_id'=>$user->id, 'status'=> Order::STATUS_PENDING]);
        factory(Order::class)->create(['user_id'=>$user->id, 'status'=> Order::STATUS_CANCELLED]);
        factory(Order::class)->create(['user_id'=>$user->id, 'status'=> Order::STATUS_PAYED, 'paid_date'=>Carbon::now(), 'resent'=>0]);
        $ordersPerUser = Order::where('user_id',$user->id)->where('status',Order::STATUS_PAYED)->count();
        $this->assertEquals($ordersPerUser, $this->boxNumberService->current($user));
    }

    /**
     * test next order box number if user doesn't have orders
     */
    public function test_get_next_box_number_user_without_orders()
    {
        $user = factory(User::class)->create();
        $ordersPerUser = Order::where('user_id',$user->id)->where('status',Order::STATUS_PAYED)->count();
        $ordersPerUser++;
        $this->assertEquals($ordersPerUser, $this->boxNumberService->next($user));
    }

    /**
     * test next order box number if user have cancelled, pending and valid orders
     */
    public function test_get_next_box_number_user_with_cancelled_and_valid_orders()
    {
        $user = factory(User::class)->create();
        factory(Order::class)->create(['user_id'=>$user->id, 'status'=> Order::STATUS_PENDING]);
        factory(Order::class)->create(['user_id'=>$user->id, 'status'=> Order::STATUS_CANCELLED]);
        factory(Order::class)->create(['user_id'=>$user->id, 'status'=> Order::STATUS_PAYED, 'paid_date'=>Carbon::now(), 'resent'=>0]);
        $ordersPerUser = Order::where('user_id',$user->id)->where('status',Order::STATUS_PAYED)->count();
        $ordersPerUser++;
        $this->assertEquals($ordersPerUser, $this->boxNumberService->next($user));
    }
}