<?php


namespace Tests\Services;
use App\Models\LoyaltyRule;
use App\Models\Voucher;
use App\Services\LoyaltyProgramme\ManageLoyaltyVoucher;
use Laravel\Lumen\Testing\DatabaseTransactions;
use TestCase;


class ManageLoyaltyVoucherTest extends TestCase
{
    use DatabaseTransactions;

    protected $manageLoyaltyVoucher;

    public function setUp()
    {
        parent::setUp();
        $this->manageLoyaltyVoucher = new ManageLoyaltyVoucher();
    }

    public function test_get_voucher_for_existing_loyalty_rule()
    {
        $boxNumber = 2;
        $rule = LoyaltyRule::where('box_number',$boxNumber)->first();
        $voucher = $rule->voucher;

        $givenVoucher = $this->manageLoyaltyVoucher->get($boxNumber);
        $this->assertTrue($givenVoucher instanceof Voucher);
        $this->assertEquals($voucher->id, $givenVoucher->id);
    }

    public function test_get_voucher_rule_exists_box_number_not()
    {
        $expectedResult = null;
        $loyaltyRule = LoyaltyRule::get()->first();
        $boxNumber = $loyaltyRule->box_number + 1;
        $givenResult = $this->manageLoyaltyVoucher->get($boxNumber);
        $this->assertNull($givenResult);
    }
}