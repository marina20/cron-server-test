<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/16/18
 * Time: 4:06 PM
 */

namespace Tests\Services;

use App\Models\Category;
use App\Models\Product;
use App\Models\Box;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Profile;
use App\Models\Subscription;
use App\Models\User;
use App\Services\CustomBox\Create;
use App\Services\CustomBox\CreateContent;
use App\Services\CustomBox\DefaultProduct;
use App\Services\CustomBox\Delete;
use App\Services\MyAccount\SubscriptionCustomBoxSave;
use Carbon\Carbon;
use Laravel\Lumen\Testing\DatabaseTransactions;
use Helpers;
use TestCase;

class SubscriptionCustomBoxSaveTest extends TestCase
{
    use  DatabaseTransactions;

    protected $subscription_custom_box_save;

    public function setUp()
    {
        parent::setUp();
        $this->subscription_custom_box_save = new SubscriptionCustomBoxSave(new Create(), new CreateContent(),
            new Delete(), new DefaultProduct());
        $this->expected_default = false;
    }

    public function test_save_subscription_with_subscription()
    {
        Helpers::set_locale(Helpers::LOCALE_DOMAIN_CH);
        $categoryCups = Category::where('name_key',Product::PRODUCT_CATEGORY_BREIL)->first();
        $products = Product::where('category_id',$categoryCups->id)->whereNotNull('product_identifier_letter')->get();
        $someProduct = $products->pop();
        $someProduct1 = $products->pop();
        $user = factory(User::class)->create();
        $this->be($user);
        $profile = Profile::firstOrCreate(['user_id' => $user->id, 'wallet_amount_cents' => 0]);
        $someProduct2 = $products->pop();
        $box = factory(Box::class)->create();
        $given_order = factory(Order::class)->create(['user_id'=>$user->id,'custom_box_id' => $box->id, 'cancellation_deadline'=>Carbon::now()->addDay(10)->toDateTimeString()]);

        $given_order_item = factory(OrderItem::class)->create(
            [
                'item_type'=>'line_item',
                'order_id'=>$given_order->id,
                'product_id'=>$someProduct->id
            ]);
        $given_order->items = $given_order_item;
        $given_subscription = factory(Subscription::class)->create(
            [
                'status'=>'active',
                'order_id'=>$given_order->id,
                'product_id'=>$someProduct->id
            ]);
        $given_items_array = [
            ['id'=>$someProduct1->id, 'quantity'=>7],
            ['id'=>$someProduct2->id, 'quantity'=>8]
        ];

        $given_result = $this->subscription_custom_box_save->save($given_subscription, $given_items_array);
        $given_result->order->load('items');
        $box = Box::find($given_result->order->custom_box_id);
        $this->assertEquals(count($given_items_array), count($box->items));
        $this->assertEquals(count($given_items_array), count($given_result->order->items));
    }
}