<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 5/16/18
 * Time: 12:54 PM
 */
namespace Tests\Services\OrderReport;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Services\OrderReport\OrderReportBox;
use TestCase;


class OrderReportBoxTest extends TestCase
{
    protected $order_report_box;
    protected $expected_default;

    public function setUp()
    {
        parent::setUp();
        $this->order_report_box = new OrderReportBox();
        $this->expected_default = [];
    }

    public function test_content_subscription()
    {
        $expected = [
                'R'=>'3',
                'B'=>'5',
                'A'=>'5',
                'P'=>'3'
            ];


        $slug1 = 'pirates-of-the-carrotean';
        $slug2 = 'fresh-prince-of-bel-pear';
        $slug3 = 'applecalypse-now';
        $slug4 = 'anthony-pumpkins';

        $product1 = Product::where('name_key',$slug1)->first();
        $product2 = Product::where('name_key',$slug2)->first();
        $product3 = Product::where('name_key',$slug3)->first();
        $product4 = Product::where('name_key',$slug4)->first();

        $order = factory(Order::class)->create();
        $orderItem1 = factory(OrderItem::class)->create(['product_id'=>$product1->id, 'order_id'=>$order->id, 'qty'=>3]);
        $orderItem2 = factory(OrderItem::class)->create(['product_id'=>$product2->id, 'order_id'=>$order->id, 'qty'=>5]);
        $orderItem3 = factory(OrderItem::class)->create(['product_id'=>$product3->id, 'order_id'=>$order->id, 'qty'=>5]);
        $orderItem4 = factory(OrderItem::class)->create(['product_id'=>$product4->id, 'order_id'=>$order->id, 'qty'=>3]);

        $given = $this->order_report_box->content($order);
        $this->assertEquals($expected, $given);
    }
}