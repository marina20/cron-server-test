<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2/22/18
 * Time: 1:49 PM
 */
namespace Tests\Services\OrderReport\Transformer;

use Illuminate\Database\Eloquent\Collection;
use TestCase;
use App\Services\OrderReport\Data\DataGermany;
use App\Services\OrderReport\Data\DataSwitzerland;
use App\Services\OrderReport\Data\DataAustria;
use App\Services\OrderReport\Transformer\TransformerGermany;
use App\Services\OrderReport\Transformer\TransformerAustria;
use App\Services\OrderReport\Transformer\TransformerSwitzerland;
use App\Services\OrderReport\Transformer\TransformerFactory;

class TransformerFactoryTest extends TestCase
{
    protected function assert_factory($order_data, $expected)
    {
        $transformer_factory = new TransformerFactory();
        $given = $transformer_factory->get($order_data);

        $this->assertEquals($expected, $given);
    }

    /** test */
    public function test_dummy()
    {
        $this->assertTrue(true);
    }

//    /** @test */
//    public function get_function_default()
//    {
//        $order_data = new DataAustria();
//        $expected = new TransformerAustria($order_data);
//        $this->assert_factory($order_data,$expected);
//    }

//    /** @test */
//    public function get_function_germany()
//    {
//        $order_data = new Collection();
//        $expected = new TransformerGermany($order_data);
//        $this->assert_factory($order_data,$expected);
//    }

//    /** @test */
//    public function get_function_switzerland()
//    {
//        $order_data = new Collection();
//        $expected = new TransformerSwitzerland($order_data);
//        $this->assert_factory($order_data,$expected);
//    }
//    /** @test */
//    public function get_function_austria()
//    {
//        $order_data = new DataAustria();
//        $expected = new TransformerAustria($order_data);
//        $this->assert_factory($order_data,$expected);
//    }
}