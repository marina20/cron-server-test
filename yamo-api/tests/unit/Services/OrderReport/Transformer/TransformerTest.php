<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2/22/18
 * Time: 12:46 PM
 */
namespace Tests\Services\OrderReport\Transformer;

use App\Models\Country;
use Illuminate\Database\Eloquent\Collection;
use Laravel\Lumen\Testing\DatabaseTransactions;
use TestCase;
use App\Services\OrderReport\Data\Data;
use App\Services\OrderReport\Data\DataGermany;
use App\Services\OrderReport\Data\DataAustria;
use App\Services\OrderReport\Data\DataSwitzerland;
use App\Services\OrderReport\Transformer\Transformer;

class TransformerTest extends TestCase
{
    use DatabaseTransactions;
    /** @test */
    public function transform_data_class()
    {
        $order_data = new Collection();
        $transformer = new Transformer($order_data);

        $this->assertEquals($order_data,$transformer->transform());
    }

    /** @test */
    public function transform_data_germany_class()
    {
        $order_data = new Collection();
        $transformer = new Transformer($order_data);

        $this->assertEquals($order_data,$transformer->transform());
    }

    /** @test */
    public function transform_data_austria_class()
    {
        $order_data = new Collection();
        $transformer = new Transformer($order_data);

        $this->assertEquals($order_data,$transformer->transform());
    }

    /** @test */
    public function transform_data_switzerland_class()
    {
        $order_data = new Collection();
        $transformer = new Transformer($order_data);

        $this->assertEquals($order_data,$transformer->transform());
    }
}