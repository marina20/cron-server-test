<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2/22/18
 * Time: 2:04 PM
 */
namespace Tests\Services\OrderReport\Transformer;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Collectionol;
use TestCase;
use App\Services\OrderReport\Data\Data;
use App\Services\OrderReport\Data\DataGermany;
use App\Services\OrderReport\Data\DataAustria;
use App\Services\OrderReport\Data\DataSwitzerland;
use App\Services\OrderReport\Transformer\TransformerGermany;

class TransformerGermanyTest extends TestCase
{
    protected $expected = [];

    public function setUp()
    {
        parent::setUp();
        $this->expected = [
            'addresslabel' =>[]
        ];
    }

    /** @test */
    public function transform_base_data_class()
    {
        $order_data = new Collection();
        $transformer = new TransformerGermany($order_data);
        $given = $transformer->transform();
        $this->assertEquals($this->expected,$given);
    }

    /** @test */
    public function transform_austria_data_class()
    {
        $order_data = new Collection();
        $transformer = new TransformerGermany($order_data);
        $given = $transformer->transform();
        $this->assertEquals($this->expected,$given);
    }

//    /** @test */
//    public function transform_wrong_data_class()
//    {
//        $order_data = new DataAustria();
//        $transformer = new TransformerGermany($order_data);
//        $given = $transformer->transform();
//        $this->assertEquals($this->expected,$given);
//    }
}