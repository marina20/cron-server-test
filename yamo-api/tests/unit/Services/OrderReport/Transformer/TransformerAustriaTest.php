<?php

/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2/22/18
 * Time: 2:04 PM
 */
namespace Tests\Services\OrderReport\Transformer;

use App\Services\OrderReport\Transformer\TransformerAustria;
use Illuminate\Database\Eloquent\Collection;
use TestCase;

class TransformerAustriaTest extends TestCase
{
    /** @test */
    public function transform_base_data_class()
    {
        $order_data = new Collection();
        $transformer = new TransformerAustria($order_data);
        $given = $transformer->transform();
        $this->assertEquals($order_data,$given);
    }

    /** @test */
    public function transform_austria_data_class()
    {
        $order_data = new Collection();
        $transformer = new TransformerAustria($order_data);
        $given = $transformer->transform();
        $this->assertEquals($order_data,$given);
    }

    /** @test */
    public function transform_wrong_data_class()
    {
        $order_data = new Collection();
        $transformer = new TransformerAustria($order_data);
        $given = $transformer->transform();
        $this->assertEquals($order_data,$given);
    }
}