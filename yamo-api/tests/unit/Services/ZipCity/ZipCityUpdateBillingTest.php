<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-04-08
 * Time: 14:50
 */

namespace Tests\Services;

use App\Models\Order;
use App\Models\Postcode;
use App\Models\Subscription;
use App\Services\ZipCity\ZipCityUpdateBilling;
use TestCase;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ZipCityUpdateBillingTest extends TestCase
{
    use  DatabaseTransactions;

    protected $zipCityUpdateBilling;
    protected $postcodeExpected;

    public function setUp()
    {
        parent::setUp();
        $this->zipCityUpdateBilling = new ZipCityUpdateBilling();
        /*$this->postcodeExpected = factory(Postcode::class)->create([
                'id' => 1,
                'postcode' => '011',
                'city' => 'Test'
            ]);*/
    }

    public function test_call_zip_city_update_billing_with_correct_postcode_id()
    {
       /* $givenOrder = factory(Order::class)->create([
            'billing_postcode_id' => $this->postcodeExpected->id
        ]);
        $expected_billing_city = $this->postcodeExpected->city;
        $expected_billing_postcode = $this->postcodeExpected->postcode;

       $expectedOrder = $this->zipCityUpdateBilling->update($givenOrder);
       $this->assertEquals($expectedOrder->billing_city, $expected_billing_city);
       $this->assertEquals($expectedOrder->billing_postcode, $expected_billing_postcode);*/
       $this->assertEquals(true,true);
    }

    public function test_call_zip_city_update_billing_with_empty_postcode_id()
    {
       /* $givenOrder = factory(Order::class)->create([
            'billing_postcode_id' => null
        ]);
        $expectedOrder = $this->zipCityUpdateBilling->update($givenOrder);
        $this->assertEquals($expectedOrder, $givenOrder);*/
        $this->assertEquals(true,true);
    }

    public function test_call_zip_city_update_billing_with_string_postcode_id()
    {
        /*$givenOrder = factory(Order::class)->create([
            'billing_postcode_id' => 'test'
        ]);
        $expectedOrder = $this->zipCityUpdateBilling->update($givenOrder);
        $this->assertEquals($expectedOrder, $givenOrder);*/
        $this->assertEquals(true,true);
    }

    public function test_call_zip_city_update_billing_with_wrong_postcode_id()
    {
        /*
        $givenOrder = factory(Order::class)->create([
            'billing_postcode_id' => 3
        ]);
        $expectedOrder = $this->zipCityUpdateBilling->update($givenOrder);
        $this->assertEquals($expectedOrder, $givenOrder);*/
        $this->assertEquals(true,true);
    }

    public function test_call_zip_city_update_billing_with_model_that_does_not_have_billing_postcode_id()
    {
        /*
        $givenModel = factory(Subscription::class)->create();
        $expectedModel = $this->zipCityUpdateBilling->update($givenModel);
        $this->assertEquals($expectedModel, $givenModel);*/
        $this->assertEquals(true,true);
    }
}