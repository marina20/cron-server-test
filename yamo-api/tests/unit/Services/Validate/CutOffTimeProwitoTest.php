<?php

namespace Tests\Services;

use App\Console\Commands\OrderReportProwitoCommand;
use App\Services\DeliveryDate\ExportDate;
use App\Services\Validation\CutOffTimeProwito;
use Carbon\Carbon;
use Laravel\Lumen\Testing\DatabaseTransactions;
use TestCase;
use Mockery as m;
use Helpers;

class CutOffTimeProwitoTest extends TestCase
{
    use DatabaseTransactions;

    protected $cutOffTime;
    protected $testDateInFutureForCutOff;

    public function setUp()
    {
        parent::setUp();
        $mockedExportDate = m::mock(ExportDate::class);
        $this->testDateInFutureForCutOff = Carbon::now()->addDay(3)->toDateTimeString();
        $mockedExportDate->shouldReceive('getByCountryAndDeliveryDate')->andReturn($this->testDateInFutureForCutOff);
        $this->cutOffTime = new CutOffTimeProwito($mockedExportDate);
    }

    public function test_country_not_in_the_list()
    {
        $countryNotToCheck = 'ch';
        $this->assertTrue($this->cutOffTime->validate(null,$countryNotToCheck));
    }

    public function test_country_in_the_list_date_empty()
    {
        $validTestCountry = 'de';
        $givenEmptyValues = ['', null, 0, false, '0000-00-00 00:00:00'];
        foreach ($givenEmptyValues as $givenEmptyValue) {
            $this->assertFalse($this->cutOffTime->validate($givenEmptyValue,$validTestCountry));
        }
    }

    public function test_valid_country_and_date()
    {
        $countryToCheck = 'de';
        $validDate = '2019-11-01';
        $this->assertTrue($this->cutOffTime->validate($validDate,$countryToCheck));
    }

    public function test_valid_country_and_date_with_time_string()
    {
        $countryToCheck = 'de';
        $validDate = '2019-11-01 14:44:44';
        $this->assertTrue($this->cutOffTime->validate($validDate,$countryToCheck));
    }

    public function test_valid_country_and_date_shorter_than_it_should_be()
    {
        $countryToCheck = 'de';
        $validDate = '2019-11-1';
        $this->assertFalse($this->cutOffTime->validate($validDate,$countryToCheck));
    }

    public function test_valid_country_and_date_and_cut_off_time_passed_yet()
    {
        // return wanted export date in mocked service
        $mockedExportDate = m::mock(ExportDate::class);
        $cutOffDateToTest = Carbon::now()->toDateTimeString();
        $mockedExportDate->shouldReceive('getByCountryAndDeliveryDate')->andReturn($cutOffDateToTest);
        $cutOffTimeService = new CutOffTimeProwito($mockedExportDate);

        $countryToCheck = 'de';

        // create test now date time to be used in service for validation
        $validDate = Carbon::now()->toDateTimeString();
        $cutOffPlusTime = Carbon::now()->setTimeFromTimeString(Helpers::convertTimeFromCETtoUTC(OrderReportProwitoCommand::PROWITO_CUT_OFF_TIME));
        $cutOffPlusTime->addHour(3);

        // set carbon now to test time
        Carbon::setTestNow($cutOffPlusTime);

        $this->assertFalse($cutOffTimeService->validate($validDate,$countryToCheck));
        // to reset carbon to normal time again
        Carbon::setTestNow();
    }

    public function test_valid_country_and_date_and_cut_off_time_not_passed()
    {
        // return wanted export date in mocked service
        $mockedExportDate = m::mock(ExportDate::class);
        $cutOffDateToTest = Carbon::now()->toDateTimeString();
        $mockedExportDate->shouldReceive('getByCountryAndDeliveryDate')->andReturn($cutOffDateToTest);
        $cutOffTimeService = new CutOffTimeProwito($mockedExportDate);

        $countryToCheck = 'de';

        // create test now date time to be used in service for validation
        $validDate = Carbon::now()->toDateTimeString();
        $cutOffMinusTime = Carbon::now()->setTimeFromTimeString(Helpers::convertTimeFromCETtoUTC(OrderReportProwitoCommand::PROWITO_CUT_OFF_TIME));
        $cutOffMinusTime->addHour(-3);

        // set carbon now to test time
        Carbon::setTestNow($cutOffMinusTime);

        $this->assertTrue($cutOffTimeService->validate($validDate,$countryToCheck));
        // to reset carbon to normal time again
        Carbon::setTestNow();
    }

    public function test_valid_country_and_date_and_cut_off_time_exactly_equal_to_cut_off()
    {
        // return wanted export date in mocked service
        $mockedExportDate = m::mock(ExportDate::class);
        $cutOffDateToTest = Carbon::now()->toDateTimeString();
        $mockedExportDate->shouldReceive('getByCountryAndDeliveryDate')->andReturn($cutOffDateToTest);
        $cutOffTimeService = new CutOffTimeProwito($mockedExportDate);

        $countryToCheck = 'de';

        // create test now date time to be used in service for validation
        // set exact the same time as cut off time
        $validDate = Carbon::now()->toDateTimeString();
        $cutOffTime = Carbon::now()->setTimeFromTimeString(Helpers::convertTimeFromCETtoUTC(OrderReportProwitoCommand::PROWITO_CUT_OFF_TIME));

        // set carbon now to test time
        Carbon::setTestNow($cutOffTime);

        $this->assertFalse($cutOffTimeService->validate($validDate,$countryToCheck));
        // to reset carbon to normal time again
        Carbon::setTestNow();
    }
}