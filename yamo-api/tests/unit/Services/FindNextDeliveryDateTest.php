<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 7/2/18
 * Time: 1:15 PM
 */

namespace Tests\Services;

use App\Models\Order;
use Carbon\Carbon;
use Couchbase\UserSettings;
use TestCase;
use App\Models\Subscription;
use App\Models\User;
use App\Services\FindNextDeliveryDate;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Testing\DatabaseTransactions;

class FindNextDeliveryDateTest extends TestCase
{
    use DatabaseTransactions;

    protected $find_next_delivery_date;
    protected $subscription;
    protected $user;

    public function setUp()
    {
        parent::setUp();
        $this->find_next_delivery_date = new FindNextDeliveryDate();
        // create user, pending order with future cancellation deadline and delivery date and connected subscription
        $this->user = factory(User::class)->create();
        $order = factory(Order::class)->create([
            'user_id'=>$this->user->id,
            'status'=>Order::STATUS_PENDING,
            'cancellation_deadline'=> Carbon::now()->addDay(Order::ORDER_FIRSTBOX_CANCELLATION_PERIOD)->toDateTimeString(),
            'delivery_date'=>Carbon::now()->addDay(Order::ORDER_FIRSTBOX_CANCELLATION_PERIOD*Order::DEFAULT_DELIVERY_FREQUENCY)->toDateTimeString()
            ]);
        $this->subscription = factory(Subscription::class)->create([
            'order_id'=>$order->id,
            'user_id'=>$this->user->id,
            'status'=>Subscription::STATUS_PENDING
        ]);
    }

    public function test_profile_next_delivery_date()
    {
        $expected_date = $this->subscription->order->delivery_date->format(FindNextDeliveryDate::DATE_FORMAT);

        $this->assertEquals($expected_date,$this->find_next_delivery_date->next($this->user));
    }

    public function test_subscription_next_delivery_date()
    {
        $expected_date = $this->subscription->order->delivery_date->format(FindNextDeliveryDate::DATE_FORMAT);

        $this->assertEquals($expected_date,$this->find_next_delivery_date->next($this->user,$this->subscription->id));
    }

    public function test_profile_without_subscription()
    {
        $expected_user = factory(User::class)->create();
        $expected_date = null;
        $this->assertEquals($expected_date,$this->find_next_delivery_date->next($expected_user));
    }

    public function test_profile_null_subsciprtion()
    {
        $expected_user = factory(User::class)->create();
        $expected_date = null;
        $this->assertEquals($expected_date,$this->find_next_delivery_date->next($expected_user,null));
    }

    public function test_profile_next_delivery_date_expired_cancellation_deadline()
    {
        // create user,order with expired delivery date and cancellation deadline and connected subscription
        $expected_user = factory(User::class)->create();
        $expected_order = factory(Order::class)->create([
            'user_id'=>$expected_user->id,
            'status'=>Order::STATUS_PENDING,
            'cancellation_deadline'=> Carbon::now()->subDay(Order::ORDER_FIRSTBOX_CANCELLATION_PERIOD*Order::DEFAULT_DELIVERY_FREQUENCY)->toDateTimeString(),
            'delivery_date'=>Carbon::now()->subDay(Order::ORDER_FIRSTBOX_CANCELLATION_PERIOD)->toDateTimeString()
        ]);
        factory(Subscription::class)->create([
            'order_id'=>$expected_order->id,
            'user_id'=>$this->user->id,
            'status'=>Subscription::STATUS_PENDING
        ]);

        $this->assertNull($this->find_next_delivery_date->next($expected_user));
    }

    public function test_subscription_next_delivery_date_expired_cancellation_deadline_with_subscription_param()
    {
        // create user,order with expired delivery date and cancellation deadline and connected subscription
        $expected_user = factory(User::class)->create();
        $expected_order = factory(Order::class)->create([
            'user_id'=>$expected_user->id,
            'status'=>Order::STATUS_PENDING,
            'cancellation_deadline'=> Carbon::now()->subDay(Order::ORDER_FIRSTBOX_CANCELLATION_PERIOD*Order::DEFAULT_DELIVERY_FREQUENCY)->toDateTimeString(),
            'delivery_date'=>Carbon::now()->subDay(Order::ORDER_FIRSTBOX_CANCELLATION_PERIOD)->toDateTimeString()
        ]);
        $expected_subscription = factory(Subscription::class)->create([
            'order_id'=>$expected_order->id,
            'user_id'=>$this->user->id,
            'status'=>Subscription::STATUS_PENDING
        ]);

        $this->assertNull($this->find_next_delivery_date->next($expected_user,$expected_subscription->id));
    }
}