<?php

namespace Tests\Services;

use App\Models\Country;
use App\Models\DeliveryDate;
use App\Services\DeliveryDate\ExportDate;
use Laravel\Lumen\Testing\DatabaseTransactions;
use TestCase;

class ExportDateTest extends TestCase
{
    use DatabaseTransactions;
    protected $exportDate;
    protected $defaultTestDateString;
    protected $defaultTestCountry;

    public function setUp()
    {
        parent::setUp();
        $this->exportDate = new ExportDate();
        $this->defaultTestDateString = '2019-11-10';
        $this->defaultTestCountry = Country::all()->first()->iso_alpha_2;
    }

    public function test_get_by_country_and_delivery_date_empty_both()
    {
        $givenResult = $this->exportDate->getByCountryAndDeliveryDate(null, null);

        $this->assertNull($givenResult);
    }

    public function test_get_by_country_and_delivery_date_empty_country()
    {
        $givenEmptyValues = ['', null, 0, false];
        foreach ($givenEmptyValues as $givenEmptyValue) {
            $givenResult = $this->exportDate->getByCountryAndDeliveryDate($givenEmptyValue, $this->defaultTestDateString);

            $this->assertNull($givenResult);
        }
    }

    public function test_get_by_country_and_delivery_date_empty_delivery_date()
    {
        $givenEmptyValues = ['', null, 0, false];
        foreach ($givenEmptyValues as $givenEmptyValue) {
            $givenResult = $this->exportDate->getByCountryAndDeliveryDate($this->defaultTestCountry, $givenEmptyValue);

            $this->assertNull($givenResult);
        }
    }

    public function test_get_by_country_and_delivery_date_non_existing_country()
    {
        $givenResult = $this->exportDate->getByCountryAndDeliveryDate('fr', $this->defaultTestDateString);

        $this->assertNull($givenResult);
    }

    public function test_get_by_country_and_delivery_date_non_existent_date()
    {
        $givenResult = $this->exportDate->getByCountryAndDeliveryDate($this->defaultTestCountry, $this->defaultTestDateString);

        $this->assertNull($givenResult);
    }

    public function test_get_by_country_and_delivery_date_exist_export_zeros()
    {
        $testDate = '2010-11-01';
        $testCountry = Country::all()->first();
        $givenDeliveryDate = factory(DeliveryDate::class)->create([
            'country_id' => $testCountry->id,
            'delivery_date' => $testDate,
            'disabled' => 0
        ]);
        $givenResult = $this->exportDate->getByCountryAndDeliveryDate($this->defaultTestCountry, $testDate);

        $this->assertNull($givenResult);
    }

    public function test_get_by_country_and_delivery_date_exist_export_disabled()
    {
        $testDate = '2010-11-01';
        $expectedExportDate = '2010-11-04';
        $testCountry = Country::all()->first();
        $givenDeliveryDate = factory(DeliveryDate::class)->create([
            'country_id' => $testCountry->id,
            'delivery_date' => $testDate,
            'export_date' => $expectedExportDate,
            'disabled' => 1
        ]);
        $givenResult = $this->exportDate->getByCountryAndDeliveryDate($this->defaultTestCountry, $testDate);

        $this->assertNull($givenResult);
    }

    public function test_get_by_country_and_delivery_date_exist_export_enabled()
    {
        $testDate = '2010-11-01';
        $expectedExportDate = '2010-11-04 00:00:00';
        $testCountry = Country::all()->first();
        $givenDeliveryDate = factory(DeliveryDate::class)->create([
            'country_id' => $testCountry->id,
            'delivery_date' => $testDate,
            'export_date' => $expectedExportDate,
            'disabled' => 0
        ]);
        $givenResult = $this->exportDate->getByCountryAndDeliveryDate($this->defaultTestCountry, $testDate);

        $this->assertEquals($expectedExportDate, $givenResult);
    }

    public function test_get_by_country_and_delivery_date_exist_bad_input_date_format()
    {
        $testDate = '2010-11-01 00:00:00';
        $expectedExportDate = '2010-11-04 00:00:00';
        $testCountry = Country::all()->first();
        $givenDeliveryDate = factory(DeliveryDate::class)->create([
            'country_id' => $testCountry->id,
            'delivery_date' => $testDate,
            'export_date' => $expectedExportDate,
            'disabled' => 0
        ]);
        $givenResult = $this->exportDate->getByCountryAndDeliveryDate($this->defaultTestCountry, $testDate);

        $this->assertEquals($expectedExportDate, $givenResult);
    }
}