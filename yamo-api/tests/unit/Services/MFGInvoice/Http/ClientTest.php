<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/30/18
 * Time: 1:38 PM
 */
namespace Tests\Services;

use App\Traits\MFGEncode;
use App\Services\MFGInvoice\Http\Client as MFGClient;
use App\Traits\XMLfromArray;
use TestCase;

class ClientTest extends TestCase
{
    use MFGEncode;
    use XMLfromArray;

    public function test_request_call_ch()
    { //TODO: fix this test as is failing due to an error with the ' or " therefore the $data string is incomplete and the request fails.
        $client = new MFGClient('ch');
	    $array = [
		    'externalReference' => '1234987',
		    'orderIpAddress' => '127.0.0.1',
		    'gender' => 'male', //TODO: Eventually we will have to set to female everywhere instead of Frau or Herr.
		    'firstName' => 'Mann',
		    'lastName' => 'Muster',
		    'street' => 'am Bohl 14B',
		    'city' => 'St. Gallen',
		    'zip'  => '9004',
		    'country' => 'CH',
		    'language' => 'de',
		    'email' => 'mustermann@muster.ch',
		    'birthdate' => '1984-02-06',
		    //'phoneNumber' => $this->phoneNumber, //Phone numbers are in the MFG docs but are not needed aparently.
		    //'mobileNumber' => $this->mobileNumber,
		    'merchantId' => config('mfgroup.country.CH.merchant'),
		    'filialId' => config('mfgroup.country.CH.filial'),
		    'terminalId' => config('mfgroup.country.CH.terminal'),
		    'amount' => '20400',
		    'currencyCode' => 'CHF'
	    ];
	    $xml = $this->xml($this->encodetoXML($array, 'cardNumberRequest', ['protocol' => 'getVirtualCardNumber', 'version' => '2.9']));
        $result = $client->request($xml);
        $this->assertObjectHasAttribute('cardNumber', simplexml_load_string($result));

    }
}