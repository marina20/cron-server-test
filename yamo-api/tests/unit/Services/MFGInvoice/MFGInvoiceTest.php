<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 8/27/18
 * Time: 3:02 PM
 */
namespace Tests\Services;

use App\Models\Country;
use App\Models\Order;
use App\Models\User;
use App\Services\CancelSubscriptionService;
use App\Services\MFGInvoice\Http\Client as MFGClient;
use App\Services\MFGInvoice\MFGInvoice;
use App\Services\MFGInvoice\Resource\ConfirmationRequest;
use App\Services\MFGInvoice\Resource\CardNumberRequest;
use App\Services\MFGInvoice\Resource\FinancialRequest;
use App\Services\MFGInvoice\Resource\FinancialRequestObject;
use App\Services\MFGInvoice\Resource\FinancialResponse;
use App\Traits\MFGEncode;
use App\Traits\XMLfromArray;
use Carbon\Carbon;
use Helpers;
use TestCase;

class MFGInvoiceTest extends TestCase
{
    use MFGEncode;
    use XMLfromArray;

    private $order;
    private $cardNumber;
    private $country;
    private $client;

    public function setUp()
    {
        parent::setUp();
        $user = factory(User::class)->create();
        $this->country = Country::where('content_code', Helpers::LOCALE_CH)->first();
        $deliveryDate = Carbon::today()->addDay(Order::ORDER_FIRSTBOX_CANCELLATION_PERIOD * 2);
        $cancellationDate = $deliveryDate->subDay(Order::ORDER_SUBSCRIPTION_CANCELLATION_PERIOD);
        $this->order = factory(Order::class)->create(
            [
                'user_id' => $user->id,
                'payment_method_id' => 'invoice',
                'payment_method_title' => 'Rechnung',
                'shipping_country' => 'Schweiz',
                'billing_country' => 'Schweiz',
                'billing_postcode' => '10115',
                'delivery_date' => $deliveryDate->toDateTimeString(),
                'customer_birthday' => "1973-07-28",
                'customer_ip_address' => '127.0.0.1',
                'child_birthday' => "2018-06-21",
                'is_subscription' => '1',
                'cancellation_deadline' => $cancellationDate->toDateTimeString(),
                'currency' => $this->country->currency,
                'country_id' => $this->country->id,
                'billing_street_nr' => '29',
                'billing_street_name' => 'Ulmenweg',
                'shipping_street_nr' => '29',
                'shipping_street_name' => 'Ulmenweg',
                // 'country' => $this->country,
                'shipping' => '0.00',
                'shipping_tax' => '0.00',
                'order_tax' => '1.51',
                'order_total' => '62.00',
                'order_total_without_tax' => '60.49',
                'order_subtotal' => '62.00',
                'order_subtotal_tax' => '1.51',
                'created_via' => 'subscription',
                'discount' => '0.00',
                'discount_tax' => '0',
                'status' => 'pending'
            ]);

        $this->client = new MFGClient();
        $cardRequest = new CardNumberRequest($this->order);

        $cardRequestResult = $this->client->request($this->xml($cardRequest->getXML()));
        $cardResult = $this->decodeSimpleXMLObject(simplexml_load_string($cardRequestResult));

        $this->cardNumber = isset($cardResult['cardNumber']) ? $cardResult['cardNumber'] : null;

    }

    public function test_check_credit_ch()
    {
        $array = [
            'externalReference' => '1234987',
            'orderIpAddress' => '127.0.0.1',
            'gender' => 'male',
            'firstName' => 'Mann',
            'lastName' => 'Muster',
            'street' => 'am Bohl 14B',
            'city' => 'St. Gallen',
            'zip'  => '9004',
            'country' => 'CH',
            'language' => 'de',
            'email' => 'mustermann@muster.ch',
            'birthdate' => '1984-02-06',
            'merchantId' => config('mfgroup.country.CH.merchant'),
            'filialId' => config('mfgroup.country.CH.filial'),
            'terminalId' => config('mfgroup.country.CH.terminal'),
            'amount' => '20400',
            'currencyCode' => 'CHF'
        ];

        $xml = $this->xml($this->encodetoXML($array, 'cardNumberRequest', ['protocol' => 'getVirtualCardNumber', 'version' => '2.9']));
        $result = $this->client->request($xml);

        $this->assertObjectHasAttribute('cardNumber', simplexml_load_string($result));
    }

    public function test_check_confirmation_ch()
    {
        $array = [
            'Conversation' => [
                'FinancialRequest protocol=PaymentServer_V2_9 msgnum=105' => [
                    'RequestDate' => '20180906140936',
                    'TransactionType' => 'debit',
                    'Currency' => 'CHF',
                    'Amount' => '6200',
                    'ExternalReference' => '8976',
                    'MerchantId' => '17933',
                    'FilialId' => '100',
                    'TerminalId' => '3323',
                ],
                'Response msgnum=105' => [
                    'ResponseCode' => '00',
                    'ResponseDate' => '20180906161236',
                    'AuthorizationCode' => '12345678',
                    'Currency' => 'CHF',
                    'Balance' => '129600',
                    'CardNumber' => 'fsVDHwXr',
                    'ExpirationDate' => '20160901',
                ],
                'DelayedUntil' => '2016-08-31'
            ]
        ];

        $xml = $this->xml($this->encodetoXML($array, 'Confirmation', [
            'msgnum' => '50017werwer8859',
            'gendate' => '2016asdfasdf0721103524',
            'protocol' => 'PaymentServer_V2_9'
        ]));
        $result = $this->client->request($xml);

        $this->assertObjectHasAttribute('CardStatistics', simplexml_load_string($result));
    }

    public function test_card_request()
    {
        $array = [
            'externalReference' => $this->order->id,
            'orderIpAddress' => $this->order->customer_ip_address,
            'gender' => 'male',
            'firstName' => $this->order->billing_first_name,
            'lastName' => $this->order->billing_last_name,
            'street' => $this->order->getBillingStreet(),
            'city' => $this->order->billing_city,
            'zip' => $this->order->billing_postcode,
            'country' => $this->order->country->iso_alpha_2,
            'language' => 'de',
            'email' => $this->order->billing_email,
            'birthdate' => $this->order->customer_birthday,
            'merchantId' => config('mfgroup.country.'.strtoupper($this->country->iso_alpha_2).'.merchant'),
            'filialId' => config('mfgroup.country.'.strtoupper($this->country->iso_alpha_2).'.filial'),
            'terminalId' => config('mfgroup.country.'.strtoupper($this->country->iso_alpha_2).'.terminal'),
            'amount' => $this->order->order_total*100,
            'currencyCode' => $this->order->getCurrencyCode($this->order->currency)
        ];
        $cardRequest = new CardNumberRequest($this->order);

        $this->assertTrue($cardRequest->getCardRequest() === $array);
    }

    public function test_financial_request()
    {
        $financialRequest = new FinancialRequest($this->cardNumber, $this->country->currency, $this->order->order_total*100, $this->order->id, $this->country->iso_alpha_2);

        $this->assertObjectHasAttribute('TransactionType', simplexml_load_string($financialRequest->getXML()));

    }

    public function test_confirmation_request()
    {
        $financialRequest = new FinancialRequest($this->cardNumber, $this->country->currency,$this->order->order_total*100, $this->order->id, $this->country->iso_alpha_2);
        $financialRequestXml = $financialRequest->getXML();
        $financialRequestResult = $this->client->request($this->xml($financialRequestXml));
        $financialResult = $this->decodeSimpleXMLObject(simplexml_load_string($financialRequestResult));
        $financialResponse = new FinancialResponse($financialResult);

        $financialRequestObject = new FinancialRequestObject((array)simplexml_load_string($financialRequestXml));

        $confirmationRequest = new ConfirmationRequest($financialResponse, $financialRequestObject, $this->country->iso_alpha_2);

        $this->assertObjectHasAttribute('Conversation', simplexml_load_string($confirmationRequest->getXML()));
    }

    public function test_process()
    {
        $client = new MFGInvoice(new CancelSubscriptionService());
        $this->assertTrue($client->process($this->order));
    }
}
