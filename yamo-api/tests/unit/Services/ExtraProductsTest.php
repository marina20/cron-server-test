<?php
/**
 * Created by PhpStorm.
 * User: nedjovic
 * Date: 2019-07-18
 * Time: 16:11
 */

namespace Tests\Services;

use App\Models\Category;
use App\Models\ExtraProduct;
use App\Models\ExtraProductLog;
use App\Models\Order;
use App\Models\Product;
use App\Services\ExtraProducts;
use Laravel\Lumen\Testing\DatabaseTransactions;
use TestCase;

class ExtraProductsTest extends TestCase
{
    use DatabaseTransactions;

    protected $extraProductsService;
    protected $givenValidSku;

    public function setUp()
    {
        parent::setUp();
        $givenArrayOfActiveIdentifiers = [
            'R', 'A', 'M', 'H', 'E'
        ];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);
        $this->extraProductsService = new ExtraProducts();

        // alternate sku that better represents base32 used in item count
        $this->givenValidSku = 'K4R00B00A00P00E00Z00M00H00F00L0GS00T00C00D00Y00J00G00I00W00K00U00N00Q00O01X00V00ZB00MB00HB00FB00LB00SB00';
    }

    public function resetActiveExtraProductsTable()
    {
        $givenIdentifiersArray = ExtraProduct::whereHas('product', function ($query){
            $query->whereHas('categories',function ($query) {
                $query->whereIn('name_key', [Product::PRODUCT_CATEGORY_POUCH,Product::PRODUCT_CATEGORY_BREIL]);
            });
        })->pluck('product_code')->toArray();

        foreach ($givenIdentifiersArray as $activeIdentifier) {
            $extraProduct = ExtraProduct::where('product_code',$activeIdentifier)->first();
            $extraProduct->count = ExtraProducts::SKU_ACTIVE_IDENTIFIER_COUNT_COLUMN_DEFAULT_VALUE;
            $extraProduct->save();
        }
    }

    public function activateExtraProducts($activeIdentifiersArray)
    {
        $this->resetActiveExtraProductsTable();
        foreach ($activeIdentifiersArray as $activeIdentifier) {
            $extraProduct = ExtraProduct::where('product_code',$activeIdentifier)->first();
            if(empty($extraProduct))
                return null;
            $extraProduct->count = ExtraProducts::SKU_IDENTIFIER_ITEM_COUNT_DEFAULT_ADDITIONAL_VALUE;
            $extraProduct->save();
        }
    }

    public function test_extra_products_service_exists()
    {
        $this->assertTrue($this->extraProductsService instanceof ExtraProducts);
    }

    public function test_transform_returns_a_string()
    {
        $this->assertIsString($this->extraProductsService->transform($this->givenValidSku));
    }

    public function test_validate_sku()
    {
        $this->assertIsBool($this->extraProductsService->validateSku());
    }

    public function test_retrieve_product_identifiers_array()
    {
        $givenIdentifiersArray = Product::whereHas('categories',function ($query) {
            $query->whereIn('name_key', [Product::PRODUCT_CATEGORY_POUCH,Product::PRODUCT_CATEGORY_BREIL]);
        })->pluck('product_identifier_letter')->toArray();

        $this->assertEquals($givenIdentifiersArray, $this->extraProductsService->getIdentifiersArray());
    }

    public function test_retrieve_existing_product_identifiers()
    {
        $this->assertIsArray($this->extraProductsService->getIdentifiersArray());
    }

    public function test_validate_identifiers_array_false()
    {
        $givenInvalidSku = 'K4R00B00A00P00E00Z02M00H03F02L03S00T00C03C00Y03J00G00I00W00K00U00N00Q00O01X00V00ZB00MB00HB00FB00LB00SB00';
        $this->assertFalse($this->extraProductsService->validateSkuProductIdentifiers($givenInvalidSku));
    }

    public function test_validate_identifiers_array_true()
    {
        $this->assertTrue($this->extraProductsService->validateSkuProductIdentifiers($this->givenValidSku));
    }

    public function test_validate_sku_false_on_empty_string()
    {
        $givenSku = '';
        $this->assertFalse($this->extraProductsService->validateSku($givenSku));
    }

    public function test_validate_sku_false_on_null()
    {
        $givenSku = null;
        $this->assertFalse($this->extraProductsService->validateSku($givenSku));
    }

    public function test_validate_sku_false_on_integer()
    {
        $givenSku = 123;
        $this->assertFalse($this->extraProductsService->validateSku($givenSku));
    }

    public function test_validate_sku_on_string_length_less_than_80()
    {
        $givenSku = 'K4R00B00A00P00E00Z02M00H03F02L03S00T00C03D00Y03J00G00I00W00K00U00N00Q00O01X00ZB00MB00HB00FB00LB00SB00';
        $this->assertFalse($this->extraProductsService->validateSku($givenSku));
    }

    public function test_validate_sku_missing_product_identifier()
    {
        $givenSku = 'K4R00B00A00P00E00Z02M00H03F02L03S00T00C03C00Y03J00G00I00W00K00U00N00Q00O01X00V00ZB00MB00HB00FB00LB00SB00';
        $this->assertFalse($this->extraProductsService->validateSku($givenSku));
    }

    public function test_validate_16_products_in_sku()
    {
        $this->assertTrue($this->extraProductsService->validateSkuNumberOfProducts($this->givenValidSku));
        $this->assertTrue($this->extraProductsService->validateSku($this->givenValidSku));
    }

    public function test_validate_16_products_in_sku_with_error_more_than_default_number    ()
    {
        $givenSku = 'K4R00B00A00P00E00Z02M00H03F02L03S00T00C01D00Y03J00G00I00W00K00U00N00Q00O01X00V00ZB00MB00HB00FB00LB00SB00';
        $this->assertFalse($this->extraProductsService->validateSkuNumberOfProducts($givenSku));
        $this->assertFalse($this->extraProductsService->validateSku($givenSku));
    }

    public function test_validate_16_products_in_sku_with_missing_items()
    {
        $givenSku = 'K4R00B00A00P00E00Z02M00H03F02L03Y03J00G00I00W00K00U00N00Q00O01X00V00ZB00MB00HB00FB00LB00SB00';
        $this->assertFalse($this->extraProductsService->validateSkuNumberOfProducts($givenSku));
        $this->assertFalse($this->extraProductsService->validateSku($givenSku));
    }

    public function test_validate_16_products_in_sku_with_missing_all_items()
    {
        $givenSku = 'K4R00B00A00P00E00Z00M00H00F02L03S00T00C01D00Y03J00G00I00W00K00U00N00Q00O01X00V00ZB00MB00HB00FB00LB00SB00';
        $this->assertFalse($this->extraProductsService->validateSkuNumberOfProducts($givenSku));
        $this->assertFalse($this->extraProductsService->validateSku($givenSku));
    }

    public function test_validate_16_products_in_sku_with_error_less_than_default_number()
    {
        $givenSku = 'K4R00B00A00P00E00Z00M00H00F02L03S00T00C01D00Y03J00G00I00W00K00U00N00Q00O01X00V00ZB00MB00HB00FB00LB00SB00';
        $this->assertFalse($this->extraProductsService->validateSkuNumberOfProducts($givenSku));
        $this->assertFalse($this->extraProductsService->validateSku($givenSku));
    }

    public function test_validate_no_active_extra_products()
    {
        $this->resetActiveExtraProductsTable();
        $this->assertFalse($this->extraProductsService->validateSku($this->givenValidSku));
    }

    public function test_get_active_product_identifiers()
    {
        $givenArrayOfActiveIdentifiers = [
            'R', 'A', 'M', 'H', 'E'
        ];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);
        $this->assertEquals($givenArrayOfActiveIdentifiers, $this->extraProductsService->getActiveIdentifiersArray());
    }

    public function test_get_active_product_identifiers_empty()
    {
        $this->resetActiveExtraProductsTable();
        $this->assertEmpty($this->extraProductsService->getActiveIdentifiersArray());
    }

    public function test_randomize_get_two_random_products_with_quantity_of_one()
    {
        $givenArrayOfActiveIdentifiers = [
            'R', 'A', 'M', 'H', 'E'
        ];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);

        $randomizedArrayOfProducts = $this->extraProductsService->randomize();

        foreach ($randomizedArrayOfProducts as $randomExtraProduct)
        {
            $this->assertTrue(in_array($randomExtraProduct['identifier'], $givenArrayOfActiveIdentifiers));
        }
        $this->assertEquals(ExtraProducts::SKU_TOTAL_OF_EXTRA_PRODUCTS, array_sum(array_column($randomizedArrayOfProducts,'value')));
    }

    public function test_randomize_no_active_extra_products()
    {
        $this->resetActiveExtraProductsTable();
        $this->assertEmpty($this->extraProductsService->randomize());
    }

    public function test_randomize_when_only_one_extra_product_is_available()
    {
        $givenArrayOfActiveIdentifiers = [
            'R'
        ];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);

        $randomizedArrayOfProducts = $this->extraProductsService->randomize();
        foreach ($randomizedArrayOfProducts as $randomExtraProduct)
        {
            $this->assertTrue(in_array($randomExtraProduct['identifier'], $givenArrayOfActiveIdentifiers));
        }
        $this->assertEquals(ExtraProducts::SKU_TOTAL_OF_EXTRA_PRODUCTS, array_sum(array_column($randomizedArrayOfProducts,'value')));
    }

    public function test_randomize_when_only_one_extra_product_is_available_and_it_is_empty_string()
    {
        $givenArrayOfActiveIdentifiers = [
            ''
        ];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);

        $this->assertEmpty($this->extraProductsService->randomize());
    }

    public function test_randomize_when_3_extra_products_are_available()
    {
        $givenArrayOfActiveIdentifiers = [
            'R','M','H'
        ];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);

        $randomizedArrayOfProducts = $this->extraProductsService->randomize();
        foreach ($randomizedArrayOfProducts as $randomExtraProduct)
        {
            $this->assertTrue(in_array($randomExtraProduct['identifier'], $givenArrayOfActiveIdentifiers));
        }
        $this->assertEquals(ExtraProducts::SKU_TOTAL_OF_EXTRA_PRODUCTS, array_sum(array_column($randomizedArrayOfProducts,'value')));
    }

    public function test_randomize_when_1_extra_product_is_available_and_one_empty_string()
    {
        $givenArrayOfActiveIdentifiers = [
            'R',''
        ];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);

        $randomizedArrayOfProducts = $this->extraProductsService->randomize();
        foreach ($randomizedArrayOfProducts as $randomExtraProduct)
        {
            $this->assertTrue(in_array($randomExtraProduct['identifier'], $givenArrayOfActiveIdentifiers));
        }
        $this->assertEquals(ExtraProducts::SKU_TOTAL_OF_EXTRA_PRODUCTS, array_sum(array_column($randomizedArrayOfProducts,'value')));
    }

    public function test_transform_add_extra_products_to_sku()
    {
        $givenArrayOfActiveIdentifiers = [
            'R','M','H'
        ];
        $transformedSku = $this->extraProductsService->transform($this->givenValidSku);
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);
        $this->assertIsString($transformedSku);
        $this->assertEquals(ExtraProducts::SKU_DEFAULT_NUMBER_OF_PRODUCTS + ExtraProducts::SKU_TOTAL_OF_EXTRA_PRODUCTS, $this->extraProductsService->countNumberOfProductsInSku($transformedSku));
    }

    public function test_transform_add_extra_products_to_sku_when_one_extra_product_is_available()
    {
        $givenArrayOfActiveIdentifiers = [
            'M'
        ];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);
        $transformedSku = $this->extraProductsService->transform($this->givenValidSku);
        $this->assertIsString($transformedSku);
        $this->assertEquals(ExtraProducts::SKU_DEFAULT_NUMBER_OF_PRODUCTS + ExtraProducts::SKU_TOTAL_OF_EXTRA_PRODUCTS, $this->extraProductsService->countNumberOfProductsInSku($transformedSku));
    }

    public function test_transform_add_extra_products_to_sku_when_none_extra_product_is_available()
    {
        $this->resetActiveExtraProductsTable();

        $this->assertIsString($this->extraProductsService->transform($this->givenValidSku));
        $this->assertEquals(ExtraProducts::SKU_DEFAULT_NUMBER_OF_PRODUCTS, $this->extraProductsService->countNumberOfProductsInSku($this->extraProductsService->transform($this->givenValidSku)));
        $this->assertEquals($this->givenValidSku, $this->extraProductsService->transform($this->givenValidSku));
    }

    public function test_flyer_identifier_is_added()
    {
        $givenArrayOfActiveIdentifiers = [
            'R','M','H'
        ];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);
        $transformedSku = $this->extraProductsService->transform($this->givenValidSku);
        $flyerCount = (int)substr($transformedSku, stripos($transformedSku,ExtraProducts::SKU_IDENTIFIER_ALWAYS_ADD_FLYER) + ExtraProducts::SKU_IDENTIFIER_ITEM_COUNT_POSITION_ADJUSTMENT, ExtraProducts::SKU_IDENTIFIER_ITEM_COUNT_STRING_LENGTH);
        $this->assertEquals(ExtraProducts::SKU_IDENTIFIER_ITEM_COUNT_DEFAULT_ADDITIONAL_VALUE, $flyerCount);
    }

    public function test_flyer_identifier_is_not_added_when_no_extra_product_is_available()
    {
        $givenArrayOfActiveIdentifiers = [];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);
        $transformedSku = $this->extraProductsService->transform($this->givenValidSku);
        $flyerCount = (int)substr($transformedSku, stripos($transformedSku,ExtraProducts::SKU_IDENTIFIER_ALWAYS_ADD_FLYER) + ExtraProducts::SKU_IDENTIFIER_ITEM_COUNT_POSITION_ADJUSTMENT, ExtraProducts::SKU_IDENTIFIER_ITEM_COUNT_STRING_LENGTH);
        $this->assertEquals(ExtraProducts::SKU_ACTIVE_IDENTIFIER_COUNT_COLUMN_DEFAULT_VALUE, $flyerCount);
    }

    public function test_flyer_identifier_is_added_when_only_one_extra_product_is_available()
    {
        $givenArrayOfActiveIdentifiers = [
            'L'
        ];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);
        $transformedSku = $this->extraProductsService->transform($this->givenValidSku);
        $flyerCount = (int)substr($transformedSku, stripos($transformedSku,ExtraProducts::SKU_IDENTIFIER_ALWAYS_ADD_FLYER) + ExtraProducts::SKU_IDENTIFIER_ITEM_COUNT_POSITION_ADJUSTMENT, ExtraProducts::SKU_IDENTIFIER_ITEM_COUNT_STRING_LENGTH);
        $this->assertEquals(    ExtraProducts::SKU_IDENTIFIER_ITEM_COUNT_DEFAULT_ADDITIONAL_VALUE, $flyerCount);
    }

    public function test_count_number_of_products_in_sku()
    {
        $this->assertEquals(ExtraProducts::SKU_DEFAULT_NUMBER_OF_PRODUCTS, $this->extraProductsService->countNumberOfProductsInSku($this->givenValidSku));
    }

    public function test_increase_count_on_transform()
    {
        $givenArrayOfActiveIdentifiers = [
            'R','M','H'
        ];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);
        $givenIdentifiersCount = ExtraProduct::whereIn('product_code',$givenArrayOfActiveIdentifiers)->sum('used');
        $transformedSku = $this->extraProductsService->transform($this->givenValidSku);
        $transformedIdentifiersCount = ExtraProduct::whereIn('product_code',$givenArrayOfActiveIdentifiers)->sum('used');
        $this->assertEquals($givenIdentifiersCount + ExtraProducts::SKU_TOTAL_OF_EXTRA_PRODUCTS, $transformedIdentifiersCount);
    }

    public function test_increase_count_on_transform_when_one_product_has_more_than_9_items()
    {
        $givenSku = 'K4R00B00A00P00E00Z00M00H00F00L0GS00T00C00D00Y00J00G00I00W00K00U00N00Q00O01X00V00ZB00MB00HB00FB00LB00SB00';
        $givenArrayOfActiveIdentifiers = [
            'L','M'
        ];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);
        $givenIdentifiersCount = ExtraProduct::whereIn('product_code',$givenArrayOfActiveIdentifiers)->sum('used');
        $transformedSku = $this->extraProductsService->transform($givenSku);
        $transformedIdentifiersCount = ExtraProduct::whereIn('product_code',$givenArrayOfActiveIdentifiers)->sum('used');
        $this->assertEquals($givenIdentifiersCount + ExtraProducts::SKU_TOTAL_OF_EXTRA_PRODUCTS, $transformedIdentifiersCount);
        $this->assertEquals($this->extraProductsService->countNumberOfProductsInSku($transformedSku), ExtraProducts::SKU_DEFAULT_NUMBER_OF_PRODUCTS + ExtraProducts::SKU_TOTAL_OF_EXTRA_PRODUCTS);
    }

    public function test_save_transformed_sku_to_log_table()
    {
        $givenArrayOfActiveIdentifiers = [
            'R','M','H'
        ];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);
        $transformedSku = $this->extraProductsService->transform($this->givenValidSku);

        $transformedLog = ExtraProductLog::where('original_sku', $this->givenValidSku)->where('transformed_sku',$transformedSku)->first();
        $this->assertNotEmpty($transformedLog);
    }

    public function test_save_transformed_sku_to_log_table_empty_active_identifiers()
    {
        $givenArrayOfActiveIdentifiers = [];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);
        $transformedSku = $this->extraProductsService->transform($this->givenValidSku);

        $transformedLog = ExtraProductLog::where('original_sku', $this->givenValidSku)->where('transformed_sku',$transformedSku)->first();
        $this->assertEmpty($transformedLog);
    }

    public function test_save_transformed_sku_to_log_table_only_one_extra_product()
    {
        $givenArrayOfActiveIdentifiers = [
            'H'
        ];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);
        $transformedSku = $this->extraProductsService->transform($this->givenValidSku);

        $transformedLog = ExtraProductLog::where('original_sku', $this->givenValidSku)->where('transformed_sku',$transformedSku)->first();
        $this->assertNotEmpty($transformedLog);
    }

    public function test_save_transformed_sku_and_order_id_to_log_table()
    {
        $givenArrayOfActiveIdentifiers = [
            'R','M','H'
        ];
        $givenOrderId = 1;
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);
        ExtraProductLog::query()->delete();
        $transformedSku = $this->extraProductsService->transform($this->givenValidSku, $givenOrderId);
        $transformedLog = ExtraProductLog::where('original_sku', $this->givenValidSku)->where('transformed_sku',$transformedSku)->first();
        $this->assertNotEmpty($transformedLog);
        $this->assertEquals($givenOrderId, $transformedLog->order_id);
    }

    public function test_convert_sku_to_decimal_array()
    {
        $expectedConvertToDecimalArray = $this->extraProductsService->convertToDecimalArray($this->givenValidSku);
        $this->assertNotEmpty($expectedConvertToDecimalArray);
        $this->assertIsArray($expectedConvertToDecimalArray);
        $this->assertArrayHasKey('value', $expectedConvertToDecimalArray[0]);
        $this->assertIsInt($expectedConvertToDecimalArray[0]['value']);
    }

    public function test_convert_base_36_to_decimal()
    {
        $givenBase36 = '0G';
        $expectedDecimal = $this->extraProductsService->convertBase36ToDecimal($givenBase36);
        $this->assertIsInt($expectedDecimal);
    }

    public function test_convert_decimal_to_base_36()
    {
        $expectedBase36 = 'G';
        $givenDecimal = 16;
        $convertedBase36 = $this->extraProductsService->convertDecimalToBase36($givenDecimal);
        $this->assertEquals($expectedBase36, $convertedBase36);
    }

    public function test_transform_consecutive_calls()
    {
        $givenSkuOne = 'K4R00B00A01P00E01Z01M04H03F02L02S02T00C00D00Y00J00G00I00W00K00U00N00Q00O01X00V00ZB00MB00HB00FB00LB00SB00';
        $givenSkuTwo = 'K4R00B02A02P00E02Z00M03H03F00L00S00T00C00D02Y02J00G00I00W00K00U00N00Q00O01X00V00ZB00MB00HB00FB00LB00SB00';
        $givenSkuThree = 'K4R00B00A00P00E00Z00M03H03F00L02S00T00C02D03Y03J00G00I00W00K00U00N00Q00O01X00V00ZB00MB00HB00FB00LB00SB00';
        $givenArrayOfActiveIdentifiers = [
            'C', 'D', 'Y'
        ];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);
        $this->assertEquals($this->extraProductsService->countNumberOfProductsInSku($this->extraProductsService->transform($givenSkuOne)), ExtraProducts::SKU_DEFAULT_NUMBER_OF_PRODUCTS + ExtraProducts::SKU_TOTAL_OF_EXTRA_PRODUCTS);
        $this->assertEquals($this->extraProductsService->countNumberOfProductsInSku($this->extraProductsService->transform($givenSkuTwo)), ExtraProducts::SKU_DEFAULT_NUMBER_OF_PRODUCTS + ExtraProducts::SKU_TOTAL_OF_EXTRA_PRODUCTS);
        $this->assertEquals($this->extraProductsService->countNumberOfProductsInSku($this->extraProductsService->transform($givenSkuThree)), ExtraProducts::SKU_DEFAULT_NUMBER_OF_PRODUCTS + ExtraProducts::SKU_TOTAL_OF_EXTRA_PRODUCTS);
    }

    public function test_convert_base36_to_decimal()
    {
        $base36 = 'G';
        $decimal = 16;
        $this->assertEquals($decimal, $this->extraProductsService->convertBase36ToDecimal($base36));
    }

    public function test_convert_decimal_to_base36()
    {
        $base36 = 'G';
        $decimal = 16;
        $this->assertEquals($base36, $this->extraProductsService->convertDecimalToBase36($decimal));
    }

    public function test_success_transform_save_new_sku_to_orders_table()
    {
        $givenOrder = factory(Order::class)->create(['sku'=>$this->givenValidSku]);
        $transformedSku = $this->extraProductsService->transform($this->givenValidSku, $givenOrder->id);
        $this->assertEquals(ExtraProducts::SKU_DEFAULT_NUMBER_OF_PRODUCTS + ExtraProducts::SKU_TOTAL_OF_EXTRA_PRODUCTS, $this->extraProductsService->countNumberOfProductsInSku($transformedSku));
        $expectedOrder = Order::find($givenOrder->id);
        $this->assertEquals($transformedSku, $expectedOrder->SKU);
    }

    public function test_add_items_to_sku_with_new_identifiers()
    {
        $givenSku = 'K4R00B07A00P00E00Z00M00H00F00L02S01T00C00D00Y00J00G00I00W00K00U00N00Q00O01X00V00ZB00MB00HB02FB02LB02SB00';
        $categoryCups = Category::where('name_key',Product::PRODUCT_CATEGORY_BREIL)->first();
        $zb = factory(Product::class)->create(['category_id'=>$categoryCups->id,'product_identifier_letter'=>'ZB']);
        $mb = factory(Product::class)->create(['category_id'=>$categoryCups->id,'product_identifier_letter'=>'MB']);
        $hb = factory(Product::class)->create(['category_id'=>$categoryCups->id,'product_identifier_letter'=>'HB']);
        $fb = factory(Product::class)->create(['category_id'=>$categoryCups->id,'product_identifier_letter'=>'FB']);
        $lb = factory(Product::class)->create(['category_id'=>$categoryCups->id,'product_identifier_letter'=>'LB']);
        $sb = factory(Product::class)->create(['category_id'=>$categoryCups->id,'product_identifier_letter'=>'SB']);
        factory(ExtraProduct::class)->create(['product_id'=>$zb->id, 'product_code'=>'ZB']);
        factory(ExtraProduct::class)->create(['product_id'=>$mb->id, 'product_code'=>'MB']);
        factory(ExtraProduct::class)->create(['product_id'=>$hb->id, 'product_code'=>'HB']);
        factory(ExtraProduct::class)->create(['product_id'=>$fb->id, 'product_code'=>'FB']);
        factory(ExtraProduct::class)->create(['product_id'=>$lb->id, 'product_code'=>'LB']);
        factory(ExtraProduct::class)->create(['product_id'=>$sb->id, 'product_code'=>'SB']);

        $givenArrayOfActiveIdentifiers = [
            'MB','ZB'
        ];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);
        $givenIdentifiersCount = ExtraProduct::whereIn('product_code',$givenArrayOfActiveIdentifiers)->sum('used');
        $transformedSku = $this->extraProductsService->transform($givenSku);
        $transformedIdentifiersCount = ExtraProduct::whereIn('product_code',$givenArrayOfActiveIdentifiers)->sum('used');
        $this->assertEquals($givenIdentifiersCount + ExtraProducts::SKU_TOTAL_OF_EXTRA_PRODUCTS, $transformedIdentifiersCount);
        $this->assertEquals($this->extraProductsService->countNumberOfProductsInSku($transformedSku), ExtraProducts::SKU_DEFAULT_NUMBER_OF_PRODUCTS + ExtraProducts::SKU_TOTAL_OF_EXTRA_PRODUCTS);
    }

    public function test_original_sku_without_additional_products()
    {
        $givenSku = 'K4R00B07A00P00E00Z00M06H00F00L02S01T00C00D00Y00J00G00I00W00K00U00N00Q00O01X00V00';

        $givenArrayOfActiveIdentifiers = [
            'R','M','L'
        ];
        $this->activateExtraProducts($givenArrayOfActiveIdentifiers);
        $transformedSku = $this->extraProductsService->transform($givenSku);
        $this->assertEquals($this->extraProductsService->countNumberOfProductsInSku($transformedSku), ExtraProducts::SKU_DEFAULT_NUMBER_OF_PRODUCTS);
        $this->assertFalse($this->extraProductsService->validateSku($transformedSku));
    }


    public function test_when_no_active_extra_products()
    {
        $this->resetActiveExtraProductsTable();
        $transformedSku = $this->extraProductsService->transform($this->givenValidSku);
        $this->assertEquals($this->givenValidSku, $transformedSku);
    }

    public function test_validate_sku_when_no_active_extra_products()
    {
        $this->resetActiveExtraProductsTable();
        $this->assertFalse($this->extraProductsService->validateSku($this->givenValidSku));
    }

}