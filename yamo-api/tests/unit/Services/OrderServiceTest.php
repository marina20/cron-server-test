<?php
/**
 * Created by PhpStorm.
 * User: milenabasaragin
 * Date: 10/5/18
 * Time: 11:40 AM
 */

namespace Tests\Services;

use App\Models\Order;
use App\Models\Subscription;
use App\Services\OrderService;
use Carbon\Carbon;
use TestCase;

class OrderServiceTest extends TestCase
{
    public function test_change_delivery_date()
    {
        $newDeliveryDate = Carbon::now();
        $deliveryDate = $newDeliveryDate->subDay(3);
        $order = factory(Order::class)->create(['delivery_date' => $deliveryDate]);
        $subscription = factory(Subscription::class)->create(['order_id' => $order->id]);
        OrderService::changeDeliveryDate($order, $newDeliveryDate);

        $this->assertTrue(!$newDeliveryDate->diffInDays($order->delivery_date) &&
            !$order->subscription->schedule_next_payment->diffInDays($newDeliveryDate->copy()
                    ->subDays(Subscription::SCHEDULE_NEXT_PAYMENT_DAYS)) &&
            !$order->cancellation_deadline->diffInDays($deliveryDate->copy()
            ->subDays(Order::ORDER_SUBSCRIPTION_CANCELLATION_PERIOD)));


    }
}
