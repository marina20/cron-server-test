<?php
namespace Tests\Controllers;

use App\Models\Order;
use App\Models\Category;
use App\Models\OrderItem;
use App\Models\OrderType;
use App\Models\Subscription;
use App\Models\SuggestedBox;
use App\Models\VatGroup;
use App\Models\Product;
use App\Models\ProductRegion;
use App\Models\User;
use App\Models\Voucher;
use App\Models\VoucherAttempts;
use App\Models\VoucherRedemption;
use App\Models\VoucherRestriction;
use Carbon\Carbon;
use Laravel\Lumen\Testing\DatabaseTransactions;
use TestCase;
use App\Services\CreateNextOrderFrom\CreateNextOrderFromSubscription;
use App\Services\DuplicateOrder\DuplicateOrder;
use App\Services\CreateOrder\CreateOrderSubscription;
use App\Services\CreateAdyenLinkService;
use App\Services\CreateInvoiceLinkService;
use App\Services\CreatePaypalLinkService;
use App\Services\CreateSofortLinkService;
use App\Services\MFGInvoice\MFGInvoice;
use App\Services\UpdateProfileService;
use App\Services\SubscriptionService;
use Tymon\JWTAuth\JWTAuth;
use App\Services\CancelSubscriptionService;

class VoucherTest extends TestCase
{

    public $testProduct;
    public $create_next_order_from_subscription;
    use DatabaseTransactions;
    public function setUp()
    {
        parent::setUp();
        $this->create_next_order_from_subscription = new CreateNextOrderFromSubscription(new DuplicateOrder(), new CreateOrderSubscription(
            new CreateAdyenLinkService(),
            new CreateInvoiceLinkService(),
            new MFGInvoice(new CancelSubscriptionService()),
            new CreatePaypalLinkService(),
            new CreateSofortLinkService(),
            new UpdateProfileService(),
            new SubscriptionService(),
            $this->createMock(JWTAuth::class)
        ));
    }
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $categoryTest = Category::firstOrCreate(['name_key'=>'test']);
        $this->testProduct = Product::firstOrCreate([
            'category_id' => $categoryTest->id,
            'name_key' => 'Testa',
            'position' => '100'
        ]);
        $vatGroupTest = VatGroup::firstOrCreate(['value'=>'10.00','country_id'=>1]);
        ProductRegion::firstOrCreate(['product_id' => $this->testProduct->id,'price' => '100.00','region_id' => 1, 'vat_group_id'=>$vatGroupTest->id, 'active'=>1]);

    }

    /*
     * Test-data is viewable in database/seeds/VoucherTableSeeder.php
     */
     public function testRelations()
     {
         $searchedVoucher = Voucher::where(['code' => "TEST_FIRST_SUBSCRIPTION_CH"])->first();
         $this->assertNotNull($searchedVoucher->restrictions);
     }

     public function testPercentage()
     {
         // LOL, i think i can save this one.. the excepted result is one of the params XD
         // Voucher::getDiscountByAmountOf(100,20);
         $this->assertEquals(Voucher::getDiscountByPercentOf(100.00,20.00), 20.00);
     }

     public function testSubscription()
     {
         app('translator')->setLocale('de-de');
         $user = factory(\App\Models\User::class)->make();
         $this->be($user);
         $code = "TEST_SUBSCRIPTION_CH";
         $order = $this->getOrder($user->id);
         $v = new Voucher;
         $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
         $infoDiscount = Voucher::getDiscountedAmount($code,$order->order_total);
         $voucherResult = Voucher::applyVoucher($code,$order);
         $order = $voucherResult['order'];
         $success = $voucherResult['success'];
         $voucherAttempt = VoucherAttempts::where(['code' => $code])->first();
         $this->assertEquals("success",$voucherAttempt->status);
         $this->assertEquals("90.00",$order->order_total);
         $this->assertEquals("10.00",$order->discount);
         $this->assertEquals(10,$infoDiscount['discount']);
         $this->assertEquals(10,$infoDiscount['percent']);
         //$this->assertEquals("85.00",$order->order_total_without_tax);
     }

    public function testFirstSubscription()
    {
        app('translator')->setLocale('de-de');
        $user = factory(\App\Models\User::class)->make();
        $this->be($user);
        $order = $this->getOrder($user->id);
        $v = new Voucher;
        $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
        $code = "TEST_FIRST_SUBSCRIPTION_CH";
        $infoDiscount = Voucher::getDiscountedAmount($code,$order->order_total);
        $voucherResult = Voucher::applyVoucher($code,$order);
        $order = $voucherResult['order'];
        $success = $voucherResult['success'];
        $voucherAttempt = VoucherAttempts::where(['code' => $code])->first();
        $this->assertEquals("success",$voucherAttempt->status);
        $this->assertEquals(30,$infoDiscount['discount']);
        $this->assertEquals(30,$infoDiscount['percent']);
        $this->assertEquals("70.00",$order->order_total);
        $this->assertEquals("30.00",$order->discount);
        //$this->assertEquals("65.00",$order->order_total_without_tax);
    }

    public function testFrequencyRestrictionRecurring()
    {
        app('translator')->setLocale('de-de');
        $user = factory(\App\Models\User::class)->create();
        $profile = factory(\App\Models\Profile::class)->create(['user_id' => $user->id]);
        $this->be($user);
        $order = $this->getOrder($user->id);
        $suggestedBox = SuggestedBox::first();
        $order->custom_box_id = $suggestedBox->box_id;
        $v = new Voucher;
        $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
        $code = "FREQUENCYRESTRICTION";
        $subscription = factory(\App\Models\Subscription::class)->make();
        $subscription->billing_period = 'week';
        $subscription->billing_interval = '1';
        $subscription->order_id = $order->id;
        $subscription->save();
        $order->subscription_id = $subscription->id;
        $order->save();
        $infoDiscount = Voucher::getDiscountedAmount($code,$order->order_total);
        $voucherResult = Voucher::applyVoucher($code,$order);
        $order = $voucherResult['order'];
        $success = $voucherResult['success'];
        $voucherAttempt = VoucherAttempts::where(['code' => $code, 'order_id' => $order->id])->first();
        $this->assertEquals("success",$voucherAttempt->status);
        $order1 = $this->create_next_order_from_subscription->create($order);
        //var_dump($order1); die();
        $order1->save();
        $subscription->order_id = $order1->id;
        $subscription->save();

        $voucherAttempt = VoucherAttempts::where(['code' => $code, 'order_id' => $order1->id])->first();
        $this->assertEquals("success",$voucherAttempt->status);
        $subscription->billing_interval = '3';
        $subscription->order_id = $order1->id;
        $subscription->save();
        $order1 = $this->create_next_order_from_subscription->create($order1);
        //var_dump($order1); die();
        $order1->save();
        $voucherAttempt = VoucherAttempts::where(['code' => $code, 'order_id' => $order1->id])->first();
        $this->assertNull($voucherAttempt);

        //$this->assertEquals("65.00",$order->order_total_without_tax);
    }

    public function testFrequencyRestriction()
    {
        app('translator')->setLocale('de-de');
        $user = factory(\App\Models\User::class)->make();
        $this->be($user);
        $order = $this->getOrder($user->id);

        $suggestedBox = SuggestedBox::first();
        $order->custom_box_id = $suggestedBox->box_id;
        $v = new Voucher;
        $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
        $code = "FREQUENCYRESTRICTION";
        $subscription = factory(\App\Models\Subscription::class)->make();
        $subscription->billing_period = 'week';
        $subscription->billing_interval = '1';
        $subscription->order_id = $order->id;
        $subscription->save();
        $order->subscription_id = $subscription->id;
        $order->save();
        $infoDiscount = Voucher::getDiscountedAmount($code,$order->order_total);
        $voucherResult = Voucher::applyVoucher($code,$order);
        $order = $voucherResult['order'];
        $success = $voucherResult['success'];
        $voucherAttempt = VoucherAttempts::where(['code' => $code, 'order_id' => $order->id])->first();
        $this->assertEquals("success",$voucherAttempt->status);

        $order = $this->getOrder($user->id);

        $order->subscription_id = $subscription->id;
        $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
        $order->save();
        $subscription->billing_interval = '3';
        $subscription->order_id = $order->id;
        $subscription->save();
        $infoDiscount = Voucher::getDiscountedAmount($code,$order->order_total);
        $voucherResult = Voucher::applyVoucher($code,$order);
        $order = $voucherResult['order'];
        $success = $voucherResult['success'];
        $voucherAttempt = VoucherAttempts::where(['code' => $code, 'order_id' => $order->id])->first();
        $this->assertNotEquals("success",$voucherAttempt->status);

    }


    public function testProductPercentVoucher()
    {
        app('translator')->setLocale('de-de');
        $user = factory(\App\Models\User::class)->create();
        $this->be($user);
        $order = $this->getOrder($user->id);
        $v = new Voucher;
        $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
        $code = "PRODUCT_PERCENT_TESTER";
        $infoDiscount = Voucher::getDiscountedAmount($code,$order->order_total);
        $voucherResult = Voucher::applyVoucher($code,$order);
        $order = $voucherResult['order'];
        $success = $voucherResult['success'];
        $voucherAttempt = VoucherAttempts::where(['code' => $code])->first();
        $addedProduct = null;
        $this->assertEquals(2,count($order->items));
        foreach($order->items as $orderItem){
            $product = $orderItem->product;
            if($product->product_identifier_letter=='B'){
                $addedProduct = $product;
            }
        }
        $this->assertNotNull($addedProduct);
        $this->assertEquals("success",$voucherAttempt->status);
        $this->assertEquals(10,$infoDiscount['discount']);
        $this->assertEquals(10,$infoDiscount['percent']);
        $this->assertEquals("90.00",$order->order_total);
        $this->assertEquals("10.00",$order->discount);
        //$this->assertNull($order->discount);
        //$this->assertEquals("95.00",$order->order_total_without_tax);
    }

        public function testProductVoucher()
        {
            app('translator')->setLocale('de-de');
            $user = factory(\App\Models\User::class)->create();
            $this->be($user);
            $order = $this->getOrder($user->id);
            $v = new Voucher;
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            $code = "PRODUCT_TESTER";
            $infoDiscount = Voucher::getDiscountedAmount($code,$order->order_total);
            $voucherResult = Voucher::applyVoucher($code,$order);
            $order = $voucherResult['order'];
            $success = $voucherResult['success'];
            $voucherAttempt = VoucherAttempts::where(['code' => $code])->first();
            $addedProduct = null;
            $this->assertEquals(2,count($order->items));
            foreach($order->items as $orderItem){
                $product = $orderItem->product;
                if($product->product_identifier_letter=='B'){
                    $addedProduct = $product;
                }
            }
            $this->assertNotNull($addedProduct);
            $this->assertEquals("success",$voucherAttempt->status);
            $this->assertEquals(0,$infoDiscount['discount']);
            $this->assertEquals(0,$infoDiscount['percent']);
            $this->assertEquals("100.00",$order->order_total);
            $this->assertNull($order->discount);
            //$this->assertEquals("95.00",$order->order_total_without_tax);
        }

        public function testCountryRestriction(){
            app('translator')->setLocale('de-de');
            // Voucherrestriction is set to 5 max uses

            $user = User::where(['email' => 'tester@test.com'])->first();
            $this->be($user);
            $order = $this->getOrder($user->id);
            $v = new Voucher;
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            $result = Voucher::applyVoucher('COUNTRY',$order);
            $this->assertEquals(true,$result['success']);

            $order = $this->getOrder($user->id);
            $order->country_id = 2;
            $order->user_id = $user->id;
            $result = Voucher::applyVoucher('COUNTRY',$order);
            $this->assertEquals(false,$result['success']);
        }

        public function testMaxUsesRestriction(){
            app('translator')->setLocale('de-de');
            // Voucherrestriction is set to 5 max uses

            $user = User::where(['email' => 'tester@test.com'])->first();
            $this->be($user);
            $v = new Voucher;
            $order = $this->getOrder($user->id);
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            $result = Voucher::applyVoucher('MAXUSES',$order);
            $redemptions = VoucherRedemption::where(['invalid' => 0])->where(['user_id' => $user->id])->get();

            foreach($redemptions as $redemption){
                $redemption->confirmed=1;
                $redemption->save();
            }
            $this->assertEquals(true,$result['success']);
            $order = $this->getOrder($user->id);
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            $result = Voucher::applyVoucher('MAXUSES',$order);
            $redemptions = VoucherRedemption::where(['invalid' => 0])->where(['user_id' => $user->id])->get();

            foreach($redemptions as $redemption){
                $redemption->confirmed=1;
                $redemption->save();
            }
            $this->assertEquals(true,$result['success']);
            $order = $this->getOrder($user->id);
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            $result = Voucher::applyVoucher('MAXUSES',$order);
            $redemptions = VoucherRedemption::where(['invalid' => 0])->where(['user_id' => $user->id])->get();

            foreach($redemptions as $redemption){
                $redemption->confirmed=1;
                $redemption->save();
            }
            $this->assertEquals(true,$result['success']);

            $user = User::where(['email' => 'ref@test.com'])->first();
            $this->be($user);
            $order = $this->getOrder($user->id);
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            $result = Voucher::applyVoucher('MAXUSES',$order);
            $redemptions = VoucherRedemption::where(['invalid' => 0])->where(['user_id' => $user->id])->get();

            foreach($redemptions as $redemption){
                $redemption->confirmed=1;
                $redemption->save();
            }
            $this->assertEquals(true,$result['success']);
            $order = $this->getOrder($user->id);
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            $result = Voucher::applyVoucher('MAXUSES',$order);
            $redemptions = VoucherRedemption::where(['invalid' => 0])->where(['user_id' => $user->id])->get();

            foreach($redemptions as $redemption){
                $redemption->confirmed=1;
                $redemption->save();
            }
            $this->assertEquals(true,$result['success']);

            $redemptionCount = VoucherRedemption::where(['confirmed' => 1])->where(['invalid' => 0])->count();
            $order = $this->getOrder($user->id);
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            $result = Voucher::applyVoucher('MAXUSES',$order);
            $this->assertEquals(false,$result['success']);
        }

        private function getOrder($userId){
            $order = new Order;
            $order->order_total = "100.00";
            $order->order_total_without_tax = "95.00";
            $order->region_id = 1;
            $order->country_id = 1;
            $order->user_id = $userId;
            $order->cancellation_deadline = Carbon::now()->addDays(5);
            $order->save();
            return $order;
        }

        private function addProducts($orderId){
            $pirateProduct = Product::where(['product_identifier_letter' => 'P'])->first();
            $order_items_1 = factory(OrderItem::class)->create([
                "item_name"=> "Anything",
                "item_type"=> "line_item",
                "order_id"=> $orderId,
                "product_id"=> $pirateProduct->id,
                "qty"=> "16",
                "item_price"=>"6.25",
                "total"=> "100.00",
                "total_tax"=> "1.51",
                "subtotal"=> "62.00",
                "subtotal_tax"=> "1.51",
                "total_discount"=> "0",
                "total_discount_tax"=> "0",
                "tax_rate"=> $pirateProduct->tax_percentage,
                "created_at"=> "2018-07-06 13:17:16",
                "updated_at"=> "2018-07-06 13:17:16"
            ]);
        }

        public function testUseCombination(){
            app('translator')->setLocale('de-de');
            // Voucherrestriction is set to 2 uses per user

            $user = User::where(['email' => 'tester@test.com'])->first();
            $this->be($user);
            $order = $this->getOrder($user->id);
            $v = new Voucher;
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            $result = Voucher::applyVoucher('USESCOMBO',$order);
            $this->assertEquals(true,$result['success']);
            $redemptions = VoucherRedemption::where(['invalid' => 0])->where(['user_id' => $user->id])->get();

            foreach($redemptions as $redemption){
                $redemption->confirmed=1;
                $redemption->save();
            }
            $result = Voucher::applyVoucher('USESCOMBO',$order);
            $redemptions = VoucherRedemption::where(['invalid' => 0])->where(['user_id' => $user->id])->get();

            foreach($redemptions as $redemption){
                $redemption->confirmed=1;
                $redemption->save();
            }
            $this->assertEquals(true,$result['success']);
            $result = Voucher::applyVoucher('USESCOMBO',$order);
            $redemptions = VoucherRedemption::where(['invalid' => 0])->where(['user_id' => $user->id])->get();

            foreach($redemptions as $redemption){
                $redemption->confirmed=1;
                $redemption->save();
            }
            $this->assertEquals(false,$result['success']);

            $user = User::where(['email' => 'ref@test.com'])->first();
            $this->be($user);
            $order = $order = $this->getOrder($user->id);
            $v = new Voucher;
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            $result = Voucher::applyVoucher('USESCOMBO',$order);
            $redemptions = VoucherRedemption::where(['invalid' => 0])->where(['user_id' => $user->id])->get();

            foreach($redemptions as $redemption){
                $redemption->confirmed=1;
                $redemption->save();
            }
            $this->assertEquals(true,$result['success']);
            $result = Voucher::applyVoucher('USESCOMBO',$order);
            $redemptions = VoucherRedemption::where(['invalid' => 0])->where(['user_id' => $user->id])->get();

            foreach($redemptions as $redemption){
                $redemption->confirmed=1;
                $redemption->save();
            }
            $this->assertEquals(true,$result['success']);
            $result = Voucher::applyVoucher('USESCOMBO',$order);
            $redemptions = VoucherRedemption::where(['invalid' => 0])->where(['user_id' => $user->id])->get();

            foreach($redemptions as $redemption){
                $redemption->confirmed=1;
                $redemption->save();
            }
            $this->assertEquals(false,$result['success']);

            $user = User::where(['email' => 'referalfriend@test.com'])->first();
            $this->be($user);
            $order = $order = $this->getOrder($user->id);
            $v = new Voucher;
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            $result = Voucher::applyVoucher('USESCOMBO',$order);
            $this->assertEquals(false,$result['success']);
        }

    public function testRecurringSpecialsVoucherRestriction(){
        app('translator')->setLocale('de-de');
        // Voucherrestriction is set to 2 uses per user
        $user = User::where(['email' => 'tester@test.com'])->first();
        $this->be($user);
        $order = $order = $this->getOrder($user->id);
        $orderType = OrderType::where('type','=','B2C')->first();
        $v = new Voucher;
        $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
        $order->order_type_id = $orderType->id;
        $result = Voucher::applyVoucher('ONORDERNUMBER',$order);
        $success = $result['success'];
        $this->assertEquals(true, $success);

        $order = $this->getOrder($user->id);
        $v = new Voucher;
        $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
        $result = Voucher::applyVoucher('ONORDERNUMBERNONFIRST',$order);
        $success = $result['success'];
        $this->assertEquals(false, $success);
    }

    public function testOrderTypeVoucherRestriction(){
        app('translator')->setLocale('de-de');
        // Voucherrestriction is set to 2 uses per user
        $user = User::where(['email' => 'tester@test.com'])->first();
        $this->be($user);
        $order = $order = $this->getOrder($user->id);
        $orderType = OrderType::where('type','=','B2C')->first();
        $v = new Voucher;
        $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
        $order->order_type_id = $orderType->id;
        $result = Voucher::applyVoucher('ORDERTYPESINGLE',$order);
        $success = $result['success'];
        $this->assertEquals(true, $success);
        $order = $order = $this->getOrder($user->id);
        $orderType = OrderType::where('type','=','VIP')->first();
        $order->order_type_id = $orderType->id;
        $v = new Voucher;
        $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
        $order->is_subscription=1;
        $result = Voucher::applyVoucher('ORDERTYPESINGLE',$order);
        $success = $result['success'];
        $this->assertEquals(false, $success);
    }


    public function testMinProductsNeededRestriction(){
        app('translator')->setLocale('de-de');
        // Voucherrestriction is set to 2 uses per user
        $user = User::where(['email' => 'tester@test.com'])->first();
        $this->be($user);
        $pirateProduct = Product::where(['product_identifier_letter' => 'P'])->first();
        $order = $order = $this->getOrder($user->id);
        $order->order_total = "400.00";
        $orderType = OrderType::where('type','=','B2C')->first();
        $v = new Voucher;
        $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>4]]);
        $order->order_type_id = $orderType->id;
        $result = Voucher::applyVoucher('NEED4PRODUCTS',$order);
        $success = $result['success'];
        $this->assertEquals(true, $success);

        $order = $order = $this->getOrder($user->id);
        $order->order_total = "400.00";
        $orderType = OrderType::where('type','=','B2C')->first();
        $v = new Voucher;
        $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>4]]);
        $order->order_type_id = $orderType->id;
        $result = Voucher::applyVoucher('NEED4PRODUCTS2PIRATES',$order);
        $success = $result['success'];
        $this->assertEquals(false, $success);

        $order = $order = $this->getOrder($user->id);
        $order->order_total = "400.00";
        $orderType = OrderType::where('type','=','B2C')->first();
        $v = new Voucher;
        $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>4]]);
        $order->order_type_id = $orderType->id;
        $result = Voucher::applyVoucher('NEED4PRODUCTS2PIRATES',$order);
        $success = $result['success'];
        $this->assertEquals(false, $success);


        $order = $order = $this->getOrder($user->id);
        $order->order_total = "400.00";
        $orderType = OrderType::where('type','=','B2C')->first();
        $v = new Voucher;
        $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>2]]);
        $order = $v->createOrderItems($order, [['id'=>$pirateProduct->id, 'quantity'=>2]]);
        $order->order_type_id = $orderType->id;
        $result = Voucher::applyVoucher('NEED4PRODUCTS2PIRATES',$order);
        $success = $result['success'];
        $this->assertEquals(true, $success);

        $order = $order = $this->getOrder($user->id);
        $order->order_total = "400.00";
        $orderType = OrderType::where('type','=','B2C')->first();
        $v = new Voucher;
        $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>3]]);
        $order = $v->createOrderItems($order, [['id'=>$pirateProduct->id, 'quantity'=>1]]);
        $order->order_type_id = $orderType->id;
        $result = Voucher::applyVoucher('NEED4PRODUCTS2PIRATES',$order);
        $success = $result['success'];
        $this->assertEquals(false, $success);

        $order = $order = $this->getOrder($user->id);
        $order->order_total = "400.00";
        $orderType = OrderType::where('type','=','B2C')->first();
        $v = new Voucher;
        $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
        $order = $v->createOrderItems($order, [['id'=>$pirateProduct->id, 'quantity'=>3]]);
        $order->order_type_id = $orderType->id;
        $result = Voucher::applyVoucher('NEED4PRODUCTS2PIRATES',$order);
        $success = $result['success'];
        $this->assertEquals(true, $success);
    }

    public function testCustomerVoucherRestriction(){
        app('translator')->setLocale('de-de');
        // Voucherrestriction is set to 2 uses per user
        $user = User::where(['email' => 'tester@test.com'])->first();
        $this->be($user);
        $order = $order = $this->getOrder($user->id);
        $orderType = OrderType::where('type','=','B2C')->first();
        $v = new Voucher;
        $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
        $order->order_type_id = $orderType->id;
        $result = Voucher::applyVoucher('CUSTOMERTYPE',$order);
        $success = $result['success'];
        $this->assertEquals(true, $success);
        $order = $order = $this->getOrder($user->id);
        $orderType = OrderType::where('type','=','VIP')->first();
        $order->order_type_id = $orderType->id;
        $v = new Voucher;
        $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
        $result = Voucher::applyVoucher('CUSTOMERTYPE',$order);
        $success = $result['success'];
        $this->assertEquals(false, $success);
        /*foreach($order->items as $item){
            $item->delete();
        }
        VoucherRedemption::where(['order_id' => $order->id])->delete();
        VoucherAttempts::where(['order_id' => $order->id])->delete();
        $order->delete();*/
    }

        public function testRichVoucherRestriction(){
            app('translator')->setLocale('de-de');
            // Voucherrestriction is set to 2 uses per user

            $user = User::where(['email' => 'tester@test.com'])->first();
            $this->be($user);
            $order = $order = $this->getOrder($user->id);
            $v = new Voucher;
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            $result = Voucher::applyVoucher('RICH',$order);
            $this->assertEquals("0", $order->order_total);
            $this->assertEquals("100.00",$order->discount);
            $this->assertEquals("0",$order->order_total_without_tax);
        }

        public function testUsesPerUserRestriction(){
            app('translator')->setLocale('de-de');
            // Voucherrestriction is set to 2 uses per user

            $user = User::where(['email' => 'tester@test.com'])->first();
            $this->be($user);
            $order = $this->getOrder($user->id);
            $v = new Voucher;
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            $result = Voucher::applyVoucher('UPU',$order);

            $voucher = Voucher::where(['code' => 'UPU'])->first();
            \App\Models\VoucherRedemption::create(['voucher_id' => $voucher->id,'user_id' => $user->id]);
            $this->assertEquals(true,$result['success']);
            $order->paid_date = '2019-09-26 14:37:59';
            $order->status='payed';
            $order->save();
            $redemptions = VoucherRedemption::where(['invalid' => 0])->where(['user_id' => $user->id])->get();
            foreach($redemptions as $redemption){
                $redemptions->confirmed=1;
                $redemption->save();
            }
            $order1 = $this->getOrder($user->id);
            $result = Voucher::applyVoucher('UPU',$order1);
            $this->assertEquals(true,$result['success']);
            $order1->paid_date = '2019-09-26 14:37:59';
            $order1->status='payed';
            $order1->save();

            \App\Models\VoucherRedemption::create(['voucher_id' => $voucher->id,'user_id' => $user->id]);

            //!!!!!!!!!!!!!!!!!!!!
            // This does not work cause redemptions are saved too temporary? at least, it never goes up with the redemptions-count (search stay at 0)
            $order2 = $this->getOrder($user->id);
            $redemptions1 = VoucherRedemption::where(['invalid' => 0])->where(['user_id' => $user->id])->get();
            foreach($redemptions1 as $redemption){
                $redemption['confirmed']=true;
                $redemption->save();
            }
            $result = Voucher::applyVoucher('UPU',$order2);
            $this->assertEquals(false,$result['success']);
            $order2->paid_date = '2019-09-26 14:37:59';
            $order2->save();
            $redemptions2 = VoucherRedemption::where(['invalid' => 0])->where(['user_id' => $user->id])->get();
            foreach($redemptions2 as $redemption){
                $redemption->confirmed=1;
                $redemption->save();
            }
            $user = User::where(['email' => 'ref@test.com'])->first();
            $this->be($user);
            $order3 = $this->getOrder($user->id);
            $result = Voucher::applyVoucher('UPU',$order3);
            $this->assertEquals(true,$result['success']);
            $redemptions = VoucherRedemption::where(['invalid' => 0])->where(['user_id' => $user->id])->get();
            foreach($redemptions as $redemption){
                $redemption->confirmed=1;
                $redemption->save();
            }
            $order3 = $this->getOrder($user->id);
            $result = Voucher::applyVoucher('UPU',$order3);
            $this->assertEquals(true,$result['success']);
            $order3 = $this->getOrder($user->id);
            $redemptions = VoucherRedemption::where(['invalid' => 0])->where(['user_id' => $user->id])->get();

            foreach($redemptions as $redemption){
                $redemption->confirmed=1;
                $redemption->save();
            }
            $result = Voucher::applyVoucher('UPU',$order3);

            $this->assertEquals(false,$result['success']);
        }

        public function testAgeRestriction(){
            app('translator')->setLocale('de-de');
            // Just old enough
            // $user = factory(\App\Models\User::class)->make(['customer_birthday' => "2000-01-01 00:00:00"]);
            $user = User::where(['email' => 'tester@test.com'])->first();
            $this->be($user);
            $order = $order = $this->getOrder($user->id);
            $v = new Voucher;
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            $result = Voucher::applyVoucher('AGERESTRICTION',$order);
            $this->assertEquals(true,$result['success']);


            // Too young
            // $user = factory(\App\Models\User::class)->make(['customer_birthday' => "2015-01-01 00:00:00"]);
            $user = User::where(['email' => 'ref@test.com'])->first();
            $this->be($user);
            $order = $order = $this->getOrder($user->id);
            $v = new Voucher;
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            $result = Voucher::applyVoucher('AGERESTRICTION',$order);
            $this->assertEquals(false,$result['success']);

            // More than old enough
            //$user = factory(\App\Models\User::class)->make(['customer_birthday' => "1968-01-01 00:00:00"]);
            $user = User::where(['email' => 'referalfriend@test.com'])->first();
            $this->be($user);
            $order = $order = $this->getOrder($user->id);
            $v = new Voucher;
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            $result = Voucher::applyVoucher('AGERESTRICTION',$order);
            $this->assertEquals(true,$result['success']);
        }

        public function testDefaultVoucher()
        {
            app('translator')->setLocale('de-de');
            $user = factory(\App\Models\User::class)->create();
            $this->be($user);
            $order = $this->getOrder($user->id);
            $v = new Voucher;
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            // discount is amount of 2050;
            $code = "TWENTY_AND_AHALF";
            $infoDiscount = Voucher::getDiscountedAmount($code,$order->order_total);
            $voucherResult = Voucher::checkVoucher($code,$order);
            $order = $voucherResult['order'];
            $success = $voucherResult['success'];
            $voucherAttempt = VoucherAttempts::where(['code' => $code])->first();
            $this->assertNull($voucherAttempt);
            $this->assertEquals(20.5,$infoDiscount['discount']);
            $this->assertEquals(20.5,$infoDiscount['percent']);
            $this->assertEquals("79.50", $order->order_total);
            $this->assertEquals("20.50",$order->discount);
            //$this->assertEquals("74.50",$order->order_total_without_tax);

            $order = $this->getOrder($user->id);
            $order = $v->createOrderItems($order, [['id'=>$this->testProduct->id, 'quantity'=>1]]);
            $v = new Voucher;
            // discount is amount of 2050;
            $code = "TWENTY_AND_AHALF";
            $infoDiscount = Voucher::getDiscountedAmount($code,$order->order_total);
            $voucherResult = Voucher::applyVoucher($code,$order);
            $order = $voucherResult['order'];
            $success = $voucherResult['success'];
            $voucherAttempt = VoucherAttempts::where(['code' => $code])->first();
            foreach($voucherResult['order']->items as $item){
                $this->assertEquals(20.5,$item->public_discount);
            }
            $this->assertEquals("success",$voucherAttempt->status);
            $this->assertEquals(20.5,$infoDiscount['discount']);
            $this->assertEquals(true, $success);
            $this->assertEquals(20.5,$infoDiscount['percent']);
            $this->assertEquals("79.50", $order->order_total);
            $this->assertEquals("20.50",$order->discount);
            //$this->assertEquals("74.50",$order->order_total_without_tax);
        }
}