<?php
namespace Tests\Controllers;

use App\Http\Controllers\ReferralController;
use App\Models\Order;
use App\Models\Profile;
use App\Models\ReferralUrl;
use App\Models\Translation;
use App\Models\User;
use Carbon\Carbon;
use TestCase;
use Illuminate\Support\Facades\Mail;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ReferralTest extends TestCase
{

    use DatabaseTransactions;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

    }

    /*
     * Test-data is viewable in database/seeds/VoucherTableSeeder.php
     */
    public function testRelations()
    {
        $referralUrl = ReferralUrl::where(['url' => 'helloWorld11'])->first();
        $this->assertNotNull($referralUrl->user);
    }


    public function testGenerateReferal() {
        Mail::fake();
        app('translator')->setLocale('de-de');
        //$user = factory(\App\Models\User::class)->create();
        $user = User::where(['email' => "tester@test.com"])->first();
        $this->be($user);
        $rurl = ReferralUrl::generateReferral();
        $user = User::where(['email' => "ref@test.com"])->first();
        $this->be($user);
        $code = $rurl->url;
        $order = $this->getOrder($user->id);
        $referralUrl = ReferralUrl::where(['url' => $code])->first();
        $originalShareInstagram = $referralUrl->share_facebook;
        $referralController = new ReferralController();
        $referralExists = $referralController->referralIsOk(['ref_code' => $code]);
        $this->assertEquals(Translation::trans('REFERRAL.CHECK.SUCCESS'),$referralExists['msg']);
        $this->assertEquals(true,$referralExists['success']);
        $referralExists = $referralController->referralIsOk(['ref_code' => 'NOTEXISTINGCODE']);
        $this->assertEquals(Translation::trans('REFERRAL.CHECK.EXISTFAIL'),$referralExists['msg']);
        $this->assertEquals(false,$referralExists['success']);
        $result = ReferralUrl::applyReferral(['ref_code' => $code, 'utm_source' => 'facebook'],$order);
        $order = $result['order'];
        $success = $result['success'];
        $this->assertEquals(true,$success);
        $this->assertEquals("10.00",$order->discount);
        $this->assertEquals("90.00",$order->order_total);

        $referralUrl = ReferralUrl::where(['url' => $code])->first();
        $this->assertEquals($originalShareInstagram+1,$referralUrl['share_facebook']);
    }

    public function testSameUser()
    {
        Mail::fake();
        app('translator')->setLocale('de-de');
        //$user = factory(\App\Models\User::class)->create();
        $user = User::where(['email' => "referalfriend@test.com"])->first();
        $this->be($user);
        $code = 'helloWorld11';
        $order = $this->getOrder($user->id);
        $result = ReferralUrl::checkReferral(['ref_code' => $code, 'utm_source' => 'instagram'],$order);
        $order = $result['order'];
        $success = $result['success'];
        $this->assertEquals(false,$success);
        $result = ReferralUrl::applyReferral(['ref_code' => $code, 'utm_source' => 'instagram'],$order);
        $order = $result['order'];
        $success = $result['success'];
        $this->assertEquals(false,$success);

    }

    public function testReferal()
    {
        Mail::fake();
        app('translator')->setLocale('de-de');
        //$user = factory(\App\Models\User::class)->create();
        $user = User::where(['email' => "ref@test.com"])->first();
        $this->be($user);
        $code = 'helloWorld11';
        $order = $this->getOrder($user->id);
        $referralUrl = ReferralUrl::where(['url' => $code])->first();
        $originalShareInstagram = $referralUrl->share_instagram;
        $referralController = new ReferralController();
        $referralExists = $referralController->referralIsOk(['ref_code' => $code]);
        $this->assertEquals(Translation::trans('REFERRAL.CHECK.SUCCESS'),$referralExists['msg']);
        $this->assertEquals(true,$referralExists['success']);
        $referralExists = $referralController->referralIsOk(['ref_code' => 'NOTEXISTINGCODE']);
        $this->assertEquals(Translation::trans('REFERRAL.CHECK.EXISTFAIL'),$referralExists['msg']);
        $this->assertEquals(false,$referralExists['success']);
        $result = ReferralUrl::applyReferral(['ref_code' => $code, 'utm_source' => 'instagram'],$order);
        $order = $result['order'];
        $success = $result['success'];
        $this->assertEquals(true,$success);
        $this->assertEquals("10.00",$order->discount);
        $this->assertEquals("90.00",$order->order_total);
        $referralUrl = ReferralUrl::where(['url' => $code])->first();
        $this->assertEquals($originalShareInstagram+1,$referralUrl['share_instagram']);

    }

    public function testSecondReferal()
    {
        Mail::fake();
        app('translator')->setLocale('de-de');
        //$user = factory(\App\Models\User::class)->create();
        $referrerUser = User::where(['email' => "ref@test.com"])->first();
        $this->be($referrerUser);
        $this->assertEquals($referrerUser->profile->wallet_amount_cents,0);

        $rurl = ReferralUrl::generateReferral();
        $user = User::where(['email' => "referalfriend@test.com"])->first();
        $this->be($user);
        $code = $rurl->url;
        $order = $this->getOrder($user->id);
        $referralUrl = ReferralUrl::where(['url' => $code])->first();
        $originalShareInstagram = $referralUrl->share_instagram;
        $referralController = new ReferralController();
        $referralExists = $referralController->referralIsOk(['ref_code' => $code]);
        $this->assertEquals(Translation::trans('REFERRAL.CHECK.SUCCESS'),$referralExists['msg']);
        $this->assertEquals(true,$referralExists['success']);
        $referralExists = $referralController->referralIsOk(['ref_code' => 'NOTEXISTINGCODE']);
        $this->assertEquals(Translation::trans('REFERRAL.CHECK.EXISTFAIL'),$referralExists['msg']);
        $this->assertEquals(false,$referralExists['success']);
        $pro = Profile::where(['user_id' => $referrerUser->id])->first();
        $this->assertEquals($pro->wallet_amount_cents,0);


        $result = ReferralUrl::applyReferral(['ref_code' => $code, 'utm_source' => 'instagram'],$order);
        $pro = Profile::where(['user_id' => $referrerUser->id])->first();
        $this->assertEquals($pro->wallet_amount_cents,10);
        $order = $result['order'];
        $success = $result['success'];
        $this->assertEquals(true,$success);
        $this->assertEquals("10.00",$order->discount);
        $this->assertEquals("90.00",$order->order_total);
        $referralUrl = ReferralUrl::where(['url' => $code])->first();
        $originalShareInstagram = $originalShareInstagram+1;
        $this->assertEquals($originalShareInstagram,$referralUrl['share_instagram']);

        $order = $this->getOrder($user->id);

        $result = ReferralUrl::applyReferral(['ref_code' => $code, 'utm_source' => 'instagram'],$order);

        $pro = Profile::where(['user_id' => $referrerUser->id])->first();
        $this->assertEquals($pro->wallet_amount_cents,10);
        $order = $result['order'];
        $success = $result['success'];
        $this->assertEquals(false,$success);
        $this->assertNull($order->discount);
        $this->assertEquals("100.00",$order->order_total);
        $referralUrl = ReferralUrl::where(['url' => $code])->first();
        $this->assertEquals($originalShareInstagram,$referralUrl['share_instagram']);

    }

    private function getOrder($userId){
        $order = new Order;
        $order->order_total = "100.00";
        $order->order_total_without_tax = "95.00";
        $order->region_id = 1;
        $order->country_id = 1;
        $order->user_id = $userId;
        $order->cancellation_deadline = Carbon::now()->addDays(5);
        $order->status = Order::STATUS_PENDING;
        $order->save();
        return $order;
    }
}