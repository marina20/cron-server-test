<?php
namespace Tests\Controllers;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\Profile;
use App\Models\ReferralUrl;
use App\Models\User;
use Carbon\Carbon;
use Laravel\Lumen\Testing\DatabaseTransactions;
use TestCase;

class ReferralWalletTest extends TestCase
{

    use DatabaseTransactions;

    public function testThat()
    {
        app('translator')->setLocale('de-de');
        $user = factory(User::class)->create();
        $this->be($user);
        $profile = Profile::firstOrCreate(['user_id' => $user->id, 'wallet_amount_cents' => 15.00]);
        $order = $this->getOrder($user->id);
        $this->assertEquals($order->order_total, "100.00");
        $this->assertEquals($profile->wallet_amount_cents, 15.00);
        $order = ReferralUrl::applyWallet($order);
        foreach($order->items as $item){
            $this->assertEquals($item->wallet_discount, 15);
            $this->assertEquals($item->total_discount, 15);
            $this->assertEquals($item->item_price, 5.3125);
        }
        $profile = Profile::where(['user_id' => $user->id])->first();
        $this->assertEquals($profile->wallet_amount_cents, 0.00);
        $this->assertEquals($order->order_total, "85.00");
    }

    public function testTooMuch()
    {
        app('translator')->setLocale('de-de');
        $user = factory(User::class)->create();
        $this->be($user);
        $profile = Profile::firstOrCreate(['user_id' => $user->id, 'wallet_amount_cents' => 120.00]);
        $order = $this->getOrder($user->id);
        $this->assertEquals($order->order_total, "100.00");
        $this->assertEquals($profile->wallet_amount_cents, 120.00);
        $order = ReferralUrl::applyWallet($order);
        $profile = Profile::where(['user_id' => $user->id])->first();
        foreach($order->items as $item){
            $this->assertEquals($item->wallet_discount, 100);
            $this->assertEquals($item->total_discount, 100);
            $this->assertEquals($item->item_price, 0);
        }
        $this->assertEquals($profile->wallet_amount_cents, 20.00);
        $this->assertEquals($order->order_total, "0.00");
    }

    public function testNoneCharge()
    {
        app('translator')->setLocale('de-de');
        $user = factory(User::class)->create();
        $this->be($user);
        $profile = Profile::firstOrCreate(['user_id' => $user->id, 'wallet_amount_cents' => 0.00]);
        $order = $this->getOrder($user->id);
        $this->assertEquals($order->order_total, "100.00");
        $this->assertEquals($profile->wallet_amount_cents, 0.00);
        $order = ReferralUrl::applyWallet($order);
        $profile = Profile::where(['user_id' => $user->id])->first();
        foreach($order->items as $item){
            $this->assertEquals($item->wallet_discount, 0);
            $this->assertEquals($item->total_discount, 0);
            $this->assertEquals($item->item_price, 6.25);
        }
        $this->assertEquals($profile->wallet_amount_cents, 0.00);
        $this->assertEquals($order->order_total, "100.00");
    }
    private function getOrder($userId){
        $order = new Order;
        $order->order_total = "100.00";
        $order->order_total_without_tax = "95.00";
        $order->region_id = 1;
        $order->country_id = 1;
        $order->user_id = $userId;
        $order->cancellation_deadline = Carbon::now()->addDays(5);
        $order->save();
        $pirateProduct = Product::where(['product_identifier_letter' => 'P'])->first();
        $order_items_1 = factory(OrderItem::class)->create([
            "item_name"=> "Anything",
            "item_type"=> "line_item",
            "order_id"=> $order->id,
            "product_id"=> $pirateProduct->id,
            "qty"=> "16",
            "item_price"=>"6.25",
            "total"=> "100.00",
            "total_tax"=> "1.51",
            "subtotal"=> "62.00",
            "subtotal_tax"=> "1.51",
            "total_discount"=> "0",
            "total_discount_tax"=> "0",
            "tax_rate"=> $pirateProduct->tax_percentage,
            "created_at"=> "2018-07-06 13:17:16",
            "updated_at"=> "2018-07-06 13:17:16"
        ]);
        return $order;
    }
}

?>